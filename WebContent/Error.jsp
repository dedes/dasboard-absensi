<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<logic:messagesPresent>
    <!-- <div id="errPup"> -->
    <!-- <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"> -->
    <!-- <tr> -->
    <!-- <td> -->
    <!-- <font class="errMsg"> -->
    <%--             	 <html:errors/> --%>
    <!-- </font> -->
    <!-- </td> -->
    <!-- </tr> -->
    <!-- </table> -->
    <!-- </div> -->
    <script language="javascript">
        alert('<html:errors/>');
    </script>
</logic:messagesPresent>
<logic:present scope="request" name="message">
    <script language="javascript">
        alert('<bean:write name="message" scope="request"/>');
    </script>
</logic:present>
