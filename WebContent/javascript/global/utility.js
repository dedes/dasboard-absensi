function checkDigit(p_strCardNbr) {
    var l_intValue = 0;
    var l_intDigit = 0;

    for (var i = 0; i < 15; i++) {
        l_intDigit = parseInt(p_strCardNbr.charAt(i), 10);
        if ((i + 1) % 2 > 0) {
            l_intDigit = l_intDigit * 2;
        }
        if (l_intDigit > 9) {
            l_intDigit = l_intDigit - 9;
        }
        l_intValue = l_intValue + l_intDigit;
    }
    l_intValue = l_intValue.toString();

    if (l_intValue.charAt(l_intValue.length - 1) == 0) {
        l_intValue = 0;
    }
    else {
        l_intValue = parseInt(10 - l_intValue.charAt(l_intValue.length - 1), 10);
    }

    if (l_intValue == p_strCardNbr.charAt(15)) {
        return true;
    }
    else {
        return false;
    }
}

function isEmail(isi) {
    var myExp = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
    return myExp.test(isi)
}

function fncCharacterFilter(checkOK, checkStr) {
    var allValid = true;
    for (i = 0; i < checkStr.length; i++) {
        ch = checkStr.charAt(i);
        for (j = 0; j < checkOK.length; j++)
            if (ch == checkOK.charAt(j)) break;
        if (j == checkOK.length) {
            allValid = false;
            break;
        }
    }
    return allValid;
}


// Helper function to trim leading and following white spaces
function trim(inputStringTrim) {
    fixedTrim = "";
    lastCh = " ";
    for (x = 0; x < inputStringTrim.length; x++) {
        ch = inputStringTrim.charAt(x);
        if ((ch != " ") || (lastCh != " ")) {
            fixedTrim += ch;
        }
        lastCh = ch;
    }
    if (fixedTrim.charAt(fixedTrim.length - 1) == " ") {
        fixedTrim = fixedTrim.substring(0, fixedTrim.length - 1);
    }
    return fixedTrim
}

//Untuk men-Check karakter khusus yang tidak diperbolehkan dalam penamaan FILE
function val_char(isi) {
    //var huruf = /[$\\@\\\#%\^\&\*\(\)\[\]\+\_\{\}\<\?\>\|]/;
    var huruf = /[#\*\\\/\:\<\?\>\|\"\'\&\+\@\!\~\?\`\$\%\^\=\.\,\;\{\}\(\)\]\[\_\-]/;
    var pass;
    pass = false;
    for (var i = 0; i < isi.length; i++) {
        if (isi.search(huruf) != -1) {
            pass = true;
            break;
        }
    }
    if (pass) {
        return true; //Ketemu
    }
    else {
        return false;
    }
}