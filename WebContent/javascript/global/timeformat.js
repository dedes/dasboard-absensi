/**
 *
 */
var clockLabel = "";
function formatTime() {
    if ("" == clockLabel) {
        if (null != document.getElementById('time')) {
            clockLabel = document.getElementById('time').innerHTML;
        }
    }
    var inner = clockLabel;
    var eltext = document.getElementById('time');
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
        "Sep", "Oct", "Nov", "Dec"];
    var weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    now = new Date();
    day = now.getDay();
    date = now.getDate();
    month = now.getMonth();
    dayName = weekday[day];
    monthName = monthNames[month];
    year = now.getFullYear();
    hour = now.getHours();
    min = now.getMinutes();
    sec = now.getSeconds();

    if (min <= 9) {
        min = "0" + min;
    }
    if (sec <= 9) {
        sec = "0" + sec;
    }
    if (hour < 10) {
        hour = "0" + hour;
    }
    inner = dayName + ", " + date + " " + monthName + " " + year + " - " + hour
        + ':' + min + ':' + sec;
    if (null != eltext) {
        eltext.innerHTML = inner;
    }
    setTimeout("formatTime()", 1000);
}

function getServerDate() {
    var label = "";
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
        "Sep", "Oct", "Nov", "Dec"];
    now = new Date();
    day = now.getDay();
    date = now.getDate();
    monthName = monthNames[month];
    year = now.getFullYear();
    label = date + " " + monthName + " " + year;
    return label;
}

function getDate() {
    var label = "";
    var datelabel, monthlabel;
    now = new Date();
    date = now.getDate();
    month = now.getMonth() + 1;

    datelabel = date.length < 2 ? "0" + date : date;
    monthlabel = month.length < 2 ? "0" + month : month;

    year = now.getFullYear();
    label = datelabel + "/" + monthlabel + "/" + year;
    return label;
}


window.onload = formatTime;