var counterReport = 0;

function isValidSQLString(value) {

    var myExp = /'|"|&|<|>|`|~|!|@|#/;
    return !(myExp.test(value));
}

function showlayer() {
    document.all['mess'].style.display = "none";
    setInterval("countup()", 1000);
}

function countup() {
    counterReport++;
    document.all['waiting'].innerHTML = "<marque><center><h1>PLEASE WAIT<br>" + parseInt(counterReport) + "</h1><font size=6 color=red><br>The Report is Being Processed.<br>Please do not stop the process while the counter is running</font></center></marque>";
}

function isEmptyTextArea(p_strValue) {
    var l_strValue = p_strValue;
    if (l_strValue != "") {
        // looping for detect enter(return) key and space
        for (var i = 0; i < l_strValue.length; i++) {
            if (l_strValue.charCodeAt(i) == 13 || l_strValue.charCodeAt(i) == 10 || l_strValue.charCodeAt(i) == 32) {
                //true
            }
            else {
                return false;
            }
        }
    }
    return true;
}

function checkDigit(p_strCardNbr) {
    var l_intValue = 0;
    var l_intDigit = 0;

    if (parseInt(p_strCardNbr, 10) == 0) {
        return false;
    }

    for (var i = 0; i < 15; i++) {
        l_intDigit = parseInt(p_strCardNbr.charAt(i), 10);
        if ((i + 1) % 2 > 0) {
            l_intDigit = l_intDigit * 2;
        }
        if (l_intDigit > 9) {
            l_intDigit = l_intDigit - 9;
        }
        l_intValue = l_intValue + l_intDigit;
    }

    l_intValue = l_intValue.toString();
    if (l_intValue.charAt(l_intValue.length - 1) == 0) {
        l_intValue = 0;
    }
    else {
        l_intValue = parseInt(10 - l_intValue.charAt(l_intValue.length - 1), 10);
    }

    if (l_intValue == p_strCardNbr.charAt(15)) {
        return true;
    }
    else {
        return false;
    }
}

function formatNumber(pValue) {
    var lArrPre = pValue.toString().split(',')
    var lStrValue = '';
    var lBytLength = 0;

    lBytLength = lArrPre[0].length - 3;
    while (lBytLength > 0) {
        lStrValue = '.' + lArrPre[0].substr(lBytLength, 3) + lStrValue;
        lBytLength -= 3;
    }
    lBytLength += 3;
    lStrValue = lArrPre[0].substr(0, lBytLength) + lStrValue;
    return lStrValue;
}

function isNumericOnly(isi) {
    var myExp = /^^\d{1,}$$/
    return myExp.test(isi)
}
function isUrl(isi) {
    var myExp = /^http:////([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?$/
    return myExp.test(isi)
}

function isEmail(isi) {
    var myExp = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
    return myExp.test(isi)
}

//Untuk validasi nilai yang diterima hanya berupa angka saja tanpa koma.
function isNumber(isi) {
    var myExp = /^[-]?[0-9]+$|^[-]?[0-9]+[\.]?[0-9]+$|^[-]?[\.][0-9]+$/
    return myExp.test(isi)
}

//Untuk menghilang spasi yang ada.
function trim(svalue) {
    if (svalue != "") {
        while (svalue.charAt(0) == " ")
            svalue = svalue.substring(1, svalue.length)
        while (svalue.charAt(svalue.length - 1) == " ")
            svalue = svalue.substring(0, svalue.length - 1)
        return svalue
    }
    return ""
}

//Untuk men-Check nilai yang hanya Integer saja.
function isInteger(sValue) {
    var lnexp = /^[-]?\d+$|^\d+$/
    return (lnexp.test(sValue)) ? false : true
}

function DB2ConvertDate(strDate, strType) {
    if (strType == "DD/MM/YYYY") {
        strDateTo = strDate.substr(0, 2);
        strMonthTo = strDate.substr(3, 2);
        strYearTo = strDate.substr(6, 4);
        strDateResult = strYearTo + "-" + strMonthTo + "-" + strDateTo;
    } else if (strType == "MM/DD/YYYY") {
        strMonthTo = strDate.substr(0, 2);
        strDateTo = strDate.substr(3, 2);
        strYearTo = strDate.substr(6, 4);
        strDateResult = strYearTo + "-" + strMonthTo + "-" + strDateTo;
    } else if (strType == "DDMMYYYY") {
        strDateTo = strDate.substr(0, 2);
        strMonthTo = strDate.substr(2, 2);
        strYearTo = strDate.substr(4, 4);

        strDateResult = strYearTo + "-" + strMonthTo + "-" + strDateTo;
    }
    if (isDate(strDateTo, strMonthTo, strYearTo))
        return strDateResult;
    else {
        alert("your entry date is not valid date, please verify your data again !!!");
        return false;
    }
}

function OracleConvertDate(strDate, format) {
    if (format == "DDMMYYYY") {
        strDateTo = strDate.substr(0, 2);
        strMonthTo = strDate.substr(2, 2);
        strYearTo = strDate.substr(4, 4);
    } else if (format == "DD/MM/YYYY") {
        strDateTo = strDate.substr(0, 2);
        strMonthTo = strDate.substr(3, 2);
        strYearTo = strDate.substr(6, 4);
    }
    strDateResult = strYearTo + strMonthTo + strDateTo;
    if (isDate(strDateTo, strMonthTo, strYearTo))
        return strDateResult;
    else {
        return false;
    }
}

//Untuk men-Check Tipe Date atau bukan
function isDate(nday, nmonth, nyear) {
    if (nyear < 4) return false;
    nmonth -= 1
    nyear = y2k(nyear)
    var dtemp = new Date(nyear, nmonth, nday)
    var dyear = dtemp.getFullYear()
    if ((dyear == nyear) && (dtemp.getMonth() == nmonth) && (dtemp.getDate() == nday)) {
        return true;
    }
    else {
        return false;
    }
}

//Function Y2K
function y2k(nyear) {
    return ((nyear < 1000) ? nyear + 1900 : nyear)
}

//cek tanggal
function cek_tgl(nmform, tgl, bln, thn, ket) {
    with (nmform) {
        if ((eval(tgl).selectedIndex != 0) || (eval(bln).selectedIndex != 0) || (eval(thn).selectedIndex != 0)) {
            if (!isDate(parseInt(eval(tgl).options[eval(tgl).selectedIndex].value, 10), parseInt(eval(bln).options[eval(bln).selectedIndex].value, 10), parseInt(eval(thn).options[eval(thn).selectedIndex].value, 10))) {
                alert(ket);
                eval(tgl).focus();
                return false;
            }
        }
        return true;
    }
}

function cek_num(nmform, nilai, ket, grant) {
    with (nmform) {
        if (isNumber(nilai)) {
            if (grant == "NO") {
                if (parseFloat(nilai) <= 0) {
                    if (parseFloat(nilai) == 0) {
                        alert(ket + " harus lebih dari 0 ...");
                        return false;
                    }
                    else {
                        alert(ket + " harus diisi angka positif ...");
                        return false;
                    }
                }
                else {
                    if (isInteger(nilai)) {
                        alert(ket + " tidak bisa decimal ...");
                        return false;
                    }
                }
            }
            else {
                if (parseFloat(nilai) < 0) {
                    alert(ket + " harus diisi angka positif ...");
                    return false;
                }
                else {
                    if (isInteger(nilai)) {
                        alert(ket + " tidak bisa decimal ...");
                        return false;
                    }
                }
            }
        }
        else {
            alert(ket + " harus diisi dengan angka ...");
            return false;
        }
        return true;
    }
}

//chkqty(document.frmnetelectrix, txtpdrqty, 'YES','Jumlah PDR Qty'))
function chkqty(nmform, objnm, permit, ket) {
    with (nmform) {
        if (eval(objnm)[0]) {
            for (var i = 0; i < eval(objnm).length; i++) {
                eval(objnm[i]).value = trim(eval(objnm[i]).value);
                if (eval(objnm[i]).value != "") {
                    if (!cek_num(nmform, eval(objnm[i]).value, ket, permit)) {
                        return false;
                        break;
                    }
                }
                else {
                    alert("Silakan isi " + ket);
                    return false;
                    break;
                }
            }
        }
        else {
            eval(objnm).value = trim(eval(objnm).value)
            if (eval(objnm).value != "") {
                if (!cek_num(nmform, eval(objnm).value, ket, permit)) {
                    return false;
                }
            }
            else {
                alert("Silakan isi " + ket);
                return false;
            }

        }
    }
    return true;
}

//format nilai/angka
function formatAngka(num) {
    num = num.toString().replace(/\$|\,/g, '');
    cents = Math.floor((num * 100 + 0.5) % 100);
    num = Math.floor((num * 100 + 0.5) / 100).toString();
    if (cents < 10) cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
    return (num + "." + cents);
}

function Desimal(nmform, nilai, ket, grant) {
    with (nmform) {
        if (isNaN(nilai)) {
            alert(ket + " must be numeric value ...");
            return false;
        }
        else {
            if (parseFloat(nilai) < 0) {
                alert(ket + " must be possitive value ...");
                return false;
            }
        }
        return true;
    }
}

function formatNonDes(num) {
    num = num.toString().replace(/\$|\,/g, '');
    cents = Math.floor((num * 100 + 0.5) % 100);
    num = Math.floor((num * 100 + 0.5) / 100).toString();
    if (cents < 10) cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        //num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
        num = num.substring(0, num.length - (4 * i + 3)) + num.substring(num.length - (4 * i + 3));
    return (num + "." + cents);
}

function validateNumeric(strValue) {
    /******************************************************************************
     DESCRIPTION: Validates that a string contains only valid numbers.

     PARAMETERS:
     strValue - String to be tested for validity

     RETURNS:
     True if valid, otherwise false.

     Changes i make:
     Change decimal separator from . to ,
     ******************************************************************************/
    var objRegExp = /(^\d\d*,\d*$)|(^\d\d*$)|(^,\d\d*$)/;

    //check for numeric characters
    return objRegExp.test(strValue);
}

/*
 * Valid value: [dd*.dd*], [dd*], [.dd*]
 */
function validateNumeric2(strValue) {
    var objRegExp = /(^\d\d*.\d\d*$)|(^\d\d*$)|(^.\d\d*$)/;
    return objRegExp.test(strValue);
}

function validateInteger(strValue) {
    /******************************************************************************
     DESCRIPTION: Validates that a string contains only valid numbers.

     PARAMETERS:
     strValue - String to be tested for validity

     RETURNS:
     True if valid, otherwise false.
     ******************************************************************************/
    var objRegExp = /(^\d\d*$)/;

    //check for numeric characters
    return objRegExp.test(strValue);
}

function validateDate(strValue) {
    /******************************************************************************
     DESCRIPTION: Validates that a string contains only valid date.

     PARAMETERS:
     strValue - String to be tested for validity, format is: dd/mm/yyyy

     RETURNS:
     True if valid, otherwise false.
     ******************************************************************************/
    if (strValue == "")
        return true;
    var m_strDate = strValue;
    var m_arrDate = m_strDate.split("/");
    if (m_arrDate.length != 3)
        return false;
    var m_DAY = m_arrDate[0];
    if (!validateInteger(m_DAY))
        return false;
    var m_MONTH = m_arrDate[1];
    if (!validateInteger(m_MONTH))
        return false;
    var m_YEAR = m_arrDate[2];
    if (!validateInteger(m_YEAR) || (m_YEAR.length != 4))
        return false;

    m_strDate = m_MONTH + "/" + m_DAY + "/" + m_YEAR;
    var testDate = new Date(m_strDate);
    if (testDate.getMonth() + 1 == m_MONTH)
        return true;
    return false;
}


//function checking PK

function isPK(value) {
    var myExp = /\W/;
    return !(myExp.test(value));
}


function numbersonly() {
    if (event.keyCode < 48 || event.keyCode > 57) {
        return false;
    } else {
        return true;
    }
}


function decimalonly() {
    if ((event.keyCode > 47 && event.keyCode < 59) || (event.keyCode == 46)) {
        return true;
    } else {
        return false;
    }
}

function validate(theForm) {
    var len = theForm.elements.length;
    for (i = 0; i < len; i++) {
        elem = theForm.elements[i];

        if (elem.type == 'text') {
            if (!isValidText(elem.value)) {
                alert('Your submitted form contains invalid character !');
                elem.focus();
                return false;
            }
        }

        if (elem.type == 'textarea') {
            if (!isValidText(elem.value)) {
                alert('Your submitted form contains invalid character !');
                return false;
            }
        }
    }
    return true;
}
/*
 this function to check is this year kabisat or not
 */
function isKabisatYear(thn) {
    return ((thn % 100 == 0 && thn % 400 == 0) || (thn % 100 != 0 && thn % 4 == 0));
}