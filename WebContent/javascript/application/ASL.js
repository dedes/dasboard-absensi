var isPanelShown = true;
var surveyors;
var myOptions;
var map;
var toggleFlag = new Array();
var contextP;
var todayDate;

$(document).ready(function () {
    dwr.engine.setActiveReverseAjax(true);
    setInterval("callAjax()", 60000);
});

function showorhide() {
    if (isPanelShown) {
        isPanelShown = false;
        $('a#controlbtn').html('<img id="controlbtnimg" src="../../../images/button/ButtonShowUser.png" width="100" height="31" border="0" alt="HIDE PANEL" />');
        $('div#panel').hide();
    }
    else {
        isPanelShown = true;
        $('a#controlbtn').html('<img id="controlbtnimg" src="../../../images/button/ButtonHideUser.png" width="100" height="31" border="0" alt="HIDE PANEL" />');
        $('div#panel').show();
    }
}
function toggleInfoWindow(index) {
    if (!toggleFlag[index]) {
        surveyors[index].infoWindow.open(map, surveyors[index].mark);
        toggleFlag[index] = true;
        surveyors[index].mark.setAnimation(google.maps.Animation.BOUNCE);
    }
    else {
        surveyors[index].infoWindow.close();
        toggleFlag[index] = false;
        surveyors[index].mark.setAnimation(null);
    }
}

function findMe(ctxPath, branch, surveyorName, status, date) {
    var features = 'scrollbars=yes,width=800,height=600,resizable=yes,top=0,left=0';
    var path;
    var destination;
    path = ctxPath;
    var searchDate = '';
    if (date != '') {
        searchDate += '&search.searchType2=40&search.status2hidden=P&search.tglStart=' + date + '&search.tglEnd=' + date;
    }
    //http://localhost:7001/AndalanMCS/feature/inquiry/Assignment.do?task=Search&search.orderField=80&search.searchType1=20&search.searchValue1=AHMAD%20JAZURI&search.status=All
    destination = path + '/feature/inquiry/Assignment.do?task=Search';
    var param = '&search.orderField=80&search.searchType1=20&search.searchValue1='
        + surveyorName + '&search.status=' + status;
    destination += param;
    var winObjRef = window.open(destination, '', features);
}

function surveyor(name, timestamp, accuracy, lintang, bujur) {
    this.name = name;
    this.timestamp = timestamp;
    this.accuracy = accuracy;
    this.lintang = lintang;
    this.bujur = bujur;
}

function createCircleOverlay(surveyor, peta) {
    var infoText = surveyor.name + "<br/>" +
        surveyor.timestamp + "<br/><br/>";
//	if (surveyor.accuracy != -1) {
//		infoText += "accuracy within " + surveyor.accuracy + " meters.";
//		surveyor.circle = new google.maps.Circle( {
//			map: peta,
//			radius: surveyor.accuracy,
// 			center: surveyor.mark.getPosition()
//		});
//	}	
//	console.log(surveyor.mark);
//	surveyor.mark = new google.maps.Marker({
//		animation: google.maps.Animation.BOUNCE
//	});
//	
    if (surveyor.infoWindow == null) {
        surveyor.infoWindow = new google.maps.InfoWindow({content: infoText});
    }
    else {
        surveyor.infoWindow.setContent(infoText);
    }
}

function createMarker(surveyor, peta) {
    surveyor.mark = new google.maps.Marker({
        position: new google.maps.LatLng(surveyor.lintang, surveyor.bujur),
        title: surveyor.name,
        map: peta
    });
}

function createMarkerEvent(surveyor, peta, infowindow) {
    google.maps.event.addListener(surveyor.mark, 'click', function () {
        infowindow.open(peta, surveyor.mark);
        surveyor.mark.setAnimation(null);
    });
}

function clearCircleOverlay(surveyor) {
    if (surveyor.circle != null) {
        surveyor.circle.setMap(null);
        surveyor.circle = null;
    }
}

function updateLocation(surveyorId, timestamp, latitude, longitude, isGps, accuracy, address) {
    var idx = $('li').index($('li#' + surveyorId));
    if (idx == -1)
        return;

    surveyors[idx].timestamp = timestamp;
    surveyors[idx].lintang = latitude;
    surveyors[idx].bujur = longitude;
    surveyors[idx].isGps = isGps;
    surveyors[idx].accuracy = parseInt(accuracy);
    surveyors[idx].address = address;

    $('#timestamp' + idx).html(surveyors[idx].timestamp);
    $('#address' + idx).html(surveyors[idx].address);
    surveyors[idx].mark.setPosition(new google.maps.LatLng(surveyors[idx].lintang, surveyors[idx].bujur));
    clearCircleOverlay(surveyors[idx]);
    createCircleOverlay(surveyors[idx], surveyors[idx].mark.getMap());
}

function updateTaskCount(ctxPath, branchId, surveyorId, surveyorName, sysdate, osTask, submittedTask) {
    var idx = $('li').index($('li#' + surveyorId));
    if (idx == -1)
        return;

    $('#os' + idx).html(createOSHtml(ctxPath, branchId, surveyorName, osTask));
    $('#submit' + idx).html(createSubmitHtml(ctxPath, branchId, surveyorName, submittedTask, sysdate));
}

function createOSHtml(ctxPath, branchId, surveyorName, osTask) {
    var result;
    var label = $('#lbltask').text();
    var header = label;//+': ';
    var anchor = '<a href="javascript:void(0)" onclick="findMe('
        + "'" + ctxPath + "', '" + branchId + "', '" + surveyorName
        + "','All','');" + '">[@count]</a>';
//	if (parseInt(osTask) > 0) {
//		var tag = '[@count]';
//		var temp = anchor.replace(tag, osTask);
//		result = header + temp;
//	}
//	else {
//		result = header + osTask;
//	}
    var tag = '[@count]';
    var temp = anchor.replace(tag, label);
    result = temp;
    return result;
}

function createSubmitHtml(ctxPath, branchId, surveyorName, submittedTask, sysdate) {
    var result;
    var label = $('#lblsubmitted').text();
    var header = label + ': ';
    var anchor = '<a href="javascript:void(0)" onclick="findMe('
        + "'" + ctxPath + "', '" + branchId + "', '" + surveyorName
        + "','S','" + sysdate + "');" + '">[@count]</a>';
    if (parseInt(submittedTask) > 0) {
        var tag = '[@count]';
        var temp = anchor.replace(tag, submittedTask);
        result = header + temp;
    }
    else {
        result = header + submittedTask;
    }
    return result;
}

function callAjax() {
    var branchId = $('#searchBranch').val();
    var job = $('#searchJob').val();
    Dashboard.reloadDashboard(branchId, job, function (data) {
        if (data.length > 0) {
            $.each(data, function (idx, obj) {
                updateLocation(obj.surveyorId, obj.timestamp, obj.latitude,
                    obj.longitude, obj.isGps, obj.accuracy, obj.address);
                updateTaskCount(contextP, obj.branchId, obj.surveyorId,
                    obj.surveyorName, todayDate, obj.cntOsTask, obj.cntSubmittedTask);
            });
        }
    });
}