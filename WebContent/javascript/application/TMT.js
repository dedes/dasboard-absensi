$(document).ready(function () {
    dwr.engine.setActiveReverseAjax(true);
    setInterval("callAjax()", 30000);
});

function updateAssignmentList(userId, listOfAssignment) {
    var idx = $('div').index($('div#' + userId));
    if (idx == -1) {
        return;
    }
    else {
        $('#' + userId).empty();
        if (listOfAssignment.length > 0) {
            $.each(listOfAssignment, function (idx2, obj2) {
                var styleclass = '';
                var assDate = valueChecker(obj2.assignmentDate);
                var revDate = valueChecker(obj2.retrieveDate);
                var subDate = valueChecker(obj2.submitDate);
//				var finDate = valueChecker(obj2.finalizationDate);


                var id = $('#id').val();
                var nn = $('#nn').val();
                var rr = $('#rr').val();
                var ss = $('#ss').val();
                var aa = $('#aa').val();

                console.log(obj2.assignmentStatus);
                if (obj2.assignmentStatus == "S") {
                    styleclass = 'divShow divGreen';
                } else if (obj2.assignmentStatus == "R") {
                    styleclass = 'divShow divYellow';
                } else if (obj2.assignmentStatus == "N") {
                    styleclass = 'divShow divRed';
                } else if (obj2.assignmentStatus == "A") {
                    styleclass = 'divShow divRed';
                }

                $('#' + userId).append("<div class=\"" + styleclass + "\" onclick=\"openViewDetail('" + obj2.mobileAssignmentId + "')\">" +
                    id + ":" + obj2.mobileAssignmentId +
//						"<div class=\"divWhite\">"+nn+"/"+aa+":"+obj2.assignmentDate+
                    "<div class=\"divWhite\">" + nn + ":" + obj2.assignmentDate +
                    "<br />" + rr + ":" + obj2.retrieveDate +
                    "<br />" + ss + ":" + obj2.submitDate + "</div></div>");

//				if(obj2.assignmentStatus=="S"){
//					$('#'+userId).append("<div class=\"divShow divGreen\" onclick=\"openViewDetail('"+obj2.mobileAssignmentId+"')\">"+
//							id+":"+obj2.mobileAssignmentId+
//							"<div class=\"divWhite\">"+nn+"/"+aa+":"+obj2.assignmentDate+
//							"<br />"+rr+":"+obj2.retrieveDate+
//							"<br />"+ss+":"+obj2.submitDate+"</div></div>");
//				}
//				else if(obj2.assignmentStatus=="N" || obj2.assignmentStatus=="A"){
//						$('#'+userId).append("<div class=\"divShow divYellow\" onclick=\"openViewDetail('"+obj2.mobileAssignmentId+"')\">"+
//								id+":"+obj2.mobileAssignmentId+
//							"<div class=\"divWhite\">"+nn+"/"+aa+":"+obj2.assignmentDate+
//							"<br />"+rr+":"+obj2.retrieveDate+
//							"<br />"+ss+":"+obj2.submitDate+"</div></div>");
//				}else if(obj2.assignmentStatus=="U"){
//						$('#'+userId).append("<div class=\"divShow divYellow\" onclick=\"openViewDetail('"+obj2.mobileAssignmentId+"')\">"+
//								id+":"+obj2.mobileAssignmentId+
//							"<div class=\"divWhite\">"+nn+"/"+aa+":"+obj2.assignmentDate+
//							"<br />"+rr+":"+obj2.retrieveDate+
//							"<br />"+ss+":"+obj2.submitDate+"</div></div>");
//				}
            });
        }
    }
}

function callAjax() {
    $("#formMonitoring").submit();
//	var branchId = $('#searchBranch').val();
//	console.log(branchId);
//	Monitoring.reloadMonitor(branchId, function(data) {
//		console.log(data.length);
//		if (data.length > 0) {
//			$.each(data, function(idx, obj){
//				updateAssignmentList(obj.userId,obj.listOfAssignment);
//			});
//		}
//	});
}

function valueChecker(dataval) {
    if (dataval == undefined || dataval == null || dataval == '') {
        return '-';
    } else {
        return dataval;
    }
}

