<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html>
<head>
    <title>Error Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <link href="../style/application.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="../javascript/global/global.js"></script>
    <script language="javascript">
        function toggleStackTrace() {
            if (st.style.display == "none") {
                st.style.display = "inline";
            } else {
                st.style.display = "none";
            }
        }
        function hideMe() {
            var divid = document.getElementById("desc");
            divid.style.display = "none";
        }
        function showMe() {
            var divid = document.getElementById("desc");
            divid.style.display = "inline";
        }
    </script>
</head>

<body bgcolor="#ffffff" oncontextmenu="return false;" onload="javascript:hideMe();">
<%
    Exception ex = (Exception) request
            .getAttribute("org.apache.struts.action.EXCEPTION");
    String msg = ex.toString();
    String stackTrace = treemas.util.tool.Tools.getStackTrace(ex);
%>
<table width="95%" border="0" cellpadding="0" cellspacing="0">
    <tr class="class">
        <td class="tdtopleft">&nbsp;</td>
        <td class="tdtopcenter">
            <div align="center"><font color="#F00">UNEXPECTED INTERNAL ERROR</font></div>
        </td>
        <td class="tdtopright">&nbsp;</td>
    </tr>
</table>
<a href="javascript:showMe();">Show Error</a>
<% if (response.getStatus() == 500) { %>
<jsp:include page="internalerror.jsp"/>
<%} else if (response.getStatus() == 404) { %>
<jsp:include page="notfound.jsp"/>
<%} %>
<div id="desc">
    <table width="95%" border="0" cellpadding="0" cellspacing="0"
           class="tablegrid">
        <tr>
            <td width="10">&nbsp;</td>
            <td>Message: <%=msg%>
            </td>
        </tr>
        <tr>
            <td width="10">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="10">&nbsp;</td>
            <td><a href="javascript:toggleStackTrace();"
                   title="Show full stack trace">Stack trace</a>
            </td>
        </tr>
        <tr>
            <td width="10">&nbsp;</td>
            <td>
                <div id="st" style="DISPLAY: none">
                    <textarea readonly="readonly" rows="50" cols="110"><%=stackTrace%></textarea>
                </div>
            </td>
        </tr>
        <tr>
            <td width="10">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</div>
</body>
</html>
