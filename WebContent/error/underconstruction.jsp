<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <head>
        <title><bean:message key="errors.404.header"/></title>
    </head>
    <body bgcolor="#000">
    <center>
        <img src="images/uconst.png" alt="PAGE NOT FOUND" title="PAGE NOT FOUND"/>
        <h3 style="color: #F00;"><bean:message key="errors.404.headerII"/></h3>
        <form method="post">
            <br>
            <br>
        </form>
    </center>
    </body>
</html:html>
