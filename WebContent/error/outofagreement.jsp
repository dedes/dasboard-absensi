<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <head>
        <title>ERROR OUT OF AGREEMENT</title>
    </head>
    <body bgcolor="#ffffff">
    <center>
        <img src="images/OUT_OF_AGREEMENT.png" alt="out of agreement" title="OUT OF AGREEMENT"/>
        <logic:present scope="request" name="msg">
            <h3 style="color: #F00;"><bean:write name="msg" scope="request"/></h3>
        </logic:present>
        <form method="post">
            <br>
            <br>
        </form>
    </center>
    </body>
</html:html>
