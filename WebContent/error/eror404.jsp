<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>(404)NotFound</title>
    </head>
    <body bgcolor="#ffffff">
    <center>
        <img src="../images/404.png" alt="PAGE NOT FOUND" title="PAGE NOT FOUND"/>
        <h3 style="color: #F00;"><bean:message key="errors.404.header"/></h3>
        <form method="post">
            <br>
            <br>
        </form>
    </center>
    </body>
</html:html>
