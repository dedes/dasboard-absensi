<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <head>
        <title><bean:message key="errors.402.header"/></title>
    </head>
    <body bgcolor="#ffffff">
    <center>
        <img src="images/notauthorized.png" alt="not authorized" title="NOT AUTHORIZED"/>
        <h3 style="color: #F00;"><bean:message key="errors.all.message"/></h3>
        <form method="post">
            <br>
            <br>
        </form>
    </center>
    </body>
</html:html>
