<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "USER DETAIL"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>

        <script language="javascript">
            function callMe() {
                var divid = document.getElementById("showMenuLevel");
                var vis = divid.style.display;
                if ("none" == vis) {
                    showMe();
                } else {
                    hideMe();
                }
            };

            function hideMe() {
                var divid = document.getElementById("showMenuLevel");
                var showid = document.getElementById("showId");
                var hideid = document.getElementById("hideId");
                divid.style.display = 'none';
                showid.style.display = 'inline';
                hideid.style.display = 'none';
            };

            function showMe() {
                var divid = document.getElementById("showMenuLevel");
                var showid = document.getElementById("showId");
                var hideid = document.getElementById("hideId");
                divid.style.display = 'inline';
                showid.style.display = 'none';
                hideid.style.display = 'inline';
            };
        </script>
    </head>
    <body bgcolor="#FFFFFF" onload="showMe()">
    <html:form action="/appadmin/User.do" method="POST">
        <html:hidden property="task" value="Load"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="userDetailForm" property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.id"/></td>
                    <td class="Edit-Right-Text">
                        <bean:write name="userDetailForm" property="userid"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.name"/></td>
                    <td class="Edit-Right-Text">
                        <bean:write name="userDetailForm" property="fullname"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.password"/></td>
                    <td class="Edit-Right-Text">
                        <bean:write name="userDetailForm" property="sqlpassword"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.job.desc"/></td>
                    <td class="Edit-Right-Text">
                        <bean:write name="userDetailForm" property="job_descName"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text">
                        <bean:write name="userDetailForm" property="branchname"/>
                    </td>
                </tr>
            </table>

            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="40%" height="30">&nbsp;</td>
                    <td width="55%" align="right">
                        <a href="<%=request.getContextPath()%>/ChangePassword.do"><img
                                src="../../images/button/ButtonChangePassword.png" width="100" height="31"
                                border="0"></a>
                        <div id="showId">
                            <a href="javascript:callMe();"><img src="../../images/button/ButtonShowMenu.png" width="100"
                                                                height="31" border="0"></a>
                        </div>
                        <div id="hideId">
                            <a href="javascript:callMe();"><img src="../../images/button/ButtonHideMenu.png" width="100"
                                                                height="31" border="0"></a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="showMenuLevel">
            <div align="center">
                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr class="Top-Table-Header">
                        <td class="Top-Table-Header-Left"></td>
                        <td class="Top-Table-Header-Center">MENU DETAIL</td>
                        <td class="Top-Table-Header-Right"></td>
                    </tr>
                </table>
                <logic:notEmpty name="list" scope="request">
                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <logic:iterate id="listParam" name="list" scope="request" indexId="index">
                            <bean:define id="menuResult" name="listParam" property="result" type="java.lang.String"/>
                            <bean:define id="space" name="listParam" property="webspace" type="java.lang.String"/>
                            <tr align="center">
                                <td width="2%"></td>
                                <td align="left" width="90%">
                                    <%
                                        if (0 == index) {
                                            out.print("<div>");
                                            out.print("<img style='vertical-align:middle;' src='../../images/menu/thispc.gif' border='0'>");
                                            out.print("<span style=''><strong>&nbsp;&nbsp;Application Directory</strong></span>");
                                            out.print("<br/></div>");
// 									out.print("<span style='vertical-align: middle;'><img src='../../images/menu/thispc.gif' border='0'><strong>Application Directory</strong></span><br/>");
                                        }

                                        String isfolder = "";
                                        if ("RD".equalsIgnoreCase(menuResult)) {
                                            isfolder = "../../images/menu/open_folder.png";
                                        } else {
                                            isfolder = "../../images/menu/menufile.png";
                                        }
                                    %>
                                    <div>
                                        <% out.print(space); %>
                                        <img style='vertical-align:middle;' src='<%=isfolder %>' border='0'>
                                        <span style=''>
											<logic:equal value="root" name="listParam" property="parentid">
												<strong>
											</logic:equal>
											<bean:write name="listParam" property="prompt"/>
											<logic:equal value="root" name="listParam" property="parentid">
												</strong>
											</logic:equal>
										</span>
                                    </div>
                                </td>
                            </tr>
                        </logic:iterate>

                    </table>
                </logic:notEmpty>
                <logic:empty name="list" scope="request">
                    <font class="errMsg" style="text-align: center;"><bean:message key="common.listnotfound"/></font>
                </logic:empty>
            </div>
        </div>
    </html:form>
    </body>
</html:html>