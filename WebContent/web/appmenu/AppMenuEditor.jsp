<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.TITLE_APP_MENU; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>

        <script language="javascript">

            var arrObjMenu = new Array;
            var arrObjForm = new Array;
            var i = 0;
            var j = 0;

            function objMenu(appid, menuid, menulabel) {
                this.appid = appid;
                this.menuid = menuid;
                this.menulabel = menulabel;
            }

            function objForm(formid, formlabel) {
                this.formid = formid;
                this.formlabel = formlabel;
            }

            <%
            ArrayList menuList = (ArrayList) (request.getAttribute("listFormid"));
            for (int i=0; i<menuList.size(); i++) {
                treemas.util.struts.MultipleBaseBean bean = (treemas.util.struts.MultipleBaseBean) menuList.get(i);
                String[] optKeys = bean.getOptKey().split("-");
                String formId = optKeys[0];
                String appId = optKeys[1];
                String prompt = bean.getOptLabel();
                    if(1==menuList.size()){
             %>
            arrObjMenu[<%=i%>] = new objMenu("<%=appId%>", "<%=formId%>", "<%=prompt%>");
            <%
                    }else{
            %>
            arrObjMenu[<%=i%>] = new objMenu("<%=appId%>", "<%=formId%>", "<%=prompt%>");

            <%
                    }
                }
            %>

            function findChild(appId) {
                arrObjFormTemp = new Array;
                return arrObjFormTemp;
            }


            function movePage(task) {
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }

            function dropdownChange() {
                <logic:equal value="Add" property="task" name="appMenuForm">
                var appidval = document.getElementById("appidselect").value;

                for (i = 0; i < arrObjMenu.length; i++) {
                    if (appidval == this.arrObjMenu[i].appid) {
                        this.arrObjForm[j] = new objForm(arrObjMenu[i].menuid, arrObjMenu[i].menulabel);
                        j++;
                    }
                }

                j = 0;
                var element = document.getElementById("formidselect");
                element.innerHTML = "";
                for (i = 0; i < arrObjForm.length; i++) {
                    if (i == 0) {
                        var optNull = document.createElement('option');
                        optNull.value = '';
                        optNull.innerHTML = 'blank';
                        element.appendChild(optNull);
                    }
                    var opt = document.createElement('option');
                    opt.value = arrObjForm[i].formid;
                    opt.innerHTML = arrObjForm[i].formlabel;
                    element.appendChild(opt);
                }
                arrObjForm = new Array();
                </logic:equal>
            }

            function action(form, task, id) {
                if (!nullOrEmpty(id)) {
                    form.task.menuid = id;
                }
                form.task.value = task;
                form.submit();
            }

        </script>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;" onload="dropdownChange()">
    <html:form action="/appadmin/Menu.do" method="POST">
        <html:hidden property="task"/>
        <%-- 		<html:hidden property="menuid"/> --%>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="appMenuForm" property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="lookup.privilege.appid"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appMenuForm">
                            <html:select property="appid" styleId="appidselect" onchange="dropdownChange()">
                                <logic:notEmpty name="listAppid" scope="request">
                                    <html:options collection="listAppid" property="optKey" labelProperty="optLabel"/>
                                </logic:notEmpty>
                            </html:select>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appMenuForm">
                            <html:hidden property="appid" write="true"/>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.formid"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appMenuForm">
                            <html:select property="formid" styleId="formidselect">
                                <%-- 							<logic:notEmpty name="listFormid" scope="request"> --%>
                                <%-- 								<html:options collection="listFormid" property="optKey" labelProperty="optLabel"/> --%>
                                <%-- 							</logic:notEmpty>  --%>
                            </html:select>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appMenuForm">
                            <html:hidden property="formid" write="true"/>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.menu.menuid"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appMenuForm">
                            <html:text property="menuid" styleClass="TextBox" size="15" maxlength="15"/>
                            <font color="#FF0000">*) <html:errors property="menuid"/></font>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appMenuForm">
                            <html:hidden property="menuid" write="true"/>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.menu.parentid"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="parentid" styleClass="TextBox" size="10" maxlength="10"/>
                        <font color="#FF0000">*) <html:errors property="parentid"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.menu.prompt"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="prompt" styleClass="TextBox" size="50" maxlength="50"/>
                        <font color="#FF0000">*) <html:errors property="prompt"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.menu.level"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="level" styleClass="TextBox" size="2" maxlength="2"/>
                        <font color="#FF0000">*) <html:errors property="level"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.menu.result"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="result" styleClass="TextBox">
                            <html:option value="RD"><bean:message key="combo.label.folder"/></html:option>
                            <html:option value="FD"><bean:message key="combo.label.file"/></html:option>
                        </html:select>
                            <%-- 			      		<html:text property="result" styleClass="TextBox"  size="2" maxlength="2" /> --%>
                            <%-- 			   			<font color="#FF0000">*) <html:errors property="result" /></font> --%>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.menu.seqorder"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="seqorder" styleClass="TextBox" size="2" maxlength="2"/>
                        <font color="#FF0000">*) <html:errors property="seqorder"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.menu.active"/></td>
                    <td class="Edit-Right-Text">
                        <html:checkbox property="active" value="1"/>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <% String action = ""; %>
                        <logic:equal value="Add" property="task" name="appMenuForm">
                            <% action = Global.WEB_TASK_INSERT; %>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appMenuForm">
                            <% action = Global.WEB_TASK_UPDATE; %>
                        </logic:equal>
                        <a href="javascript:action(document.appMenuForm,'<%=action%>','');">
                            <img src="../../images/button/ButtonSubmit.png" alt="Action <%=action %>"
                                 title="<bean:message key="tooltip.action"/> <%=action %>" width="100" height="31"
                                 border="0">
                        </a>
                        &nbsp;
                        <a href="javascript:movePage('Load');">
                            <img src="../../images/button/ButtonCancel.png" alt="Cancel"
                                 title="<bean:message key="tooltip.cancel"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>