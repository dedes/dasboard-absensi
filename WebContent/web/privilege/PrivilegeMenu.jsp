<%@page import="java.util.ArrayList" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "MENU USER PRIVILEGE"; %>
<html:html>
<html:base/>
<head>
	<title><bean:message key="user.privilege"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
	
    <script language="javascript" SRC="../../javascript/global/jquery-1.10.2.js"></script>
    <script language="javascript" SRC="../../javascript/global/jquery-all.js"></script>
        
	<link rel="stylesheet" href="../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../assets/css/custom.css">
	<link rel="stylesheet" href="../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../assets/css/skins/green.css">

	<script src="../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	  <script language="javascript">
            var arrObjMenu = new Array;
            var i = 0;

            function objMenu(menuid, parentid, menutype) {
                this.menuid = menuid;
                this.parentid = parentid;
                this.menutype = menutype;
            }

            <%
                ArrayList menuList = (ArrayList) (request.getAttribute("listMenuPrivilege"));
                for (int i=0; i<menuList.size(); i++) {
                    treemas.application.general.privilege.menu.PrivilegeMenuBean bean = (treemas.application.general.privilege.menu.PrivilegeMenuBean) menuList.get(i);
            %>
            arrObjMenu[<%=i%>] = new objMenu("<%=bean.getMenuid()%>", "<%=bean.getParentid()%>", "<%=bean.getResult()%>");
            <%
                }
            %>

            function findParent(menuid) {
                for (i = 0; i < arrObjMenu.length; i++) {
                    if (menuid == arrObjMenu[i].menuid) {
                        return (arrObjMenu[i].parentid);
                    }
                }
                return ("none");
            }

            function findIdxMenu(start, menuid) {
                for (i = start; i < arrObjMenu.length; i++) {
                    if (menuid == arrObjMenu[i].menuid) {
                        return (i);
                    }
                }
                return (-1);
            }

            function findChild(start, menuid) {
                for (i = start; i < arrObjMenu.length; i++) {
                    if (menuid == arrObjMenu[i].parentid) {
                        return (i);
                    }
                }
                return (-1);
            }

            function changeChild(start, menuid, check) {
                var i;
                var idxChildId = findChild(start, menuid);
                if (idxChildId != -1) {
                    for (i = idxChildId; i < arrObjMenu.length && menuid == arrObjMenu[i].parentid; i++) {
                        document.privilegeForm.menuidlist[i].checked = check;
                        if (arrObjMenu[i].menuType == "RD")
                            changeChild(i, arrObjMenu[i].menuid, check);
                    }
                    changeChild(i, menuid, check);
                }
            }

            function checkedParent(chkbox) {
                var tmpChecked = chkbox.checked;
                var tmpmenuid = chkbox.value;
                if (tmpChecked == false) {
                    changeChild(0, tmpmenuid, tmpChecked);
                }
                else {
                    parentId = findParent(tmpmenuid);
                    while (parentId != "none") {
                        for (i = 0; i < document.privilegeForm.menuidList.length; i++)
                            if (document.privilegeForm.menuidlist[i].value == parentId) {
                                document.privilegeForm.menuidlist[i].checked = tmpChecked;
                                break;
                            }
                        parentId = findParent(parentId);
                    }
                    idxParentId = findIdxMenu(0, tmpmenuid);
                    if (idxParentId > -1) {
                        if (arrObjMenu[i].menuType == "SM") {
                            changeChild(0, tmpmenuid, tmpChecked);
                        }
                    }
                }
            }


            function checkedAll() {
                <%
                    if (menuList.size() > 1) {
                %>
                for (i = 0; i < document.privilegeForm.menuidlist.length; i++)
                    with (document.privilegeForm.menuidlist[i])
                        checked = document.privilegeForm.checkAll.checked;
                <%
                    } else {
                %>
                with (document.privilegeForm.menuidlist)
                    checked = document.privilegeForm.checkAll.checked;
                <%
                    }
                %>
            }

            function movePage(task, idParam) {
                if ('Edit' != task && 'BackEdit' != task) {
// 			ceklist
                    var c = 0;
                    for (i = 0; i < document.privilegeForm.menuidlist.length; i++) {
                        if (document.privilegeForm.menuidlist[i].checked != false) {
                            c = 1;
                            break;
                        }
                    }

                    if (c != '1') {
                        var newgp = '<bean:message key="common.menu.privilege"/>';
                        var message = '<bean:message key="common.harusPilih" arg0="'+newgp+'"/>';
                        alert(message);
                        return;
                    }
                }
                document.privilegeForm.task.value = task;
                document.privilegeForm.submit();
            }

            function action(form, task, id) {
                form.groupid.value = id;
                form.task.value = task;
                form.submit();
            }
        </script>
         <%@include file="../../Error.jsp" %>
</head>
<body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
				<ol class="breadcrumb bc-2">
					<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="aplikasi.management"/></a></li>
					<li class="active"><a href="<%=request.getContextPath()%>/appadmin/Privilege.do"><bean:message key="user.privilege"/></a></li>
				</ol>
				<div class="row">
					<html:form action="/appadmin/Privilege.do" method="POST" styleClass="form-horizontal form-groups-bordered">
			        <html:hidden property="task" value="Load"/>
			        <html:hidden property="checkedMemberList" value=""/>
			        <html:hidden property="template"/>
			        <html:hidden property="paging.dispatch" value="Go"/>
					<bean:define id="indexLastRow" name="privilegeForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<bean:define id="indexFirstRow" name="privilegeForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="panel panel-primary" data-collapsed="0">
						<div class="panel-heading" style="background:#00a651;">
							<div  class="panel-title"><span style="color:#FFFFFF;"><bean:message key="user.privilege"/></span></div>
							<div class="panel-options">
								<a href="javascript:void(0);" data-rel="collapse" style="color:#FFFFFF;"><i class="entypo-down-open"></i></a>
								<a href="javascript:movePage('Load');" data-rel="reload" style="color:#FFFFFF;"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<div class="row">
											<div class="form-group">
												<label for='appid' class="col-sm-3 control-label"><bean:message key="lookup.privilege.appid" /></label>
												<div class="col-sm-5">
													<html:hidden property="appid" write="true"/>
												</div>
											</div>
											<div class="form-group">
												<label for='appid' class="col-sm-3 control-label"><bean:message key="lookup.privilege.appname" /></label>
												<div class="col-sm-5">
													<html:hidden property="appname" write="true"/>
												</div>
											</div>
											<div class="form-group">
												<label for='appid' class="col-sm-3 control-label"><bean:message key="lookup.privilege.groupid" /></label>
												<div class="col-sm-5">
													<html:hidden property="groupid" write="true"/>
												</div>
											</div>
											<div class="form-group">
												<label for='appid' class="col-sm-3 control-label"><bean:message key="lookup.privilege.groupname" /></label>
												<div class="col-sm-5">
													<html:hidden property="groupname" write="true"/>
												</div>
											</div>
										</div>
									</div>
								</div>
							<div class="row">
								<div class="col-md-12 col-xl-8" >
									<table class="table responsive table-bordered"  >
										<thead >
											<tr align="center">
												<td width="3%" align="center">
											      	<logic:notEmpty name="listMenuPrivilege" scope="request">
							                            <input type="checkbox" value="ON" name="checkAll" onclick="javascript:checkedAll()">
							                        </logic:notEmpty>
						                        </td>
										      	<td width="92%" align="center"><bean:message key="common.menu"/></td>
										    </tr>
										</thead>
										<tbody>
											<logic:notEmpty name="listMenuPrivilege" scope="request">
						                    <logic:iterate id="listParam" name="listMenuPrivilege" scope="request" indexId="index">
						                        <bean:define id="spaceLevel" name="listParam" property="filelevel" type="java.lang.Integer"/>
						                        <bean:define id="space" type="java.lang.String" value=""/>
						                        <%
						                            int i = 1;
						                            while (i < spaceLevel.intValue()) {
						                                space = space + "\t";
						                                i++;
						                            }
						                            space += "-";
						                        %>
						                        <tr class="evalcol">
						                            <td align="center">
						                                <html:multibox name="privilegeForm" property="menuidlist"
						                                               onclick="javascript:checkedParent(this)">
						                                    <bean:write name="listParam" property="menuid"/>
						                                </html:multibox>
						                            </td>
						                            <td align="left">
						                                <%=space %>
						                                <logic:equal name="listParam" property="result" value="RD">
						                                    <b><bean:write name="listParam" property="prompt"/></b>
						                                </logic:equal>
						                                <logic:notEqual name="listParam" property="result" value="RD">
						                                    <bean:write name="listParam" property="prompt"/>
						                                </logic:notEqual>
						                            </td>
						                        </tr>
						                    </logic:iterate>
						                </logic:notEmpty>
										</tbody>
									</table>
								</div>
							</div>
						<br/>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<logic:equal value="NextAdd" name="privilegeForm" property="task">
									<button type="button" onclick="javascript:movePage('Save');" class="btn btn-green"  title="<bean:message key="tooltip.add"/>"><bean:message key="tooltip.add"/></button>
					                <button type="button" onclick="javascript:movePage('BackEdit');" class="btn btn-grey"  title="<bean:message key="tooltip.back"/>"><bean:message key="tooltip.back"/></button>
					             </logic:equal>
					             <logic:notEqual value="NextAdd" name="privilegeForm" property="task">
					             	<button type="button" onclick="javascript:movePage('Update');" class="btn btn-green"  title="<bean:message key="tooltip.add"/>"><bean:message key="tooltip.add"/></button>
					                <button type="button" onclick="javascript:movePage('Edit');" class="btn btn-grey"  title="<bean:message key="tooltip.back"/>"><bean:message key="tooltip.back"/></button>
					            </logic:notEqual>
				             
							</div>
						</div>
					</div> <!--  END PANEL BODY -->
					</div>
					</div>
					</html:form>
				</div>
		</div>	
	</div>
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../assets/js/bootstrap.js"></script>
	<script src="../../assets/js/joinable.js"></script>
	<script src="../../assets/js/resizeable.js"></script>
	<script src="../../assets/js/neon-api.js"></script>
	<script src="../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../assets/js/jquery.validate.min.js"></script>
	<script src="../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../assets/js/jquery.multi-select.js"></script>
	<script src="../../assets/js/neon-chat.js"></script>
	<script src="../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../assets/js/select2/select2.min.js"></script>
	<script src="../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../assets/js/typeahead.min.js"></script>
	<script src="../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../assets/js/neon-demo.js"></script>
</html:html>