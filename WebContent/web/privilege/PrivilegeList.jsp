<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.TITLE_APP_USER;; %>
<html:html>
<html:base/>
<head>
	<title><bean:message key="user.privilege"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
	
    <script language="javascript" SRC="../../javascript/global/jquery-1.10.2.js"></script>
    <script language="javascript" SRC="../../javascript/global/jquery-all.js"></script>
        
	<link rel="stylesheet" href="../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../assets/css/custom.css">
	<link rel="stylesheet" href="../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../assets/css/skins/green.css">

	<script src="../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	  <script language="javascript">
            function openTemplate(form, groupid, appid, groupname) {
                var path = '<%=request.getContextPath()%>';
                var curUri = '<bean:write name="privilegeForm" property="currentUri"/>';
                form.task.value = 'Load';
                form.groupid.value = groupid;
                form.appid.value = appid;
                form.groupname.value = groupname;
                findElement(form, "paging.currentPageNo").value = 1;
                findElement(form, "paging.dispatch").value = null;
                form.action = path + '/feature/group/Template.do?previousUri=' + curUri;
                form.submit();
            }

            function movePage(task) {
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }

            function openLuPrivilege(name, option, groupid) {
                var uri = '<html:rewrite action="/lookup/Privilege"/>?task=Detail&groupid=' + groupid;
                openLU(name, option, window, uri);
            }

            function openLuMember(name, option, groupid, appid, groupname) {
                var member = document.getElementById("member").value;
                var uri = '<html:rewrite action="/lookup/User"/>?task=Load&search.groupid=' + groupid + '&search.appid=' + appid + '&search.appname=' + groupname;
                openLU(name, option, window, uri);
            }

            function setData(map) {
                var members = map.member;
                var member = document.getElementById("member");
                member.value = members;
                console.log(members);
            }

            function action(form, task, groupid, appid) {
                if (task == 'Delete') {
                    if (!confirm('<bean:message key="common.confirm.action"/>'))
                        return;
                    alert('<bean:message key="common.success"/>');
                }
                form.groupid.value = groupid;
                form.appid.value = appid;
                form.task.value = task;
                form.submit();
            }

            function openDetail(styleId) {
                var divid = document.getElementById(styleId);
                var displayStyle = divid.style.display;
                if (displayStyle == "table-row") {
                    divid.style.display = "none";
                } else {
                    divid.style.display = "table-row";
                }
            }
        </script>
         <%@include file="../../Error.jsp" %>
</head>
<body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
				<ol class="breadcrumb bc-2">
					<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="aplikasi.management"/></a></li>
					<li class="active"><a href="<%=request.getContextPath()%>/appadmin/Privilege.do"><bean:message key="user.privilege"/></a></li>
				</ol>
				<div class="row">
					<html:form action="/appadmin/Privilege.do" method="POST">
					 <html:hidden property="currentUri"/>
			        <html:hidden property="task" value="Load"/>
			        <html:hidden property="groupid"/>
			        <html:hidden property="appid"/>
			        <html:hidden property="groupname"/>
			        <html:hidden property="groupmember" styleId="member"/>
			        <html:hidden property="paging.dispatch" value="Go"/>
					<bean:define id="indexLastRow" name="privilegeForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<bean:define id="indexFirstRow" name="privilegeForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="panel panel-primary" data-collapsed="0">
						<div class="panel-heading" style="background:#00a651;">
							<div  class="panel-title"><span style="color:#FFFFFF;"><bean:message key="user.privilege"/></span></div>
							<div class="panel-options">
								<a href="javascript:void(0);" data-rel="collapse" style="color:#FFFFFF;"><i class="entypo-down-open"></i></a>
								<a href="javascript:movePage('Load');" data-rel="reload" style="color:#FFFFFF;"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="appid" class=" control-label"><bean:message key="lookup.privilege.appname"/></label>
												 <html:select property="search.appid" styleClass="form-control">
                                                   <html:option value=""><bean:message key="common.all"/></html:option>
                                                  <logic:notEmpty name="listAppAdmin" scope="request">
                                               <html:options collection="listAppAdmin" property="optKey" labelProperty="optLabel"/>
                                            </logic:notEmpty>
                                         </html:select>		
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="groupid" class=" control-label"><bean:message key="lookup.privilege.groupname"/></label>
												  <html:select property="search.groupid" styleClass="form-control">
		                                             <html:option value=""><bean:message key="common.all"/></html:option>
		                                             <logic:notEmpty name="listAppGroup" scope="request">
		                                             <html:options collection="listAppGroup" property="optKey" labelProperty="optLabel"/>
		                                        	</logic:notEmpty>
		                                 		</html:select>	
											</div>
										</div>
										<div class="col-md-3 col-sm-3">
											<div class="form-group">
												<label style="color:transparent!important;">&nbsp;&nbsp;</label> <br/>
												<button type="button" onclick="javascript:movePage('Search');" class="btn btn-blue"  title="<bean:message key="tooltip.search"/>"><bean:message key="tooltip.search"/></button>
											</div>
										</div>
										<%-- <logic:notEmpty name="list" scope="request">
											<div class="col-md-1 col-sm-2">
												<div class="form-group">
													<label style="color:transparent!important;">&nbsp;&nbsp;</label> 
													<button type="button" onclick="javascript:openDownload('Download','scrollbars=no,width=300,height=100,resizable=no,top=250,left=550');" class="btn btn-blue"  title="<bean:message key="tooltip.download"/>"><bean:message key="tooltip.download"/></button>
												</div>
											</div>
					    				</logic:notEmpty> --%>
									</div>
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="col-md-12 col-xl-8" >
									<table class="table responsive table-bordered" >
										<thead>
											<tr align="center">
				
											<td width="10%" rowspan="2" style="vertical-align : middle;text-align:center;" class="text-nowrap"><bean:message key="lookup.privilege.appid"/></td>
						                    <td width="25%" rowspan="2" style="vertical-align : middle;text-align:center;"><bean:message key="lookup.privilege.appname"/></td>
						                    <td width="10%" rowspan="2" style="vertical-align : middle;text-align:center;"><bean:message key="lookup.privilege.groupid"/></td>
						                    <td width="25%" rowspan="2" style="vertical-align : middle;text-align:center;"><bean:message key="lookup.privilege.groupname"/></td>
						                   <%--  <td width="5%" rowspan="2" align="center"><bean:message key="app.privilege.form"/></td> --%>
						                    <td width="5%" rowspan="2" style="vertical-align : middle;text-align:center;"><bean:message key="common.member"/></td>
						                    <td width="5%" rowspan="2" style="vertical-align : middle;text-align:center;"><bean:message key="common.approval"/></td>
						                    <td width="15%" colspan="3" align="center"><bean:message key="common.action"/></td>
											</tr>
						                    <tr align="center" class="headercol">
						                    <td width="5%" align="center"><bean:message key="common.detail"/></td>
						                    <td width="5%" align="center"><bean:message key="common.edit"/></td>
						                    <td width="5%" align="center"><bean:message key="common.delete"/></td>
						                   </tr>
										</thead>
										<tbody>
										<logic:notEmpty name="list" scope="request" >
											<logic:iterate id="listParam" name="list" scope="request" indexId="index">
												<%
													int x = indexLastRow;
													indexLastRow++;
												%>
						<tr>	
			   			   <td align="center"><bean:write name="listParam" property="appid"/></td>
                           <td align="left"><bean:write name="listParam" property="appname"/></td>
                           <td align="center"><bean:write name="listParam" property="groupid"/></td>
                           <td align="left"><bean:write name="listParam" property="groupname"/></td>
                             
                          <%--  <td align="center">
                             <logic:equal value="1" name="listParam" property="template">
                               <a href="javascript:openTemplate(document.privilegeForm,'<bean:write name="listParam" property="groupid" />','<bean:write name="listParam" property="appid" />','<bean:write name="listParam" property="groupname" />');">
	                               <img alt="form" src="../../images/icon/IconList.gif" width="16" height="16"
	                                  title="Open Template"/>
                			    </a>
                         	 </logic:equal>
                           </td>  --%> 
						 <td align="center">		    
						 <%-- 	<a href="javascript:openLuMember(document.privilegeForm,'<bean:write name="listParam" property="groupid" />','<bean:write name="listParam" property="appid" />','<bean:write name="listParam" property="groupname" />');"> --%>
                             <a href="javascript:openLuMember('Member','scrollbars=yes,width=750,height=500,resizable=yes,top=0,left=0','<bean:write name="listParam" property="groupid" />','<bean:write name="listParam" property="appid" />','<bean:write name="listParam" property="groupname" />');">
                                 <img alt="form" src="../../images/icon/IconMember.gif" width="16" height="16"
                                      title="Open Member"/>
                             </a>
                         </td>
                         <td align="center">
                             <logic:equal value="1" name="listParam" property="isapproved">
                                 <img alt="approved" src="../../images/icon/Smile.gif" width="16" height="16"
                                      title="<bean:message key="tooltip.approve"/>"/>
                             </logic:equal>
                             <logic:notEqual value="1" name="listParam" property="isapproved">
                                 <img alt="waiting for approval" src="../../images/icon/imwaiting.gif" width="36"
                                      height="24" title="<bean:message key="tooltip.notapprove"/>"/>
                             </logic:notEqual>
                         </td>
                         <td align="center">
                             <a href="javascript:openLuPrivilege('Privilege','scrollbars=yes,width=750,height=500,resizable=yes,top=0,left=0','<bean:write name="listParam" property="groupid"/>');">
                                 <img alt="detail" src="../../images/icon/IconLookUp.gif" width="16" height="16"
                                      title="<bean:message key="tooltip.detail"/>"/>
                             </a>
                         </td>
                         <td align="center">
                             <logic:equal value="1" name="listParam" property="isapproved">
                                 <a href="javascript:action(document.privilegeForm,'Edit','<bean:write name="listParam" property="groupid"/>');">
                                     <img alt="edit" src="../../images/icon/IconEdit.gif" width="16" height="16"
                                          title="<bean:message key="tooltip.edit"/>"/>
                                 </a>
                             </logic:equal>
                             <logic:notEqual value="1" name="listParam" property="isapproved">
                                 -
                             </logic:notEqual>
                         </td>
                         <td align="center">
                             <logic:equal value="1" name="listParam" property="isapproved">
                                 <a href="javascript:action(document.privilegeForm, 'Delete','<bean:write name="listParam" property="groupid"/>','<bean:write name="listParam" property="appid"/>');">
                                     <img alt="delete" src="../../images/icon/IconDelete.gif" width="16" height="16"
                                          title="<bean:message key="tooltip.delete"/>"/>
                                 </a>
                             </logic:equal>
                             <logic:notEqual value="1" name="listParam" property="isapproved">
                                 -
                             </logic:notEqual>
                         </td>
                            
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                </tbody>
				</table>
			</div>
		</div>
		<div>
		<logic:notEmpty name="list" scope="request">
			<div class="row">
    			<div class=" col-md-5 "> 
	    			<button type="button" onclick="javascript:moveToPageNumber(document.privilegeForm,'First')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-left"></i></button>
								        	
		        	<button type="button" onclick="javascript:moveToPageNumber(document.privilegeForm,'Prev')" class="btn btn-xs btn-default"><i class="fa fa-angle-left"></i></button>
	   				
	   				<html:text name="privilegeForm" property="paging.currentPageNo" size="3" maxlength="6" styleClass="inpType "/> <bean:message key="paging.of"/> <html:hidden property="paging.totalPage" write="true" />
		    		
		    		<button type="button" onclick="javascript:moveToPageNumber(document.privilegeForm,'Go')" class="btn btn-xs btn-default"><i class="fa fa-search"></i></button>
		    		
		        	<button type="button" onclick="javascript:moveToPageNumber(document.privilegeForm,'Next')" class="btn btn-xs btn-default"><i class="fa fa-angle-right"></i></button>
	   				
	        		<button type="button" onclick="javascript:moveToPageNumber(document.privilegeForm,'Last')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-right"></i></button>
        		</div>
    			<div class="pull-right col-md-4">
   					<label class="pull-right"><bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message key="paging.showing.to"/> <%=indexLastRow-1%> <bean:message key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/> <bean:message key="paging.showing.record"/></label>
    			</div>
	      	</div> 	
  		</logic:notEmpty> 
		</div>
		<br/>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<button type="button" onclick="javascript:movePage('Add');" class="btn btn-green"  title="<bean:message key="tooltip.add"/>"><bean:message key="tooltip.add"/></button>
			</div>
		</div>
	</div> <!--  END PANEL BODY -->
	</div>
</div>
</html:form>
</div>
</div>	
</div>
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../assets/js/bootstrap.js"></script>
	<script src="../../assets/js/joinable.js"></script>
	<script src="../../assets/js/resizeable.js"></script>
	<script src="../../assets/js/neon-api.js"></script>
	<script src="../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../assets/js/jquery.validate.min.js"></script>
	<script src="../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../assets/js/jquery.multi-select.js"></script>
	<script src="../../assets/js/neon-chat.js"></script>
	<script src="../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../assets/js/select2/select2.min.js"></script>
	<script src="../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../assets/js/typeahead.min.js"></script>
	<script src="../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../assets/js/neon-demo.js"></script>
</html:html>