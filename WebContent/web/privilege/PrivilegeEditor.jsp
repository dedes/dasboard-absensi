<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "USER PRIVILEGE"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		
		<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
		
	<!-- 	<link href="../../style/style.css" rel="stylesheet" type="text/css"> -->
		<script language="javascript" SRC="../../javascript/global/global.js"></script>
		<script language="javascript" SRC="../../javascript/global/disable.js"></script>
		<script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
		<script language="javascript" SRC="../../javascript/global/jquery-1.10.2.js"></script>
	    <script language="javascript" SRC="../../javascript/global/jquery-all.js"></script>
		
		<link rel="stylesheet" href="../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
		<link rel="stylesheet" href="../../assets/css/font-icons/entypo/css/entypo.css">
		<link rel="stylesheet" href="../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
		<link rel="stylesheet" href="../../assets/css/bootstrap.css">
		<link rel="stylesheet" href="../../assets/css/neon-core.css">
		<link rel="stylesheet" href="../../assets/css/neon-theme.css">
		<link rel="stylesheet" href="../../assets/css/neon-forms.css">
		<link rel="stylesheet" href="../../assets/css/custom.css">
		<link rel="stylesheet" href="../../assets/js/select2/select2-bootstrap.css">
		<link rel="stylesheet" href="../../assets/js/select2/select2.css">
		<link rel="stylesheet" href="../../assets/css/skins/green.css">
	
		<script src="../../assets/js/jquery-1.11.3.min.js"></script>
	
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	<script language="javascript">

            function movePage(task) {
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }

            function action(form, task, id) {
                form.groupid.value = id;
                form.task.value = task;
                form.submit();
            }

        </script>
    </head>
    <body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
			<ol class="breadcrumb bc-2">
				<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="aplikasi.management"/></a></li>
				<li><a href="<%=request.getContextPath()%>/appadmin/Privilege.do"><bean:message key="user.privilege"/></a></li>
				<li class="active">
					<logic:equal value="Add" property="task" name="privilegeForm">
						<bean:message key="tooltip.add"/> 
					</logic:equal>
					<logic:equal value="BackEdit" name="privilegeForm" property="task">
                         <bean:message key="tooltip.add"/>
                     </logic:equal>
					<logic:equal value="Edit" property="task" name="privilegeForm">
						<bean:message key="tooltip.edit"/> 
					</logic:equal>
				</li>
			</ol>
			<div class="row">
    		<html:form action="/appadmin/Privilege.do" method="POST" styleClass="form-horizontal form-groups-bordered">
	        <html:hidden property="task" value="Load"/>
		        <div class="col-xs-12 col-sm-12 col-md-12">
					<div class="panel panel-primary" data-collapsed="0">
							<div class="panel-heading" style="background:#00a651;">
								<div class="panel-title"><span style="color:#FFFFFF;" >
								<logic:equal value="Add" property="task" name="privilegeForm">
									<bean:message key="tooltip.add"/> 
								</logic:equal>
								<logic:equal value="Edit" property="task" name="privilegeForm">
									<bean:message key="tooltip.edit"/> 
								</logic:equal>
								<bean:message key="user"/></span></div>
								<div class="panel-options">
									<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
									<a href="javascript:movePage('Add');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
								</div>
							</div>
            				<div class="panel-body">
								<div class="form-group">
									<label for='appid' class="col-sm-3 control-label"><bean:message key="lookup.privilege.appname" /></label>
									<div class="col-sm-5">
										<logic:equal value="Add" property="task" name="privilegeForm">
										<html:select property="appid" styleClass="form-control">
			                                <logic:notEmpty name="listAppAdmin" scope="request">
			                                    <html:options collection="listAppAdmin" property="optKey" labelProperty="optLabel"/>
			                                </logic:notEmpty>
			                            </html:select>
							      		</logic:equal>
							      		 <logic:equal value="BackEdit" name="privilegeForm" property="task">
				                            <html:select property="appid" styleClass="form-control">
				                                <logic:notEmpty name="listAppAdmin" scope="request">
				                                    <html:options collection="listAppAdmin" property="optKey" labelProperty="optLabel"/>
				                                </logic:notEmpty>
				                            </html:select>
				                        </logic:equal>
				                        <logic:notEqual value="Add" name="privilegeForm" property="task">
				                            <logic:notEqual value="BackEdit" name="privilegeForm" property="task">
				                                <html:hidden name="privilegeForm" property="appid"/>
				                                <html:hidden name="privilegeForm" property="appname" write="true"/>
				                            </logic:notEqual>
				                        </logic:notEqual>
									</div>
								</div>
								<div class="form-group">
									<label for='groupid' class="col-sm-3 control-label"><bean:message key="lookup.privilege.groupid" /><font color="#FF0000">*)</font></label>
									<div class="col-sm-5">
										<logic:equal value="Add" name="privilegeForm" property="task">
				                            <html:text property="groupid" styleId='groupid' styleClass="form-control"  size="5" maxlength="5" />
				                            <font color="#FF0000"><html:errors property="groupid"/></font>
				                        </logic:equal>
				   						<logic:equal value="BackEdit" name="privilegeForm" property="task">
				                            <html:text property="groupid" styleClass="form-control" size="5" maxlength="5"/>
				                            <font color="#FF0000"><html:errors property="groupid"/></font>
				                        </logic:equal>
				                        <logic:notEqual value="Add" name="privilegeForm" property="task">
				                            <logic:notEqual value="BackEdit" name="privilegeForm" property="task">
				                                <html:hidden name="privilegeForm" property="groupid" write="true"/>
				                            </logic:notEqual>
				                        </logic:notEqual>
									</div>
								</div>
								 <div class="form-group">
									<label for='groupname' class="col-sm-3 control-label"><bean:message key="lookup.privilege.groupname" /><font color="#FF0000"> *)</font></label>
									<div class="col-sm-5">
										<html:text property="groupname" styleId='groupname' styleClass="form-control"  size="50" maxlength="50" />
				   						<font color="#FF0000"><html:errors property="groupname" /></font>
									</div>
								</div>
							<%--	<div class="form-group">
									<label for='groupname' class="col-sm-3 control-label"><bean:message key="app.privilege.form" /></label>
									<div class="col-sm-5">
										<html:checkbox styleClass="checkbox checkbox-replace color-green" property="template" value="1"/>
									</div>
								</div> --%>
								<% String action = "";%>
					      		<logic:equal value="Add" property="task" name="privilegeForm">
					      			<% action=Global.WEB_TASK_NEXT_ADD; %>
					      		</logic:equal>
					      		<logic:equal value="BackEdit" property="task" name="privilegeForm">
					      			<% action=Global.WEB_TASK_NEXT_ADD; %>
					      		</logic:equal>
					      		<logic:notEqual value="Add" name="privilegeForm" property="task">
		                            <logic:notEqual value="BackEdit" name="privilegeForm" property="task">
		                                <% action=Global.WEB_TASK_NEXT_EDIT; %>
		                            </logic:notEqual>
		                        </logic:notEqual>
					      		
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-5">
										<button type="button" onclick="javascript:movePage('<%=action%>');" title="<bean:message key="tooltip.berikutnya"/>" class="btn btn-blue"><bean:message key="tooltip.berikutnya"/></button>
										<button type="button" onclick="javascript:movePage('Load');" title="<bean:message key="tooltip.cancel"/>" class="btn btn-default"><bean:message key="tooltip.cancel"/></button>
									</div>
								</div>
							</div>
	           		</div>
	        	</div>
    		</html:form>
    		</div>
    	</div>
    </div>
    </body>
    <!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../assets/js/bootstrap.js"></script>
	<script src="../../assets/js/joinable.js"></script>
	<script src="../../assets/js/resizeable.js"></script>
	<script src="../../assets/js/neon-api.js"></script>
	<script src="../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../assets/js/jquery.validate.min.js"></script>
	<script src="../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../assets/js/jquery.multi-select.js"></script>
	<script src="../../assets/js/neon-chat.js"></script>
	<script src="../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../assets/js/select2/select2.min.js"></script>
	<script src="../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../assets/js/typeahead.min.js"></script>
	<script src="../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../assets/js/neon-demo.js"></script>
</html:html>