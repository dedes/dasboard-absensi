<%@page import="com.google.common.base.Strings" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tlds/birt.tld" prefix="birt" %>
<%
    String title = "AUDITTRAIL";
%>
<html:html>
    <html:base/>
    <head>
        <title><%=title%>
        </title>
    </head>
    <body>
    <bean:define id="formHeight" name="auditForm" property="height" type="java.lang.String"/>
    <bean:define id="formWidth" name="auditForm" property="width" type="java.lang.String"/>
    <bean:define id="tableId" name="auditForm" property="tableId" type="java.lang.String"/>
    <bean:define id="transactionDate" name="auditForm" property="transactionDate" type="java.lang.String"/>
    <bean:define id="transactionDateAfter" name="auditForm" property="transactionDateAfter" type="java.lang.String"/>
    <bean:define id="action" name="auditForm" property="action" type="java.lang.String"/>
    <bean:define id="active" name="auditForm" property="active" type="java.lang.String"/>
    <bean:define id="dateKey" name="auditForm" property="dateKey" type="java.lang.String"/>
    <%
        String height = getServletContext().getInitParameter("birt_viewer_height");
        String width = getServletContext().getInitParameter("birt_viewer_width");
        if (!Strings.isNullOrEmpty(formHeight) && formHeight.matches("[0-9.]*")) {
            height = formHeight;
        }
        if (!Strings.isNullOrEmpty(formWidth) && formWidth.matches("[0-9.]*")) {
            width = formWidth;
        }
    %>

    <center>
        <birt:viewer id="birtReport"
                     reportDesign="reportaudit.rptdesign" height="<%=height%>"
                     width="<%=width%>" frameborder="0" scrolling="no">
            <birt:param name="tableId" value="<%=tableId%>"></birt:param>
            <birt:param name="transactionDate" value="<%=transactionDate%>"></birt:param>
            <birt:param name="transactionDateAfter" value="<%=transactionDateAfter%>"></birt:param>
            <birt:param name="action" value="<%=action%>"></birt:param>
            <birt:param name="active" value="<%=active%>"></birt:param>
            <birt:param name="dateKey" value="<%=dateKey%>"></birt:param>
        </birt:viewer>
    </center>
    </body>
</html:html>