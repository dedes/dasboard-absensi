<%@page import="java.util.List" %>
<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String title = Global.TITLE_APP_USER;;
%>
<html:html>
<html:base />
<head>
<title><bean:message key="approval.privilege" /> | <bean:message
		key="header.tittle" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />

<link rel="icon"
	href="<%=request.getContextPath()%>/assets/images/favicon.ico">

<!-- 	<link href="../../../style/style.css" rel="stylesheet" type="text/css"> -->
<script language="javascript" SRC="../../../javascript/global/global.js"></script>
<script language="javascript"
	SRC="../../../javascript/global/disable.js"></script>
<script language="javascript"
	SRC="../../../javascript/global/js_validator.js"></script>

<script language="javascript"
	SRC="../../../javascript/global/jquery-1.10.2.js"></script>
<script language="javascript"
	SRC="../../../javascript/global/jquery-all.js"></script>

<link rel="stylesheet"
	href="../../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet"
	href="../../../assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet"
	href="../../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
<link rel="stylesheet" href="../../../assets/css/bootstrap.css">
<link rel="stylesheet" href="../../../assets/css/neon-core.css">
<link rel="stylesheet" href="../../../assets/css/neon-theme.css">
<link rel="stylesheet" href="../../../assets/css/neon-forms.css">
<link rel="stylesheet" href="../../../assets/css/custom.css">
<link rel="stylesheet"
	href="../../../assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="../../../assets/js/select2/select2.css">
<link rel="stylesheet" href="../../../assets/css/skins/green.css">

<script src="../../../assets/js/jquery-1.11.3.min.js"></script>

<!--[if lt IE 9]><script src="../../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

<script language="javascript">
	<logic:present name="list">
		function checkedAll() {
		    <% int size = ((List)request.getAttribute("list")).size(); %>
		    var sizeList = <%=size%>;
		    var member = document.getElementsByName("search.memberList");
		    if (sizeList > 1) {
		        for (i = 0; i < member.length; i++)
		            with (member[i])
		                checked = document.approvalPrivilegeForm.checkAll.checked;
		    } else {
		        with (member[0])
		            checked = document.approvalPrivilegeForm.checkAll.checked;
		    }
		}
	</logic:present>

	function action(task, idParam) {
		if (task == "Reject" || task == "Approve") {
			if (!beforeSubmit(task))
				return;
		} else if (task == "Single") {
			document.approvalPrivilegeForm.id.value = idParam;
		}
		document.approvalPrivilegeForm.task.value = task;
		document.approvalPrivilegeForm.submit();
	}

	function beforeSubmit(task) {
		var count = 0;
		var ret = true;
		var member = document.getElementsByName("search.memberList");
		with (document.approvalPrivilegeForm) {
			if (member.length > 1) {
				for (var i = 0; i < member.length; i++) {
					if (member[i].checked) {
						count++;
						if (i == 0) {
							checkedMemberList.value = member[i].value;
						} else {
							checkedMemberList.value = checkedMemberList.value
									+ ',' + member[i].value;
						}
					}
				}
			} else if (member.length == 1) {
				if (member[0].checked) {
					count++;
					checkedMemberList.value = member.value;
				}
			}
			if (count == 0) {
				alert('<bean:message key="common.minimalHarusPilih" arg0="Data"/>');
				ret = false;
			}
			if (ret) {
				if (task == "Reject") {
					if (!confirm('<bean:message key="common.confirmreject"/>'))
						ret = false;
				} else if (task == "Approve") {
					if (!confirm('<bean:message key="common.confirmapprove"/>'))
						ret = false;
				}
			}
		}
		return ret;
	}

	function openDetail(id) {
		window
				.open(
						'<html:rewrite action="/lookup/approval/Privilege.do"/>?task=Load&groupid='
								+ id,
						'approvalPrivilegeForm',
						'height=650,width=800,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes,titlebar=no,location=no');
		return;
	}
</script>
<%@include file="../../../Error.jsp"%>
</head>
<body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
			<ol class="breadcrumb bc-2">
				<li><a href="javascript:void(0);"><i class="entypo-folder"></i>
					<bean:message key="approval.data" /></a></li>
				<li class="active"><a
					href="<%=request.getContextPath()%>/approval/Privilege.do"><bean:message
							key="approval.privilege" /></a></li>
			</ol>
			<div class="row">
				<html:form action="/approval/Privilege.do" method="POST">
					<html:hidden property="checkedMemberList" value="" />
					<html:hidden property="task" value="Load" />
					<html:hidden property="groupid" />
					<html:hidden property="paging.dispatch" value="Go" />
					<bean:define id="indexLastRow" name="approvalPrivilegeForm"
						property="paging.firstRecord" type="java.lang.Integer" />
					<bean:define id="indexFirstRow" name="approvalPrivilegeForm"
						property="paging.firstRecord" type="java.lang.Integer" />
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="panel panel-primary" data-collapsed="0">
							<div class="panel-heading" style="background: #00a651;">
								<div class="panel-title">
									<span style="color: #FFFFFF;"><bean:message
											key="approval.privilege" /></span>
								</div>
								<div class="panel-options">
									<a href="javascript:void(0);" data-rel="collapse"
										style="color: #FFFFFF;"><i class="entypo-down-open"></i></a> <a
										href="javascript:movePage('Load');" data-rel="reload"
										style="color: #FFFFFF;"><i class="entypo-arrows-ccw"></i></a>
								</div>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label for="appid" class=" control-label"><bean:message
															key="lookup.privilege.appname" /></label>
													<html:select property="search.appid"
														styleClass="form-control">
														<html:option value="">
															<bean:message key="common.all" />
														</html:option>
														<logic:notEmpty name="listAppAdmin" scope="request">
															<html:options collection="listAppAdmin" property="optKey"
																labelProperty="optLabel" />
														</logic:notEmpty>
													</html:select>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label for="groupid" class=" control-label"><bean:message
															key="lookup.privilege.groupname" /></label>
													<html:select property="search.groupid"
														styleClass="form-control">
														<html:option value="">
															<bean:message key="common.all" />
														</html:option>
														<logic:notEmpty name="listAppGroup" scope="request">
															<html:options collection="listAppGroup" property="optKey"
																labelProperty="optLabel" />
														</logic:notEmpty>
													</html:select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group">
													<label for="orderField" class=" control-label"><bean:message
															key="common.sort" /></label>
													<html:select property="search.orderField"
														styleClass="form-control">
														<html:option value="10"><bean:message key="combo.label.groupname"/></html:option>
							                            <html:option value="20"><bean:message key="combo.label.appname"/></html:option>
							                            <html:option value="30"><bean:message key="combo.label.oldmenu"/></html:option>
							                            <html:option value="40"><bean:message key="combo.label.newmenu"/></html:option>
							                            <html:option value="70"><bean:message key="combo.label.usract"/></html:option>
							                            <html:option value="50"><bean:message key="combo.label.editor"/></html:option>
							                            <html:option value="60"><bean:message key="combo.label.change.date"/></html:option>
													</html:select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group">
													<label for="orderby" class=" control-label"><bean:message
															key="common.order" /></label>
													<html:select property="search.orderby"
														styleClass="form-control">
														<html:option value="10"><bean:message key="common.ascending"/></html:option>
                           								<html:option value="20"><bean:message key="common.descending"/></html:option>
													</html:select>
												</div>
											</div>
											<div class="col-md-3 col-sm-3">
												<div class="form-group">
													<label style="color: transparent !important;">&nbsp;&nbsp;</label>
													<br />
													<button type="button"
														onclick="javascript:movePage('Search');"
														class="btn btn-blue"
														title="<bean:message key="tooltip.search"/>">
														<bean:message key="tooltip.search" />
													</button>
												</div>
											</div>
											<%-- <logic:notEmpty name="list" scope="request">
											<div class="col-md-1 col-sm-2">
												<div class="form-group">
													<label style="color:transparent!important;">&nbsp;&nbsp;</label> 
													<button type="button" onclick="javascript:openDownload('Download','scrollbars=no,width=300,height=100,resizable=no,top=250,left=550');" class="btn btn-blue"  title="<bean:message key="tooltip.download"/>"><bean:message key="tooltip.download"/></button>
												</div>
											</div>
					    				</logic:notEmpty> --%>
										</div>
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-md-12 col-xl-8">
										<table class="table responsive table-bordered">
											<thead>
												<tr align="center">
													<td width="10%" align="center"><bean:message key="lookup.privilege.appid"/></td>
								                    <td width="10%" align="center"><bean:message key="lookup.privilege.groupid"/></td>
								                    <td width="25%" align="center"><bean:message key="lookup.privilege.appname"/></td>
								                    <td width="25%" align="center"><bean:message key="lookup.privilege.groupname"/></td>
								                    <td width="10%" align="center"><bean:message key="lookup.privilege.oldmenu"/></td>
								                    <td width="10%" align="center"><bean:message key="lookup.privilege.newmenu"/></td>
								                    <td width="10%" align="center"><bean:message key="common.action"/></td>
								                    <td width="10%" align="center"><bean:message key="common.user"/></td>
								                    <td width="10%" align="center"><bean:message key="general.change.date"/></td>
								                    <td width="5%" align="center"><bean:message key="common.detail"/></td>
												</tr>
											</thead>
											<tbody>
												<logic:notEmpty name="list" scope="request">
													<logic:iterate id="listParam" name="list" scope="request" indexId="index">
							                        <%
							                            String color = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
							                        %>
							                        <tr class="<%=color%>" onmouseover="this.className='hovercol'"
							                            onmouseout="this.className='<%=color%>'">
															<td align="center">
								                                <html:multibox property="search.memberList"> <bean:write name="listParam" property="groupid"/></html:multibox>
								                            </td>
								                            <td align="left"><bean:write name="listParam" property="appid"/></td>
								                            <td align="left"><bean:write name="listParam" property="groupid"/></td>
								                            <td align="left"><bean:write name="listParam" property="appname"/></td>
								                            <td align="left"><bean:write name="listParam" property="groupname"/></td>
								                            <td align="center"><bean:write name="listParam" property="oldcount"/></td>
								                            <td align="center"><bean:write name="listParam" property="newcount"/></td>
								                            <td align="center"><bean:write name="listParam" property="usract"/></td>
								                            <td align="center"><bean:write name="listParam" property="usrupd"/></td>
								                            <td align="center"><bean:write name="listParam" property="dtmupd"/></td>
								                            <td align="center">
								                                <a href="javascript:openDetail('<bean:write name="listParam" property="groupid"/>');">
								                                    <img src="../../../images/icon/IconLookUp.gif" alt="detail"
								                                         title="<bean:message key="tooltip.detail"/>" width="16" height="16" border="0">
								                                </a>
								                            </td>
													</tr>
													</logic:iterate>
												</logic:notEmpty>
												<logic:empty name="list" scope="request">
								                    <tr class="evalcol">
								                        <td bgcolor="FFFFFF" colspan="11" align="center"><font class="errMsg"><bean:message
								                                key="common.listnotfound"/></font></td>
								                    </tr>
								                </logic:empty>
											</tbody>
										</table>
										
										<logic:notEmpty name="list" scope="request">
							                <br/>
							                <table width="95%" border="0" cellspacing="0" cellpadding="0">
							                    <tr>
							                        <td width="50%" height="30">&nbsp;</td>
							                        <td width="50%" align="right">
							                            <a href="javascript:action('Approve');">
							                                <img src="../../../images/button/ButtonApprove.png" alt="cancel"
							                                     title="<bean:message key="tooltip.button.app"/>" width="100" height="31"
							                                     border="0">
							                            </a>&nbsp;&nbsp;
							                            <a href="javascript:action('Reject');">
							                                <img src="../../../images/button/ButtonReject.png" alt="cancel"
							                                     title="<bean:message key="tooltip.button.reject"/>" width="100" height="31"
							                                     border="0">
							                            </a>
							                        </td>
							                    </tr>
							                </table>
							            </logic:notEmpty>
									</div>
								</div>
								<%-- <div>
									<logic:notEmpty name="list" scope="request">
										<div class="row">
											<div class=" col-md-5 ">
												<button type="button"
													onclick="javascript:moveToPageNumber(document.privilegeForm,'First')"
													class="btn btn-xs btn-default">
													<i class="fa fa-angle-double-left"></i>
												</button>

												<button type="button"
													onclick="javascript:moveToPageNumber(document.privilegeForm,'Prev')"
													class="btn btn-xs btn-default">
													<i class="fa fa-angle-left"></i>
												</button>

												<html:text name="privilegeForm"
													property="paging.currentPageNo" size="3" maxlength="6"
													styleClass="inpType " />
												<bean:message key="paging.of" />
												<html:hidden property="paging.totalPage" write="true" />

												<button type="button"
													onclick="javascript:moveToPageNumber(document.privilegeForm,'Go')"
													class="btn btn-xs btn-default">
													<i class="fa fa-search"></i>
												</button>

												<button type="button"
													onclick="javascript:moveToPageNumber(document.privilegeForm,'Next')"
													class="btn btn-xs btn-default">
													<i class="fa fa-angle-right"></i>
												</button>

												<button type="button"
													onclick="javascript:moveToPageNumber(document.privilegeForm,'Last')"
													class="btn btn-xs btn-default">
													<i class="fa fa-angle-double-right"></i>
												</button>
											</div>
											<div class="pull-right col-md-4">
												<label class="pull-right"><bean:message
														key="paging.showing.row" /> <%=indexFirstRow%> <bean:message
														key="paging.showing.to" /> <%=indexLastRow - 1%> <bean:message
														key="paging.showing.total" /> <html:hidden
														property="paging.totalRecord" write="true" /> <bean:message
														key="paging.showing.record" /></label>
											</div>
										</div>
									</logic:notEmpty>
								</div> --%>
								<%-- <br />
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<button type="button" onclick="javascript:movePage('Add');"
											class="btn btn-green"
											title="<bean:message key="tooltip.add"/>">
											<bean:message key="tooltip.add" />
										</button>
									</div>
								</div> --%>
							</div>
							<!--  END PANEL BODY -->
						</div>
					</div>
				</html:form>
			</div>
		</div>
	</div>
</body>
<!-- Imported styles on this page -->
<link rel="stylesheet"
	href="../../assets/js/selectboxit/jquery.selectBoxIt.css">
<link rel="stylesheet"
	href="../../assets/js/zurb-responsive-tables/responsive-tables.css">

<!-- Bottom scripts (common) -->
<script src="../../../assets/js/gsap/TweenMax.min.js"></script>
<script
	src="../../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="../../../assets/js/bootstrap.js"></script>
<script src="../../../assets/js/joinable.js"></script>
<script src="../../../assets/js/resizeable.js"></script>
<script src="../../../assets/js/neon-api.js"></script>
<script
	src="../../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

<!-- Imported scripts on this page -->
<script src="../../../assets/js/jquery.bootstrap.wizard.min.js"></script>
<script src="../../../assets/js/jquery.validate.min.js"></script>
<script src="../../../assets/js/jquery.inputmask.bundle.js"></script>
<script src="../../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script src="../../../assets/js/bootstrap-datepicker.js"></script>
<script src="../../../assets/js/bootstrap-switch.min.js"></script>
<script src="../../../assets/js/jquery.multi-select.js"></script>
<script src="../../../assets/js/neon-chat.js"></script>
<script
	src="../../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
<script src="../../../assets/js/select2/select2.min.js"></script>
<script src="../../../assets/js/bootstrap-tagsinput.min.js"></script>
<script src="../../../assets/js/typeahead.min.js"></script>
<script src="../../../assets/js/icheck/icheck.min.js"></script>


<!-- JavaScripts initializations and stuff -->
<script src="../../../assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="../../../assets/js/neon-demo.js"></script>
</html:html>