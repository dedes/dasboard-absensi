<%@page import="java.util.List" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "APPROVAL GENERAL PARAMETER"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <html:javascript formName="parameterForm" method="validateForm" staticJavascript="false" page="0"/>

        <script language="javascript">
            <logic:present name="list">
            function checkedAll() {
                <% int size = ((List)request.getAttribute("list")).size(); %>
                var sizeList = <%=size%>;
                var member = document.getElementsByName("search.memberList");
                if (sizeList > 1) {
                    for (i = 0; i < member.length; i++)
                        with (member[i])
                            checked = document.approvalParameterForm.checkAll.checked;
                } else {
                    with (member[0])
                        checked = document.approvalParameterForm.checkAll.checked;
                }
            }
            </logic:present>

            function movePage(task, idParam) {
                if (task == "Reject" || task == "Approve") {
                    if (!beforeSubmit(task))
                        return;
                } else if (task == "Single") {
                    document.approvalParameterForm.id.value = idParam;
                }
                document.approvalParameterForm.task.value = task;
                document.approvalParameterForm.submit();
            }

            function beforeSubmit(task) {
                var count = 0;
                var ret = true;
                var member = document.getElementsByName("search.memberList");
                with (document.approvalParameterForm) {
                    if (member.length > 1) {
                        for (var i = 0; i < member.length; i++) {
                            if (member[i].checked) {
                                count++;
                                if (i == 0) {
                                    checkedMemberList.value = member[i].value;
                                } else {
                                    checkedMemberList.value = checkedMemberList.value + ',' + member[i].value;
                                }
                            }
                        }
                    } else if (member.length == 1) {
                        if (member[0].checked) {
                            count++;
                            checkedMemberList.value = member.value;
                        }
                    }
                    if (count == 0) {
                        alert('<bean:message key="common.minimalHarusPilih" arg0="Data"/>');
                        ret = false;
                    }
                    if (ret) {
                        if (task == "Reject") {
                            if (!confirm('<bean:message key="common.confirmreject"/>'))
                                ret = false;
                        } else if (task == "Approve") {
                            if (!confirm('<bean:message key="common.confirmapprove"/>'))
                                ret = false;
                        }
                    }
                }
                return ret;
            }

            function openDetail(id) {
                window.open('<html:rewrite action="/lookup/approval/Parameter.do"/>?task=Load&id=' + id,
                    'approvalLookUpForm', 'height=650,width=800,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes,titlebar=no,location=no');
                return;
            }
        </script>

        <%@include file="../../../../Error.jsp" %>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/approval/Parameter.do" method="POST">
        <html:hidden property="checkedMemberList" value=""/>
        <html:hidden property="task" value="Load"/>
        <html:hidden property="id"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><%=title %>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.status"/></td>
                    <td class="Edit-Right-Text"><bean:message key="approval.needed"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.id"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.id" styleClass="TextBox" maxlength="10" size="10"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.data.type"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.data_type" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <html:option value="N"><bean:message key="combo.label.numeric"/></html:option>
                            <html:option value="C"><bean:message key="combo.label.alfanumerik"/></html:option>
                            <html:option value="D"><bean:message key="combo.label.date"/></html:option>
                            <html:option value="T1"><bean:message key="combo.label.time"/></html:option>
                            <html:option value="T2"><bean:message key="combo.label.datetime"/></html:option>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.sort"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.orderField" styleClass="TextBox">
                            <html:option value="10"><bean:message key="general.id"/></html:option>
                            <html:option value="20"><bean:message key="combo.label.keterangan"/></html:option>
                            <html:option value="30"><bean:message key="combo.label.tipedata"/></html:option>
                            <html:option value="40"><bean:message key="combo.label.nilai"/></html:option>
                            <html:option value="50"><bean:message key="combo.label.editor"/></html:option>
                            <html:option value="60"><bean:message key="combo.label.change.date"/></html:option>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.order"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.orderby" styleClass="TextBox">
                            <html:option value="10"><bean:message key="common.ascending"/></html:option>
                            <html:option value="20"><bean:message key="common.descending"/></html:option>
                        </html:select>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../../images/button/ButtonSearch.png" alt="cancel"
                                 title="<bean:message key="tooltip.cancel"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="3%" align="center">
                        <logic:notEmpty name="list" scope="request">
                            <input type="checkbox" value="ON" name="checkAll" onclick="javascript:checkedAll()">
                        </logic:notEmpty>
                    </td>
                    <td width="10%" align="center"><bean:message key="general.id"/></td>
                    <td width="25%" align="center"><bean:message key="general.desc"/></td>
                    <td width="10%" align="center"><bean:message key="general.data.type"/></td>
                    <td width="25%" align="center"><bean:message key="general.value"/></td>
                    <td width="10%" align="center"><bean:message key="common.user"/></td>
                    <td width="10%" align="center"><bean:message key="general.change.date"/></td>
                    <td width="5%" align="center"><bean:message key="common.detail"/></td>
                </tr>
                <logic:notEmpty name="list" scope="request">
                    <logic:iterate id="listParam" name="list" scope="request" indexId="index">
                        <%
                            String color = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                        %>
                        <tr class="<%=color%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=color%>'">
                            <td align="center">
                                <html:multibox property="search.memberList">
                                    <bean:write name="listParam" property="id"/>
                                </html:multibox>
                            </td>
                            <td align="left"><bean:write name="listParam" property="id"/></td>
                            <td align="left"><bean:write name="listParam" property="newparamdesc"/></td>
                            <td align="center">
                                <logic:equal name="listParam" property="newdata_type" value="N"><bean:message
                                        key="combo.label.numeric"/></logic:equal>
                                <logic:equal name="listParam" property="newdata_type" value="C"><bean:message
                                        key="combo.label.alfanumerik"/></logic:equal>
                                <logic:equal name="listParam" property="newdata_type" value="D"><bean:message
                                        key="combo.label.date"/></logic:equal>
                                <logic:equal name="listParam" property="newdata_type" value="T1"><bean:message
                                        key="combo.label.time"/></logic:equal>
                                <logic:equal name="listParam" property="newdata_type" value="T2"><bean:message
                                        key="combo.label.datetime"/></logic:equal>
                            </td>
                            <td align="left" style="word-break:break-all;"><bean:write name="listParam"
                                                                                       property="newval"/></td>
                            <td align="center"><bean:write name="listParam" property="usrupd"/></td>
                            <td align="center"><bean:write name="listParam" property="dtmupd"/></td>
                            <td align="center">
                                <a href="javascript:openDetail('<bean:write name="listParam" property="id"/>');">
                                    <img src="../../../images/icon/IconLookUp.gif" alt="detail"
                                         title="<bean:message key="tooltip.detail"/>" width="16" height="16" border="0">
                                </a>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="list" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="10" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
            <logic:notEmpty name="list" scope="request">
                <br/>
                <table width="95%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" height="30">&nbsp;</td>
                        <td width="50%" align="right">
                            <a href="javascript:movePage('Approve');">
                                <img src="../../../images/button/ButtonApprove.png" alt="cancel"
                                     title="<bean:message key="tooltip.button.app"/>" width="100" height="31"
                                     border="0">
                            </a>&nbsp;&nbsp;
                            <a href="javascript:movePage('Reject');">
                                <img src="../../../images/button/ButtonReject.png" alt="cancel"
                                     title="<bean:message key="tooltip.button.reject"/>" width="100" height="31"
                                     border="0">
                            </a>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
        </div>
    </html:form>
    </body>
</html:html>