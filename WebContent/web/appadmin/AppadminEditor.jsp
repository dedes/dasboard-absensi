<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.TITLE_APP_ADMIN; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>

        <script language="javascript">
            function movePage(task) {
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }

            function action(form, task, id) {
                if (!nullOrEmpty(id))
                    form.task.appid = id;
                form.task.value = task;
                form.submit();
            }

        </script>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form enctype="multipart/form-data" action="/appadmin/Application.do" method="POST">
        <html:hidden property="task"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="appadminForm" property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="lookup.privilege.appid"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appadminForm">
                            <html:text property="appid" styleClass="TextBox" size="10" maxlength="10"/>
                            <font color="#FF0000">*) <html:errors property="appid"/></font>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appadminForm">
                            <html:hidden property="appid" write="true"/>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="lookup.privilege.appname"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appadminForm">
                            <html:text property="appname" styleClass="TextBox" size="50" maxlength="50"/>
                            <font color="#FF0000">*) <html:errors property="appname"/></font>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appadminForm">
                            <html:text property="appname" styleClass="TextBox" size="50" maxlength="50"/>
                            <font color="#FF0000">*) <html:errors property="appname"/></font>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.administration.version"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appadminForm">
                            <html:text property="version" styleClass="TextBox" size="10" maxlength="10"/>
                            <font color="#FF0000">*) <html:errors property="version"/></font>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appadminForm">
                            <html:text property="version" styleClass="TextBox" size="10" maxlength="10"/>
                            <font color="#FF0000">*) <html:errors property="version"/></font>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.administration.desc"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appadminForm">
                            <html:textarea property="description" styleClass="TextBox" rows="4" cols="100"/>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appadminForm">
                            <html:textarea property="description" styleClass="TextBox" rows="4" cols="100"/>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.administration.icon"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appadminForm">
                            <html:file property="iconupload" styleId="iconupload" accept=".jpg;.png"/>
                            <html:hidden property="icon" write="false"/>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appadminForm">
                            <html:file property="iconupload" styleId="iconupload" accept=".jpg;.png"/>
                            <html:hidden property="icon" write="false"/>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.administration.sn"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appadminForm">
                            <html:text property="sn" styleClass="TextBox" size="10" maxlength="10"/>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appadminForm">
                            <html:text property="sn" styleClass="TextBox" size="10" maxlength="10"/>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.administration.virdir"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appadminForm">
                            <html:text property="virdir" styleClass="TextBox" size="10" maxlength="10"/>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appadminForm">
                            <html:text property="virdir" styleClass="TextBox" size="10" maxlength="10"/>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.administration.numberofserialdb"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appadminForm">
                            <html:text property="numberofserialdb" styleClass="TextBox" size="40" maxlength="38"/>
                            <font color="#FF0000"><html:errors property="numberofserialdb"/></font>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appadminForm">
                            <html:text property="numberofserialdb" styleClass="TextBox" size="40" maxlength="38"/>
                            <font color="#FF0000"><html:errors property="numberofserialdb"/></font>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.administration.captionforgroupdb"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="appadminForm">
                            <html:text property="captionforgroupdb" styleClass="TextBox" size="40" maxlength="38"/>
                            <font color="#FF0000"><html:errors property="captionforgroupdb"/></font>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appadminForm">
                            <html:text property="captionforgroupdb" styleClass="TextBox" size="40" maxlength="38"/>
                            <font color="#FF0000"><html:errors property="captionforgroupdb"/></font>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.active"/></td>
                    <td class="Edit-Right-Text">
                        <html:checkbox property="active" value="1"/>
                    </td>
                </tr>


            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <% String action = ""; %>
                        <logic:equal value="Add" property="task" name="appadminForm">
                            <% action = Global.WEB_TASK_INSERT; %>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="appadminForm">
                            <% action = Global.WEB_TASK_UPDATE; %>
                        </logic:equal>
                        <a href="javascript:action(document.appadminForm,'<%=action%>','');">
                            <img src="../../images/button/ButtonSubmit.png" alt="Action <%=action %>"
                                 title="<bean:message key="tooltip.action"/> <%=action %>" width="100" height="31"
                                 border="0">
                        </a>
                        &nbsp;
                        <a href="javascript:movePage('Load');">
                            <img src="../../images/button/ButtonCancel.png" alt="Cancel"
                                 title="<bean:message key="tooltip.cancel"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>