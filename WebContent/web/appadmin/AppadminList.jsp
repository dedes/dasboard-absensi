<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<% String title = Global.TITLE_APP_ADMIN; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title%>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function movePage(task) {
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }

            function action(form, task, id) {
                if (task == 'Delete') {
                    if (!confirm('<bean:message key="common.confirm.action"/>'))
                        return;
                    alert('<bean:message key="common.success"/>');
                }
                form.appid.value = id;
                form.task.value = task;
                form.submit();
            }

        </script>
    </HEAD>

    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/appadmin/Application.do" method="POST">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="appid"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <bean:define id="indexLastRow" name="appadminForm" property="paging.firstRecord" type="java.lang.Integer"/>
        <bean:define id="indexFirstRow" name="appadminForm" property="paging.firstRecord" type="java.lang.Integer"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="appadminForm" property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="lookup.privilege.appid"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.appid" styleClass="TextBox" size="15" maxlength="15"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="lookup.privilege.appname"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.appname" styleClass="TextBox" size="25" maxlength="25"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.active.header"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.active" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <html:option value="1"><bean:message key="common.active"/></html:option>
                            <html:option value="0"><bean:message key="common.nonactive"/></html:option>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.sort"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.orderField" styleClass="TextBox">
                            <html:option value="10"><bean:message key="combo.label.appid"/></html:option>
                            <html:option value="20"><bean:message key="combo.label.appname"/></html:option>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.order"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.orderby" styleClass="TextBox">
                            <html:option value="10"><bean:message key="common.ascending"/></html:option>
                            <html:option value="20"><bean:message key="common.descending"/></html:option>
                        </html:select>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../images/button/ButtonSearch.png" alt="Search" width="100" height="31"
                                 border="0" title="<bean:message key="tooltip.search"/>">
                        </a>
                    </td>
                </tr>
            </table>
            <div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;">
                <table border="0" width="100%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="10%" rowspan="2" align="center"><bean:message key="lookup.privilege.appid"/></td>
                        <td width="8%" rowspan="2" align="center"><bean:message key="lookup.privilege.appname"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="app.administration.version"/></td>
                        <td width="25%" rowspan="2" align="center"><bean:message key="app.administration.desc"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="app.administration.icon"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="app.administration.sn"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="app.administration.usrupd"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="app.administration.virdir"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message
                                key="app.administration.numberofserialdb"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message
                                key="app.administration.captionforgroupdb"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="app.administration.active"/></td>
                        <td width="8%" colspan="3" align="center"><bean:message key="common.action"/></td>
                    </tr>
                    <tr align="center" class="headercol">
                        <td width="5%" align="center"><bean:message key="common.edit"/></td>
                    </tr>
                    <logic:notEmpty name="list" scope="request">
                        <logic:iterate id="listParam" name="list" scope="request" indexId="index">
                            <%
                                String color = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                                indexLastRow++;
                            %>
                            <tr class="<%=color%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=color%>'">
                                <td align="center"><bean:write name="listParam" property="appid"/></td>
                                <td align="left"><bean:write name="listParam" property="appname"/></td>
                                <td align="center"><bean:write name="listParam" property="version"/></td>
                                <td align="left"><bean:write name="listParam" property="description"/></td>
                                <td align="left"><bean:write name="listParam" property="icon"/></td>
                                <td align="left"><bean:write name="listParam" property="sn"/></td>
                                <td align="center"><bean:write name="listParam" property="usrupd"/></td>
                                <td align="left"><bean:write name="listParam" property="virdir"/></td>
                                <td align="left"><bean:write name="listParam" property="numberofserialdb"/></td>
                                <td align="left"><bean:write name="listParam" property="captionforgroupdb"/></td>
                                <td align="center" valign="middle">
                                    <logic:equal value="1" name="listParam" property="active">
                                        <img src="../../images/icon/IconActive.gif" alt="Active" width="16" height="16"
                                             title="<bean:message key="tooltip.active"/>"/>
                                    </logic:equal>
                                    <logic:notEqual value="1" name="listParam" property="active">
                                        <img src="../../images/icon/IconInactive.gif" alt="Inactive" width="16"
                                             height="16" title="<bean:message key="tooltip.inactive"/>"/>
                                    </logic:notEqual>
                                </td>
                                <td align="center" valign="middle" title="<bean:message key="tooltip.edit"/>">
                                    <a href="javascript:action(document.appadminForm, 'Edit', '<bean:write name="listParam" property="appid"/>')">
                                        <img src="../../images/icon/IconEdit.gif" alt="Edit" width="16" height="16"/>
                                    </a>
                                </td>
                            </tr>
                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="list" scope="request">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="12" align="center"><font class="errMsg"><bean:message
                                    key="common.listnotfound"/></font></td>
                        </tr>
                    </logic:empty>
                </table>
            </div>
            <logic:notEmpty name="list" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center" title="<bean:message key="tooltip.first"/>">
                            <html:link href="javascript:moveToPageNumber(document.appadminForm,'First')">
                                <img src="../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center" title="<bean:message key="tooltip.prev"/>">
                            <html:link href="javascript:moveToPageNumber(document.appadminForm,'Prev')">
                                <img src="../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="appadminForm" property="paging.currentPageNo" size="3" maxlength="6"
                                       styleClass="inpType"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <a href="javascript:moveToPageNumber(document.appadminForm,'Go');">
                                <img src="../../images/gif/go.gif" width="20" height="20" align="absmiddle" border="0"/>
                            </a>
                        </td>
                        <td width="30" align="center" title="<bean:message key="tooltip.next"/>">
                            <html:link href="javascript:moveToPageNumber(document.appadminForm,'Next')">
                                <img src="../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center" title="<bean:message key="tooltip.last"/>">
                            <html:link href="javascript:moveToPageNumber(document.appadminForm,'Last')">
                                <img src="../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message
                                key="paging.showing.to"/> <%=indexLastRow - 1%> <bean:message
                                key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/>
                            <bean:message key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right" title="<bean:message key="tooltip.add"/>">
                        <a href="javascript:movePage('Add');">
                            <img src="../../images/button/ButtonAdd.png" alt="Add" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>