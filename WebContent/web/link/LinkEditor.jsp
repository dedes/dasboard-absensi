<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "PEMETAAN ALPRO"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
            <%-- <html:javascript formName="linkForm" method="validateForm"  staticJavascript="false" page="0"/> --%>

        <script language="javascript">
            function movePage(task, idParam) {
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }
        </script>

    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/feature/Link.do" method="POST">
        <html:hidden property="task" value="Load"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><%=title %>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.id"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal name="linkForm" property="task" value="Add">
                            <html:text property="id" styleClass="TextBox" maxlength="100" size="10"/>
                            <font color="#FF0000">*) <html:errors property="id"/></font>
                        </logic:equal>
                        <logic:notEqual name="linkForm" property="task" value="Add">
                            <html:hidden property="id" write="true"/>
                        </logic:notEqual>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.desc"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="paramdesc" styleClass="TextBox" maxlength="100" size="60"/>
                        <font color="#FF0000">*) <html:errors property="paramdesc"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.data.type"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="data_type" styleClass="TextBox">
                            <html:option value="C"><bean:message key="combo.label.alfanumerik"/></html:option>
                        </html:select>
                        <font color="#FF0000">*) <html:errors property="data_type"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.value"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="val" styleClass="TextBox" maxlength="150" size="40"/>
                        <font color="#FF0000">*) <html:errors property="val"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.visible"/></td>
                    <td class="Edit-Right-Text"><html:checkbox property="isvisible" value="1"
                                                               styleClass="CheckBox"/></td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <% String action = ""; %>
                        <logic:notEqual name="linkForm" property="task" value="Add">
                            <% action = Global.WEB_TASK_UPDATE; %>
                        </logic:notEqual>
                        <logic:equal name="linkForm" property="task" value="Add">
                            <% action = Global.WEB_TASK_INSERT; %>
                        </logic:equal>
                        <a href="javascript:movePage('<%=action%>');">
                            <img src="../../images/button/ButtonSubmit.png" alt="Action '<%=action%>'"
                                 title="<bean:message key="tooltip.action"/> <%=action%>" width="100" height="31"
                                 border="0"></a>

                        <a href="javascript:movePage('Load');">
                            <img src="../../images/button/ButtonCancel.png" alt="cancel"
                                 title="<bean:message key="tooltip.cancel"/>" width="100" height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>