<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.TITLE_APP_USER;; %>
<html:html>
<html:base/>
<head>
	<title><bean:message key="user"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
	
	<link rel="stylesheet" href="../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../assets/css/custom.css">
	<link rel="stylesheet" href="../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../assets/css/skins/green.css">

	<script src="../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
<script language="javascript">
	function movePage(task){
	   	document.forms[0].task.value = task;
	   	document.forms[0].submit();
	}

	function actionX(form, task, id){
		if(task == 'Reset'
			|| task == 'ReleaseDormant'
			|| task == 'ReleaseLock'
			|| task == 'Enable'
			|| task == 'Disable'
			|| task == 'Delete'){
			if (!confirm('<bean:message key="common.confirm.action"/>'))
				return;
			alert('<bean:message key="common.success"/>');
		}
		form.id.value = id;
		form.task.value = task;
		form.submit();
	}
	
	function actionEdit(form, task, id, approve, dormant){
		if(dormant != '1' ){
			if(approve == '1' || approve == '-1')
				actionX(form, task, id);
		}
	}
	
	function setData(map) {
		document.getElementById("area").value = map.cityId;
// 		document.getElementById("arealabel").value = map.city;
// 		document.forms[0].getElementById("area").value = map.cityId;
	}
	
// 	function openRegion(name, option) {
// 		var uri = '<html:rewrite action="/lookup/Region"/>';
// 		window.open(uri, name, option);
// 	}

	function openLu(name, option) {
		var uri = '<html:rewrite action="/lookup/City"/>';
		openLU(name, option, window, uri);
	}
	
	function openLuBranch(name,userid,branchid,option) {
// 		var branch = document.getElementById("member").value;
		var uri = '<html:rewrite action="/lookup/Branch"/>?userid='+userid+'&branchid='+branchid;
		openLU(name, option, window, uri);
	}
	function popUpClosed() {
	    window.location.reload();
	}
	function openLuPrivilege(name, option,member) {
// 		var member = document.getElementById("member").value;
		var uri = '<html:rewrite action="/lookup/PrivilegeView"/>?groupid='+member;
		openLU(name, option, window, uri);
	}
	
	 function openDownload(nama, option){
		    var uri = '<html:rewrite action="/servlet/reportListUID"/>';
		    window.open(uri, "_self", "scrollbars=no,width=300,height=100,resizable=no,top=250,left=550");
	   }
	 function movePage(task){
		   	if('Generate'==task){
		   		putForm(document.userForm);
			}
	       	document.forms[0].task.value = task;
	       	document.forms[0].submit();
	   }
	
</script>
</head>
<body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
				<ol class="breadcrumb bc-2">
					<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="aplikasi.management"/></a></li>
					<li class="active"><a href="<%=request.getContextPath()%>/appadmin/User.do"><bean:message key="user"/></a></li>
				</ol>
				<div class="row">
					<html:form action="/appadmin/User.do" method="POST">
					<html:hidden property="task" value="Load"/>
					<html:hidden property="userid" styleId="id"/>
				<%-- 		<html:hidden property="groupbranch" styleId="member"/> --%>
					<html:hidden property="paging.dispatch" value="Go"/>
					<bean:define id="indexLastRow" name="userForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<bean:define id="indexFirstRow" name="userForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="panel panel-primary" data-collapsed="0">
						<div class="panel-heading" style="background:#00a651;">
							<div  class="panel-title"><span style="color:#FFFFFF;" ><bean:message key="user"/></span></div>
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="javascript:movePage('Load');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label for="userid" class=" control-label"><bean:message key="app.user.id"/></label>
												<html:text property="search.userid" styleClass="form-control" styleId="userid" size="30" maxlength="30"/>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="fullname" class=" control-label"><bean:message key="app.user.name"/></label>
												<html:text property="search.fullname" styleClass="form-control" styleId="fullname" size="30" maxlength="30"/>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="islogin" class=" control-label"><bean:message key="common.login.header"/></label>
												<html:select property="search.islogin" styleId='islogin' styleClass="form-control">
										           	<html:option value=""><bean:message key="common.choose.one"/></html:option>
												   	<html:option value="1"><bean:message key="common.login"/></html:option>
												   	<html:option value="0"><bean:message key="common.notlogin"/></html:option>
											    </html:select>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="islocked" class=" control-label"><bean:message key="common.lock.header"/></label>
												<html:select property="search.islocked" styleId='islocked' styleClass="form-control">
													<html:option value=""><bean:message key="common.choose.one"/></html:option>
												   	<html:option value="1"><bean:message key="common.lock"/></html:option>
												   	<html:option value="0"><bean:message key="combo.label.unlock"/></html:option>
											    </html:select>
											</div>
										</div>
										
										<div class="col-md-2">
											<div class="form-group">
												<label style="color:transparent!important" for="search" ><bean:message key="tooltip.search"/><bean:message key="tooltip.search"/></label>
												<!-- <label style="color:transparent!important;"></label>  -->
												<button type="button" onclick="javascript:movePage('Search');" class="pull-right btn btn-blue"  title="<bean:message key="tooltip.search"/>"><bean:message key="tooltip.search"/></button>
											</div>
										</div>
										<%-- <logic:notEmpty name="list" scope="request">
											<div class="col-md-1">
												<div class="form-group">
													<label style="color:transparent!important" for="generate" ><bean:message key="tooltip.generate"/></label>
													<button type="button" onclick="javascript:openDownload('Download','scrollbars=no,width=300,height=100,resizable=no,top=250,left=550');" class="pull-right btn btn-blue"  title="<bean:message key="tooltip.generate"/>"><i class="entypo-download"></i></button>
												</div>
											</div>
					    				</logic:notEmpty> --%>	
									</div>
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="col-md-12 col-xl-8" >
									<table class="table responsive table-bordered"  >
										<thead >
											<tr align="center">
										      	<td><bean:message key="app.user.id"/></td>
										      	<td><bean:message key="app.user.name"/></td>
										      	<%-- <td><bean:message key="common.date.create"/></td>	
										      	<td><bean:message key="common.date.update"/></td> --%>
										      	<td><bean:message key="app.user.last.login"/></td>	
										      	<td><bean:message key="common.login.header"/></td>	
										      	<td><bean:message key="common.lock.header"/></td>
										      	<td><bean:message key="common.active.header"/></td>
										      	<td><bean:message key="common.approval"/></td>
										      	<td><bean:message key="common.action"/></td>
										    </tr>
										</thead>
										<tbody>
											<logic:notEmpty name="list" scope="request" >
												<logic:iterate id="listParam" name="list" scope="request" indexId="index">
													<%indexLastRow++;%>
													<tr>
													    <td align="left"><bean:write name="listParam" property="userid"/></td>
													    <td align="left"><bean:write name="listParam" property="fullname"/></td><%-- 
													    <td align="center"><bean:write name="listParam" property="dtmcrt"/></td>
													    <td align="center"><bean:write name="listParam" property="dtmupd"/></td> --%>
<%-- 													    <td align="center"><bean:write name="listParam" property="groupname"/></td>
 --%>													    <td align="center">
													    	<logic:notEmpty  name="listParam" property="lastlogin">
														    		<bean:write name="listParam" property="lastlogin"/>
														    </logic:notEmpty>
														    <logic:empty name="listParam" property="lastlogin">
														    		-
														    </logic:empty>
													    </td>
													    
													    <td align="center" valign="middle" >
															    	<logic:equal value="1" name="listParam" property="islogin">
															    		<img src="../../images/icon/IconYes.gif" alt="Login" width="16" height="16" title="<bean:message key="tooltip.login"/>"/>
															    	</logic:equal>
															    	<logic:notEqual value="1" name="listParam" property="islogin" >
															    		-
															    	</logic:notEqual>
														</td>	    
														
													    <td align="center" valign="middle">
													    	
														    	<logic:equal value="1" name="listParam" property="islocked">
														    		<img src="../../images/icon/IconLocked.gif" alt="Locked" width="16" height="16" title="<bean:message key="common.lock"/>"/>
														    	</logic:equal>
														    	<logic:notEqual value="1" name="listParam" property="islocked">
														    		-
														    	</logic:notEqual>
													    </td>
													    
													    <td align="center" valign="middle">
															    	<logic:equal value="0" name="listParam" property="isapproved">
															    		-
															    	</logic:equal>
															    	<logic:notEqual value="0" name="listParam" property="isapproved">
															    		<logic:equal value="1" name="listParam" property="active">
															    			<img src="../../images/icon/IconActive.gif" alt="Active" width="16" height="16" title="<bean:message key="tooltip.active"/>"/>
															    		</logic:equal>
																    	<logic:notEqual value="1" name="listParam" property="active">
																    		<img src="../../images/icon/IconInactive.gif" alt="Inactive" width="16" height="16" title="<bean:message key="tooltip.inactive"/>"/>
																    	</logic:notEqual>
															    	</logic:notEqual>
													    </td>
													    
													    <td align="center" valign="middle">
														    	<logic:equal value="1" name="listParam" property="isapproved">
														    		<img src="../../images/icon/ThumbsUp.png" alt="Approved" width="16" height="16" title="<bean:message key="tooltip.approve"/>"/>
														    	</logic:equal>
														    	<logic:equal value="0" name="listParam" property="isapproved">
														    		<img src="../../images/icon/imwaiting.gif" alt="Waiting Approval" width="16" height="16" title="<bean:message key="tooltip.notapprove"/>"/>
														    	</logic:equal>
														    	<logic:equal value="-1" name="listParam" property="isapproved">
														    		<img src="../../images/icon/ThumbsDown.png" alt="Reject" width="16" height="16" title="<bean:message key="tooltip.reject"/>"/>
														    	</logic:equal>
													    </td>
													    
												     	<td align="center">
													    	<logic:equal value="1" name="listParam" property="isapproved">
																<button type="button" onclick="javascript:actionX(document.userForm, 'Edit', '<bean:write name="listParam" property="userid"/>');" class="btn btn-gold btn-xs"  title="<bean:message key="tooltip.edit"/>"><i class="fa fa-pencil"></i></button>
																<button type="button" onclick="javascript:actionX(document.userForm,'Delete','<bean:write name="listParam" property="userid"/>');" class="btn btn-xs btn-red" title="<bean:message key="tooltip.delete"/>" ><i class="fa fa-trash"></i></button>
															</logic:equal>
															<logic:notEqual value="1" name="listParam" property="isapproved">
																-
															</logic:notEqual>
														</td>
						<%-- 							    <td align="center"><bean:write name="listParam" property="ipaddress"/></td> --%>
						
								      				</tr>
								      				
								   				</logic:iterate>
								  			</logic:notEmpty>
											<logic:empty name="list" scope="request">
											   <tr>
											      <td colspan="11" align="center" ><font class="errMsg"><bean:message key="common.listnotfound"/></font></td>
											   </tr>
								   			</logic:empty>
										</tbody>
									</table>
								</div>
							</div>
							<div>
							<logic:notEmpty name="list" scope="request">
								<div class="row">
					    			<div class=" col-md-5 "> 
							        	<button type="button" onclick="javascript:moveToPageNumber(document.userForm,'First')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-left"></i></button>
							        	
							        	<button type="button" onclick="javascript:moveToPageNumber(document.userForm,'Prev')" class="btn btn-xs btn-default"><i class="fa fa-angle-left"></i></button>
					    				
					    				<html:text name="userForm" property="paging.currentPageNo" size="3" maxlength="6" styleClass="inpType "/> <bean:message key="paging.of"/> <html:hidden property="paging.totalPage" write="true" />
							    		
							    		<button type="button" onclick="javascript:moveToPageNumber(document.userForm,'Go')" class="btn btn-xs btn-default"><i class="fa fa-search"></i></button>
							    		
							        	<button type="button" onclick="javascript:moveToPageNumber(document.userForm,'Next')" class="btn btn-xs btn-default"><i class="fa fa-angle-right"></i></button>
					    				
						        		<button type="button" onclick="javascript:moveToPageNumber(document.userForm,'Last')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-right"></i></button>
				    					
					    			</div>
					    			<div class="pull-right col-md-4">
				    					<label class="pull-right"><bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message key="paging.showing.to"/> <%=indexLastRow-1%> <bean:message key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/> <bean:message key="paging.showing.record"/></label>
					    			</div>
						      	</div> 	
					  		</logic:notEmpty> 
							</div>
							<br/>
							<%-- <div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<button type="button" onclick="javascript:movePage('Add');" class="btn btn-green"  title="<bean:message key="tooltip.add"/>"><bean:message key="tooltip.add"/></button>
								</div>
							</div> --%>
						</div> <!--  END PANEL BODY -->
					</div>
				</div>
			</html:form>
			</div>
		</div>	
	</div> 	  
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../assets/js/bootstrap.js"></script>
	<script src="../../assets/js/joinable.js"></script>
	<script src="../../assets/js/resizeable.js"></script>
	<script src="../../assets/js/neon-api.js"></script>
	<script src="../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../assets/js/jquery.validate.min.js"></script>
	<script src="../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../assets/js/jquery.multi-select.js"></script>
	<script src="../../assets/js/neon-chat.js"></script>
	<script src="../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../assets/js/select2/select2.min.js"></script>
	<script src="../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../assets/js/typeahead.min.js"></script>
	<script src="../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="<%=request.getContextPath()%>/assets/js/neon-demo.js"></script>
	
	
</html:html>

