<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = TreemasFormatter.toCamelCase(Global.TITLE_APP_USER); %>
<html:html>
<html:base/>
<head>
	<title><bean:message key="user"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
	<script language="javascript" SRC="../../javascript/global/jquery-1.10.2.js"></script>
    <script language="javascript" SRC="../../javascript/global/jquery-all.js"></script>
	
	<link rel="stylesheet" href="../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../assets/css/custom.css">
	<link rel="stylesheet" href="../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../assets/css/skins/green.css">

	<script src="../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
<script language="javascript">

	$(function () {
	    var job_desc = $("#job_desc_id").val();
	    toggleJob(job_desc);
	});
	
	function toggleJob(job_desc) {
        var label = '';
        <logic:notEmpty name="listJobDesc" scope="request">
        <logic:iterate id="job_desc" name="listJobDesc" scope="request" indexId="idx">
        var job = '<bean:write name="job_desc" property="optKey"/>';
        var job_name = '<bean:write name="job_desc" property="optLabel"/>';
        if (job_desc == job) {
            label = job_name;
        }
        </logic:iterate>
        </logic:notEmpty>
        $("#labelJob").html(label);
        if (job_desc == "SCL" || job_desc == "PCL") {
            $("#email_error").show();
//		$("#phone_no_error").show();
//		$("#handphone_no_error").show();
            $("#handset_imei_error").show();
        } else {
            $("#email_error").hide();
//		$("#phone_no_error").hide();
//		$("#handphone_no_error").hide();
            $("#handset_imei_error").hide();
        }
    }
	
	function movePage(task){
	   	document.forms[0].task.value = task;
	   	document.forms[0].submit();
	}

	function actionX(form, task, id) {
        if (!nullOrEmpty(id))
            form.task.userid = id;
        form.task.value = task;
        form.submit();
    }

    function setData(map) {
        var members = map.member;
        var member = document.getElementById("member");
        member.value = members;
    }
 	
    function openLuCity(name, option) {
        var uri = '<html:rewrite action="/lookup/City"/>';
        openLU(name, option, window, uri);
    }

    function openLuPrivilege(name, option) {
        var member = document.getElementById("member").value;
        var uri = '<html:rewrite action="/lookup/Privilege"/>?groupid=' + member;
        openLU(name, option, window, uri);
    }

	

	/* $(document).ready(function(){
        toggleTest(document.getElementById("myCheck"));
	});

	function toggleTest(){
        var x = document.getElementById("myCheck").checked;
        var hidden = document.getElementById("hidden");
        
        if(x == true){
                hidden.style.display = 'inline';
        }else {
                hidden.style.display = 'none';
        }
	} */

</script>
</head>
<body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
			<ol class="breadcrumb bc-2">
				<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="aplikasi.management"/></a></li>
				<li><a href="<%=request.getContextPath()%>/appadmin/User.do"><bean:message key="user"/></a></li>
				<li class="active">
					<logic:equal value="Add" property="task" name="userForm">
						<bean:message key="tooltip.add"/> 
					</logic:equal>
					<logic:equal value="Edit" property="task" name="userForm">
						<bean:message key="tooltip.edit"/> 
					</logic:equal>
				</li>
			</ol>
			<div class="row">
				<html:form action="/appadmin/User.do" method="POST" styleClass="form-horizontal form-groups-bordered">
					<html:hidden property="task"/>
					<html:hidden property="groupPrivilege" styleId="member"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="panel panel-primary" data-collapsed="0">
							<div class="panel-heading" style="background:#00a651;">
								<div class="panel-title"><span style="color:#FFFFFF;" >
								<logic:equal value="Add" property="task" name="userForm">
									<bean:message key="tooltip.add"/> 
								</logic:equal>
								<logic:equal value="Edit" property="task" name="userForm">
									<bean:message key="tooltip.edit"/> 
								</logic:equal>
								<bean:message key="user"/></span></div>
								<div class="panel-options">
									<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
									<a href="javascript:movePage('Add');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
								</div>
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label for='userid' class="col-sm-3 control-label"><bean:message key="app.user.id" /><font color="#FF0000"> *)</font></label>
									<div class="col-sm-5">
										<logic:equal value="Add" property="task" name="userForm">
											<html:text property="userid" styleId='userid' styleClass="form-control"  size="30" maxlength="100" />
							      		</logic:equal>
							      		<logic:equal value="Edit" property="task" name="userForm">
							   				<html:text property="userid" styleId='userid' styleClass="form-control" readonly="true"  size="30" maxlength="100" />
							      		</logic:equal>
							      		<font color="#FF0000"><html:errors property="userid" /></font>
									</div>
								</div>
								<div class="form-group">
									<label for='fullname' class="col-sm-3 control-label"><bean:message key="app.user.name" /><font color="#FF0000"> *)</font></label>
									<div class="col-sm-5">
										<html:text property="fullname" styleId='fullname' styleClass="form-control"  size="50" maxlength="150" />
				   						<font color="#FF0000"><html:errors property="fullname" /></font>
									</div>
								</div>
								<%-- <div class="form-group">
									<label for='branchid' class="col-sm-3 control-label"><bean:message key="app.user.branch" /><font color="#FF0000"> *)</font></label>
									<div class="col-sm-5">
										<html:select property="branchid" styleClass="form-control">
				                            <logic:notEmpty name="listBranch" scope="request">
				                                <html:options collection="listBranch" property="optKey" labelProperty="optLabel"/>
				                            </logic:notEmpty>
				                        </html:select>
									</div>
								</div> --%>
								 <%-- <div class="form-group">
									<label for='job_desc' class="col-sm-3 control-label"><bean:message key="app.user.job.desc" /><font color="#FF0000"> *)</font></label>
									<div class="col-sm-5">
										<html:select property="job_desc" styleClass="form-control" styleId="job_desc_id">
				                            <logic:notEmpty name="listJobDesc" scope="request">
				                                <html:options collection="listJobDesc" property="optKey" labelProperty="optLabel"/>
				                            </logic:notEmpty>
				                        </html:select>
									</div>
								 </div>  --%>
								 <div class="form-group">
									<label class="col-sm-3 control-label"><bean:message key="common.privilege" /><font color="#FF0000"></font></label>
									<!-- <div class="col-sm-3">
		                       			<input type="text" class="form-control col-sm-2" size="4" maxlength="4" onkeyup="javascript:disableInput();"/>
									</div> -->
									<div class="col-sm-2">
										<button type="button" onclick="javascript:openLuPrivilege('Privilege','scrollbars=yes,width=750,height=500,resizable=yes,top=0,left=0');" class="btn btn-light"  title="<bean:message key="tooltip.detail"/>"><i class="fa fa-search"></i></button>
										<%-- <a href="javascript:openLuPrivilege('Privilege','scrollbars=yes,width=750,height=500,resizable=yes,top=0,left=0');">
				                            <img alt="Look Up Privilege" src="../../images/icon/IconLookUp.gif" width="16" height="16"
				                                 title="<bean:message key="tooltip.detail"/>"/>
			                        	</a> --%>
									</div>
								</div> 
								<logic:equal value="Add" property="task" name="userForm">
									<div class="form-group">
										<label for='sqlpassword' class="col-sm-3 control-label"><bean:message key="app.user.password"/><font color="#FF0000"> *)</font></label>
										<div class="col-sm-5">
											<html:password property="sqlpassword" styleClass="form-control" size="10" maxlength="70"/>
					   						<font color="#FF0000"><html:errors property="sqlpassword" /></font>
										</div>
									</div>
									<div class="form-group">
										<label for='sqlpasswordconfirm' class="col-sm-3 control-label"><bean:message key="app.user.confirm.password"/><font color="#FF0000"></font></label>
										<div class="col-sm-5">
									    	<html:password property="sqlpasswordconfirm" styleClass="form-control" size="10" maxlength="70"/>
											<font color="#FF0000"><html:errors property="sqlpasswordconfirm" /></font>
										</div>
									</div>
								</logic:equal>
								<%-- <div class="form-group">
									<label for='email' class="col-sm-3 control-label"><bean:message key="app.user.email" /><font color="#FF0000"></font></label>
									<div class="col-sm-5">
										<html:text property="email" styleId='email' styleClass="form-control"  size="50" maxlength="150" />
				   						<font color="#FF0000"><html:errors property="email" /></font>
									</div>
								</div>
								<div class="form-group">
									<label for='phone_no' class="col-sm-3 control-label"><bean:message key="app.user.phone" /><font color="#FF0000"></font></label>
									<div class="col-sm-5">
										<html:text property="phone_no" styleId='phone_no' styleClass="form-control"  size="50" maxlength="150" />
				   						<font color="#FF0000"><html:errors property="phone_no" /></font>
									</div>
								</div>
								<div class="form-group">
									<label for='handphone_no' class="col-sm-3 control-label"><bean:message key="app.user.handphone" /><font color="#FF0000"></font></label>
									<div class="col-sm-5">
										<html:text property="handphone_no" styleId='handphone_no' styleClass="form-control"  size="50" maxlength="150" />
				   						<font color="#FF0000"><html:errors property="handphone_no" /></font>
									</div>
								</div>
								 
								 <div class="form-group">
									<label for='handset_code' class="col-sm-3 control-label"><bean:message key="app.user.handset.imei" /></label>
									<div class="col-sm-5">
								       <html:text styleId="handset_code_id" property="handset_code" styleClass="form-control" size="20"
                                       maxlength="25"/>
                                       <font color="#FF0000"> <html:errors property="handset_code"/> </font>
                            	   </div>	
                            	 </div>	
								--%>
								
								<hr/>
								
									<div class="form-group">
										<label for='active' class="col-sm-3 control-label"><bean:message key="common.activate"/><font color="#FF0000"></font></label>
										<div class="col-sm-5">
											<html:checkbox styleClass="checkbox checkbox-replace color-green" property="active" value="1"/>
					   						<font color="#FF0000"><html:errors property="active" /></font>
										</div>
									</div>
									<div class="form-group">
										<label for='islocked' class="col-sm-3 control-label"><bean:message key="common.unlock"/><font color="#FF0000"></font></label>
										<div class="col-sm-5">
									    	<html:checkbox styleClass="checkbox checkbox-replace color-green" property="islocked" value="1"/>
											<font color="#FF0000"><html:errors property="islocked" /></font>
										</div>
									</div>
								
								<% String action = "";%>
					      		<logic:equal value="Add" property="task" name="userForm">
					      			<% action=Global.WEB_TASK_INSERT; %>
					      		</logic:equal>
					      		<logic:equal value="Edit" property="task" name="userForm">
					      			<% action=Global.WEB_TASK_UPDATE; %>
					      		</logic:equal>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-5">
										<button type="button" onclick="javascript:actionX(document.userForm,'<%=action%>','');" title="<bean:message key="tooltip.save"/>" class="btn btn-blue"><bean:message key="tooltip.save"/></button>
										<button type="button" onclick="javascript:movePage('Load');" title="<bean:message key="tooltip.cancel"/>" class="btn btn-default"><bean:message key="tooltip.cancel"/></button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</html:form>
			</div>
		</div>
	</div>
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../assets/js/bootstrap.js"></script>
	<script src="../../assets/js/joinable.js"></script>
	<script src="../../assets/js/resizeable.js"></script>
	<script src="../../assets/js/neon-api.js"></script>
	<script src="../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../assets/js/jquery.validate.min.js"></script>
	<script src="../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../assets/js/jquery.multi-select.js"></script>
	<script src="../../assets/js/neon-chat.js"></script>
	<script src="../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../assets/js/select2/select2.min.js"></script>
	<script src="../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../assets/js/typeahead.min.js"></script>
	<script src="../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../assets/js/neon-demo.js"></script>
</html:html>