<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.ABOUT_USER; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>

        <script type="text/javascript">
            function action(form, task, id) {
                if (!nullOrEmpty(id))
                    form.task.userid = id;
                form.task.value = task;
                form.submit();
            }
        </script>
        <logic:present scope="request" name="messagesave">
            <script language="javascript">
                alert('<bean:write name="messagesave" scope="request"/>');
            </script>
        </logic:present>
            <%-- <%@include file="../../../Error.jsp"%> --%>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/appadmin/Handset/lookup/lookUpHandset.do" method="POST">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="userid"/>
        <html:hidden property="gcmno"/>
        <html:hidden property="fullname"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="luManagementHandsetForm"
                                                                    property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.pass.old"/></td>
                    <td class="Edit-Right-Text">
                        <html:hidden property="password" write="true"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.administration.desc"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="description" styleClass="TextBox" size="40" maxlength="100"/>
                        <font color="#FF0000">*) <html:errors property="description"/></font>
                    </td>
                </tr>
            </table>
            <br/>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:action(document.luManagementHandsetForm,'Lock','');">
                            <img src="../../../images/button/ButtonSubmit.png" alt="Action Input" title="Action Input"
                                 width="100" height="31" border="0">
                        </a>
                    </td>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close();">
                            <img src="../../../images/button/ButtonClose.png" alt="cancel" title="Cancel" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>