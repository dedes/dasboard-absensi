<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.ABOUT_USER; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>

        <script type="text/javascript">
        </script>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/appadmin/Handset/lookup/lookUpHandset.do" method="POST">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="userid"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="luManagementHandsetForm"
                                                                    property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="7%" rowspan="2" align="center"><bean:message key="app.user.id"/></td>
                    <td width="15%" rowspan="2" align="center"><bean:message key="app.user.name"/></td>
                    <td width="30%" rowspan="2" align="center"><bean:message key="app.administration.desc"/></td>
                    <td width="7%" rowspan="2" align="center"><bean:message key="common.date"/></td>
                    <td width="7%" rowspan="2" align="center"><bean:message key="app.menu.user.update"/></td>
                    <td width="7%" rowspan="2" align="center"><bean:message key="app.pass.old"/></td>
                </tr>
                <tr></tr>
                <logic:notEmpty name="listDetail" scope="request">
                    <logic:iterate id="list" name="listDetail" scope="request" indexId="index">
                        <%
                            String color = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                        %>
                        <tr class="<%=color%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=color%>'">
                            <td align="left"><bean:write name="list" property="userid"/></td>
                            <td align="left"><bean:write name="list" property="fullname"/></td>
                            <td align="left"><bean:write name="list" property="description"/></td>
                            <td align="center"><bean:write name="list" property="dtmupd"/></td>
                            <td align="left"><bean:write name="list" property="usrupd"/></td>
                            <td align="left"><bean:write name="list" property="password"/></td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="listDetail" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="19" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
            <br/>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close();">
                            <img src="../../../images/button/ButtonClose.png" alt="cancel" title="Cancel" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>