<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "MASTER HANDSET"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>

        <script language="javascript">
            function movePage(task) {
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }

            function action(form, task, userid, gcmno) {
                if (task == 'Reset') {
                    if (!confirm('<bean:message key="common.confirm.action"/>'))
                        return;
                    alert('<bean:message key="common.success"/>');
                }
                form.userid.value = userid;
                form.gcmno.value = gcmno;
                form.task.value = task;
                form.submit();
            }

            function openLuDetailHistory(name, option, userid) {
                var uri = '<html:rewrite action="/appadmin/Handset/lookup/lookUpHandset"/>?task=Load&userid=' + userid;
                openLU(name, option, window, uri);
            }

            function openLuInputLock(name, option, userid, gcmno, fullname) {
                var uri = '<html:rewrite action="/appadmin/Handset/lookup/lookUpHandset"/>?task=Add&userid=' + userid + '&gcmno=' + gcmno + '&fullname=' + fullname;
                openLU(name, option, window, uri);
            }

        </script>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="appadmin/Handset.do" method="POST">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="userid"/>
        <html:hidden property="gcmno"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <bean:define id="indexLastRow" name="managementHandsetForm" property="paging.firstRecord"
                     type="java.lang.Integer"/>
        <bean:define id="indexFirstRow" name="managementHandsetForm" property="paging.firstRecord"
                     type="java.lang.Integer"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="managementHandsetForm" property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.id"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.userid" styleClass="TextBox" size="15" maxlength="15"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.name"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.fullname" styleClass="TextBox" size="30" maxlength="30"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.job.desc"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.job_desc" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="listJobDesc" scope="request">
                                <html:options collection="listJobDesc" property="optKey" labelProperty="optLabel"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.sort"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.orderField" styleClass="TextBox">
                            <html:option value="10"><bean:message key="combo.label.userid"/></html:option>
                            <html:option value="20"><bean:message key="combo.label.name"/></html:option>
                            <html:option value="30"><bean:message key="combo.label.branch"/></html:option>
                            <html:option value="40"><bean:message key="combo.label.jobdesc"/></html:option>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.order"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.orderby" styleClass="TextBox">
                            <html:option value="10"><bean:message key="common.ascending"/></html:option>
                            <html:option value="20"><bean:message key="common.descending"/></html:option>
                        </html:select>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../images/button/ButtonSearch.png" alt="Search"
                                 title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;">
                <table border="0" width="100%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="7%" rowspan="2" align="center"><bean:message key="app.user.id"/></td>
                        <td width="15%" rowspan="2" align="center"><bean:message key="app.user.name"/></td>
                        <td width="7%" rowspan="2" align="center"><bean:message key="app.user.branch"/></td>
                        <td width="7%" rowspan="2" align="center"><bean:message key="app.user.handphone"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="app.user.job.desc"/></td>
                        <td width="15%" colspan="4" align="center"><bean:message key="common.action"/></td>
                    </tr>
                    <tr align="center" class="headercol">
                        <td width="5%" align="center"><bean:message key="common.detail"/></td>
                        <td width="5%" align="center"><bean:message key="common.lock.header"/></td>
                        <td width="5%" align="center"><bean:message key="common.reset"/></td>
                    </tr>
                    <logic:notEmpty name="list" scope="request">
                        <logic:iterate id="listParam" name="list" scope="request" indexId="index">
                            <%
                                String color = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                                indexLastRow++;
                            %>
                            <tr class="<%=color%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=color%>'">
                                <td align="left"><bean:write name="listParam" property="userid"/></td>
                                <td align="left"><bean:write name="listParam" property="fullname"/></td>
                                <td align="left"><bean:write name="listParam" property="branchname"/></td>
                                <td align="center"><bean:write name="listParam" property="handphone_no"/></td>
                                <td align="left"><bean:write name="listParam" property="job_descName"/></td>
                                <td align="center">
                                    <a href="javascript:openLuDetailHistory('DetailHistory','scrollbars=yes,width=750,height=400,resizable=yes,top=0,left=0','<bean:write name="listParam" property="userid"/>');">
                                        <img alt="detail" src="../../images/icon/IconLookUp.gif" width="16" height="16"
                                             title="<bean:message key="tooltip.detail"/>"/>
                                    </a>
                                </td>
                                <logic:present scope="request" name="loginDesc">
                                    <logic:equal value="S" name="loginDesc" scope="request">
                                        <td align="center" valign="middle">
                                            <a href="javascript:openLuInputLock('InputLock','scrollbars=yes,width=500,height=200,resizable=yes,top=0,left=0','<bean:write name="listParam" property="userid"/>','<bean:write name="listParam" property="gcmno"/>','<bean:write name="listParam" property="fullname"/>');">
                                                <img src="../../images/icon/IconLocked.gif" alt="Disable" width="16"
                                                     height="16" title="<bean:message key="tooltip.lockhandset"/>"/>
                                            </a>
                                        </td>
                                        <td align="center" valign="middle">
                                            <a href="javascript:action(document.managementHandsetForm, 'Reset', '<bean:write name="listParam" property="userid"/>','<bean:write name="listParam" property="gcmno"/>')">
                                                <img src="../../images/icon/IconReset.gif" alt="Disable" width="16"
                                                     height="16" title="<bean:message key="tooltip.resethandset"/>"/>
                                            </a>
                                        </td>
                                    </logic:equal>
                                    <logic:notEqual value="S" name="loginDesc" scope="request">
                                        <logic:equal value="US" name="loginDesc" scope="request">
                                            <td align="center" valign="middle">
                                                <a href="javascript:openLuInputLock('InputLock','scrollbars=yes,width=500,height=200,resizable=yes,top=0,left=0','<bean:write name="listParam" property="userid"/>','<bean:write name="listParam" property="gcmno"/>','<bean:write name="listParam" property="fullname"/>');">
                                                    <img src="../../images/icon/IconLocked.gif" alt="Disable" width="16"
                                                         height="16" title="<bean:message key="tooltip.lockhandset"/>"/>
                                                </a>
                                            </td>
                                            <td align="center" valign="middle">
                                                <a href="javascript:action(document.managementHandsetForm, 'Reset', '<bean:write name="listParam" property="userid"/>','<bean:write name="listParam" property="gcmno"/>')">
                                                    <img src="../../images/icon/IconReset.gif" alt="Disable" width="16"
                                                         height="16"
                                                         title="<bean:message key="tooltip.resethandset"/>"/>
                                                </a>
                                            </td>
                                        </logic:equal>
                                        <logic:notEqual value="S" name="loginDesc" scope="request">
                                            <logic:notEqual value="US" name="loginDesc" scope="request">
                                                <td align="center" valign="middle">-</td>
                                                <td align="center" valign="middle">-</td>
                                            </logic:notEqual>
                                        </logic:notEqual>
                                    </logic:notEqual>
                                </logic:present>
                                <logic:notPresent name="loginDesc" scope="request">
                                    <td align="center" valign="middle">-</td>
                                    <td align="center" valign="middle">-</td>
                                </logic:notPresent>
                            </tr>
                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="list" scope="request">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="19" align="center"><font class="errMsg"><bean:message
                                    key="common.listnotfound"/></font></td>
                        </tr>
                    </logic:empty>
                </table>
            </div>
            <logic:notEmpty name="list" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center" title="<bean:message key="tooltip.first"/>">
                            <html:link href="javascript:moveToPageNumber(document.managementHandsetForm,'First')">
                                <img src="../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center" title="<bean:message key="tooltip.previous"/>">
                            <html:link href="javascript:moveToPageNumber(document.managementHandsetForm,'Prev')">
                                <img src="../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="managementHandsetForm" property="paging.currentPageNo" size="3"
                                       maxlength="6" styleClass="inpType"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <a href="javascript:moveToPageNumber(document.managementHandsetForm,'Go');">
                                <img src="../../images/gif/go.gif" width="20" height="20" align="absmiddle" border="0"/>
                            </a>
                        </td>
                        <td width="30" align="center" title="<bean:message key="tooltip.next"/>">
                            <html:link href="javascript:moveToPageNumber(document.managementHandsetForm,'Next')">
                                <img src="../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center" title="<bean:message key="tooltip.last"/>">
                            <html:link href="javascript:moveToPageNumber(document.managementHandsetForm,'Last')">
                                <img src="../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message
                                key="paging.showing.to"/> <%=indexLastRow - 1%> <bean:message
                                key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/>
                            <bean:message key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
        </div>
    </html:form>
    </body>
</html:html>