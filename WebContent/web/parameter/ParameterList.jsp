<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "GENERAL PARAMETER"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
        <html:javascript formName="parameterForm" method="validateForm" staticJavascript="false" page="0"/>

        <script language="javascript">
            function movePage(task, idParam) {
                document.forms[0].task.value = task;
                document.forms[0].id.value = idParam;
                if (task == "Search" && !validateForm(document.forms[0])) {
                    document.forms[0].searchString.value = "";
                    return;
                }

                document.forms[0].submit();
            }
        </script>

        <%@include file="../../../Error.jsp" %>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/appadmin/Parameter.do" method="POST">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="id"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><%=title %>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="10%" align="center"><bean:message key="general.id"/></td>
                    <td width="35%" align="center"><bean:message key="general.desc"/></td>
                    <td width="10%" align="center"><bean:message key="general.data.type"/></td>
                    <td width="35%" align="center"><bean:message key="general.value"/></td>
                    <td width="5%" align="center"><bean:message key="common.status"/></td>
                    <td width="5%" align="center"><bean:message key="common.edit"/></td>
                </tr>
                <logic:notEmpty name="list" scope="request">
                    <logic:iterate id="listParam" name="list" scope="request" indexId="index">
                        <%
                            String color = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                        %>
                        <tr class="<%=color%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=color%>'">
                            <td align="left"><bean:write name="listParam" property="id"/></td>
                            <td align="left"><bean:write name="listParam" property="paramdesc"/></td>
                            <td align="center">
                                <logic:equal name="listParam" property="data_type" value="N"><bean:message
                                        key="combo.label.numeric"/></logic:equal>
                                <logic:equal name="listParam" property="data_type" value="C"><bean:message
                                        key="combo.label.alfanumerik"/></logic:equal>
                                <logic:equal name="listParam" property="data_type" value="D"><bean:message
                                        key="combo.label.date"/></logic:equal>
                                <logic:equal name="listParam" property="data_type" value="T1"><bean:message
                                        key="combo.label.time"/></logic:equal>
                                <logic:equal name="listParam" property="data_type" value="T2"><bean:message
                                        key="combo.label.datetime"/></logic:equal>
                            </td>
                            <td align="left" style="word-break:break-all;"><bean:write name="listParam"
                                                                                       property="val"/></td>
                            <td align="center">
                                <logic:equal name="listParam" property="isApprove" value="0">
                                    <img src="../../images/icon/IconInactive.gif" width="16" height="13" border="0"
                                         title="<bean:message key="tooltip.inactive"/>">
                                </logic:equal>
                                <logic:notEqual name="listParam" property="isApprove" value="0">
                                    <img src="../../images/icon/IconActive.gif" width="16" height="13" border="0"
                                         title="<bean:message key="tooltip.active"/>">
                                </logic:notEqual>
                            </td>
                            <td align="center">
                                <logic:equal name="listParam" property="isApprove" value="0">
                                    -
                                </logic:equal>
                                <logic:notEqual name="listParam" property="isApprove" value="0">
                                    <a href="javascript:movePage('Edit','<bean:write name="listParam" property="id" />')">
                                        <img src="../../images/icon/IconEdit.gif" width="16" height="16" border="0"
                                             title="<bean:message key="tooltip.edit"/>">
                                    </a>
                                </logic:notEqual>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="list" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="10" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Add');">
                            <img src="../../images/button/ButtonAdd.png" alt="Add"
                                 title="<bean:message key="tooltip.add"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>