<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "AUDITTRAIL"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
        <html:javascript formName="parameterForm" method="validateForm" staticJavascript="false" page="0"/>

        <script language="javascript">
            $(function () {
                toggleTrx(document.getElementById("dateKey"));
            });
            function toggleTrx(element) {
                var val = element.value;
                var hiddenDate = document.getElementById("hiddenDate");
                var dateAfter = document.getElementById("dateAfter");
                var dateBefore = document.getElementById("dateBefore");
                if (val == '') {
                    dateBefore.disabled = true;
                } else {
                    dateBefore.disabled = false;
                    $("input[name*='search.transactionDate']").datepicker({
                        maxDate: "+0M +0D",
                        dateFormat: "ddmmyy",
                        changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        showOn: "both"
                    });
                    if (val == '40') {
                        hiddenDate.style.display = 'inline';
                    } else {
                        hiddenDate.style.display = 'none';
                        dateAfter.value = '';
                    }
                }
            }

            function movePage(task) {
                if ('Generate' == task) {
                    putForm(document.auditForm);
                }
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }
        </script>

        <%@include file="../../Error.jsp" %>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/audit/Auditrail.do" method="POST">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="param" name="auditForm"/>
        <html:hidden property="width" name="auditForm"/>
        <html:hidden property="height" name="auditForm"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <bean:define id="indexLastRow" name="auditForm" property="paging.firstRecord" type="java.lang.Integer"/>
        <bean:define id="indexFirstRow" name="auditForm" property="paging.firstRecord" type="java.lang.Integer"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><%=title + " " %><bean:write name="auditForm"
                                                                                     property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <logic:empty name="auditForm" property="param">
                    <tr>
                        <td class="Edit-Left-Text"><bean:message key="audit.table.name"/></td>
                        <td class="Edit-Right-Text">
                            <html:select property="search.tableId" styleClass="TextBox">
                                <html:option value=""><bean:message key="common.all"/></html:option>
                                <logic:notEmpty name="listTable" scope="request">
                                    <html:options collection="listTable" property="optKey" labelProperty="optLabel"/>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                    </tr>
                </logic:empty>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.action"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.action" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <html:option value="ADD"><bean:message key="combo.label.add"/></html:option>
                            <html:option value="EDIT"><bean:message key="combo.label.edit"/></html:option>
                            <html:option value="DELETE"><bean:message key="combo.label.delete"/></html:option>
                            <html:option value="APPROVE"><bean:message key="combo.label.approve"/></html:option>
                            <html:option value="REJECT"><bean:message key="combo.label.reject"/></html:option>
                            <logic:notEmpty name="auditForm" property="param">
                                <logic:equal value="ADTUSR" name="auditForm" property="param">
                                    <html:option value="RELEASE"><bean:message key="combo.label.release"/></html:option>
                                    <html:option value="UNLOCK"><bean:message key="combo.label.unlock"/></html:option>
                                    <html:option value="ACTIVATE"><bean:message
                                            key="combo.label.activate"/></html:option>
                                    <html:option value="DEACTIVATE"><bean:message
                                            key="combo.label.deactivate"/></html:option>
                                </logic:equal>
                            </logic:notEmpty>
                            <logic:empty name="auditForm" property="param">
                                <html:option value="RELEASE"><bean:message key="combo.label.release"/></html:option>
                                <html:option value="UNLOCK"><bean:message key="combo.label.unlock"/></html:option>
                                <html:option value="ACTIVATE"><bean:message key="combo.label.activate"/></html:option>
                                <html:option value="DEACTIVATE"><bean:message
                                        key="combo.label.deactivate"/></html:option>
                            </logic:empty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.transaction.date"/></td>
                    <td class="Edit-Right-Text">
                        <html:select styleId="dateKey" property="search.dateKey" onchange="javascript:toggleTrx(this);"
                                     styleClass="TextBox">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <html:option value="10"><bean:message key="combo.label.equal"/></html:option>
                            <html:option value="20"><bean:message key="combo.label.after"/></html:option>
                            <html:option value="30"><bean:message key="combo.label.before"/></html:option>
                            <html:option value="40"><bean:message key="combo.label.between"/></html:option>
                        </html:select>
                        &nbsp;
                        <html:text disabled="true" styleId="dateBefore" property="search.transactionDate"
                                   onkeydown="return disableInput();" size="8" maxlength="8" styleClass="TextBox"/>
                        &nbsp;
                        <div id="hiddenDate" style="display: none;">
                            <bean:message key="common.label.to"/>&nbsp;
                            <html:text styleId="dateAfter" property="search.transactionDateAfter"
                                       onkeydown="return disableInput();" size="8" maxlength="8" styleClass="TextBox"/>
                        </div>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <logic:notEmpty name="list" scope="request">
                            <a href="javascript:movePage('Generate');">
                                <img src="../../images/button/ButtonGenerate.png" alt="Search" title="Search"
                                     width="100" height="31" border="0">
                            </a>
                            &nbsp;
                        </logic:notEmpty>
                        <a href="javascript:movePage('Search');">
                            <img src="../../images/button/ButtonSearch.png" alt="Search" title="Search" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="10%" rowspan="2" align="center"><bean:message key="common.date"/></td>
                    <td width="8%" rowspan="2" align="center"><bean:message key="common.time"/></td>
                    <td width="10%" rowspan="2" align="center"><bean:message key="common.user"/></td>
                    <td width="7%" rowspan="2" align="center"><bean:message key="common.action"/></td>
                    <td width="22%" colspan="2" align="center"><bean:message key="audit.table"/></td>
                    <td width="60%" colspan="3" align="center"><bean:message key="audit.data"/></td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="12%" align="center"><bean:message key="common.name"/></td>
                    <td width="9%" align="center"><bean:message key="common.field"/></td>
                    <td width="20%" align="center"><bean:message key="common.before"/></td>
                    <td width="20%" align="center"><bean:message key="common.after"/></td>
                    <td width="20%" align="center"><bean:message key="audit.keyvalue"/></td>
                </tr>
                <logic:notEmpty name="list" scope="request">
                    <logic:iterate id="listParam" name="list" scope="request" indexId="index">
                        <%
                            String color = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                            indexLastRow++;
                        %>
                        <tr class="<%=color%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=color%>'">
                            <td align="center"><bean:write name="listParam" property="transactionDate"/></td>
                            <td align="center"><bean:write name="listParam" property="transactionTime"/></td>
                            <td align="left"><bean:write name="listParam" property="userId"/></td>
                            <td align="center"><bean:write name="listParam" property="action"/></td>
                            <td align="center"><bean:write name="listParam" property="tableName"/></td>
                            <td align="left"><bean:write name="listParam" property="fieldName"/></td>
                            <td align="left"><bean:write name="listParam" property="oldValue"/></td>
                            <td align="left"><bean:write name="listParam" property="newValue"/></td>
                            <td align="left">
                                <bean:write name="listParam" property="auditDesc"/>
                                <logic:equal value="ADTUSR" name="auditForm" property="param">
                                    &nbsp;<bean:write name="listParam" property="keyValue"/>
                                </logic:equal>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="list" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="9" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
            <logic:notEmpty name="list" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.auditForm,'First')" title="First">
                                <img src="../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.auditForm,'Prev')" title="Previous">
                                <img src="../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="auditForm" property="paging.currentPageNo" size="3" maxlength="6"
                                       styleClass="inpType"/>
                            &nbsp;<bean:message key="paging.of"/>&nbsp;<html:hidden property="paging.totalPage"
                                                                                    write="true"/>
                            <a href="javascript:moveToPageNumber(document.auditForm,'Go');">
                                <img src="../../images/gif/go.gif" width="20" height="20" align="absmiddle" border="0"/></a>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.auditForm,'Next')" title="Next">
                                <img src="../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.auditForm,'Last')" title="Last">
                                <img src="../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:message key="paging.showing.row"/>&nbsp;
                            <%= indexFirstRow %>
                            &nbsp;<bean:message key="paging.showing.to"/>&nbsp;<%=indexLastRow - 1%>
                            &nbsp;<bean:message key="paging.showing.total"/>&nbsp;
                            <html:hidden property="paging.totalRecord" write="true"/>&nbsp;<bean:message
                                key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
        </div>
    </html:form>
    </body>
</html:html>