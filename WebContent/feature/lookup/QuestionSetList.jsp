<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>

<html:html>
    <html:base/>

    <head>
        <title><bean:message key="feature.lookup.question.set"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function setDataForm(qg, q) {
                window.opener.setQuestion(qg, q);
                var lbl = document.getElementById(qg + q).innerHTML;
                var start = lbl.indexOf('.') + 1;
                window.opener.setQuestionLabel(trim(lbl.substring(start)));
                window.close();
            }
        </script>
    </head>

    <body>
    <html:form action="/feature/lookup/Questions.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="schemeId"/>

        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <bean:message key="feature.lookup.question.form"/>
                        <bean:write name="luQuestionSetForm" property="schemeId"/> - <bean:write
                            name="luQuestionSetForm" property="schemeDescription"/>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                <logic:notEmpty name="listQuestionSet" scope="request">
                    <logic:iterate id="data" name="listQuestionSet" scope="request">
                        <bean:define id="firstHeader" scope="request" value="0" type="java.lang.String"/>
                        <logic:iterate id="question" name="data" indexId="idx">
                            <% String classColor = (idx.intValue() % 2 == 0) ? "evalcol" : "oddcol"; %>
                            <logic:equal value="0" name="firstHeader">
                                <tr class="headercol">
                                    <td colspan="5">
                                        <bean:write name="question" property="questionGroupName"/>
                                    </td>
                                </tr>
                                <bean:define id="firstHeader" scope="request" value="1" type="java.lang.String"/>
                            </logic:equal>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td width="70%"
                                    id="<bean:write name="question" property="questionGroupId"/><bean:write name="question" property="questionId"/>"><%=idx.intValue() + 1%>
                                    . <bean:write name="question" property="questionLabel"/></td>
                                <td width="25%"><bean:write name="question" property="answerType"/></td>
                                <td width="5%">
                                    <a href="javascript:setDataForm('<bean:write name="question" property="questionGroupId"/>', '<bean:write name="question" property="questionId"/>')">
                                        <img src="../../images/icon/IconSelect.gif" width="16" height="16" border="0"/>
                                    </a>
                                </td>
                            </tr>
                        </logic:iterate>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="listQuestionSet" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="3" align="center"><font class="errMsg">
                            <bean:message key="common.listnotfound"/></font>
                        </td>
                    </tr>
                </logic:empty>
            </table>
            <table width="95%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%" height="30"></td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close();">
                            <html:img src="../../images/button/ButtonClose.png" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>
