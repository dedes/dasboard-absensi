<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title><bean:message key="common.lookup"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
        <script language="javascript" type="text/JavaScript">
            function movePage(taskID) {
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

            function setDataForm(luCode, luDesc) {
                var nameParam = findElement(document.forms[0], "textFieldLookup").value;
                window.opener.setLookupDinamic(luCode, luDesc, nameParam);
                window.close();
            }
        </script>
    </head>
    <body>
    <html:form action="/feature/lookup/LookUpDinamic.do" method="post">
        <%@include file="../../../Error.jsp" %>
        <html:hidden property="task" value="Load"/>
        <html:hidden property="lookupCode"/>
        <html:hidden property="nameLookup"/>
        <html:hidden property="textFieldLookup"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="search.searchType1" value="10"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="luDinamicForm" property="nameLookup"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.filtering"/>
                        <logic:notEmpty name="listCriteria" scope="request">
                            <html:select property="search.lookupField">
                                <html:options collection="listCriteria" property="lookupField"
                                              labelProperty="lookupDisplayName"/>
                            </html:select>
                            <html:text property="search.searchValue1" size="25" maxlength="30" styleClass="TextBox"/>
                        </logic:notEmpty>
                    </td>
                    <td class="Edit-Right-Text">
                        <html:text property="searchValue1" styleClass="TextBox" size="25" maxlength="30"/>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../images/button/ButtonSearch.png" alt="Search"
                                 title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <logic:notEqual name="luDinamicForm" property="task" value="Show">
                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr class="Top-Table-Header">
                        <td class="Top-Table-Header-Left"></td>
                        <td class="Top-Table-Header-Center"><bean:write name="luDinamicForm" property="nameLookup"/>
                            <bean:message key="common.list"/></td>
                        <td class="Top-Table-Header-Right"></td>
                    </tr>
                </table>
                <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="20%" rowspan="2"><bean:message key="feature.code.dinamic.list"/></td>
                        <td width="70%" rowspan="2"><bean:message key="general.desc"/></td>
                        <td width="5%" colspan="1"><bean:message key="common.action"/></td>
                    </tr>
                    <tr align="center" class="headercol">
                        <td width="5%"><bean:message key="common.select"/></td>
                    </tr>
                    <logic:notEmpty name="listLookupResult" scope="request">
                        <logic:iterate id="data" name="listLookupResult" scope="request" indexId="idx">
                            <%
                                String classColor = (idx.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                            %>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td><bean:write name="data" property="kodeDisplayLookup"/></td>
                                <td><bean:write name="data" property="descriptionLookup"/></td>
                                <td align="center">
                                    <a href="javascript:setDataForm('<bean:write name="data" property="kodeDisplayLookup"/>','<bean:write name ="data" property="descriptionLookup"/>')">
                                        <img alt="Select" src="../../../images/icon/IconSelect.gif" width="16"
                                             height="16" border="0">
                                    </a>
                                </td>
                            </tr>
                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listLookupResult">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="3" align="center"><font class="errMsg"><bean:message
                                    key="common.listnotfound"/></font></td>
                        </tr>
                    </logic:empty>
                </table>
                <logic:notEmpty name="listLookupResult">
                    <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.luDinamicForm,'First')"
                                           title="First">
                                    <img src="../../images/gif/first.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.luDinamicForm,'Prev')"
                                           title="Previous">
                                    <img src="../../images/gif/prev.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="200" align="center"><bean:message key="paging.page"/>
                                <html:text name="luDinamicForm" property="paging.currentPageNo" size="3" maxlength="6"
                                           styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                        property="paging.totalPage" write="true"/>
                                <html:hidden property="paging.totalRecord" write="false"/>
                                <a href="javascript:moveToPageNumber(document.luDinamicForm,'Go');">
                                    <img src="../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                         border="0"/></a></td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.luDinamicForm,'Next')"
                                           title="Next">
                                    <img src="../../images/gif/next.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.luDinamicForm,'Last')"
                                           title="Last">
                                    <img src="../../images/gif/last.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td align="right">
                                <bean:define id="currentPage" name="luDinamicForm" property="paging.currentPageNo"
                                             type="java.lang.Integer"/>
                                <bean:define id="total" name="luDinamicForm" property="paging.totalRecord"
                                             type="java.lang.Integer"/>
                                <bean:define id="perpage" name="luDinamicForm" property="paging.rowPerPage"
                                             type="java.lang.Integer"/>
                                <%
                                    int startShow = 1;
                                    int endShow = 0;
                                    if (currentPage > 1) {
                                        int tempstart = ((currentPage - 1) * perpage) + 1;
                                        int tempend = currentPage * perpage;
                                        if (total < tempend) {
                                            tempend = total;
                                        }
                                        startShow = tempstart;
                                        endShow = tempend;
                                    } else {
                                        endShow = total;
                                    }
                                %>
                                <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                    key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                                <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                    key="paging.showing.record"/>
                            </td>
                        </tr>
                    </table>
                </logic:notEmpty>
            </logic:notEqual>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close()"><img src="../../images/button/ButtonClose.png" width="100"
                                                                 height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>