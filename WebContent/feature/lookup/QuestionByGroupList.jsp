<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title><<bean:message key="feature.question.by.group.lookup"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
        <script language="javascript" type="text/JavaScript">
            function setQuestionGroup(kode, nama) {
                findElement(document.forms[0], "questionGroupId").value = kode;
                findElement(document.forms[0], "questionGroupName").value = nama;
            }

            function openQuestionGroup(winName, features) {
                var toUrl = '<html:rewrite action="/feature/lookup/QuestionGroup"/>'
                window.open(toUrl, winName, features);
            }

            function movePage(taskID) {
                if (taskID = 'Search') {
                    if (findElement(document.forms[0], "questionGroupId").value == '') {
                        alert('<bean:message key="common.harusPilih" arg0="Question Group"/>');
                        return;
                    }
                }

                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

            function setDataForm(kode, tipe, label) {
// 			var lbl = trim(document.getElementById(kode).innerHTML);
// 			var start = lbl.indexOf('.') + 1;
// 			lbl = trim(lbl.substring(start));
                var qg = document.forms[0].questionGroupId.value;
                window.opener.setQuestion(qg, kode, label, tipe);
                window.close();
            }
        </script>
    </head>
    <body>
    <html:form action="/feature/lookup/QuestionByGroup.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="questionGroupId"/>
        <html:hidden property="questionId"/>
        <html:hidden property="excQuestionGroupId"/>
        <html:hidden property="excQuestionId"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.question.group.lookup"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.group"/></td>
                    <td class="Edit-Right-Text">
                        <html:text readonly="true" property="questionGroupName" styleClass="TextBox" size="30"/>
                        <font color="#FF0000">*) <html:errors property="questionGroupName"/></font>
                        <a href="javascript:openQuestionGroup('QuestionGroup','scrollbars=yes,width=640,height=480,resizable=yes,top=0,left=0')">
                            <img src="../../images/icon/IconLookUp.gif" alt="Pilih QuestionGroup" border="0"
                                 title="Lookup QuestionGroup" width="16" height="16">
                        </a>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../images/button/ButtonSearch.png" alt="Search"
                                 title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <logic:notEqual name="luQuestionByGroupForm" property="task" value="Show">
                <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="70%" rowspan="2"><bean:message key="feature.question.group.id"/></td>
                        <td width="20%" rowspan="2"><bean:message key="feature.question.group.name"/></td>
                        <td width="5%" colspan="1"><bean:message key="common.action"/></td>
                    </tr>
                    <tr align="center" class="headercol">
                        <td width="5%" align="center"><bean:message key="common.select"/></td>
                    </tr>
                    <logic:notEmpty name="listQuestion">
                        <logic:iterate id="data" name="listQuestion" scope="request" indexId="idx">
                            <%
                                String classColor = (idx.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                            %>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td id="<bean:write name="data" property="questionId"/><bean:write name ="data" property="answerTypeId"/>">
                                    <bean:write name="data" property="questionLabel"/></td>
                                <td><bean:write name="data" property="answerTypeName"/></td>
                                <td align="center">
                                    <a href="javascript:setDataForm('<bean:write name="data" property="questionId"/>','<bean:write name ="data" property="answerTypeId"/>','<bean:write name="data" property="questionLabel"/>')">
                                        <img alt="Select" src="../../images/icon/IconSelect.gif" width="16" height="16"
                                             border="0">
                                    </a>
                                </td>
                            </tr>
                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listQuestion">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="3" align="center"><font class="errMsg"><bean:message
                                    key="common.listnotfound"/></font></td>
                        </tr>
                    </logic:empty>
                </table>
            </logic:notEqual>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close()"><img src="../../images/button/ButtonClose.png" width="100"
                                                                 height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>