<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>

<html:html>
    <html:base/>
    <head>
        <title><bean:message key="feature.option.answer.lookup"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
        <script language="javascript" type="text/JavaScript">
            function movePage(taskID) {
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

            function setDataForm(qg, g, o) {
                var lbl = trim(document.getElementById(qg + g + o).innerHTML);
                var start = lbl.indexOf('.') + 1;
                lbl = trim(lbl.substring(start));
                window.opener.setOptionAnswer(o, lbl);
                window.close();
            }
        </script>
    </head>
    <body>
    <html:form action="/feature/lookup/OptionAnswer.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="questionGroupId"/>
        <html:hidden property="questionId"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.lookup.option.answer"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.group"/></td>
                    <td class="Edit-Right-Text"><bean:write name="luOptionAnswerForm"
                                                            property="questionGroupName"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question"/></td>
                    <td class="Edit-Right-Text"><bean:write name="luOptionAnswerForm" property="questionLabel"/></td>
                </tr>
            </table>

            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="95%" rowspan="2"><bean:message key="feature.label.option.answer"/></td>
                    <td width="5%" colspan="1"><bean:message key="common.action"/></td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="5%"><bean:message key="common.select"/></td>
                </tr>
                <logic:notEmpty name="listOptionAnswer">
                    <logic:iterate id="data" name="listOptionAnswer" scope="request" indexId="idx">
                        <%
                            String classColor = (idx.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                        %>
                        <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=classColor%>'">
                            <td id="<bean:write name="data" property="questionGroupId"/><bean:write name="data" property="questionId"/><bean:write name="data" property="optionAnswerId"/>">
                                <bean:write name="data" property="optionAnswerLabel"/></td>
                            <td align="center">
                                <a href="javascript:setDataForm('<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>','<bean:write name="data" property="optionAnswerId"/>')">
                                    <img alt="Select" src="../../images/icon/IconSelect.gif" width="16" height="16"
                                         border="0">
                                </a>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="listOptionAnswer">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="2" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close()"><img src="../../images/button/ButtonClose.png" width="100"
                                                                 height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>