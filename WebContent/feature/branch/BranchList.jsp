<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
    <html:base/>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title></title>
        <script language="javascript">
            var flag = 0;
            function actionDelete(branchId) {
                if (!confirm('<bean:message key="common.confirmdelete" />'))
                    return;
                document.forms[0].task.value = 'Delete';
                document.forms[0].branchId.value = branchId;
                document.forms[0].submit();
            }

            function movePage(taskID, branchId) {

                if (taskID == 'Sync' && flag == 1) {
                    alert('<bean:message key="errors.sync"/>');
                }
                else {
                    if (taskID == 'Sync') {
                        if (!confirm('<bean:message key="common.confirmwebservice" />'))
                            return;
                        flag = 1;
                    }
                    document.forms[0].task.value = taskID;
                    document.forms[0].branchId.value = branchId;
                    document.forms[0].submit();
                }
            }
        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
    </head>
    <body>
    <html:form method="post" action="/feature/branch/Branch.do">
        <html:hidden property="task"/>
        <html:hidden property="branchId"/>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.branch.list"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.filtering"/>
                        <html:select property="searchType">
                            <html:option value="10"><bean:message key="combo.label.branch.id"/></html:option>
                            <html:option value="20"><bean:message key="combo.label.branch.name"/></html:option>
                        </html:select>
                    </td>
                    <td class="Edit-Right-Text">
                        <html:text property="searchValue" styleClass="TextBox" size="25" maxlength="30"/>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../images/button/ButtonSearch.png" alt="Search"
                                 title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;">
                <table border="0" width="150%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="10%" rowspan="2" align="center"><bean:message key="feature.branch.id"/></td>
                        <td width="35%" rowspan="2" align="center"><bean:message key="feature.branch.name"/></td>
                        <td width="60%" rowspan="2" align="center"><bean:message key="feature.branch.address"/></td>
                        <td width="10%" colspan="2" align="center"><bean:message key="common.action"/></td>
                    </tr>
                    <tr align="center" class="headercol">
                        <td width="5%" align="center"><bean:message key="common.edit"/></td>
                        <td width="5%" align="center"><bean:message key="common.delete"/></td>
                    </tr>
                    <logic:notEmpty name="listBranch" scope="request">
                        <logic:iterate id="data" name="listBranch" scope="request" indexId="index">
                            <%
                                String classColor = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                            %>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td align="left">
                                    <bean:write name="data" property="indentation" filter="false"/>
                                    <bean:write name="data" property="branchId"/>
                                </td>
                                <td align="left"><bean:write name="data" property="branchName"/></td>
                                <td align="left"><bean:write name="data" property="branchAddress"/></td>
                                <td align="center">
                                    <a title="Edit"
                                       href="javascript:movePage('Edit','<bean:write name="data" property="branchId"/>');">
                                        <img src="../../images/icon/IconEdit.gif" alt="Edit" width="16" height="16"
                                             title="<bean:message key="tooltip.edit"/>"/>
                                    </a>
                                </td>
                                <td align="center">
                                    <a title="Delete"
                                       href="javascript:actionDelete('<bean:write name="data" property="branchId"/>');">
                                        <img src="../../images/icon/IconDelete.gif" alt="Delete" width="16" height="16"
                                             title="<bean:message key="tooltip.delete"/>"/>
                                    </a>
                                </td>
                            </tr>
                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listBranch" scope="request">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="5" align="center"><font
                                    class="errMsg"><bean:message key="common.listnotfound"/></font></td>
                        </tr>
                    </logic:empty>
                </table>
            </div>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Add');">
                            <img src="../../images/button/ButtonAdd.png" alt="Add"
                                 title="<bean:message key="tooltip.add"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>