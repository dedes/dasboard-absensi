<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.jsp"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="../../../javascript/global/jquery-1.4.2.min.js"></script>
        <title>Branch Geofence</title>
        <style type="text/css">
            body {
                margin: 10px 10px 40px 10px;
            }
        </style>
        <script language="javascript">
            var poly, map;
            var markers = [];
            var path = new google.maps.MVCArray;

            function loadBound() {
                var bound = null;
                <logic:notEmpty name="branchForm" property="bound">
                bound = new google.maps.LatLngBounds(
                    new google.maps.LatLng(<bean:write name="branchForm" property="bound.southBound"/>, <bean:write name="branchForm" property="bound.westBound"/>),
                    new google.maps.LatLng(<bean:write name="branchForm" property="bound.northBound"/>, <bean:write name="branchForm" property="bound.eastBound"/>)
                );
                </logic:notEmpty>
                return bound;
            }

            function addPoint(event) {
                path.insertAt(path.length, event.latLng);

                var marker = new google.maps.Marker({
                    position: event.latLng,
                    map: map,
                    draggable: true
                });
                markers.push(marker);
                marker.setTitle("#" + path.length);

                google.maps.event.addListener(marker, 'click', function () {
                        marker.setMap(null);
                        for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
                        markers.splice(i, 1);
                        path.removeAt(i);
                    }
                );

                google.maps.event.addListener(marker, 'dragend', function () {
                        for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
                        path.setAt(i, marker.getPosition());
                    }
                );
            }

            function addLoadedMarker(latLng) {
                path.insertAt(path.length, latLng);

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    draggable: true
                });
                markers.push(marker);
                marker.setTitle("#" + path.length);

                google.maps.event.addListener(marker, 'click', function () {
                        marker.setMap(null);
                        for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
                        markers.splice(i, 1);
                        path.removeAt(i);
                    }
                );

                google.maps.event.addListener(marker, 'dragend', function () {
                        for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
                        path.setAt(i, marker.getPosition());
                    }
                );
            }

            function loadPath() {
                <logic:notEmpty name="listPath" scope="request"><logic:iterate id="data" scope="request" name="listPath">
                var myLatLng = new google.maps.LatLng(<bean:write name="data" property="latitude"/>, <bean:write name="data" property="longitude"/>);
                addLoadedMarker(myLatLng);
                </logic:iterate></logic:notEmpty>
            }

            function initialize() {
                var myLatLng = new google.maps.LatLng(0, 115);
                myOptions = {
                    zoom: 5,
                    center: myLatLng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    navigationControl: true,
                    navigationControlOptions: {
                        style: google.maps.NavigationControlStyle.ZOOM_PAN,
                        position: google.maps.ControlPosition.RIGHT
                    },
                    scaleControl: true,
                    scaleControlOptions: {
                        position: google.maps.ControlPosition.BOTTOM_RIGHT
                    }
                };

                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                var bound = loadBound();
                if (bound != null)
                    map.fitBounds(bound);

                poly = new google.maps.Polygon({
                    strokeWeight: 2,
                    fillColor: '#5555FF'
                });
                poly.setMap(map);
                poly.setPaths(new google.maps.MVCArray([path]));

                loadPath();

                google.maps.event.addListener(map, 'click', addPoint);
            }

            function flyToPage(action) {
                if ('GeofenceSave' == action) {
                    var path = document.forms[0].path;
                    path.value = "";
                    if (markers.length > 0) {
                        $.each(markers, function (idx, obj) {
                            var lat = obj.getPosition().lat().toFixed(6);
                            var lng = obj.getPosition().lng().toFixed(6);
                            var cord = new String(lat).concat(",").concat(lng).concat(",").concat(idx + 1);
                            path.value = path.value + "|" + cord;
                        });
                        path.value = path.value.substring(1);
                    }
                }
                document.forms[0].task.value = action;
                document.forms[0].submit();
            }

            function clearMarker() {
                if (markers.length > 0) {
                    $.each(markers, function (idx, obj) {
                        obj.setMap(null);
                    });
                }
                markers = [];

                path.clear();
            }
        </script>
    </head>
    <body onload="initialize();">
    <html:form method="post" action="/feature/branch/Branch.do">
        <html:hidden property="path"/>
        <html:hidden property="task"/>
        <html:hidden property="searchType"/>
        <html:hidden property="searchValue"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <%@include file="../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.branch.geofence.title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text">
                        <html:hidden name="branchForm" property="branchId" write="true"/> - <bean:write
                            name="branchForm" property="branchName"/>
                    </td>
                </tr>
            </table>
            <div id="map_canvas" style="width:100%;height:90%;"></div>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:clearMarker();">
                            <img src="../../../images/button/ButtonClear.png" width="100" height="31" border="0"/>
                        </a>
                        <a href="javascript:flyToPage('GeofenceSave');">
                            <img src="../../../images/button/ButtonSubmit.png" width="100" height="31" border="0"/>
                        </a>
                        <a href="javascript:flyToPage('Load');">
                            <img src="../../../images/button/ButtonBack.png" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>