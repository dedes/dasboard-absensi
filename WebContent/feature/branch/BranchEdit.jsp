<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
    <html:base/>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title></title>
        <script language="javascript">
            function movePage(taskID) {
                if (taskID == "Save" || taskID == "Update") {
                    if (document.forms[0].onsubmit == null || document.forms[0].onsubmit()) {
                        document.forms[0].task.value = taskID;
                        document.forms[0].submit();
                    }
                }
                else if (taskID == "Load") {
                    document.forms[0].task.value = taskID;
                    document.forms[0].submit();
                }
            }

            function openWinCabang(branchId) {
                var tipe = 'EXC';

                <logic:equal name="branchForm" property="task" value="Add" scope="request">
                tipe = 'ALL';
                </logic:equal>

                window.open('<html:rewrite action="/feature/lookup/Branch"/>?tipe=' + tipe + '&branchId=' + branchId, 'popup',
                    'height=480,width=640,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=no,titlebar=no');
            }

            function setBranch(kode, nama) {
                findElement(document.forms[0], "parentId").value = kode;
                findElement(document.forms[0], "parentName").value = nama;
            }

            function clearParentBranch() {
                findElement(document.forms[0], "parentId").value = '';
                findElement(document.forms[0], "parentName").value = '';
            }
        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
            <%-- <logic:equal name="branchForm" property="task" value="Edit" scope="request"> --%>
            <%-- <html:javascript scriptLanguage="javascript" formName="branchForm" method="validateForm" staticJavascript="false" page="2"/> --%>
            <%-- </logic:equal> --%>
            <%-- <logic:equal name="branchForm" property="task" value="Add" scope="request" > --%>
            <%-- <html:javascript scriptLanguage="javascript" formName="branchForm" method="validateForm" staticJavascript="false" page="1"/> --%>
            <%-- </logic:equal> --%>
    </head>
    <!-- <body onload="initialize();"> -->
    <body>
    <html:form method="post" action="/feature/branch/Branch.do">
        <html:hidden property="task"/>
        <html:hidden property="searchType"/>
        <html:hidden property="searchValue"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <html:hidden property="latitude"/>
        <html:hidden property="longitude"/>
        <%-- 	<%@include file="../../../Error.jsp"%> --%>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <bean:message key="app.user.branch"/> -
                        <logic:equal name="branchForm" property="task" value="Edit" scope="request">
                            <bean:message key="common.edit"/>
                        </logic:equal>
                        <logic:equal name="branchForm" property="task" value="Add" scope="request">
                            <bean:message key="common.add"/>
                        </logic:equal>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>

            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.parent.branch"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="parentId" maxlength="10" size="10" styleClass="TextBox" readonly="true"/>
                        -
                        <html:text property="parentName" maxlength="50" size="30" styleClass="TextBox" readonly="true"/>
                        <a href="javascript:openWinCabang('<bean:write name="branchForm" property="branchId"/>');">
                            <html:img border="0" src="../../images/icon/IconLookUp.gif" width="15" height="15"
                                      title="Lookup branch"/>
                        </a>
                        <!-- 	                    		 |  -->
                        <a href="javascript:clearParentBranch();"><img src="../../images/icon/IconDelete.gif" width="16"
                                                                       height="16" title="Select"/></a>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.branch.id"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal name="branchForm" property="task" value="Edit" scope="request">
                            <html:hidden property="branchId" write="true"/>
                        </logic:equal>
                        <logic:equal name="branchForm" property="task" value="Add" scope="request">
                            <html:text property="branchId" maxlength="10" size="10" styleClass="TextBox"/> <font
                                color="#FF0000">*) <html:errors property="branchId"/></font>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.branch.name"/></td>
                    <td class="Edit-Right-Text"><html:text name="branchForm" property="branchName" size="50"
                                                           maxlength="50" styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="branchName"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.branch.address"/></td>
                    <td class="Edit-Right-Text"><html:text name="branchForm" property="branchAddress" size="50"
                                                           maxlength="50" styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="branchAddress"/></font>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <logic:equal name="branchForm" property="task" value="Edit" scope="request">
                            <a href="javascript:movePage('Update');">
                                <img src="../../images/button/ButtonSubmit.png"
                                     alt="Action <bean:write name="branchForm" property="task"/>"
                                     title="<bean:message key="tooltip.action"/> <bean:write name="branchForm" property="task"/>"
                                     width="100" height="31" border="0">
                            </a>
                        </logic:equal>
                        <logic:equal name="branchForm" property="task" value="Add" scope="request">
                            <a href="javascript:movePage('Save');">
                                <img src="../../images/button/ButtonSubmit.png"
                                     alt="Action <bean:write name="branchForm" property="task" />"
                                     title="<bean:message key="tooltip.action"/> <bean:write name="branchForm" property="task"/>"
                                     width="100" height="31" border="0">
                            </a>
                        </logic:equal>
                        <a href="javascript:movePage('Load');">
                            <img src="../../images/button/ButtonCancel.png" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>

</html:html>