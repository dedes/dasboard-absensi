<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<% String title = "PRIVILEGE FORM"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function checkedAll() {
                var form = document.forms[0];
                iLength = form.schemeList.length;
                if (iLength > 0) {
                    for (var i = 0; i < iLength; i++) {
                        form.schemeList[i].checked = form.chekAll.checked;
                    }
                }
                else
                    form.schemeList.checked = form.chekAll.checked;
            }

            function setDataForm(form) {
                var path;
                path = '<%=request.getContextPath()%>';
                form.task.value = 'Load';
                form.action = path + '/appadmin/Privilege.do';
                form.submit();
            }

            function movePage(form, task) {
                form.task.value = task;
                form.submit();
            }
        </script>
    </head>

    <body leftmargin="0" topmargin="10" marginwidth="0" marginheight="0">
    <html:form method="post" action="/feature/group/Template.do">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="appid"/>
        <html:hidden property="groupid"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <%=title%>
                        <html:hidden property="appname" write="true"/> - <html:hidden property="groupname"
                                                                                      write="true"/>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="10%" align="center">
                        <logic:notEmpty name="listTemplate" scope="request">
                            <input type="checkbox" value="ON" name="chekAll" onclick="javascript:checkedAll();"/>
                        </logic:notEmpty>
                    </td>
                    <td width="45%" align="center"><bean:message key="common.id"/></td>
                    <td width="45%" align="center"><bean:message key="general.desc"/></td>
                </tr>
                <logic:notEmpty name="listTemplate" scope="request">
                    <logic:iterate id="data" name="listTemplate" scope="request" indexId="i">
                        <%
                            String classColor = i % 2 == 0 ? "evalcol" : "oddcol";
                        %>
                        <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=classColor%>'">
                            <td align="center">
                                <html:multibox name="templateOfGroupForm" property="schemeList">
                                    <bean:write name="data" property="schemeId"/>
                                </html:multibox>
                            </td>
                            <td align="left"><bean:write name="data" property="schemeId"/></td>
                            <td align="left"><bean:write name="data" property="schemeDescription"/></td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="listTemplate" scope="request">
                    <tr bgcolor="#FFFFFF">
                        <td bgcolor="FFFFFF" colspan="3" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage(document.templateOfGroupForm,'Save')">
                            <html:img border="0" src="../../images/button/ButtonSubmit.png" width="100" height="31"/>
                        </a>
                        <a href="javascript:setDataForm(document.templateOfGroupForm);">
                            <html:img border="0" src="../../images/button/ButtonCancel.png" width="100" height="31"/>
                        </a></td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>
