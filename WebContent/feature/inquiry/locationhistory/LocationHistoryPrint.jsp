<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@page import="com.google.common.base.Strings" %>

<%@ taglib uri="/WEB-INF/tlds/birt.tld" prefix="birt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>
</head>
<body>
<%
    String survid = (String) request.getAttribute("name");
    String dateFrom = (String) request.getAttribute("dateFrom");
    String timeFrom = (String) request.getAttribute("timeFrom");
    String dateUntil = (String) request.getAttribute("dateUntil");
    String timeUntil = (String) request.getAttribute("timeUntil");
    String type = (String) request.getAttribute("type");
    String height = getServletContext().getInitParameter("birt_viewer_height");
    String width = getServletContext().getInitParameter("birt_viewer_width");
    String formHeight = (String) request.getAttribute("height");
    String formWidth = (String) request.getAttribute("width");
    if (!Strings.isNullOrEmpty(formHeight) && formHeight.matches("[0-9.]*")) {
        height = formHeight;
    }
    if (!Strings.isNullOrEmpty(formWidth) && formWidth.matches("[0-9.]*")) {
        width = formWidth;
    }
%>

<center>
    <birt:viewer id="birtReport"
                 reportDesign="Location_History_Report.rptdesign" height="<%=height%>"
                 width="<%=width%>" frameborder="0" scrolling="no">
        <birt:param name="survid" value="<%=survid%>"></birt:param>
        <birt:param name="dateFrom" value="<%=dateFrom%>"></birt:param>
        <birt:param name="timeFrom" value="<%=timeFrom%>"></birt:param>
        <birt:param name="dateUntil" value="<%=dateUntil%>"></birt:param>
        <birt:param name="timeUntil" value="<%=timeUntil%>"></birt:param>
        <birt:param name="type" value="<%=type%>"></birt:param>
    </birt:viewer>
</center>
</body>
</html>