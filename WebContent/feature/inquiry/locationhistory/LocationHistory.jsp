<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@page import="treemas.util.constant.PropertiesKey" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<% String key = ConfigurationProperties.getInstance().get(PropertiesKey.WEB_KEY); %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery.ui.all.css" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" src="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" src="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" src="../../../javascript/global/global.js"></script>
        <script language="javascript" src="../../../javascript/global/disable.js"></script>
        <script language="javascript" src="../../../javascript/global/js_validator.js"></script>
        <script language="javascript" src="../../../javascript/global/html2canvas.js"></script>
        <!-- 	<script language="javascript" SRC="../../../javascript/application/google.js"></script> -->
        <!-- 	<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script> -->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=<%=key%>&sensor=false"></script>
        <script language="javascript">
            function getFrameSize(frameID) {
                var result = {height: 0, width: 0};
                if (document.getElementById) {
                    var frame = parent.document.getElementById(frameID);
                    if (frame.scrollWidth) {
                        result.height = frame.scrollHeight;
                        result.width = frame.scrollWidth;
                    }
                }
                return result;
            }

            $(function () {
                $("input[name*='search.date']").datepicker({
                    maxDate: "+0M +0D",
                    dateFormat: "ddmmyy",
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    showOn: "both",
                    buttonImage: "../../../images/DatePicker.gif"
                });

                $("#btnSave").click(function () {

                    html2canvas($("#map_canvas"), {
                        useCORS: true,
                        onrendered: function (canvas) {
                            var dataUrel = canvas.toDataURL("image/png");
                            $("#img-out").html("");
                            $("#img-out").append('<img src="' + dataUrel + '"/>');

                            var canvas = document.getElementById("print");
                            var canvasinner = canvas.innerHTML;
                            var mywindow = window.open('', 'my div', 'height=400,width=600');
                            mywindow.document.write('<html><head><title>my div</title>');
                            mywindow.document.write('</head><body >');
                            mywindow.document.write(canvasinner);
                            mywindow.document.write('</body></html>');
                            mywindow.document.close();
                            mywindow.focus();
                            mywindow.print();
                            mywindow.close();
                        }

                    });
                });
            });

            function print() {
            }

            function movePage(taskID) {
                if ('Generate' == taskID) {
                    var result = getFrameSize('frame_main');
                    if (result.height > 0 && result.width > 0) {
                        document.forms[0].height.value = result.height - 50;
                        document.forms[0].width.value = result.width - 50;
                    }
                }

                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

            function setSurveyor(kode, nama) {
                findElement(document.forms[0], "search.surveyorId").value = kode;
                findElement(document.forms[0], "search.surveyorName").value = nama;
            }

            function openSurveyor(winName, features) {
                var toUrl = '<html:rewrite action="/feature/lookup/UserList"/>?tipe=CHILD&jobFilter=<bean:write name="picLocationForm" property="job" />';
                window.open(toUrl, winName, features);
            }

            function initialize() {
                <logic:empty name="listLocation" scope="request">
                $("#tablePrint").hide();
                </logic:empty>
                <logic:notEmpty name="listLocation" scope="request">
                $("#tablePrint").show();
                <%
                  StringBuffer absolutePath = new StringBuffer()
                      .append("http://")
                      .append(request.getServerName())
                      .append(":")
                      .append(request.getServerPort())
                      .append(request.getContextPath())
                      .append("/images/map/");
                %>

                var coordinateHistory = [
                    <logic:iterate id="data" indexId="idx" name="listLocation" scope="request">

                    <logic:greaterThan name="picLocationForm" property="listSize" value="<%=String.valueOf(idx.intValue() + 1)%>">
                    <logic:notEmpty name="data" property="latitude">
                    new google.maps.LatLng(<bean:write name="data" property="latitude"/>, <bean:write name="data" property="longitude"/>),
                    </logic:notEmpty>
                    </logic:greaterThan>

                    <logic:lessEqual name="picLocationForm" property="listSize" value="<%=String.valueOf(idx.intValue() + 1)%>">
                    <logic:notEmpty name="data" property="latitude">
                    new google.maps.LatLng(<bean:write name="data" property="latitude"/>, <bean:write name="data" property="longitude"/>)
                    </logic:notEmpty>
                    </logic:lessEqual>
                    </logic:iterate>
                ];

                var timestamp = [
                    <logic:iterate id="data" indexId="idx" name="listLocation" scope="request">
                    <logic:greaterThan name="picLocationForm" property="listSize" value="<%=String.valueOf(idx.intValue() + 1)%>">
                    <logic:notEmpty name="data" property="latitude">
                    '<bean:write name="data" property="timestamp"/>',
                    </logic:notEmpty>
                    </logic:greaterThan>

                    <logic:lessEqual name="picLocationForm" property="listSize" value="<%=String.valueOf(idx.intValue() + 1)%>">
                    <logic:notEmpty name="data" property="latitude">
                    '<bean:write name="data" property="timestamp"/>'
                    </logic:notEmpty>
                    </logic:lessEqual>
                    </logic:iterate>
                ];

                var flagArr = [
                    <logic:iterate id="data" indexId="idx" name="listLocation" scope="request">
                    <logic:greaterThan name="picLocationForm" property="listSize" value="<%=String.valueOf(idx.intValue() + 1)%>">
                    <logic:notEmpty name="data" property="latitude">
                    '<bean:write name="data" property="isTask"/>',
                    </logic:notEmpty>
                    </logic:greaterThan>

                    <logic:lessEqual name="picLocationForm" property="listSize" value="<%=String.valueOf(idx.intValue() + 1)%>">
                    <logic:notEmpty name="data" property="latitude">
                    '<bean:write name="data" property="isTask"/>'
                    </logic:notEmpty>
                    </logic:lessEqual>
                    </logic:iterate>
                ];

                var myLatLng = coordinateHistory[0];
                var myOptions = {
                    zoom: 12,
                    center: myLatLng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    navigationControl: true,
                    navigationControlOptions: {
                        style: google.maps.NavigationControlStyle.ZOOM_PAN,
                        position: google.maps.ControlPosition.RIGHT
                    },
                    scaleControl: true,
                    scaleControlOptions: {
                        position: google.maps.ControlPosition.BOTTOM_RIGHT
                    }
                };

                var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                var movementPath = new google.maps.Polyline({
                    path: coordinateHistory,
                    strokeColor: "#FF0000",
                    strokeOpacity: 0.5,
                    strokeWeight: 2
                });


                movementPath.setMap(map);

                var absPath = '<%=absolutePath.toString()%>';
                var markA = absPath + 'map-marker-a.png';
                var markZ = absPath + 'map-marker-z.png';
                var flagTask = absPath + 'map-black-flag.png';
                var flagTrack = absPath + 'map-red-flag.png';


                for (var i = 0; i < coordinateHistory.length; i++) {
                    if (i == 0)
                        createMarker(coordinateHistory[i], timestamp[i], map, markA);
                    else if (i == (coordinateHistory.length - 1))
                        createMarker(coordinateHistory[i], timestamp[i], map, markZ);
                    else {
                        if (flagArr[i] == '1')
                            createMarker(coordinateHistory[i], timestamp[i], map, flagTask);
                        else
                            createMarker(coordinateHistory[i], timestamp[i], map, flagTrack);
                    }
                }

                var bound = new google.maps.LatLngBounds(
                    new google.maps.LatLng(<bean:write name="picLocationForm" property="southBound"/>, <bean:write name="picLocationForm" property="westBound"/>),
                    new google.maps.LatLng(<bean:write name="picLocationForm" property="northBound"/>, <bean:write name="picLocationForm" property="eastBound"/>)
                );
                map.fitBounds(bound);
                </logic:notEmpty>
            }

            function createMarker(posisi, judul, peta, gambar) {
                var marker = new google.maps.Marker({
                    position: posisi,
                    title: judul,
                    map: peta,
                    icon: gambar
                });
            }
        </script>
        <logic:notEqual name="picLocationForm" property="task" value="Show">
        </logic:notEqual>

    </head>
    <body onload="initialize();">
    <html:form action="/feature/inquiry/PicLocation.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="job"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="height"/>
        <html:hidden property="width"/>
        <%-- <%@include file="../../../Error.jsp"%> --%>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <logic:equal name="picLocationForm" property="job" value="COLL"><bean:message
                                key="feature.location.history.collector"/></logic:equal>
                        <logic:notEqual name="picLocationForm" property="job" value="COLL"><bean:message
                                key="app.assginment.surveyorid"/></logic:notEqual>
                        <bean:message key="feature.location.history"/>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.location.history.pic"/></td>
                    <td class="Edit-Right-Text">
                        <html:hidden property="search.surveyorId"/>
                        <html:text property="search.surveyorName" size="30" readonly="true" styleClass="inpType"/>
                        <a href="javascript:openSurveyor('Surveyor','scrollbars=yes,width=640,height=480,resizable=yes,top=0,left=0')">
                            <img src="../../../images/icon/IconLookUp.gif" alt="Pilih Surveyor" width="16" height="16"
                                 border="0" title="Lookup Surveyor">
                        </a>
                        <font color="#FF0000">*)<html:errors property="search.surveyorName"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.location.history.start.periode"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.dateFrom" size="8" maxlength="8" styleClass="inpType"/>
                        <html:text property="search.timeFrom" size="4" maxlength="4" styleClass="inpType"
                                   title="format:hhmm"/>
                        <bean:message key="feature.location.history.format.hhmm"/><font color="#FF0000">*)<html:errors
                            property="search.dateFrom"/>&nbsp&nbsp<html:errors property="search.timeFrom"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.location.history.end.periode"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.dateUntil" size="8" maxlength="8" styleClass="inpType"/>
                        <html:text property="search.timeUntil" size="4" maxlength="4" styleClass="inpType"
                                   title="format:hhmm"/>
                        <bean:message key="feature.location.history.format.hhmm"/><font color="#FF0000">*)<html:errors
                            property="search.dateUntil"/>&nbsp&nbsp<html:errors property="search.timeUntil"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.location.history.tracking.source"/></td>
                    <td class="Edit-Right-Text">
                        <html:radio property="search.source" value="10"><bean:message
                                key="feature.location.history.gps"/></html:radio>&nbsp;&nbsp;<html:radio
                            property="search.source" value="20"><bean:message
                            key="feature.location.history.gps.lbs"/></html:radio>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search" width="100" height="31"
                                 border="0" title="<bean:message key="tooltip.search"/>">
                        </a>
                    </td>
                </tr>
            </table>
            <logic:notEqual name="picLocationForm" property="task" value="Show">
                <table width="95%" border="0" cellspacing="0" cellpadding="0" id="tablePrint">
                    <tr>
                        <td width="50%" height="30">&nbsp;</td>
                        <td width="50%" align="right">
                            <a id="btnGenerateXLS" href="javascript:movePage('Generate');">
                                <img src="../../../images/button/ButtonGenerate.png" width="100" height="31"
                                     border="0"/>
                            </a>
                            <a id="btnSave" href="javascript:print();" style="text-decoration:none;">
                                <img src="../../../images/button/ButtonPrintPDF.png" width="100" height="31"
                                     border="0"/>
                            </a>
                        </td>
                    </tr>
                </table>
                <div id="print" style="text-decoration:none;display:none;">
                    <bean:message key="feature.location.history.report.tracking"/> <bean:write name="picLocationForm"
                                                                                               property="search.surveyorName"/>_<bean:write
                        name="picLocationForm" property="search.dateFrom"/><bean:write name="picLocationForm"
                                                                                       property="search.timeFrom"/>-<bean:write
                        name="picLocationForm" property="search.dateUntil"/><bean:write name="picLocationForm"
                                                                                        property="search.timeUntil"/>
                    <div id="img-out"></div>
                </div>
                <div id="map_canvas" style="width:95%; height:90%"></div>
            </logic:notEqual>
        </div>
    </html:form>
    </body>
</html:html>