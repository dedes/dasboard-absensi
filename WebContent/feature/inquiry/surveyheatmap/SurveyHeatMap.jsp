<%@page import="treemas.util.constant.PropertiesKey" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<% String key = ConfigurationProperties.getInstance().get(PropertiesKey.WEB_KEY); %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery.ui.all.css" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" src="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" src="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" src="../../../javascript/global/global.js"></script>
        <script language="javascript" src="../../../javascript/global/disable.js"></script>
        <script language="javascript" src="../../../javascript/global/js_validator.js"></script>
        <!-- 	<script language="javascript" SRC="../../../javascript/application/google.js"></script> -->
        <!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=<%=key%>&sensor=false"></script>
        <script language="javascript" SRC="../../../javascript/application/swfobject.js"></script>
        <script language="javascript" SRC="../../../javascript/application/HM.js"></script>
        <script type="text/javascript">
            $(function () {
                $("input[name*='search.tgl']").datepicker({
                    maxDate: "+0M +0D",
                    dateFormat: "ddmmyy",
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    showOn: "both",
                    buttonImage: "../../../images/DatePicker.gif"
                });
            });

            var grad = [0, 167772262, 336396403, 504430711, 672727155, 857605496, 1025311865, 1193542778, 1361445755, 1529480062, 1714226559, 1882326399, 2050229378, 2218264197, 2386232710, 2571044231, 2739013001, 2906982028, 3075081868, 3243050383, 3427796369, 3595765395, 3763734164, 3931768213, 4099736983, 4284614554, 4284745369, 4284876441, 4285007513, 4285138585, 4285334937, 4285466009, 4285597081, 4285728153, 4285924505, 4286055577, 4286186649, 4286317721, 4286514073, 4286645145, 4286776217, 4286907289, 4287103641, 4287234713, 4287365785, 4287496857, 4287693209, 4287824281, 4287955353, 4288086425, 4288283033, 4288348568, 4288414103, 4288545431, 4288610966, 4288742293, 4288807829, 4288938900, 4289004691, 4289135763, 4289201554, 4289332625, 4289398161, 4289529488, 4289595024, 4289726351, 4289791886, 4289922958, 4289988749, 4290119820, 4290185612, 4290316683, 4290382218, 4290513546, 4290579081, 4290710409, 4290776198, 4290841987, 4290907777, 4290973822, 4291039612, 4291105401, 4291171447, 4291237236, 4291303026, 4291369071, 4291434861, 4291500650, 4291566696, 4291632485, 4291698275, 4291764320, 4291830110, 4291895899, 4291961945, 4292027734, 4292093524, 4292159569, 4292225359, 4292291148, 4292422730, 4292422983, 4292489029, 4292489282, 4292555328, 4292621118, 4292621627, 4292687417, 4292753462, 4292753972, 4292819762, 4292885807, 4292886061, 4292952106, 4292952360, 4293018406, 4293084195, 4293084705, 4293150750, 4293216540, 4293217050, 4293282839, 4293348885, 4293349138, 4293415184, 4293481230, 4293481485, 4293481996, 4293547788, 4293548299, 4293614091, 4293614602, 4293614858, 4293680905, 4293681416, 4293747208, 4293747719, 4293747975, 4293814022, 4293814278, 4293880325, 4293880581, 4293881092, 4293947139, 4293947395, 4294013442, 4294013698, 4294014209, 4294080001, 4294080512, 4294146560, 4294146816, 4294147328, 4294213376, 4294213632, 4294214144, 4294280192, 4294280704, 4294280960, 4294347008, 4294347520, 4294347776, 4294413824, 4294414336, 4294480384, 4294480640, 4294481152, 4294547200, 4294547456, 4294547968, 4294614016, 4294614528, 4294614784, 4294680832, 4294681344, 4294747392, 4294747648, 4294747904, 4294748416, 4294748672, 4294749184, 4294749440, 4294749952, 4294750208, 4294750464, 4294750976, 4294751232, 4294751744, 4294752000, 4294752512, 4294752768, 4294753280, 4294753536, 4294753792, 4294754304, 4294754560, 4294755072, 4294755328, 4294755840, 4294756096, 4294756608, 4294756869, 4294757130, 4294757391, 4294757652, 4294757913, 4294758174, 4294758435, 4294758696, 4294758957, 4294759219, 4294759480, 4294759741, 4294760258, 4294760519, 4294760780, 4294761041, 4294826838, 4294827099, 4294827360, 4294827622, 4294827883, 4294828144, 4294828405, 4294828666, 4294829183, 4294829444, 4294829705, 4294829966, 4294830227, 4294830489, 4294830750, 4294831011, 4294831272, 4294897069, 4294897330, 4294897591, 4294897852, 4294898369, 4294898630, 4294898892, 4294899153, 4294899414, 4294899675, 4294899936, 4294900197, 4294900458, 4294900719, 4294900980, 4294901241, 4294967295, 4294967295, 4294967295, 4294967295, 4294967295, 4294967295];
            $(document).ready(function () {
                $('#map').heatmap({
                    'data': $('#points').val(),
                    'opacity': 0.7,
                    'radius': <bean:write name="heatMapForm" property="search.radius"/>,
                    gradient: grad,
                    southBound:<bean:write name="heatMapForm" property="southBound"/>,
                    westBound:<bean:write name="heatMapForm" property="westBound"/>,
                    northBound:<bean:write name="heatMapForm" property="northBound"/>,
                    eastBound:<bean:write name="heatMapForm" property="eastBound"/>
                });
            })
        </script>
        <script type="text/javascript">
            function movePage(taskID) {
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }
        </script>
        <style type="text/css">
            div#map {
                width: 100%;
                height: 70%;
                position: relative;
                margin-left: 0px;
                text-align: center;
            }
        </style>
    </head>
    <body>
    <html:form action="/feature/inquiry/HeatMap.do" method="post">
        <html:hidden property="task" value="Load"/>
        <%-- 	<%@include file="../../../Error.jsp"%> --%>
        <div align="center">

            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.heatmap.title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.branch">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboBranch" scope="request">
                                <html:options collection="comboBranch" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.privilege.form"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.scheme">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboScheme" scope="request">
                                <html:options collection="comboScheme" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.heatmap.radius"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.radius" size="4" maxlength="3" styleClass="TextBox"/>
                        <font color="#FF0000">*) <html:errors property="radius"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.heatmap.dateperiod"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.tglStart" size="8" maxlength="8" styleClass="TextBox"/>
                        to
                        <html:text property="search.tglEnd" size="8" maxlength="8" styleClass="TextBox"/>
                        <font color="#FF0000">*) <html:errors property="datePeriod"/></font>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Load');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search"
                                 title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
        <logic:equal name="heatMapForm" property="task" value="Load">
            <div>
                <logic:messagesNotPresent>
                    <div id="map"></div>
                </logic:messagesNotPresent>
            </div>
            <textarea id="points" style="display:none;">
			<logic:notEmpty name="listLocation" scope="request">
                <logic:iterate id="data" name="listLocation" scope="request">
                    1,<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/></logic:iterate>
            </logic:notEmpty>
		</textarea>
        </logic:equal>
    </html:form>
    </body>
</html:html>