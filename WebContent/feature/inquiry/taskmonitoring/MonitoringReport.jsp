<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link href="../../../style/TMT.css" rel="stylesheet" type="text/css">
        <link href="Monitoring.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery.ui.all.css" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript">
            function movePage(taskID, branchId) {
                document.forms[0].task.value = taskID;
                //document.forms[0].branchId.value = branchId;
                document.forms[0].submit();

            }
            function openViewDetail(mobileAssignmentId) {
                window.open('<html:rewrite action="/feature/view/Assignment"/>?task=Load&mobileAssignmentId=' + mobileAssignmentId,
                    'viewSurveyDetail', 'height=650,width=800,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes,titlebar=no,location=no');
                return;
            }
        </script>
        <script language="javascript" src="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" src="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" src="../../../javascript/global/global.js"></script>
        <script language="javascript" src="../../../javascript/global/disable.js"></script>
        <script language="javascript" src="../../../javascript/application/TMT.js"></script>
        <script language="javascript" src="../../../javascript/global/js_validator.js"></script>
        <script type='text/javascript' src='../../../dwr/interface/Monitoring.js'></script>
        <script type="text/javascript" src='../../../dwr/engine.js'></script>
        <script type="text/javascript" src="Monitoring.js"></script>
    </head>
    <body>
    <html:form method="post" action="/feature/inquiry/TaskMonitoring.do">
        <html:hidden property="task"/>
        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.task.monitoring"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="2" cellspacing="1" class="tableview">
                <tr id="searchbranchvalue">
                    <td width="25%" class="Edit-Left-Text"><bean:message key="feature.surveyor.working.branch"/></td>
                    <td class="Edit-Right-Text">
                        <html:select styleId="searchBranch" property="searchBranch">
                            <logic:notEmpty name="comboBranch" scope="request">
                                <html:options collection="comboBranch" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="70%" height="30">
                        <table>
                            <tr>
                                <td><bean:message key="feature.task.monitoring.legend.block"/></td>
                                <td>
                                    <div class="divShowBox divRed">&nbsp;<bean:message
                                            key="feature.task.monitoring.legend.n"/>/<bean:message
                                            key="feature.task.monitoring.legend.a"/>&nbsp;
                                    </div>
                                </td>
                                <td>
                                    <div class="divShowBox divYellow">&nbsp;<bean:message
                                            key="feature.task.monitoring.legend.r"/>&nbsp;
                                    </div>
                                </td>
                                <td>
                                    <div class="divShowBox divGreen">&nbsp;<bean:message
                                            key="feature.task.monitoring.legend.s"/>&nbsp;
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="30%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../../images/button/ButtonSearch.png" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
            <logic:notEqual name="reportMonitorForm" property="task" value="Show">
                <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td><bean:message key="feature.surveyor.filed.person"/></td>
                        <td><bean:message key="feature.task.monitoring.legend.tasks"/></td>
                    </tr>
                    <logic:notEmpty name="listAssignmentSet" scope="request">
                        <logic:iterate id="data" name="listAssignmentSet" scope="request" indexId="i">
                            <%String classColor = ((i % 2) != 1) ? "evalcol" : "oddcol";%>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td width="10%" valign="top">
                                    <bean:write name="data" property="fullName"/>
                                    <br/>[<bean:write name="data" property="userId"/>]
                                </td>
                                <td>
                                    <div id="<bean:write name="data" property="userId"/>">
                                        <logic:iterate id="data" name="data" property="listOfAssignment">
                                            <% String divClass = ""; %>
                                            <logic:equal value="S" name="data" property="assignmentStatus">
                                                <%divClass = "divGreen"; %>
                                            </logic:equal>
                                            <logic:equal value="R" name="data" property="assignmentStatus">
                                                <logic:notEmpty name="data" property="retrieveDate">
                                                    <%divClass = "divYellow"; %>
                                                </logic:notEmpty>
                                            </logic:equal>
                                            <logic:equal value="N" name="data" property="assignmentStatus">
                                                <logic:empty name="data" property="retrieveDate">
                                                    <%divClass = "divRed"; %>
                                                </logic:empty>
                                            </logic:equal>
                                            <logic:equal value="A" name="data" property="assignmentStatus">
                                                <logic:empty name="data" property="retrieveDate">
                                                    <%divClass = "divRed"; %>
                                                </logic:empty>
                                            </logic:equal>
                                            <div class="divShow <%=divClass %>"
                                                 onclick="openViewDetail('<bean:write name="data"
                                                                                      property="mobileAssignmentId"/>')">
                                                <bean:message key="common.id"/>:<bean:write name="data"
                                                                                            property="mobileAssignmentId"/>
                                                <div class="divWhite">
                                                    <bean:message
                                                            key="feature.task.monitoring.legend.nn"/>/<bean:message
                                                        key="feature.task.monitoring.legend.aa"/>:<bean:write
                                                        name="data" property="assignmentDate"/>
                                                    <br/><bean:message
                                                        key="feature.task.monitoring.legend.rr"/>:<bean:write
                                                        name="data" property="retrieveDate"/>
                                                    <br/><bean:message
                                                        key="feature.task.monitoring.legend.ss"/>:<bean:write
                                                        name="data" property="submitDate"/>
                                                </div>
                                            </div>
                                        </logic:iterate>
                                    </div>
                                </td>
                            </tr>
                        </logic:iterate>
                    </logic:notEmpty>
                </table>
            </logic:notEqual>
        </div>
    </html:form>
    <div style="visibility: hidden;">
        <label id="id"><bean:message key="common.id"/></label>
        <label id="nn"><bean:message key="feature.task.monitoring.legend.nn"/></label>
        <label id="rr"><bean:message key="feature.task.monitoring.legend.rr"/></label>
        <label id="ss"><bean:message key="feature.task.monitoring.legend.ss"/></label>
        <label id="aa"><bean:message key="feature.task.monitoring.legend.aa"/></label>
    </div>
    </body>
</html:html>