<%@page import="treemas.util.constant.PropertiesKey" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<% String key = ConfigurationProperties.getInstance().get(PropertiesKey.WEB_KEY); %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery.ui.all.css" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <link href="../../../style/ASL.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" src="../../../javascript/global/jquery-all.js"></script>
        <!-- 	<script language="javascript" SRC="../../../javascript/application/google.js"></script> -->
        <script language="javascript" SRC="../../../javascript/application/ASL.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=<%=key%>&sensor=false"></script>
        <script type='text/javascript' src='../../../dwr/interface/Dashboard.js'></script>
        <script type="text/javascript" src='../../../dwr/engine.js'></script>
        <script type="text/javascript">
            function initialize() {
                <%-- 	  var searchJob = <%=request.getAttribute("searchJob")%>; --%>
// 	  $("#searchJob").val(searchJob);

                <logic:notEmpty name="listAllSurveyorLocation" scope="request">
                contextP = '<%=request.getContextPath()%>';
                <%
                StringBuffer absolutePath = new StringBuffer()
                    .append("http://")
                    .append(request.getServerName())
                    .append(":")
                    .append(request.getServerPort())
                    .append(request.getContextPath())
                    .append("/images/map/");
              %>
                surveyors = [
                    <logic:iterate id="data" indexId="idx" name="listAllSurveyorLocation" scope="request">
                    <logic:greaterThan name="personLocationForm" property="listSize" value="<%=String.valueOf(idx.intValue() + 1)%>">
                    <logic:notEmpty name="data" property="latitude">
                    new surveyor(
                        '<bean:write name="data" property="surveyorName"/>',
                        '<bean:write name="data" property="timestamp"/>',
                        <bean:write name="data" property="accuracy" />,
                        <bean:write name="data" property="latitude"/>,
                        <bean:write name="data" property="longitude"/>
                    ),
                    </logic:notEmpty>
                    </logic:greaterThan>

                    <logic:lessEqual name="personLocationForm" property="listSize" value="<%=String.valueOf(idx.intValue() + 1)%>">
                    <logic:notEmpty name="data" property="latitude">
                    new surveyor(
                        '<bean:write name="data" property="surveyorName"/>',
                        '<bean:write name="data" property="timestamp"/>',
                        <bean:write name="data" property="accuracy" />,
                        <bean:write name="data" property="latitude"/>,
                        <bean:write name="data" property="longitude"/>
                    )
                    </logic:notEmpty>
                    </logic:lessEqual>
                    </logic:iterate>
                ];

                var myLatLng = new google.maps.LatLng(surveyors[0].lintang, surveyors[0].bujur);
                myOptions = {
                    zoom: 5,
                    center: myLatLng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    navigationControl: true,
                    navigationControlOptions: {
                        style: google.maps.NavigationControlStyle.ZOOM_PAN,
                        position: google.maps.ControlPosition.RIGHT
                    },
                    scaleControl: true,
                    scaleControlOptions: {
                        position: google.maps.ControlPosition.BOTTOM_RIGHT
                    }
                };

                var absPath = '<%=absolutePath.toString()%>';
                var flagCustomer = absPath + 'map-customer-marker.png';
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                for (var i = 0; i < surveyors.length; i++) {
                    toggleFlag[i] = false;

                    createMarker(surveyors[i], map);
                    createCircleOverlay(surveyors[i], map);
                    createMarkerEvent(surveyors[i], map, surveyors[i].infoWindow);
                }

                var bound = new google.maps.LatLngBounds(
                    new google.maps.LatLng(<bean:write name="personLocationForm" property="southBound"/>, <bean:write name="personLocationForm" property="westBound"/>),
                    new google.maps.LatLng(<bean:write name="personLocationForm" property="northBound"/>, <bean:write name="personLocationForm" property="eastBound"/>)
                );
                map.fitBounds(bound);
                </logic:notEmpty>
            }
        </script>
    </head>
    <body onload="initialize();">
    <html:form action="/feature/inquiry/PersonLocation.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="searchJob" styleId="searchJob"/>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <logic:equal name="personLocationForm" property="searchJob" value="COLL"><bean:message
                                key="feature.location.history.collector"/></logic:equal>
                        <logic:notEqual name="personLocationForm" property="searchJob" value="COLL"><bean:message
                                key="app.assginment.surveyorid"/></logic:notEqual>
                        <bean:message key="feature.person.location.last"/>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text">
                        <html:select styleId="searchBranch" property="searchBranch"
                                     onchange="document.forms[0].submit();">
                            <logic:notEmpty name="comboBranch" scope="request">
                                <html:options collection="comboBranch" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Right-Text" colspan="2" align="right">
                        <div id="control" align="right">
                            <a id="controlbtn" class="open" onclick="showorhide();" href="javascript:void(0);"
                               title="SHOW/HIDE USER PANEL">
                                <img id="controlbtnimg" src="../../../images/button/ButtonHideUser.png" width="100"
                                     height="31" border="0" alt="HIDE PANEL"/>
                            </a>
                        </div>
                    </td>
                </tr>
            </table>
            <logic:messagesNotPresent>
                <div id="mapcvs" style="width: 95%;">
                    <bean:define id="sysdate" name="sysdate" scope="request" type="java.lang.String"/>
                    <script type="text/javascript">todayDate = '<%=sysdate %>';</script>
                    <bean:define id="branch" name="branch" scope="request" type="java.lang.String"/>
                    <div id="panel">
                        <ul type="square">
                            <logic:notEmpty name="listAllSurveyorLocation" scope="request">
                                <logic:iterate id="data" indexId="idx" name="listAllSurveyorLocation" scope="request">
                                    <li id="<bean:write name="data" property="surveyorId"/>">
                                        <a id="list" onclick="toggleInfoWindow(<%=idx.intValue()%>)"
                                           href="javascript:void(0);">
                                            <bean:write name="data" property="surveyorName"/>
                                        </a>
                                            <%--                			<br/><bean:message key="feature.activityreport.lasttrack"/> <span id="sysdate<%=idx.intValue()%>"><script type="text/javascript">console.log(todayDate);</script></span> --%>
                                        <br/><bean:message key="feature.activityreport.lastsubmit"/> <span
                                            id="timestamp<%=idx.intValue()%>"><bean:write name="data"
                                                                                          property="timestamp"/></span>
                                        <br/>
                                        <span id="submit<%=idx.intValue()%>"><bean:message
                                                key="feature.person.location.submitted"/>
		                	<logic:greaterThan value="0" name="data" property="cntSubmittedTask">
                                <%-- 					<a href="javascript:void(0)" onclick="flyToInquiry('<%=request.getContextPath()%>', '<%=branch %>','<bean:write name="data" property="surveyorName"/>','S','<%=sysdate %>');"> --%>
                            </logic:greaterThan>
                    		<bean:write name="data" property="cntSubmittedTask"/>
                  			<logic:greaterThan value="0" name="data" property="cntSubmittedTask">
                                <!-- </a> -->
                            </logic:greaterThan></span> <bean:message key="common.of"/>
                                        <span id="os<%=idx.intValue()%>"><bean:message
                                                key="feature.person.location.num.of.task"/>
	                  		<logic:greaterThan value="0" name="data" property="cntOsTask">
                                <%-- 					<a href="javascript:void(0)" onclick="flyToInquiry('<%=request.getContextPath()%>', '<%=branch %>','<bean:write name="data" property="surveyorName"/>','N','');"> --%>
                            </logic:greaterThan>
                    		<bean:write name="data" property="cntOsTask"/>
				  			<logic:greaterThan value="0" name="data" property="cntOsTask">
                                <!-- </a> -->
                            </logic:greaterThan>
                  		</span>
                                        <br/>
                                        <span id="address<%=idx.intValue()%>"><bean:write name="data"
                                                                                          property="address"/></span>
                                    </li>
                                </logic:iterate>
                            </logic:notEmpty>
                        </ul>
                    </div>
                    <div id="map_canvas"></div>
                </div>
            </logic:messagesNotPresent>
        </div>
        <div style="visibility: hidden;">
            <label id="lblsubmitted"><bean:message key="feature.person.location.submitted"/></label>
            <label id="lbltask"><bean:message key="feature.person.location.num.of.task"/></label>
        </div>
    </html:form>
    </body>
</html:html>