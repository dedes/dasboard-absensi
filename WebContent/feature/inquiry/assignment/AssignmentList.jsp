<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery.ui.all.css" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" src="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" src="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" src="../../../javascript/global/global.js"></script>
        <script language="javascript" src="../../../javascript/global/disable.js"></script>
        <script language="javascript" src="../../../javascript/global/js_validator.js"></script>
        <script language="javascript" src="../../../javascript/global/html2canvas.js"></script>
        <!-- 	<script language="javascript" SRC="../../../javascript/application/google.js"></script> -->
        <script language="javascript">
            $(function () {
                $("input[name*='search.tgl']").datepicker({
                    maxDate: "+0M +0D",
                    dateFormat: "ddmmyy",
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    showOn: "both",
                    buttonImage: "../../../images/DatePicker.gif"
                });

// 			var availableTags = [
// 								<logic:notEmpty name="comboBranch" scope="request">
// 									<logic:iterate id="data" name="comboBranch" scope="request" >
// 										'<bean:write name="data" property="keyValue"/>',
// 									</logic:iterate>
// 								</logic:notEmpty>
// 								''
// 			                   ];
// 			                   $("#search.branch").autocomplete({
// 			                       source: availableTags
// 			                   });
            });

            function movePage(taskID) {
                if ('Generate' == taskID) {
                    putForm(document.assignmentForm);
                }
// 			findElement(document.forms[0], 'search.status2hidden').value = '';
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

            function openViewDetail(mobileAssignmentId) {
                window.open('<html:rewrite action="/feature/view/Assignment"/>?task=Load&mobileAssignmentId=' + mobileAssignmentId,
                    'viewSurveyDetail', 'height=650,width=800,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes,titlebar=no,location=no');
                return;
            }
        </script>
        <style>
            input[type=search] {
                background: none;
                font-weight: bold;
                border-color: #2e2e2e;
                border-style: solid;
                border-width: 2px 2px 2px 2px;
                outline: none;
                padding: 10px 20px 10px 20px;
                width: 250px;
            }

            ul.ui-autocomplete {
                color: red !important;
                -moz-border-radius: 15px;
                border-radius: 15px;
            }
        </style>

    <body>
    <html:form action="/feature/inquiry/Assignment.do" method="post">
        <html:hidden property="task" value="Load"/>
        <%--     <html:hidden property="search.status2hidden"/> --%>
        <html:hidden property="width" name="assignmentForm"/>
        <html:hidden property="height" name="assignmentForm"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <%-- 	<%@include file="../../../Error.jsp"%> --%>


        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.assignment.pic"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:select property="search.branch">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboBranch" scope="request">
                                <html:options collection="comboBranch" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                    <td class="Edit-Left-Text"><bean:message key="app.privilege.form"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:select property="search.scheme">
                            <html:option value="All"><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboScheme" scope="request">
                                <html:options collection="comboScheme" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text">
                        <div id="columntext" style="text-align: right !important; display: inline">
                            <html:select property="search.searchType1">
                                <html:option value="10"><bean:message
                                        key="combo.label.assignment.customername"/></html:option>
                                <html:option value="20"><bean:message
                                        key="combo.label.assignment.pic.name"/></html:option>
                                <html:option value="30"><bean:message
                                        key="combo.label.assignment.application.no"/></html:option>
                            </html:select>
                        </div>
                    </td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="search.searchValue1" size="25" maxlength="30" styleClass="TextBox"/>
                    </td>
                    <td class="Edit-Left-Text">
                        <div id="columntext" style="text-align: right !important; display: inline">
                            <html:select property="search.searchType2">
                                <html:option value="20"><bean:message key="combo.label.assignment.date"/></html:option>
                                <html:option value="30"><bean:message
                                        key="combo.label.assignment.retrieve.date"/></html:option>
                                <html:option value="40"><bean:message key="combo.label.submit.date"/></html:option>
                            </html:select>
                        </div>
                    </td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="search.tglStart" size="8" maxlength="8" styleClass="TextBox"/>
                        <bean:message key="paging.showing.to"/>
                        <html:text property="search.tglEnd" size="8" maxlength="8" styleClass="TextBox"/>
                        <font color="#FF0000"><html:errors property="search.tglStart"/></font>&nbsp&nbsp
                        <font color="#FF0000"><html:errors property="search.tglEnd"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.assignment.task.status"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:select property="search.status">
                            <html:option value="All"><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboStatus" scope="request">
                                <html:options collection="comboStatus" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                    <td class="Edit-Left-Text"><bean:message key="common.sort.by"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:select property="search.orderField">
                            <html:option value="80"><bean:message key="combo.label.assignment.no"/></html:option>
                            <html:option value="10"><bean:message
                                    key="combo.label.assignment.field.person"/></html:option>
                            <html:option value="20"><bean:message
                                    key="combo.label.assignment.customername"/></html:option>
                            <html:option value="40"><bean:message key="combo.label.assignment.date"/></html:option>
                            <html:option value="60"><bean:message key="combo.label.submit.date"/></html:option>
                        </html:select>
                        <br/>
                        <label style="vertical-align: middle;"><html:checkbox property="search.sortDesc"><bean:message
                                key="common.check.sort"/></html:checkbox></label>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <logic:notEmpty name="listAssignment" scope="request">
                            <a href="javascript:movePage('Generate','');">
                                <img src="../../../images/button/ButtonGenerate.png" alt="Generate" width="100"
                                     height="31" border="0" title="<bean:message key="tooltip.generate"/>">
                            </a>
                            &nbsp;
                        </logic:notEmpty>
                        <a href="javascript:movePage('Search','');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search" width="100" height="31"
                                 border="0" title="<bean:message key="tooltip.search"/>">
                        </a>
                    </td>
                </tr>
            </table>
            <logic:notEqual name="assignmentForm" property="task" value="Show">
                <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="5%" rowspan="2" align="center"><bean:message key="common.id"/></td>
                        <td width="5%" rowspan="2" align="center"><bean:message
                                key="feature.assignment.application.no"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="app.privilege.form"/></td>
                        <td width="15%" rowspan="2" align="center"><bean:message key="app.assignment.pic"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message
                                key="feature.assignment.customername"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="common.status"/></td>
                        <td width="15%" colspan="3" align="center"><bean:message key="common.date"/></td>
                        <td width="5%" colspan="1" align="center"><bean:message key="common.action"/></td>
                    </tr>
                    <tr align="center" class="headercol">
                        <td width="5%" align="center"><bean:message key="common.assignment"/></td>
                        <td width="5%" align="center"><bean:message key="common.retrieve"/></td>
                        <td width="5%" align="center"><bean:message key="common.submit"/></td>
                        <td width="5%" align="center"><bean:message key="app.assignment.view"/></td>
                    </tr>
                    <logic:notEmpty name="listAssignment" scope="request">
                        <logic:iterate id="data" name="listAssignment" scope="request" indexId="i">
                            <%
                                String classColor = ((i % 2) != 1) ? "evalcol" : "oddcol";
                            %>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td align="right"><bean:write name="data" property="mobileAssignmentId"/></td>
                                <td><bean:write name="data" property="applNo"/></td>
                                <td align="center" title="<bean:write name="data" property="schemeDescription"/>">
                                    <bean:write name="data" property="schemeId"/></td>
                                <td><bean:write name="data" property="surveyorName"/></td>
                                <td><bean:write name="data" property="customerName"/></td>
                                <td align="center"><bean:write name="data" property="statusDescription"/></td>
                                <td align="center"><bean:write name="data" property="assignmentDate"/></td>
                                <td align="center"><bean:write name="data" property="retrieveDate"/></td>
                                <td align="center"><bean:write name="data" property="submitDate"/></td>
                                <td align="center">
                                    <a href="javascript:openViewDetail('<bean:write name="data" property="mobileAssignmentId"/>')">
                                        <img src="../../../images/icon/IconLookUp.gif" width="16" height="16" border="0"
                                             alt="VIEW" title="VIEW ASSIGNMENT"/>
                                    </a>
                                </td>
                            </tr>
                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listAssignment" scope="request">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="10" align="center"><font class="errMsg"><bean:message
                                    key="common.listnotfound"/></font></td>
                        </tr>
                    </logic:empty>
                </table>
            </logic:notEqual>
        </div>

        <logic:notEmpty name="listAssignment" scope="request">
            <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="30" align="center">
                        <html:link href="javascript:moveToPageNumber(document.assignmentForm,'First')" title="First">
                            <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                        </html:link>
                    </td>
                    <td width="30" align="center">
                        <html:link href="javascript:moveToPageNumber(document.assignmentForm,'Prev')" title="Previous">
                            <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                        </html:link>
                    </td>
                    <td width="200" align="center"><bean:message key="paging.page"/>
                        <html:text name="assignmentForm" property="paging.currentPageNo" size="3" maxlength="6"
                                   styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                property="paging.totalPage" write="true"/>
                        <html:hidden property="paging.totalRecord" write="false"/>
                        <a href="javascript:moveToPageNumber(document.assignmentForm,'Go');">
                            <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle" border="0"/>
                        </a>
                    </td>
                    <td width="30" align="center">
                        <html:link href="javascript:moveToPageNumber(document.assignmentForm,'Next')" title="Next">
                            <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                        </html:link>
                    </td>
                    <td width="30" align="center">
                        <html:link href="javascript:moveToPageNumber(document.assignmentForm,'Last')" title="Last">
                            <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                        </html:link>
                    </td>
                    <td align="right">
                        <bean:define id="currentPage" name="assignmentForm" property="paging.currentPageNo"
                                     type="java.lang.Integer"/>
                        <bean:define id="total" name="assignmentForm" property="paging.totalRecord"
                                     type="java.lang.Integer"/>
                        <bean:define id="perpage" name="assignmentForm" property="paging.rowPerPage"
                                     type="java.lang.Integer"/>
                        <%
                            int startShow = 1;
                            int endShow = 0;
                            if (currentPage == 1) {
                                endShow = perpage;
                            } else if (currentPage > 1) {
                                int tempstart = ((currentPage - 1) * perpage) + 1;
                                int tempend = currentPage * perpage;
                                if (total < tempend) {
                                    tempend = total;
                                }
                                startShow = tempstart;
                                endShow = tempend;
                            }

                            if (endShow >= total) {
                                endShow = total;
                            }
                        %>
                        <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                            key="paging.showing.to"/> <%=endShow%> <bean:message
                            key="paging.showing.total"/> <%=total %> <bean:message key="paging.showing.record"/>
                    </td>
                </tr>
            </table>
        </logic:notEmpty>
    </html:form>
    </body>
</html:html>

