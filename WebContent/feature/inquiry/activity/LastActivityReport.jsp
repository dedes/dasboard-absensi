<%@page import="treemas.util.constant.PropertiesKey" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tlds/birt.tld" prefix="birt" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
    <html:base/>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title></title>
        <script language="javascript">

            function movePage(taskID, branchId, useId) {
                document.forms[0].task.value = taskID;
                //document.forms[0].searchBranch.value = branchId;
                document.forms[0].searchBranch.value = document.getElementById('sb_id').value;
                if (taskID == 'Preview')
                    document.forms[0].surveyor.value = useId;
                //if(taskID=='Search')
                //alert(document.getElementById('sb_id').value);
                document.forms[0].submit();

            }
        </script>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery.ui.all.css" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" src="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" src="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" src="../../../javascript/global/global.js"></script>
        <script language="javascript" src="../../../javascript/global/disable.js"></script>
        <script language="javascript" src="../../../javascript/global/js_validator.js"></script>
    </head>

    <body>
    <html:form method="post" action="/feature/inquiry/HandsetActivityLog.do">
        <html:hidden property="task"/>
        <html:hidden property="surveyor"/>
        <html:hidden property="job"/>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.activityreport.title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="searchBranch" styleId="sb_id">
                            <logic:notEmpty name="comboBranch" scope="request">
                                <html:options collection="comboBranch" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search','','');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search" width="100" height="31"
                                 border="0" title="<bean:message key="tooltip.search"/>">
                        </a>
                    </td>
                </tr>
            </table>
            <logic:notEqual name="userMobileForm" property="task" value="Show">
                <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="15%" align="center" colspan="2"><bean:message
                                key="feature.location.history.pic"/></td>
                        <td width="5%" align="center" rowspan="2"><bean:message
                                key="feature.activityreport.totallog"/></td>
                        <td width="20%" align="center" rowspan="2"><bean:message
                                key="feature.activityreport.lasttrack"/></td>
                        <td width="10%" align="center" rowspan="2"><bean:message
                                key="feature.activityreport.lastsubmit"/></td>
                        <td width="5%" colspan="1" align="center"><bean:message key="common.action"/></td>
                    </tr>
                    <tr align="center" class="headercol">
                        <td width="5%" align="center"><bean:message key="app.user.id"/></td>
                        <td width="10%" align="center"><bean:message key="common.name"/></td>
                        <td width="5" align="center"><bean:message key="app.assignment.view"/></td>
                    </tr>
                    <logic:notEmpty name="listReportBranch" scope="request">
                        <logic:iterate id="data" scope="request" name="listReportBranch" indexId="i">
                            <%
                                String classColor = ((i % 2) != 1) ? "evalcol" : "oddcol";
                            %>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td align="center"><bean:write name="data" property="surveyor"/></td>
                                <td align="center"><bean:write name="data" property="nama"/></td>
                                <td align="right"><bean:write name="data" property="totalLog"/></td>
                                <td align="left"><bean:write name="data" property="lastActivityLog"/></td>
                                <td align="center"><bean:write name="data" property="lastLog"/></td>
                                <td align="center">
                                    <a href="javascript:movePage('Preview','<bean:write name="userMobileForm" property="searchBranch"/>','<bean:write name="data" property="surveyor" />')">
                                        <img src="../../../images/icon/IconLookUp.gif" alt="View Log" title="View Log"
                                             width="16" height="16"/>
                                    </a>
                                </td>
                            </tr>
                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listReportBranch" scope="request">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="6" align="center"><font class="errMsg"><bean:message
                                    key="common.listnotfound"/></font></td>
                        </tr>
                    </logic:empty>
                </table>
            </logic:notEqual>
        </div>
    </html:form>
    </body>
</html:html>