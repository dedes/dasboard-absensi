<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery.ui.all.css" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript" src="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" src="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript">
            $(function () {
                $("#dateSearch").datepicker({
                    maxDate: "+0M +0D",
                    dateFormat: "ddmmyy",
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    showOn: "both",
                    buttonImage: "../../../images/DatePicker.gif"
                });

                $("#target").change(function () {
                    if ($("#target").val() == '0') {
                        $("input[name*='searchDate']").val('');
                    }
                });

            });

            function movePage(taskID) {
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }
        </script>
    </head>
    <body oncontextmenu="return false;">
    <html:form action="/feature/inquiry/HandsetActivityLog.do" method="post">
        <html:hidden property="task" value="Preview"/>
        <html:hidden property="job"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.activity.log"/> <html:hidden
                            property="surveyor" write="true"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text"><html:hidden property="searchBranch" write="true"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.activity.date.filtering"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="filter" styleId="target">
                            <html:option value="0"><bean:message key="common.all"/></html:option>
                            <html:option value="10"><bean:message key="combo.label.equal"/></html:option>
                            <html:option value="20"><bean:message key="combo.label.after"/></html:option>
                            <html:option value="30"><bean:message key="combo.label.before"/></html:option>
                        </html:select>
                        &nbsp;
                        <html:text property="searchDate" styleId="dateSearch" size="8" maxlength="8"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Preview');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search" width="100" height="31"
                                 border="0" title="<bean:message key="tooltip.search"/>">
                        </a>
                    </td>
                </tr>
            </table>
            <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="5%" rowspan="2" align="center"><bean:message key="feature.location.history.pic"/></td>
                    <td width="10%" rowspan="2" align="center"><bean:message key="feature.surveyor.name"/></td>
                    <td width="25%" colspan="2" align="center"><bean:message key="feature.activity.log2"/></td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="15%" align="center"><bean:message key="feature.activity"/></td>
                    <td width="10%" align="center"><bean:message key="common.time"/></td>
                </tr>
                <logic:notEmpty name="listDetailReportBranch" scope="request">
                    <logic:iterate id="data" scope="request" name="listDetailReportBranch" indexId="i">
                        <% String classColor = ((i % 2) != 1) ? "evalcol" : "oddcol"; %>
                        <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=classColor%>'">
                            <td align="center"><bean:write name="data" property="surveyor"/></td>
                            <td align="left"><bean:write name="data" property="nama"/></td>
                            <td align="left"><bean:write name="data" property="lastActivityLog"/></td>
                            <td align="center"><bean:write name="data" property="lastLog"/></td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="listDetailReportBranch" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="4" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
        </div>
        <logic:notEmpty name="listDetailReportBranch" scope="request">
            <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="30" align="center">
                        <html:link href="javascript:moveToPageNumber(document.userMobileForm,'First')" title="First">
                            <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                        </html:link>
                    </td>
                    <td width="30" align="center">
                        <html:link href="javascript:moveToPageNumber(document.userMobileForm,'Prev')" title="Previous">
                            <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                        </html:link>
                    </td>
                    <td width="200" align="center"><bean:message key="paging.page"/>
                        <html:text name="userMobileForm" property="paging.currentPageNo" size="3" maxlength="6"
                                   styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                property="paging.totalPage" write="true"/>
                        <html:hidden property="paging.totalRecord" write="false"/>
                        <a href="javascript:moveToPageNumber(document.userMobileForm,'Go');">
                            <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle" border="0"/>
                        </a>
                    </td>
                    <td width="30" align="center">
                        <html:link href="javascript:moveToPageNumber(document.userMobileForm,'Next')" title="Next">
                            <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                        </html:link>
                    </td>
                    <td width="30" align="center">
                        <html:link href="javascript:moveToPageNumber(document.userMobileForm,'Last')" title="Last">
                            <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                        </html:link>
                    </td>
                    <td align="right">
                        <bean:define id="currentPage" name="userMobileForm" property="paging.currentPageNo"
                                     type="java.lang.Integer"/>
                        <bean:define id="total" name="userMobileForm" property="paging.totalRecord"
                                     type="java.lang.Integer"/>
                        <bean:define id="perpage" name="userMobileForm" property="paging.rowPerPage"
                                     type="java.lang.Integer"/>
                        <%
                            int startShow = 1;
                            int endShow = 0;
                            if (currentPage > 1) {
                                int tempstart = ((currentPage - 1) * perpage) + 1;
                                int tempend = currentPage * perpage;
                                if (total < tempend) {
                                    tempend = total;
                                }
                                startShow = tempstart;
                                endShow = tempend;
                            } else {
                                endShow = total;
                            }
                        %>
                        <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                            key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                        <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                            key="paging.showing.record"/>
                    </td>
                </tr>
            </table>
        </logic:notEmpty>
    </html:form>
    </body>
</html:html>