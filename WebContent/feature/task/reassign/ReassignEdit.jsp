<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="../../../javascript/global/global.js"></script>
        <script language="javascript" src="../../../javascript/global/disable.js"></script>
        <script language="javascript" src="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function movePage(taskID) {
                if (taskID == "Update") {
                    if (!validateForm(document.forms[0]))
                        return;
                }
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

            function openViewDetail(mobileAssignmentId) {
                window.open('<html:rewrite action="/feature/view/Assignment"/>?task=Load&mobileAssignmentId=' + mobileAssignmentId,
                    'viewSurveyDetail', 'height=600,width=800,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes,titlebar=no,location=no');
                return;
            }

            function setSurveyor(kode, nama) {
                findElement(document.forms[0], "surveyorId").value = kode;
                findElement(document.forms[0], "surveyorName").value = nama;
            }

            function openSurveyor(winName, features, excSurveyor) {
                var branchId = findElement(document.forms[0], "search.branch").value
                var toUrl = '<html:rewrite action="/feature/lookup/UserList"/>?tipe=ONE' + excSurveyor + '&branchId=' + branchId;
                window.open(toUrl, winName, features);
            }
        </script>


    <body>
    <html:form action="/feature/task/Reassign.do" method="post">
        <html:hidden property="task"/>
        <html:hidden property="paging.dispatch"/>
        <html:hidden property="search.branch"/>
        <html:hidden property="search.customerName"/>
        <html:hidden property="search.surveyorId"/>
        <html:hidden property="search.surveyorName"/>
        <html:hidden property="search.priority"/>
        <html:hidden property="search.scheme"/>
        <logic:iterate id="sel" name="reassignForm" property="pilih">
            <input type="hidden" name="pilih" value="<%=sel%>"/>
        </logic:iterate>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.reassign.task"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.reassign.task.to"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.branch" onchange="setBranch();">
                            <logic:notEmpty name="comboBranch" scope="request">
                                <html:text property="surveyorName" size="25" maxlength="30" styleClass="TextBox"/><font
                                    color="#FF0000">*)</font>
                                <a href="javascript:openSurveyor('Surveyor','scrollbars=yes,width=640,height=480,resizable=yes,top=0,left=0','<bean:write name="reassignForm" property="urlPilih"/>')">
                                    <img src="../../../images/icon/IconLookUp.gif" alt="Look Up PIC" border="0"
                                         title="Look Up PIC" width="16" height="16">
                                </a>
                                <html:hidden property="surveyorId"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.notes"/></td>
                    <td class="Edit-Right-Text"><html:textarea property="notes"
                                                               onkeypress="maxLengthTextArea(this, 100)" rows="4"
                                                               cols="50%" styleClass="TextBox"/></td>
                </tr>
            </table>

            <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="5%" align="center"><bean:message key="common.id"/></td>
                    <td width="5%" align="center"><bean:message key="app.privilege.form"/></td>
                    <td width="10%" align="center"><bean:message key="feature.location.history.pic"/></td>
                    <td width="15%" align="center"><bean:message key="feature.approval.customer.name"/></td>
                </tr>
                <logic:notEmpty name="listAssignment" scope="request">
                    <logic:iterate id="data" name="listAssignment" scope="request" indexId="i">
                        <% String classColor = (i % 2) != 1 ? "evalcol" : "oddcol";%>
                        <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=classColor%>'">
                            <td align="right">
                                <a href="javascript:openViewDetail('<bean:write name="data" property="mobileAssignmentId"/>')">
                                    <bean:write name="data" property="mobileAssignmentId"/>
                                </a>
                            </td>
                            <td align="center" title="<bean:write name="data" property="schemeDescription"/>">
                                <bean:write name="data" property="schemeId"/></td>
                            <td><bean:write name="data" property="surveyorName"/></td>
                            <td><bean:write name="data" property="customerName"/></td>
                                <%-- 							<td title="<bean:write name="data" property="branchId"/> - <bean:write name="data" property="branchName"/>"><bean:write name="data" property="branchName"/></td> --%>
                                <%-- 							<td align="center"><bean:write name="data" property="priorityDescription"/></td> --%>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="listAssignment" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="4" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
        </div>
        <table width="95%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="50%" height="30">&nbsp;</td>
                <td width="50%" align="right">
                    <logic:notEmpty name="listAssignment" scope="request">
                        <a href="javascript:movePage('Update');">
                            <img src="../../../images/button/ButtonSubmit.png" width="100" height="31" border="0">
                        </a>
                    </logic:notEmpty>
                    <a href="javascript:movePage('Load');">
                        <img src="../../../images/button/ButtonCancel.png" width="100" height="31" border="0">
                    </a>
                </td>
            </tr>
        </table>
    </html:form>
    </body>
</html:html>