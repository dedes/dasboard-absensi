<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="../../../javascript/global/global.js"></script>
        <script language="javascript" src="../../../javascript/global/disable.js"></script>
        <script language="javascript" src="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function whoami(evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 8 || charCode == 46) {
                    findElement(document.forms[0], "search.surveyorName").value = null;
                    findElement(document.forms[0], "search.surveyorId").value = null;
                }
                return false;
            }

            function checkAll() {
                var form = document.forms[0];
                iLength = form.pilih.length;
                if (iLength > 0) {
                    for (var i = 0; i < iLength; i++) {
                        form.pilih[i].checked = form.chekAll.checked;
                    }
                }
                else
                    form.pilih.checked = form.chekAll.checked;
            }

            function movePage(taskID) {
                if (taskID == "Edit") {
                    len = document.forms[0].elements.length;
                    var i = 0;
                    var found = false;
                    for (i = 0; i < len && !found; i++) {
                        if (document.forms[0].elements[i].name == "pilih") {
                            found = document.forms[0].elements[i].checked;
                        }
                    }
                    if (!found) {
                        alert("Must Choose 1 Assignment");
                        return;
                    }
                }

                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

            function openViewDetail(mobileAssignmentId) {
                window.open('<html:rewrite action="/feature/view/Assignment"/>?task=Load&mobileAssignmentId=' + mobileAssignmentId,
                    'viewSurveyDetail', 'height=600,width=800,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes,titlebar=no,location=no');
                return;
            }

            function setSurveyor(kode, nama) {
                findElement(document.forms[0], "search.surveyorId").value = kode;
                findElement(document.forms[0], "search.surveyorName").value = nama;
            }

            function openSurveyor(winName, features) {
                var e = findElement(document.forms[0], "search.branch");
                var branchId = e.options[e.selectedIndex].value;
                //var branchId = findElement(document.forms[0],"branchId").value;
                if (branchId == "" || branchId == null)
                    alert("choose Branch first");
                else {
                    var toUrl = '<html:rewrite action="/feature/lookup/UserList"/>?tipe=ONE&branchId=' + branchId;
                    window.open(toUrl, winName, features);
                }
            }

            function setBranch() {
                findElement(document.forms[0], "search.surveyorName").value = '';
                findElement(document.forms[0], "search.surveyorId").value = '';
            }
        </script>


    <body>
    <html:form action="/feature/task/Reassign.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.reassign.task"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.branch" onchange="setBranch();">
                            <logic:notEmpty name="comboBranch" scope="request">
                                <html:options collection="comboBranch" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.location.history.pic"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.surveyorName" size="25" maxlength="30" styleClass="TextBox"
                                   onkeypress="javascript:return whoami(event);"/>
                        <a href="javascript:openSurveyor('Surveyor','scrollbars=yes,width=640,height=480,resizable=yes,top=0,left=0')">
                            <img src="../../../images/icon/IconLookUp.gif" alt="Look Up PIC" border="0"
                                 title="Look Up PIC" width="16" height="16">
                        </a>
                        <html:hidden property="search.surveyorId"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.approval.customer.name"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.customerName" size="25" maxlength="30" styleClass="TextBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.privilege.form"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.scheme">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboScheme" scope="request">
                                <html:options collection="comboScheme" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search','');">
                            <img src="../../../images/ButtonSearch.gif" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
            <logic:notEqual name="reassignForm" property="task" value="Show">
                <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="5%" align="center"><input type="checkbox" name="chekAll"
                                                             onclick="javascript:checkAll();"/></td>
                        <td width="5%" align="center"><bean:message key="common.id"/></td>
                        <td width="5%" align="center"><bean:message key="app.privilege.form"/></td>
                        <td width="10%" align="center"><bean:message key="feature.location.history.pic"/></td>
                        <td width="15%" align="center"><bean:message key="feature.approval.customer.name"/></td>
                    </tr>
                    <logic:notEmpty name="listAssignment" scope="request">
                        <logic:iterate id="data" name="listAssignment" scope="request" indexId="i">
                            <% String classColor = (i % 2) != 1 ? "evalcol" : "oddcol";%>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td align="center">
                                    <html:multibox property="pilih">
                                        <bean:write name="data" property="mobileAssignmentId"/>;
                                        <bean:write name="data" property="surveyorId"/>;
                                    </html:multibox>
                                </td>
                                <td align="right">
                                    <a href="javascript:openViewDetail('<bean:write name="data" property="mobileAssignmentId"/>')">
                                        <bean:write name="data" property="mobileAssignmentId"/>
                                    </a>
                                </td>
                                <td align="center" title="<bean:write name="data" property="schemeDescription"/>">
                                    <bean:write name="data" property="schemeId"/></td>
                                <td><bean:write name="data" property="surveyorName"/></td>
                                <td><bean:write name="data" property="customerName"/></td>
                            </tr>
                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listAssignment" scope="request">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="5" align="center"><font class="errMsg"><bean:message
                                    key="common.listnotfound"/></font></td>
                        </tr>
                    </logic:empty>
                </table>
                <logic:notEmpty name="listAssignment" scope="request">
                    <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.reassignForm,'First')"
                                           title="First">
                                    <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.reassignForm,'Prev')"
                                           title="Previous">
                                    <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="200" align="center"><bean:message key="paging.page"/>
                                <html:text name="reassignForm" property="paging.currentPageNo" size="3" maxlength="6"
                                           styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                        property="paging.totalPage" write="true"/>
                                <html:hidden property="paging.totalRecord" write="false"/>
                                <a href="javascript:moveToPageNumber(document.reassignForm,'Go');">
                                    <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                         border="0"/>
                                </a>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.reassignForm,'Next')"
                                           title="Next">
                                    <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.reassignForm,'Last')"
                                           title="Last">
                                    <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td align="right">
                                <bean:define id="currentPage" name="reassignForm" property="paging.currentPageNo"
                                             type="java.lang.Integer"/>
                                <bean:define id="total" name="reassignForm" property="paging.totalRecord"
                                             type="java.lang.Integer"/>
                                <bean:define id="perpage" name="reassignForm" property="paging.rowPerPage"
                                             type="java.lang.Integer"/>
                                <%
                                    int startShow = 1;
                                    int endShow = 0;
                                    if (currentPage > 1) {
                                        int tempstart = ((currentPage - 1) * perpage) + 1;
                                        int tempend = currentPage * perpage;
                                        if (total < tempend) {
                                            tempend = total;
                                        }
                                        startShow = tempstart;
                                        endShow = tempend;
                                    } else {
                                        endShow = total;
                                    }
                                %>
                                <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                    key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                                <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                    key="paging.showing.record"/>
                            </td>
                        </tr>
                    </table>
                </logic:notEmpty>
                <table width="95%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" height="30">&nbsp;</td>
                        <td width="50%" align="right">
                            <logic:notEmpty name="listAssignment" scope="request">
                                <a href="javascript:movePage('Edit');">
                                    <img src="../../../images/button/ButtonNext.png" width="100" height="31"
                                         border="0"/>
                                </a>
                            </logic:notEmpty>
                        </td>
                    </tr>
                </table>
            </logic:notEqual>
        </div>
    </html:form>
    </body>
</html:html>