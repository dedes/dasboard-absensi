<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>

<html:html>
    <html:base/>
    <head>
        <title><bean:message key="feature.assignment.approval"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../../../includes/mss.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/js_validator.js"></script>
        <script language="javascript" SRC="../../../javascript/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global.js"></script>
        <html:javascript scriptLanguage="javascript" formName="approvalInputForm" method="validateForm"
                         staticJavascript="false" page="1"/>
    </head>
    <script language="javascript">
        function flyToPage(taskID) {
            if (taskID == "Update") {
                if (document.forms[0].onsubmit == null || document.forms[0].onsubmit()) {
                    document.forms[0].task.value = taskID;
                    document.forms[0].submit();
                }
            }
            else if (taskID == "Load") {
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }
        }

        function openViewDetail(mobileAssignmentId) {
            window.open('<html:rewrite action="/view/Assignment"/>?task=Load&mobileAssignmentId=' + mobileAssignmentId,
                'viewSurveyDetail', 'height=600,width=800,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes,titlebar=no,location=no');
            return;
        }
    </script>

    <body>
    <html:form action="/survey/ApprovalInput.do" method="POST" onsubmit="return validateForm(this);">
        <html:hidden property="task"/>
        <html:hidden property="search.searchType1"/>
        <html:hidden property="search.searchValue1"/>
        <html:hidden property="search.branch"/>
        <html:hidden property="search.priority"/>
        <html:hidden property="search.scheme"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <%-- <logic:messagesPresent> --%>
        <!-- <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"> -->
        <!-- <tr> -->
        <!-- <td><font class="errMsg"> -->
        <%--              <html:errors/> --%>
        <!-- </font> -->
        <!-- </td> -->
        <!-- </tr> -->
        <!-- </table> -->
        <%-- </logic:messagesPresent> --%>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr class="Top-Table-Header">
                    <td width="10" height="20" class="Top-Table-Header-Left">&nbsp;</td>
                    <td class="Top-Table-Header-Center">
                        <bean:message key="feature.task.assignment.approval"/>
                    </td>
                    <td class="Top-Table-Header-Right">&nbsp;</td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="2" cellspacing="1" class="tableview">
                <tr>
                    <td width="20%" class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td width="30%" class="Edit-Right-Text" colspan="3">
                        <bean:write name="approvalInputForm" property="branchId"/> - <bean:write
                            name="approvalInputForm" property="branchName"/></td>
                </tr>
                <tr>
                    <td width="20%" class="Edit-Left-Text"><bean:message key="feature.approval.task.id"/></td>
                    <td width="30%" class="Edit-Right-Text">
                        <a href="javascript:openViewDetail('<bean:write name="approvalInputForm" property="mobileAssignmentId"/>')">
                            <html:hidden property="mobileAssignmentId" write="true"/>
                        </a>
                    </td>
                    <td width="20%" class="Edit-Left-Text"><bean:message key="app.assignment.priority"/></td>
                    <td width="30%" class="Edit-Right-Text"><bean:write name="approvalInputForm"
                                                                        property="priority"/></td>
                </tr>
                <tr>
                    <td width="20%" class="Edit-Left-Text"><bean:message key="feature.surveyor.filed.person"/></td>
                    <td width="30%" class="Edit-Right-Text"><bean:write name="approvalInputForm"
                                                                        property="surveyorName"/></td>
                    <td width="20%" class="Edit-Left-Text"><bean:message key="app.privilege.form"/></td>
                    <td width="30%" class="Edit-Right-Text">
                        <bean:write name="approvalInputForm" property="schemeId"/>
                        -
                        <bean:write name="approvalInputForm" property="schemeDescription"/></td>
                </tr>
                <tr>
                    <td width="20%" class="Edit-Left-Text"><bean:message key="app.assignment.notes"/></td>
                    <td width="30%" class="Edit-Right-Text" colspan="3"><bean:write name="approvalInputForm"
                                                                                    property="notes"/></td>
                </tr>
                <tr>
                    <td colspan="4" class="tdjudulbiru"><strong><bean:message
                            key="app.assignment.customerdetail"/></strong></td>
                </tr>
                <tr>
                    <td width="20%" class="Edit-Left-Text"><bean:message key="feature.surveyor.name"/></td>
                    <td width="30%" class="Edit-Right-Text"><bean:write name="approvalInputForm"
                                                                        property="customerName"/></td>
                    <td width="20%" class="Edit-Left-Text"><bean:message key="app.user.phone"/></td>
                    <td width="30%" class="Edit-Right-Text"><bean:write name="approvalInputForm"
                                                                        property="customerPhone"/></td>
                </tr>
                <tr>
                    <td width="20%" class="Edit-Left-Text"><bean:message key="feature.branch.address"/></td>
                    <td width="30%" class="Edit-Right-Text" colspan="3"><bean:write name="approvalInputForm"
                                                                                    property="customerAddress"/></td>
                </tr>
            </table>
            <br/>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr class="Top-Table-Header">
                    <td width="10" height="20" class="Top-Table-Header-Left">&nbsp;</td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.task.assignment.verifikasi"/></td>
                    <td class="Top-Table-Header-Right">&nbsp;</td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="2" cellspacing="1" class="tableview">
                <tr>
                    <td width="20%" class="Edit-Left-Text"><bean:message key="common.approval"/></td>
                    <td width="80%" colspan="3" class="Edit-Right-Text">
                        <html:select property="approval">
                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
                            <html:option value="N"><bean:message key="tooltip.button.app"/></html:option>
                            <html:option value="V"><bean:message key="combo.label.need.revision"/></html:option>
                            <html:option value="J"><bean:message key="tooltip.button.reject"/></html:option>
                        </html:select>
                        <font color="#FF0000">*)<html:errors property="approval"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.approval.approval.notes"/></td>
                    <td class="Edit-Right-Text" colspan="3">
                        <html:textarea name="approvalInputForm" property="approvalNotes" cols="50" rows="5"
                                       styleClass="inptype" onkeyup="maxLengthTextArea(this, 100);"/>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%">&nbsp;</td>
                    <td width="50%" align="right">&nbsp;</td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:flyToPage('Update');"><img src="../../../images/button/ButtonSubmit.png"
                                                                       width="100" height="31" border="0"/></a>
                        <a href="javascript:flyToPage('Load');"><img src="../../../images/button/ButtonCancel.png"
                                                                     width="100" height="31" border="0"/></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>