<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.TITLE_APP_ADMIN; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>

        <script language="javascript">
            function movePage(taskID) {
                if (taskID == "Save" || taskID == "Update" || taskID == "SaveInput") {
                    if (document.forms[0].onsubmit == null || document.forms[0].onsubmit()) {
                        document.forms[0].task.value = taskID;
                        document.forms[0].submit();
                    }
                }
                else if (taskID == "Load") {
                    document.forms[0].task.value = taskID;
                    document.forms[0].submit();
                }
            }

            function setSurveyor(kode, nama) {
                findElement(document.forms[0], "surveyorId").value = kode;
                findElement(document.forms[0], "surveyorName").value = nama;
            }

            function setBranch() {
                findElement(document.forms[0], "surveyorId").value = '';
                findElement(document.forms[0], "surveyorName").value = '';
            }

            function openSurveyor(winName, features) {
                var e = findElement(document.forms[0], "branchId");
                var branchId = e.options[e.selectedIndex].value;
                if (branchId == "" || branchId == null)
                    alert("choose Branch first");
                else {
                    var toUrl = '<html:rewrite action="/feature/lookup/UserList"/>?task=Search&tipe=ONE&branchId=' + branchId + '&jobFilter=MAYOR';
                    window.open(toUrl, winName, features);
                }
            }
        </script>
    </head>

    <body>
    <html:form action="/feature/task/InputAssignment.do" method="POST">
        <html:hidden property="task"/>
        <logic:equal name="inputAssignmentForm" property="task" value="Edit" scope="request">
            <html:hidden property="mobileAssignmentId"/>
        </logic:equal>
        <html:hidden property="search.searchType1"/>
        <html:hidden property="search.searchValue1"/>
        <html:hidden property="search.status"/>
        <html:hidden property="search.scheme"/>
        <html:hidden property="search.branchId"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <%-- 		<%@include file="../../../Error.jsp"%> --%>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <bean:write name="inputAssignmentForm" property="title"/>
                        <bean:message key="app.assignment.title"/>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="headercol" colspan="4" align="center"><bean:message
                            key="app.assignment.customerdetail"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.nik"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="nik" styleClass="TextBox" size="25" maxlength="30"/>
                        <font color="#FF0000">*) <html:errors property="nik"/></font></td>
                    <td class="Edit-Left-Text"><bean:message key="common.name"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="customerName" styleClass="TextBox" size="35" maxlength="60"/>
                        <font color="#FF0000">*) <html:errors property="customerName"/></font></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.priority"/></td>
                    <td class="Edit-Right-Text-II" colspan="3">
                        <html:select property="priority" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
                            <logic:notEmpty name="comboPriority" scope="request">
                                <html:options collection="comboPriority" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                        <font color="#FF0000">*) <html:errors property="priority"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="headercol" colspan="4" align="center">
                        <bean:message key="app.assignment.phonetitle"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.customerphone"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="customerPhone" styleClass="TextBox" size="30" maxlength="20"/>
                        <font color="#FF0000"><html:errors property="customerPhone"/></font>
                    </td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.officephone"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="officePhone" styleClass="TextBox" size="30" maxlength="20"/>
                        <font color="#FF0000"><html:errors property="officePhone"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.cellularphone"/></td>
                    <td class="Edit-Right-Text-II" colspan="3">
                        <html:text property="customerHandPhone" styleClass="TextBox" size="30" maxlength="20"/>
                        <font color="#FF0000"><html:errors property="customerHandPhone"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="headercol" colspan="4" align="center">
                        <bean:message key="app.assignment.customeraddresstitle"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.customeraddress"/></td>
                    <td class="Edit-Right-Text-II" colspan="3">
                        <html:text property="addressStreet1" styleClass="TextBox" size="80" maxlength="80"/>
                        <font color="#FF0000">*) <html:errors property="customerAddress"/></font>
                        <br/><br/>
                        <html:text property="addressStreet2" styleClass="TextBox" size="80" maxlength="80"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.addressnumber"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="noaddr" styleClass="TextBox" size="10" maxlength="10"/>
                        <font color="#FF0000">*) <html:errors property="noaddr"/></font>
                    </td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.rt"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="rt" styleClass="TextBox" size="3" maxlength="3"/>&nbsp;/&nbsp;
                        <html:text property="rw" styleClass="TextBox" size="3" maxlength="3"/>
                        <font color="#FF0000"><html:errors property="rt"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.kelurahan"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="kelurahan" styleClass="TextBox" size="30" maxlength="30"/>
                    </td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.kecamatan"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="kecamatan" styleClass="TextBox" size="30" maxlength="30"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.kabupaten"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="kabupaten" styleClass="TextBox" size="30" maxlength="30"/>
                    </td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.city"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="city" styleClass="TextBox" size="30" maxlength="50"/>
                        <font color="#FF0000">*) <html:errors property="city"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.province"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="propinsi" styleClass="TextBox" size="30" maxlength="50"/>
                        <font color="#FF0000">*) <html:errors property="propinsi"/></font>
                    </td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.postcode"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="kodePos" styleClass="TextBox" size="5" maxlength="5"/>
                        <font color="#FF0000">*) <html:errors property="postcode"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.country"/></td>
                    <td class="Edit-Right-Text-II" colspan="3">
                        <html:text property="negara" styleClass="TextBox" size="20" maxlength="30" value="Indonesia"/>
                        <font color="#FF0000">*) <html:errors property="negara"/></font>
                    </td>
                </tr>
                <tr>
                    <!-- 				<img src="../../../images/icon/IconGoogle.gif" alt="GOOGLE" title="GOOGLE"> -->
                    <td class="Edit-Right-Text" colspan="4" align="left"><font color="#FF0000"><bean:message
                            key="app.assignment.coordinatemessage"/></font>
                        <div id="googleText"
                             style="text-decoration:none; font-family: 'Californian FB'; display: inline; font-size: large;">
                            <a href="http://www.maps.google.com" target="_blank"
                               title="Click to visit www.maps.google.com">
                                <font color="#186DEE">G</font><font color="#DB4632">o</font><font
                                    color="#FFBB07">o</font><font color="#186DEE">g</font><font color="#049F5C">l</font><font
                                    color="#D5402D">e</font>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.latitude"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="gpsLatitude" size="10" maxlength="10" styleClass="TextBox"/>
                        <bean:message key="app.assignment.coordinateformat"/>
                        <font color="#FF0000"><html:errors property="gpsLatitude"/></font>
                    </td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.longitude"/></td>
                    <td class="Edit-Right-Text-II">
                        <html:text property="gpsLongitude" size="10" maxlength="10" styleClass="TextBox"/>
                        <bean:message key="app.assignment.coordinateformat"/>
                        <font color="#FF0000"><html:errors property="gpsLongitude"/></font>
                    </td>
                </tr>
            </table>
            <br/>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="headercol" colspan="2" align="center"><bean:message key="app.assignment.picdetail"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="branchId" onchange="setBranch();" styleClass="TextBox">
                            <logic:notEmpty name="comboBranch" scope="request">
                                <html:options collection="comboBranch" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.assignto"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="surveyorName" styleClass="TextBox" size="30" maxlength="50"
                                   readonly="true"/>
                        <font color="#FF0000">*) <html:errors property="surveyorId"/></font>
                        <html:hidden property="surveyorId"/>
                        <a href="javascript:openSurveyor('Surveyor','scrollbars=yes,width=640,height=480,resizable=yes,top=0,left=0')">
                            <img src="../../../images/icon/IconLookUp.gif" width="16" height="16"
                                 alt="Choose Field Person" border="0" title="Lookup Field Person"></a>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.scheme"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="schemeId" styleClass="TextBox">
                            <logic:notEmpty name="comboActiveScheme" scope="request">
                                <html:options collection="comboActiveScheme" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                        <font color="#FF0000">*) <html:errors property="schemeId"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.notes"/></td>
                    <td class="Edit-Right-Text"><html:textarea property="notes"
                                                               onkeypress="maxLengthTextArea(this, 1000)" rows="2"
                                                               cols="50%" styleClass="TextBox"/></td>
                </tr>
            </table>
        </div>
        <table width="95%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="50%" height="30">&nbsp;</td>
                <td width="50%" align="right">
                    <% String action = ""; %>
                    <logic:equal value="Add" property="task" name="inputAssignmentForm">
                        <% action = Global.WEB_TASK_INSERT; %>
                    </logic:equal>
                    <logic:equal value="Edit" property="task" name="inputAssignmentForm">
                        <% action = Global.WEB_TASK_UPDATE; %>
                    </logic:equal>
                    <logic:equal name="inputAssignmentForm" property="task" value="Input" scope="request">
                        <% action = "SaveInput"; %>
                    </logic:equal>
                    <a href="javascript:movePage('<%=action%>');">
                        <img src="../../../images/button/ButtonSubmit.png" alt="Action <%=action %>"
                             title="<bean:message key="tooltip.action"/> <%=action %>" width="100" height="31"
                             border="0">
                    </a>
                    &nbsp;
                    <a href="javascript:movePage('Load');">
                        <img src="../../../images/button/ButtonCancel.png" alt="Cancel"
                             title="<bean:message key="tooltip.cancel"/>" width="100" height="31" border="0">
                    </a>
                </td>
            </tr>
        </table>
    </html:form>
    </body>
</html:html>