<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.TITLE_APP_ADMIN; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function checkAll() {
                var form = document.forms[0];
                iLength = form.pilih.length;
                if (iLength > 0) {
                    for (var i = 0; i < iLength; i++) {
                        form.pilih[i].checked = form.chekAll.checked;
                    }
                }
                else
                    form.pilih.checked = form.chekAll.checked;
            }

            function movePage(taskID, mobileAssignmentId) {
                if (taskID == 'Request') {
                    len = document.forms[0].elements.length;
                    var i = 0;
                    var found = false;
                    for (i = 0; i < len && !found; i++) {
                        if (document.forms[0].elements[i].name == "pilih") {
                            found = document.forms[0].elements[i].checked;
                        }
                    }
                    if (!found) {
                        alert("Minimal 1 data choosed");
                        return;
                    }
                }

                document.forms[0].mobileAssignmentId.value = mobileAssignmentId;
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

            function viewTaskDetail(mobileAssignmentId) {
                window.open('<html:rewrite action="/feature/view/Assignment"/>?task=Load&mobileAssignmentId=' + mobileAssignmentId,
                    'OPEN_DETAIL', 'height=600,width=800,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes,titlebar=no,location=no');
                return;
            }

            function moveToPage(mobileAssignmentId, thePlace) {
                var path;
                var curUri = '<bean:write name="inputAssignmentForm" property="currentUri"/>';
                path = '<%=request.getContextPath()%>';
                document.forms[0].task.value = 'Load';
                document.forms[0].mobileAssignmentId.value = mobileAssignmentId;
                if (thePlace == "Answers") {
                    findElement(document.forms[0], "paging.currentPageNo").value = 1;
                    findElement(document.forms[0], "paging.dispatch").value = null;
                    document.forms[0].action = path
                        + '/view/Assignment.do?previousUri='
                        + curUri;
                }
                document.forms[0].submit();
            }
        </script>


    <body>
    <html:form action="/feature/task/InputAssignment.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="mobileAssignmentId"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="app.assignment.title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.branchId" styleClass="TextBox">
                            <logic:notEmpty name="comboBranch" scope="request">
                                <html:options collection="comboBranch" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.privilege.form"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.scheme" styleClass="TextBox">
                            <html:option value="All"><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboScheme" scope="request">
                                <html:options collection="comboScheme" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.status"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.status" styleClass="TextBox">
                            <html:option value="All"><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboStatus" scope="request">
                                <html:options collection="comboStatus" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.priority"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.priority" styleClass="TextBox">
                            <html:option value="All"><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboPriority" scope="request">
                                <html:options collection="comboPriority" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text">
                        <bean:message key="app.assignment.filtering"/>
                        <html:select property="search.searchType1" styleClass="TextBox">
                            <html:option value="10"><bean:message key="app.assignment.customername"/></html:option>
                            <html:option value="20"><bean:message key="app.assignment.fieldpersonname"/></html:option>
                        </html:select>
                    </td>
                    <td class="Edit-Right-Text"><html:text property="search.searchValue1" size="25" maxlength="30"
                                                           styleClass="TextBox"/></td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search"
                                 title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <logic:notEqual name="inputAssignmentForm" property="task" value="Show">
                <table border="0" width="95%" cellpadding="3" cellspacing="0" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="10%" align="center"/>
                        <td width="75%" align="center"><bean:message key="app.assignment.titlelist"/></td>
                        <td width="10%" align="center"/>
                    </tr>
                </table>
                <div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;">
                    <table border="0" width="100%" cellpadding="3" cellspacing="1" class="tableview">
                        <tr align="center" class="headercol">
                            <td width="5%" rowspan="2" align="center"><input type="checkbox" name="chekAll"
                                                                             onclick="javascript:checkAll();"/></td>
                            <td width="5%" rowspan="2" align="center"><bean:message
                                    key="app.assignment.tasknumber"/></td>
                            <td width="10%" rowspan="2" align="center"><bean:message key="app.user.branch"/></td>
                            <td width="15%" rowspan="2" align="center"><bean:message
                                    key="feature.location.history.pic"/></td>
                            <td width="15%" rowspan="2" align="center"><bean:message
                                    key="app.assignment.customername"/></td>
                            <td width="10%" rowspan="2" align="center"><bean:message key="app.privilege.form"/></td>
                            <td width="10%" rowspan="2" align="center"><bean:message key="common.status"/></td>
                            <!-- 					      	<td width="10%" rowspan="2" align="center">PRIORITY</td> -->
                            <td width="10%" colspan="2" align="center"><bean:message key="common.action"/></td>
                        </tr>
                        <tr align="center" class="headercol">
                            <td width="5%" align="center"><bean:message key="app.assignment.view"/></td>
                            <td width="5%" align="center"><bean:message key="common.edit"/></td>
                        </tr>
                        <logic:notEmpty name="listAssignment" scope="request">
                            <logic:iterate id="data" name="listAssignment" scope="request" indexId="i">
                                <%
                                    String classColor = ((i % 2) != 1) ? "evalcol" : "oddcol";
                                %>
                                <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                    onmouseout="this.className='<%=classColor%>'">
                                    <td align="center">
                                        <html:multibox property="pilih">
                                            <bean:write name="data" property="mobileAssignmentId"/>
                                        </html:multibox>
                                    </td>
                                    <td align="right">
                                            <%-- 										<a href="javascript:viewTaskDetail('<bean:write name="data" property="mobileAssignmentId"/>')"> --%>
                                        <bean:write name="data" property="mobileAssignmentId"/>
                                        <!-- 				                        </a> -->
                                    </td>
                                    <td title="<bean:write name="data" property="branchId"/> - <bean:write name="data" property="branchName"/>">
                                        <bean:write name="data" property="branchName"/>
                                    </td>
                                    <td><bean:write name="data" property="surveyorName"/></td>
                                    <td><bean:write name="data" property="customerName"/></td>
                                    <td align="center" title="<bean:write name="data" property="schemeDescription"/>">
                                        <bean:write name="data" property="schemeId"/></td>
                                    <td align="center"><bean:write name="data" property="status"/></td>
                                        <%-- 									<td align="center"><bean:write name="data" property="priority"/></td> --%>
                                    <td align="center">
                                        <a href="javascript:viewTaskDetail('<bean:write name="data" property="mobileAssignmentId"/>')">
                                            <img src="../../../images/icon/IconView.gif" width="16" height="16"
                                                 border="0"/>
                                        </a>
                                    </td>
                                    <td align="center">
                                        <a title="Edit"
                                           href="javascript:movePage('Edit','<bean:write name="data" property="mobileAssignmentId"/>');">
                                            <img src="../../../images/icon/IconEdit.gif" width="16" height="16"
                                                 border="0"/>
                                        </a>
                                    </td>
                                </tr>
                            </logic:iterate>
                        </logic:notEmpty>
                        <logic:empty name="listAssignment" scope="request">
                            <tr class="evalcol">
                                <td bgcolor="FFFFFF" colspan="9" align="center"><font
                                        class="errMsg"><bean:message key="common.listnotfound"/></font></td>
                            </tr>
                        </logic:empty>
                    </table>
                </div>
                <logic:notEmpty name="listAssignment" scope="request">
                    <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.inputAssignmentForm,'First')"
                                           title="First">
                                    <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.inputAssignmentForm,'Prev')"
                                           title="Previous">
                                    <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="200" align="center"><bean:message key="paging.page"/>
                                <html:text name="inputAssignmentForm" property="paging.currentPageNo" size="3"
                                           maxlength="6" styleClass="TextBox"/> <bean:message key="paging.of"/>
                                <html:hidden property="paging.totalPage" write="true"/>
                                <html:hidden property="paging.totalRecord" write="false"/>
                                <a href="javascript:moveToPageNumber(document.inputAssignmentForm,'Go');">
                                    <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                         border="0"/>
                                </a>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.inputAssignmentForm,'Next')"
                                           title="Next">
                                    <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.inputAssignmentForm,'Last')"
                                           title="Last">
                                    <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td align="right">
                                <bean:define id="currentPage" name="inputAssignmentForm" property="paging.currentPageNo"
                                             type="java.lang.Integer"/>
                                <bean:define id="total" name="inputAssignmentForm" property="paging.totalRecord"
                                             type="java.lang.Integer"/>
                                <bean:define id="perpage" name="inputAssignmentForm" property="paging.rowPerPage"
                                             type="java.lang.Integer"/>
                                <%
                                    int startShow = 1;
                                    int endShow = 0;
                                    if (currentPage > 1) {
                                        int tempstart = ((currentPage - 1) * perpage) + 1;
                                        int tempend = currentPage * perpage;
                                        if (total < tempend) {
                                            tempend = total;
                                        }
                                        startShow = tempstart;
                                        endShow = tempend;
                                    } else {
                                        endShow = total;
                                    }
                                %>
                                <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                    key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                                <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                    key="paging.showing.record"/>
                            </td>
                        </tr>
                    </table>
                </logic:notEmpty>
                <table width="95%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" height="30">&nbsp;</td>
                        <td width="50%" align="right">
                            <a href="javascript:movePage('Request','')"><img
                                    src="../../../images/button/ButtonDelete.png" width="100" height="31"
                                    border="0"/></a>
                            <a href="javascript:movePage('Add','')"><img src="../../../images/button/ButtonAdd.png"
                                                                         width="100" height="31" border="0"/></a>
                        </td>
                    </tr>
                </table>
            </logic:notEqual>
        </div>
    </html:form>
    </body>
</html:html>

