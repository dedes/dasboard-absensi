<%@page import="java.util.List" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- <% String title = Global.TITLE_APPROVAL_NASABAH; %> --%>
<html:html>
    <html:base/>
    <head>
        <title><bean:message key="feature.approval.assignment"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <html:javascript formName="parameterForm" method="validateForm" staticJavascript="false" page="0"/>

        <script language="javascript">
            <logic:present name="list">
            function checkedAll() {
                <% int size = ((List)request.getAttribute("list")).size(); %>
                var sizeList = <%=size%>;
                var member = document.getElementsByName("search.memberList");
                if (sizeList > 1) {
                    for (i = 0; i < member.length; i++)
                        with (member[i])
                            checked = document.approvalAssignmentForm.checkAll.checked;
                } else {
                    with (member[0])
                        checked = document.approvalAssignmentForm.checkAll.checked;
                }
            }
            </logic:present>

            function action(task, idParam) {
                if (task == "Reject" || task == "Approve") {
                    if (!beforeSubmit(task))
                        return;
                } else if (task == "Single") {
                    document.approvalAssignmentForm.id.value = idParam;
                }
                document.approvalAssignmentForm.task.value = task;
                document.approvalAssignmentForm.submit();
            }

            function beforeSubmit(task) {
                var count = 0;
                var ret = true;
                var member = document.getElementsByName("search.memberList");
                with (document.approvalAssignmentForm) {
                    if (member.length > 1) {
                        for (var i = 0; i < member.length; i++) {
                            if (member[i].checked) {
                                count++;
                                if (i == 0) {
                                    checkedMemberList.value = member[i].value;
                                } else {
                                    checkedMemberList.value = checkedMemberList.value + ',' + member[i].value;
                                }
                            }
                        }
                    } else if (member.length == 1) {
                        if (member[0].checked) {
                            count++;
                            checkedMemberList.value = member.value;
                        }
                    }
                    if (count == 0) {
                        alert('<bean:message key="common.minimalHarusPilih" arg0="Data"/>');
                        ret = false;
                    }
                    if (ret) {
                        if (task == "Reject") {
                            if (!confirm('<bean:message key="common.confirmreject"/>'))
                                ret = false;
                        } else if (task == "Approve") {
                            if (!confirm('<bean:message key="common.confirmapprove"/>'))
                                ret = false;
                        }
                    }
                }
                return ret;
            }

            function movePage(task) {
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }

            function viewTaskDetail(mobileAssignmentId) {
                window.open('<html:rewrite action="/feature/view/Assignment"/>?task=View&mobileAssignmentId=' + mobileAssignmentId,
                    'OPEN_DETAIL', 'height=600,width=800,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes,titlebar=no,location=no');
                return;
            }

            function moveToPage(mobileAssignmentId, thePlace) {
                var path;
                var curUri = '<bean:write name="approvalAssignmentForm" property="currentUri"/>';
                path = '<%=request.getContextPath()%>';
                document.forms[0].task.value = 'Load';
                document.forms[0].mobileAssignmentId.value = mobileAssignmentId;
                if (thePlace == "Answers") {
                    findElement(document.forms[0], "paging.currentPageNo").value = 1;
                    findElement(document.forms[0], "paging.dispatch").value = null;
                    document.forms[0].action = path
                        + '/view/Assignment.do?previousUri='
                        + curUri;
                }
                document.forms[0].submit();
            }

        </script>

        <%@include file="../../../../Error.jsp" %>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/feature/task/ApprovalAssignment.do" method="POST">
        <html:hidden property="checkedMemberList" value=""/>
        <html:hidden property="task" value="Load"/>
        <html:hidden property="mobileAssignmentId"/>
        <html:hidden property="search.schemeRole"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <logic:equal name="approvalAssignmentForm" property="search.schemeRole" value="CF"><bean:message
                                key="feature.collection"/></logic:equal>
                        <logic:notEqual name="approvalAssignmentForm" property="search.schemeRole"
                                        value="CF"><bean:message key="feature.survey"/></logic:notEqual>
                        <bean:message key="feature.approval.assignment"/>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.privilege.form"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.schemeId" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboScheme" scope="request">
                                <html:options collection="comboScheme" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.location.history.pic"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.surveyorId" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <logic:notEmpty name="comboPIC" scope="request">
                                <html:options collection="comboPIC" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.approval.customer.name"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.customerName" styleClass="TextBox" size="30" maxlength="130"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.sort"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.orderField" styleClass="TextBox">
                            <html:option value="10"><bean:message
                                    key="feature.assignment.application.no"/></html:option>
                            <html:option value="20"><bean:message key="app.privilege.form"/></html:option>
                            <html:option value="30"><bean:message key="app.assignment.scheme"/></html:option>
                            <html:option value="40"><bean:message key="app.user.branch"/></html:option>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.order"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="search.orderby" styleClass="TextBox">
                            <html:option value="10"><bean:message key="common.ascending"/></html:option>
                            <html:option value="20"><bean:message key="common.descending"/></html:option>
                        </html:select>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:action('Search');">
                            <img src="../../../images/button/ButtonSearch.png" alt="cancel"
                                 title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;">
                <table width="150%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="3%" align="center">
                            <logic:notEmpty name="list" scope="request">
                                <input type="checkbox" value="ON" name="checkAll" onclick="javascript:checkedAll()">
                            </logic:notEmpty>
                        </td>
                        <td width="10%" align="center"><bean:message key="feature.assignment.application.no"/></td>
                        <td width="7%" align="center"><bean:message key="app.user.branch"/></td>
                        <td width="10%" align="center"><bean:message key="feature.location.history.pic"/></td>
                        <td width="5%" align="center"><bean:message key="app.privilege.form"/></td>
                        <td width="10%" align="center"><bean:message key="feature.approval.customer.name"/></td>
                        <td width="50%" align="center"><bean:message key="app.assignment.customeraddress"/></td>
                        <td width="40%" align="center"><bean:message key="app.assignment.notes"/></td>
                        <td width="5%" align="center"><bean:message key="app.assignment.view"/></td>
                    </tr>
                    <logic:notEmpty name="list" scope="request">
                        <logic:iterate id="listParam" name="list" scope="request" indexId="index">
                            <%
                                String color = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                            %>
                            <tr class="<%=color%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=color%>'">
                                <td align="center">
                                    <html:multibox property="search.memberList">
                                        <bean:write name="listParam" property="mobileAssignmentId"/>
                                    </html:multibox>
                                </td>
                                <td align="center"><bean:write name="listParam" property="applNo"/></td>
                                <td align="center"><bean:write name="listParam" property="branchName"/></td>
                                <td align="center"><bean:write name="listParam" property="surveyorName"/></td>
                                <td align="center"><bean:write name="listParam" property="schemeId"/></td>
                                <td align="center"><bean:write name="listParam" property="customerName"/></td>
                                <td align="left"><bean:write name="listParam" property="customerAddress"/></td>
                                <td align="left"><bean:write name="listParam" property="notes"/></td>
                                <td align="center">
                                    <a href="javascript:viewTaskDetail('<bean:write name="listParam" property="mobileAssignmentId"/>')">
                                        <img src="../../../images/icon/IconView.gif" width="16" height="16" border="0"/>
                                    </a>
                                </td>
                            </tr>
                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="list" scope="request">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="11" align="center"><font class="errMsg"><bean:message
                                    key="common.listnotfound"/></font></td>
                        </tr>
                    </logic:empty>
                </table>
            </div>
            <logic:notEmpty name="list" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.approvalAssignmentForm,'First')"
                                       title="First">
                                <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.approvalAssignmentForm,'Prev')"
                                       title="Previous">
                                <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="approvalAssignmentForm" property="paging.currentPageNo" size="3"
                                       maxlength="6" styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <html:hidden property="paging.totalRecord" write="false"/>
                            <a href="javascript:moveToPageNumber(document.approvalAssignmentForm,'Go');">
                                <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                     border="0"/>
                            </a>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.approvalAssignmentForm,'Next')"
                                       title="Next">
                                <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.approvalAssignmentForm,'Last')"
                                       title="Last">
                                <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:define id="currentPage" name="approvalAssignmentForm" property="paging.currentPageNo"
                                         type="java.lang.Integer"/>
                            <bean:define id="total" name="approvalAssignmentForm" property="paging.totalRecord"
                                         type="java.lang.Integer"/>
                            <bean:define id="perpage" name="approvalAssignmentForm" property="paging.rowPerPage"
                                         type="java.lang.Integer"/>
                            <%
                                int startShow = 1;
                                int endShow = 0;
                                if (currentPage > 1) {
                                    int tempstart = ((currentPage - 1) * perpage) + 1;
                                    int tempend = currentPage * perpage;
                                    if (total < tempend) {
                                        tempend = total;
                                    }
                                    startShow = tempstart;
                                    endShow = tempend;
                                } else {
                                    endShow = total;
                                }
                            %>
                            <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                            <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
            <logic:notEmpty name="list" scope="request">
                <br/>
                <table width="95%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" height="30">&nbsp;</td>
                        <td width="50%" align="right">
                            <a href="javascript:action('Approve');">
                                <img src="../../../images/button/ButtonApprove.png" alt="Approve"
                                     title="<bean:message key="tooltip.button.app"/>" width="100" height="31"
                                     border="0">
                            </a>&nbsp;&nbsp;
                            <a href="javascript:action('Reject');">
                                <img src="../../../images/button/ButtonReject.png" alt="Reject"
                                     title="<bean:message key="tooltip.button.reject"/>" width="100" height="31"
                                     border="0">
                            </a>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
        </div>
    </html:form>
    </body>
</html:html>