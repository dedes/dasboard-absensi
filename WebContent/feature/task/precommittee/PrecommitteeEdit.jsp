<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>

    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            var listSize = <bean:write name="precommitteeForm" property="listSize"/>
                function movePage(task) {
                    var form = document.forms[0];

                    if (task == 'Save') {
                        for (var i = 0; i < listSize; i++) {
                            if (!($('#radOld' + i).is(':checked') || $('#radNew' + i).is(':checked'))) {
                                alert('<bean:message key="precommittee.validate"/>');
                                return;
                            }
                        }
                    }
                    form.task.value = task;
                    form.submit();
                }
        </script>
        <script>
            $(document).ready(function () {
                $('[name=checkAllOld]').change(function () {
                    if ($(this).val() == 1) {
                        for (var i = 0; i < listSize; i++) {
                            $('#radOld' + i).attr('checked', 'checked');
                            $('#radNew' + i).removeAttr('checked');
                            $('#fin' + i).html($('#old' + i).html());
                        }
                    }
                    else {
                        for (var i = 0; i < listSize; i++) {
                            $('#radNew' + i).attr('checked', 'checked');
                            $('#radOld' + i).removeAttr('checked');
                            $('#fin' + i).html($('#new' + i).html());
                        }
                    }
                });
            })

            function setFinal(from, idx) {
                from = from.concat(idx);
                var id = 'fin' + idx;
                $('#' + id).html($('#' + from).html());
            }
        </script>
    </head>

    <body>
    <html:form action="/feature/task/PreCommittee">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="mobileAssignmentId"/>
        <html:hidden property="search.branch"/>
        <html:hidden property="search.searchType1"/>
        <html:hidden property="search.searchValue1"/>
        <html:hidden property="search.searchType2"/>
        <html:hidden property="search.tglStart"/>
        <html:hidden property="search.tglEnd"/>
        <html:hidden property="search.scheme"/>
        <html:hidden property="search.priority"/>
        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.precommittee.assignment"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="headercol" colspan="4" align="center"><bean:message
                            key="feature.precommittee.assignment.detail"/> [<bean:write name="precommitteeForm"
                                                                                        property="mobileAssignmentId"/>]
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.answer.assignment.date"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="assignmentDate"/></td>
                    <td class="Edit-Left-Text"><bean:message key="feature.precommittee.read.date"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="retrieveDate"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.precommittee.submit.date"/></td>
                    <td class="Edit-Right-Text-II" colspan="1"><bean:write name="precommitteeForm"
                                                                           property="submitDate"/></td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.priority"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm"
                                                               property="priorityDescription"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.notes"/></td>
                    <td class="Edit-Right-Text-II" colspan="3"><bean:write name="precommitteeForm"
                                                                           property="notes"/></td>
                </tr>
                <tr>
                    <td class="headercol" colspan="4" align="center"><bean:message
                            key="app.assignment.customerdetail"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.nik"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="nik"/></td>
                    <td class="Edit-Left-Text"><bean:message key="common.name"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="customerName"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.customerphone"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="customerPhone"/></td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.officephone"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="officePhone"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.cellularphone"/></td>
                    <td class="Edit-Right-Text-II" colspan="3"><bean:write name="precommitteeForm"
                                                                           property="customerHandPhone"/></td>
                </tr>
                <tr>
                    <td class="headercol" colspan="4" align="center"><bean:message
                            key="app.assignment.customeraddresstitle"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.customeraddress"/></td>
                    <td class="Edit-Right-Text-II" colspan="3">
                        <bean:write name="precommitteeForm" property="addressStreet1"/><br/><br/>
                        <bean:write name="precommitteeForm" property="addressStreet2"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.number"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="noAddr"/></td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.rt"/></td>
                    <td class="Edit-Right-Text-II">
                        <bean:write name="precommitteeForm" property="rt"/>&nbsp;/&nbsp;
                        <bean:write name="precommitteeForm" property="rw"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.kelurahan"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="kelurahan"/></td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.kecamatan"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="kecamatan"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.kabupaten"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="kabupaten"/></td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.city"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="city"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.province"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="propinsi"/></td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.postcode"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="kodePos"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.country"/></td>
                    <td class="Edit-Right-Text-II" colspan="3"><bean:write name="precommitteeForm"
                                                                           property="negara"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.latitude"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="gpsLatitude"/></td>
                    <td class="Edit-Left-Text"><bean:message key="app.assignment.longitude"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="gpsLongitude"/></td>
                </tr>
                <tr>
                    <td class="headercol" colspan="2" align="center"><bean:message key="app.assignment.picdetail"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.branch"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="branchId"/> -
                        <bean:write name="precommitteeForm" property="branchName"/></td>
                    <td class="Edit-Left-Text"><bean:message key="feature.location.history.pic"/></td>
                    <td class="Edit-Right-Text-II"><bean:write name="precommitteeForm" property="surveyorName"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.privilege.form"/></td>
                    <td class="Edit-Right-Text-II" colspan="3"><html:hidden name="precommitteeForm" property="schemeId"
                                                                            write="true"/>-<bean:write
                            name="precommitteeForm" property="schemeDescription"/></td>
                </tr>
                <tr>
                    <td class="headercol" colspan="2" align="center"><bean:message key="common.assignment"/></td>
                </tr>
            </table>
            <table border="0" width="95%" cellpadding="3" cellspacing="0" class="tableview">
                <tr align="center" class="headercol">
                    <td width="10%" align="center"/>
                    <td width="75%" align="center"><bean:message key="feature.precommittee.assignment.answer"/></td>
                    <td width="10%" align="center"/>
                </tr>
            </table>
            <table border="0" width="100%" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="15%" align="center"><bean:message key="common.label"/></td>
                    <td width="20%"><bean:message key="feature.precommittee.old.answer"/></td>
                    <td width="20%"><bean:message key="feature.precommittee.new.answer"/></td>
                    <td width="20%"><bean:message key="feature.precommittee.final.answer ="/></td>
                    <td width="5%" align="center">
                        <logic:notEmpty name="precommitteeForm" property="listAnswer" scope="request">
                            <bean:message key="feature.precommittee.old"/><br/><bean:message
                                key="common.answer"/><br/><input type="radio" name="checkAllOld" value="1"/>
                        </logic:notEmpty>
                    </td>
                    <td width="5%" align="center">
                        <logic:notEmpty name="precommitteeForm" property="listAnswer" scope="request">
                            <bean:message key="feature.precommittee.new"/><br/><bean:message
                                key="common.answer"/><br/><input type="radio" name="checkAllOld" value="0"/>
                        </logic:notEmpty>
                    </td>
                </tr>
                <logic:notEmpty name="precommitteeForm" property="listAnswer" scope="request">
                    <logic:iterate id="data" name="precommitteeForm" property="listAnswer" indexId="idx">
                        <% String color = (idx.intValue() % 2 == 0) ? "evalcol" : "oddcol"; %>
                        <tr class="<%=color%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=color%>'">
                            <td>
                                <bean:write name="data" property="questionLabel"/>
                                <html:hidden name="data" property="mobileAssignmentId" indexed="true"/>
                                <html:hidden name="data" property="answerType" indexed="true"/>
                                <html:hidden name="data" property="questionGroupId" indexed="true"/>
                                <html:hidden name="data" property="questionId" indexed="true"/>
                                <html:hidden name="data" property="optionAnswerId" indexed="true"/>
                                <html:hidden name="data" property="optionAnswerLabel" indexed="true"/>
                                <html:hidden name="data" property="textAnswer" indexed="true"/>
                                <html:hidden name="data" property="lookupId" indexed="true"/>
                                <html:hidden name="data" property="oldOptionAnswerId" indexed="true"/>
                                <html:hidden name="data" property="oldOptionAnswerLabel" indexed="true"/>
                                <html:hidden name="data" property="oldTextAnswer" indexed="true"/>
                                <html:hidden name="data" property="oldLookupId" indexed="true"/>
                                <html:hidden name="data" property="isOldOption" indexed="true"/>
                            </td>
                            <td id="old<%=idx.intValue()%>" <logic:equal value="0" name="data"
                                                                         property="isSameAnswer">class="tdtopimerah"</logic:equal>>
                                <logic:notEmpty name="data" property="oldOptionAnswerLabel">
                                    [ <bean:write name="data" property="oldOptionAnswerLabel"/> ]
                                </logic:notEmpty>
                                <bean:write name="data" property="oldTextAnswer"/>
                            </td>
                            <td id="new<%=idx.intValue()%>" <logic:equal value="0" name="data"
                                                                         property="isSameAnswer">class="tdtopimerah"</logic:equal>>
                                <logic:equal value="1" name="data" property="isImage">
                                    <logic:equal value="1" name="data" property="hasImage">
                                        <a href="<%=request.getContextPath()%>/inquiry/image?mid=<bean:write name="data" property="mobileAssignmentId"/>&gid=<bean:write name="data" property="questionGroupId"/>&qid=<bean:write name="data" property="questionId"/>"
                                           target="_blank">
                                            <img src="<%=request.getContextPath()%>/inquiry/image?mid=<bean:write name="data" property="mobileAssignmentId"/>&gid=<bean:write name="data" property="questionGroupId"/>&qid=<bean:write name="data" property="questionId"/>"
                                                 width="160" height="120"/>
                                        </a>
                                        <logic:notEmpty name="data" property="gpsLatitude">
                                            <a href="javascript:movePageMap('Map','<bean:write name="data" property="gpsLatitude"/>','<bean:write name="data" property="gpsLongitude"/>','<bean:write name="data" property="accuracy"/>','<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>');">
                                                <img src="http://maps.google.com/maps/api/staticmap?center=<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>&zoom=12&size=160x120&sensor=false&markers=color:red|<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>">
                                            </a>
                                        </logic:notEmpty>
                                    </logic:equal>
                                    <logic:equal value="0" name="data" property="hasImage">
                                        <img src="../../../images/gif/NoImageAvailable.gif" width="160" height="120"/>
                                    </logic:equal>
                                </logic:equal>
                                <logic:notEqual value="1" name="data" property="isImage">
                                    <logic:notEmpty name="data" property="optionAnswerLabel">
                                        [ <bean:write name="data" property="optionAnswerLabel"/> ]
                                    </logic:notEmpty><bean:write name="data" property="textAnswer"/></logic:notEqual>
                            </td>
                            <td id="fin<%=idx.intValue()%>"></td>
                            <td align="center">
                                <input type="radio" id="radOld<%=idx.intValue()%>"
                                       name="data[<%=idx.intValue()%>].isUsingOldAnswer" value="1"
                                       onchange="setFinal('old',<%=idx.intValue()%>);"/></td>
                            <td align="center">
                                <input type="radio" id="radNew<%=idx.intValue()%>"
                                       name="data[<%=idx.intValue()%>].isUsingOldAnswer" value="0"
                                       onchange="setFinal('new',<%=idx.intValue()%>);"/></td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="precommitteeForm" property="listAnswer" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="10" align="center"><font class="errMsg">
                            <bean:message key="common.listnotfound"/></font>
                        </td>
                    </tr>
                </logic:empty>
            </table>

            <table width="95%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%" height="30">
                        &nbsp;
                    </td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Save');">
                            <html:img src="../../../images/button/ButtonSubmit.png" width="100" height="31" border="0"/>
                        </a>
                        <a href="javascript:movePage('Load');">
                            <html:img src="../../../images/button/ButtonBack.png" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>
