<%@page import="com.google.common.base.Strings" %>
<%@page import="treemas.application.feature.task.answerinput.list.FeatQuestion" %>
<%@page import="treemas.application.feature.task.answerinput.list.FeatQuestionAnswer" %>
<%@page import="treemas.util.constant.AnswerType" %>
<%@page import="treemas.util.constant.PropertiesKey" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<% String mapKey = ConfigurationProperties.getInstance().get(PropertiesKey.WEB_STATIC_KEY); %>
<%
    Map mapAnswer = (Map) request.getAttribute("mapper");
    if (null == mapAnswer) {
        mapAnswer = new HashMap();
    }
%>
<html:html>
    <html:base/>

    <head>
        <title><bean:message key="tittle.assignment.input.answer"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            $.fn.serializeObject = function () {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function () {
                    if (this.name !== 'jsonAnswer' && this.name !== 'map') {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || null;
                        }
                    }
                });
                return o;
            };

            function movePage(task, page) {
                var form = document.forms[0];
                form.task.value = task;
                if (page != 0)
                    form.nextGroup.value = page;
                if (null == form.jsonAnswer.value || undefined == form.jsonAnswer.value)
                    form.jsonAnswer.value = '';
                form.jsonAnswer.value = JSON.stringify($('form').serializeObject());
                form.submit();
            }

            function showImage(questionIdKey, event) {
                if (window.File && window.FileList && window.FileReader) {
                    var filesInput = document.getElementById(questionIdKey);
                    var inputHidden = document.getElementById("hidden" + questionIdKey);
//             filesInput.addEventListener("change", function(event){
                    var files = event.target.files; //FileList object
                    var output = document.getElementById("result" + questionIdKey);
                    for (var i = 0; i < files.length; i++) {
                        var file = files[i];
                        if (!file.type.match('image')) continue;
                        var picReader = new FileReader();
                        picReader.addEventListener("load", function (event) {
                            var picFile = event.target;
                            var div = document.createElement("div");
                            var divEx = document.getElementById("image" + questionIdKey);
                            if (null != divEx || undefined != divEx) {
                                divEx.innerHtml = "";
                                div = divEx;
                            }
                            div.innerHTML = "<img width='300' height='300' src='" + picFile.result + "'" + "title='" + picFile.name + "'/>";
                            inputHidden.value = picFile.result;
                            divEx = div;
//                         output.insertBefore(div,null);      
                        });
                        picReader.readAsDataURL(file);
                    }
//             });
                }
            }

            function toggleText(idMulti, idText) {
                var multiple = document.getElementById(idMulti);//.disabled = false
                var texter = document.getElementById(idText);

                if (multiple.checked) {
                    texter.disabled = false;
                } else {
                    texter.value = '';
                    texter.disabled = true;
                }
            }

            function geoFindMe(idout, idintext) {
                var output = document.getElementById(idout);
                var elmText = document.getElementById(idintext);

                if (!navigator.geolocation) {
                    output.innerHTML = "<p>Geolocation is not supported by your browser</p>";
                    return;
                }

                function success(position) {
                    var latitude = position.coords.latitude;
                    var longitude = position.coords.longitude;

                    output.innerHTML = '<p>Latitude:' + latitude + '<br>Longitude:' + longitude + '</p>';
                    elmText.value = 'LAT' + latitude + 'LGT' + longitude;
                    var img = new Image();
                    img.src = "https://maps.google.com/maps/api/staticmap?key=<%=mapKey%>&center=" + latitude + "," + longitude + "&zoom=12&size=160x120&sensor=false&markers=color:red|" + latitude + "," + longitude;
                    output.appendChild(img);
                };

                function error() {
                    output.innerHTML = "Unable to retrieve your location";
                };

                output.innerHTML = "....locating....";

                navigator.geolocation.getCurrentPosition(success, error);
            }
        </script>
    </head>

    <body>
    <html:form action="/feature/task/InputAnswerAssignment.do" method="POST">
        <html:hidden property="task"/>
        <html:hidden property="schemeId"/>

        <div align="center">
            <%@include file="../../../Error.jsp" %>
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="app.assignment.scheme"/> <bean:write
                            name="answerSurveyAssignmentForm" property="schemeId"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.group"/></td>
                    <td class="Edit-Right-Text"><strong><bean:write name="answerSurveyAssignmentForm"
                                                                    property="featQuestionGroup.questionGroupName"/></strong>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="5%" align="center">&nbsp;</td>
                    <td width="30%" align="center"><bean:message key="feature.question"/></td>
                    <td width="60%" align="center"><bean:message key="common.answer"/></td>
                </tr>
                <logic:notEmpty name="answerSurveyAssignmentForm" property="featQuestionGroup.questionGroupSet">
                    <bean:define id="groupId" name="answerSurveyAssignmentForm"
                                 property="featQuestionGroup.questionGroupId" type="java.lang.Integer"/>
                    <logic:iterate id="data" name="answerSurveyAssignmentForm"
                                   property="featQuestionGroup.questionGroupSet" indexId="idx">
                        <% String classColor = (idx.intValue() % 2 == 0) ? "evalcol" : "oddcol";%>
                        <tr class="<%=classColor%>">
                            <td>
                                <% String name = idx + "_";
                                    out.print(idx.intValue() + 1 + ".");
                                %>
                            </td>
                            <td>
                                <bean:write name="data" property="questionLabel"/>
                                <logic:equal name="data" property="isMandatory" value="1">
                                    <font color="#FF0000" title="MandatoryField">*)</font>
                                </logic:equal>
                            </td>
                            <td>
                                <bean:define id="answType" name="data" property="answerType" type="java.lang.String"/>
                                <%
                                    Integer maxlength = ((FeatQuestion) data).getMaxLength();
                                    String len = null != maxlength ? "maxlength='" + maxlength + "' size='" + maxlength + "'" : "";
                                    String questionIdKey = "_" + answType + "#" + groupId + "#" + ((FeatQuestion) data).getQuestionId() + "_";
                                    String questionIdTextKey = "_" + answType + "#" + groupId + "#" + ((FeatQuestion) data).getQuestionId() + "text_";
                                    Object answerText = null == mapAnswer.get(questionIdKey) ? "" : mapAnswer.get(questionIdKey);
                                    Object answerTextField = null == mapAnswer.get(questionIdTextKey) ? "" : mapAnswer.get(questionIdTextKey);
                                    if (AnswerType.TEXT.equals(answType) || AnswerType.RESPONDEN.equals(answType) || AnswerType.TRESHOLD.equals(answType)
                                            || AnswerType.NUMERIC.equals(answType) || AnswerType.DECIMAL.equals(answType) || AnswerType.ACCOUNTING.equals(answType)) {
                                        String returnFormat = "";
                                        if (AnswerType.NUMERIC.equals(answType) || AnswerType.ACCOUNTING.equals(answType)) {
                                            returnFormat = "onkeydown='javascript:return numericInput(event);'";
                                        } else if (AnswerType.DECIMAL.equals(answType) || AnswerType.TRESHOLD.equals(answType)) {
                                            returnFormat = "onkeydown='javascript:return numericInputKoma(event);'";
                                        }
                                %>
                                <input type="text" class="TextBox"
                                       name='<%=questionIdKey %>' <%=len + " " + returnFormat %>
                                       value="<%=answerText%>"/>
                                <% } else if (AnswerType.IMAGE_WITH_TIME.equals(answType)
                                        || AnswerType.IMAGE.equals(answType) || AnswerType.DRAWING.equals(answType) || AnswerType.SIGNATURE.equals(answType)
                                        || AnswerType.GEODATA_TIME.equals(answType) || AnswerType.IMAGE_WITH_GEODATA_TIME.equals(answType) || AnswerType.IMAGE_WITH_GEODATA.equals(answType)
                                        || AnswerType.GPS_TIME.equals(answType) || AnswerType.IMAGE_WITH_GPS_TIME.equals(answType) || AnswerType.IMAGE_WITH_GEODATA_GPS.equals(answType)) {
                                    if (AnswerType.IMAGE_WITH_TIME.equals(answType) || AnswerType.IMAGE.equals(answType)
                                            || AnswerType.IMAGE_WITH_GEODATA_TIME.equals(answType) || AnswerType.IMAGE_WITH_GEODATA.equals(answType)
                                            || AnswerType.IMAGE_WITH_GPS_TIME.equals(answType) || AnswerType.IMAGE_WITH_GEODATA_GPS.equals(answType)) {%>
                                <input id="<%=questionIdKey %>" type="file" accept=".jpg"
                                       onchange="javascript:showImage('<%=questionIdKey%>',event)"/>
                                <label id="result<%=questionIdKey %>"></label>
                                <input type="hidden" name="<%=questionIdKey%>" id="hidden<%=questionIdKey%>"
                                       value="<%=answerText%>"/>
                                <div id='image<%=questionIdKey%>'>
                                    <%
                                        if (!Strings.isNullOrEmpty(String.valueOf(answerText))) {
                                            out.print("<img width='300' height='300' src='" + answerText + "'/>");
                                        }
                                    %>
                                </div>
                                <br/>
                                <% }
                                    if (AnswerType.IMAGE_WITH_GPS_TIME.equals(answType) || AnswerType.IMAGE_WITH_GEODATA_GPS.equals(answType)
                                            || AnswerType.IMAGE_WITH_GEODATA_TIME.equals(answType) || AnswerType.IMAGE_WITH_GEODATA.equals(answType)) { %>
                                <div id="<%=questionIdTextKey%>"></div>
                                <script type="text/javascript">$(function () {
                                    geoFindMe('<%=questionIdTextKey%>', 'id<%=questionIdTextKey%>');
                                });</script>
                                <input type="hidden" name="<%=questionIdTextKey%>" id="id<%=questionIdTextKey%>"/>
                                <%
                                    }
                                    if (AnswerType.GPS_TIME.equals(answType) || AnswerType.GEODATA_TIME.equals(answType)) {
                                %>
                                <div id="<%=questionIdKey%>"></div>
                                <script type="text/javascript">$(function () {
                                    geoFindMe('<%=questionIdKey%>', 'id<%=questionIdKey%>');
                                });</script>
                                <input type="hidden" name="<%=questionIdKey%>" id="id<%=questionIdKey%>"/>
                                <!-- 								<script type="text/javascript"> -->
                                <!-- // 								$(function(){ -->
                                <!-- //							$("input[name*='<%=questionIdTextKey%>']").timepicker({-->
                                <!-- // 										timeFormat: 'hhmmss', -->
                                <!-- // 									    showButtonPanel : true, -->
                                <!-- // 										showOn : "both" -->
                                <!-- // 									}); -->
                                <!-- // 								}); -->
                                <!-- 							</script> -->
                                    <%-- 							<input type="text" class="TextBox" name="<%=questionIdTextKey%>" size="8" maxlength="8" id="<%=questionIdTextKey%>"  value="<%=answerTextField%>"/>					 --%>
                                <br/><input type="text" class="TextBox" name="<%=questionIdTextKey%>" size="4"
                                            maxlength="4" id="<%=questionIdTextKey%>"
                                            value="<%=answerTextField%>"/><bean:message
                                    key="feature.location.history.format.hhmm"/>
                                <%}%>
                                <% } else if (AnswerType.DATE.equals(answType) || AnswerType.DATE_TIME.equals(answType) || AnswerType.TIME.equals(answType)) {
                                    if (AnswerType.DATE.equals(answType) || AnswerType.DATE_TIME.equals(answType)) { %>
                                <script type="text/javascript">
                                    $(function () {
                                        $("input[name*='<%=questionIdKey%>']").datepicker({
                                            maxDate: "+0M +0D",
                                            dateFormat: "ddmmyy",
                                            changeMonth: true,
                                            changeYear: true,
                                            showButtonPanel: true,
                                            showOn: "both"
                                        });
                                    });
                                </script>
                                <input type="text" class="TextBox" name="<%=questionIdKey%>" size="8" maxlength="8"
                                       id="<%=questionIdKey%>" value="<%=answerText%>"/>
                                <% }
                                    if (AnswerType.TIME.equals(answType) || AnswerType.DATE_TIME.equals(answType)) { %>
                                <!-- 							<script type="text/javascript"> -->
                                <!-- // 								$(function(){ -->
                                    <%-- // 									$("input[name*='<%=questionIdTextKey%>']").timepicker({  --%>
                                <!-- // 										timeFormat: 'hhmmss', -->
                                <!-- // 									    showButtonPanel : true, -->
                                <!-- // 										showOn : "both" -->
                                <!-- // 									}); -->
                                <!-- // 								}); -->
                                <!-- 							</script> -->
                                    <%-- 							<input type="text" class="TextBox" name="<%=questionIdTextKey%>" size="8" maxlength="8" id="<%=questionIdTextKey%>"  value="<%=answerTextField%>"/>					 --%>
                                &nbsp;<input type="text" class="TextBox" name="<%=questionIdTextKey%>" size="4"
                                             maxlength="4" id="<%=questionIdTextKey%>"
                                             value="<%=answerTextField%>"/><bean:message
                                    key="feature.location.history.format.hhmm"/>
                                <% } %>
                                <% } else if (AnswerType.RADIO.equals(answType) || AnswerType.RADIO_WITH_DESCRIPTION.equals(answType)) { %>
                                <logic:notEmpty name="data" property="listQuestionAnswer">
                                    <logic:iterate id="dataQuestion" name="data" property="listQuestionAnswer"
                                                   indexId="idxQ">
                                        <% String checked = "";
                                            if (answerText.equals(((FeatQuestionAnswer) dataQuestion).getOptionAnswerId().toString())) {
                                                checked = "checked='checked'";
                                            } else {
                                                checked = "";
                                            }%>
                                        <label><input type="radio" <%=checked %>
                                                      value='<bean:write name="dataQuestion" property="optionAnswerId"/>'
                                                      name='<%=questionIdKey%>'/><bean:write name="dataQuestion"
                                                                                             property="optionAnswerLabel"/></label>
                                    </logic:iterate>
                                    <% if (AnswerType.RADIO_WITH_DESCRIPTION.equals(answType)) {%>
                                    <input type="text" class="TextBox" name='<%=questionIdTextKey%>'
                                           value="<%=answerText%>" <%=len %> />
                                    <%} %>
                                </logic:notEmpty>
                                <% } else if (AnswerType.DROPDOWN.equals(answType) || AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(answType)) { %>
                                <logic:notEmpty name="data" property="listQuestionAnswer">
                                    <select name='<%=questionIdKey%>'>
                                        <logic:iterate id="dataQuestion" name="data" property="listQuestionAnswer"
                                                       indexId="idxQ">
                                            <% String selected = "";
                                                if (answerText.equals(((FeatQuestionAnswer) dataQuestion).getOptionAnswerId().toString())) {
                                                    selected = "selected='selected'";
                                                } else {
                                                    selected = "";
                                                }%>
                                            <option <%=selected %>
                                                    value='<bean:write name="dataQuestion" property="optionAnswerId"/>'>
                                                <bean:write name="dataQuestion" property="optionAnswerLabel"/></option>
                                        </logic:iterate>
                                    </select>
                                    <% if (AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(answType)) {%>
                                    <input type="text" class="TextBox" name='<%=questionIdTextKey%>' <%=len %> />
                                    <%} %>
                                </logic:notEmpty>
                                <%
                                } else if (AnswerType.MULTIPLE.equals(answType) || AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(answType)
                                        || AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(answType)) {// || AnswerType.MULTIPLE_LOOKUP_WITH_FILTER.equals(answType)){
                                %>
                                <logic:notEmpty name="data" property="listQuestionAnswer">
                                    <%
                                        String clicked = "";
                                        String qId = String.valueOf(((FeatQuestion) data).getQuestionId());
                                        if (AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(answType)) {
                                            clicked = "onclick=\"javascript:toggleText('_" + qId + "_#idxQ','_" + qId + "text_#idxQ');\"";
                                        }
                                        Map<String, String> multimap = new HashMap<String, String>();
                                        Map<String, String> multimapText = new HashMap<String, String>();
                                        if (answerText.getClass().equals(ArrayList.class)) {
                                            List listAnsw = (ArrayList) answerText;
                                            List listAnswText = null;
                                            if (answerTextField.getClass().equals(ArrayList.class)) {
                                                listAnswText = (ArrayList) answerTextField;
                                            }
                                            int indexArr = 0;
                                            for (Object o : listAnsw) {
                                                String values = String.valueOf(o);
                                                multimap.put(values, "checked='checked'");
                                                if (null != listAnswText)
                                                    multimapText.put(values + "text", "value='" + listAnswText.get(indexArr) + "'");
                                                indexArr++;
                                            }
                                        } else {
                                            multimap.put((String) answerText, "checked='checked'");
                                            List listAnswText = null;
                                            if (answerTextField.getClass().equals(ArrayList.class)) {
                                                listAnswText = (ArrayList) answerTextField;
                                                multimapText.put((String) answerText + "text", "value='" + listAnswText.get(0) + "'");
                                            } else {
                                                multimapText.put((String) answerText + "text", "value='" + answerTextField + "'");
                                            }
                                        }
                                    %>
                                    <logic:iterate id="dataQuestion" name="data" property="listQuestionAnswer"
                                                   indexId="idxQ">
                                        <input type="checkbox" <%= multimap.get(((FeatQuestionAnswer) dataQuestion).getOptionAnswerId().toString()) %>
                                               value='<bean:write name="dataQuestion" property="optionAnswerId"/>' <% out.print(clicked.replaceAll("#idxQ", idxQ + ""));%>
                                               name='<%=questionIdKey%>'
                                               id='_<bean:write name="data" property="questionId"/>_<%=idxQ%>'/>
                                        <bean:write name="dataQuestion" property="optionAnswerLabel"/>
                                        <% if (AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(answType)) { %>
                                        <input type="text" <%= multimapText.get(((FeatQuestionAnswer) dataQuestion).getOptionAnswerId() + "text") %>
                                               disabled="disabled" class="TextBox" name='<%=questionIdTextKey%>'
                                               id='_<bean:write name="data" property="questionId"/>text_<%=idxQ%>'  <%=len %> />
                                        <script type="text/javascript">
                                            $(function () {
                                                var multiple = document.getElementById('_<bean:write name="data" property="questionId"/>_<%=idxQ%>');//.disabled = false
                                                var texter = document.getElementById('_<bean:write name="data" property="questionId"/>text_<%=idxQ%>');
                                                if (multiple.checked) {
                                                    texter.disabled = false;
                                                }
                                                else {
                                                    texter.value = '';
                                                    texter.disabled = true;
                                                }
                                            });
                                        </script>
                                        <% } %>
                                        <br/>
                                    </logic:iterate>
                                    <% if (AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(answType)) {
                                        clicked = "onclick=\"javascript:toggleText('_" + qId + "_#idxQ','_" + qId + "text_#idxQ');\"";%>
                                    <label><input type="checkbox" value='999' <%
                                        out.print(clicked.replaceAll("#idxQ", "999"));%> name='<%=questionIdKey%>'
                                                  id='_<bean:write name="data" property="questionId"/>_999'/>Lainnya</label>
                                    <input type="text" disabled="disabled" class="TextBox" name='<%=questionIdTextKey%>'
                                           id='_<bean:write name="data" property="questionId"/>text_999' <%=len %> />
                                    <% } %>
                                </logic:notEmpty>
                                <% } %>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
            </table>
            <table width="95%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%" height="30">
                        &nbsp;
                    </td>
                    <td width="50%" align="right">
                        <html:hidden property="nextGroup"/>
                        <bean:define id="currentGroup" name="answerSurveyAssignmentForm"
                                     property="featQuestionGroup.rownum" type="java.lang.Integer"/>
                        <bean:define id="actionBack" type="java.lang.String" value="0"/>
                        <bean:define id="current" type="java.lang.String" value="0"/>
                        <bean:define id="totalGroup" name="answerSurveyAssignmentForm"
                                     property="featQuestionGroup.countGroup" type="java.lang.Integer"/>
                        <%
                            if ("1".equalsIgnoreCase(currentGroup.toString()) || ("0".equalsIgnoreCase(currentGroup.toString()))) {
                                actionBack = "Load";
                            } else {
                                actionBack = "BackAdd";
                                current = String.valueOf(currentGroup - 1);
                            }
                            if (currentGroup.equals(totalGroup)) {
                        %>
                        <a href="javascript:movePage('Save',0);">
                            <html:img src="../../../images/button/ButtonSubmit.png" width="100" height="31" border="0"/>
                        </a>
                        <%
                        } else {
                        %>
                        <a href="javascript:movePage('NextAdd',0);">
                            <img src="../../../images/button/ButtonNext.png" alt="Action Input Question"
                                 title="<bean:message key="tooltip.action"/> Input Question" width="100" height="31"
                                 border="0">
                        </a>
                        <%
                            }
                        %>
                        <a href="javascript:movePage('<%=actionBack%>','<%=current%>');">
                            <html:img src="../../../images/button/ButtonBack.png" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
            <html:hidden property="jsonAnswer"/>
            <html:hidden property="seq_id"/>
        </div>
    </html:form>
    </body>
</html:html>
