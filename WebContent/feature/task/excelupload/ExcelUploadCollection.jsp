<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            var submit = 0;
            function movePage(taskID) {
                if (taskID == "SaveExcel") {
                    if (trim(document.forms[0].excelFile.value) == "") {
                        alert('<bean:message key="common.harusPilih" arg0="File excel"/>');
                        return;
                    }
                    else if (check()) {
                        if (submit == 0) {
                            submit++;
                        } else {
                            alert('<bean:message key="common.doubleaccess"/>');
                            return;
                        }
                    } else if (!check()) {
                        return;
                    }

                    if (!checkSchemeFile()) {
                        return;
                    }
                }
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

            function check() {
                var ext = document.forms[0].excelFile.value;
                var lastDot = ext.lastIndexOf(".");

                if (lastDot == -1) {
                    alert('<bean:message key="upload.errorexcel.file.noext" arg0="'+ext+'"/>');
                    return false;
                }

                ext = ext.substring(lastDot, ext.length);
                ext = ext.toLowerCase();
                if (ext != '.xls') {
                    alert('<bean:message key="upload.errorexcel.file.ext" arg0="'+ext+'"/>');
                    return false;
                } else
                    return true;
            }

            function checkSchemeFile() {
                var scheme = document.forms[0].schemeId.value;
                var today = new Date();
                var currentDate;
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                currentDate = yy + '' + mm + '' + dd;

                var ext = document.forms[0].excelFile.value;
                var fileName = '<bean:message key="upload.collection.file.name" arg0="'+currentDate+'" arg1="'+scheme+'"/>';
                var lastDot = ext.lastIndexOf(fileName);

                if (lastDot == -1) {
                    alert('<bean:message key="upload.errorexcel.file.match" arg0="'+ext+'"/>');
                    document.forms[0].excelFile.value = "";
                    submit = 0;
                    return false;
                } else {
                    return true;
                }
            }
        </script>
    </head>

    <body>
    <html:form action="/feature/task/UploadCollection.do" method="post" enctype="multipart/form-data">
        <html:hidden property="task" value="Load"/>
        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.excel.upload.bucket"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.privilege.form"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="schemeId" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
                            <logic:notEmpty name="comboScheme" scope="request">
                                <html:options collection="comboScheme" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.excel.upload.browse"/></td>
                    <td class="Edit-Right-Text"><html:file property="excelFile" styleClass="TextBox"></html:file></td>
                </tr>
            </table>
        </div>
        <table width="95%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="50%" height="30">&nbsp;</td>
                <td width="50%" align="right">
                    <a href="javascript:movePage('SaveExcel');">
                        <img src="../../../images/button/ButtonSubmit.png" alt="Action Bucket Upload"
                             title="<bean:message key="tooltip.action"/> Bucket Upload" width="100" height="31"
                             border="0">
                    </a>
                    <a href="javascript:movePage('Download');">
                        <img src="../../../images/button/ButtonDownloadTemplate.png" alt="Download Template"
                             title="Download Template" width="100" height="31" border="0">
                    </a>
                </td>
            </tr>
        </table>
    </html:form>
    </body>
</html:html>

