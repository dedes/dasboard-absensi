<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function movePage(taskID, schemeId) {
                document.forms[0].task.value = taskID;
                document.forms[0].schemeId.value = schemeId;
                document.forms[0].submit();
            }

            function taskDelete(schemeId) {
                if (!confirm('<bean:message key="common.confirmdelete" />'))
                    return;
                document.forms[0].task.value = 'Delete';
                document.forms[0].schemeId.value = schemeId;
                document.forms[0].submit();
            }

            function miniAction(schemeId, thePlace) {
                var path;
                var curUri = '<bean:write name="schemeForm" property="currentUri"/>';
                path = '<%=request.getContextPath()%>';
                document.forms[0].schemeId.value = schemeId;
                if (thePlace == "SchemeGroup") {
                    document.forms[0].task.value = 'QuestionGroupLoad';
                    document.forms[0].action = path
                        + '/feature/form/SchemeGroup.do?previousUri='
                        + curUri;
                }
                else if (thePlace == "SchemePrintItem") {
                    document.forms[0].task.value = 'PrintItemLoad';
                    document.forms[0].action = path
                        + '/feature/form/PrintItem.do?previousUri='
                        + curUri;
                }
                document.forms[0].submit();
            }

            function openViewDetail(schemeId) {
                window.open('<html:rewrite action="/feature/view/Questions"/>?task=Load&schemeId=' + schemeId,
                    'viewQuestionSet', 'height=600,width=800,left=0,top=0,menubar=no,status=no,toolbar=no,scrollbars=yes,resizable=yes,titlebar=no,location=no');
                return;
            }
        </script>
    </head>

    <body>
    <html:form action="/feature/form/Scheme.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="schemeId"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="currentUri"/>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.form.group"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.filtering"/>
                        <html:select property="searchType">
                            <html:option value="10"><bean:message key="common.id"/></html:option>
                            <html:option value="20"><bean:message key="general.desc"/></html:option>
                        </html:select>
                    </td>
                    <td class="Edit-Right-Text">
                        <html:text property="searchValue" size="25" maxlength="30" styleClass="TextBox"/>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search" width="100" height="31"
                                 border="0" title="<bean:message key="tooltip.search"/>">
                        </a>
                    </td>
                </tr>
            </table>
            <div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;">
                <table border="0" width="100%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="10%" rowspan="2" align="center"><bean:message key="common.id"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="general.desc"/></td>
                        <td width="5%" align="center" rowspan="2"><bean:message key="common.active.header"/></td>
                        <td width="10%" align="center" rowspan="2"><bean:message key="feature.form.template"/></td>
                        <td width="15%" colspan="3" align="center"><bean:message key="common.action"/></td>
                    </tr>
                    <tr align="center" class="headercol">
                        <td width="10%" align="center"><bean:message key="feature.print.template"/></td>
                        <td width="10%" align="center"><bean:message key="feature.group.list"/></td>
                        <td width="5%" align="center"><bean:message key="common.edit"/></td>
                    </tr>
                    <%
                        int i = 0;
                        String classColor;
                    %>
                    <logic:notEmpty name="listScheme" scope="request">
                        <logic:iterate id="data" name="listScheme" scope="request">
                            <%
                                if ((i % 2) != 1)
                                    classColor = "evalcol";
                                else
                                    classColor = "oddcol";
                                i++;
                            %>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td><bean:write name="data" property="schemeId"/></td>
                                <td><bean:write name="data" property="schemeDescription"/></td>
                                <td align="center">
                                    <logic:equal name="data" property="active" value="1">
                                        <img src="../../../images/icon/IconActive.gif" alt="Active" width="16"
                                             height="16" title="<bean:message key="tooltip.active"/>"/>
                                    </logic:equal>
                                    <logic:notEqual name="data" property="active" value="1">
                                        <img src="../../../images/icon/IconInactive.gif" alt="Inactive" width="16"
                                             height="16" title="<bean:message key="tooltip.inactive"/>"/>
                                    </logic:notEqual>
                                </td>
                                <td align="center">
                                    <a href="javascript:openViewDetail('<bean:write name="data" property="schemeId"/>')">
                                        <html:img border="0" src="../../../images/icon/IconList.gif" width="16"
                                                  height="16" title="View Template"/>
                                    </a>
                                </td>
                                <td align="center">
                                    <logic:equal name="data" property="isPrintable" value="1">
                                        <a href="javascript:miniAction('<bean:write name="data" property="schemeId"/>','SchemePrintItem')">
                                            <html:img border="0" src="../../../images/icon/IconPrintPreview.gif"
                                                      width="16" height="16" title="View Print Template"/>
                                        </a>
                                    </logic:equal>
                                </td>
                                <td align="center">
                                    <a href="javascript:miniAction('<bean:write name="data" property="schemeId"/>','SchemeGroup')">
                                        <html:img border="0" src="../../../images/icon/IconSetting.gif" width="16"
                                                  height="16" title="Add/Edit Question Group List"/>
                                    </a>
                                </td>
                                <td align="center">
                                    <a title="Edit"
                                       href="javascript:movePage('Edit','<bean:write name="data" property="schemeId"/>');">
                                        <img src="../../../images/icon/IconEdit.gif" width="16" height="13" border="0"/>
                                    </a>
                                </td>
                            </tr>

                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listScheme" scope="request">
                        <tr class="tdgenap">
                            <td bgcolor="FFFFFF" colspan="8" align="center">
                                <font class="errMsg"><bean:message key="common.listnotfound"/></font>
                            </td>
                        </tr>
                    </logic:empty>
                </table>
            </div>
            <logic:notEmpty name="listScheme" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.schemeForm,'First')" title="First">
                                <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.schemeForm,'Prev')" title="Previous">
                                <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="schemeForm" property="paging.currentPageNo" size="3" maxlength="6"
                                       styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <html:hidden property="paging.totalRecord" write="false"/>
                            <a href="javascript:moveToPageNumber(document.schemeForm,'Go');">
                                <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                     border="0"/></a></td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.schemeForm,'Next')" title="Next">
                                <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.schemeForm,'Last')" title="Last">
                                <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:define id="currentPage" name="schemeForm" property="paging.currentPageNo"
                                         type="java.lang.Integer"/>
                            <bean:define id="total" name="schemeForm" property="paging.totalRecord"
                                         type="java.lang.Integer"/>
                            <bean:define id="perpage" name="schemeForm" property="paging.rowPerPage"
                                         type="java.lang.Integer"/>
                            <%
                                int startShow = 1;
                                int endShow = 0;
                                if (currentPage > 1) {
                                    int tempstart = ((currentPage - 1) * perpage) + 1;
                                    int tempend = currentPage * perpage;
                                    if (total < tempend) {
                                        tempend = total;
                                    }
                                    startShow = tempstart;
                                    endShow = tempend;
                                } else {
                                    endShow = total;
                                }
                            %>
                            <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                            <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Add','')"><img src="../../../images/button/ButtonAdd.png"
                                                                     alt="Add" width="100" height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>