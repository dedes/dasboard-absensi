<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>

<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
    </head>

    <script language="javascript">
        function movePage(taskID) {
            if (taskID == "Save" || taskID == "Update") {
                if (document.forms[0].onsubmit == null || document.forms[0].onsubmit()) {
                    document.forms[0].task.value = taskID;
                    document.forms[0].submit();
                }
            }
            else if (taskID == "Load") {
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }
        }
    </script>

    <body>
    <html:form action="/feature/form/Scheme.do" method="POST" enctype="multipart/form-data">
        <html:hidden property="task"/>
        <html:hidden property="searchType"/>
        <html:hidden property="searchValue"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <html:hidden property="activeOri"/>
        <html:hidden property="currentUri"/>
        <logic:present scope="request" name="schemeinuse">
            <script language="javascript">
                alert('<bean:write name="schemeinuse" scope="request"/>');
            </script>
        </logic:present>
        <%-- 			<%@include file="../../../Error.jsp"%> --%>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <bean:message key="app.privilege.form"/>
                        <logic:equal name="schemeForm" property="task" value="Edit" scope="request"><html:hidden
                                name="schemeForm" property="schemeId" write="true"/> <bean:message
                                key="common.edit"/></logic:equal>
                        <logic:equal name="schemeForm" property="task" value="Add" scope="request"><bean:message
                                key="common.add"/></logic:equal>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <logic:equal name="schemeForm" property="task" value="Add" scope="request">
                    <tr>
                        <td class="Edit-Left-Text"><bean:message key="common.formid"/></td>
                        <td class="Edit-Right-Text">
                            <logic:equal name="schemeForm" property="task" value="Edit" scope="request">
                                <html:hidden property="schemeId" write="true"/>
                            </logic:equal>
                            <logic:equal name="schemeForm" property="task" value="Add" scope="request">
                                <html:text name="schemeForm" property="schemeId" size="5" maxlength="5"
                                           styleClass="TextBox"/>
                                <font color="#FF0000">*)<html:errors property="schemeId"/></font>
                            </logic:equal>
                        </td>
                    </tr>
                </logic:equal>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.scheme.description"/></td>
                    <td class="Edit-Right-Text"><html:text name="schemeForm" property="schemeDescription" size="30"
                                                           maxlength="30" styleClass="TextBox"/><font color="#FF0000">*)
                        <html:errors property="schemeDescription"/></font></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.scheme.function"/></td>
                    <td class="Edit-Right-Text">
                        <html:select name="schemeForm" property="scheme_role">
                            <%-- 			   				<html:option value="AF"><bean:message key="common.all" /></html:option> --%>
                            <html:option value="SF"><bean:message key="combo.label.survey.form"/></html:option>
                            <html:option value="CF"><bean:message key="combo.label.collection.form"/></html:option>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.scheme.print"/></td>
                    <td class="Edit-Right-Text"><html:checkbox name="schemeForm" value="1" property="isPrintable"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.active.header"/></td>
                    <td class="Edit-Right-Text"><html:checkbox name="schemeForm" value="1" property="active"/></td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <logic:equal name="schemeForm" property="task" value="Edit" scope="request">
                            <a href="javascript:movePage('Update');"><img src="../../../images/button/ButtonSubmit.png"
                                                                          width="100" height="31" border="0"/></a>
                        </logic:equal>
                        <logic:equal name="schemeForm" property="task" value="Add" scope="request">
                            <a href="javascript:movePage('Save');"><img src="../../../images/button/ButtonSubmit.png"
                                                                        width="100" height="31" border="0"/></a>
                        </logic:equal>
                        <a href="javascript:movePage('Load');"><img src="../../../images/button/ButtonCancel.png"
                                                                    width="100" height="31" border="0"/></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>