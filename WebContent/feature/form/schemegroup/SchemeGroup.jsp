<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.j	s"></script>
        <script language="javascript">
            function setQuestionGroup(kode, nama) {
                findElement(document.forms[0], "questionGroupId").value = kode;
                findElement(document.forms[0], "questionGroupName").value = nama;
            }

            function openQuestionGroup(schemeId, winName, features) {
                var toUrl = '<html:rewrite action="feature/lookup/QuestionGroup"/>' +
                    '?schemeId=' + schemeId;
                window.open(toUrl, winName, features);
            }

            function taskBack() {
                document.forms[0].task.value = 'Back';
                document.forms[0].submit();
            }

            function taskSave() {
                document.forms[0].task.value = 'QuestionGroupSave';
                document.forms[0].submit();
            }

            function taskUpdate() {
                document.forms[0].task.value = 'QuestionGroupUpdate';
                document.forms[0].submit();
            }

            function taskDelete(questionGroupId) {
                if (!confirm('<bean:message key="common.confirmdelete" />'))
                    return;
                document.forms[0].task.value = 'QuestionGroupDelete';
                document.forms[0].selectedQuestionGroupId.value = questionGroupId;
                document.forms[0].submit();
            }

            function validateSequence(form) {
                var errIdx = 0;
                var messages = new Array();

                <logic:iterate id="id" name="schemeGroupForm" property="listQuestionGroup" scope="request" indexId="idx">
                element = findElement(form, "data[<%=idx%>].lineSeqOrder");
                if (trim(element.value).length == 0) {
                    messages[errIdx] = '<bean:message key="errors.required" arg0="Sequence"/>' + ' (<%=idx.intValue()+1%>)';
                    errIdx++;
                }
                else if (/^\d+$/.test(trim(element.value)) == false) {
                    messages[errIdx] = '<bean:message key="errors.integer" arg0="Sequence"/>' + ' (<%=idx.intValue()+1%>)';
                    errIdx++;
                }

                </logic:iterate>

                if (errIdx > 0) {
                    alert(messages.join("\n"));
                    return false;
                }
                return true;
            }
        </script>
    </head>

    <body>
    <html:form action="/feature/form/SchemeGroup.do" method="post">
        <html:hidden property="task" value="QuestionGroupLoad"/>
        <html:hidden property="selectedQuestionGroupId"/>
        <html:hidden property="previousUri"/>
        <logic:present scope="request" name="message">
            <script language="javascript">
                alert('<bean:write name="message" scope="request"/>');
            </script>
        </logic:present>
        <%-- 	<%@include file="../../../Error.jsp"%> --%>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <logic:equal name="schemeGroupForm" property="task" value="Edit" scope="request">
                            <bean:message key="common.edit"/>
                        </logic:equal>
                        <logic:notEqual name="schemeGroupForm" property="task" value="Edit" scope="request">
                            <bean:message key="common.add"/>
                        </logic:notEqual>
                        <bean:message key="feature.scheme.group.of"/> <html:hidden property="schemeId" write="true"/>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.desc"/></td>
                    <td class="Edit-Right-Text">
                        <html:hidden name="schemeGroupForm" property="schemeDescription" write="true"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.group"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="questionGroupName" styleClass="TextBox" size="20" maxlength="20"
                                   readonly="true"/>
                        <html:hidden property="questionGroupId"/>
                        <a href="javascript:openQuestionGroup('<bean:write property="schemeId" name="schemeGroupForm"/>','QuestionGroup','scrollbars=yes,width=640,height=480,resizable=yes,top=0,left=0')">
                            <img src="../../../images/icon/IconLookUp.gif" alt="Look up QuestionGroup" border="0"
                                 title="Look up QuestionGroup" width="16" height="16"></a>
                        <font color="#FF0000">*)<html:errors property="questionGroupName"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.sequence.order"/></td>
                    <td class="Edit-Right-Text">
                        <html:text style="text-align:right;" property="lineSeqOrder" size="5" maxlength="5"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="lineSeqOrder"/></font>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:taskSave();">
                            <img src="../../../images/button/ButtonAdd.png" alt="<bean:message key="tooltip.add"/>"
                                 width="100" height="31" border="0" title="<bean:message key="tooltip.add"/>">
                        </a>
                    </td>
                </tr>
            </table>
            <table border="0" width="96%" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="80%" rowspan="2"><bean:message key="feature.scheme.group.list"/></td>
                    <td width="10%" rowspan="2" align="center"><bean:message key="feature.sequence"/></td>
                    <td width="5%" colspan="1" align="center"><bean:message key="common.action"/></td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="5%" align="center"><bean:message key="common.delete"/></td>
                </tr>
                <%
                    int i = 0;
                    String classColor;
                %>
                <logic:notEmpty name="schemeGroupForm" property="listQuestionGroup" scope="request">
                    <logic:iterate id="data" name="schemeGroupForm" property="listQuestionGroup" scope="request">
                        <%
                            if ((i % 2) != 1)
                                classColor = "evalcol";
                            else
                                classColor = "oddcol";
                            i++;
                        %>
                        <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=classColor%>'">
                            <td>
                                <bean:write name="data" property="questionGroupName"/>
                                <html:hidden name="data" property="schemeId" indexed="true"/>
                                <html:hidden name="data" property="questionGroupId" indexed="true"/>
                            </td>
                            <td align="center"><html:text style="text-align:right;" name="data" property="lineSeqOrder"
                                                          size="5" maxlength="3" indexed="true"
                                                          styleClass="TextBox"/></td>
                            <td align="center">
                                <a title="Delete"
                                   href="javascript:taskDelete('<bean:write name="data" property="questionGroupId"/>');">
                                    <img src="../../../images/icon/IconDelete.gif" width="16" height="16" border="0"/>
                                </a>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>

                <logic:empty name="schemeGroupForm" property="listQuestionGroup" scope="request">
                    <tr class="tdgenap">
                        <td bgcolor="FFFFFF" colspan="3" align="center">
                            <font class="errMsg"><bean:message key="common.listnotfound"/></font>
                        </td>
                    </tr>
                </logic:empty>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30"></td>
                    <td width="50%" align="right">
                        <logic:notEmpty name="schemeGroupForm" property="listQuestionGroup" scope="request">
                            <a href="javascript:taskUpdate();">
                                <img src="../../../images/button/ButtonSubmit.png" width="100" height="31" border="0"/>
                            </a>
                        </logic:notEmpty>
                        <a href="javascript:taskBack();"><img src="../../../images/button/ButtonBack.png" width="100"
                                                              height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>

