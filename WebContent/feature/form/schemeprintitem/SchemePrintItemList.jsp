<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title>FormPrintItem . View</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function movePage(taskID, printItemSeqno) {
                document.forms[0].task.value = taskID;
                document.forms[0].printItemSeqno.value = printItemSeqno;
                document.forms[0].submit();
            }

            function actionBack() {
                document.forms[0].task.value = 'Back';
                document.forms[0].submit();
            }

            function updateForm() {
                document.forms[0].task.value = 'PrintItemSeqUpdate';
                document.forms[0].submit();
            }

            function taskDelete(printItemSeqno) {
                if (!confirm('<bean:message key="common.confirmdelete" />'))
                    return;
                document.forms[0].task.value = 'PrintItemDelete';
                document.forms[0].printItemSeqno.value = printItemSeqno;
                document.forms[0].submit();
            }

            function validateSequence(form) {
                var errIdx = 0;
                var messages = new Array();
                <logic:notEmpty name="schemePrintItemForm" property="listPrintItem" scope="request">
                <logic:iterate id="id" name="schemePrintItemForm" property="listPrintItem" scope="request" indexId="idx">
                element = findElement(form, "data[<%=idx%>].lineSeqOrder");
                if (trim(element.value).length == 0) {
                    messages[errIdx] = '<bean:message key="errors.required" arg0="Sequence"/>' + ' (<%=idx.intValue()+1%>)';
                    errIdx++;
                }
                else if (/^\d+$/.test(trim(element.value)) == false) {
                    messages[errIdx] = '<bean:message key="errors.integer" arg0="Sequence"/>' + ' (<%=idx.intValue()+1%>)';
                    errIdx++;
                }

                </logic:iterate>
                </logic:notEmpty>

                if (errIdx > 0) {
                    alert(messages.join("\n"));
                    return false;
                }
                return true;
            }
        </script>


    <body>
    <html:form action="/feature/form/PrintItem.do" method="post">
        <html:hidden property="task" value="PrintItemLoad"/>
        <html:hidden property="printItemSeqno"/>
        <html:hidden property="previousUri"/>
        <html:hidden name="schemePrintItemForm" property="schemeDescription" write="false"/>
        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.template.print.item"/> <html:hidden
                            property="schemeId" write="true"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="70%" rowspan="2" align="center"><bean:message key="feature.scheme.printed.text"/></td>
                    <td width="10%" rowspan="2" align="center"><bean:message key="common.type"/></td>
                    <td width="10%" align="center" rowspan="2"><bean:message key="feature.sequence.order"/></td>
                    <td width="10%" colspan="2" align="center"><bean:message key="common.action"/></td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="5%" align="center"><bean:message key="common.edit"/></td>
                    <td width="5%" align="center"><bean:message key="common.delete"/></td>
                </tr>
                <%
                    int i = 0;
                    String classColor;
                %>
                <logic:notEmpty name="schemePrintItemForm" property="listPrintItem" scope="request">
                    <logic:iterate id="data" name="schemePrintItemForm" property="listPrintItem" scope="request">
                        <%
                            if ((i % 2) != 1)
                                classColor = "evalcol";
                            else
                                classColor = "oddcol";
                            i++;
                        %>
                        <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=classColor%>'">
                            <td>
                                <bean:write name="data" property="printItemLabel"/>
                                <html:hidden name="data" property="schemeId" indexed="true"/>
                                <html:hidden name="data" property="printItemSeqno" indexed="true"/>
                            </td>
                            <td><bean:write name="data" property="printItemTypeName"/></td>
                            <td align="center"><html:text style="text-align:right;" name="data" property="lineSeqOrder"
                                                          size="5" maxlength="3" indexed="true"
                                                          styleClass="TextBox"/></td>
                            <td align="center">
                                <a title="Edit"
                                   href="javascript:movePage('PrintItemEdit','<bean:write name="data" property="printItemSeqno"/>');">
                                    <img src="../../../images/icon/IconEdit.gif" width="16" height="16" border="0"/>
                                </a>
                            </td>
                            <td align="center">
                                <a title="Delete"
                                   href="javascript:taskDelete('<bean:write name="data" property="printItemSeqno"/>');">
                                    <img src="../../../images/icon/IconDelete.gif" width="16" height="16" border="0"/>
                                </a>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="schemePrintItemForm" property="listPrintItem" scope="request">
                    <tr class="tdgenap">
                        <td bgcolor="FFFFFF" colspan="5" align="center">
                            <font class="errMsg"><bean:message key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>

            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30"></td>
                    <td width="50%" align="right">
                        <logic:notEmpty name="schemePrintItemForm" property="listPrintItem" scope="request">
                            <a href="javascript:updateForm();"><img src="../../../images/button/ButtonSubmit.png"
                                                                    width="100" height="31" border="0"/></a>
                        </logic:notEmpty>
                        <a href="javascript:movePage('PrintItemAdd','')"><img src="../../../images/button/ButtonAdd.png"
                                                                              width="100" height="31" border="0"/></a>
                        <a href="javascript:actionBack();"><img src="../../../images/button/ButtonBack.png" width="100"
                                                                height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>

