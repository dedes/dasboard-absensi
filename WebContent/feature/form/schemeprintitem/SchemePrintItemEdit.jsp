<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>

<html:html>
    <html:base/>
    <head>
        <title>FormPrintItem . Edit</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
    </head>

    <script language="javascript">
        var uri = '';
        function movePage(taskID) {
            if (taskID == "PrintItemSave" || taskID == "PrintItemUpdate") {
//             if (!validateForm(document.forms[0])){
//                 return;
//             }
                if (!lookup_required()) {
                    return;
                }
            }

            document.forms[0].task.value = taskID;
            document.forms[0].submit();
        }

        function setQuestion(qg, g, label) {
            findElement(document.forms[0], "questionGroupId").value = qg;
            findElement(document.forms[0], "questionId").value = g;
        }

        function setQuestionLabel(label) {
            findElement(document.forms[0], "printItemLabel").value = label;
        }

        function openLU(key, winName, features) { //v2.0
            var toUrl = uri + key;
            window.open(toUrl, winName, features);
        }


        function openQuestionSet(schemeId, winName, features) { //v2.0
            var toUrl = '<html:rewrite action="/feature/lookup/Questions"/>' +
                '?schemeId=' + schemeId;
            window.open(toUrl, winName, features);
        }

        function openHeader(schemeId, winName, features) { //v2.0
            var toUrl = '<html:rewrite action="/feature/lookup/Questions"/>' +
                '?schemeId=' + schemeId;
            window.open(toUrl, winName, features);
        }

        function lookup_required() {
            var printItemType = findElement(document.forms[0], "printItemTypeId").value;
            if (printItemType == "400") {  //Question
                var questionId = trim(document.forms[0].questionId.value);
                if (questionId == "") {
                    alert('<bean:message key="common.harusPilih" arg0="Lookup Question"/>');
                    return false;
                }
            } else if (printItemType == "000") {  //Data Header
                var questionId = trim(document.forms[0].questionId.value);
                if (questionId == "") {
                    alert('<bean:message key="common.harusPilih" arg0="Lookup Question"/>');
                    return false;
                }
            }
            return true;
        }

        function cekPilihanType() {
            var printItemType = findElement(document.forms[0], "printItemTypeId").value;
            if (printItemType == "400") {
                document.getElementById("lookup_required").style.visibility = "visible";
                uri = document.getElementById("lu_req").innerHTML;
                document.forms[0].printItemLabel.disabled = false;
            }
            else if (printItemType == "000") {
                document.getElementById("lookup_required").style.visibility = "visible";
                uri = document.getElementById("lu_head_req").innerHTML;
                document.forms[0].printItemLabel.disabled = false;
            }
            else {
                document.getElementById("lookup_required").style.visibility = "hidden";
                uri = '';
                document.forms[0].questionGroupId.value = "";
                document.forms[0].questionId.value = "";
                document.forms[0].printItemLabel.disabled = false;
            }
        }
    </script>

    <body onload="cekPilihanType();">
    <html:form action="/feature/form/PrintItem.do" method="POST">
        <html:hidden property="task"/>
        <html:hidden property="schemeId"/>
        <html:hidden property="questionGroupId"/>
        <html:hidden property="questionId"/>
        <html:hidden property="previousUri"/>
        <%-- 	<%@include file="../../../Error.jsp"%> --%>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <logic:equal name="schemePrintItemForm" property="task" value="PrintItemEdit" scope="request">
                            <html:hidden property="printItemSeqno"/>
                            <html:hidden property="schemeDescription"/>
                            <bean:message key="common.edit"/>
                        </logic:equal>
                        <logic:equal name="schemePrintItemForm" property="task" value="PrintItemAdd" scope="request">
                            <html:hidden property="schemeDescription"/>
                            <bean:message key="common.add"/>
                        </logic:equal>
                        <bean:message key="feature.template.print.item"/><html:hidden property="schemeId"
                                                                                      write="true"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table border="0" width="95%" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.privilege.form"/></td>
                    <td class="Edit-Right-Text"><bean:write name="schemePrintItemForm" property="schemeId"/> -
                        <bean:write name="schemePrintItemForm" property="schemeDescription"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.scheme.item.type"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="printItemTypeId" onchange="cekPilihanType();">
                            <html:option value="">- Choose One -</html:option>
                            <logic:notEmpty name="listPrintItemType" scope="request">
                                <html:options collection="listPrintItemType" property="keyValue" labelProperty="label"/>
                            </logic:notEmpty>
                        </html:select>
                        <font color="#FF0000">*)<html:errors property="printItemTypeId"/></font>
                        <span id="lookup_required">
						<a href="javascript:openLU('<bean:write property="schemeId" name="schemePrintItemForm"/>','QuestionGroup','scrollbars=yes,width=640,height=480,resizable=yes,top=0,left=0')">
			        		<img src="../../../images/icon/IconLookUp.gif" alt="Lookup Question" width="16" height="16"
                                 border="0" title="Lookup Question">
			        	</a>
			        	<label id="lu_req" style="visibility: hidden;"><html:rewrite
                                action="/feature/lookup/Questions"/>?schemeId=</label>
			        	<label id="lu_head_req" style="visibility: hidden;"><html:rewrite
                                action="/feature/lookup/Questions"/>?schemeId=</label>
			        </span>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.scheme.item.label"/></td>
                    <td class="Edit-Right-Text"><html:text name="schemePrintItemForm" property="printItemLabel"
                                                           size="75" maxlength="32" styleClass="TextBox"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.sequence.order"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="schemePrintItemForm" property="lineSeqOrder" size="5" maxlength="5"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="lineSeqOrder"/></font>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <logic:equal name="schemePrintItemForm" property="task" value="PrintItemEdit" scope="request">
                            <a href="javascript:movePage('PrintItemUpdate');">
                                <img src="../../../images/button/ButtonSubmit.png" width="100" height="31" border="0"/>
                            </a>
                        </logic:equal>
                        <logic:equal name="schemePrintItemForm" property="task" value="PrintItemAdd" scope="request">
                            <a href="javascript:movePage('PrintItemSave');">
                                <img src="../../../images/button/ButtonSubmit.png" width="100" height="31" border="0"/>
                            </a>
                        </logic:equal>
                        <a href="javascript:movePage('PrintItemLoad');">
                            <img src="../../../images/button/ButtonCancel.png" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>