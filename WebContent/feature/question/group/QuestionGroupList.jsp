<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            var flag = 0;
            function movePage(taskID, questionGroupId) {
                if (taskID == 'Sync' && flag == 1) {
                    alert('<bean:message key="errors.sync"/>');
                }
                else {
                    if (taskID == 'Sync') {
                        if (!confirm('<bean:message key="common.confirmwebservice" />'))
                            return;
                        flag = 1;
                    }
                    document.forms[0].task.value = taskID;
                    document.forms[0].questionGroupId.value = questionGroupId;
                    document.forms[0].submit();
                }
            }

            function actionDelete(questionGroupId) {
                if (!confirm('<bean:message key="common.confirmdelete" />'))
                    return;
                document.forms[0].task.value = 'Delete';
                document.forms[0].questionGroupId.value = questionGroupId;
                document.forms[0].submit();
            }

            function openPage(questionGroupId, thePlace, task) {
                var path;
                var curUri = '<bean:write name="questionGroupForm" property="currentUri"/>';
                path = '<%=request.getContextPath()%>';
                document.forms[0].task.value = task;
                document.forms[0].questionGroupId.value = questionGroupId;
                if (thePlace == "Question") {
                    findElement(document.forms[0], "paging.currentPageNo").value = 1;
                    findElement(document.forms[0], "paging.dispatch").value = null;
                    document.forms[0].action = path
                        + '/feature/question/Question.do?previousUri='
                        + curUri;
                }
                document.forms[0].submit();
            }
        </script>

    </head>
    <body>
    <html:form action="/feature/question/QuestionGroup.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="questionGroupId"/>
        <html:hidden property="questionGroupName"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="currentUri"/>
        <%@include file="../../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.question.group.list"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.group.name"/><html:hidden
                            property="srcType" value="10"/></td>
                    <td class="Edit-Right-Text"><html:text property="srcValue" size="25" maxlength="30"
                                                           styleClass="TextBox"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.group.type"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="srcValue2">
                            <html:option value=""><bean:message key="common.all"/></html:option>
                            <html:option value="000"><bean:message key="combo.label.usage.normal"/></html:option>
                            <html:option value="010"><bean:message key="combo.label.usage.ranking"/></html:option>
                            <html:option value="020"><bean:message key="combo.label.usage.total"/></html:option>
                        </html:select>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search"
                                 title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="40%" align="center" rowspan="2"><bean:message key="feature.question.group.name"/></td>
                    <td width="20%" align="center" rowspan="2"><bean:message key="feature.question.group.type"/></td>
                    <td width="10%" align="center" rowspan="2"><bean:message key="common.label.max.value"/></td>
                    <td width="5%" align="center" rowspan="2"><bean:message key="feature.questions"/></td>
                    <td width="5%" align="center" rowspan="2"><bean:message key="common.active.header"/></td>
                    <td width="10%" align="center" colspan="2"><bean:message key="common.action"/></td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="5%" align="center"><bean:message key="common.edit"/></td>
                    <td width="5%" align="center"><bean:message key="common.delete"/></td>
                </tr>
                <logic:notEmpty name="listQuestionGroup" scope="request">
                    <logic:iterate id="data" name="listQuestionGroup" scope="request" indexId="i">
                        <%
                            String classColor = (i % 2) != 1 ? "evalcol" : "oddcol";
                            i++;
                        %>
                        <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=classColor%>'">
                            <td><bean:write name="data" property="questionGroupName"/></td>
                            <td align="center">
                                <logic:equal value="000" name="data" property="usage_type">
                                    <bean:message key="combo.label.usage.normal"/>
                                </logic:equal>
                                <logic:equal value="010" name="data" property="usage_type">
                                    <bean:message key="combo.label.usage.ranking"/>
                                </logic:equal>
                                <logic:equal value="020" name="data" property="usage_type">
                                    <bean:message key="combo.label.usage.total"/>
                                </logic:equal>
                            </td>
                            <td align="right">
                                <logic:notEmpty name="data" property="totalvalue">
                                    <logic:greaterThan value="0" name="data" property="totalvalue">
                                        <bean:write name="data" property="totalvalue"/>
                                    </logic:greaterThan>
                                    <%-- 							<logic:lessEqual value="0" name="data" property="totalvalue"> --%>
                                    <!-- - -->
                                    <%-- 							</logic:lessEqual>	 --%>
                                </logic:notEmpty>
                                    <%-- 						<logic:empty name="data" property="totalvalue"> --%>
                                <!-- 							- -->
                                    <%-- 						</logic:empty> --%>
                            </td>
                            <td align="center">
                                <logic:equal value="010" name="data" property="usage_type">
                                    <a href="javascript:openPage('<bean:write name="data" property="questionGroupId"/>','Question','LoadRank')">
                                        <html:img border="0" src="../../../images/icon/IconLookUp.gif" width="16"
                                                  height="16"/>
                                    </a>
                                </logic:equal>
                                <logic:notEqual value="010" name="data" property="usage_type">
                                    <a href="javascript:openPage('<bean:write name="data" property="questionGroupId"/>','Question','Load')">
                                        <html:img border="0" src="../../../images/icon/IconLookUp.gif" width="16"
                                                  height="16"/>
                                    </a>
                                </logic:notEqual>
                            </td>
                            <td align="center">
                                <logic:equal name="data" property="active" value="1">
                                    <html:img border="0" src="../../../images/icon/IconActive.gif" width="16"
                                              height="16"/>
                                </logic:equal>
                                <logic:notEqual name="data" property="active" value="1">
                                    <html:img border="0" src="../../../images/icon/IconInactive.gif" width="16"
                                              height="16"/>
                                </logic:notEqual>
                            </td>
                            <td align="center">
                                <a title="Edit"
                                   href="javascript:movePage('Edit','<bean:write name="data" property="questionGroupId"/>');">
                                    <img src="../../../images/icon/IconEdit.gif" width="16" height="13" border="0"/>
                                </a>
                            </td>
                            <td align="center">
                                <a title="Delete"
                                   href="javascript:actionDelete('<bean:write name="data" property="questionGroupId"/>');">
                                    <img src="../../../images/icon/IconDelete.gif" width="16" height="16" border="0"/>
                                </a>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="listQuestionGroup" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="7" align="center"><font
                                class="errMsg"><bean:message key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
            <logic:notEmpty name="listQuestionGroup" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionGroupForm,'First')"
                                       title="First">
                                <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionGroupForm,'Prev')"
                                       title="Previous">
                                <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="questionGroupForm" property="paging.currentPageNo" size="3" maxlength="6"
                                       styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <html:hidden property="paging.totalRecord" write="false"/>
                            <a href="javascript:moveToPageNumber(document.questionGroupForm,'Go');">
                                <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                     border="0"/>
                            </a>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionGroupForm,'Next')"
                                       title="Next">
                                <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionGroupForm,'Last')"
                                       title="Last">
                                <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:define id="currentPage" name="questionGroupForm" property="paging.currentPageNo"
                                         type="java.lang.Integer"/>
                            <bean:define id="total" name="questionGroupForm" property="paging.totalRecord"
                                         type="java.lang.Integer"/>
                            <bean:define id="perpage" name="questionGroupForm" property="paging.rowPerPage"
                                         type="java.lang.Integer"/>
                            <%
                                int startShow = 1;
                                int endShow = 0;
                                if (currentPage > 1) {
                                    int tempstart = ((currentPage - 1) * perpage) + 1;
                                    int tempend = currentPage * perpage;
                                    if (total < tempend) {
                                        tempend = total;
                                    }
                                    startShow = tempstart;
                                    endShow = tempend;
                                } else {
                                    endShow = total;
                                }
                            %>
                            <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                            <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Add','')"><img src="../../../images/button/ButtonAdd.png"
                                                                     alt="Add" width="100" height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>

