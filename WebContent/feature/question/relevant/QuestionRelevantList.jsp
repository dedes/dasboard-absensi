<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
    </head>
    <script language="javascript">
        function movePage(taskID) {
            document.forms[0].submit();
        }

        function actionDelete(relSeqno) {
            if (!confirm('<bean:message key="common.confirmdelete" />'))
                return;
            document.forms[0].task.value = 'Delete';
            document.forms[0].relSeqno.value = relSeqno;
            document.forms[0].submit();
        }

        function actionSave() {
            if (!relvalue_required()) {
                return;
            }
            document.forms[0].task.value = 'Save';
            document.forms[0].submit();
        }

        function actionBack() {
            document.forms[0].task.value = 'Back';
            document.forms[0].submit();
        }

        function openQuestion(questionGroupId, questionId, winName, features) {
            var toUrl = '<html:rewrite action="/feature/lookup/QuestionByGroup"/>' +
                '?excQuestionGroupId=' + questionGroupId +
                '&excQuestionId=' + questionId;
            window.open(toUrl, winName, features);
        }

        function openOptionAnswer(winName, features) {
            var questionGroupId, questionId;
            questionGroupId = document.forms[0].relQuestionGroupId.value;
            questionId = document.forms[0].relQuestionId.value;
            var toUrl = '<html:rewrite action="/feature/lookup/OptionAnswer"/>' +
                '?questionGroupId=' + questionGroupId +
                '&questionId=' + questionId;
            window.open(toUrl, winName, features);
        }

        function clearRelQuestion() {
            document.forms[0].relQuestionGroupId.value = "";
            document.forms[0].relQuestionId.value = "";
            document.forms[0].relQuestionLabel.value = "";
            clearRelAnswer();
            document.getElementById("relRequired").style.visibility = "hidden";
            document.getElementById("lookupValue").style.visibility = "hidden";
        }

        function clearRelAnswer() {
            document.forms[0].relOptionAnswerId.value = "";
            document.forms[0].relTextAnswer.value = "";
        }

        function setQuestion(qg, kode, lbl, tipe) {
            document.forms[0].relQuestionGroupId.value = qg;
            document.forms[0].relQuestionId.value = kode;
            document.forms[0].relQuestionLabel.value = lbl;
            document.forms[0].relAnswerTypeId.value = tipe;
            checkRelAnswerType();
            clearRelAnswer();
            document.getElementById("relRequired").style.visibility = "visible";
        }

        function setOptionAnswer(o, lbl) {
            document.forms[0].relOptionAnswerId.value = o;
            document.forms[0].relTextAnswer.value = lbl;
        }

        function relvalue_required() {
            var relQG = findElement(document.forms[0], "relQuestionGroupId").value;
            var relQ = findElement(document.forms[0], "relQuestionId").value;
            var relText = trim(findElement(document.forms[0], "relTextAnswer").value);
            var relO = findElement(document.forms[0], "relOptionAnswerId").value;
            var relAnswerType = findElement(document.forms[0], "relAnswerTypeId").value;
            if (relQG == '' && relQ == '') {
                alert('<bean:message key="common.harusPilih" arg0="Relevant To Question"/>');
                return false;
            }
            else if (relQG != "" && relQ != "" && relText == "") {
                alert('<bean:message key="errors.required" arg0="Relevant To Questions Value"/>');
                return false;
            }
            else if (relQG != "" && relQ != "" && relO == "" && isOption(relAnswerType)) {
                alert('<bean:message key="common.harusPilih" arg0="Relevant To Questions Value"/>');
                return false;
            }
            return true;
        }

        function isOption(answerType) {
            var arr = ['002', '003', '004', '005', '006', '007'];
            var idx = arr.join(',').indexOf(answerType);
            if (idx == -1)
                return false;
            else
                return true;
        }

        function checkRelAnswerType() {
            var type = document.forms[0].relAnswerTypeId.value;
            if (type == null || type == "") {
                document.getElementById("lookupValue").style.visibility = "hidden";
                return;
            }

            if (isOption(type)) {
                document.getElementById("lookupValue").style.visibility = "visible";
            }
            else {
                document.getElementById("lookupValue").style.visibility = "hidden";
            }
        }

        function checkRelQ() {
            var qg = document.forms[0].relQuestionGroupId.value;
            var q = document.forms[0].relQuestionId.value;
            if (qg != "" && q != "") {
                document.getElementById("relRequired").style.visibility = "visible";
            }
            else {
                document.getElementById("relRequired").style.visibility = "hidden";
            }
        }

        function init() {
            checkRelAnswerType();
            checkRelQ();
        }
    </script>

    <body onload="init();">
    <html:form action="/feature/question/Relevant.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="questionGroupId"/>
        <html:hidden property="questionId"/>
        <html:hidden property="relSeqno"/>
        <html:hidden property="relQuestionGroupId"/>
        <html:hidden property="relQuestionId"/>
        <html:hidden property="relAnswerTypeId"/>
        <html:hidden property="relOptionAnswerId"/>
        <html:hidden property="previousUri"/>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.question.relevant"/><bean:write
                            property="questionGroupName" name="relevantForm"/> - <bean:write property="questionLabel"
                                                                                             name="relevantForm"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.relevant.show"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="relevantForm" property="relQuestionLabel" size="80" maxlength="150"
                                   styleClass="TextBox"/>
                        <a href="javascript:openQuestion('<bean:write property="questionGroupId" name="relevantForm"/>','<bean:write property="questionId" name="relevantForm"/>','Question','scrollbars=yes,width=640,height=480,resizable=yes,top=0,left=0')">
                            <img src="../../../images/icon/IconLookUp.gif" alt="Pilih Question" border="0" width="16"
                                 height="16" title="Lookup Question">
                        </a>&nbsp;
                        <a href="javascript:clearRelQuestion();">
                            <img src="../../../images/icon/IconDelete.gif" alt="Pilih Question" border="0" width="16"
                                 height="16" title="Clear Question">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.answered"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="relevantForm" property="relTextAnswer" size="80" maxlength="500"
                                   styleClass="TextBox"/><span id="relRequired"><font color="#FF0000"
                                                                                      id="maxlength_required">*)<html:errors
                            property="relTextAnswer"/></font></span>
                        <span id="lookupValue">
	  					<a href="javascript:openOptionAnswer('OptionAnswer','scrollbars=yes,width=640,height=480,resizable=yes,top=0,left=0')">
      						<img src="../../../images/icon/IconLookUp.gif" alt="Pilih OptionAnswer" width="16"
                                 height="16" border="0" title="Lookup Question"></a> &nbsp;&nbsp;
	  					<a href="javascript:clearRelAnswer();">
	  						<img src="../../../images/icon/IconDelete.gif" alt="Pilih Question" width="16" height="16"
                                 border="0" title="Clear Answer">
	  					</a>
     				</span>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:actionSave()"><img src="../../../images/button/ButtonAdd.png" alt="Add"
                                                               width="100" height="31" border="0"></a>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="30%" align="center" rowspan="2"><bean:message key="common.group"/></td>
                    <td width="35%" align="center" rowspan="2"><bean:message key="common.label"/></td>
                    <td width="30%" align="center" rowspan="2"><bean:message key="common.answer"/></td>
                    <td width="5%" align="center" colspan="2"><bean:message key="common.action"/></td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="5%" align="center"><bean:message key="common.delete"/></td>
                </tr>
                <logic:notEmpty name="list" scope="request">
                    <logic:iterate id="data" name="list" scope="request" indexId="i">
                        <%
                            String classColor = (i % 2) != 1 ? "evalcol" : "oddcol";
                        %>
                        <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=classColor%>'">
                            <td><bean:write name="data" property="relQuestionGroupName"/></td>
                            <td><bean:write name="data" property="relQuestionLabel"/></td>
                            <td><bean:write name="data" property="relTextAnswer"/></td>
                            <td align="center">
                                <a title="Delete"
                                   href="javascript:actionDelete('<bean:write name="data" property="relSeqno"/>');">
                                    <img src="../../../images/icon/IconDelete.gif" width="16" height="16" border="0"/>
                                </a>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="list" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="8" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:actionBack();"><img src="../../../images/button/ButtonBack.png" width="100"
                                                                height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>