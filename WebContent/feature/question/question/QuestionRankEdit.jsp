<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">

            function movePage(taskID) {
                if (taskID == "SaveRank" || taskID == "UpdateRank") {
                }

                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

        </script>
    </head>
    <body>
    <html:form action="/feature/question/Question.do" method="POST">
        <html:hidden property="isMandatory" value="1"/>
        <html:hidden property="isReadonly" value="0"/>
        <html:hidden property="isVisible" value="1"/>
        <html:hidden property="isActive" value="1"/>
        <html:hidden property="task"/>
        <html:hidden property="usage_type"/>
        <html:hidden property="searchType"/>
        <html:hidden property="searchValue"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <html:hidden property="previousUri"/>
        <html:hidden property="currentUri"/>
        <html:hidden property="questionGroupId"/>
        <logic:equal name="questionForm" property="task" value="EditRank" scope="request">
            <html:hidden property="questionId"/>
        </logic:equal>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <bean:message key="feature.question"/> -
                        <logic:equal name="questionForm" property="task" value="EditRank" scope="request"><bean:message
                                key="common.edit"/></logic:equal>
                        <logic:equal name="questionForm" property="task" value="AddRank" scope="request"><bean:message
                                key="common.add"/></logic:equal></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>

            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="headercol" colspan="2" align="center"><bean:message key="feature.question.detail"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.group.name"/></td>
                    <td class="Edit-Right-Text"><html:hidden property="questionGroupName" write="true"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.label"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="questionForm" property="questionLabel" size="80" maxlength="150"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="questionLabel"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.type"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal name="questionForm" property="task" value="AddRank" scope="request">
                            <html:select styleId="answerType" property="answerType" onchange="cekPilihanAnswerType();">
                                <html:option value=""><bean:message key="common.choose.one"/></html:option>
                                <logic:notEmpty name="listAnswerType" scope="request">
                                    <html:options collection="listAnswerType" property="keyValue"
                                                  labelProperty="label"/>
                                </logic:notEmpty>
                            </html:select>
                            <font color="#FF0000">*)<html:errors property="answerType"/></font>
                        </logic:equal>
                        <logic:notEqual name="questionForm" property="task" value="AddRank" scope="request">
                            <html:hidden property="answerType"/>
                            <html:hidden property="answerTypeName" write="true"/>
                        </logic:notEqual>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.sequence.order"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="questionForm" property="lineSeqOrder" size="5" maxlength="5"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="lineSeqOrder"/></font></td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <% String action = ""; %>
                        <logic:equal value="AddRank" property="task" name="questionForm">
                            <% action = "SaveRank"; %>
                        </logic:equal>
                        <logic:notEqual value="AddRank" property="task" name="questionForm">
                            <% action = "UpdateRank"; %>
                        </logic:notEqual>
                        <a href="javascript:movePage('<%=action%>');">
                            <img src="../../../images/button/ButtonSubmit.png" alt="Action <%=action %>"
                                 title="<bean:message key="tooltip.action"/> <%=action %>" width="100" height="31"
                                 border="0">
                        </a>
                        &nbsp;
                        <a href="javascript:movePage('LoadRank');">
                            <img src="../../../images/button/ButtonCancel.png" alt="Cancel"
                                 title="<bean:message key="tooltip.cancel"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>