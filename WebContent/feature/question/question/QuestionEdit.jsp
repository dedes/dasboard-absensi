<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">

            function maxlength_required() {//Text , Numeric, Decimal
                var answerType = $("#answerType").val();
                if (answerType == '001' || answerType == '008' ||
                    answerType == '009' || answerType == '027' || answerType == '028') {
                    var maxLength = trim(document.forms[0].maxLength.value);
                    if (maxLength == "") {
                        alert('<bean:message key="errors.required" arg0="Max Text Length"/>');
                        return false;
                    }
                }
                return true;
            }

            function openQuestion(questionGroupId, winName, features) {
                var toUrl = '<html:rewrite action="/feature/lookup/Question"/>' +
                    '?questionGroupId=' + questionGroupId;
                <logic:equal name="questionForm" property="task" value="Edit" scope="request">
                toUrl = toUrl + '&questionId=<bean:write name="questionForm" property="questionId"/>';
                </logic:equal>
                window.open(toUrl, winName, features);
            }

            function openOptionAnswer(winName, features) {
                var questionGroupId, questionId;
                questionGroupId = document.forms[0].relQuestionGroupId.value;
                questionId = document.forms[0].relQuestionId.value;
                var toUrl = '<html:rewrite action="/feature/lookup/OptionAnswer"/>' +
                    '?questionGroupId=' + questionGroupId +
                    '&questionId=' + questionId;
                window.open(toUrl, winName, features);
            }

            function movePage(taskID) {
                if (taskID == "Save" || taskID == "Update") {
//             if (!lookup_required()) {
// 				return;
//             }
                    if (!maxlength_required()) {
                        return;
                    }
                    //if (!relvalue_required()) {
                    //return;
                    //}
                }

                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }

            function isOption(answerType) {
                //multiple
                var arr = ['002', '003', '004', '005', '006', '007', '018', '026'];
                var idx = arr.join(',').indexOf(answerType);
                if (idx == -1)
                    return false;
                else
                    return true;
            }


            //     function lookup_required() {
            //     	var answerType = $("#answerType").val();
            //     	if (answerType == "015" || answerType == '016'){  //Lookup
            //         	var lookupCode = $('#lookupCode').val();
            //     		if ($.inArray(lookupCode, ['- Choose One -','']) != -1) {
            // 				alert('<bean:message key="common.harusPilih" arg0="Lookup"/>');
            // 				return false;
            //     		}
            //     	}
            //     	return true;
            //     }

            function relvalue_required() {
                var relQG = findElement(document.forms[0], "relQuestionGroupId").value;
                var relQ = findElement(document.forms[0], "relQuestionId").value;
                var relText = trim(findElement(document.forms[0], "relTextAnswer").value);
                var relO = findElement(document.forms[0], "relOptionAnswerId").value;
                var relAnswerType = findElement(document.forms[0], "relAnswerTypeId").value;
                if (relQG != "" && relQ != "" && relText == "") {
                    alert('<bean:message key="errors.required" arg0="Relevant To Questions Value"/>');
                    return false;
                }
                else if (relQG != "" && relQ != "" && relO == "" && isOption(relAnswerType)) {
                    alert('<bean:message key="common.harusPilih" arg0="Relevant To Questions Value"/>');
                    return false;
                }
                return true;
            }

            function cekPilihanAnswerType() {
                var answerType = $("#answerType").val();
// 		if (answerType == "015" || answerType == "016"){
// 			<logic:equal name="questionForm" property="task" value="Add" scope="request">reloadLookupDropdown(answerType);</logic:equal>
// 			document.getElementById("maxInf").style.visibility = "hidden";	
// 			document.getElementById("maxlength_required").style.visibility = "hidden";
// 			document.forms[0].maxLength.disabled = true;
// 			document.forms[0].regexPattern.disabled = true;
// 			document.forms[0].isReadonly.disabled = false;
// 		}
// 		else 

                toggle_treshold(0);
                if (answerType == '001' || answerType == '008' ||
                    answerType == '009' || answerType == '027' || answerType == '028') {
                    document.getElementById("maxInf").style.visibility = "hidden";
                    document.getElementById("maxlength_required").style.visibility = "visible";
                    document.forms[0].maxLength.disabled = false;
                    document.forms[0].regexPattern.disabled = false;
                    document.forms[0].isReadonly.disabled = false;
                } else if (answerType == '030') {//treshold
                    toggle_treshold(1);
                } else {
                    document.forms[0].regexPattern.disabled = true;
                    document.getElementById("maxlength_required").style.visibility = "hidden";

                    if (answerType == '010' || answerType == '012') {
                        document.forms[0].maxLength.disabled = false;
                        document.getElementById("maxInf").style.visibility = "visible";
                    }
                    else {
                        document.forms[0].maxLength.disabled = true;
                        document.getElementById("maxInf").style.visibility = "hidden";
// 				$("#minchooseval_id").val('0');
// 				document.forms[0].min_choose.disabled = true;
                        $("#usemin_id").hide();
                        $("#minchoose_id").hide();
                    }

                    if (answerType == '006' || answerType == '007' || //dropdown
                        answerType == '004' || answerType == '005') { //radio
                        document.forms[0].isReadonly.disabled = false;
                    } else if (answerType == '002' || answerType == '003' || //Multiple
                        answerType == '018' || answerType == '026') {
                        var use_min = $("#val_use_min").val();
// 				document.getElementById("usemin_id").style.visibility = "true";
                        $("#usemin_id").show();
// 				console.log(use_min);
                        if (use_min == '1' || use_min == 'on') {
                            document.forms[0].min_choose.disabled = false;
                            $("#minchoose_id").show();
                            $("#val_use_min").attr("checked", "checked");
                        } else {
                            $("#minchooseval_id").val('0');
                            document.forms[0].min_choose.disabled = true;
                            $("#minchoose_id").hide();
                            $("#val_use_min").attr("checked", false);
                        }
                    } else {
                        document.forms[0].isReadonly.disabled = true;
                        $("#minchooseval_id").val('0');
                        document.forms[0].min_choose.disabled = true;
                        $("#usemin_id").hide();
                        $("#minchoose_id").hide();
                    }
                }
            }

            function toggle_treshold(type) {
                if (type == 1) {
                    document.forms[0].minval.disabled = false;
                    document.forms[0].maxval.disabled = false;
                    $("#min_treshold_id").show();
                    $("#max_treshold_id").show();
                    document.getElementById("maxlength_required").style.visibility = "visible";
                    document.getElementById("maxInf").style.visibility = "hidden";
                    document.forms[0].maxLength.disabled = false;
                    document.forms[0].regexPattern.disabled = false;
                    document.forms[0].isReadonly.disabled = true;
                } else {
                    $("#min_treshold_id").hide();
                    $("#max_treshold_id").hide();
                }
            }

            function toggle_usemin() {
                var use_min = $("#val_use_min").val();
                if (use_min == '1') {
                    use_min = '0';
                } else {
                    use_min = '1';
                }
                $("#val_use_min").val(use_min);

//     	console.log(use_min);
                if (use_min == '1') {
                    document.forms[0].min_choose.disabled = false;
                    $("#minchoose_id").show();
                } else {
                    $("#minchooseval_id").val('0');
                    document.forms[0].min_choose.disabled = true;
                    $("#minchoose_id").hide();
                }
            }

            //     function reloadLookupDropdown(answerType) {
            // 		$('#lookupCode').html("");
            // 		$('#lookupCode').append($("<option/>").text('- Choose One -'));
            // 		Question.getLookupCombo(answerType, function(data) {
            // 			if (data.length > 0) {
            // 				$.each(data, function(idx, obj){
            // 					$('#lookupCode').append($("<option/>").val(obj.keyValue).text(obj.label));
            // 				});
            // 			}
            // 		});
            //     }


            function clearRelQuestion() {
                document.forms[0].relQuestionGroupId.value = "";
                document.forms[0].relQuestionId.value = "";
                document.forms[0].relQuestionLabel.value = "";
                clearRelAnswer();
                document.getElementById("relRequired").style.visibility = "hidden";
                document.getElementById("lookupValue").style.visibility = "hidden";
            }

            function clearRelAnswer() {
                document.forms[0].relOptionAnswerId.value = "";
                document.forms[0].relTextAnswer.value = "";
            }

            function setQuestion(qg, kode, lbl, tipe) {
                document.forms[0].relQuestionGroupId.value = qg;
                document.forms[0].relQuestionId.value = kode;
                document.forms[0].relQuestionLabel.value = lbl;
                document.forms[0].relAnswerTypeId.value = tipe;
                clearRelAnswer();
                document.getElementById("relRequired").style.visibility = "visible";
            }

            function setOptionAnswer(o, lbl) {
                document.forms[0].relOptionAnswerId.value = o;
                document.forms[0].relTextAnswer.value = lbl;
            }

            function init() {
                $("#usemin_id").hide();
                $("#minchoose_id").hide();
                cekPilihanAnswerType();
            }
        </script>
    </head>
    <body onload="init();">
    <html:form action="/feature/question/Question.do" method="POST">
        <html:hidden property="task"/>
        <html:hidden property="usage_type"/>
        <html:hidden property="searchType"/>
        <html:hidden property="searchValue"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <html:hidden property="previousUri"/>
        <html:hidden property="currentUri"/>
        <html:hidden property="questionGroupId"/>
        <logic:equal name="questionForm" property="task" value="Edit" scope="request">
            <html:hidden property="questionId"/>
        </logic:equal>
        <%-- 	<%@include file="../../../Error.jsp"%> --%>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <bean:message key="feature.question"/> -
                        <logic:equal name="questionForm" property="task" value="Edit" scope="request"><bean:message
                                key="common.edit"/></logic:equal>
                        <logic:equal name="questionForm" property="task" value="Add" scope="request"><bean:message
                                key="common.add"/></logic:equal></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>

            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="headercol" colspan="2" align="center"><bean:message key="feature.question.detail"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.group.name"/></td>
                    <td class="Edit-Right-Text"><html:hidden property="questionGroupName" write="true"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.label"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="questionForm" property="questionLabel" size="80" maxlength="150"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="questionLabel"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.type"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal name="questionForm" property="task" value="Add" scope="request">
                            <html:select styleId="answerType" property="answerType" onchange="cekPilihanAnswerType();">
                                <html:option value=""><bean:message key="common.choose.one"/></html:option>
                                <logic:notEmpty name="listAnswerType" scope="request">
                                    <html:options collection="listAnswerType" property="keyValue"
                                                  labelProperty="label"/>
                                </logic:notEmpty>
                            </html:select>
                            <font color="#FF0000">*)<html:errors property="answerType"/></font>
                        </logic:equal>
                        <logic:equal name="questionForm" property="task" value="Edit" scope="request">
                            <html:hidden property="answerType" styleId="answerType"/>
                            <html:hidden property="answerTypeName" write="true"/>
                        </logic:equal>
                    </td>
                </tr>
                <!-- 		    <tr id="lookupPart"> -->
                <!-- 		      	<td width="20%" class="Edit-Left-Text">Lookup</td> -->
                <!-- 		      	<td class="Edit-Right-Text"> -->
                    <%-- 				      <html:select styleId="lookupCode" property="lookupCode"> --%>
                    <%-- 				        <html:option value="">- Choose One -</html:option> --%>
                    <%-- 			            <logic:notEmpty name="listLookup" scope="request"> --%>
                    <%-- 				          <html:options collection="listLookup" property="keyValue" labelProperty="label"/> --%>
                    <%-- 			            </logic:notEmpty> --%>
                    <%-- 				      </html:select> --%>
                    <%-- 					<html:checkbox property="lookupCode"> Yes</html:checkbox> --%>
                <!-- 					<font color="#FF0000" id="lookup_required">*)</font>  -->
                <!-- 		      	</td> -->
                <!-- 		    </tr> -->
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.sequence.order"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="questionForm" property="lineSeqOrder" size="5" maxlength="5"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="lineSeqOrder"/></font></td>
                </tr>
                <tr>
                    <td class="headercol" colspan="2" align="center"><bean:message
                            key="feature.question.parameter"/></td>
                </tr>
                <tr id="min_treshold_id">
                    <td class="Edit-Left-Text"><bean:message key="feature.question.min.value"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="questionForm" property="minval" size="5" maxlength="5" styleClass="TextBox"
                                   styleId="min_val_treshold_id"
                                   onkeydown="javascript:return numericInputKoma(event);"/>
                        <font color="#FF0000">*)<html:errors property="minval"/></font>
                    </td>
                </tr>
                <tr id="max_treshold_id">
                    <td class="Edit-Left-Text"><bean:message key="feature.question.max.value"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="questionForm" property="maxval" size="5" maxlength="5" styleClass="TextBox"
                                   styleId="max_val_treshold_id"
                                   onkeydown="javascript:return numericInputKoma(event);"/>
                        <font color="#FF0000">*)<html:errors property="maxval"/></font>
                    </td>
                </tr>
                <tr id="usemin_id">
                    <td class="Edit-Left-Text"><bean:message key="feature.question.use.min.choose"/></td>
                    <td class="Edit-Right-Text"><html:checkbox name="questionForm" value="1" property="use_min"
                                                               styleId="val_use_min"
                                                               onchange="javascript:toggle_usemin();"/></td>
                </tr>
                <tr id="minchoose_id">
                    <td class="Edit-Left-Text"><bean:message key="feature.question.min.choose"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="questionForm" property="min_choose" size="5" maxlength="5" styleClass="TextBox"
                                   styleId="minchooseval_id" onkeydown="javascript:return numericInput(event);"/>
                        <font color="#FF0000">*)<html:errors property="min_choose"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.max.length"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="questionForm" property="maxLength" size="4" maxlength="4" styleClass="TextBox"
                                   onkeydown="javascript:return numericInput(event);"/>
                        <font color="#FF0000" id="maxlength_required">*)<html:errors property="maxLength"/></font>
                        <span id="maxInf"></span>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.expression.pattern"/></td>
                    <td class="Edit-Right-Text"><html:text name="questionForm" property="regexPattern" size="40"
                                                           maxlength="30" styleClass="TextBox"/></td>
                </tr>
                <tr>
                    <td class="headercol" colspan="2" align="center"><bean:message key="feature.question.status"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.mandatory"/></td>
                    <td class="Edit-Right-Text"><html:checkbox name="questionForm" value="1"
                                                               property="isMandatory"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.read.only"/></td>
                    <td class="Edit-Right-Text"><html:checkbox name="questionForm" value="1"
                                                               property="isReadonly"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.visible.handset"/></td>
                    <td class="Edit-Right-Text"><html:checkbox name="questionForm" value="1" property="isVisible"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.active.header"/></td>
                    <td class="Edit-Right-Text"><html:checkbox name="questionForm" value="1" property="isActive"/></td>
                </tr>
            </table>
            <!-- <tr>
      <td class="tdganjil">Template Parameter Name</td>
      <td class="tdgenap">
        <html:text name="questionForm" property="paramName" size="25" maxlength="25" styleClass="TextBox"/>
		<font color="#FF0000">*)</font></td>
    </tr> -->
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <% String action = ""; %>
                        <logic:equal value="Add" property="task" name="questionForm">
                            <% action = Global.WEB_TASK_INSERT; %>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="questionForm">
                            <% action = Global.WEB_TASK_UPDATE; %>
                        </logic:equal>
                        <a href="javascript:movePage('<%=action%>');">
                            <img src="../../../images/button/ButtonSubmit.png" alt="Action <%=action %>"
                                 title="<bean:message key="tooltip.action"/> <%=action %>" width="100" height="31"
                                 border="0">
                        </a>
                        &nbsp;
                        <a href="javascript:movePage('Load');">
                            <img src="../../../images/button/ButtonCancel.png" alt="Cancel"
                                 title="<bean:message key="tooltip.cancel"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>