<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function actionBack() {
                document.forms[0].task.value = 'Back';
                document.forms[0].submit();
            }

            function movePage(taskID, questionId) {
                document.forms[0].task.value = taskID;
                document.forms[0].questionId.value = questionId;
                document.forms[0].submit();
            }

            function actionDelete(questionId) {
                if (!confirm('<bean:message key="common.confirmdelete" />'))
                    return;
                document.forms[0].task.value = 'Delete';
                document.forms[0].questionId.value = questionId;
                document.forms[0].submit();
            }

            function moveToPage(questionId, thePlace) {
                var path;
                var curUri = '<bean:write name="questionForm" property="currentUri"/>';
                path = '<%=request.getContextPath()%>';
                document.forms[0].task.value = 'Load';
                document.forms[0].questionId.value = questionId;
                if (thePlace == "Relevant") {
                    findElement(document.forms[0], "paging.currentPageNo").value = 1;
                    findElement(document.forms[0], "paging.dispatch").value = null;
                    document.forms[0].action = path
                        + '/feature/question/Relevant.do?previousUri='
                        + curUri;
                }
                document.forms[0].submit();
            }
        </script>


    <body>
    <html:form action="/feature/question/Question.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="questionGroupId"/>
        <html:hidden property="usage_type"/>
        <html:hidden property="questionId"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="previousUri"/>
        <html:hidden property="currentUri"/>
        <html:hidden property="paging.totalRecord"/>
        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.question.list.of"/> <html:hidden
                            property="questionGroupName" write="true"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.question.label"/><html:hidden
                            property="searchType" value="10"/></td>
                    <td class="Edit-Right-Text"><html:text property="searchValue" size="25" maxlength="30"
                                                           styleClass="TextBox"/></td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search"
                                 title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="20%" align="center" rowspan="2"><bean:message key="feature.question"/></td>
                    <td width="10%" align="center" rowspan="2"><bean:message key="feature.question.type"/></td>
                    <td width="5%" align="center" rowspan="2"><bean:message key="feature.sequence.order"/></td>
                    <td width="25%" align="center" colspan="5"><bean:message key="feature.question.status"/></td>
                    <td width="5%" align="center" rowspan="2"><bean:message key="feature.question.relevant"/></td>
                    <td width="5%" align="center" rowspan="2"><bean:message key="feature.question.option.set"/></td>
                    <td width="10%" align="center" colspan="2"><bean:message key="common.action"/></td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="5%" align="center"><bean:message key="common.mandatory"/></td>
                    <td width="5%" align="center"><bean:message key="feature.question.read.only"/></td>
                    <td width="5%" align="center"><bean:message key="common.visible"/></td>
                    <td width="5%" align="center"><bean:message key="feature.question.multiple.answer"/></td>
                    <td width="5%" align="center"><bean:message key="common.active.header"/></td>
                    <td width="5%" align="center"><bean:message key="common.edit"/></td>
                    <td width="5%" align="center"><bean:message key="common.delete"/></td>
                </tr>
                <logic:notEmpty name="listQuestion" scope="request">
                    <logic:iterate id="data" name="listQuestion" scope="request" indexId="i">
                        <%
                            String classColor = ((i % 2) != 1) ? "evalcol" : "oddcol";
                        %>
                        <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=classColor%>'">
                            <td><bean:write name="data" property="questionLabel"/></td>
                            <td><bean:write name="data" property="answerTypeName"/></td>
                            <td align="right"><bean:write name="data" property="lineSeqOrder"/></td>
                            <td align="center">
                                <logic:equal name="data" property="isMandatory" value="1">
                                    <img src="../../../images/icon/IconYes.gif" border="0" width="16" height="16"/>
                                </logic:equal>
                                <logic:notEqual name="data" property="isMandatory" value="1">
                                    <img src="../../../images/icon/IconInactive.gif" border="0" width="16" height="16"/>
                                </logic:notEqual>
                            </td>
                            <td align="center">
                                <logic:equal name="data" property="isReadonly" value="1">
                                    <img src="../../../images/icon/IconYes.gif" border="0" width="16" height="16"/>
                                </logic:equal>
                                <logic:notEqual name="data" property="isReadonly" value="1">
                                    <img src="../../../images/icon/IconInactive.gif" border="0" width="16" height="16"/>
                                </logic:notEqual>
                            </td>
                            <td align="center">
                                <logic:equal name="data" property="isVisible" value="1">
                                    <img src="../../../images/icon/IconInactive.gif" border="0" width="16" height="16"/>
                                </logic:equal>
                                <logic:notEqual name="data" property="isVisible" value="1">
                                    <img src="../../../images/icon/IconYes.gif" border="0" width="16" height="16"/>
                                </logic:notEqual>
                            </td>
                            <td align="center">
                                <logic:equal name="data" property="isHaveAnswer" value="1">
                                    <img src="../../../images/icon/IconYes.gif" border="0" width="16" height="16"/>
                                </logic:equal>
                                <logic:notEqual name="data" property="isHaveAnswer" value="1">
                                    <img src="../../../images/icon/IconInactive.gif" border="0" width="16" height="16"/>
                                </logic:notEqual>
                            </td>
                            <td align="center">
                                <logic:equal name="data" property="isActive" value="1">
                                    <html:img border="0" src="../../../images/icon/IconActive.gif" width="16"
                                              height="16"/>
                                </logic:equal>
                                <logic:notEqual name="data" property="isActive" value="1">
                                    <html:img border="0" src="../../../images/icon/IconInactive.gif" width="16"
                                              height="16"/>
                                </logic:notEqual>
                            </td>
                            <td align="center">
                                <a href="javascript:moveToPage('<bean:write name="data" property="questionId"/>','Relevant');">
                                    <img src="../../../images/icon/IconRelevant.gif" width="16" height="16" border="0"/>
                                </a>
                            </td>
                            <td align="center">
                                <logic:equal name="data" property="isHaveAnswer" value="1">
                                    <logic:notEqual name="data" property="answerType" value="026">
                                        <a href="javascript:movePage('OptionAnswer','<bean:write name="data" property="questionId"/>');">
                                            <img src="../../../images/icon/IconOption.gif" width="16" height="16"
                                                 border="0"/>
                                        </a>
                                    </logic:notEqual>
                                </logic:equal>
                            </td>
                            <td align="center">
                                <a title="Edit"
                                   href="javascript:movePage('Edit','<bean:write name="data" property="questionId"/>');">
                                    <img src="../../../images/icon/IconEdit.gif" width="16" height="16" border="0"/>
                                </a>
                            </td>
                            <td align="center">
                                <a title="Delete"
                                   href="javascript:actionDelete('<bean:write name="data" property="questionId"/>');">
                                    <img src="../../../images/icon/IconDelete.gif" width="16" height="16" border="0"/>
                                </a>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="listQuestion" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="12" align="center">
                            <font class="errMsg"><bean:message key="common.listnotfound"/></font>
                        </td>
                    </tr>
                </logic:empty>
            </table>
            <logic:notEmpty name="listQuestion" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionForm,'First')" title="First">
                                <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionForm,'Prev')"
                                       title="Previous">
                                <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="questionForm" property="paging.currentPageNo" size="3" maxlength="6"
                                       styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <html:hidden property="paging.totalRecord" write="false"/>
                            <a href="javascript:moveToPageNumber(document.questionForm,'Go');">
                                <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                     border="0"/>
                            </a>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionForm,'Next')" title="Next">
                                <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionForm,'Last')" title="Last">
                                <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:define id="currentPage" name="questionForm" property="paging.currentPageNo"
                                         type="java.lang.Integer"/>
                            <bean:define id="total" name="questionForm" property="paging.totalRecord"
                                         type="java.lang.Integer"/>
                            <bean:define id="perpage" name="questionForm" property="paging.rowPerPage"
                                         type="java.lang.Integer"/>
                            <%
                                int startShow = 1;
                                int endShow = 0;
                                if (currentPage > 1) {
                                    int tempstart = ((currentPage - 1) * perpage) + 1;
                                    int tempend = currentPage * perpage;
                                    if (total < tempend) {
                                        tempend = total;
                                    }
                                    startShow = tempstart;
                                    endShow = tempend;
                                } else {
                                    endShow = total;
                                }
                            %>
                            <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                            <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Add','')"><img src="../../../images/button/ButtonAdd.png"
                                                                     alt="Add" width="100" height="31" border="0"></a>
                        <a href="javascript:actionBack();"><img src="../../../images/button/ButtonBack.png" width="100"
                                                                height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>

