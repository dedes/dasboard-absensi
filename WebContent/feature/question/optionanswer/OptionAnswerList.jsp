<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">

            function movePage(taskID, optionAnswerId) {
                document.forms[0].task.value = taskID;
                document.forms[0].optionAnswerId.value = optionAnswerId;
                if (taskID == "OptionAnswerSearch" && optionAnswerId != null) {
                    document.forms[0].searchValue.value = "";
                    return;
                }
                document.forms[0].submit();
            }

            function actionDelete(optionAnswerId) {
                if (!confirm('<bean:message key="common.confirmdelete" />'))
                    return;
                document.forms[0].task.value = 'OptionAnswerDelete';
                document.forms[0].optionAnswerId.value = optionAnswerId;
                document.forms[0].submit();
            }
            function search(optionAnswerId) {
                if (!confirm('<bean:message key="common.confirmdelete" />'))
                    return;
                document.forms[0].task.value = 'OptionAnswerDelete';
                document.forms[0].optionAnswerId.value = optionAnswerId;
                document.forms[0].submit();
            }
            
        </script>


    <body>
    <html:form action="/feature/question/Question.do" method="post">
        <html:hidden property="task" value="OptionAnswerLoad"/>
        <html:hidden property="questionGroupId"/>
        <html:hidden property="questionGroupName"/>
        <html:hidden property="questionId"/>
        <html:hidden property="optionAnswerId"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="previousUri"/>
        <%@include file="../../../Error.jsp" %>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.answer.options.of"/><html:hidden
                            property="questionGroupName" write="true"/> - <html:hidden property="questionLabel"
                                                                                       write="true"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.label"/> <html:hidden property="searchType"
                                                                                               value="10"/></td>
                    <td class="Edit-Right-Text"><html:text property="searchValue" size="25" maxlength="30"
                                                           styleClass="TextBox"/></td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('OptionAnswerSearch');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search"
                                 title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="5%" align="center" rowspan="2"><bean:message key="feature.question.options.legacy"/></td>
                    <td width="20%" align="center" rowspan="2"><bean:message key="common.label"/></td>
                    <td width="10%" align="center" colspan="2"><bean:message key="feature.answer.options.status"/></td>
                    <td width="10%" align="center" colspan="2"><bean:message key="common.action"/></td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="5%" align="center"><bean:message key="feature.sequence.order"/></td>
                    <td width="5%" align="center"><bean:message key="common.active"/></td>
                    <td width="5%" align="center"><bean:message key="common.edit"/></td>
                    <td width="5%" align="center"><bean:message key="common.delete"/></td>
                </tr>
                <logic:notEmpty name="listOptionAnswer" scope="request">
                <logic:iterate id="data" name="listOptionAnswer" scope="request" indexId="i">
                <% String classColor = (i % 2) != 1 ? "evalcol" : "oddcol"; %>
                <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                    onmouseout="this.className='<%=classColor%>'">
                    <td><bean:write name="data" property="legacyCode"/></td>
                    <td><bean:write name="data" property="optionAnswerLabel"/></td>
                    <td align="right"><bean:write name="data" property="lineSeqOrder"/></td>
                    <td align="center">
                        <logic:equal name="data" property="isActive" value="1">
                            <html:img border="0" src="../../../images/icon/IconActive.gif" width="16" height="16"/>
                        </logic:equal>
                        <logic:notEqual name="data" property="isActive" value="1">
                            <html:img border="0" src="../../../images/icon/IconInactive.gif" width="16" height="16"/>
                        </logic:notEqual>
                    </td>
                    <td align="center">
                        <a title="Edit"
                           href="javascript:movePage('OptionAnswerEdit','<bean:write name="data" property="optionAnswerId"/>');">
                            <img src="../../../images/icon/IconEdit.gif" width="16" height="16" border="0"/>
                        </a>
                    </td>
                    <td align="center">
                        <a title="Delete"
                           href="javascript:actionDelete('<bean:write name="data" property="optionAnswerId"/>');">
                            <img src="../../../images/icon/IconDelete.gif" width="16" height="16" border="0"/>
                        </a>
                    </td>
                    </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listOptionAnswer" scope="request">
                <tr class="evalcol">
                    <td bgcolor="FFFFFF" colspan="6" align="center"><font class="errMsg"><bean:message
                            key="common.listnotfound"/></font></td>
                </tr>
                </logic:empty>
            </table>
            <logic:notEmpty name="listOptionAnswer" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionForm,'First')" title="First">
                                <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionForm,'Prev')"
                                       title="Previous">
                                <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="questionForm" property="paging.currentPageNo" size="3" maxlength="6"
                                       styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <html:hidden property="paging.totalRecord" write="false"/>
                            <a href="javascript:moveToPageNumber(document.questionForm,'Go');">
                                <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                     border="0"/>
                            </a>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionForm,'Next')" title="Next">
                                <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.questionForm,'Last')" title="Last">
                                <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:define id="currentPage" name="questionForm" property="paging.currentPageNo"
                                         type="java.lang.Integer"/>
                            <bean:define id="total" name="questionForm" property="paging.totalRecord"
                                         type="java.lang.Integer"/>
                            <bean:define id="perpage" name="questionForm" property="paging.rowPerPage"
                                         type="java.lang.Integer"/>
                            <%
                                int startShow = 1;
                                int endShow = 0;
                                if (currentPage > 1) {
                                    int tempstart = ((currentPage - 1) * perpage) + 1;
                                    int tempend = currentPage * perpage;
                                    if (total < tempend) {
                                        tempend = total;
                                    }
                                    startShow = tempstart;
                                    endShow = tempend;
                                } else {
                                    endShow = total;
                                }
                            %>
                            <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                            <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('OptionAnswerAdd','')"><img
                                src="../../../images/button/ButtonAdd.png" alt="Add" width="100" height="31" border="0"></a>
                        <a href="javascript:movePage('BackToQuestion','');"><img
                                src="../../../images/button/ButtonBack.png" width="100" height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
        <html:hidden property="paging.totalRecord" write="false"/>
    </html:form>
    </body>
</html:html>

