<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>

    <head>
        <title><bean:message key="tittle.view.questionset"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function movePage(task) {
                var form = document.forms[0];
                form.task.value = task;
                form.submit();
            }
        </script>
    </head>

    <body oncontextmenu="return false;">
    <html:form action="/feature/view/Questions.do">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="schemeId"/>
        <div align="center">
            <%@include file="../../../Error.jsp" %>
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message
                            key="feature.view.assignment.questionset.template"/> <bean:write name="viewQuestionSetForm"
                                                                                             property="schemeDescription"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.desc"/></td>
                    <td class="Edit-Right-Text"><bean:write name="viewQuestionSetForm"
                                                            property="schemeDescription"/></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="20%" align="center"><bean:message key="feature.question.group"/></td>
                    <td width="30%" align="center"><bean:message key="feature.question"/></td>
                    <td width="20%" align="center"><bean:message key="feature.question.type"/></td>
                    <td width="30%" align="center"><bean:message key="feature.answer.options.detail"/></td>
                </tr>
                <logic:notEmpty name="listQuestionSet" scope="request">
                    <logic:iterate id="data" name="listQuestionSet" scope="request">
                        <bean:define id="firstHeader" scope="request" value="0" type="java.lang.String"/>
                        <logic:iterate id="question" name="data" indexId="idx">
                            <% String classColor = (idx.intValue() % 2 == 0) ? "evalcol" : "oddcol"; %>
                            <logic:equal value="0" name="firstHeader">
                                <tr class="headercol" height="10px"
                                    style="padding: 0px; border: 0px 0px 0px 0px; border-spacing: 0px; border-color: #FFF;">
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                            </logic:equal>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <logic:equal value="1" name="firstHeader">
                                    <td width="10%" align="left">&nbsp;</td>
                                </logic:equal>
                                <logic:equal value="0" name="firstHeader">
                                    <td width="10%"><bean:write name="question" property="questionGroupName"/></td>
                                    <bean:define id="firstHeader" scope="request" value="1" type="java.lang.String"/>
                                </logic:equal>
                                <td width="30%"><bean:write name="question" property="questionLabel"/>
                                    <logic:equal name="question" property="isMandatory" value="1">
                                        <font color="#FF0000" title="MandatoryField">*)<html:errors
                                                property="isMandatory"/></font>
                                    </logic:equal>
                                </td>
                                <td width="20%"><bean:write name="question" property="answerType"/></td>
                                <td width="40%"><logic:notEmpty name="question" property="optionAnswer">
                                    <bean:write name="question" property="optionAnswer"/></logic:notEmpty></td>
                            </tr>
                        </logic:iterate>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="listQuestionSet" scope="request">
                    <tr class="tdganjil">
                        <td bgcolor="FFFFFF" colspan="4" align="center"><font class="errMsg">
                            <bean:message key="common.listnotfound"/></font>
                        </td>
                    </tr>
                </logic:empty>
            </table>
            <table width="95%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%" height="30">
                        &nbsp;
                    </td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close();">
                            <html:img src="../../../images/button/ButtonClose.png" width="100" height="31"
                                      border="0"/></a></td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>
