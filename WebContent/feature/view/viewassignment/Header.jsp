<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr class="Top-Table-Header">
        <td class="Top-Table-Header-Left"></td>
        <td class="Top-Table-Header-Center"><bean:message key="feature.view.assignment.detail"/> [<bean:write
                name="viewAssignmentForm" property="mobileAssignmentId"/>]
        </td>
        <td class="Top-Table-Header-Right"></td>
    </tr>
</table>
<table width="95%" border="0" cellpadding="2" cellspacing="1" class="tableview">
    <tr>
        <td colspan="4" class="headercol" align="center"><strong><bean:message key="common.assignment"/></strong></td>
    </tr>
    <tr>
        <td width="20%" class="oddcol"><bean:message key="app.user.branch"/></td>
        <td width="30%" class="evalcol">
            <bean:write name="viewAssignmentForm" property="branchId"/> [<bean:write name="viewAssignmentForm"
                                                                                     property="branchName"/>]
        </td>
        <td width="20%" class="oddcol"><bean:message key="app.appform.appid"/></td>
        <td width="30%" class="evalcol"><bean:write name="viewAssignmentForm" property="applNo"/></td>
    </tr>
    <tr>
        <td width="20%" class="oddcol"><bean:message key="app.privilege.form"/></td>
        <td width="30%" class="evalcol">
            <html:hidden write="true" name="viewAssignmentForm" property="schemeId"/></td>
        <td width="20%" class="oddcol"><bean:message key="feature.scheme.description"/></td>
        <td width="30%" class="evalcol">
            <bean:write name="viewAssignmentForm" property="schemeDescription"/></td>
    </tr>
    <tr>
        <td width="20%" class="oddcol"><bean:message key="feature.precommittee.read.date"/></td>
        <td width="30%" class="evalcol"><bean:write name="viewAssignmentForm" property="retrieveDate"/></td>
        <td width="20%" class="oddcol"><bean:message key="feature.precommittee.submit.date"/></td>
        <td width="30%" class="evalcol"><bean:write name="viewAssignmentForm" property="submitDate"/></td>
    </tr>
    <tr>
        <td width="20%" class="oddcol"><bean:message key="app.assignment.notes"/></td>
        <td width="30%" class="evalcol" colspan="3" style="word-break:break-all;"><bean:write name="viewAssignmentForm"
                                                                                              property="notes"/></td>
    </tr>
    <tr>
        <td colspan="4" class="headercol" align="center"><strong><bean:message key="app.assignment.pic"/></strong></td>
    </tr>
    <tr>
        <td width="20%" class="oddcol"><bean:message key="app.assignment.pic"/></td>
        <td width="30%" class="evalcol"><bean:write name="viewAssignmentForm" property="surveyorId"/></td>
        <td width="20%" class="oddcol"><bean:message key="common.name"/></td>
        <td width="30%" class="evalcol"><bean:write name="viewAssignmentForm" property="surveyor"/></td>
    </tr>
    <tr>
        <td colspan="4" class="headercol" align="center"><strong><bean:message
                key="app.assignment.customerdetail"/></strong></td>
    </tr>
    <tr>
        <td width="20%" class="oddcol"><bean:message key="common.name"/></td>
        <td width="30%" class="evalcol" colspan="3"><bean:write name="viewAssignmentForm" property="customerName"/></td>
    </tr>
    <tr>
        <td width="20%" class="oddcol"><bean:message key="app.user.phone"/></td>
        <td width="30%" class="evalcol"><bean:write name="viewAssignmentForm" property="customerPhone"/></td>
        <td width="20%" class="oddcol"><bean:message key="app.assignment.cellularphone"/></td>
        <td width="30%" class="evalcol"><bean:write name="viewAssignmentForm" property="customerHP"/></td>
    </tr>
    <tr>
        <td width="20%" class="oddcol"><bean:message key="feature.branch.address"/></td>
        <td width="30%" class="evalcol" colspan="3"><bean:write name="viewAssignmentForm" property="customerAddress"/></td>
	</tr>
	
	
	<!-- IRFAN 17-12-2-18 -->
     <tr>
        <td width="20%" class="oddcol" ><bean:message key="feature.branch.rt"/></td>
    	<td width="30%" class="evalcol" ><bean:write name="viewAssignmentForm" property="rt"/></td>
   		<td width="20%" class="oddcol" ><bean:message key="feature.branch.rw"/></td>
    	<td width="30%" class="evalcol"><bean:write name="viewAssignmentForm" property="rw"/></td>
    </tr>
    <tr>
        <td width="20%" class="oddcol" ><bean:message key="feature.branch.kelurahan"/></td>
    	<td width="30%" class="evalcol" ><bean:write name="viewAssignmentForm" property="kelurahan"/></td>
    	<td width="20%" class="oddcol" ><bean:message key="feature.branch.kecamatan"/></td>
    	<td width="30%" class="evalcol" ><bean:write name="viewAssignmentForm" property="kecamatan"/></td>
   		
    </tr>
    <tr>
    	<td width="20%" class="oddcol" ><bean:message key="feature.branch.kabupaten"/></td>
    	<td width="30%" class="evalcol"><bean:write name="viewAssignmentForm" property="kabupaten"/></td>
        <td width="20%" class="oddcol" ><bean:message key="feature.branch.city"/></td>
    	<td width="30%" class="evalcol" ><bean:write name="viewAssignmentForm" property="city"/></td>
   		
    </tr>
    <tr>
    	<td width="20%" class="oddcol" ><bean:message key="feature.branch.propinsi"/></td>
    	<td width="30%" class="evalcol"><bean:write name="viewAssignmentForm" property="propinsi"/></td>
        <td width="20%" class="oddcol" ><bean:message key="feature.branch.kodePos"/></td>
    	<td width="30%" class="evalcol" ><bean:write name="viewAssignmentForm" property="kodePos"/></td>	
    </tr>
    <!-- END IRFAN 17-12-2-18 -->
    
    <tr>
        <td class="evalcol" colspan="4" align="right">
            <logic:notEmpty name="location" scope="request">
                <logic:notEmpty name="location" scope="request" property="lat">
                    <a href="javascript:openMap('Load','Map','<bean:write name="location" property="lat"/>','<bean:write name="location" property="lng"/>','350','20','1588');">
                        <img src="../../../images/button/ButtonOpenMap.png" width="100" height="31"
                             alt="Open Customer Address In Map" title="Open Customer Address In Map"/>
                    </a>
                </logic:notEmpty>
            </logic:notEmpty>
            <!-- 		  <logic:empty name="location" scope="request"> -->
                <%-- 				<a href="javascript:openMap('Map','<bean:write name="viewAssignmentForm" property="gpsLatitude"/>','<bean:write name="viewAssignmentForm" property="gpsLongitude"/>','350','20','1588');"> --%>
            <!-- 					<img src="../../../images/button/ButtonOpenMap.png" width="100" height="31" alt="Open Customer Address In Map" title="Open Customer Address In Map"/> -->
            <!-- 				</a> -->
            <!-- 		  </logic:empty> -->
        </td>
    </tr>
</table>