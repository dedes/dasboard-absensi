<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<% String key = ConfigurationProperties.getInstance().get(PropertiesKey.WEB_KEY); %>
<html:html>
    <html:base/>

    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function movePage(task) {
                var form = document.forms[0];
                form.task.value = task;
                form.submit();
            }
            function openMap(taskbefore, task, gpsLatitude, gpsLongitude, accuracy, questionGroupId, questionId) {
                var frm = document.forms[0];
                frm.taskView.value = taskbefore;
                frm.task.value = task;
                frm.gpsLatitude.value = gpsLatitude;
                frm.gpsLongitude.value = gpsLongitude;
                frm.accuracy.value = accuracy;
                frm.questionGroupId.value = questionGroupId;
                frm.questionId.value = questionId;

                frm.submit();
            }
        </script>
    </head>

    <body oncontextmenu="return false;">
    <bean:define id="actionPath" name="viewAssignmentForm" property="path" type="java.lang.String"/>
    <html:form action="<%=actionPath%>">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="mobileAssignmentId"/>
        <html:hidden property="path"/>
        <html:hidden property="mobileAssignmentId"/>
        <html:hidden property="gpsLatitude"/>
        <html:hidden property="gpsLongitude"/>
        <html:hidden property="accuracy"/>
        <html:hidden property="questionGroupId"/>
        <html:hidden property="questionId"/>
        <html:hidden property="taskView"/>

        <div align="center">
            <%@include file="../../../Error.jsp" %>
            <%@include file="Header.jsp" %>
            <%@include file="AnswerViewer.jsp" %>
            <table width="95%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%" height="30"></td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close();">
                            <html:img src="../../../images/button/ButtonClose.png" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>
