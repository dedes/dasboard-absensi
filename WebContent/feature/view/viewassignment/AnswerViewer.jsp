<%@page import="com.google.common.base.Strings" %>
<%@page import="treemas.application.feature.view.assignment.ViewAssignmentForm" %>
<%@page import="treemas.util.constant.AnswerType" %>
<%@page import="treemas.util.constant.PropertiesKey" %>
<%@page import="treemas.util.format.TreemasFormatter" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<% String mapKey = ConfigurationProperties.getInstance().get(PropertiesKey.WEB_STATIC_KEY); %>
<table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
    <tr>
        <td align="center" class="headercol" colspan="3"><bean:message
                key="feature.view.assignment.question.result"/></td>
    </tr>
    <tr>
        <td align="center" class="headercol" width="5%"><bean:message key="common.number"/></td>
        <td align="center" class="headercol" width="45%"><bean:message key="feature.question"/></td>
        <td align="center" class="headercol" width="50%"><bean:message key="common.answer"/></td>
    </tr>
    <logic:notEmpty name="listJawaban" scope="request">
        <bean:define id="firstHeader" scope="request" value="kosong" type="java.lang.String"/>
        <bean:define id="firstCount" scope="request" value="1" type="java.lang.String"/>
        <bean:define id="firstRow" scope="request" value="1" type="java.lang.String"/>
        <logic:iterate id="data" name="listJawaban" scope="request" indexId="idx">
            <% String classColor = (idx.intValue() % 2 == 0) ? "evalcol" : "oddcol"; %>
            <bean:define id="dataGroup" name="data" property="questionGroupName" type="java.lang.String"/>
            <logic:notEqual value="<%=dataGroup%>" name="firstHeader">
                <bean:define id="firstHeader" scope="request" value="<%=dataGroup%>" type="java.lang.String"/>
                <bean:define id="firstCount" scope="request" value="1" type="java.lang.String"/>
            </logic:notEqual>
            <logic:equal value="1" name="firstCount">
                <logic:equal value="0" name="firstRow">
                    <tr>
                        <td colspan="3" align="center" style="background-color: #FFF;">&nbsp;</td>
                    </tr>
                </logic:equal>
                <logic:equal value="1" name="firstRow">
                    <bean:define id="firstRow" scope="request" value="0" type="java.lang.String"/>
                </logic:equal>
                <tr>
                    <td colspan="3" align="center" class="headercol"><bean:write name="data"
                                                                                 property="questionGroupName"/></td>
                </tr>
                <bean:define id="firstCount" scope="request" value="0" type="java.lang.String"/>
            </logic:equal>
            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                onmouseout="this.className='<%=classColor%>'">
                <td width="5%" align="right"><%=idx.intValue() + 1%>
                </td>
                <td width="45%"><bean:write name="data" property="questionLabel"/></td>
                <td width="50%" style="word-break:break-all;">
                    <logic:equal value="1" name="data" property="isImage">
                        <logic:equal value="1" name="data" property="hasImage">
                            <a href="<%=request.getContextPath()%>/inquiry/image?mid=<bean:write name="data" property="mobileAssignmentId"/>&gid=<bean:write name="data" property="questionGroupId"/>&qid=<bean:write name="data" property="questionId"/>"
                               target="_blank">
                                <img src="<%=request.getContextPath()%>/inquiry/image?mid=<bean:write name="data" property="mobileAssignmentId"/>&gid=<bean:write name="data" property="questionGroupId"/>&qid=<bean:write name="data" property="questionId"/>"
                                     width="160" height="120"/>
                            </a>
                            <logic:notEmpty name="data" property="gpsLatitude">
                                <a href="javascript:openMap('Load', 'Map','<bean:write name="data" property="gpsLatitude"/>','<bean:write name="data" property="gpsLongitude"/>','<bean:write name="data" property="accuracy"/>','<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>');">
                                    <img src="https://maps.google.com/maps/api/staticmap?key=<%=mapKey%>&center=<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>&zoom=12&size=160x120&sensor=false&markers=color:red|<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>">
                                </a>
                                <logic:equal name="data" property="answerType" value="014"></logic:equal>
                                <logic:equal name="data" property="answerType" value="022"></logic:equal>
                            </logic:notEmpty>
                        </logic:equal>
                        <logic:equal value="0" name="data" property="hasImage">
                            <img src="../../../images/gif/NoImageAvailable.gif"/>
                        </logic:equal>
                    </logic:equal>
                    <logic:notEqual value="1" name="data" property="isImage">
                        <logic:notEmpty name="data" property="optionAnswerLabel">
                            <bean:write name="data" property="optionAnswerLabel"/>
                        </logic:notEmpty>
                        <bean:define id="answType" name="data" property="answerType" type="java.lang.String"/>
                        <%

                            String answer = ((ViewAssignmentForm) data).getTextAnswer();
                            if (Strings.isNullOrEmpty(answer)) {
                                answer = "";
                            }
                            if (answType.equalsIgnoreCase(AnswerType.NUMERIC)
                                    || answType.equalsIgnoreCase(AnswerType.DECIMAL)
                                    || answType.equalsIgnoreCase(AnswerType.ACCOUNTING)
                                    || answType.equalsIgnoreCase(AnswerType.NUMERIC_WITH_CALCULATION)) {
                                if (Strings.isNullOrEmpty(answer)) {
                                    answer = "0";
                                }
                                out.print(TreemasFormatter.getNumberFormat(TreemasFormatter.NFORMAT_ID_0).format(Double.parseDouble(answer)));
// 							} else if(answType.equalsIgnoreCase(AnswerType.MULTIPLE)
// 									||answType.equalsIgnoreCase(AnswerType.MULTIPLE_ONE_DESCRIPTION)
// 									||answType.equalsIgnoreCase(AnswerType.MULTIPLE_LOOKUP_WITH_FILTER)
// 									||answType.equalsIgnoreCase(AnswerType.MULTIPLE_WITH_DESCRIPTION)){

                            } else {
                                out.print(answer);
                            }
                        %>
                        <logic:notEmpty name="data" property="optionAnswerLabel">
                            <logic:notEmpty name="data" property="textAnswer">
                                <bean:message key="feature.view.assignment.text.answer1"/>
                            </logic:notEmpty>
                            <logic:empty name="data" property="textAnswer">
                                <bean:message key="feature.view.assignment.text.answer2"/>
                            </logic:empty>
                        </logic:notEmpty>
                        <logic:notEmpty name="data" property="gpsLatitude">
                            <logic:equal value="329" name="data" property="questionId">
                                <a href="javascript:openMap('Load','Map','<bean:write name="data" property="gpsLatitude"/>','<bean:write name="data" property="gpsLongitude"/>','<bean:write name="data" property="accuracy"/>','<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>');">
                                    <img src="https://maps.google.com/maps/api/staticmap?key=<%=mapKey%>&center=<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>&zoom=12&size=160x120&sensor=false&markers=color:red|<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>">
                                </a>
                            </logic:equal>
                            <logic:equal name="data" property="answerType" value="025">
                                <a href="javascript:openMap('Load','Map','<bean:write name="data" property="gpsLatitude"/>','<bean:write name="data" property="gpsLongitude"/>','<bean:write name="data" property="accuracy"/>','<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>');">
                                    <img src="https://maps.google.com/maps/api/staticmap?key=<%=mapKey%>&center=<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>&zoom=12&size=160x120&sensor=false&markers=color:red|<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>">
                                </a>
                            </logic:equal>
                        </logic:notEmpty>
                    </logic:notEqual>
                </td>
            </tr>
        </logic:iterate>
    </logic:notEmpty>
    <logic:empty name="listJawaban" scope="request">
        <tr class="eval">
            <td bgcolor="FFFFFF" colspan="3" align="center"><font class="errMsg"><bean:message
                    key="common.listnotfound"/></font></td>
        </tr>
    </logic:empty>
</table>