<%@page import="treemas.util.constant.PropertiesKey" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>

<% String skey = ConfigurationProperties.getInstance().get(PropertiesKey.WEB_STATIC_KEY); %>

<html:html>
    <html:base/>

    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function movePage(task) {
                var form = document.forms[0];

                form.task.value = task;
                form.submit();
            }

            function openMap(taskbefore, task, gpsLatitude, gpsLongitude, accuracy, questionGroupId, questionId) {
                var frm = document.forms[0];
                frm.taskView.value = taskbefore;
                frm.task.value = task;
                frm.gpsLatitude.value = gpsLatitude;
                frm.gpsLongitude.value = gpsLongitude;
                frm.accuracy.value = accuracy;
                frm.questionGroupId.value = questionGroupId;
                frm.questionId.value = questionId;

                frm.submit();
            }

            // 	function openMap(task, gpsLatitude, gpsLongitude, accuracy, questionGroupId, questionId) {
            // 		var frm = document.forms[0];

            // 		frm.task.value = task;
            // 		frm.gpsLatitude.value = gpsLatitude;
            // 		frm.gpsLongitude.value = gpsLongitude;
            // 		frm.accuracy.value = accuracy;
            // 		frm.questionGroupId.value = questionGroupId;
            // 		frm.questionId.value = questionId;

            // 		frm.submit();
            // 	}


        </script>
    </head>

    <body oncontextmenu="return false;">
    <bean:define id="actionPath" name="viewAssignmentForm" property="path" type="java.lang.String"/>
    <html:form action="<%=actionPath%>">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="mobileAssignmentId"/>
        <html:hidden property="gpsLatitude"/>
        <html:hidden property="gpsLongitude"/>
        <html:hidden property="accuracy"/>
        <html:hidden property="questionGroupId"/>
        <html:hidden property="questionId"/>
        <html:hidden property="path"/>

        <div align="center">
            <%@include file="../../../Error.jsp" %>
            <%@include file="Header.jsp" %>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="30" align="right">
                        <a href="javascript:movePage('Load');">
                            <bean:message key="feature.view.assignment.task.history"/>
                        </a>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr class="headercol">
                    <td align="center" colspan="3"><bean:message key="feature.view.assignment.question.result"/></td>
                </tr>
                <tr class="headercol">
                    <td align="center" rowspan="2" width="5%"><bean:message key="common.number"/></td>
                    <td align="center" rowspan="2" width="45%"><bean:message key="feature.question"/></td>
                    <td align="center" colspan="2" width="50%"><bean:message key="common.answer"/></td>
                </tr>
                <tr class="headercol">
                    <td align="center" width="25%"><bean:message key="feature.precommittee.old"/></td>
                    <td align="center" width="25%"><bean:message key="feature.precommittee.new"/></td>
                </tr>
                <logic:notEmpty name="listJawaban" scope="request">
                    <logic:iterate id="data" name="listJawaban" scope="request" indexId="idx">
                        <% String classColor = (idx.intValue() % 2 == 0) ? "evalcol" : "oddcol"; %>
                        <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=classColor%>'">
                            <td align="right"><%=idx.intValue() + 1%>.</td>
                            <td align="left"><bean:write name="data" property="questionLabel"/></td>
                            <td style="word-break:break-all;">
                                <logic:equal value="1" name="data" property="isImage">
                                    <logic:equal value="1" name="data" property="hasImage">
                                        <a href="<%=request.getContextPath()%>/inquiry/image?mid=<bean:write name="data" property="mobileAssignmentId"/>&gid=<bean:write name="data" property="questionGroupId"/>&qid=<bean:write name="data" property="questionId"/>"
                                           target="_blank">
                                            <img src="<%=request.getContextPath()%>/inquiry/image?mid=<bean:write name="data" property="mobileAssignmentId"/>&gid=<bean:write name="data" property="questionGroupId"/>&qid=<bean:write name="data" property="questionId"/>"
                                                 width="160" height="120"/>
                                        </a>
                                        <logic:notEmpty name="data" property="gpsLatitude">
                                            <a href="javascript:openMap('ViewAnswer', 'Map','<bean:write name="data" property="gpsLatitude"/>','<bean:write name="data" property="gpsLongitude"/>','<bean:write name="data" property="accuracy"/>','<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>');">
                                                <img src="http://maps.googleapis.com/maps/api/staticmap?key=<%=skey%>&center=<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>&zoom=12&size=160x120&sensor=false&markers=color:red|<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>">
                                            </a>
                                            <logic:equal name="data" property="answerType" value="014"></logic:equal>
                                            <logic:equal name="data" property="answerType" value="022"></logic:equal>
                                        </logic:notEmpty>
                                    </logic:equal>
                                    <logic:equal value="0" name="data" property="hasImage">
                                        <img src="../../../images/gif/NoImageAvailable.gif"/>
                                    </logic:equal>
                                </logic:equal>
                                <logic:notEqual value="1" name="data" property="isImage">
                                    <logic:notEmpty name="data" property="oldOptionAnswerLabel">
                                        <bean:write name="data" property="oldOptionAnswerLabel"/>
                                    </logic:notEmpty>
                                    <bean:write name="data" property="oldTextAnswer"/>
                                    <logic:notEmpty name="data" property="oldOptionAnswerLabel">
                                        <logic:notEmpty name="data" property="oldTextAnswer">
                                            <bean:message key="feature.view.assignment.text.answer1"/>
                                        </logic:notEmpty>
                                        <logic:empty name="data" property="oldTextAnswer">
                                            <bean:message key="feature.view.assignment.text.answer2"/>
                                        </logic:empty>
                                    </logic:notEmpty>
                                    <logic:notEmpty name="data" property="gpsLatitude">
                                        <logic:equal value="329" name="data" property="questionId">
                                            <a href="javascript:openMap('ViewAnswer','Map','<bean:write name="data" property="gpsLatitude"/>','<bean:write name="data" property="gpsLongitude"/>','<bean:write name="data" property="accuracy"/>','<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>');">
                                                <img src="http://maps.googleapis.com/maps/api/staticmap?key=<%=skey%>&center=<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>&zoom=12&size=160x120&sensor=false&markers=color:red|<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>">
                                            </a>
                                        </logic:equal>
                                        <logic:equal name="data" property="answerType" value="025">
                                            <a href="javascript:openMap('ViewAnswer','Map','<bean:write name="data" property="gpsLatitude"/>','<bean:write name="data" property="gpsLongitude"/>','<bean:write name="data" property="accuracy"/>','<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>');">
                                                <img src="http://maps.googleapis.com/maps/api/staticmap?key=<%=skey%>&center=<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>&zoom=12&size=160x120&sensor=false&markers=color:red|<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>">
                                            </a>
                                        </logic:equal>
                                    </logic:notEmpty>
                                </logic:notEqual>
                            </td>
                            <td style="word-break:break-all;">
                                <logic:equal value="1" name="data" property="isImage">
                                    <logic:equal value="1" name="data" property="hasImage">
                                        <a href="<%=request.getContextPath()%>/inquiry/image?mid=<bean:write name="data" property="mobileAssignmentId"/>&gid=<bean:write name="data" property="questionGroupId"/>&qid=<bean:write name="data" property="questionId"/>"
                                           target="_blank">
                                            <img src="<%=request.getContextPath()%>/inquiry/image?mid=<bean:write name="data" property="mobileAssignmentId"/>&gid=<bean:write name="data" property="questionGroupId"/>&qid=<bean:write name="data" property="questionId"/>"
                                                 width="160" height="120"/>
                                        </a>
                                        <logic:notEmpty name="data" property="gpsLatitude">
                                            <a href="javascript:openMap('ViewAnswer', 'Map','<bean:write name="data" property="gpsLatitude"/>','<bean:write name="data" property="gpsLongitude"/>','<bean:write name="data" property="accuracy"/>','<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>');">
                                                <img src="http://maps.googleapis.com/maps/api/staticmap?key=<%=skey%>&center=<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>&zoom=12&size=160x120&sensor=false&markers=color:red|<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>">
                                            </a>
                                            <logic:equal name="data" property="answerType" value="014"></logic:equal>
                                            <logic:equal name="data" property="answerType" value="022"></logic:equal>
                                        </logic:notEmpty>
                                    </logic:equal>
                                    <logic:equal value="0" name="data" property="hasImage">
                                        <img src="../../../images/gif/NoImageAvailable.gif"/>
                                    </logic:equal>
                                </logic:equal>
                                <logic:notEqual value="1" name="data" property="isImage">
                                    <logic:notEmpty name="data" property="optionAnswerLabel">
                                        <bean:write name="data" property="optionAnswerLabel"/>
                                    </logic:notEmpty>
                                    <bean:write name="data" property="textAnswer"/>
                                    <logic:notEmpty name="data" property="optionAnswerLabel">
                                        <logic:notEmpty name="data" property="textAnswer">
                                            <bean:message key="feature.view.assignment.text.answer1"/>
                                        </logic:notEmpty>
                                        <logic:empty name="data" property="textAnswer">
                                            <bean:message key="feature.view.assignment.text.answer2"/>
                                        </logic:empty>
                                    </logic:notEmpty>
                                    <logic:notEmpty name="data" property="gpsLatitude">
                                        <logic:equal value="329" name="data" property="questionId">
                                            <a href="javascript:openMap('ViewAnswer','Map','<bean:write name="data" property="gpsLatitude"/>','<bean:write name="data" property="gpsLongitude"/>','<bean:write name="data" property="accuracy"/>','<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>');">
                                                <img src="http://maps.googleapis.com/maps/api/staticmap?key=<%=skey%>&center=<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>&zoom=12&size=160x120&sensor=false&markers=color:red|<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>">
                                            </a>
                                        </logic:equal>
                                        <logic:equal name="data" property="answerType" value="025">
                                            <a href="javascript:openMap('ViewAnswer','Map','<bean:write name="data" property="gpsLatitude"/>','<bean:write name="data" property="gpsLongitude"/>','<bean:write name="data" property="accuracy"/>','<bean:write name="data" property="questionGroupId"/>','<bean:write name="data" property="questionId"/>');">
                                                <img src="http://maps.googleapis.com/maps/api/staticmap?key=<%=skey%>&center=<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>&zoom=12&size=160x120&sensor=false&markers=color:red|<bean:write name="data" property="gpsLatitude"/>,<bean:write name="data" property="gpsLongitude"/>">
                                            </a>
                                        </logic:equal>
                                    </logic:notEmpty>
                                </logic:notEqual>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                <logic:empty name="listJawaban" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="5" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
            <table width="95%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%" height="30">
                        &nbsp;
                    </td>
                    <td width="50%" align="right">
                        <logic:notEmpty name="listJawaban" scope="request">
                            <a href="javascript:movePage('Print')">
                                <img src="../../../images/button/ButtonGenerate.png" width="100" height="31" border="0">
                            </a>
                        </logic:notEmpty>
                        <a href="javascript:window.close();">
                            <html:img src="../../../images/button/ButtonClose.png" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>
