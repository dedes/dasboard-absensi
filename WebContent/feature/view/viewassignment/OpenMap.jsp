<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@page import="treemas.util.constant.PropertiesKey" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<% String key = ConfigurationProperties.getInstance().get(PropertiesKey.WEB_KEY); %>
<html:html>
    <html:base/>

    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=<%=key%>&sensor=false"></script>
        <script language="javascript">
            function movePage(task) {
                var form = document.forms[0];

                form.task.value = task;
                form.submit();
            }

            function initialize() {

                var myLatlng = new google.maps.LatLng(<bean:write name="viewAssignmentForm" property="gpsLatitude"/>, <bean:write name="viewAssignmentForm" property="gpsLongitude"/>);
                var myOptions = {
                    zoom: 15,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    navigationControl: true,
                    navigationControlOptions: {
                        style: google.maps.NavigationControlStyle.ZOOM_PAN,
                        position: google.maps.ControlPosition.RIGHT
                    },
                    scaleControl: true,
                    scaleControlOptions: {
                        position: google.maps.ControlPosition.BOTTOM_RIGHT
                    }
                }

                var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                var contentString = '<img src="<%=request.getContextPath()%>/inquiry/image?mid=<bean:write name="viewAssignmentForm" property="mobileAssignmentId"/>&gid=<bean:write name="viewAssignmentForm" property="questionGroupId"/>&qid=<bean:write name="viewAssignmentForm" property="questionId"/>" width="160" height="120"/>';
                <logic:notEmpty name="viewAssignmentForm" property="accuracy">
                var accuracy = <bean:write name="viewAssignmentForm" property="accuracy"/>;
                contentString += '<br/>accuracy within ' + accuracy + ' meters';

                var circle = new google.maps.Circle({
                    strokeColor: '#CC0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 1.2,
                    fillColor: '#414141',
                    fillOpacity: 0.35,
                    map: map,
                    radius: accuracy,
                    center: myLatlng
                });
                </logic:notEmpty>

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: '<bean:write name="viewAssignmentForm" property="customerName"/>'
                });


                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });
            }
        </script>
    </head>

    <body onload="initialize()" oncontextmenu="return false;">
    <bean:define id="actionPath" name="viewAssignmentForm" property="path" type="java.lang.String"/>

    <html:form action="<%=actionPath%>">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="mobileAssignmentId"/>
        <html:hidden property="path"/>

        <div align="center">
            <%@include file="../../../Error.jsp" %>
            <div id="map_canvas" style="width:95%; height:80%"></div>
            <table width="95%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%" height="30">
                    </td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('<bean:write name="viewAssignmentForm" property="taskView" />');">
                            <html:img src="../../../images/button/ButtonBack.png" width="100" height="31" border="0"/>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>