<%@page import="treemas.util.constant.PropertiesKey" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<% String key = ConfigurationProperties.getInstance().get(PropertiesKey.WEB_KEY); %>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="30" align="right">
            <a href="javascript:movePage('ViewAnswer');">
                <img src="../../../images/button/ButtonViewAnswer.png" height="31" width="100" border="0"
                     alt="View Assignment Result" title="View Assignment Result"/>
            </a>
        </td>
    </tr>
</table>
<br/>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
    <tr class="headercol">
        <td align="center" height="20" class="tdtopibiru"><bean:message key="feature.view.assignment.cust.addr"/></td>
    </tr>
</table>
<table width="95%" border="0" cellspacing="1" cellpadding="3" class="tableview">
    <tr class="evalcol">
        <td width="100%" colspan="3" align="center" valign="middle">
            <logic:notEmpty name="location" scope="request">
                <logic:notEmpty name="location" scope="request" property="lat">
                    <a href="javascript:openMap('Load','Map','<bean:write name="location" property="lat"/>','<bean:write name="location" property="lng"/>','350','20','1588');">
                        <img src="http://maps.googleapis.com/maps/api/staticmap?key=<%=key%>&center=<bean:write name="location" property="lat"/>,<bean:write name="location" property="lng"/>&zoom=12&size=800x250&sensor=false&markers=color:red|<bean:write name="location" property="lat"/>,<bean:write name="location" property="lng"/>">
                    </a>
                </logic:notEmpty>
            </logic:notEmpty>
            <logic:empty name="location" scope="request">
                <a href="javascript:openMap('Map','<bean:write name="viewAssignmentForm" property="gpsLatitude"/>','<bean:write name="viewAssignmentForm" property="gpsLongitude"/>','100','20','1588');">
                    <img src="http://maps.googleapis.com/maps/api/staticmap?key=<%=key%>&center=<bean:write name="viewAssignmentForm" property="gpsLatitude"/>,<bean:write name="viewAssignmentForm" property="gpsLongitude"/>&zoom=12&size=800x250&sensor=false&markers=color:red|<bean:write name="viewAssignmentForm" property="gpsLatitude"/>,<bean:write name="viewAssignmentForm" property="gpsLongitude"/>">
                </a>
            </logic:empty>
        </td>
    </tr>
</table>