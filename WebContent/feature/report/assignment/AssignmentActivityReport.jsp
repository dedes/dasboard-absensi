<%@page import="com.google.common.base.Strings" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tlds/birt.tld" prefix="birt" %>
<%
    String title = "ACTIVITY LOG";
%>
<html:html>
    <html:base/>
    <head>
        <title><%=title%>
        </title>
    </head>
    <body>
    <%
        String branch = (String) request.getAttribute("branch");
        String searchValue1 = (String) request.getAttribute("searchValue1");
        String tglStart = (String) request.getAttribute("tglStart");
        String tglEnd = (String) request.getAttribute("tglEnd");
        String status = (String) request.getAttribute("status");
        String status2hidden = (String) request.getAttribute("status2hidden");
        String scheme = (String) request.getAttribute("scheme");
        String userId = (String) request.getAttribute("userId");
        String formWidth = (String) request.getAttribute("width");
        String formHeight = (String) request.getAttribute("height");
        String orderField = (String) request.getAttribute("orderField");
        String sortDesc = (String) request.getAttribute("sortDesc");
        String searchType1 = (String) request.getAttribute("searchType1");
        String searchType2 = (String) request.getAttribute("searchType2");

        String height = getServletContext().getInitParameter("birt_viewer_height");
        String width = getServletContext().getInitParameter("birt_viewer_width");

        if (!Strings.isNullOrEmpty(formHeight) && formHeight.matches("[0-9.]*")) {
            height = formHeight;
        }
        if (!Strings.isNullOrEmpty(formWidth) && formWidth.matches("[0-9.]*")) {
            width = formWidth;
        }
    %>

    <center>
        <birt:viewer id="birtReport" reportDesign="activitylog.rptdesign" height="<%=height%>" width="<%=width%>"
                     frameborder="0" scrolling="no">
            <birt:param name="branch" value="<%=branch%>"></birt:param>
            <birt:param name="searchValue1" value="<%=searchValue1%>"></birt:param>
            <birt:param name="tglStart" value="<%=tglStart%>"></birt:param>
            <birt:param name="tglEnd" value="<%=tglEnd%>"></birt:param>
            <birt:param name="status" value="<%=status%>"></birt:param>
            <birt:param name="status2hidden" value="<%=status2hidden%>"></birt:param>
            <birt:param name="scheme" value="<%=scheme%>"></birt:param>
            <birt:param name="userId" value="<%=userId%>"></birt:param>
            <birt:param name="orderField" value="<%=orderField%>"></birt:param>
            <birt:param name="sortDesc" value="<%=sortDesc%>"></birt:param>
            <birt:param name="searchType1" value="<%=searchType1%>"></birt:param>
            <birt:param name="searchType2" value="<%=searchType2%>"></birt:param>
        </birt:viewer>
    </center>
    </body>
</html:html>