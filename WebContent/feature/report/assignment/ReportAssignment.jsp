<%@page import="treemas.util.constant.PropertiesKey" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tlds/birt.tld" prefix="birt" %>
<html>
<head>
    <title>Task Result Preview</title>
</head>
<body bgcolor="#ffffff" oncontextmenu="return false;">
<%
    String surveyId = (String) request.getAttribute("surveyId");
    String REPORT_FILENAME = (String) request.getAttribute("reportFileName");
    //List listResult = (List) request.getAttribute("form");


    String driverClass = ConfigurationProperties.getInstance().get(PropertiesKey.DATABASE_DRIVER);
    String driverUrl = ConfigurationProperties.getInstance().get(PropertiesKey.DATABASE_URL);
    String user = ConfigurationProperties.getInstance().get(PropertiesKey.DATABASE_USER);
    String password = ConfigurationProperties.getInstance().get(PropertiesKey.DATABASE_PASSWORD);
    String width = getServletContext().getInitParameter("birt_viewer_width");
    String height = getServletContext().getInitParameter("birt_viewer_height");

// 	System.out.println("Print preview: " + REPORT_FILENAME);
%>

<birt:viewer id="birtReport" reportDesign="<%=REPORT_FILENAME%>" height="<%=height%>" width="<%=width%>"
             showParameterPage="false" frameborder="0" scrolling="auto">
    <birt:param name="driverClass" value="<%=driverClass%>"></birt:param>
    <birt:param name="driverUrl" value="<%=driverUrl%>"></birt:param>
    <birt:param name="user" value="<%=user%>"></birt:param>
    <birt:param name="password" value="<%=password%>"></birt:param>
    <birt:param name="surveyId" value="<%=surveyId%>"></birt:param>
    <logic:notEmpty name="form" scope="request">
        <logic:iterate id="frm" name="form" scope="request">
            <bean:define id="birtParamName" name="frm" property="paramName" type="java.lang.String"/>
            <bean:define id="question" type="java.lang.String" value=""/>
            <bean:define id="answer" type="java.lang.String" value=""/>
            <bean:define id="option" type="java.lang.String" value=""/>
            <bean:define id="image" type="java.lang.String" value=""/>

            <logic:notEmpty name="frm" property="questionLabel">
                <bean:define id="question" name="frm" property="questionLabel" type="java.lang.String"/>
            </logic:notEmpty>
            <logic:notEmpty name="frm" property="textAnswer">
                <bean:define id="answer" name="frm" property="textAnswer" type="java.lang.String"/>
            </logic:notEmpty>
            <logic:notEmpty name="frm" property="optionAnswerLabel">
                <bean:define id="option" name="frm" property="optionAnswerLabel" type="java.lang.String"/>
            </logic:notEmpty>
            <logic:notEmpty name="frm" property="imagePath">
                <bean:define id="image" name="frm" property="imagePath" type="java.lang.String"/>
            </logic:notEmpty>

            <birt:param name='<%=\"question\"+birtParamName%>' value="<%=question%>"></birt:param>
            <birt:param name='<%=\"answer\"+birtParamName%>' value="<%=answer%>"></birt:param>
            <birt:param name='<%=\"option\"+birtParamName%>' value="<%=option%>"></birt:param>
            <birt:param name='<%=\"image\"+birtParamName%>' value="<%=image%>"></birt:param>
        </logic:iterate>
    </logic:notEmpty>
</birt:viewer>
</body>
</html>