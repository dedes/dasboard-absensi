<%@page import="java.util.ArrayList" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "FORM LIST USER MEMBER"; %>
<html:html>
<html:base/>
<head>
	<title><bean:message key="user.member"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	
    <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
    <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        
	<link rel="stylesheet" href="../../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../../assets/css/custom.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../../assets/css/skins/green.css">

	<script src="../../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	  <script language="javascript">
            var arrObjMenu = new Array;
            var arrObjMember = new Array;
            var i = 0;

            function objMenu(nikMember) {
                this.nikMember = nikMember;
            }
            
            function objMember(nikMember) {
                this.nikMember = nikMember;
            }

            <%
               ArrayList menuList = (ArrayList) (request.getAttribute("listAll"));
               ArrayList memberList = (ArrayList) (request.getAttribute("listByNik"));
            
                for (int i=0; i<menuList.size(); i++) {
                	treemas.application.feature.master.userapp.UserAppBean bean = (treemas.application.feature.master.userapp.UserAppBean) menuList.get(i);
            %>
            arrObjMenu[<%=i%>] = new objMenu("<%=bean.getNikMember()%>");
            <%
                }
                for (int i=0; i<memberList.size(); i++) {
                	treemas.application.feature.master.userapp.UserAppBean bean = (treemas.application.feature.master.userapp.UserAppBean) memberList.get(i);
            %>
            arrObjMember[<%=i%>] = new objMember("<%=bean.getNikMember()%>");
            <%
                }
             %>
             
            $( document ).ready(function() {
            	findCheck(arrObjMember);
           	});
             
            function findCheck(memberList) {
            	var member = document.getElementsByName("search.memberList");
            	for (var j = 0; j < member.length; j++) {
                   	member[j].checked = false;
                }
            	for (var i = 0; i < memberList.length; i++) {
            		for (var j = 0; j < member.length; j++) {
                        if (memberList[i].nikMember == member[j].value) {
                           	member[j].checked = true;
                        }
                    }
                }
            }

            function findIdxMenu(start, nikMember) {
                for (i = start; i < arrObjMenu.length; i++) {
                    if (nikMember == arrObjMenu[i].nikMember) {
                        return (i);
                    }
                }
                return (-1);
            }

            function findChild(start, nikMember) {
                for (i = start; i < arrObjMenu.length; i++) {
                    if (nikMember == arrObjMenu[i].nikMember) {
                        return (i);
                    }
                }
                return (-1);
            }

            function changeChild(start, nikMember, check) {
                var i;
                var idxChildId = findChild(start, nikMember);
                if (idxChildId != -1) {
                    for (i = idxChildId; i < arrObjMenu.length && nikMember == arrObjMenu[i].nikMember; i++) {
                        document.userAppForm.checkedMemberList[i].checked = check;
                    }
                    changeChild(i, nikMember, check);
                }
            }

            function checkedParent(chkbox) {
            	var member = document.getElementsByName("search.memberList");
            	var nikMember = document.getElementsByName("nikMember");
            	var tmpChecked = chkbox.checked;
                var tmpnikMember = chkbox.value;
                if (tmpChecked == false) {
                   changeChild(0, tmpnikMember, tmpChecked);
                } else {
                   /* for (i = 0; i < member.length; i++) {
                	   console.log("member1 : " + member[i].value);
                	   console.log("member2 : " + nikMember.value);
                   	if (member[i].value == nikMember.value) {
                   		member[i].checked = tmpChecked;
                   		
                           break;
                       }
                   } */
                   idxnik = findIdxMenu(0, tmpnikMember);
                   if (idxnik > -1) {
                       changeChild(0, tmpnikMember, tmpChecked);
                   }
                }
            }
            
            function checkedOne(chkbox) {
            	/* var member = document.getElementsByName("search.memberList");
            	var tmpChecked = chkbox.checked;
                var tmpnikMember = chkbox.value;
                if (tmpChecked){
                	document.userMemberForm.nikList.value = document.userMemberForm.nikList.value + ',' + tmpnikMember;
        		} */
              	/* var listMember = document.getElementsByName("search.memberList");
            	console.log("len : " + arrObjMenu.length);
            	var arr = [];
            	var idx = 0;
            	for (var i=0; i<listMember.length && tmpnikMember == listMember[i].value; i++) {
            		
            		
					/* if (listMember[i].checked == true){
						arr[idx] = listMember[i].value;
						idx++;
						console.log("sult1 : " + listMember[i].value);
	                } */	
            }


            function checkedAll() {
            	var member = document.getElementsByName("search.memberList");
                <%
                    if (menuList.size() > 1) {
                %>
                for (i = 0; i < member.length; i++)
                    with (member[i])
                        checked = document.userAppForm.checkAll.checked;
                <%
                    } else {
                %>
               		with (member)
                    	checked = document.userAppForm.checkAll.checked;
                <%
                    }
                %>
            }

            function movePage(task, idParam) {
             	var member = document.getElementsByName("search.memberList");
             	/*checkedOne(member);
            	var listMember = document.getElementsByName("checkedMemberList");
            	console.log("len : " + listMember.length);
            	console.log("isi : " + listMember[0].value);*/
      
            	/* for (var i=0; i<arr.lenght; i++) {
            		for (var j=0; j<arrObjMenu.length; i++) {
    					console.log("ress : " + arrObjMenu[j]['nikMember']);
    					if (arr[i].value == arrObjMenu[j].nikMember){
    						console.log("sult2 : " + arrObjMenu[j].nikMember);
    					}
                   // 	document.userMemberForm.nikList[i].value = listMember[i].value;
    				 }
				 }  */
			/* 	 */
				var arr = [];
	            var idx = 0;
				with (document.userAppForm) {
			 	 	if (member.length > 1) {
						 for (var i=0; i<member.length; i++) {
							if (member[i].checked) {
								arr[idx] = member[i].value;
								idx++;
							}
						 }
					}
				}
				document.userAppForm.nikList.value = arr;
            	document.userAppForm.task.value = task;
                document.userAppForm.submit();
            }

            function actionX(form, task) {
            	var nik = document.getElementById('nik');
                document.forms[0].task.value = task;
                document.forms[0].nik.value = nik.value;
                document.forms[0].submit();
            }
        </script>
         <%@include file="../../../Error.jsp" %>
</head>
<body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
				<ol class="breadcrumb bc-2">
					<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="master.data"/></a></li>
					<li class="active"><a href="<%=request.getContextPath()%>/feature/master/UserApprovel.do"><bean:message key="user.member"/></a></li>
				</ol>
				<div class="row">
					<html:form action="/feature/master/UserApprovel.do" method="POST" styleClass="form-horizontal form-groups-bordered">
			        <html:hidden property="task" value="Load"/>
			        <html:hidden property="nikList" />
			        <html:hidden property="paging.dispatch" value="Go"/>
					<bean:define id="indexLastRow" name="userAppForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<bean:define id="indexFirstRow" name="userAppForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="panel panel-primary" data-collapsed="0">
						<div class="panel-heading" style="background:#00a651;">
							<div  class="panel-title"><span style="color:#FFFFFF;"><bean:message key="user.member"/></span></div>
							<div class="panel-options">
								<a href="javascript:void(0);" data-rel="collapse" style="color:#FFFFFF;"><i class="entypo-down-open"></i></a>
								<a href="javascript:movePage('Load');" data-rel="reload" style="color:#FFFFFF;"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<div class="row">
											<div class="form-group">
												<label for='nik' class="col-sm-3 control-label"><bean:message key="nik.member" /><font color="#FF0000"> *)</font></label>
												<div class="col-sm-5">
													<html:select property="nik" styleClass="form-control" styleId="nik" onchange="javascript:actionX('document.userAppForm','Search');">
							                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
							                            <logic:notEmpty name="listLeader" scope="request">
							                                <html:options collection="listLeader" property="optKey" labelProperty="optLabel"/>
							                            </logic:notEmpty>
							                        </html:select>
												</div>
											 </div> 
										</div>
									</div>
								</div>
							<div class="row">
								<div class="col-md-4 col-xl-4" >
									<table class="table responsive table-bordered"  > 
										<thead>
											<tr align="center">
												<td width="3%" align="center">
											      	<logic:notEmpty name="listAll" scope="request">
							                            <input type="checkbox" value="ON" name="checkAll" onclick="javascript:checkedAll()">
							                        </logic:notEmpty>
						                        </td>
										      	<td width="92%" align="center"><bean:message key="feature.form.karyawan"/></td>
										    </tr>
										</thead>
										<tbody>
											<logic:notEmpty name="listAll" scope="request">
						                    <logic:iterate id="listParam" name="listAll" scope="request" indexId="index">
						                        <tr class="evalcol">
						                            <td align="center">
						                                <html:multibox name="userAppForm" property="search.memberList" onclick="javascript:checkedOne(this)">
						                                    <bean:write name="listParam" property="nikMember"/>
						                                </html:multibox>
						                            </td>
						                            <td align="left">
					                                    <b><bean:write name="listParam" property="nama"/></b>
						                            </td>
						                        </tr>
						                    </logic:iterate>
						                	</logic:notEmpty>
										</tbody>
									</table> 
									<%-- <ul>
										<logic:notEmpty name="listAll" scope="request">
										<logic:iterate id="listParam" name="listAll" scope="request" indexId="index">
										<%
						                    if (index < 29) {
						                %>
										<li>
											<html:multibox name="userMemberForm" property="nikList" onclick="javascript:checkedParent(this)">
			                                    <bean:write name="listParam" property="nikMember"/>
			                                </html:multibox> - <b><bean:write name="listParam" property="nama"/></b>
		                                </li>
		                                <%
						                    }
						                %>
										</logic:iterate>
										</logic:notEmpty>
									</ul> --%>
								</div>
								<%-- <div class="col-md-4 col-xl-4">
									<ul>
										<logic:notEmpty name="listAll" scope="request">
										<logic:iterate id="listParam" name="listAll" scope="request" indexId="index">
										<%
						                    if (index >= 29 && index < 58) {
						                %>
										<li>
											<html:multibox name="userMemberForm" property="nikList" onclick="javascript:checkedParent(this)">
			                                    <bean:write name="listParam" property="nikMember"/>
			                                </html:multibox> - <b><bean:write name="listParam" property="nama"/></b>
		                                </li>
		                                <%
						                    }
						                %>
										</logic:iterate>
										</logic:notEmpty>
									</ul>
								</div>
								<div class="col-md-4 col-xl-4"> 
									<ul>
										<logic:notEmpty name="listAll" scope="request">
										<logic:iterate id="listParam" name="listAll" scope="request" indexId="index">
										<%
						                    if (index >= 58) {
						                %>
										<li>
											<html:multibox name="userMemberForm" property="nikList" onclick="javascript:checkedParent(this)">
			                                    <bean:write name="listParam" property="nikMember"/>
			                                </html:multibox> - <b><bean:write name="listParam" property="nama"/></b>
		                                </li>
		                                <%
						                    }
						                %>
										</logic:iterate>
										</logic:notEmpty>
									</ul>--%>
								</div>
							</div>
						<br/>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
					             <button type="button" onclick="javascript:movePage('Update');" class="btn btn-green"  title="<bean:message key="tooltip.add"/>"><bean:message key="tooltip.add"/></button>
							</div>
						</div>
					</div> <!--  END PANEL BODY -->
					</div>
					</div>
					</html:form>
				</div>
		</div>	
	</div>
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../../assets/js/bootstrap.js"></script>
	<script src="../../../assets/js/joinable.js"></script>
	<script src="../../../assets/js/resizeable.js"></script>
	<script src="../../../assets/js/neon-api.js"></script>
	<script src="../../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../../assets/js/jquery.validate.min.js"></script>
	<script src="../../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../../assets/js/jquery.multi-select.js"></script>
	<script src="../../../assets/js/neon-chat.js"></script>
	<script src="../../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../../assets/js/select2/select2.min.js"></script>
	<script src="../../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../../assets/js/typeahead.min.js"></script>
	<script src="../../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../../assets/js/neon-demo.js"></script>
</html:html>