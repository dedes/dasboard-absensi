<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.TITLE_APP_USER;; %>
<html:html>
<html:base/>
<head>
	<title><bean:message key="reimburse"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	
    <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
    <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        
	<link rel="stylesheet" href="../../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../../assets/css/custom.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../../assets/css/skins/green.css">

	<script src="../../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<script language="javascript">
	$(function(){
		toggleTrx();
		 }); 
	function toggleTrx(){
		var hiddenDate = document.getElementById("hiddenDate");
		var dateBefore = document.getElementById("dateBefore");
		var dateAfter = document.getElementById("dateAfter");
			$("input[name*='search.tgl']").datepicker({
				maxDate : "+0M +0D", 
				dateFormat : "yy-mm-dd",
				changeMonth : true,
				changeYear : true,
				showButtonPanel : true,
				showOn : "both"
			});	
	}     
    function movePage(taskID, nik) {
            document.forms[0].task.value = taskID;
            document.forms[0].nik.value = nik;
            document.forms[0].submit();
        }

    function openDownload(nama, option){
		var dateBefore = document.getElementById("dateBefore").value;
		var dateAfter = document.getElementById("dateAfter").value;
		 var uri = '<html:rewrite action="/servlet/reimburseDownload"/>'+'?dateBefore='+dateBefore+'&dateAfter='+dateAfter;
			 window.open(uri, "_self", "scrollbars=no,width=300,height=100,resizable=no,top=250,left=550"); 
    		    
	   }
	
</script>
</head>
<body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
				<ol class="breadcrumb bc-2">
					<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="detail.data"/></a></li>
					<li class="active"><a href="<%=request.getContextPath()%>/feature/master/ReimburseKrwn.do"><bean:message key="reimburse"/></a></li>
				</ol>
				<div class="row">
					<html:form action="/feature/master/ReimburseKrwn.do" method="POST">
					<html:hidden property="task" value="Load"/>
					<html:hidden property="nik"/>
				<%-- 		<html:hidden property="groupbranch" styleId="member"/> --%>
					<html:hidden property="paging.dispatch" value="Go"/>
					<bean:define id="indexLastRow" name="reimburseForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<bean:define id="indexFirstRow" name="reimburseForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="panel panel-primary" data-collapsed="0">
						<div class="panel-heading" style="background:#00a651;">
							<div  class="panel-title"><span style="color:#FFFFFF;" ><bean:message key="feature.form.reimburse"/></span></div>
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="javascript:movePage('Load');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label for="search.tgl" class=" control-label"><bean:message key="common.tanggal"/><font color="#FF0000"> *)</font></label>
												<html:text  styleId="dateBefore" property="search.tgl" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="form-control \" autoComplete=\"off" />
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="search.tglAfter" class=" control-label"><bean:message key="common.to"/><font color="#FF0000"> *)</font></label>
												<html:text styleId="dateAfter" property="search.tglAfter" onkeydown="return disableInput();"  size="8" maxlength="8" styleClass="form-control \" autoComplete=\"off" />
											</div>
										</div>
										<div class="col-md-3 col-sm-3">
											<div class="form-group">
												<label style="color:transparent!important;">&nbsp;&nbsp;</label> <br/>
												<button type="button" onclick="javascript:movePage('Search');" class="btn btn-blue"  title="<bean:message key="tooltip.search"/>"><bean:message key="tooltip.search"/></button>
												<logic:notEmpty name="listReimburse" scope="request">
													<button type="button" onclick="javascript:openDownload('Download','scrollbars=no,width=300,height=100,resizable=no,top=250,left=550');" class="btn btn-blue"  title="<bean:message key="tooltip.download"/>"><bean:message key="tooltip.download"/></button>
												</logic:notEmpty>
											</div>
										</div>
										<%-- <logic:notEmpty name="listReimburse" scope="request">
											<div class="col-md-1">
												<div class="form-group">
													<label style="color:transparent!important" for="generate" ><bean:message key="tooltip.download"/></label>
													<button type="button" onclick="javascript:openDownload('Download','scrollbars=no,width=300,height=100,resizable=no,top=250,left=550');" class="btn btn-blue"  title="<bean:message key="tooltip.download"/>"><bean:message key="tooltip.download"/></button>
												</div>
											</div>
					    				</logic:notEmpty> --%>
									</div>
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="col-md-12 col-xl-8" >
									<table width="100%" class="table responsive table-bordered"  >
										<thead >
											<tr align="center">
										      	<td><bean:message key="common.id.nik"/></td>
										      	<td><bean:message key="common.namakary"/></td>
										      	<td><bean:message key="common.hari"/></td>
										      	<td width="10%"><bean:message key="common.tanggal"/></td>
										      	<td><bean:message key="common.id.project"/></td>
										      	<td><bean:message key="common.in"/></td>	
										      	<td><bean:message key="common.out"/></td>	
										      	<td><bean:message key="common.transport"/></td>
										      	<td><bean:message key="common.makan"/></td>
										      	<td><bean:message key="common.note"/></td>
										      	<td><bean:message key="common.jamkerja"/></td>
										      	<td><bean:message key="common.status"/></td>	
										    </tr>
										</thead>
										<tbody>
											<logic:notEmpty name="listReimburse" scope="request" >
												<logic:iterate id="listParam" name="listReimburse" scope="request" indexId="index">
													<%indexLastRow++;%>
													<tr>
													    <td align="left"><bean:write name="listParam" property="nik"/></td>
													    <td align="left"><bean:write name="listParam" property="nama"/></td>
													    <td align="center"><bean:write name="listParam" property="hari"/></td>
													    <td align="center"><bean:write name="listParam" property="tgl"/></td>
													    <td align="center"><bean:write name="listParam" property="idProject"/></td>
													    <td align="center"><bean:write name="listParam" property="jamMasuk"/></td>
													    <td align="center"><bean:write name="listParam" property="jamKeluar"/></td>
													    <td align="left"><bean:write name="listParam" property="transport"/></td>
													    <td align="left"><bean:write name="listParam" property="uangMakan"/></td>
													    <td align="left"><bean:write name="listParam" property="keterangan"/></td>
													    <td align="center"><bean:write name="listParam" property="totalJamKerja"/></td>
													     <td align="center" valign="middle">
													     
															 <logic:equal value="A" name="listParam" property="noteapp">
															    	<img src="../../../images/icon/IconActive.gif" alt="Approve" width="16" height="16" title="<bean:message key="tooltip.approve"/>"/>
															 </logic:equal>
															 <logic:notEqual value="A" name="listParam" property="noteapp">
														    	
														    </logic:notEqual>
															 <logic:equal value="R" name="listParam" property="noteapp">
																    <img src="../../../images/icon/IconInactive.gif" alt="Reject" width="16" height="16" title="<bean:message key="tooltip.reject"/>"/>
													     	 </logic:equal>
													     	 <logic:notEqual value="R" name="listParam" property="noteapp">
														    	
														    </logic:notEqual>
													    </td>
													     
								      				</tr>
								      				
								   				</logic:iterate>
								  			</logic:notEmpty>
											<logic:empty name="listReimburse" scope="request">
											   <tr>
											      <td colspan="14" align="center" ><font class="errMsg"><bean:message key="common.listnotfound"/></font></td>
											   </tr>
								   			</logic:empty>
										</tbody>
									</table>
								</div>
							</div>
							<div>
							<logic:notEmpty name="listReimburse" scope="request">
								<div class="row">
					    			<div class=" col-md-5 "> 
							        	<button type="button" onclick="javascript:moveToPageNumber(document.reimburseForm,'First')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-left"></i></button>
							        	
							        	<button type="button" onclick="javascript:moveToPageNumber(document.reimburseForm,'Prev')" class="btn btn-xs btn-default"><i class="fa fa-angle-left"></i></button>
					    				
					    				<html:text name="reimburseForm" property="paging.currentPageNo" size="3" maxlength="6" styleClass="inpType "/> <bean:message key="paging.of"/> <html:hidden property="paging.totalPage" write="true" />
							    		
							    		<button type="button" onclick="javascript:moveToPageNumber(document.reimburseForm,'Go')" class="btn btn-xs btn-default"><i class="fa fa-search"></i></button>
							    		
							        	<button type="button" onclick="javascript:moveToPageNumber(document.reimburseForm,'Next')" class="btn btn-xs btn-default"><i class="fa fa-angle-right"></i></button>
					    				
						        		<button type="button" onclick="javascript:moveToPageNumber(document.reimburseForm,'Last')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-right"></i></button>
				    					
					    			</div>
					    			<div class="pull-right col-md-4">
				    					<label class="pull-right"><bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message key="paging.showing.to"/> <%=indexLastRow-1%> <bean:message key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/> <bean:message key="paging.showing.record"/></label>
					    			</div>
						      	</div> 	
					  		</logic:notEmpty> 
							</div>
							<br/>
						</div> <!--  END PANEL BODY -->
					</div>
				</div>
			</html:form>
			</div>
		</div>	
	</div> 	  
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../../assets/js/bootstrap.js"></script>
	<script src="../../../assets/js/joinable.js"></script>
	<script src="../../../assets/js/resizeable.js"></script>
	<script src="../../../assets/js/neon-api.js"></script>
	<script src="../../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../../assets/js/jquery.validate.min.js"></script>
	<script src="../../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../../assets/js/jquery.multi-select.js"></script>
	<script src="../../../assets/js/neon-chat.js"></script>
	<script src="../../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../../assets/js/select2/select2.min.js"></script>
	<script src="../../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../../assets/js/typeahead.min.js"></script>
	<script src="../../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../../assets/js/neon-demo.js"></script>
</html:html>