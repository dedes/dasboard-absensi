<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
        $(function(){
			toggleTrx();
			 }); 
		function toggleTrx(){
			var hiddenDate = document.getElementById("hiddenDate");
			var dateBefore = document.getElementById("dateBefore");
			var dateAfter = document.getElementById("dateAfter");
				$("input[name*='search.tgl']").datepicker({
					maxDate : "+0M +0D", 
					dateFormat : "yy-mm-dd",
					changeMonth : true,
					changeYear : true,
					showButtonPanel : true,
					showOn : "both"
				});	
		}     
        function movePage(taskID, nik) {
                document.forms[0].task.value = taskID;
                document.forms[0].nik.value = nik;
                document.forms[0].submit();
            }

            
            
            function openDownload(nama, option){
            	var nik = document.getElementById("nik").value;
				var nama = document.getElementById("nama").value;
				var dateBefore = document.getElementById("dateBefore").value;
				var dateAfter = document.getElementById("dateAfter").value;
				 var uri = '<html:rewrite action="/servlet/reimburseDownload"/>'+'?nik='+nik+'&nama='+nama+'&dateBefore='+dateBefore+'&dateAfter='+dateAfter;
	    		    opener.open(uri, "_self", "scrollbars=no,width=300,height=100,resizable=no,top=250,left=550"); 
	    		    
		   }
            

        </script>
    </head>

    <body>
    <html:form action="/feature/master/Reimburse.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="nik"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="currentUri"/>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.form.reimburse"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.id.nik"/></td>
                    <td class="Edit-Right-Text">
                        <html:text styleId="nik" property="search.nik" styleClass="TextBox" size="30" maxlength="10"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.namakary"/></td>
                    <td class="Edit-Right-Text">
                        <html:text styleId="nama" property="search.nama" styleClass="TextBox" size="30" maxlength="30"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.tanggal"/></td>
			      <td class="Edit-Right-Text">
					<html:text  styleId="dateBefore" property="search.tgl" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="TextBox" />
					&nbsp;
						- 
					&nbsp;&nbsp; 
					<html:text styleId="dateAfter" property="search.tglAfter" onkeydown="return disableInput();"  size="8" maxlength="8" styleClass="TextBox" />
			      </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search" width="100" height="31"
                                 border="0" title="<bean:message key="tooltip.search"/>">
                        </a>
                        
                    </td>
                </tr>
            </table>
            <div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;">
                <table border="0" width="100%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="10%" align="center"><bean:message key="common.id.nik"/></td>
                        <td width="10%" align="center"><bean:message key="common.namakary"/></td>
                        <td width="10%" align="center"><bean:message key="common.hari"/></td>
                        <td width="10%" align="center"><bean:message key="common.tanggal"/></td>
                        <td width="50%" align="center"><bean:message key="common.lokasi"/></td>
                        <td width="10%" align="center"><bean:message key="general.idp"/></td>
                        <td width="10%" align="center"><bean:message key="common.in"/></td>
                        <td width="10%" align="center"><bean:message key="common.out"/></td>
                        <td width="10%" align="center"><bean:message key="common.transport"/></td>
                        <td width="10%" align="center"><bean:message key="common.makan"/></td>
                        <td width="10%" align="center"><bean:message key="common.data"/></td>
                        <td width="10%" align="center"><bean:message key="common.ket"/></td>
                        <td width="10%" align="center"><bean:message key="common.jamkerja"/></td>
                        <td width="10%" align="center"><bean:message key="common.isapp"/></td>
                        
                    </tr>
                    <%
                        int i = 0;
                        String classColor;
                    %>
                    <logic:notEmpty name="listReimburse" scope="request">
                        <logic:iterate id="data" name="listReimburse" scope="request">
                        
                            <%
                                if ((i % 2) != 1)
                                    classColor = "evalcol";
                                else
                                    classColor = "oddcol";
                                i++;
                            %>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td align="center"><bean:write name="data" property="nik"/></td>
                                <td align="center"><bean:write name="data" property="nama"/></td>
                                <td align="center"><bean:write name="data" property="hari"/></td>
                                <td align="center"><bean:write name="data" property="tgl"/></td>
                                <td align="center"><bean:write name="data" property="lokasi"/></td>
                                <td align="center"><bean:write name="data" property="idProject"/></td>
                                <td align="center"><bean:write name="data" property="jamMasuk"/></td>
                                <td align="center"><bean:write name="data" property="jamKeluar"/></td>
                                <td align="center"><bean:write name="data" property="transport"/></td>
                                <td align="center"><bean:write name="data" property="uangMakan"/></td>
                                <td align="center"><bean:write name="data" property="uangData"/></td>
                           		<td align="center"><bean:write name="data" property="keterangan"/></td>
                           		<td align="center"><bean:write name="data" property="totalJamKerja"/></td>
                           		<td align="center">
                                <logic:equal name="data" property="isApproved" value="1">
                                    <img src="../../../images/icon/IconYes.gif" border="0" width="16" height="16"/>
                                </logic:equal>
                                <logic:notEqual name="data" property="isApproved" value="1">
                                    <img src="../../../images/icon/IconInactive.gif" border="0" width="16" height="16"/>
                                </logic:notEqual>
                           		</td>
                            </tr>

                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listReimburse" scope="request">
                        <tr class="tdgenap">
                            <td bgcolor="FFFFFF" colspan="14" align="center">
                                <font class="errMsg"><bean:message key="common.listnotfound"/></font>
                            </td>
                        </tr>
                    </logic:empty>
                </table>
            </div>
            <logic:notEmpty name="listReimburse" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.reimburseForm,'First')" title="First">
                                <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.reimburseForm,'Prev')" title="Previous">
                                <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="reimburseForm" property="paging.currentPageNo" size="3" maxlength="6"
                                       styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <html:hidden property="paging.totalRecord" write="false"/>
                            <a href="javascript:moveToPageNumber(document.reimburseForm,'Go');">
                                <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                     border="0"/></a></td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.reimburseForm,'Next')" title="Next">
                                <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.reimburseForm,'Last')" title="Last">
                                <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:define id="currentPage" name="reimburseForm" property="paging.currentPageNo"
                                         type="java.lang.Integer"/>
                            <bean:define id="total" name="reimburseForm" property="paging.totalRecord"
                                         type="java.lang.Integer"/>
                            <bean:define id="perpage" name="reimburseForm" property="paging.rowPerPage"
                                         type="java.lang.Integer"/>
                            <%
                                int startShow = 1;
                                int endShow = 0;
                                if (currentPage > 1) {
                                    int tempstart = ((currentPage - 1) * perpage) + 1;
                                    int tempend = currentPage * perpage;
                                    if (total < tempend) {
                                        tempend = total;
                                    }
                                    startShow = tempstart;
                                    endShow = tempend;
                                } else {
                                    endShow = total;
                                }
                            %>
                            <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                            <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
           <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:openDownload('Download','scrollbars=no,width=300,height=100,resizable=no,top=250,left=550');" >
                        <img src="../../../images/button/ButtonDownload.png"
                        alt="Download" width="100" height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>