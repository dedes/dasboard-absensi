<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <link rel="stylesheet" href="../../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
		<link rel="stylesheet" href="../../../assets/css/font-icons/entypo/css/entypo.css">
		<link rel="stylesheet" href="../../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
		<link rel="stylesheet" href="../../../assets/css/bootstrap.css">
		<link rel="stylesheet" href="../../../assets/css/neon-core.css">
		<link rel="stylesheet" href="../../../assets/css/neon-theme.css">
		<link rel="stylesheet" href="../../../assets/css/neon-forms.css">
		<link rel="stylesheet" href="../../../assets/css/custom.css">
		<link rel="stylesheet" href="../../../assets/js/select2/select2-bootstrap.css">
		<link rel="stylesheet" href="../../../assets/js/select2/select2.css">
		<link rel="stylesheet" href="../../../assets/css/skins/green.css">
        <script language="javascript">
         $(function(){
			toggleTrx();
			 }); 
		function toggleTrx(){
			var hiddenDate = document.getElementById("hiddenDate");
			var dateAfter = document.getElementById("dateAfter");
			var dateBefore = document.getElementById("dateBefore");
				$("input[name*='search.tgl']").datepicker({
					maxDate : "+0M +0D", 
					dateFormat : "yy-mm-dd",
					changeMonth : true,
					changeYear : true,
					showButtonPanel : true,
					showOn : "both"
				});	
		} 
		function movePage(taskID, nik) {
			
                document.forms[0].task.value = taskID;
                document.forms[0].nik.value = nik;
                document.forms[0].submit();
            }

        </script>
    </head>

    <body class="page-body skin-green">
		<jsp:include page="/general/side_menu.jsp" />
			<jsp:include page="/general/kepala.jsp" />
    <html:form action="/feature/master/Absen.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="nik"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="currentUri"/>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.form.absen"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.id.nik"/></td>
                    <td class="Edit-Right-Text">
                        <html:text  property="search.nik" styleClass="TextBox" size="30" maxlength="10"/>
                    </td>
                </tr>
                
               <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.tanggal"/></td>
			      <td class="Edit-Right-Text">
					<html:text  styleId="dateBefore" property="search.tgl" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="TextBox" />
					&nbsp;
						- 
					&nbsp;&nbsp; 
					<html:text styleId="dateAfter" property="search.tglAfter" onkeydown="return disableInput();"  size="8" maxlength="8" styleClass="TextBox" />
			      </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search" width="100" height="31"
                                 border="0" title="<bean:message key="tooltip.search"/>">
                        </a>
                    </td>
                </tr>
             </table>
            <div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;">
                <table border="0" width="100%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="10%" align="center"><bean:message key="common.id.nik"/></td>
                        <td width="10%" align="center"><bean:message key="common.tanggal"/></td>
                        <td width="10%" align="center"><bean:message key="common.in"/></td>
                        <td width="10%" align="center"><bean:message key="common.out"/></td>
                        <td width="10%" align="center"><bean:message key="common.gpslatitudemsk"/></td>
                        <td width="10%" align="center"><bean:message key="common.gpslongitudemsk"/></td>
                        <td width="20%" align="center"><bean:message key="common.lokasimsk"/></td>
                        <td width="10%" align="center"><bean:message key="common.lmbr"/></td>
                        <td width="10%" align="center"><bean:message key="common.note"/></td>
                        <td width="10%" align="center"><bean:message key="common.gpslatitudeplg"/></td>
                        <td width="10%" align="center"><bean:message key="common.gpslongitudeplg"/></td>
                        <td width="20%" align="center"><bean:message key="common.lokasiplg"/></td>
                        <td width="10%" align="center"><bean:message key="common.jarakmsk"/></td>
                        <td width="10%" align="center"><bean:message key="common.jarakplg"/></td>
                        <td width="10%" align="center"><bean:message key="common.jamkerja"/></td>
                    </tr>
                    <logic:notEmpty name="listAbsen" scope="request">
                        <logic:iterate id="data" name="listAbsen" scope="request" indexId="i">
                         <%
                            String classColor = ((i % 2) != 1) ? "evalcol" : "oddcol";
                        %>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'" 
					   		onmouseout="this.className='<%=classColor%>'">
                                <td><bean:write name="data" property="nik"/></td>
                                <td><bean:write name="data" property="tgl"/></td>
                                <td><bean:write name="data" property="jamMasuk"/></td>
                                <td><bean:write name="data" property="jamKeluar"/></td>
                                <td><bean:write name="data" property="gpsLatitudeMsk"/></td>
                                <td><bean:write name="data" property="gpsLongitudeMsk"/></td>
                                <td><bean:write name="data" property="lokasiMsk"/></td>
                                <td align="center">
                                <logic:equal name="data" property="lembur" value="1">
                                    <img src="../../../images/icon/IconYes.gif" border="0" width="16" height="16"/>
                                </logic:equal>
                                <logic:notEqual name="data" property="lembur" value="1">
                                    <img src="../../../images/icon/IconInactive.gif" border="0" width="16" height="16"/>
                                </logic:notEqual>
                           		</td>
                           		<td><bean:write name="data" property="note"/></td>
                           		<td><bean:write name="data" property="gpsLatitudePlg"/></td>
                                <td><bean:write name="data" property="gpsLongitudePlg"/></td>
                                <td><bean:write name="data" property="lokasiPlg"/></td>
                                <td><bean:write name="data" property="jrkMsk"/></td>
                                <td><bean:write name="data" property="jrkPlg"/></td>
                                <td><bean:write name="data" property="totalJamKerja"/></td>
                            </tr>

                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listAbsen" scope="request">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="15" align="center">
                                <font class="errMsg"><bean:message key="common.listnotfound"/></font>
                            </td>
                        </tr>
                    </logic:empty>
                </table>
            </div>
            <logic:notEmpty name="listAbsen" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.absenForm,'First')" title="First">
                                <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.absenForm,'Prev')" title="Previous">
                                <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="absenForm" property="paging.currentPageNo" size="3" maxlength="6"
                                       styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <html:hidden property="paging.totalRecord" write="false"/>
                            <a href="javascript:moveToPageNumber(document.absenForm,'Go');">
                                <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                     border="0"/></a></td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.absenForm,'Next')" title="Next">
                                <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.absenForm,'Last')" title="Last">
                                <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:define id="currentPage" name="absenForm" property="paging.currentPageNo"
                                         type="java.lang.Integer"/>
                            <bean:define id="total" name="absenForm" property="paging.totalRecord"
                                         type="java.lang.Integer"/>
                            <bean:define id="perpage" name="absenForm" property="paging.rowPerPage"
                                         type="java.lang.Integer"/>
                            <%
                                int startShow = 1;
                                int endShow = 0;
                                if (currentPage > 1) {
                                    int tempstart = ((currentPage - 1) * perpage) + 1;
                                    int tempend = currentPage * perpage;
                                    if (total < tempend) {
                                        tempend = total;
                                    }
                                    startShow = tempstart;
                                    endShow = tempend;
                                } else {
                                    endShow = total;
                                }
                            %>
                            <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                            <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
            
        </div>
    </html:form>
    </body>
</html:html>