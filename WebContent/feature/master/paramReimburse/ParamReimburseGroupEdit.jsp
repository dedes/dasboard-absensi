<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title>Jabatan . Edit</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            $(function () {
                toggle_usagetype();
            });

            function movePage(taskID) {
                if (taskID == "Save" || taskID == "Update") {
                    document.forms[0].task.value = taskID;
                    document.forms[0].submit();
                }
                else if (taskID == "Load") {
                    document.forms[0].task.value = taskID;
                    document.forms[0].submit();
                }
            }

            function toggle_usagetype() {
                var usage_type = $("#ut_id").val();

                if (usage_type == '020') {
                    $("#total_id").show();
                } else {
                    $("#total_val_id").val(null);
                    $("#total_id").hide();
                }
            }
        </script>
    </head>
    <body>
    <html:form action="/feature/master/Jabatan.do" method="POST">
        <html:hidden property="task"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <html:hidden property="currentUri"/>
        <logic:equal name="jabatanForm" property="task" value="Edit" scope="request">
            <html:hidden property="id"/>
        </logic:equal>
        <%-- 	<%@include file="../../../Error.jsp"%> --%>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <bean:message key="app.privilege.form"/> -
                        <logic:equal name="jabatanForm" property="task" value="Edit" scope="request"><bean:message
                                key="common.edit"/></logic:equal>
                        <logic:equal name="jabatanForm" property="task" value="Add" scope="request"><bean:message
                                key="common.add"/></logic:equal>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.id.jabatan"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="jabatanForm" property="id" size="20" maxlength="6"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="id"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.namj"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="jabatanForm" property="namaJabatan" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="namaJabatan"/></font>
                    </td>
                </tr>
               
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <% String action = ""; %>
                        <logic:equal value="Add" property="task" name="jabatanForm">
                            <% action = Global.WEB_TASK_INSERT; %>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="jabatanForm">
                            <% action = Global.WEB_TASK_UPDATE; %>
                        </logic:equal>
                        <a href="javascript:movePage('<%=action%>');">
                            <img src="../../../images/button/ButtonSubmit.png" alt="Action <%=action %>"
                                 title="Action <%=action %>" width="100" height="31" border="0">
                        </a>
                        &nbsp;
                        <a href="javascript:movePage('Load');">
                            <img src="../../../images/button/ButtonCancel.png" alt="Cancel" title="Cancel" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>