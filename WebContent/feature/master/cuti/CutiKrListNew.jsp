<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.TITLE_APP_USER;; %>
<html:html>
<html:base/>
<head>
	<title><bean:message key="cuti"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	
    <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
    <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        
	<link rel="stylesheet" href="../../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../../assets/css/custom.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../../assets/css/skins/green.css">

	<script src="../../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<script language="javascript">
	
	 function movePage(taskID, nik) {
			
         document.forms[0].task.value = taskID;
         document.forms[0].nik.value = nik;
         document.forms[0].submit();
     }
	
</script>
</head>
<body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
				<ol class="breadcrumb bc-2">
					<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="detail.data"/></a></li>
					<li class="active"><a href="<%=request.getContextPath()%>/feature/master/CutiKrywn.do"><bean:message key="cuti"/></a></li>
				</ol>
				<div class="row">
					<html:form action="/feature/master/CutiKrywn.do" method="POST">
					<html:hidden property="task" value="Load"/>
					<html:hidden property="nik"/>
				<%-- 		<html:hidden property="groupbranch" styleId="member"/> --%>
					<html:hidden property="paging.dispatch" value="Go"/>
					<bean:define id="indexLastRow" name="cutiForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<bean:define id="indexFirstRow" name="cutiForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="panel panel-primary" data-collapsed="0">
						<div class="panel-heading" style="background:#00a651;">
							<div  class="panel-title"><span style="color:#FFFFFF;" ><bean:message key="feature.form.cuti"/></span></div>
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="javascript:movePage('Load');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<br/>
							<div class="row">
								<div class="col-md-12 col-xl-8" >
									<table class="table responsive table-bordered"  >
										<thead >
											<tr align="center">
										      	<td><bean:message key="common.id.nik"/></td>
										      	<td><bean:message key="common.namakary"/></td>
										      	<td><bean:message key="common.leader"/></td>
										      	<td><bean:message key="common.tglmulai"/></td>
										      	<td><bean:message key="common.tglselesai"/></td>
										      	<td><bean:message key="common.tglkerja"/></td>
										      	<td><bean:message key="common.jmlcuti"/></td>
										      	<td><bean:message key="common.almtcuti"/></td>
										      	<td><bean:message key="common.kprlncuti"/></td>
										      	<td><bean:message key="common.status"/></td>
										    </tr>
										</thead>
										<tbody>
											<logic:notEmpty name="listCuti" scope="request" >
												<logic:iterate id="listParam" name="listCuti" scope="request" indexId="index">
													<%indexLastRow++;%>
													<tr>
													    <td align="left"><bean:write name="listParam" property="nik"/></td>
													    <td align="left"><bean:write name="listParam" property="nama"/></td>
													    <td align="left"><bean:write name="listParam" property="leader"/></td>
													    <td align="left"><bean:write name="listParam" property="tglMulai"/></td>
													    <td align="left"><bean:write name="listParam" property="tglSelesai"/></td>
													    <td align="left"><bean:write name="listParam" property="tglKerja"/></td>
													    <td align="left"><bean:write name="listParam" property="jmlCuti"/></td>
													    <td align="left"><bean:write name="listParam" property="almtCuti"/></td>
													    <td align="left"><bean:write name="listParam" property="keperluanCuti"/></td>
													    
													    <td align="center" valign="middle">
															 <logic:equal value="A" name="listParam" property="status">
															    	<img src="../../../images/icon/IconActive.gif" alt="Active" width="16" height="16" title="<bean:message key="tooltip.approve"/>"/>
															 </logic:equal>
															 <logic:notEqual value="A" name="listParam" property="status">
														    	
														    </logic:notEqual>
															 <logic:equal value="R" name="listParam" property="status">
																    <img src="../../../images/icon/IconInactive.gif" alt="Inactive" width="16" height="16" title="<bean:message key="tooltip.reject"/>"/>
													     	 </logic:equal>
													     	 <logic:notEqual value="R" name="listParam" property="status">
														    	
														    </logic:notEqual>
													    </td>
								      				</tr>
								      				
								   				</logic:iterate>
								  			</logic:notEmpty>
											<logic:empty name="listCuti" scope="request">
											   <tr>
											      <td colspan="10" align="center" ><font class="errMsg"><bean:message key="common.listnotfound"/></font></td>
											   </tr>
								   			</logic:empty>
										</tbody>
									</table>
								</div>
							</div>
							<div>
							<logic:notEmpty name="listCuti" scope="request">
								<div class="row">
					    			<div class=" col-md-5 "> 
							        	<button type="button" onclick="javascript:moveToPageNumber(document.cutiForm,'First')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-left"></i></button>
							        	
							        	<button type="button" onclick="javascript:moveToPageNumber(document.cutiForm,'Prev')" class="btn btn-xs btn-default"><i class="fa fa-angle-left"></i></button>
					    				
					    				<html:text name="cutiForm" property="paging.currentPageNo" size="3" maxlength="6" styleClass="inpType "/> <bean:message key="paging.of"/> <html:hidden property="paging.totalPage" write="true" />
							    		
							    		<button type="button" onclick="javascript:moveToPageNumber(document.cutiForm,'Go')" class="btn btn-xs btn-default"><i class="fa fa-search"></i></button>
							    		
							        	<button type="button" onclick="javascript:moveToPageNumber(document.cutiForm,'Next')" class="btn btn-xs btn-default"><i class="fa fa-angle-right"></i></button>
					    				
						        		<button type="button" onclick="javascript:moveToPageNumber(document.cutiForm,'Last')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-right"></i></button>
				    					
					    			</div>
					    			<div class="pull-right col-md-4">
				    					<label class="pull-right"><bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message key="paging.showing.to"/> <%=indexLastRow-1%> <bean:message key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/> <bean:message key="paging.showing.record"/></label>
					    			</div>
						      	</div> 	
					  		</logic:notEmpty> 
							</div>
							<br/>
							
						</div> <!--  END PANEL BODY -->
					</div>
				</div>
			</html:form>
			</div>
		</div>	
	</div> 	  
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../../assets/js/bootstrap.js"></script>
	<script src="../../../assets/js/joinable.js"></script>
	<script src="../../../assets/js/resizeable.js"></script>
	<script src="../../../assets/js/neon-api.js"></script>
	<script src="../../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../../assets/js/jquery.validate.min.js"></script>
	<script src="../../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../../assets/js/jquery.multi-select.js"></script>
	<script src="../../../assets/js/neon-chat.js"></script>
	<script src="../../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../../assets/js/select2/select2.min.js"></script>
	<script src="../../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../../assets/js/typeahead.min.js"></script>
	<script src="../../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../assets/js/neon-demo.js"></script>
</html:html>