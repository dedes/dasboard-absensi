<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
      
        function taskApprove(nik) {
            if (!confirm('<bean:message key="common.confirmapprove" />'))
                return;
            document.forms[0].task.value = 'Update';
            document.forms[0].nik.value = nik;
            document.forms[0].submit();
        }
		function taskDelete(nik) {
            if (!confirm('<bean:message key="common.confirmreject" />'))
                return;
            document.forms[0].task.value = 'Delete';
            document.forms[0].nik.value = nik;
            document.forms[0].submit();
        }
        </script>
    </head>

    <body>
    <html:form action="/feature/master/ApproveCuti.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="nik"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="currentUri"/>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.app.cuti"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
          
            <div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;">
                <table border="0" width="100%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="5%" rowspan="2" align="center"><bean:message key="common.id.nik"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="common.namakary"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="common.leader"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="common.tglmulai"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="common.tglselesai"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="common.tglkerja"/></td>
                        <td width="5%" rowspan="2" align="center"><bean:message key="common.jmlcuti"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="common.almtcuti"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="common.kprlncuti"/></td>
                   		<td width="20%" colspan="3" align="center"><bean:message key="common.action"/></td>
                    </tr>
                    <tr align="center" class="headercol">
                        <td width="10%" align="center"><bean:message key="tooltip.button.app"/></td>
                        <td width="10%" align="center"><bean:message key="tooltip.button.reject"/></td>
                    </tr>
                    <logic:notEmpty name="listCuti" scope="request">
                        <logic:iterate id="data" name="listCuti" scope="request" indexId="i">
                         <%
                            String classColor = ((i % 2) != 1) ? "evalcol" : "oddcol";
                        %>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'" 
					   		onmouseout="this.className='<%=classColor%>'">
                                <td><bean:write name="data" property="nik"/></td>
                                <td><bean:write name="data" property="nama"/></td>
                                <td><bean:write name="data" property="leader"/></td>
                                <td><bean:write name="data" property="tglMulai"/></td>
                                <td><bean:write name="data" property="tglSelesai"/></td>
                                <td><bean:write name="data" property="tglKerja"/></td>
                                <td><bean:write name="data" property="jmlCuti"/></td>
                                <td><bean:write name="data" property="almtCuti"/></td>
                           		<td><bean:write name="data" property="keperluanCuti"/></td>
                           		<td align="center">
                                    <a title="Approve"
                                       href="javascript:taskApprove('<bean:write name="data" property="nik"/>');">
                                        <img src="../../../images/icon/IconYes.gif" width="16" height="13" border="0"/>
                                    </a>
                                </td>
                                <td align="center">
                                <a title="Reject"
                                   href="javascript:taskDelete('<bean:write name="data" property="nik"/>');">
                                    <img src="../../../images/icon/IconInactive.gif" width="16" height="16" border="0"/>
                                </a>
                            </td>
                            </tr>

                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listCuti" scope="request">
                        <tr class="evalcol">
                            <td bgcolor="FFFFFF" colspan="11" align="center">
                                <font class="errMsg"><bean:message key="common.listnotfound"/></font>
                            </td>
                        </tr>
                    </logic:empty>
                </table>
            </div>
            <logic:notEmpty name="listCuti" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.cutiForm,'First')" title="First">
                                <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.cutiForm,'Prev')" title="Previous">
                                <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="cutiForm" property="paging.currentPageNo" size="3" maxlength="6"
                                       styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <html:hidden property="paging.totalRecord" write="false"/>
                            <a href="javascript:moveToPageNumber(document.cutiForm,'Go');">
                                <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                     border="0"/></a></td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.cutiForm,'Next')" title="Next">
                                <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.cutiForm,'Last')" title="Last">
                                <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:define id="currentPage" name="cutiForm" property="paging.currentPageNo"
                                         type="java.lang.Integer"/>
                            <bean:define id="total" name="cutiForm" property="paging.totalRecord"
                                         type="java.lang.Integer"/>
                            <bean:define id="perpage" name="cutiForm" property="paging.rowPerPage"
                                         type="java.lang.Integer"/>
                            <%
                                int startShow = 1;
                                int endShow = 0;
                                if (currentPage > 1) {
                                    int tempstart = ((currentPage - 1) * perpage) + 1;
                                    int tempend = currentPage * perpage;
                                    if (total < tempend) {
                                        tempend = total;
                                    }
                                    startShow = tempstart;
                                    endShow = tempend;
                                } else {
                                    endShow = total;
                                }
                            %>
                            <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                            <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
            
        </div>
    </html:form>
    </body>
</html:html>