<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title>Cuti . Edit</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
        $(function(){
			toggleTrx();
			toggleTrx2();
			toggleTrx3();
			 }); 
		function toggleTrx(){
			
			var date = document.getElementById("date");
				$("input[name*='tglMulai']").datepicker({
					maxDate : "+0M +0D", 
					dateFormat : "yy-mm-dd",
					changeMonth : true,
					changeYear : true,
					showButtonPanel : true,
					showOn : "both"
				});	
		} 
		function toggleTrx2(){
			
			var date2 = document.getElementById("date2");
				$("input[name*='tglSelesai']").datepicker({
					maxDate : "+0M +0D", 
					dateFormat : "yy-mm-dd",
					changeMonth : true,
					changeYear : true,
					showButtonPanel : true,
					showOn : "both"
				});	
		} 
		
		function toggleTrx3(){
			
			var date3 = document.getElementById("date3");
				$("input[name*='tglKerja']").datepicker({
					maxDate : "+0M +0D", 
					dateFormat : "yy-mm-dd",
					changeMonth : true,
					changeYear : true,
					showButtonPanel : true,
					showOn : "both"
				});	
		} 
		
		 function movePage(taskID) {
             if (taskID == "Save") {
                 document.forms[0].task.value = taskID;
                 document.forms[0].submit();
                /*  alert('<bean:message key="common.process.save"/>'); */
             } else if (taskID == "Load") {
                 document.forms[0].task.value = taskID;
                 document.forms[0].submit();
             }
         }
		 
		 

           
        </script>
    </head>
    <body>
   <html:form action="/feature/master/Cuti.do" method="post">
        <html:hidden property="task"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.form.cuti"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.id.nik"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="cutiForm" property="nik" size="20" maxlength="6"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="nik"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.namakary"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="cutiForm" property="nama" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="nama"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.leader"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="cutiForm" property="leader" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="leader"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.tglmulai"/></td>
                    <td class="Edit-Right-Text">
                        <html:text  styleId="date" property="tglMulai" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="TextBox" />
                    	<font color="#FF0000">*)<html:errors property="tglMulai"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.tglselesai"/></td>
                    <td class="Edit-Right-Text">
                        <html:text  styleId="date2" property="tglSelesai" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="TextBox" />
                    	<font color="#FF0000">*)<html:errors property="tglSelesai"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.tglkerja"/></td>
                    <td class="Edit-Right-Text">
                        <html:text  styleId="date3" property="tglKerja" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="TextBox" />
                    	<font color="#FF0000">*)<html:errors property="tglKerja"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.jmlcuti"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="cutiForm" property="jmlCuti" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.almtcuti"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="cutiForm" property="almtCuti" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.kprlncuti"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="cutiForm" property="keperluanCuti" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
               
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                          <a href="javascript:movePage('Save')"><img src="../../../images/button/ButtonSubmit.png"
                                                                     alt="Add" width="100" height="31" border="0"></a>
                        &nbsp;
                        <a href="javascript:movePage('Load');">
                            <img src="../../../images/button/ButtonCancel.png" alt="Cancel" title="Cancel" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>