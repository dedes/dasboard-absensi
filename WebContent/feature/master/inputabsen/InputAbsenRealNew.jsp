<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.TITLE_APP_USER;; %>

<html:html>
<html:base/>
<head>
	<title><bean:message key="absen"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	
    <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
    <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        
	<link rel="stylesheet" href="../../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../../assets/css/custom.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../../assets/css/skins/green.css">

	<script src="../../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script language="javascript">
	
	function movePage(taskID, gpsLatitudeMsk, gpsLongitudeMsk) {
		
	        document.forms[0].task.value = taskID;
	        document.forms[0].gpsLatitudeMsk.value = document.getElementById("gpsLatitudeMsk").value;
	        document.forms[0].gpsLongitudeMsk.value = document.getElementById("gpsLongitudeMsk").value;
	        document.forms[0].submit();
	        console.log("Latitude: " + gpsLatitudeMsk + "<br>Longitude: " + gpsLongitudeMsk) ;
	    }
	
	function getLocation() {
	  if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(showPosition);
	  } else { 
	    console.log("Geolocation is not supported by this browser.") ;
	  }
	}

	function showPosition(position) {
		var lat = position.coords.latitude;
      	var lon = position.coords.longitude;
		document.getElementById('gpsLatitudeMsk').value = String(lat);
		document.getElementById('gpsLongitudeMsk').value = String(lon);
	}
	
	
</script>
</head>
<% String message = (String)request.getAttribute("alertMsg");%>
<body class="page-body skin-green">
	<%-- <logic:messagesPresent> --%>
		<script type="text/javascript">
			$(function(){
				var msg = "<%=message%>";
				var inf = "<html:errors property='nik' />";
				var note = "<html:errors property='note' />";
				var alas = "<html:errors property='alasan' />";
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-full-width",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					if (msg != 'null'){
						toastr.info(msg, "", opts);
					}
					if (inf != '' && inf != note && inf != alas){
						toastr.error(inf, "", opts);
					} else if (note != '' && note != inf && note != alas){
						toastr.error(note, "", opts);
					}else if (alas != '' && alas != inf && alas != note){
						toastr.error(alas, "", opts);
					}
			});
		</script>
	<%-- </logic:messagesPresent> --%>
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
				<ol class="breadcrumb bc-2">
					<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="Input.absen"/></a></li>
					<li class="active"><a href="<%=request.getContextPath()%>/feature/master/InputAbsen.do"><bean:message key="absen"/></a></li>
				</ol>
				<div class="row">
					<html:form action="/feature/master/InputAbsen.do" method="POST">
					<html:hidden property="task" value="Load"/>
					<html:hidden property="nik"/>
				<%-- 		<html:hidden property="groupbranch" styleId="member"/> --%>
					<html:hidden property="paging.dispatch" value="Go"/>
					<bean:define id="indexLastRow" name="inputabsenForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<bean:define id="indexFirstRow" name="inputabsenForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="panel panel-primary" data-collapsed="0">
						<div class="panel-heading" style="background:#00a651;">
							<div  class="panel-title"><span style="color:#FFFFFF;" ><bean:message key="feature.form.absen"/></span></div>
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="javascript:movePage('Load');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						<div class="panel-body">
						<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="row">
										<div align="center" class="col-md-4 col-md-offset-4">
											<label for="selamat" class=" control-label"><bean:message key="common.selamat"/></label>
										</div>
									</div>
									<div class="row">
										<div align="center" class="col-md-4 col-md-offset-4">
											<label for="selamat" class=" control-label"><bean:write name="SESS_LOGIN" property="fullName" /></label>
										</div>
									</div>
									<div class="row">
										<div align="center" class="col-md-4 col-md-offset-4">
											<label for="silahkan" class=" control-label"><bean:message key="common.silahkan"/></label>
										</div>
									</div>
									<div class="row">
										<div align="center" class="col-md-4 col-md-offset-4">
											<button type="button" onclick="javascript:getLocation();" class="btn btn-blue"  title="<bean:message key="tooltip.show"/>"><bean:message key="tooltip.show"/></button>
										 	<br/><br/>
											<!-- Latitude: <span id="gpsLatitudeMsk"></span> 
											<br/>
											Longitude: <span id="gpsLongitudeMsk"></span> -->
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label for='idProject' class="col-sm-3 control-label"><bean:message key="common.gpslatitude"/></label>
											<div class="col-sm-5">
												<html:text property="gpsLatitudeMsk" styleClass="form-control" styleId="gpsLatitudeMsk" size="30" maxlength="150"/>
											</div>
										</div>
									</div> 
										<div align="center" class="col-md-4 col-md-offset-4">
											<div align="center" class="form-group">
												<label for="gpsLatitudeMsk" class=" control-label"><bean:message key="common.gpslatitude"/></label>
												<html:text property="gpsLatitudeMsk" styleClass="form-control" styleId="gpsLatitudeMsk" size="30" maxlength="150"/>
												<br/>
												<label for="gpsLongitudeMsk" class=" control-label"><bean:message key="common.gpslongitude"/></label>
												<html:text property="gpsLongitudeMsk" styleClass="form-control" styleId="gpsLongitudeMsk" size="30" maxlength="150"/>
											</div>
										</div>
									</div> 
									<div class="row">
										<div align="center" class="col-md-4 col-md-offset-4">
												<label style="color:transparent!important;">&nbsp;&nbsp;</label> <br/>
												<button type="button" onclick="javascript:movePage('Add','gpsLatitudeMsk','gpsLongitudeMsk');" class="btn btn-blue"  title="<bean:message key="tooltip.in"/>"><bean:message key="tooltip.in"/></button>			
												<button type="button" onclick="javascript:movePage('Edit','idProject');" class="btn btn-blue"  title="<bean:message key="tooltip.out"/>"><bean:message key="tooltip.out"/></button>
										</div>
									</div>
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="col-md-12 col-xl-8" >
									<table width="100%" class="table responsive table-bordered"  >
										<thead>
											<tr align="center" >
										      	<td bgcolor="green"><bean:message key="common.id.nik"/></td>
										      	<td><span style="color:black!importan; font-weight: 10px!importan;"><bean:message key="common.id.project"/></span></td>							      	
										      	<td width="10%"><bean:message key="common.tglAbsen"/></td>
										      	<td><bean:message key="common.hari"/></td>
										      	<td><bean:message key="common.in"/></td>
										      	<td><bean:message key="common.lokasimsk"/></td>
										      	<td><bean:message key="common.out"/></td>
										      	<td><bean:message key="common.lokasiplg"/></td>
										      	<td><bean:message key="common.jamkerja"/></td>
										      	<td><bean:message key="common.status"/></td>
										    </tr>
										</thead>
										<tbody>
											<logic:notEmpty name="listInpAbsen" scope="request" >
												<logic:iterate id="listParam" name="listInpAbsen" scope="request" indexId="index">
													<%indexLastRow++;%>
													<tr>
													    <td align="left"><bean:write name="listParam" property="nik"/></td>
													    <td align="left"><bean:write name="listParam" property="idProject"/></td>
													    <td align="left"><bean:write name="listParam" property="tglAbsen"/></td>
													    <td align="left"><bean:write name="listParam" property="hariAbsen"/></td>
													    <td align="left"><bean:write name="listParam" property="jamMasuk"/></td>
													    <td align="left"><bean:write name="listParam" property="lokasiMsk"/></td>
													    <td align="left"><bean:write name="listParam" property="jamPulang"/></td>
													    <td align="left"><bean:write name="listParam" property="lokasiPlg"/></td>
													    <td align="left"><bean:write name="listParam" property="totalJamKerja"/></td>
													    <td align="center" valign="middle">
															 <logic:equal value="1" name="listParam" property="flgabs">
															    	<img src="../../../images/icon/IconActive.gif" alt="Approved" width="16" height="16" title="<bean:message key="tooltip.approve"/>"/>
															 </logic:equal>
															 <logic:equal value="0" name="listParam" property="flgabs">
																    <img src="../../../images/icon/imwaiting.gif" alt="Waiting" width="16" height="16" title="<bean:message key="tooltip.notapprove"/>"/>
													     	 </logic:equal>
													    </td>
								      				</tr>
								   				</logic:iterate>
								  			</logic:notEmpty>
											<logic:empty name="listInpAbsen" scope="request">
											   <tr>
											      <td colspan="9" align="center" ><font class="errMsg"><bean:message key="common.listnotfound"/></font></td>
											   </tr>
								   			</logic:empty>
										</tbody>
									</table>
								</div>
							</div>
							<div>
							<logic:notEmpty name="listInpAbsen" scope="request">
								<div class="row">
					    			<div class=" col-md-5 "> 
							        	<button type="button" onclick="javascript:moveToPageNumber(document.inputabsenForm,'First')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-left"></i></button>
							        	
							        	<button type="button" onclick="javascript:moveToPageNumber(document.inputabsenForm,'Prev')" class="btn btn-xs btn-default"><i class="fa fa-angle-left"></i></button>
					    				
					    				<html:text name="inputabsenForm" property="paging.currentPageNo" size="3" maxlength="6" styleClass="inpType "/> <bean:message key="paging.of"/> <html:hidden property="paging.totalPage" write="true" />
							    		
							    		<button type="button" onclick="javascript:moveToPageNumber(document.inputabsenForm,'Go')" class="btn btn-xs btn-default"><i class="fa fa-search"></i></button>
							    		
							        	<button type="button" onclick="javascript:moveToPageNumber(document.inputabsenForm,'Next')" class="btn btn-xs btn-default"><i class="fa fa-angle-right"></i></button>
					    				
						        		<button type="button" onclick="javascript:moveToPageNumber(document.inputabsenForm,'Last')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-right"></i></button>
				    					
					    			</div>
					    			<div class="pull-right col-md-4">
				    					<label class="pull-right"><bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message key="paging.showing.to"/> <%=indexLastRow-1%> <bean:message key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/> <bean:message key="paging.showing.record"/></label>
					    			</div>
						      	</div> 	
					  		</logic:notEmpty> 
							</div>
							<br/>
						</div> <!--  END PANEL BODY -->
					</div>
				</div>
			</html:form>
			</div>
		</div>	
	</div> 	  
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../../assets/js/bootstrap.js"></script>
	<script src="../../../assets/js/joinable.js"></script>
	<script src="../../../assets/js/resizeable.js"></script>
	<script src="../../../assets/js/neon-api.js"></script>
	<script src="../../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../../assets/js/jquery.validate.min.js"></script>
	<script src="../../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../../assets/js/jquery.multi-select.js"></script>
	<script src="../../../assets/js/neon-chat.js"></script>
	<script src="../../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../../assets/js/select2/select2.min.js"></script>
	<script src="../../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../../assets/js/typeahead.min.js"></script>
	<script src="../../../assets/js/icheck/icheck.min.js"></script>
	<script src="../../../assets/js/toastr.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../assets/js/neon-demo.js"></script>
</html:html>