<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = TreemasFormatter.toCamelCase(Global.TITLE_APP_USER); %>
<html:html>
<html:base/>
<head>
	<title><bean:message key="form.timesheet"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	<script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
    <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
	
	<link rel="stylesheet" href="../../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../../assets/css/custom.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../../assets/css/skins/green.css">

	<script src="../../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<script language="javascript">
	$(function () {
	    toggle_usagetype();
	});
	
	function movePage(taskID) {
	    if (taskID == "NextEdit" || taskID == "BackEdit" ) {
	        document.forms[0].task.value = taskID;
	        document.forms[0].submit();
	    }
	    else if (taskID == "Load") {
	        document.forms[0].task.value = taskID;
	        document.forms[0].submit();
	    }
	}
	
	function toggle_usagetype() {
	    var usage_type = $("#ut_id").val();
	
	    if (usage_type == '020') {
	        $("#total_id").show();
	    } else {
	        $("#total_val_id").val(null);
	        $("#total_id").hide();
	    }
	}
	
	
	</script>
</head>
<% String message = (String)request.getAttribute("alertMsg");%>
<body class="page-body skin-green">
<script type="text/javascript">
			$(function(){
				var msg = "<%=message%>";
				var inf = "<html:errors property='nik' />";
				var tms = "<html:errors property='timesheet' />";
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-full-width",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					if (msg != 'null'){
						toastr.info(msg, "", opts);
					}
					if (inf != '' && inf != tms){
						toastr.error(inf, "", opts);
					} else if (tms != '' && tms != inf){
						toastr.error(tms, "", opts);
					} 
						
				    
			});
		</script>
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
			<ol class="breadcrumb bc-2">
				<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="absen"/></a></li>
				<li><a href="<%=request.getContextPath()%>/feature/master/InputAbsen.do"><bean:message key="form.timesheet"/></a></li>
			</ol>
			<div class="row">
				<html:form action="/feature/master/InputAbsen.do" method="POST" styleClass="form-horizontal form-groups-bordered">
					<html:hidden property="task"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="panel panel-primary" data-collapsed="0">
							<div class="panel-heading" style="background:#00a651;">
								<div class="panel-title"><span style="color:#FFFFFF;" >
									<bean:message key="form.timesheet"/> 
								</span></div>
								<div class="panel-options">
									<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
									<a href="javascript:movePage('Add');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
								</div>
							</div>
							<div class="panel-body">
								 <div class="form-group">
									<label for='idProject' class="col-sm-3 control-label">
										<logic:equal value="Edit" property="task" name="inputabsenForm">
											<bean:message key="common.id.project" /><font color="#FF0000"> *)</font>
										</logic:equal>
									</label>
									<div class="col-sm-5">
										<logic:equal value="Edit" property="task" name="inputabsenForm">
											<html:text property="idProject" styleId='idProject' styleClass="form-control" readonly="true" size="50" maxlength="150" />
					   						<font color="#FF0000"><html:errors property="idProject" /></font>
					   					</logic:equal>
									</div>
								</div>
								<%-- <div class="form-group">
									<label for='note' class="col-sm-3 control-label">
									<logic:equal value="Save" property="task" name="inputabsenForm">
										<bean:message key="common.note.cepat"/> <font color="#FF0000"> *)</font>
									</logic:equal>
									</label>
									<div class="col-sm-5">
							      		<logic:equal value="Save" property="task" name="inputabsenForm">
							   				<html:text property="alasan" styleId='alasan' styleClass="form-control" readonly="true" size="50" maxlength="150" />
							      			<font color="#FF0000"><html:errors property="alasan" /></font>
							      		</logic:equal>
									</div>
								</div> --%>
								<div class="form-group">
									<label for='timesheet' class="col-sm-3 control-label">
										<bean:message key="common.note.timesheet" />
									<font color="#FF0000"> *)</font>
									</label>
									<div class="col-sm-5">
										<html:text property="timesheet" styleId='timesheet' styleClass="form-control" size="50" maxlength="150" />
							      		<font color="#FF0000"><html:errors property="timesheet" /></font>
									</div>
								</div>
								<% String action = "";%>
					      		<logic:equal value="Update" property="task" name="inputabsenForm">
					      			<% action=Global.WEB_TASK_BACK_EDIT; %>
					      		</logic:equal>
					      		<logic:equal value="Edit" property="task" name="inputabsenForm">
					      			<% action=Global.WEB_TASK_NEXT_EDIT; %>
					      		</logic:equal>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-5">
										<button type="button" onclick="javascript:movePage('<%=action%>','');" title="<bean:message key="tooltip.save"/>" class="btn btn-red"><bean:message key="tooltip.save"/></button>
										<button type="button" onclick="javascript:movePage('Load');" title="<bean:message key="tooltip.cancel"/>" class="btn btn-default"><bean:message key="tooltip.cancel"/></button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</html:form>
			</div>
		</div>
	</div>
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../../assets/js/bootstrap.js"></script>
	<script src="../../../assets/js/joinable.js"></script>
	<script src="../../../assets/js/resizeable.js"></script>
	<script src="../../../assets/js/neon-api.js"></script>
	<script src="../../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../../assets/js/jquery.validate.min.js"></script>
	<script src="../../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../../assets/js/jquery.multi-select.js"></script>
	<script src="../../../assets/js/neon-chat.js"></script>
	<script src="../../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../../assets/js/select2/select2.min.js"></script>
	<script src="../../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../../assets/js/typeahead.min.js"></script>
	<script src="../../../assets/js/icheck/icheck.min.js"></script>
	<script src="../../../assets/js/toastr.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../../assets/js/neon-demo.js"></script>
</html:html>