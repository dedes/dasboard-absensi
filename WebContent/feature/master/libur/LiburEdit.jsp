<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title>Libur . Edit</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
        $(function(){
			toggleTrx();
			 }); 
		function toggleTrx(){
			var date = document.getElementById("date");
				$("input[name*='tglLibur']").datepicker({
					maxDate : "+0M +0D", 
					dateFormat : "yy-mm-dd",
					changeMonth : true,
					changeYear : true,
					showButtonPanel : true,
					showOn : "both"
				});	
		} 
            function movePage(taskID) {
                if (taskID == "Save" || taskID == "Update") {
                    document.forms[0].task.value = taskID;
                    document.forms[0].submit();
                }
                else if (taskID == "Load") {
                    document.forms[0].task.value = taskID;
                    document.forms[0].submit();
                }
            }

           
        </script>
    </head>
    <body>
    <html:form action="/feature/master/Libur.do" method="POST">
        <html:hidden property="task"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <html:hidden property="currentUri"/>
        <logic:equal name="liburForm" property="task" value="Edit" scope="request">
            <html:hidden property="tglLibur"/>
        </logic:equal>
        <%-- 	<%@include file="../../../Error.jsp"%> --%>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <bean:message key="app.privilege.form"/> -
                        <logic:equal name="liburForm" property="task" value="Edit" scope="request"><bean:message
                                key="common.edit"/></logic:equal>
                        <logic:equal name="liburForm" property="task" value="Add" scope="request"><bean:message
                                key="common.add"/></logic:equal>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="feature.libur.description"/></td>
                    <td class="Edit-Right-Text">
                        <html:text  styleId="date" property="tglLibur" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="TextBox" />
                        <font color="#FF0000">*)<html:errors property="tglLibur"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.ket"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="liburForm" property="keterangan" size="20" maxlength="100"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="keterangan"/></font>
                    </td>
                </tr>
               
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <% String action = ""; %>
                        <logic:equal value="Add" property="task" name="liburForm">
                            <% action = Global.WEB_TASK_INSERT; %>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="liburForm">
                            <% action = Global.WEB_TASK_UPDATE; %>
                        </logic:equal>
                        <a href="javascript:movePage('<%=action%>');">
                            <img src="../../../images/button/ButtonSubmit.png" alt="Action <%=action %>"
                                 title="Action <%=action %>" width="100" height="31" border="0">
                        </a>
                        &nbsp;
                        <a href="javascript:movePage('Load');">
                            <img src="../../../images/button/ButtonCancel.png" alt="Cancel" title="Cancel" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>