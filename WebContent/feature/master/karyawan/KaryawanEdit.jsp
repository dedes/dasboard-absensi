<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title>Karyawan . Edit</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../../../style/jquery-ui.css" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
        <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
         $(function(){
			toggleTrx();
			toggleTrx2();
			 }); 
		function toggleTrx(){
			
			var date = document.getElementById("date");
				$("input[name*='tanggalLahir']").datepicker({
					maxDate : "+0M +0D", 
					dateFormat : "yy-mm-dd",
					changeMonth : true,
					changeYear : true,
					showButtonPanel : true,
					showOn : "both"
				});	
		} 
		function toggleTrx2(){
			
			
			var date2 = document.getElementById("date2");
				$("input[name*='tanggalBergabung']").datepicker({
					maxDate : "+0M +0D", 
					dateFormat : "yy-mm-dd",
					changeMonth : true,
					changeYear : true,
					showButtonPanel : true,
					showOn : "both"
				});	
		} 
			  
            function movePage(taskID) {
                if (taskID == "Save" || taskID == "Update") {
                    document.forms[0].task.value = taskID;
                    document.forms[0].submit();
                }
                else if (taskID == "Load") {
                    document.forms[0].task.value = taskID;
                    document.forms[0].submit();
                }
            }
            

            
        </script>
    </head>
    <body>
    <html:form action="/feature/master/Karyawan.do" method="POST" enctype="MULTIPART/FORM-DATA">
        <html:hidden property="task"/>
        <html:hidden property="paging.currentPageNo"/>
        <html:hidden property="paging.totalRecord"/>
        <html:hidden property="currentUri"/>
        <logic:equal name="karyawanForm" property="task" value="Edit" scope="request">
            <html:hidden property="nik"/>
        </logic:equal>
        <%-- 	<%@include file="../../../Error.jsp"%> --%>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center">
                        <bean:message key="feature.form.karyawan"/> -
                        <logic:equal name="karyawanForm" property="task" value="Edit" scope="request"><bean:message
                                key="common.edit"/></logic:equal>
                        <logic:equal name="karyawanForm" property="task" value="Add" scope="request"><bean:message
                                key="common.add"/></logic:equal>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.id.nik"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="nik" size="20" maxlength="10"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="nik"/></font>
                    </td>
                </tr>
                
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.namakary"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="nama" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        <font color="#FF0000">*)<html:errors property="nama"/></font>
                    </td>
                </tr>
                 <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.tanggalgabung"/></td>
                    <td class="Edit-Right-Text">
                        <html:text  styleId="date2" property="tanggalBergabung" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="TextBox" />
                    	
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.alamatktp"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="alamatKtp" size="20" maxlength="100"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
                 <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.alamat"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="alamatSekarang" size="20" maxlength="100"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.kodep"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="kodePos" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.tempatlahir"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="tempatLahir" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.tanggallahir"/></td>
                    <td class="Edit-Right-Text">
                        <html:text  styleId="date" property="tanggalLahir" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="TextBox" />
                    	
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.jenkel"/></td>
                    <td class="Edit-Right-Text">
                           
                        <html:select property="jenisKelamin" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
                            <html:option value="LAKI-LAKI"><bean:message key="common.lakilaki"/></html:option>
                            <html:option value="PEREMPUAN"><bean:message key="common.perempuan"/></html:option>
                        </html:select>
                        
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.goldar"/></td>
                    <td class="Edit-Right-Text">
                           
                        <html:select property="golonganDarah" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
                            <html:option value="O"><bean:message key="common.o"/></html:option>
                            <html:option value="A"><bean:message key="common.a"/></html:option>
                            <html:option value="B"><bean:message key="common.b"/></html:option>
                            <html:option value="AB"><bean:message key="common.ab"/></html:option>
                        </html:select>
                        
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.stts"/></td>
                    <td class="Edit-Right-Text">
                           
                        <html:select property="statusPerkawinan" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
                            <html:option value="BELUM MENIKAH"><bean:message key="common.belnikah"/></html:option>
                            <html:option value="MENIKAH"><bean:message key="common.nikah"/></html:option>
                        </html:select>
                        
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.agama"/></td>
                    <td class="Edit-Right-Text">
                        <html:select property="agama" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
                            <html:option value="BUDHA"><bean:message key="common.budha"/></html:option>
                            <html:option value="HINDU"><bean:message key="common.hindu"/></html:option>
                            <html:option value="ISLAM"><bean:message key="common.islam"/></html:option>
                            <html:option value="KATHOLIK"><bean:message key="common.katolik"/></html:option>
                            <html:option value="KRISTEN"><bean:message key="common.kristen"/></html:option>
                        </html:select>
                        
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.kwgn"/></td>
                    <td class="Edit-Right-Text">
                       <html:select property="kewarganegaraan" styleClass="TextBox">
                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
                            <html:option value="WNI"><bean:message key="common.wni"/></html:option>
                            <html:option value="WNA"><bean:message key="common.wna"/></html:option>
                        </html:select>
                       
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.jenpen"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="jenjangPendidikan" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.noktp"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="nomorKtp" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
               <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.npwp"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="npwp" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.nohp"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="noHP" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        
                    </td>
                </tr>
                 <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.email"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="email" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.imei"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="imei" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.emcon"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="emergencyContact" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        
                    </td>
                </tr>
               <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.sttsem"/></td>
                    <td class="Edit-Right-Text">
                       <html:text name="karyawanForm" property="statusEmergency" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.alem"/></td>
                    <td class="Edit-Right-Text">
                       <html:text name="karyawanForm" property="alamatEmergency" size="20" maxlength="100"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.telpem"/></td>
                    <td class="Edit-Right-Text">
                       <html:text name="karyawanForm" property="telpEmergency" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.norek"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="noRek" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.idp"/></td>
                    <td class="Edit-Right-Text">
                        <html:text name="karyawanForm" property="idProject" size="20" maxlength="50"
                                   styleClass="TextBox"/>
                        
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.upload"/></td>
                    <td class="Edit-Right-Text">
			            <html:file name="karyawanForm" property="upload" size="20"
			            styleClass="TextBox"/>
                    </td>
                </tr>
                
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <% String action = ""; %>
                        <logic:equal value="Add" property="task" name="karyawanForm">
                            <% action = Global.WEB_TASK_INSERT; %>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="karyawanForm">
                            <% action = Global.WEB_TASK_UPDATE; %>
                        </logic:equal>
                        <a href="javascript:movePage('<%=action%>');">
                            <img src="../../../images/button/ButtonSubmit.png" alt="Action <%=action %>"
                                 title="Action <%=action %>" width="100" height="31" border="0">
                        </a>
                        &nbsp;
                        <a href="javascript:movePage('Load');">
                            <img src="../../../images/button/ButtonCancel.png" alt="Cancel" title="Cancel" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>