<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "LOOK UP KARYAWAN"; %>
<html:html>
<html:base/>
<head>
	<title><bean:message key="karyawan"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	
    <script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
    <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
        
	<link rel="stylesheet" href="../../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../../assets/css/custom.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../../assets/css/skins/green.css">

	<script src="../../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<script language="javascript">
	
    function movePage(task) {
        document.forms[0].task.value = task;
        document.forms[0].submit();
    }

    function action(form, task, nik) {
        form.nik.value = nik;
        form.task.value = task;
        form.submit();
    }

   
</script>
</head>
<body class="page-body skin-green">
	<div class="page-container">
		<div class="main-content">
				<div class="row">
				 	<html:form action="/lookup/Karyawan.do" method="POST">
					<html:hidden property="task" value="Load"/>
					<html:hidden property="nik"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="panel panel-primary" data-collapsed="0">
						<div class="panel-heading" style="background:#00a651;">
							<div  class="panel-title"><span style="color:#FFFFFF;" ><bean:message key="feature.det.karyawan"/></span></div>
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="javascript:movePage('Load');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						<div class="panel-body">
								<div class="row">
									<div class="form-group">
										<label for='nama' class="col-sm-3 control-label"><bean:message key="common.namakary" /></label>
											<div class="col-sm-5">
												<html:hidden name="luKaryawanForm" property="nama" write="true"/>
											</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='tanggalBergabung' class="col-sm-3 control-label"><bean:message key="common.tanggalgabung" /></label>
											<div class="col-sm-2">
												<html:hidden name="luKaryawanForm" property="tanggalBergabung" write="true"/>
											</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='alamatKtp' class="col-sm-3 control-label"><bean:message key="common.alamatktp" /></label>
											<div class="col-sm-5">
												<html:hidden name="luKaryawanForm" property="alamatKtp" write="true"/>
											</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='alamatSekarang' class="col-sm-3 control-label"><bean:message key="common.alamat" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="alamatSekarang" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='kodePos' class="col-sm-3 control-label"><bean:message key="common.kodep" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="kodePos" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='tempatLahir' class="col-sm-3 control-label"><bean:message key="common.tempatlahir" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="tempatLahir" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='tanggalLahir' class="col-sm-3 control-label"><bean:message key="common.tanggallahir" /></label>
										<div class="col-sm-2">
											<html:hidden name="luKaryawanForm" property="tanggalLahir" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='jenisKelamin' class="col-sm-3 control-label"><bean:message key="common.jenkel" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="jenisKelamin" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='golonganDarah' class="col-sm-3 control-label"><bean:message key="common.goldar" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="golonganDarah" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='statusPerkawinan' class="col-sm-3 control-label"><bean:message key="common.stts" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="statusPerkawinan" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='agama' class="col-sm-3 control-label"><bean:message key="common.agama" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="agama" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='kewarganegaraan' class="col-sm-3 control-label"><bean:message key="common.kwgn" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="kewarganegaraan" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='jenjangPendidikan' class="col-sm-3 control-label"><bean:message key="common.jenpen" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="jenjangPendidikan" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='nomorKtp' class="col-sm-3 control-label"><bean:message key="common.noktp" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="nomorKtp" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='npwp' class="col-sm-3 control-label"><bean:message key="common.npwp" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="npwp" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='noHP' class="col-sm-3 control-label"><bean:message key="common.nohp" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="noHP" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='email' class="col-sm-3 control-label"><bean:message key="common.email" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="email" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='imei' class="col-sm-3 control-label"><bean:message key="common.imei" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="imei" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='emergencyContact' class="col-sm-3 control-label"><bean:message key="common.emcon" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="emergencyContact" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='statusEmergency' class="col-sm-3 control-label"><bean:message key="common.sttsem" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="statusEmergency" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='alamatEmergency' class="col-sm-3 control-label"><bean:message key="common.alem" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="alamatEmergency" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='telpEmergency' class="col-sm-3 control-label"><bean:message key="common.telpem" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="telpEmergency" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='noRek' class="col-sm-3 control-label"><bean:message key="common.norek" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="noRek" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='idProject' class="col-sm-3 control-label"><bean:message key="common.id.project" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="idProject" write="true"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for='divisi' class="col-sm-3 control-label"><bean:message key="common.divisi" /></label>
										<div class="col-sm-5">
											<html:hidden name="luKaryawanForm" property="divisi" write="true"/>
										</div>
									</div>
								</div>
								<%-- <div class="row">
									<div class="form-group">
										<label for='image' class="col-sm-3 control-label"><bean:message key="common.foto" /></label>
										<div class="col-sm-5">
											<img src="<%=request.getContextPath()%>/upload/karyawan/<bean:write name="luKaryawanForm" property="image"/>" width="80" height="80" border="0"/>
										</div>
									</div>
								</div> --%>
						</div> <!--  END PANEL BODY -->
					</div>
				</div>
			</html:form>
			</div>
		</div>	
	</div> 	  
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../../assets/js/bootstrap.js"></script>
	<script src="../../../assets/js/joinable.js"></script>
	<script src="../../../assets/js/resizeable.js"></script>
	<script src="../../../assets/js/neon-api.js"></script>
	<script src="../../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../../assets/js/jquery.validate.min.js"></script>
	<script src="../../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../../assets/js/jquery.multi-select.js"></script>
	<script src="../../../assets/js/neon-chat.js"></script>
	<script src="../../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../../assets/js/select2/select2.min.js"></script>
	<script src="../../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../../assets/js/typeahead.min.js"></script>
	<script src="../../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../assets/js/neon-demo.js"></script>
</html:html>