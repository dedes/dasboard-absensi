<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = TreemasFormatter.toCamelCase(Global.TITLE_APP_USER); %>
<html:html>
<html:base/>
<head>
	<title><bean:message key="karyawan"/> | <bean:message key="header.tittle"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
<!-- 	<link href="../../../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	<script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
    <script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
	
	<link rel="stylesheet" href="../../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../../../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../../assets/css/neon-core.css">
	<link rel="stylesheet" href="../../../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../../../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../../../assets/css/custom.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../../../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../../../assets/css/skins/green.css">

	<script src="../../../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<script language="javascript">
	$(function(){
		toggleTrx();
		toggleTrx2();
		 }); 
	function toggleTrx(){
		
		var date = document.getElementById("date");
			$("input[name*='tanggalLahir']").datepicker({
				maxDate : "+0M +0D", 
				dateFormat : "yy-mm-dd",
				changeMonth : true,
				changeYear : true,
				showButtonPanel : true,
				showOn : "both"
			});	
	} 
	function toggleTrx2(){
		var date2 = document.getElementById("date2");
			$("input[name*='tanggalBergabung']").datepicker({
				maxDate : "+0M +0D", 
				dateFormat : "yy-mm-dd",
				changeMonth : true,
				changeYear : true,
				showButtonPanel : true,
				showOn : "both"
			});	
	} 
		  
        function movePage(taskID) {
            if (taskID == "Save" || taskID == "Update") {
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }
            else if (taskID == "Load") {
                document.forms[0].task.value = taskID;
                document.forms[0].submit();
            }
        }
        
	
	</script>
</head>
<body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
			<ol class="breadcrumb bc-2">
				<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="master.data"/></a></li>
				<li><a href="<%=request.getContextPath()%>/feature/master/Karyawan.do"><bean:message key="karyawan"/></a></li>
				<li class="active">
					<logic:equal value="Add" property="task" name="karyawanForm">
						<bean:message key="tooltip.add"/> 
					</logic:equal>
					<logic:equal value="Edit" property="task" name="karyawanForm">
						<bean:message key="tooltip.edit"/> 
					</logic:equal>
				</li>
			</ol>
			<div class="row">
				<html:form action="/feature/master/Karyawan.do" method="POST" styleClass="form-horizontal form-groups-bordered" enctype="MULTIPART/FORM-DATA">
					<html:hidden property="task"/>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="panel panel-primary" data-collapsed="0">
							<div class="panel-heading" style="background:#00a651;">
								<div class="panel-title"><span style="color:#FFFFFF;" >
								<logic:equal value="Add" property="task" name="karyawanForm">
									<bean:message key="tooltip.add"/> 
								</logic:equal>
								<logic:equal value="Edit" property="task" name="karyawanForm">
									<bean:message key="tooltip.edit"/> 
								</logic:equal>
								<bean:message key="karyawan"/></span></div>
								<div class="panel-options">
									<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
									<a href="javascript:movePage('Add');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
								</div>
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label for='nik' class="col-sm-3 control-label"><bean:message key="common.id.nik" /><font color="#FF0000"> *)</font></label>
									<div class="col-sm-5">
										<logic:equal value="Add" property="task" name="karyawanForm">
											<html:text property="nik" styleId='nik' styleClass="form-control"  size="30" maxlength="6" />
							      		</logic:equal>
							      		<logic:equal value="Edit" property="task" name="karyawanForm">
							   				<html:text property="nik" styleId='nik' styleClass="form-control" readonly="true"  size="30" maxlength="6" />
							      		</logic:equal>
							      		<font color="#FF0000"><html:errors property="nik" /></font>
									</div>
								</div>
								<div class="form-group">
									<label for='nama' class="col-sm-3 control-label"><bean:message key="common.namakary" /><font color="#FF0000"> *)</font></label>
									<div class="col-sm-5">
										<html:text property="nama" styleId='nama' styleClass="form-control"  size="50" maxlength="500" />
				   						<font color="#FF0000"><html:errors property="nama" /></font>
									</div>
								</div>
								<div class="form-group">
									<label for='tanggalBergabung' class="col-sm-3 control-label"><bean:message key="common.tanggalgabung" /></label>
									<div class="col-sm-2">
				   						<html:text  styleId="date2" property="tanggalBergabung" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="form-control \" autoComplete=\"off"/>
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='alamatKtp' class="col-sm-3 control-label"><bean:message key="common.alamatktp" /></label>
									<div class="col-sm-5">
										<html:text property="alamatKtp" styleId='alamatKtp' styleClass="form-control"  size="50" maxlength="500" />
									</div>
								</div>
								<div class="form-group">
									<label for='alamatSekarang' class="col-sm-3 control-label"><bean:message key="common.alamat" /></label>
									<div class="col-sm-5">
										<html:text property="alamatSekarang" styleId='alamatSekarang' styleClass="form-control"  size="50" maxlength="500" />
									</div>
								</div>
								<div class="form-group">
									<label for='kodePos' class="col-sm-3 control-label"><bean:message key="common.kodep" /></label>
									<div class="col-sm-5">
										<html:text property="kodePos" styleId='kodePos' styleClass="form-control"  size="50" maxlength="5" />
									</div>
								</div>
								<div class="form-group">
									<label for='tempatLahir' class="col-sm-3 control-label"><bean:message key="common.tempatlahir" /></label>
									<div class="col-sm-5">
										<html:text property="tempatLahir" styleId='tempatLahir' styleClass="form-control"  size="50" maxlength="500" />
									</div>
								</div>
								<div class="form-group">
									<label for='tanggalLahir' class="col-sm-3 control-label"><bean:message key="common.tanggallahir" /></label>
									<div class="col-sm-2">
				   						<html:text  styleId="date" property="tanggalLahir" onkeydown="return disableInput();" size="8" maxlength="8" styleClass="form-control \" autoComplete=\"off" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='jenisKelamin' class="col-sm-3 control-label"><bean:message key="common.jenkel" /></label>
									<div class="col-sm-5">
										<html:select property="jenisKelamin" styleId='jenisKelamin' styleClass="form-control" >
				                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
				                            <html:option value="LAKI-LAKI"><bean:message key="common.lakilaki"/></html:option>
				                            <html:option value="PEREMPUAN"><bean:message key="common.perempuan"/></html:option>
				                        </html:select>
									</div>
								</div>
								<div class="form-group">
									<label for='golonganDarah' class="col-sm-3 control-label"><bean:message key="common.goldar" /></label>
									<div class="col-sm-5">
										<html:select property="golonganDarah" styleId='golonganDarah' styleClass="form-control" >
				                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
				                            <html:option value="O"><bean:message key="common.o"/></html:option>
				                            <html:option value="A"><bean:message key="common.a"/></html:option>
				                            <html:option value="B"><bean:message key="common.b"/></html:option>
				                            <html:option value="AB"><bean:message key="common.ab"/></html:option>
				                        </html:select>
									</div>
								</div>
								<div class="form-group">
									<label for='statusPerkawinan' class="col-sm-3 control-label"><bean:message key="common.stts" /></label>
									<div class="col-sm-5">
										<html:select property="statusPerkawinan" styleId='statusPerkawinan' styleClass="form-control" >
				                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
				                            <html:option value="BELUM MENIKAH"><bean:message key="common.belnikah"/></html:option>
				                            <html:option value="MENIKAH"><bean:message key="common.nikah"/></html:option>
				                        </html:select>
									</div>
								</div>
								<div class="form-group">
									<label for='agama' class="col-sm-3 control-label"><bean:message key="common.agama" /></label>
									<div class="col-sm-5">
										<html:select property="agama" styleId='agama' styleClass="form-control"   >
				                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
				                            <html:option value="BUDHA"><bean:message key="common.budha"/></html:option>
				                            <html:option value="HINDU"><bean:message key="common.hindu"/></html:option>
				                            <html:option value="ISLAM"><bean:message key="common.islam"/></html:option>
				                            <html:option value="KATHOLIK"><bean:message key="common.katolik"/></html:option>
				                            <html:option value="KRISTEN"><bean:message key="common.kristen"/></html:option>
				                        </html:select>
									</div>
								</div>
								<div class="form-group">
									<label for='kewarganegaraan' class="col-sm-3 control-label"><bean:message key="common.kwgn" /></label>
									<div class="col-sm-5">
										<html:select property="kewarganegaraan" styleId='kewarganegaraan' styleClass="form-control"  >
				                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
				                            <html:option value="WNI"><bean:message key="common.wni"/></html:option>
				                            <html:option value="WNA"><bean:message key="common.wna"/></html:option>
				                        </html:select>
									</div>
								</div>
								<div class="form-group">
									<label for='jenjangPendidikan' class="col-sm-3 control-label"><bean:message key="common.jenpen" /></label>
									<div class="col-sm-5">
				   						<html:select property="jenjangPendidikan" styleId='jenjangPendidikan' styleClass="form-control"  >
				                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
				                            <html:option value="SMA"><bean:message key="common.sma"/></html:option>
				                            <html:option value="D-3"><bean:message key="common.d3"/></html:option>
				                            <html:option value="S-1"><bean:message key="common.s1"/></html:option>
				                            <html:option value="S-2"><bean:message key="common.s2"/></html:option>
				                        </html:select>
									</div>
								</div>
								<div class="form-group">
									<label for='nomorKtp' class="col-sm-3 control-label"><bean:message key="common.noktp" /></label>
									<div class="col-sm-5">
										<html:text property="nomorKtp" styleId='nomorKtp' styleClass="form-control"  size="50" maxlength="50" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='npwp' class="col-sm-3 control-label"><bean:message key="common.npwp" /></label>
									<div class="col-sm-5">
										<html:text property="npwp" styleId='npwp' styleClass="form-control"  size="50" maxlength="50" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='noHP' class="col-sm-3 control-label"><bean:message key="common.nohp" /></label>
									<div class="col-sm-5">
										<html:text property="noHP" styleId='noHP' styleClass="form-control"  size="50" maxlength="50" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='email' class="col-sm-3 control-label"><bean:message key="common.email" /></label>
									<div class="col-sm-5">
										<html:text property="email" styleId='email' styleClass="form-control"  size="50" maxlength="50" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='imei' class="col-sm-3 control-label"><bean:message key="common.imei" /><font color="#FF0000"> *)</font></label>
									<div class="col-sm-5">
										<html:text property="imei" styleId='imei' styleClass="form-control"  size="50" maxlength="500" />
				   						<font color="#FF0000"><html:errors property="imei" /></font>
									</div>
								</div>
								<div class="form-group">
									<label for='emergencyContact' class="col-sm-3 control-label"><bean:message key="common.emcon" /></label>
									<div class="col-sm-5">
										<html:text property="emergencyContact" styleId='emergencyContact' styleClass="form-control"  size="50" maxlength="50" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='statusEmergency' class="col-sm-3 control-label"><bean:message key="common.sttsem" /></label>
									<div class="col-sm-5">
										<html:text property="statusEmergency" styleId='statusEmergency' styleClass="form-control"  size="50" maxlength="50" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='alamatEmergency' class="col-sm-3 control-label"><bean:message key="common.alem" /></label>
									<div class="col-sm-5">
										<html:text property="alamatEmergency" styleId='alamatEmergency' styleClass="form-control"  size="50" maxlength="500" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='telpEmergency' class="col-sm-3 control-label"><bean:message key="common.telpem" /></label>
									<div class="col-sm-5">
										<html:text property="telpEmergency" styleId='telpEmergency' styleClass="form-control"  size="50" maxlength="50" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='noRek' class="col-sm-3 control-label"><bean:message key="common.norek" /></label>
									<div class="col-sm-5">
										<html:text property="noRek" styleId='noRek' styleClass="form-control"  size="50" maxlength="50" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='idProject' class="col-sm-3 control-label"><bean:message key="common.id.project" /></label>
									<div class="col-sm-5">
										<html:text property="idProject" styleId='idProject' styleClass="form-control"  size="50" maxlength="20" />
				   						
									</div>
								</div>
								<div class="form-group">
									<label for='divisi' class="col-sm-3 control-label"><bean:message key="common.divisi" /></label>
									<div class="col-sm-5">
										<%-- <html:text property="divisi" styleId='divisi' styleClass="form-control"  size="50" maxlength="50" /> --%>
				   						<html:select property="divisi" styleId='divisi' styleClass="form-control"  >
				                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
				                            <html:option value="HRD"><bean:message key="common.hrd"/></html:option>
				                            <html:option value="FINANCE"><bean:message key="common.finance"/></html:option>
				                            <html:option value="PM"><bean:message key="common.pm"/></html:option>
				                            <html:option value="PROGRAMER AS 400"><bean:message key="common.as400"/></html:option>
				                            <html:option value="PROGRAMER JAVA"><bean:message key="common.java"/></html:option>
				                            <html:option value="PROGRAMER .NET"><bean:message key="common.net"/></html:option>
				                            <html:option value="TESTER"><bean:message key="common.tester"/></html:option>
				                        </html:select>
									</div>
								</div>
								<div class="form-group">
									<label for='isLeader' class="col-sm-3 control-label"><bean:message key="common.leader" /></label>
									<div class="col-sm-5">
										<html:select property="isLeader" styleId='isLeader' styleClass="form-control"  >
				                            <html:option value=""><bean:message key="common.choose.one"/></html:option>
				                            <html:option value="1"><bean:message key="common.isLeaderT"/></html:option>
				                            <html:option value="0"><bean:message key="common.isLeaderF"/></html:option>
				                        </html:select>
									</div>
								</div>
								<%-- <div class="form-group">
									<label for='upload' class="col-sm-3 control-label"><bean:message key="common.upload" /></label>
									<div class="col-sm-5">
									<html:file name="karyawanForm" property="upload" styleId='upload' size="20" styleClass="form-control"/>
										
									</div>
								</div>
								<div class="form-group">
									<label for='uploadKTP' class="col-sm-3 control-label"><bean:message key="common.uploadKTP" /></label>
									<div class="col-sm-5">
									<html:file name="karyawanForm" property="uploadKTP" styleId='uploadKTP' size="20" styleClass="form-control"/>
										
									</div>
								</div>
								<div class="form-group">
									<label for='uploadNPWP' class="col-sm-3 control-label"><bean:message key="common.uploadNPWP" /></label>
									<div class="col-sm-5">
									<html:file name="karyawanForm" property="uploadNPWP" styleId='uploadNPWP' size="20" styleClass="form-control"/>
										
									</div>
								</div>
								<div class="form-group">
									<label for='uploadKK' class="col-sm-3 control-label"><bean:message key="common.uploadKK" /></label>
									<div class="col-sm-5">
									<html:file name="karyawanForm" property="uploadKK" styleId='uploadKK' size="20" styleClass="form-control"/>
										
									</div>
								</div>
								<div class="form-group">
									<label for='uploadMI' class="col-sm-3 control-label"><bean:message key="common.uploadMI" /></label>
									<div class="col-sm-5">
									<html:file name="karyawanForm" property="uploadMI" styleId='uploadMI' size="20" styleClass="form-control"/>
										
									</div> --%>
								</div>
								
								<hr/>
								
								
								<% String action = "";%>
					      		<logic:equal value="Add" property="task" name="karyawanForm">
					      			<% action=Global.WEB_TASK_INSERT; %>
					      		</logic:equal>
					      		<logic:equal value="Edit" property="task" name="karyawanForm">
					      			<% action=Global.WEB_TASK_UPDATE; %>
					      		</logic:equal>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-5">
										<button type="button" onclick="javascript:movePage('<%=action%>','');" title="<bean:message key="tooltip.save"/>" class="btn btn-red"><bean:message key="tooltip.save"/></button>
										<button type="button" onclick="javascript:movePage('Load');" title="<bean:message key="tooltip.cancel"/>" class="btn btn-default"><bean:message key="tooltip.cancel"/></button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</html:form>
			</div>
		</div>
	</div>
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../../assets/js/bootstrap.js"></script>
	<script src="../../../assets/js/joinable.js"></script>
	<script src="../../../assets/js/resizeable.js"></script>
	<script src="../../../assets/js/neon-api.js"></script>
	<script src="../../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../../assets/js/jquery.validate.min.js"></script>
	<script src="../../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../../../assets/js/jquery.multi-select.js"></script>
	<script src="../../../assets/js/neon-chat.js"></script>
	<script src="../../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../../assets/js/select2/select2.min.js"></script>
	<script src="../../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../../assets/js/typeahead.min.js"></script>
	<script src="../../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../../assets/js/neon-demo.js"></script>
</html:html>