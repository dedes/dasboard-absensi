<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
    <html:base/>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">
            function movePage(taskID, idProject) {
                document.forms[0].task.value = taskID;
                document.forms[0].idProject.value = idProject;
                document.forms[0].submit();
            }

            function taskDelete(idProject) {
                if (!confirm('<bean:message key="common.confirmdelete" />'))
                    return;
                document.forms[0].task.value = 'Delete';
                document.forms[0].idProject.value = idProject;
                document.forms[0].submit();
            }
            
            function openDownload(nama, option){
            	var id = document.getElementById("idK").value;
				var nama = document.getElementById("nama").value;
				
				 var uri = '<html:rewrite action="/servlet/karyawanDownload"/>'+'?idProject='+idProject+'&namaProject='+namaProject;
	    		    opener.open(uri, "_self", "scrollbars=no,width=300,height=100,resizable=no,top=250,left=550"); 
	    		    
		   }
            

        </script>
    </head>

    <body>
    <html:form action="/feature/master/Project.do" method="post">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="idProject"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <html:hidden property="currentUri"/>
        <%@include file="../../../Error.jsp" %>

        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:message key="feature.form.Project"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.id"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.idProject" styleClass="TextBox" size="30" maxlength="10"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.nama"/></td>
                    <td class="Edit-Right-Text">
                        <html:text property="search.namaProject" styleClass="TextBox" size="30" maxlength="30"/>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../../images/button/ButtonSearch.png" alt="Search" width="100" height="31"
                                 border="0" title="<bean:message key="tooltip.search"/>">
                        </a>
                        
                    </td>
                </tr>
            </table>
            <div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;">
                <table border="0" width="100%" cellpadding="3" cellspacing="1" class="tableview">
                    <tr align="center" class="headercol">
                        <td width="10%" rowspan="2" align="center"><bean:message key="common.id"/></td>
                        <td width="20%" rowspan="2" align="center"><bean:message key="common.nama"/></td>
                        <td width="20%" align="center" rowspan="2"><bean:message key="common.alamat"/></td>
                        <td width="10%" align="center" rowspan="2"><bean:message key="common.notlpn"/></td>
                        <td width="10%" align="center" rowspan="2"><bean:message key="common.biaya"/></td>
                        <td width="10%" align="center" rowspan="2"><bean:message key="common.latitude"/></td>
                        <td width="10%" align="center" rowspan="2"><bean:message key="common.longitude"/></td>
                        <td width="10%" colspan="3" align="center"><bean:message key="common.action"/></td>
                    </tr>
                    <tr align="center" class="headercol">
                        <td width="5%" align="center"><bean:message key="common.edit"/></td>
                        <td width="5%" align="center"><bean:message key="common.delete"/></td>
                    </tr>
                    <%
                        int i = 0;
                        String classColor;
                    %>
                    <logic:notEmpty name="listProject" scope="request">
                        <logic:iterate id="data" name="listProject" scope="request">
                        
                            <%
                                if ((i % 2) != 1)
                                    classColor = "evalcol";
                                else
                                    classColor = "oddcol";
                                i++;
                            %>
                            <tr class="<%=classColor%>" onmouseover="this.className='hovercol'"
                                onmouseout="this.className='<%=classColor%>'">
                                <td align="center"><bean:write name="data" property="idProject"/></td>
                                <td align="center"><bean:write name="data" property="namaProject"/></td>
                                <td align="center"><bean:write name="data" property="lokasi"/></td>
                                <td align="center"><bean:write name="data" property="noTlpn"/></td>
                                <td align="center"><bean:write name="data" property="biayaReimburse"/></td>
                                <td align="center"><bean:write name="data" property="gpsLatitude"/></td>
                                <td align="center"><bean:write name="data" property="gpsLongitude"/></td>
                                <td align="center">
                                      <a title="Edit"
                                       href="javascript:movePage('Edit','<bean:write name="data" property="idProject"/>');">
                                        <img src="../../../images/icon/IconEdit.gif" width="16" height="13" border="0"/>
                                    </a>
                                </td>
                                <td align="center">
                                <a title="Delete"
                                   href="javascript:taskDelete('<bean:write name="data" property="idProject"/>');">
                                    <img src="../../../images/icon/IconDelete.gif" width="16" height="16" border="0"/>
                                </a>
                           		</td>
                            </tr>

                        </logic:iterate>
                    </logic:notEmpty>
                    <logic:empty name="listProject" scope="request">
                        <tr class="tdgenap">
                            <td bgcolor="FFFFFF" colspan="8" align="center">
                                <font class="errMsg"><bean:message key="common.listnotfound"/></font>
                            </td>
                        </tr>
                    </logic:empty>
                </table>
            </div>
            <logic:notEmpty name="listProject" scope="request">
                <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.projectForm,'First')" title="First">
                                <img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.projectForm,'Prev')" title="Previous">
                                <img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="200" align="center"><bean:message key="paging.page"/>
                            <html:text name="projectForm" property="paging.currentPageNo" size="3" maxlength="6"
                                       styleClass="TextBox"/> <bean:message key="paging.of"/> <html:hidden
                                    property="paging.totalPage" write="true"/>
                            <html:hidden property="paging.totalRecord" write="false"/>
                            <a href="javascript:moveToPageNumber(document.projectForm,'Go');">
                                <img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                     border="0"/></a></td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.projectForm,'Next')" title="Next">
                                <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td width="30" align="center">
                            <html:link href="javascript:moveToPageNumber(document.projectForm,'Last')" title="Last">
                                <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
                            </html:link>
                        </td>
                        <td align="right">
                            <bean:define id="currentPage" name="projectForm" property="paging.currentPageNo"
                                         type="java.lang.Integer"/>
                            <bean:define id="total" name="projectForm" property="paging.totalRecord"
                                         type="java.lang.Integer"/>
                            <bean:define id="perpage" name="projectForm" property="paging.rowPerPage"
                                         type="java.lang.Integer"/>
                            <%
                                int startShow = 1;
                                int endShow = 0;
                                if (currentPage > 1) {
                                    int tempstart = ((currentPage - 1) * perpage) + 1;
                                    int tempend = currentPage * perpage;
                                    if (total < tempend) {
                                        tempend = total;
                                    }
                                    startShow = tempstart;
                                    endShow = tempend;
                                } else {
                                    endShow = total;
                                }
                            %>
                            <bean:message key="paging.showing.row"/> <%= startShow %> <bean:message
                                key="paging.showing.to"/> <%=endShow%> <bean:message key="paging.showing.total"/>
                            <html:hidden property="paging.totalRecord" write="true"/> <bean:message
                                key="paging.showing.record"/>
                        </td>
                    </tr>
                </table>
            </logic:notEmpty>
           <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Add','')"><img src="../../../images/button/ButtonAdd.png"                                                                    alt="Add" width="100" height="31" border="0"></a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>