<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%-- <%@page import="java.sql.*" %>  --%>
<%-- <%!Connection con;%>  --%>
<%-- <%!Statement stmt = null;%>  --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "DETAIL APPROVAL"; %>
<html:html>
<html:base/>
<head>
	<title><%=title %></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link href="../../../style/style.css" rel="stylesheet" type="text/css">
		<script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
	<script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	<html:javascript formName="parameterForm" method="validateForm"  staticJavascript="false" page="0"/>
      <script language="javascript">
      
      function actionDelete(form, task, ApprovalSchemeID) {
          if (!confirm('<bean:message key="common.confirmdelete" />'))
              return;
          document.forms[0].task.value = task;
          document.forms[0].approvalSchemeID.value = ApprovalSchemeID;
          document.forms[0].submit();
      } 
      
      
      
	   function movePage(task){
	   	document.forms[0].task.value = task;
	   	document.forms[0].submit();
	}
	
	   function action(form, task, ApprovalSchemeID) {
           if (task == 'Reset'
               || task == 'ReleaseLock'
               || task == 'Enable'
               || task == 'Disable'
               || task == 'Delete') {
               if (!confirm('<bean:message key="common.confirm.action"/>'))
                   return;
               alert('<bean:message key="common.success"/>');
           }
           form.ApprovalSchemeID.value = ApprovalSchemeID;
           form.task.value = task;
           form.submit();
       }

	function openDownload(nama, option){
	    var ApprovalSchemeID = document.getElementById("ApprovalSchemeID").value;
	    var TransactionNoLabel = document.getElementById("TransactionNoLabel").value;
		var TransactionNo = document.getElementById("TransactionNo").value;
		var OptionalLabel1 = document.getElementById("OptionalLabel1").value;
		
		var uri = '<html:rewrite action="/servlet/servletdetail"/>?&ApprovalSchemeID='+ApprovalSchemeID+'&TransactionNoLabel='+TransactionNoLabel+'&TransactionNo='+TransactionNo+'&OptionalLabel1='+OptionalLabel1;
		opener.open(uri, "_self", "scrollbars=no,width=300,height=100,resizable=no,top=250,left=550");
    
	}
	
	
	/* function action(form, task, ApprovalSchemeID){
		if(task == 'Delete'){
			if (!confirm('<bean:message key="common.confirm.action"/>'))
				return;
			alert('<bean:message key="common.success"/>');
		}
		form.ApprovalSchemeID.value = ApprovalSchemeID;
		form.task.value = task;
		form.submit();
	} */

	function popUpClosed() {
	    window.location.reload();
	}

</script>

</head>
<body bgcolor="#FFFFFF" oncontextmenu="return false;">
	<html:form action="/feature/monitoring/Detail.do" method="POST">
		<html:hidden property="task" value="Load"/>
		<html:hidden property="approvalSchemeID"/>
		<html:hidden property="paging.dispatch" value="Go"/>
		<bean:define id="indexLastRow" name="detailForm" property="paging.firstRecord" type="java.lang.Integer"/>
		<bean:define id="indexFirstRow" name="detailForm" property="paging.firstRecord" type="java.lang.Integer"/>
		<div align="center">
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
		    	<tr class="Top-Table-Header">
		      		<td class="Top-Table-Header-Left"></td>
		      		<td class="Top-Table-Header-Center"><%=title %></td>
		      		<td class="Top-Table-Header-Right"></td>
		    	</tr>
		  	</table>
		  	<table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
			   <tr>
			      <td class="Edit-Left-Text"><bean:message key="approvalscheme.id"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="ApprovalSchemeID" property="search.approvalSchemeID" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   <tr>
			      <td class="Edit-Left-Text"><bean:message key="transaction.nolabel"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="TransactionNoLabel" property="search.transactionNoLabel" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   
			   
			      <tr>
			      <td class="Edit-Left-Text"><bean:message key="transaction.no"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="TransactionNo" property="search.transactionNo" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   
			      <tr>
			      <td class="Edit-Left-Text"><bean:message key="optional.label1"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="OptionalLabel1" property="search.optionalLabel1" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   
			   
			       <tr>
			      <td class="Edit-Left-Text"><bean:message key="optional.sql1"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="OptionalSQL1" property="search.optionalSQL1" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			
			       <tr>
			      <td class="Edit-Left-Text"><bean:message key="optional.label2"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="optionalLabel2" property="search.optionalLabel2" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   
			      <tr>
			      <td class="Edit-Left-Text"><bean:message key="optional.sql2"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="optionalSQL2" property="search.optionalSQL2" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   

	  		</table>
	  		<table width="95%" border="0" cellspacing="0" cellpadding="0">
				 <tr>
			      <td width="50%" height="30">&nbsp;</td>
			      <td width="50%" align="right">	
				        <a href="javascript:movePage('Search');">
				         <img src="../../../images/button/ButtonSearch.png" alt="Search" title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
				        </a>
				      <logic:notEmpty name="list" scope="request">
					    	<a href="javascript:openDownload('Download','scrollbars=no,width=300,height=100,resizable=no,top=250,left=550');">
					     		<img src="../../../images/button/ButtonDownload.png" alt="Download" title="<bean:message key="tooltip.generate"/>" width="100" height="31" border="0">
					 		</a>
					    </logic:notEmpty>
			      </td>
			    </tr>
			  	
			</table>
			<div align="left" STYLE="width: 95%; font-size: 12px; overflow: auto;" >
				<table border="0" width="100%" cellpadding="2" cellspacing="1" class="tableview">
				    <tr align="center" class="headercol">
				    	<td width="2%" rowspan="2" align="center"><bean:message key="common.number"/></td>
				      	<td width="10%" rowspan="2" align="center"><bean:message key="approvalscheme.id"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="transaction.nolabel"/></td>
                        <td width="5%" rowspan="2" align="center"><bean:message key="transaction.no"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="optional.label1"/></td>
                        <td width="55%" rowspan="2" align="center"><bean:message key="optional.sql1"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="optional.label2"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="optional.sql2"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="note.label"/></td>
                        <td width="90%" rowspan="2" align="center"><bean:message key="notesql.cmd"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="dtmupd"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="usrUpd"/></td>
                        
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label3"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL3"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label4"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL4"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label5"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL5"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label6"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL6"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label7"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL7"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label8"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL8"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label9"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL9"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label10"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL10"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label11"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL11"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label12"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL12"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label13"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL13"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label14"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL14"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.Label15"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="Optional.SQL15"/></td>
          				<%-- <td width="15%" colspan="3" align="center"><bean:message key="common.action"/></td>
                        
                    </tr>
                    <tr align="center" class="headercol"> 
                        <td width="5%" align="center"><bean:message key="common.edit"/></td>
                        <td width="5%" align="center"><bean:message key="common.delete"/></td> --%>
                    </tr>
				   	    <tr align="center" class="headercol">
					<logic:notEmpty name="list" scope="request" >
						<%
							int x = indexLastRow;
						%>
						<logic:iterate id="listParam" name="list" scope="request" indexId="index">
							<%
									String color = (index.intValue() %2 == 0) ? "evalcol" : "oddcol";
									indexLastRow++;
							%>
						   	<tr class="<%=color%>" onmouseover="this.className='hovercol'" onmouseout="this.className='<%=color%>'">
						   		<td align="right"><%=x++%></td>
						   	
							    <td align="center"><bean:write name="listParam" property="approvalSchemeID"/></td>
		      					<td align="center"><bean:write name="listParam" property="transactionNoLabel"/></td>
		      					<td align="center"><bean:write name="listParam" property="transactionNo"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel1"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL1"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel2"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL2"/></td>
		      					<td align="center"><bean:write name="listParam" property="noteLabel"/></td>
		      					<td align="center"><bean:write name="listParam" property="noteSQLCmd"/></td>
		      					<td align="center"><bean:write name="listParam" property="dtmUpd"/></td>
		      					<td align="center"><bean:write name="listParam" property="usrUpd"/></td>
		      					
		      					
		      					<td align="center"><bean:write name="listParam" property="optionalLabel3"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL3"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel4"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL4"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel5"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL5"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel6"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL6"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel7"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL7"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel8"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL8"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel9"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL9"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel10"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL10"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel1"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL11"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel2"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL12"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel13"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL13"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel14"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL14"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalLabel15"/></td>
		      					<td align="center"><bean:write name="listParam" property="optionalSQL15"/></td>
		      				
		      				
		      				
								<%--  <td align="center">                   
	                               		<a href="javascript:action(document.detailForm, 'Edit', '<bean:write name="listParam" property="approvalSchemeID"/>')">
                                                <img src="../../../images/icon/IconEdit.gif" alt="Edit" width="16"
                                                     height="16" title="<bean:message key="tooltip.edit"/>"/>
                                            </a>
                                </td>
                                <td align="center">
                                   <a href="javascript:actionDelete(document.detailForm, 'Delete', '<bean:write name="listParam" property="approvalSchemeID"/>')">
                                                <img src="../../../images/icon/IconDelete.gif" alt="Delete" width="16"
                                                     height="16" title="<bean:message key="tooltip.delete"/>"/>
                                            </a>
                             	</td>			   	 --%>
		      				</tr>
		   				</logic:iterate>
		  			</logic:notEmpty>
		   			<logic:empty name="list" scope="request">
					   <tr class="evalcol">
					      <td bgcolor="FFFFFF" colspan="20" align="center"><font class="errMsg"><bean:message key="common.listnotfound"/></font></td>
					   </tr>
		   			</logic:empty>
		   		</table>
	   		</div>
		   	<logic:notEmpty name="list" scope="request">
	   		<table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
			    	<td width="30" align="center" title="<bean:message key="tooltip.first"/>">
			        	<html:link href="javascript:moveToPageNumber(document.detailForm,'First')">
			        		<img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
			        	</html:link>
			      	</td>
			      	<td width="30" align="center" title="<bean:message key="tooltip.previous"/>">
			        	<html:link href="javascript:moveToPageNumber(document.detailForm,'Prev')">
			        		<img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
			        	</html:link>
			      	</td>
			      	<td width="200" align="center"><bean:message key="paging.page"/>
			        	<html:text name="detailForm" property="paging.currentPageNo" size="3" maxlength="6" styleClass="inpType"/> <bean:message key="paging.of"/> <html:hidden property="paging.totalPage" write="true" />
			        	<a href="javascript:moveToPageNumber(document.detailForm,'Go');">
				        	<img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle" border="0"/>
				        </a>
				  	</td>
					<td width="30" align="center" title="<bean:message key="tooltip.next"/>">
						<html:link href="javascript:moveToPageNumber(document.detailForm,'Next')">
					        <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
					    </html:link>
					</td>
					<td width="30" align="center" title="<bean:message key="tooltip.last"/>">
						<html:link href="javascript:moveToPageNumber(document.detailForm,'Last')">
					        <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
					    </html:link>
					</td>
					<td align="right">
						<bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message key="paging.showing.to"/> <%=indexLastRow-1%> <bean:message key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/> <bean:message key="paging.showing.record"/>
			   		</td>
				</tr>
		  </table>
		  </logic:notEmpty>
	  	
	</div>
	</html:form>		  
</body>
</html:html>