<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = Global.TITLE_APP_DETAIL; %>
<html:html>
<html:base/>
<head>
	<title><%=title %></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link href="../../../style/style.css" rel="stylesheet" type="text/css">
		<script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
	<script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	<html:javascript formName="parameterForm" method="validateForm"  staticJavascript="false" page="0"/>
      <script language="javascript">
      
      
<script language="javascript">
	function movePage(task){
	   	document.forms[0].task.value = task;
	   	document.forms[0].submit();
	}

	function action(form, task, id){
		if(!nullOrEmpty(id))
			form.task.id = ApprovalSchemeID;
		form.task.value = task;
		form.submit();
	}
	
</script>

</head>
<body bgcolor="#FFFFFF" oncontextmenu="return false;">
	<html:form action="/feature/monitoring/Detail.do" method="POST">
		<html:hidden property="task"/>
<%-- 		<html:hidden property="code"/> --%>
		<div align="center">
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
		    	<tr class="Top-Table-Header">
		      		<td class="Top-Table-Header-Left"></td>
		      		<td class="Top-Table-Header-Center"><%=title %></td>
		      		<td class="Top-Table-Header-Right"></td>
		    	</tr>
		  	</table>
		  	<table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">

			<td class="Edit-Left-Text"><bean:message key="approvalscheme.id"/></td>
                    <td class="Edit-Right-Text">
                        <logic:equal value="Add" property="task" name="DetailForm">
                            <html:text property="approvalSchemeID" styleClass="TextBox" size="15" maxlength="15"/>
                            <font color="#FF0000">*) <html:errors property="approvalSchemeID"/></font>
                        </logic:equal>
                        <logic:equal value="Edit" property="task" name="DetailForm">
                            <html:hidden property="approvalSchemeID" write="true"/>
                        </logic:equal>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="transaction.nolabel"/></td>
                    <td class="Edit-Right-Text">             
                        <html:text property="transactionNoLabel" styleClass="TextBox" size="30" maxlength="150"/>
                        <font color="#FF0000">*) <html:errors property="transactionNoLabel"/></font>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="transaction.no"/></td>
                    <td class="Edit-Right-Text">  
                        <html:text property="TransactionNo" styleClass="TextBox" size="15" maxlength="150"/>
                        <font color="#FF0000">*) <html:errors property="transactionNo"/></font>
                    </td>
                </tr>
                 
               <tr>
                    <td class="Edit-Left-Text"><bean:message key="optional.label1"/></td>
                    <td class="Edit-Right-Text">  
                        <html:text property="OptionalLabel1" styleClass="TextBox" size="15" maxlength="150"/>
                        <font color="#FF0000">*) <html:errors property="optionalLabel1"/></font>
                    </td>
                </tr>
                
                  <tr>
                    <td class="Edit-Left-Text"><bean:message key="optional.sql2"/></td>
                    <td class="Edit-Right-Text">  
                        <html:text property="optionalSQL2" styleClass="TextBox" size="15" maxlength="150"/>
                        <font color="#FF0000">*) <html:errors property="optionalSQL2"/></font>
                    </td>
                </tr>

            <%--  <tr>
                    <td class="Edit-Left-Text"><bean:message key="app.user.active"/></td>
                    <td class="Edit-Right-Text">
                        <html:checkbox property="isActive" value="1"/>
                    </td>
                </tr> --%>   	
	  		</table>
	  		
	  		<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
			    	<td width="50%" height="30">&nbsp;</td>
			     	<td width="50%" align="right">
			     	<% String action = ""; %>
			      		<logic:equal value="Add" property="task" name="detailForm">
			      			<% action=Global.WEB_TASK_INSERT; %>
			      		</logic:equal>
			      		<logic:equal value="Edit" property="task" name="detailForm">
			      			<% action=Global.WEB_TASK_UPDATE; %>
			      		</logic:equal>
			        	<a href="javascript:action(document.detailForm,'<%=action%>','');">
			         		<img src="../../../../../images/button/ButtonSubmit.png" alt="Action <%=action %>" title="Action <%=action %>" width="100" height="31" border="0">
			        	</a>
			        	&nbsp;
			        	<a href="javascript:movePage('Load');">
			         		<img src="../../../../../images/button/ButtonCancel.png" alt="Cancel" title="Cancel" width="100" height="31" border="0">
			        	</a>
			     	</td>
			  	</tr>
			</table>  		
		</div>
	</html:form>	
		  
</body>
</html:html>