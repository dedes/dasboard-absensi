<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%-- <%@page import="java.sql.*" %>  --%>
<%-- <%!Connection con;%>  --%>
<%-- <%!Statement stmt = null;%>  --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "APPROVAL HISTORY"; %>
<html:html>
<html:base/>
<head>
	<title><%=title %></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link href="../../../style/style.css" rel="stylesheet" type="text/css">
		<script language="javascript" SRC="../../../javascript/global/jquery-1.10.2.js"></script>
	<script language="javascript" SRC="../../../javascript/global/jquery-all.js"></script>
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	<html:javascript formName="parameterForm" method="validateForm"  staticJavascript="false" page="0"/>
	
	
      <script language="javascript">
	   function movePage(task){
	   	document.forms[0].task.value = task;
	   	document.forms[0].submit();
	}
	
	


function openDownload(nama, option){
	 var ApprovalNo = document.getElementById("ApprovalNo").value;
	    var ApprovalID = document.getElementById("ApprovalID").value;
		var ApprovalSchemeID = document.getElementById("ApprovalSchemeID").value;
		var ApprovalStatus = document.getElementById("ApprovalStatus").value;
		var TransactionNo = document.getElementById("TransactionNo").value;
		var IsApprovedByMobile = document.getElementById("IsApprovedByMobile").value;
		var IsFinal = document.getElementById("IsFinal").value;
		
		var uri = '<html:rewrite action="/servlet/servlethistory"/>?&ApprovalNo='+ApprovalNo+'&ApprovalID='+ApprovalID+'&ApprovalSchemeID='+ApprovalSchemeID+'&ApprovalStatus='+ApprovalStatus+'&TransactionNo='+TransactionNo+'&IsApprovedByMobile='+IsApprovedByMobile+'&IsFinal='+IsFinal;
		opener.open(uri, "_self", "scrollbars=no,width=300,height=100,resizable=no,top=250,left=550");
 
}	
	
	function action(form, task, ApprovalID){
		if(task == 'Delete'){
			if (!confirm('<bean:message key="common.confirm.action"/>'))
				return;
			alert('<bean:message key="common.success"/>');
		}
		form.ApprovalID.value = ApprovalID;
		form.task.value = task;
		form.submit();
	}

	function popUpClosed() {
	    window.location.reload();
	}

</script>

</head>
<body bgcolor="#FFFFFF" oncontextmenu="return false;">
	<html:form action="/feature/monitoring/ApprovalHistory.do" method="POST">
		<html:hidden property="task" value="Load"/>
		<html:hidden property="paging.dispatch" value="Go"/>
		<bean:define id="indexLastRow" name="historyForm" property="paging.firstRecord" type="java.lang.Integer"/>
		<bean:define id="indexFirstRow" name="historyForm" property="paging.firstRecord" type="java.lang.Integer"/>
		<div align="center">
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
		    	<tr class="Top-Table-Header">
		      		<td class="Top-Table-Header-Left"></td>
		      		<td class="Top-Table-Header-Center"><%=title %></td>
		      		<td class="Top-Table-Header-Right"></td>
		    	</tr>
		  	</table>
		  	<table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
			   <tr>
			      <td class="Edit-Left-Text"><bean:message key="approval.no"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="ApprovalNo" property="search.approvalNo" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   <tr>
			      <td class="Edit-Left-Text"><bean:message key="approval.id"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="ApprovalID" property="search.approvalID" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   
			   
			      <tr>
			      <td class="Edit-Left-Text"><bean:message key="approvalScheme.id"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="ApprovalSchemeID" property="search.approvalSchemeID" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   
			      <tr>
			      <td class="Edit-Left-Text"><bean:message key="approval.status"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="ApprovalStatus" property="search.approvalStatus" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			
			
			       <tr>
			      <td class="Edit-Left-Text"><bean:message key="transaction.no"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="TransactionNo" property="search.transactionNo" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   
			      <tr>
			      <td class="Edit-Left-Text"><bean:message key="isapprovedby.mobile"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="IsApprovedByMobile" property="search.isApprovedByMobile" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>
			   
			   
			      <tr>
			      <td class="Edit-Left-Text"><bean:message key="is.final"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text styleId="IsFinal" property="search.isFinal" styleClass="TextBox"  size="20" maxlength="20" />
			      </td>
			   </tr>

	  		</table>
	  		<table width="95%" border="0" cellspacing="0" cellpadding="0">
				 <tr>
			      <td width="50%" height="30">&nbsp;</td>
			      <td width="50%" align="right">	
				        <a href="javascript:movePage('Search');">
				         <img src="../../../images/button/ButtonSearch.png" alt="Search" title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0">
				        </a>
				      <logic:notEmpty name="list" scope="request">
					    	<a href="javascript:openDownload('Download','scrollbars=no,width=300,height=100,resizable=no,top=250,left=550');">
					     		<img src="../../../images/button/ButtonDownload.png" alt="Download" title="<bean:message key="tooltip.generate"/>" width="100" height="31" border="0">
					 		</a>
					    </logic:notEmpty>
			      </td>
			    </tr>
			  	
			</table>
			<div align="left" STYLE="width: 95%; font-size: 12px; overflow: auto;" >
				<table border="0" width="100%" cellpadding="2" cellspacing="1" class="tableview">
				    <tr align="center" class="headercol">
				    	<td width="2%" rowspan="2" align="center"><bean:message key="common.number"/></td>
				      	<td width="10%" rowspan="2" align="center"><bean:message key="approval.no"/></td>
                        <td width="10%" rowspan="2" align="center"><bean:message key="approval.id"/></td>
                        <td width="5%" rowspan="2" align="center"><bean:message key="is.final"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="security.code"/></td>
                        <td width="55%" rowspan="2" align="center"><bean:message key="approvalScheme.id"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="request.date"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="isvia.sms"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="isvia.email"/></td>
                        <td width="70%" rowspan="2" align="center"><bean:message key="isvia.fax"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="areafax.no"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="faxno"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="user.request"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="approval.date"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="approval.status"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="approval.result"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="approval.note"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="limit.value"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="approval.value"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="is.forward"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="user.approval"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="usersecurity.code"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="approval.roleid"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="transaction.no"/></td>
                         <td width="45%" rowspan="2" align="center"><bean:message key="wwwww"/></td>
                        
                        <td width="45%" rowspan="2" align="center"><bean:message key="isever.rejected"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="client.ip"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="argumen.tasi"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="ApprovalValue.Pencentage"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="ApprovalValue.Pencentage"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="isapprovedby.mobile"/></td>
                         <td width="45%" rowspan="2" align="center"><bean:message key="approval.answer"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="approval.noteanswer"/></td>
                         <td width="45%" rowspan="2" align="center"><bean:message key="date.approval"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="userApproval.next"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="dtmupd"/></td>
                        <td width="45%" rowspan="2" align="center"><bean:message key="UsrUpd"/></td>
				    </tr>
				   	    <tr align="center" class="headercol">
					<logic:notEmpty name="list" scope="request" >
						<%
							int x = indexLastRow;
						%>
						<logic:iterate id="listParam" name="list" scope="request" indexId="index">
							<%
									String color = (index.intValue() %2 == 0) ? "evalcol" : "oddcol";
									indexLastRow++;
							%>
						   	<tr class="<%=color%>" onmouseover="this.className='hovercol'" onmouseout="this.className='<%=color%>'">
						   		<td align="right"><%=x++%></td>
						   	
							    <td align="center"><bean:write name="listParam" property="approvalNo"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalID"/></td>
		      					<td align="center"><bean:write name="listParam" property="isFinal"/></td>
		      					<td align="center"><bean:write name="listParam" property="securityCode"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalSchemeID"/></td>
		      					<td align="center"><bean:write name="listParam" property="requestDate"/></td>
		      					<td align="center"><bean:write name="listParam" property="isViaSMS"/></td>
		      					<td align="center"><bean:write name="listParam" property="isViaEmail"/></td>
		      					<td align="center"><bean:write name="listParam" property="isViaFax"/></td>
		      					<td align="center"><bean:write name="listParam" property="areaFaxNo"/></td>
		      					<td align="center"><bean:write name="listParam" property="faxNo"/></td>
		      				    <td align="center"><bean:write name="listParam" property="userRequest"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalDate"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalStatus"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalResult"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalNote"/></td>
		      					<td align="center"><bean:write name="listParam" property="limitValue"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalValue"/></td>
		      					<td align="center"><bean:write name="listParam" property="isForward"/></td>
		      					<td align="center"><bean:write name="listParam" property="userApproval"/></td>
		      					<td align="center"><bean:write name="listParam" property="userSecurityCode"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalRoleID"/></td>
		      				    <td align="center"><bean:write name="listParam" property="transactionNo"/></td>
		      					<td align="center"><bean:write name="listParam" property="WOA"/></td>
		      					<td align="center"><bean:write name="listParam" property="isEverRejected"/></td>
		      					<td align="center"><bean:write name="listParam" property="clientIP"/></td>
		      					<td align="center"><bean:write name="listParam" property="argumentasi"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalValuePencentage"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalValuePencentage"/></td>
		      					<td align="center"><bean:write name="listParam" property="isApprovedByMobile"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalanswer"/></td>
		      					<td align="center"><bean:write name="listParam" property="approvalnoteanswer"/></td>
		      				    <td align="center"><bean:write name="listParam" property="dateApproval"/></td>
		      					<td align="center"><bean:write name="listParam" property="userApprovalnext"/></td>
		      					<td align="center"><bean:write name="listParam" property="dtmupd"/></td>
		      					<td align="center"><bean:write name="listParam" property="usrUpd"/></td>
		      				
											   	
		      				</tr>
		   				</logic:iterate>
		  			</logic:notEmpty>
		   			<logic:empty name="list" scope="request">
					   <tr class="evalcol">
					      <td bgcolor="FFFFFF" colspan="40" align="center"><font class="errMsg"><bean:message key="common.listnotfound"/></font></td>
					   </tr>
		   			</logic:empty>
		   		</table>
	   		</div>
		   	<logic:notEmpty name="list" scope="request">
	   		<table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
			    	<td width="30" align="center" title="<bean:message key="tooltip.first"/>">
			        	<html:link href="javascript:moveToPageNumber(document.historyForm,'First')">
			        		<img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
			        	</html:link>
			      	</td>
			      	<td width="30" align="center" title="<bean:message key="tooltip.previous"/>">
			        	<html:link href="javascript:moveToPageNumber(document.historyForm,'Prev')">
			        		<img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
			        	</html:link>
			      	</td>
			      	<td width="200" align="center"><bean:message key="paging.page"/>
			        	<html:text name="historyForm" property="paging.currentPageNo" size="3" maxlength="6" styleClass="inpType"/> <bean:message key="paging.of"/> <html:hidden property="paging.totalPage" write="true" />
			        	<a href="javascript:moveToPageNumber(document.historyForm,'Go');">
				        	<img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle" border="0"/>
				        </a>
				  	</td>
					<td width="30" align="center" title="<bean:message key="tooltip.next"/>">
						<html:link href="javascript:moveToPageNumber(document.historyForm,'Next')">
					        <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
					    </html:link>
					</td>
					<td width="30" align="center" title="<bean:message key="tooltip.last"/>">
						<html:link href="javascript:moveToPageNumber(document.historyForm,'Last')">
					        <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
					    </html:link>
					</td>
					<td align="right">
						<bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message key="paging.showing.to"/> <%=indexLastRow-1%> <bean:message key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/> <bean:message key="paging.showing.record"/>
			   		</td>
				</tr>
		  </table>
		  </logic:notEmpty>
	  	
	</div>
	</html:form>		  
</body>
</html:html>