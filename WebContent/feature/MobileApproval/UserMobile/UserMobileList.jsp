<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "USER MOBILE APPROVAL"; %>
<html:html>
<html:base/>
<head>
	<title><%=title %></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link href="../../../style/style.css" rel="stylesheet" type="text/css">
	<script language="javascript" SRC="../../../javascript/global/global.js"></script>
	<script language="javascript" SRC="../../../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
	
<script language="javascript">
	function movePage(task){
	   	document.forms[0].task.value = task;
	   	document.forms[0].submit();
	}

	function action(form, task, id){
		if(	   task == 'Reset' 
			|| task == 'ReleaseLock'
			|| task == 'Enable'
			|| task == 'Disable'
			|| task == 'Delete'){
			if (!confirm('<bean:message key="common.confirm.action"/>'))
				return;
			alert('<bean:message key="common.success"/>');
		}
		form.loginid.value = id;
		form.task.value = task;
		form.submit();
	}
	
	
</script>
</head>
<body bgcolor="#FFFFFF" oncontextmenu="return false;">
	<html:form action="/feature/monitoring/UserMobile.do" method="POST">
		<html:hidden property="task" value="Load"/>
		<html:hidden property="loginid"/>
		<html:hidden property="paging.dispatch" value="Go"/>
		<bean:define id="indexLastRow" name="userMobileForm" property="paging.firstRecord" type="java.lang.Integer"/>
		<bean:define id="indexFirstRow" name="userMobileForm" property="paging.firstRecord" type="java.lang.Integer"/>
		<div align="center">
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
		    	<tr class="Top-Table-Header">
		      		<td class="Top-Table-Header-Left"></td>
		      		<td class="Top-Table-Header-Center"><bean:write name="userMobileForm" property="title"/></td>
		      		<td class="Top-Table-Header-Right"></td>
		    	</tr>
		  	</table>
		  	<table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
			   <tr>
			      <td class="Edit-Left-Text"><bean:message key="app.user.id"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text property="search.loginid" styleClass="TextBox"  size="15" maxlength="15" />
			      </td>
			   </tr>
			   <tr>
			      <td class="Edit-Left-Text"><bean:message key="app.user.name"/></td>
			      <td class="Edit-Right-Text">
			      	<html:text property="search.fullname" styleClass="TextBox"  size="30" maxlength="30" />
			      </td>
			   </tr>
			      <td class="Edit-Left-Text"><bean:message key="common.login.header"/></td>
			      <td class="Edit-Right-Text">
						<html:select property="search.islogin" styleClass="TextBox">
			      			<html:option value=""><bean:message key="common.all"/></html:option>
				            <html:option value="1"><bean:message key="common.login"/></html:option>
				            <html:option value="0"><bean:message key="common.notlogin"/></html:option>
			      		</html:select>
			      </td>
			   </tr>
			   <tr>
			      <td class="Edit-Left-Text"><bean:message key="common.lock.header"/></td>
			      <td class="Edit-Right-Text">
						<html:select property="search.islocked" styleClass="TextBox">
			      			<html:option value=""><bean:message key="common.all"/></html:option>
				            <html:option value="1"><bean:message key="common.lock"/></html:option>
				            <html:option value="0"><bean:message key="combo.label.unlock"/></html:option>
			      		</html:select>
			      </td>
			   </tr>
	  		</table>
	  		<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
			    	<td width="50%" height="30">&nbsp;</td>
			     	<td width="50%" align="right">
			        	<a href="javascript:movePage('Search');">
			         		<img src="../../../images/button/ButtonSearch.png" alt="Search" title="<bean:message key="tooltip.search"/>" width="100" height="31" border="0"/>
			        	</a>
			     	</td>
			  	</tr>
			</table>
			<div align="center" STYLE="width: 95%; font-size: 12px; overflow: auto;" >
				<table border="0" width="100%" cellpadding="3" cellspacing="1" class="tableview">
				    <tr align="center" class="headercol">
				      	<td width="1%" rowspan="2" align="center"><bean:message key="app.user.id"/></td>
				      	<td width="2%" rowspan="2" align="center"><bean:message key="app.user.name"/></td>
				      	<td width="5%" colspan="4" align="center"><bean:message key="common.status"/></td>
				      	<td width="5%" colspan="2" align="center"><bean:message key="common.action"/></td>
				    </tr>
				    <tr align="center" class="headercol">		      	
				      	<td width="1%" align="center"><bean:message key="app.user.last.login"/></td>	
				      	<td width="1%" align="center"><bean:message key="common.login.header"/></td>	
				      	<td width="1%" align="center"><bean:message key="common.lock.header"/></td>
				      	<td width="1%" align="center"><bean:message key="common.active.header"/></td>
				      
				      	<td width="1%" align="center"><bean:message key="tooltip.unlock"/></td>
				      	<td width="1%" align="center"><bean:message key="tooltip.release"/></td>
				      	

				    </tr>
					<logic:notEmpty name="list" scope="request" >
						<logic:iterate id="listParam" name="list" scope="request" indexId="index">
			
							<%
							        String color = (index.intValue() %2 == 0) ? "evalcol" : "oddcol";
									indexLastRow++;
							%>
						   	<tr class="<%=color%>" onmouseover="this.className='hovercol'" onmouseout="this.className='<%=color%>'">
							    <td align="center"><bean:write name="listParam" property="loginid"/></td>
							    <td align="left"><bean:write name="listParam" property="fullname"/></td>
							    <td align="center"><bean:write name="listParam" property="lastlogin"/></td>
							    <td align="center" valign="middle" >
							    	<logic:equal value="1" name="listParam" property="islogin">
							    		<img src="../../../images/icon/IconYes.gif" alt="Login" width="16" height="16" title="<bean:message key="tooltip.login"/>"/>
							    	</logic:equal>
							    	<logic:notEqual value="1" name="listParam" property="islogin" >
							    		-
							    	</logic:notEqual>
							    	</td>
							    <td align="center" valign="middle">
							    	<logic:equal value="1" name="listParam" property="islocked">
							    		<img src="../../../images/icon/IconLocked.gif" alt="Locked" width="16" height="16" title="<bean:message key="tooltip.locked"/>"/>
							    	</logic:equal>
							    	<logic:notEqual value="1" name="listParam" property="islocked">
							    		-
<!-- 							    		<img src="../../images/icon/IconUnlocked.gif" alt="Unlocked" width="16" height="16"/> -->
							    	</logic:notEqual>
							    </td>
							    <td align="center" valign="middle">	  
							    		<logic:equal value="1" name="listParam" property="active">
							    		<img src="../../../images/icon/IconActive.gif" alt="Active" width="16" height="16" title="<bean:message key="tooltip.active"/>"/>
							    	</logic:equal>
							    	<logic:notEqual value="1" name="listParam" property="active">
							    		<img src="../../../images/icon/IconInactive.gif" alt="Inactive" width="16" height="16" title="<bean:message key="tooltip.inactive"/>"/>
							    	</logic:notEqual>
							    </td>
							    <td align="center" valign="middle">
								    	<logic:equal value="1" name="listParam" property="islocked" >
									    	<a href="javascript:action(document.userMobileForm, 'ReleaseLock', '<bean:write name="listParam" property="loginid"/>')">
									    		<img src="../../../images/icon/IconUnlocked.gif" alt="Release" width="16" height="16"/>
									    	</a>
								    	</logic:equal>
								    	<logic:notEqual value="1" name="listParam" property="islocked">
								    		-
								    	</logic:notEqual>
							    </td>
							    <td align="center" valign="middle">
								    	<logic:equal value="1" name="listParam" property="islogin">
									    	<a href="javascript:action(document.userMobileForm, 'Release', '<bean:write name="listParam" property="loginid"/>')">
									    		<img src="../../../images/icon/IconRelease.gif" alt="Release" width="16" height="16" title="<bean:message key="tooltip.release"/>"/>
									    	</a>
								    	</logic:equal>
								    	<logic:notEqual value="1" name="listParam" property="islogin">
								    		-
								    	</logic:notEqual>
							    </td>
		   				</logic:iterate>
		  			</logic:notEmpty>
		   			<logic:empty name="list" scope="request">
					   <tr class="evalcol">
					      <td bgcolor="FFFFFF" colspan="16" align="center"><font class="errMsg"><bean:message key="common.listnotfound"/></font></td>
					   </tr>
		   			</logic:empty>
		   		</table>
	   		</div>
		   	<logic:notEmpty name="list" scope="request">
		   		<table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
				    	<td width="30" align="center" title="<bean:message key="tooltip.first"/>">
				        	<html:link href="javascript:moveToPageNumber(document.userMobileForm,'First')">
				        		<img src="../../../images/gif/first.gif" width="20" height="20" border="0"/>
				        	</html:link>
				      	</td>
				      	<td width="30" align="center" title="<bean:message key="tooltip.previous"/>">
				        	<html:link href="javascript:moveToPageNumber(document.userMobileForm,'Prev')">
				        		<img src="../../../images/gif/prev.gif" width="20" height="20" border="0"/>
				        	</html:link>
				      	</td>
				      	<td width="200" align="center"><bean:message key="paging.page"/>
				        	<html:text name="userMobileForm" property="paging.currentPageNo" size="3" maxlength="6" styleClass="inpType"/> <bean:message key="paging.of"/> <html:hidden property="paging.totalPage" write="true" />
				        	<a href="javascript:moveToPageNumber(document.userMobileForm,'Go');">
					        	<img src="../../../images/gif/go.gif" width="20" height="20" align="absmiddle" border="0"/>
					        </a>
					  	</td>
						<td width="30" align="center" title="<bean:message key="tooltip.next"/>">
							<html:link href="javascript:moveToPageNumber(document.userMobileForm,'Next')">
						        <img src="../../../images/gif/next.gif" width="20" height="20" border="0"/>
						    </html:link>
						</td>
						<td width="30" align="center" title="<bean:message key="tooltip.last"/>">
							<html:link href="javascript:moveToPageNumber(document.userMobileForm,'Last')">
						        <img src="../../../images/gif/last.gif" width="20" height="20" border="0"/>
						    </html:link>
						</td>
						<td align="right">
							<bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message key="paging.showing.to"/> <%=indexLastRow-1%> <bean:message key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/> <bean:message key="paging.showing.record"/>
				   		</td>
					</tr>
			  </table>
		  </logic:notEmpty>
	  <%-- 	  	<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
			    	<td width="50%" height="30">&nbsp;</td>
			     	<td width="50%" align="right">
			        	<a href="javascript:movePage('Add');">
			         	<img src="../../images/button/ButtonAdd.png" alt="Add" title="<bean:message key="tooltip.add"/>" width="100" height="31" border="0">
			        	</a>
			     	</td>
			  	</tr>
			</table> --%>
	</div>
	</html:form>		  
</body>
</html:html>