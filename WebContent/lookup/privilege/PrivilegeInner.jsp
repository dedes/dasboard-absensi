<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
    <tr align="center" class="headercol">
        <td width="100%" align="center"><bean:message key="common.menu"/></td>
    </tr>
    <tr class="evalcol">
        <td align="left" class="evalcol">
            <logic:notEmpty property="listPrivilege" name="listParam">
                <% int counter = 0; %>
                <bean:size id="sizePrivilege" property="listPrivilege" name="listParam"/>
                <logic:iterate id="listParamDetail" property="listPrivilege" name="listParam" indexId="index">
                    <%
                        if (0 == counter) {
                    %>
                    <div id="folder<%=counter+"" %>" style="display:block; position:block;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td valign="top">
                                    <img id="folderIcon<%=counter+"" %>" name="folderIcon<%=counter+"" %>"
                                         src="../../images/menu/open_folder.png" border="0">
                                </td>
                                <td class="menu" nowrap="" valign="middle" width="100%">
                                    <strong><bean:message key="menu.root"/></strong>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <% counter = counter + 1; %>
                    <div id="folder<%=counter+"" %>" style="display:block; position:block;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td valign="top">
                                    <img id="folderIcon<%=counter+"" %>" name="folderIcon<%=counter+"" %>"
                                         src="../../images/menu/ftv2lastnode.gif" border="0">
                                </td>
                                <td valign="top">
                                    <img id="itemIcon<%=counter+"" %>" src="../../images/menu/open_folder.png"
                                         border="0">
                                </td>
                                <td class="menu" nowrap="" valign="middle" width="100%">
                                    <strong><bean:write name="listParamDetail" property="groupname"/></strong>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <%
                        }
                    %>
                    <logic:equal value="<%=treemas.util.constant.Global.MENU_RD %>" name="listParamDetail"
                                 property="result">
                        <div id="folder<%=counter+"" %>" style="display:block; position:block;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td valign="top">
                                        <% if (counter - 1 < sizePrivilege - 1) { %>
                                        <img id="folderIcon<%=counter+"" %>" name="folderIcon<%=counter+"" %>"
                                             src="../../images/menu/ftv2pnode.gif" border="0">
                                        <% } else { %>
                                        <img id="folderIcon<%=counter+"" %>" name="folderIcon<%=counter+"" %>"
                                             src="../../images/menu/ftv2lastnode.gif" border="0">
                                        <% } %>
                                        <img id="folderIcon<%=counter+"" %>" name="folderIcon<%=counter+"" %>"
                                             src="../../images/menu/open_folder.png" border="0">
                                    </td>
                                    <td class="menu" nowrap="" valign="middle" width="100%">
                                        <strong><bean:write name="listParamDetail" property="prompt"/></strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </logic:equal>
                    <logic:equal value="<%=treemas.util.constant.Global.MENU_FD %>" name="listParamDetail"
                                 property="result">
                        <div id="item<%=counter+"" %>" style="display: block;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td valign="top">
                                        <img src="../../images/menu/ftv2blank.gif" height="22" width="16">
                                    </td>
                                    <%
                                        if (counter - 1 == sizePrivilege - 1) {
                                    %>
                                    <td valign="top">
                                        <img src="../../images/menu/ftv2lastnode.gif" height="22" width="16">
                                    </td>
                                    <%
                                    } else {
                                    %>
                                    <td background="../../images/menu/ftv2vertline.gif" valign="top">
                                        <img src="../../images/menu/ftv2node.gif" height="22" width="16">
                                    </td>
                                    <%
                                        }
                                    %>
                                    <td valign="top">
                                        <img id="itemIcon<%=counter+"" %>" src="../../images/menu/menufile.png"
                                             border="0">
                                    </td>
                                    <td class="submenu" nowrap="" valign="middle" width="100%">
                                        <bean:write name="listParamDetail" property="prompt"/>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </logic:equal>
                    <%
                        counter = counter + 1;
                    %>
                </logic:iterate>
            </logic:notEmpty>
            <logic:empty property="listPrivilege" name="listParam">
                <font class="errMsg" style="text-align: center;"><bean:message key="common.listnotfound"/></font>
            </logic:empty>
        </td>
    </tr>
</table>