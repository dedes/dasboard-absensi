<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "DETAIL APPROVAL PRIVILEGE"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <script language="javascript">

        </script>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/lookup/approval/Privilege.do" method="POST">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="groupid"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="luApprovalPrivilegeForm"
                                                                    property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="lookup.privilege.appid"/></td>
                    <td class="Edit-Right-Text"><bean:write name="luApprovalPrivilegeForm" property="appid"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="lookup.privilege.appname"/></td>
                    <td class="Edit-Right-Text"><bean:write name="luApprovalPrivilegeForm" property="appname"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="lookup.privilege.groupid"/></td>
                    <td class="Edit-Right-Text"><bean:write name="luApprovalPrivilegeForm" property="groupid"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.user"/></td>
                    <td class="Edit-Right-Text"><bean:write name="luApprovalPrivilegeForm" property="usrupd"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="general.change.date"/></td>
                    <td class="Edit-Right-Text"><bean:write name="luApprovalPrivilegeForm" property="dtmupd"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text"><bean:message key="common.action"/></td>
                    <td class="Edit-Right-Text"><bean:write name="luApprovalPrivilegeForm" property="actChange"/></td>
                </tr>
            </table>
            <br/>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td colspan="4" align="center" width="100%"><bean:message key="common.detail"/></td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="5%" align="center"><bean:message key="common.number"/></td>
                    <td width="30%" align="center"><bean:message key="common.field"/></td>
                    <td width="30%" align="center"><bean:message key="common.before"/></td>
                    <td width="30%" align="center"><bean:message key="common.after"/></td>
                </tr>
                <% String color = ""; %>
                <bean:define id="privname" name="luApprovalPrivilegeForm" property="groupname" type="java.lang.String"/>
                <logic:equal value="<%=privname%>" name="luApprovalPrivilegeForm"
                             property="groupnameold"><% color = "style='color:#000;'"; %></logic:equal>
                <logic:notEqual value="<%=privname%>" name="luApprovalPrivilegeForm"
                                property="groupnameold"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                </logic:notEqual>
                <tr align="center" class="evalcol">
                    <td align="right">1.</td>
                    <td align="left" <%out.print(color); %>><bean:message key="lookup.privilege.groupname"/></td>
                    <td align="left"><bean:write name="luApprovalPrivilegeForm" property="groupnameold"/></td>
                    <td align="left"><bean:write name="luApprovalPrivilegeForm" property="groupname"/></td>
                </tr>
                <%
                    String paramdesc = "";
                    String paramnew = "";
                %>
                <logic:present name="luApprovalPrivilegeForm" property="menuold">
                    <bean:define id="paramdesctemp" name="luApprovalPrivilegeForm" property="menuold"
                                 type="java.lang.String"/>
                    <%
                        paramdesc = paramdesctemp;
                    %>
                </logic:present>
                <logic:present name="luApprovalPrivilegeForm" property="menu">
                    <bean:define id="paramnewtemp" name="luApprovalPrivilegeForm" property="menu"
                                 type="java.lang.String"/>
                    <%
                        paramnew = paramnewtemp;
                    %>
                </logic:present>
                <logic:equal value="<%=paramdesc%>" name="luApprovalPrivilegeForm"
                             property="menu"><% color = "style='color:#000;'"; %></logic:equal>
                <logic:notEqual value="<%=paramdesc%>" name="luApprovalPrivilegeForm"
                                property="menu"><% color = "style='color:#F00;font-weight:bolder;'"; %></logic:notEqual>
                <tr align="center" class="evalcol">
                    <td align="right">2.</td>
                    <td align="left" <%out.print(color); %>><bean:message key="common.menu"/></td>
                    <td align="left" valign="top"><%=paramdesc %>
                    </td>
                    <td align="left" valign="top"><%=paramnew %>
                    </td>
                </tr>
            </table>
            <br/>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close();">
                            <img src="../../../images/button/ButtonClose.png" alt="cancel" title="Cancel" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>