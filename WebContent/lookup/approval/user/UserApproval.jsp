<%@page import="treemas.util.constant.Global" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "DETAIL APPROVAL USER"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <html:javascript formName="parameterForm" method="validateForm" staticJavascript="false" page="0"/>

        <script language="javascript">

        </script>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/lookup/approval/User.do" method="POST">
        <html:hidden property="checkedMemberList" value=""/>
        <html:hidden property="task" value="Load"/>
        <html:hidden property="userid"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="luApprovalUserForm" property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text">USER ID</td>
                    <td class="Edit-Right-Text"><bean:write name="luApprovalUserForm" property="userid"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text">EDITOR</td>
                    <td class="Edit-Right-Text"><bean:write name="luApprovalUserForm" property="usrupd"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text">DATE CHANGE</td>
                    <td class="Edit-Right-Text"><bean:write name="luApprovalUserForm" property="dtmupd"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text">ACTION</td>
                    <td class="Edit-Right-Text"><bean:write name="luApprovalUserForm" property="usract"/></td>
                </tr>
            </table>
            <br/>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td colspan="4" align="center" width="100%">DETAIL</td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="5%" align="center">NO</td>
                    <td width="30%" align="center">FIELD</td>
                    <td width="30%" align="center">BEFORE</td>
                    <td width="30%" align="center">AFTER</td>
                </tr>
                <tr align="center" class="evalcol">
                    <%
                        String color = "";
                        String branch = "";
                        String fullname = "";
                        String active = "";
                        String password = "";
                        String email = "";
                        String phone_no = "";
                        String handphone = "";
                        String jobdesc = "";
                        String priviledge = "";
                    %>
                    <logic:present name="luApprovalUserForm" property="new_branchid">
                        <bean:define id="newbranch" name="luApprovalUserForm" property="new_branchid"
                                     type="java.lang.String"/>
                        <%
                            branch = newbranch;
                        %>
                    </logic:present>
                    <logic:present name="luApprovalUserForm" property="new_fullname">
                        <bean:define id="newfullname" name="luApprovalUserForm" property="new_fullname"
                                     type="java.lang.String"/>
                        <%
                            fullname = newfullname;
                        %>
                    </logic:present>
                    <logic:present name="luApprovalUserForm" property="new_active">
                        <bean:define id="newactive" name="luApprovalUserForm" property="new_active"
                                     type="java.lang.String"/>
                        <%
                            active = newactive;
                        %>
                    </logic:present>
                    <logic:present name="luApprovalUserForm" property="new_sqlpassword">
                        <bean:define id="newsqlpassword" name="luApprovalUserForm" property="new_sqlpassword"
                                     type="java.lang.String"/>
                        <%
                            password = newsqlpassword;
                        %>
                    </logic:present>
                    <logic:present name="luApprovalUserForm" property="new_email">
                        <bean:define id="newemail" name="luApprovalUserForm" property="new_email"
                                     type="java.lang.String"/>
                        <%
                            email = newemail;
                        %>
                    </logic:present>
                    <logic:present name="luApprovalUserForm" property="new_phone_no">
                        <bean:define id="newphone_no" name="luApprovalUserForm" property="new_phone_no"
                                     type="java.lang.String"/>
                        <%
                            phone_no = newphone_no;
                        %>
                    </logic:present>
                    <logic:present name="luApprovalUserForm" property="new_handphone_no">
                        <bean:define id="newhandphone_no" name="luApprovalUserForm" property="new_handphone_no"
                                     type="java.lang.String"/>
                        <%
                            handphone = newhandphone_no;
                        %>
                    </logic:present>
                    <logic:present name="luApprovalUserForm" property="new_job_desc">
                        <bean:define id="newjob_desc" name="luApprovalUserForm" property="new_job_desc"
                                     type="java.lang.String"/>
                        <%
                            jobdesc = newjob_desc;
                        %>
                    </logic:present>
                    <logic:present name="luApprovalUserForm" property="new_priviledge">
                        <bean:define id="newpriviledge" name="luApprovalUserForm" property="new_priviledge"
                                     type="java.lang.String"/>
                        <%
                            priviledge = newpriviledge;
                        %>
                    </logic:present>

                    <td align="right">1.</td>
                    <logic:notEqual value="ADD" name="luApprovalUserForm" property="usract">
                        <%-- 				     <bean:define id="branch" name="luApprovalUserForm" property="old_branchid" type="java.lang.String"/> --%>
                        <logic:equal value="<%=branch%>" name="luApprovalUserForm"
                                     property="old_branchid"><% color = "style='color:#000;'"; %></logic:equal>
                        <logic:notEqual value="<%=branch%>" name="luApprovalUserForm"
                                        property="old_branchid"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                        </logic:notEqual>
                        <td align="left" <%out.print(color); %>><bean:message key="app.user.branch"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="old_branchid"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_branchid"/></td>
                    </logic:notEqual>
                    <logic:equal value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" style='color:#F00;font-weight:bolder;'><bean:message
                                key="app.user.branch"/></td>
                        <td align="left"></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_branchid"/></td>
                    </logic:equal>

                </tr>
                <tr align="center" class="evalcol">
                    <td align="right">2.</td>
                    <logic:equal value="<%=fullname%>" name="luApprovalUserForm"
                                 property="old_fullname"><% color = "style='color:#000;'"; %></logic:equal>
                    <logic:notEqual value="<%=fullname%>" name="luApprovalUserForm"
                                    property="old_fullname"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                    </logic:notEqual>
                    <logic:notEqual value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" <%out.print(color); %>><bean:message key="app.user.name"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="old_fullname"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_fullname"/></td>
                    </logic:notEqual>
                    <logic:equal value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" style='color:#F00;font-weight:bolder;'><bean:message key="app.user.name"/></td>
                        <td align="left"></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_fullname"/></td>
                    </logic:equal>

                </tr>
                <tr align="center" class="evalcol">
                    <td align="right">3.</td>
                    <logic:equal value="<%=active%>" name="luApprovalUserForm"
                                 property="old_active"><% color = "style='color:#000;'"; %></logic:equal>
                    <logic:notEqual value="<%=active%>" name="luApprovalUserForm"
                                    property="old_active"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                    </logic:notEqual>
                    <logic:notEqual value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" <%out.print(color); %>><bean:message key="app.user.active"/></td>
                        <td align="left">
                            <logic:equal value="1" name="luApprovalUserForm" property="old_active">
                                <%=Global.ACTION_ACTIVATE %>
                            </logic:equal>
                            <logic:equal value="0" name="luApprovalUserForm" property="old_active">
                                <%=Global.ACTION_DEACTIVATE %>
                            </logic:equal></td>
                        <td align="left">
                            <logic:equal value="1" name="luApprovalUserForm" property="new_active">
                                <%=Global.ACTION_ACTIVATE %>
                            </logic:equal>
                            <logic:equal value="0" name="luApprovalUserForm" property="new_active">
                                <%=Global.ACTION_DEACTIVATE %>
                            </logic:equal></td>
                    </logic:notEqual>
                    <logic:equal value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" style='color:#F00;font-weight:bolder;'><bean:message
                                key="app.user.active"/></td>
                        <td align="left"></td>
                        <td align="left">
                            <logic:equal value="1" name="luApprovalUserForm" property="new_active">
                                <%=Global.ACTION_ACTIVATE %>
                            </logic:equal>
                            <logic:equal value="0" name="luApprovalUserForm" property="new_active">
                                <%=Global.ACTION_DEACTIVATE %>
                            </logic:equal></td>
                    </logic:equal>

                </tr>
                <tr align="center" class="evalcol">
                    <td align="right">4.</td>
                    <logic:equal value="<%=password%>" name="luApprovalUserForm"
                                 property="old_sqlpassword"><% color = "style='color:#000;'"; %></logic:equal>
                    <logic:notEqual value="<%=password%>" name="luApprovalUserForm"
                                    property="old_sqlpassword"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                    </logic:notEqual>
                    <logic:notEqual value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" <%out.print(color); %>><bean:message key="app.user.password"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="old_sqlpassword"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_sqlpassword"/></td>
                    </logic:notEqual>
                    <logic:equal value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" style='color:#F00;font-weight:bolder;'><bean:message
                                key="app.user.password"/></td>
                        <td align="left"></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_sqlpassword"/></td>
                    </logic:equal>

                </tr>
                <tr align="center" class="evalcol">
                    <td align="right">5.</td>
                    <logic:equal value="<%=email%>" name="luApprovalUserForm"
                                 property="old_email"><% color = "style='color:#000;'"; %></logic:equal>
                    <logic:notEqual value="<%=email%>" name="luApprovalUserForm"
                                    property="old_email"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                    </logic:notEqual>
                    <logic:notEqual value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" <%out.print(color); %>><bean:message key="app.user.email"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="old_email"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_email"/></td>
                    </logic:notEqual>
                    <logic:equal value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" style='color:#F00;font-weight:bolder;'><bean:message
                                key="app.user.email"/></td>
                        <td align="left"></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_email"/></td>
                    </logic:equal>

                </tr>
                <tr align="center" class="evalcol">
                    <td align="right">6.</td>
                    <logic:equal value="<%=phone_no%>" name="luApprovalUserForm"
                                 property="old_phone_no"><% color = "style='color:#000;'"; %></logic:equal>
                    <logic:notEqual value="<%=phone_no%>" name="luApprovalUserForm"
                                    property="old_phone_no"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                    </logic:notEqual>
                    <logic:notEqual value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" <%out.print(color); %>><bean:message key="app.user.phone"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="old_phone_no"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_phone_no"/></td>
                    </logic:notEqual>
                    <logic:equal value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" style='color:#F00;font-weight:bolder;'><bean:message
                                key="app.user.phone"/></td>
                        <td align="left"></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_phone_no"/></td>
                    </logic:equal>

                </tr>
                <tr align="center" class="evalcol">
                    <td align="right">7.</td>
                    <logic:equal value="<%=handphone%>" name="luApprovalUserForm"
                                 property="old_handphone_no"><% color = "style='color:#000;'"; %></logic:equal>
                    <logic:notEqual value="<%=handphone%>" name="luApprovalUserForm"
                                    property="old_handphone_no"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                    </logic:notEqual>
                    <logic:notEqual value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" <%out.print(color); %>><bean:message key="app.user.handphone"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="old_handphone_no"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_handphone_no"/></td>
                    </logic:notEqual>
                    <logic:equal value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" style='color:#F00;font-weight:bolder;'><bean:message
                                key="app.user.handphone"/></td>
                        <td align="left"></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_handphone_no"/></td>
                    </logic:equal>

                </tr>
                <tr align="center" class="evalcol">
                    <td align="right">8.</td>
                    <logic:equal value="<%=jobdesc%>" name="luApprovalUserForm"
                                 property="old_job_desc"><% color = "style='color:#000;'"; %></logic:equal>
                    <logic:notEqual value="<%=jobdesc%>" name="luApprovalUserForm"
                                    property="old_job_desc"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                    </logic:notEqual>
                    <logic:notEqual value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" <%out.print(color); %>><bean:message key="app.user.job.desc"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="old_job_desc"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_job_desc"/></td>
                    </logic:notEqual>
                    <logic:equal value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" style='color:#F00;font-weight:bolder;'><bean:message
                                key="app.user.job.desc"/></td>
                        <td align="left"></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_job_desc"/></td>
                    </logic:equal>
                </tr>
                <tr align="center" class="evalcol">
                    <td align="right">9.</td>
                    <logic:equal value="<%=priviledge%>" name="luApprovalUserForm"
                                 property="old_priviledge"><% color = "style='color:#000;'"; %></logic:equal>
                    <logic:notEqual value="<%=priviledge%>" name="luApprovalUserForm"
                                    property="old_priviledge"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                    </logic:notEqual>
                    <logic:notEqual value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" <%out.print(color); %>><bean:message key="common.privilege"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="old_priviledge"/></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_priviledge"/></td>
                    </logic:notEqual>
                    <logic:equal value="ADD" name="luApprovalUserForm" property="usract">
                        <td align="left" style='color:#F00;font-weight:bolder;'><bean:message
                                key="common.privilege"/></td>
                        <td align="left"></td>
                        <td align="left"><bean:write name="luApprovalUserForm" property="new_priviledge"/></td>
                    </logic:equal>
                </tr>
            </table>
            <br/>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close();">
                            <img src="../../../images/button/ButtonClose.png" alt="cancel" title="Cancel" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>