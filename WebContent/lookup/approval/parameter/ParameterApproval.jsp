<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "DETAIL APPROVAL GENERAL PARAMETER"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../../javascript/global/js_validator.js"></script>
        <html:javascript formName="parameterForm" method="validateForm" staticJavascript="false" page="0"/>

        <script language="javascript">

        </script>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/lookup/approval/Parameter.do" method="POST">
        <html:hidden property="checkedMemberList" value=""/>
        <html:hidden property="task" value="Load"/>
        <html:hidden property="id"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                    <td class="Top-Table-Header-Center"><bean:write name="luParameterForm" property="title"/></td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text">ID</td>
                    <td class="Edit-Right-Text"><bean:write name="luParameterForm" property="id"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text">EDITOR</td>
                    <td class="Edit-Right-Text"><bean:write name="luParameterForm" property="userid"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text">DATE CHANGE</td>
                    <td class="Edit-Right-Text"><bean:write name="luParameterForm" property="changedate"/></td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text">ACTION</td>
                    <td class="Edit-Right-Text"><bean:write name="luParameterForm" property="actChange"/></td>
                </tr>
            </table>
            <br/>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td colspan="4" align="center" width="100%">DETAIL</td>
                </tr>
                <tr align="center" class="headercol">
                    <td width="5%" align="center">NO</td>
                    <td width="30%" align="center">FIELD</td>
                    <td width="30%" align="center">BEFORE</td>
                    <td width="30%" align="center">AFTER</td>
                </tr>
                <% String color = ""; %>
                <bean:define id="paramdesc" name="luParameterForm" property="old_paramdesc" type="java.lang.String"/>
                <logic:equal value="<%=paramdesc%>" name="luParameterForm"
                             property="newparamdesc"><% color = "style='color:#000;'"; %></logic:equal>
                <logic:notEqual value="<%=paramdesc%>" name="luParameterForm"
                                property="newparamdesc"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                </logic:notEqual>
                <tr align="center" class="evalcol">
                    <td align="right">1.</td>
                    <td align="left" <%out.print(color); %>>DESCRIPTION</td>
                    <td align="left"><bean:write name="luParameterForm" property="old_paramdesc"/></td>
                    <td align="left"><bean:write name="luParameterForm" property="newparamdesc"/></td>
                </tr>
                <bean:define id="old_data_type" name="luParameterForm" property="old_data_type"
                             type="java.lang.String"/>
                <logic:equal value="<%=old_data_type%>" name="luParameterForm"
                             property="newdata_type"><% color = "style='color:#000;'"; %></logic:equal>
                <logic:notEqual value="<%=old_data_type%>" name="luParameterForm"
                                property="newdata_type"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                </logic:notEqual>
                <tr align="center" class="oddcol">
                    <td align="right">2.</td>
                    <td align="left" <%out.print(color); %>>DATA TYPE</td>
                    <td align="left">
                        <logic:equal name="luParameterForm" property="old_data_type" value="N">Numeric</logic:equal>
                        <logic:equal name="luParameterForm" property="old_data_type" value="C">Alfanumeric</logic:equal>
                        <logic:equal name="luParameterForm" property="old_data_type" value="D">Date</logic:equal>
                        <logic:equal name="luParameterForm" property="old_data_type"
                                     value="T1">Time(hh:mm)</logic:equal>
                        <logic:equal name="luParameterForm" property="old_data_type"
                                     value="T2">Time(hh:mm:ss)</logic:equal>
                    </td>
                    <td align="left">
                        <logic:equal name="luParameterForm" property="newdata_type" value="N">Numeric</logic:equal>
                        <logic:equal name="luParameterForm" property="newdata_type" value="C">Alfanumeric</logic:equal>
                        <logic:equal name="luParameterForm" property="newdata_type" value="D">Date</logic:equal>
                        <logic:equal name="luParameterForm" property="newdata_type" value="T1">Time(hh:mm)</logic:equal>
                        <logic:equal name="luParameterForm" property="newdata_type"
                                     value="T2">Time(hh:mm:ss)</logic:equal>
                    </td>
                </tr>
                <bean:define id="old_val" name="luParameterForm" property="old_val" type="java.lang.String"/>
                <logic:equal value="<%=old_val%>" name="luParameterForm"
                             property="newval"><% color = "style='color:#000;'"; %></logic:equal>
                <logic:notEqual value="<%=old_val%>" name="luParameterForm"
                                property="newval"><% color = "style='color:#F00;font-weight:bolder;'"; %>
                </logic:notEqual>
                <tr align="center" class="evalcol">
                    <td align="right">3.</td>
                    <td align="left" <%out.print(color); %>>VALUE</td>
                    <td align="left"><bean:write name="luParameterForm" property="old_val"/></td>
                    <td align="left"><bean:write name="luParameterForm" property="newval"/></td>
                </tr>
            </table>
            <br/>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:window.close();">
                            <img src="../../../images/button/ButtonClose.png" alt="cancel" title="Cancel" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
    </body>
</html:html>