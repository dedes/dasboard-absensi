<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "LOOK UP CITY"; %>
<html:html>
    <html:base/>
    <head>
        <title><%=title %>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link href="../../style/style.css" rel="stylesheet" type="text/css">
        <script language="javascript" SRC="../../javascript/global/global.js"></script>
        <script language="javascript" SRC="../../javascript/global/disable.js"></script>
        <script language="javascript" SRC="../../javascript/global/js_validator.js"></script>

        <script language="javascript">
            function movePage(task) {
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }

            function setback(cityId, city) {
                var map = {
                    'cityId': cityId,
                    'city': city
                };
                back(map, window);
                window.close();
            }

        </script>
    </head>
    <body bgcolor="#FFFFFF" oncontextmenu="return false;">
    <html:form action="/lookup/City.do" method="POST">
        <html:hidden property="task" value="Load"/>
        <html:hidden property="city" styleId="hidCity"/>
        <html:hidden property="cityid" styleId="hidCityId"/>
        <html:hidden property="paging.dispatch" value="Go"/>
        <bean:define id="indexLastRow" name="luCityForm" property="paging.firstRecord" type="java.lang.Integer"/>
        <bean:define id="indexFirstRow" name="luCityForm" property="paging.firstRecord" type="java.lang.Integer"/>
        <div align="center">
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="Top-Table-Header">
                    <td class="Top-Table-Header-Left"></td>
                        <%-- 		      		<td class="Top-Table-Header-Center"><bean:write name="luCityForm" property="title"/></td> --%>
                    <td class="Top-Table-Header-Center"><%=title %>
                    </td>
                    <td class="Top-Table-Header-Right"></td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr>
                    <td class="Edit-Left-Text">CITY ID</td>
                    <td class="Edit-Right-Text">
                        <html:text styleId="cityId" property="search.cityid" styleClass="TextBox" size="30"
                                   maxlength="30"/>
                    </td>
                </tr>
                <tr>
                    <td class="Edit-Left-Text">CITY</td>
                    <td class="Edit-Right-Text">
                        <html:text styleId="city" property="search.city" styleClass="TextBox" size="30" maxlength="30"/>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" height="30">&nbsp;</td>
                    <td width="50%" align="right">
                        <a href="javascript:movePage('Search');">
                            <img src="../../images/button/ButtonSearch.png" alt="Search" title="Search" width="100"
                                 height="31" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" cellpadding="3" cellspacing="1" class="tableview">
                <tr align="center" class="headercol">
                    <td width="30%" align="center">CITY ID</td>
                    <td width="30%" align="center">CITY</td>
                    <td width="5%" align="center">ACTION</td>
                </tr>
                <logic:notEmpty name="list" scope="request">
                    <logic:iterate id="listParam" name="list" scope="request" indexId="index">
                        <%
                            String color = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
                            indexLastRow++;
                        %>
                        <tr class="<%=color%>" onmouseover="this.className='hovercol'"
                            onmouseout="this.className='<%=color%>'">
                            <td align="left"><bean:write name="listParam" property="cityid"/></td>
                            <td align="left"><bean:write name="listParam" property="city"/></td>
                            <td align="center">
                                <a href="javascript:setback('<bean:write name="listParam" property="cityid"/>', '<bean:write name="listParam" property="city"/>');">
                                    <img alt="select" src="../../images/icon/IconSelect.gif"/>
                                </a>
                            </td>
                        </tr>
                    </logic:iterate>
                    <table width="95%" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.luCityForm,'First')"
                                           title="First">
                                    <img src="../../images/gif/first.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.luCityForm,'Prev')"
                                           title="Previous">
                                    <img src="../../images/gif/prev.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="200" align="center">Page
                                <html:text name="luCityForm" property="paging.currentPageNo" size="3" maxlength="6"
                                           styleClass="inpType"/> of <html:hidden property="paging.totalPage"
                                                                                  write="true"/>
                                <a href="javascript:moveToPageNumber(document.luCityForm,'Go');">
                                    <img src="../../images/gif/go.gif" width="20" height="20" align="absmiddle"
                                         border="0"/></a>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.luCityForm,'Next')" title="Next">
                                    <img src="../../images/gif/next.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td width="30" align="center">
                                <html:link href="javascript:moveToPageNumber(document.luCityForm,'Last')" title="Last">
                                    <img src="../../images/gif/last.gif" width="20" height="20" border="0"/>
                                </html:link>
                            </td>
                            <td align="right">Showing row <%= indexFirstRow %> to <%=indexLastRow - 1%> from total
                                <html:hidden property="paging.totalRecord" write="true"/> record(s)
                            </td>
                        </tr>
                    </table>
                </logic:notEmpty>
                <logic:empty name="list" scope="request">
                    <tr class="evalcol">
                        <td bgcolor="FFFFFF" colspan="3" align="center"><font class="errMsg"><bean:message
                                key="common.listnotfound"/></font></td>
                    </tr>
                </logic:empty>
            </table>
        </div>
    </html:form>
    </body>
</html:html>