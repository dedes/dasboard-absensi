<%@page import="com.google.common.base.Strings"%>
<%@page import="java.util.List"%>
<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "LOOK UP USER"; %>
<html:html>
   <html:base/>
   <head>
        <title><%=title%></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		
		<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
		
	<!--<link href="../../style/style.css" rel="stylesheet" type="text/css"> -->
		<script language="javascript" SRC="../../javascript/global/global.js"></script>
		<script language="javascript" SRC="../../javascript/global/disable.js"></script>
		<script language="javascript" SRC="../../javascript/global/js_validator.js"></script>
		
	    <script language="javascript" SRC="../../javascript/global/jquery-1.10.2.js"></script>
	    <script language="javascript" SRC="../../javascript/global/jquery-all.js"></script>
	        
		<link rel="stylesheet" href="../../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
		<link rel="stylesheet" href="../../assets/css/font-icons/entypo/css/entypo.css">
		<link rel="stylesheet" href="../../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
		<link rel="stylesheet" href="../../assets/css/bootstrap.css">
		<link rel="stylesheet" href="../../assets/css/neon-core.css">
		<link rel="stylesheet" href="../../assets/css/neon-theme.css">
		<link rel="stylesheet" href="../../assets/css/neon-forms.css">
		<link rel="stylesheet" href="../../assets/css/custom.css">
		<link rel="stylesheet" href="../../assets/js/zurb-responsive-tables/responsive-tables.css">
		<link rel="stylesheet" href="../../assets/js/selectboxit/jquery.selectBoxIt.css">
		<link rel="stylesheet" href="../../assets/js/select2/select2-bootstrap.css">
		<link rel="stylesheet" href="../../assets/js/select2/select2.css">
		<link rel="stylesheet" href="../../assets/css/skins/green.css">
	
		<script src="../../assets/js/jquery-1.11.3.min.js"></script>
	
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script language="javascript">
            <logic:present name="list">
            function checkedAll() {

                <% int size = ((List)request.getAttribute("list")).size(); %>
                var sizeList = <%=size%>;
                var member = document.getElementsByName("search.memberList");
                if (sizeList > 1) {
                    for (i = 0; i < member.length; i++)
                        with (member[i])
                            checked = document.luUserForm.checkAll.checked;
                } else {
                    with (member[0])
                        checked = document.luUserForm.checkAll.checked;
                }
            }
            </logic:present>

            function setBack() {
                var count = 0;
                var ret = true;
                var dataTemp = '';
                var member = document.getElementsByName("search.memberList");
                with (document.luUserForm) {
                    if (member.length > 1) {
                        for (var i = 0; i < member.length; i++) {
                            if (member[i].checked) {
                                count++;
                                if (i == 0) {
                                    checkedMemberList.value = member[i].value;
                                } else {
                                    checkedMemberList.value = checkedMemberList.value + ',' + member[i].value;
                                }
                            }
                        }
                    } else if (member.length == 1) {
                        if (member[0].checked) {
                            count++;
                            checkedMemberList.value = member.value;
                        }
                    }
                    if (count == 0) {
                        alert('<bean:message key="common.minimalHarusPilih" arg0="Data"/>');
                        ret = false;
                    }
                    dataTemp = checkedMemberList.value;
                }
                if (ret) {
                    var map = {
                        'member': dataTemp
                    };
                    back(map, window);
                    window.close();
                }
            }

            function movePage(task) {
                document.forms[0].task.value = task;
                document.forms[0].submit();
            }


        </script>
    </head>
    <body class="page-body skin-green">
		<div class="page-container">
			<div class="main-content">
				<div class="row">
				 	<html:form action="/lookup/User.do" method="POST" >
					<html:hidden property="task" value="Load"/>
       				<html:hidden property="checkedMemberList" value=""/>
			        <html:hidden property="paging.dispatch" value="Go"/>
			        <bean:define id="indexLastRow" name="luUserForm" property="paging.firstRecord" type="java.lang.Integer"/>
					<bean:define id="indexFirstRow" name="luUserForm" property="paging.firstRecord" type="java.lang.Integer"/>
				        <div class="col-xs-12 col-sm-12 col-md-12">
							<div class="panel panel-primary" data-collapsed="0">
								<div class="panel-heading" style="background:#00a651;">
									<div  class="panel-title"><span style="color:#FFFFFF;" ><%=title %></span></div>
									<div class="panel-options">
										<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
										<a href="javascript:movePage('Load');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
									</div>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-12 col-xl-8" >
											<div class="row">
												<div class="form-group">
													<label for='groupname' class="col-sm-3 control-label"><bean:message key="lookup.privilege.groupname"/></label>
													<div class="col-sm-5">
														<html:hidden property="search.appid"/>
								                        <html:hidden property="search.groupid"/>
								                        <html:hidden property="search.appname" write="true"/>
													</div>
												</div>
											</div>
											<br/>
											<div class="row">
												<div class="form-group">
													<div class="col-sm-3">
														<html:select property="search.searchType1">
								                            <html:option value="10"><bean:message key="combo.label.userid"/></html:option>
								                            <html:option value="20"><bean:message key="combo.label.name"/></html:option>
								                        </html:select>
							                        </div>
													<div class="col-sm-5">
														<html:text property="search.searchValue1" styleClass="form-control" size="50" maxlength="150"/>
													</div>
												</div>
											</div>
											<br/>
											<div class="row">
												<div class="form-group">
													<label for='job_desc' class="col-sm-3 control-label"><bean:message key="app.user.job.desc"/></label>
													<div class="col-sm-5">
														<html:select property="search.job_desc" styleClass="form-control">
								                            <html:option value=""><bean:message key="common.all"/></html:option>
								                            <logic:notEmpty name="listJobDesc" scope="request">
								                                <html:options collection="listJobDesc" property="optKey" labelProperty="optLabel"/>
								                            </logic:notEmpty>
								                        </html:select>
													</div>
												</div>
											</div>
											<br/>
											<div class="row">
												<div class="form-group">
													<label for='job_desc' class="col-sm-3 control-label"><bean:message key="feature.surveyor.filed.person"/></label>
													<div class="col-sm-5">
														<html:checkbox property="search.fieldperson" value="1"/>
													</div>
												</div>
											</div>
											<br/>
											<div class="row">
												<div class="form-group">
													<div class="col-md-3">
														<label style="color:transparent!important" for="search" ><bean:message key="tooltip.search"/><bean:message key="tooltip.search"/></label>
														<!-- <label style="color:transparent!important;"></label>  -->
													</div>
													<div class="col-md-2">
														<button type="button" onclick="javascript:movePage('Search');" class="btn btn-blue"  title="<bean:message key="tooltip.search"/>"><bean:message key="tooltip.search"/></button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<br/>
									<div class="row">
										<div class="col-md-12 col-xl-8" >
											<table class="table table-bordered"  >
												<thead >
													<tr align="center">
												      	<td width="10%" align="center"><bean:message key="app.user.id"/></td>
									                    <td width="20%" align="center"><bean:message key="app.user.name"/></td>
									                    <td width="15%" align="center"><bean:message key="app.user.job.desc"/></td>
									                    <td width="15%" align="center"><bean:message key="app.user.branch"/></td>
												    </tr>
												</thead>
												<tbody>
													<logic:notEmpty name="list" scope="request">
								                    <logic:iterate id="listParam" name="list" scope="request" indexId="index">
								                        <%
								                            String color = (index.intValue() % 2 == 0) ? "evalcol" : "oddcol";
								                            indexLastRow++;
								                        %>
								                        <tr class="<%=color%>" onmouseover="this.className='hovercol'"
								                            onmouseout="this.className='<%=color%>'">
								                            <!-- 						    <td align="center"> -->
								                                <%-- 			  					<html:multibox property="search.memberList"> --%>
								                                <%-- 									<bean:write name="listParam" property="userid" /> --%>
								                                <%-- 								</html:multibox> --%>
								                            <!-- 							</td> -->
								                            <td align="left"><bean:write name="listParam" property="userid"/></td>
								                            <td align="left"><bean:write name="listParam" property="fullname"/></td>
								                            <td align="left"><bean:write name="listParam" property="job_descName"/></td>
								                            <td align="left"><bean:write name="listParam" property="branchname"/></td>
								                        </tr>
								                    </logic:iterate>
								                </logic:notEmpty>
								                <logic:empty name="list" scope="request">
												   <tr>
												      <td colspan="5" align="center" ><font class="errMsg"><bean:message key="common.listnotfound"/></font></td>
												   </tr>
									   			</logic:empty>
												</tbody>
											</table>
										</div>
									</div>
									<div>
										<logic:notEmpty name="list" scope="request">
											<div class="row">
								    			<div class=" col-md-5 "> 
										        	<button type="button" onclick="javascript:moveToPageNumber(document.luUserForm,'First')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-left"></i></button>
										        	
										        	<button type="button" onclick="javascript:moveToPageNumber(document.luUserForm,'Prev')" class="btn btn-xs btn-default"><i class="fa fa-angle-left"></i></button>
								    				
								    				<html:text name="luUserForm" property="paging.currentPageNo" size="3" maxlength="6" styleClass="inpType "/> <bean:message key="paging.of"/> <html:hidden property="paging.totalPage" write="true" />
										    		
										    		<button type="button" onclick="javascript:moveToPageNumber(document.luUserForm,'Go')" class="btn btn-xs btn-default"><i class="fa fa-search"></i></button>
										    		
										        	<button type="button" onclick="javascript:moveToPageNumber(document.luUserForm,'Next')" class="btn btn-xs btn-default"><i class="fa fa-angle-right"></i></button>
								    				
									        		<button type="button" onclick="javascript:moveToPageNumber(document.luUserForm,'Last')" class="btn btn-xs btn-default"><i class="fa fa-angle-double-right"></i></button>
							    					
								    			</div>
								    			<div class="pull-right col-md-4">
							    					<label class="pull-right"><bean:message key="paging.showing.row"/> <%= indexFirstRow %> <bean:message key="paging.showing.to"/> <%=indexLastRow-1%> <bean:message key="paging.showing.total"/> <html:hidden property="paging.totalRecord" write="true"/> <bean:message key="paging.showing.record"/></label>
								    			</div>
									      	</div> 	
								  		</logic:notEmpty> 
										</div>
									<br/>
									<%-- <div class="row">
										<div class="col-md-3 .col-md-offset-3">
											 <button type="button" onclick="javascript:movePage('Close');" class="btn btn-red"  title="<bean:message key="tooltip.back"/>"><bean:message key="tooltip.back"/></button>
										</div>
									</div> --%>
								</div> <!--  END PANEL BODY -->
							</div>
			    		</div>
		    		</html:form>
		    	</div>
    		</div>
    	</div>
    </body>
    <!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../../assets/js/bootstrap.js"></script>
	<script src="../../assets/js/joinable.js"></script>
	<script src="../../assets/js/resizeable.js"></script>
	<script src="../../assets/js/neon-api.js"></script>
	<script src="../../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../../assets/js/jquery.validate.min.js"></script>
	<script src="../../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../../assets/js/bootstrap-datepicker.js"></script>
	<script src="../../assets/js/bootstrap-switch.min.js"></script>
	<script src="../..//assets/js/jquery.multi-select.js"></script>
	<script src="../../assets/js/neon-chat.js"></script>
	<script src="../../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../../assets/js/select2/select2.min.js"></script>
	<script src="../../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../../assets/js/typeahead.min.js"></script>
	<script src="../../assets/js/icheck/icheck.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../../assets/js/neon-demo.js"></script>
</html:html>