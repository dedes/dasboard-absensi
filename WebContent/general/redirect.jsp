<%@page import="treemas.util.constant.Global" %>
<html>
<%
    String contentUri = request.getParameter("redir_url");
    if (contentUri == null) {
        contentUri = (String) request.getAttribute("redir_url");
    }
    if (null != contentUri) {
        request.setAttribute(Global.HOME_USER, request.getAttribute(Global.HOME_USER));
        request.setAttribute(Global.HOME_INFO, request.getAttribute(Global.HOME_INFO));
    }
%>
<body>
<form action="<%=request.getContextPath()%>/APPL.do" method="POST">
    <input type="hidden" name="contentUri" value="<%=contentUri%>"/>
</form>
</body>
<script language="JavaScript">
    document.forms[0].submit();
</script>
</html>
