<%@page import="treemas.util.struts.ApplicationResources"%>
<%@page import="org.apache.struts.Globals"%>
<%@page import="treemas.util.constant.Global"%>
<%@page import="treemas.application.menu.MenuBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ page import="java.util.*"%>
<%@page import="java.util.Locale"%>
<% String clientWeb = "	http://www.treemas.com/"; %>
<bean:define id="userId" scope="session" name="SESS_LOGIN" property="loginId" type="java.lang.String"/>
<%-- <html:base/> --%>
<%-- <html:html> --%>
<!-- <head> -->
<!--   	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
<!--   	<meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
<!--   	<meta http-equiv="Expires" content="0"> -->
<!-- 	<meta http-equiv="Pragma" content="no-cache"> -->
<!-- 	<meta charset="utf-8"> -->
<!-- 	<meta name="viewport" content="width=device-width, initial-scale=1" /> -->
<!-- 	<meta name="description" content="Invoice" /> -->
<!-- 	<meta name="author" content="" /> -->
<!--   </head> -->
<jsp:useBean id="directoryTreeManager" scope="application" class="treemas.application.menu.MenuTreeGenerator"/>
<script language="javascript">
	<% 
		String dispLang = ((Locale) request.getSession().getAttribute(Globals.LOCALE_KEY)).getLanguage();
		List<MenuBean> list = directoryTreeManager.listSideMenu(userId); 
		String formId = null;
		String menuId = null;
		
		String lang = null, activeId = null, activeEn = null, flag = null;
		if ("en".equals(dispLang)) {
	        lang = "?lang=en";
	        flag = "Flag_of_Britain.png";
	        activeId = "";
	        activeEn = "Active";
	    } else {
	        lang = "?lang=id";
	        flag = "Flag_of_Indonesia.png";
	        activeId = "Active";
	        activeEn = "";
	    } 
	   %>
	
	function getParentId(formID, menuID){
		formId = formID;
		menuId = menuID;
	}
	
</script>
		<div class="sidebar-menu sidebar fixed" style="overflow-y: auto;">
			<div class="sidebar-menu-inner">
				<header class="logo-env">
					<!-- logo -->
					<div class="logo">
						 <%--<a href="<%=clientWeb%>">
							<img src="<%=request.getContextPath()%>/images/company/treemas.png" width="120" alt="" />
						</a> --%>
						<label style="Color:#fff; font-size: 12pt; font-weight: 750;">TREEMAS<br/><span style="Color:#ff0; font-size: 12pt; font-weight: 750;">SOLUSI UTAMA</span></label>
					</div>
					
					<!-- logo collapse icon -->
					<div class="sidebar-collapse">
						<a href="javascript:void(0);" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
							<i class="entypo-menu"></i>
						</a>
					</div>
					<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
					<div class="sidebar-mobile-menu visible-xs">
						<a href="javascript:void(0);" class="with-animation"><!-- add class "with-animation" to support animation -->
							<i class="entypo-menu"></i>
						</a>
					</div>
	
				</header>
										
				<ul id="main-menu" class="main-menu">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li>
						<a href="<%=request.getContextPath()%>/APPL.do">
							<i class="entypo-gauge"></i>
							<span class="title"><bean:message key="menu.dashboard"/></span>
						</a>
					</li>
					<% for(int i=0; i<list.size(); i++) {
						String idMenu = list.get(i).getMenuId();
						if(idMenu.equals(menuId)){%>
							<li class="opened active has-sub">
						<%} else {%>
							<li class="has-sub">
						<%}%>
							<a href="javascript:void(0);">
								<i class="entypo-folder"></i>
								<span class="title"><bean:message key="<%=list.get(i).getPrompt()%>" /></span>
							</a>
							<%if(idMenu.equals(menuId)){%>
								<ul class="visible">
							<%} else {%>
								<ul>
							<%}
								if (list.get(i).getChild().size() > 0) {
									for(int x = 0; x < list.get(i).getChild().size(); x++) {
									String id = list.get(i).getChild().get(x).getParentId();
									if(id.equals(formId)){%>
										<li class="Active">
									<%} else {%>
										<li>
									<%}%>
										<a href="<%=request.getContextPath() + list.get(i).getChild().get(x).getFormFileName()%>" onclick="javascript:getParentId('<%=list.get(i).getChild().get(x).getFormId()%>','<%=list.get(i).getMenuId()%>');">
											<i class="entypo-doc-text-inv"></i>
											<span class="title"><bean:message key="<%=list.get(i).getChild().get(x).getPrompt()%>" /></span>
										</a>
									</li>
								<%}%>
								</ul>
								
							<%}%>
						</li>	
					<%}%>					
					<!-- Language Selector -->
					<%-- <li class="has-sub visible-xs">
						<a href="javascript:void(0);">
							<i class="entypo-language"></i>
							<span class="title"><bean:message key="common.language"/></span>
						</a>
						<ul>
							<li>
								<a href="<%=request.getContextPath() %>/APPL.do??lang=id">
									<img src="<%=request.getContextPath() %>/images/icon/Flag_of_Indonesia.png" width="16" height="16" />
									<span class="title">Bahasa</span>
								</a>
							</li>
							<li>
								<a href="<%=request.getContextPath() %>/APPL.do?lang=en">
								<img src="<%=request.getContextPath() %>/images/icon/Flag_of_Britain.png" width="16" height="16" />
									<span class="title">English</span>
								</a>
							</li>
						</ul>			
					</li> --%>
					<li class="visible-xs">
						<a href="javascript:void(0);" >
							<i class="entypo-user"></i> <bean:write name="SESS_LOGIN" property="fullName" />
						</a>
					</li>
					<li class="visible-xs">
						<a href="<%=request.getContextPath()%>/Login.do?task=Logout">
							<i class="entypo-logout right">&nbsp;</i><bean:message key="common.logout"/>
						</a>
					</li>
				</ul>
			</div>
		</div>
<%-- </html:html> --%>