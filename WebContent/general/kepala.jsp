<%@page import="org.apache.struts.Globals"%>
<%@page import="java.util.Locale"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<% 
	String dispLang = ((Locale) request.getSession().getAttribute(Globals.LOCALE_KEY)).getLanguage();
	String lang = null, activeId = null, activeEn = null, flag = null;
	if ("en".equals(dispLang)) {
        lang = "?lang=en";
        flag = "Flag_of_Britain.png";
        activeId = "";
        activeEn = "Active";
    } else {
        lang = "?lang=id";
        flag = "Flag_of_Indonesia.png";
        activeId = "Active";
        activeEn = "";
    } 
   %>
	  	<div class="row">
			<div class="col-md-6 col-sm-4 clearfix hidden-xs pull-right">
		
				<ul class="list-inline links-list pull-right">
		
					
					<li>
						<a href="javascript:void(0);" >
							<i class="entypo-user"></i> <bean:write name="SESS_LOGIN" property="fullName" />
						</a>
					</li>
		
					<li class="sep"></li>
					
					<!-- Language Selector -->
					<li class="dropdown language-selector">
						<%-- <a href="<%=request.getContextPath() %>/APPL.do?<%=lang%>" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true"> --%>
							<img src="<%=request.getContextPath() %>/images/icon/<%=flag%>" width="16" height="16" />
						<!-- </a>	 -->	
						<%-- <ul class="dropdown-menu pull-right">
							<li class="<%=activeId%>">
								<a href="<%=request.getContextPath() %>/APPL.do?lang=id">
									<img src="<%=request.getContextPath() %>/images/icon/Flag_of_Indonesia.png" width="16" height="16" />
									<span>Bahasa</span>
								</a>							
							</li>
							<li class="<%=activeEn%>">
								<a href="<%=request.getContextPath() %>/APPL.do?lang=en">
									<img src="<%=request.getContextPath() %>/images/icon/Flag_of_Britain.png" width="16" height="16" />
									<span>English</span>
								</a>
							</li>
						</ul>	 --%>		
					</li>
					<li class="sep"></li>
					<li>
						<a href="<%=request.getContextPath()%>/Login.do?task=Logout">
							<i class="entypo-logout right">&nbsp;</i><bean:message key="common.logout"/>
						</a>
					</li>
				</ul>
		
			</div>
			
		</div>
<!-- 		<hr /> -->

<%-- </html:html> --%>