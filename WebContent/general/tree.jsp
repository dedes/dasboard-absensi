<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<bean:define id="userId" scope="session" name="SESS_LOGIN" property="loginId" type="java.lang.String"/>
<html:html>
<html:base/>
<HEAD>
<link rel="stylesheet" href="../style/tree.css">
<jsp:useBean id="directoryTreeManager" scope="application" class="treemas.application.menu.MenuTreeGenerator"/>
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<script language="javascript" SRC="../javascript/global/global.js"></script>
<script language="javascript" >
  var ICONPATH = "<%=request.getContextPath()%>/images/menu/";
</script>
<script SRC="../javascript/directory/ua.js"></script>
<script SRC="../javascript/directory/tree.js"></script>
<script SRC="../javascript/global/disable.js"></script>
<script language="javascript">
   <%= directoryTreeManager.getMenu(userId, request.getContextPath()) %>
   initializeDocument();
</script>
<STYLE>
body         { 
scrollbar-face-color: #CCCCCC; scrollbar-highlight-color: #FFFFFF;
               scrollbar-shadow-color: #CCCCCC; color: #000000;
               scrollbar-arrow-color: #000000; scrollbar-base-color: #CCCCCC; 
               padding-left: 10px; padding-right: 10px;
             }
</STYLE>
</HEAD>
<BODY bgcolor="#FFF">
   <form name="frmMenu" method=post>
      <input type="hidden" name="hidCaller" value="menu">
   </form>
   <script>
      window.parent.fmax();
   </script>
</BODY>
</html:html>
