<%@page import="treemas.util.format.TreemasFormatter"%>
<%@page import="treemas.util.constant.Global"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% String title = "CHANGE PASSWORD";%>
<html:html>
<html:base/>
<head>
	<title><bean:message key="change.password"/> | <bean:message key="header.tittle"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
    
   <!-- 	<link href="../style/style.css" rel="stylesheet" type="text/css"> -->
	<script language="javascript" SRC="../javascript/global/global.js"></script>
	<script language="javascript" SRC="../javascript/global/disable.js"></script>
	<script language="javascript" SRC="../javascript/global/js_validator.js"></script>
	
	<link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="../assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../assets/css/neon-core.css">
	<link rel="stylesheet" href="../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../assets/css/custom.css">
	<link rel="stylesheet" href="../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../assets/js/select2/select2.css">
	<link rel="stylesheet" href="../assets/css/skins/green.css">

	<script src="../assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <script type="text/javascript">
        function moveTo(task) {
            if (task == 'Change') {
// 	    if (!validateForm(document.forms[0])) {
// 	        return;
// 	    }
// 	    if(document.forms[0].newPassword.value != document.forms[0].confirmPassword.value){
// 		    var newpass = '<bean:message key="app.pass.new"/>';
// 		    var newpassconf = '<bean:message key="app.pass.new.confirm"/>';
// 		    var message = '<bean:message key="errors.match" arg0="'+newpassconf+'" arg1="'+newpass+'"/>';
// 	    	alert('<bean:message key="errors.match" arg0="Confirm New Password" arg1="New Password"/>');
// 			alert(message);
// 	     	return;
// 	    }
            }

            if (task == 'Cancel') {
                document.forms[0].flagChg.value = "0";
            }

            document.forms[0].task.value = task;
            document.forms[0].submit();
        }
    </script>
</head>

<body class="page-body skin-green" oncontextmenu="return false;">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
		<div class="main-content">
			<jsp:include page="/general/kepala.jsp" />
				<ol class="breadcrumb bc-2">
					<li><a href="javascript:void(0);"><i class="entypo-folder"></i><bean:message key="common.login"/></a></li>
					<li class="active"><a href="<%=request.getContextPath()%>/ChangePassword.do"><bean:message key="change.password"/></a></li>
				</ol>
				
				<logic:equal value="Load" name="chgPasswordForm" property="task">
				    <logic:equal value="1" name="chgPasswordForm" property="isExpired">
				        <script type="text/javascript">
				            alert('<bean:message key="appmgr.password.expired"/>');
				        </script>
				    </logic:equal>
				</logic:equal>
				
				<div class="row">
					<html:form action="/ChangePassword.do" method="POST">
					    <logic:equal value="1" name="chgPasswordForm" property="isExpired">
					        <html:hidden name="chgPasswordForm" property="isExpired" value="1"/>
					    </logic:equal>
					
					    <html:hidden name="chgPasswordForm" property="oldDBPassword"/>
					    <html:hidden name="chgPasswordForm" property="task"/>
					    <html:hidden name="chgPasswordForm" property="flagChg"/>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="panel panel-primary" data-collapsed="0">
								<div class="panel-heading" style="background:#00a651;">
									<div class="panel-title"><span style="color:#FFFFFF;" >
									<bean:message key="change.password"/></span></div>
									<div class="panel-options">
										<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
										<a href="javascript:movePage('Add');" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
									</div>
								</div>
					    <%-- <%@include file="../Error.jsp"%> --%>
					    <div class="panel-body">
					    	<div class="row">
								<div class="form-group">
										<label for='userId' class="col-sm-3 control-label"><bean:message key="app.user.id" /></label>
										<div class="col-sm-5">
											<html:hidden name="chgPasswordForm" property="userId" write="true"/>
										</div>
								</div>
							</div>
							<br/>
							<div class="row">
						        <div class="form-group">
										<label for='oldPassword' class="col-sm-3 control-label"><bean:message key="app.pass.old"/><font color="#FF0000"> *)</font></label>
										<div class="col-sm-5">
											<html:password name="chgPasswordForm" property="oldPassword" styleClass="form-control" size="35" maxlength="26"/>
						   					<font color="#FF0000"><html:errors property="oldPassword" /></font>
										</div>
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="form-group">
										<label for='newPassword' class="col-sm-3 control-label"><bean:message key="app.pass.new"/><font color="#FF0000"> *)</font></label>
										<div class="col-sm-5">
											<html:password name="chgPasswordForm" property="newPassword" styleClass="form-control" size="35" maxlength="26"/>
						   					<font color="#FF0000"><html:errors property="newPassword" /></font>
										</div>
								</div>
							</div>
							<br/>
							<div class="row">
						        <div class="form-group">
										<label for='confirmPassword' class="col-sm-3 control-label"><bean:message key="app.pass.new.confirm"/><font color="#FF0000"> *)</font></label>
										<div class="col-sm-5">
											<html:password name="chgPasswordForm" property="confirmPassword" styleClass="form-control" size="35" maxlength="26"/>
						   					<font color="#FF0000"><html:errors property="confirmPassword" /></font>
										</div>
								</div> 
							</div>   
							<br/> 
							<div class="row">
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-5">
									 <logic:equal value="1" name="chgPasswordForm" property="isExpired">
									 	<button type="button" onclick="javascript:moveTo('ChangeExp');" title="<bean:message key="tooltip.save"/>" class="btn btn-red"><bean:message key="tooltip.save"/></button>
				                     </logic:equal>
				                     <logic:notEqual value="1" name="chgPasswordForm" property="isExpired">
				                     	<button type="button" onclick="javascript:moveTo('Change');" title="<bean:message key="tooltip.save"/>" class="btn btn-red"><bean:message key="tooltip.save"/></button>
				                     </logic:notEqual>
										<button type="button" onclick="javascript:moveTo('Cancel');" title="<bean:message key="tooltip.cancel"/>" class="btn btn-default"><bean:message key="tooltip.cancel"/></button>
									</div>
								</div>
					        </div>
    					</div>
						</div>
					</div>
				</html:form>
			</div>
		</div>
	</div>
</body>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../../../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../../../assets/js/zurb-responsive-tables/responsive-tables.css">
	
	<!-- Bottom scripts (common) -->
	<script src="../assets/js/gsap/TweenMax.min.js"></script>
	<script src="../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../assets/js/bootstrap.js"></script>
	<script src="../assets/js/joinable.js"></script>
	<script src="../assets/js/resizeable.js"></script>
	<script src="../assets/js/neon-api.js"></script>
	<script src="../assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../assets/js/jquery.validate.min.js"></script>
	<script src="../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../assets/js/bootstrap-datepicker.js"></script>
	<script src="../assets/js/bootstrap-switch.min.js"></script>
	<script src="../assets/js/jquery.multi-select.js"></script>
	<script src="../assets/js/neon-chat.js"></script>
	<script src="../assets/js/zurb-responsive-tables/responsive-tables.js"></script>
	<script src="../assets/js/select2/select2.min.js"></script>
	<script src="../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../assets/js/typeahead.min.js"></script>
	<script src="../assets/js/icheck/icheck.min.js"></script>
	<script src="../assets/js/toastr.js"></script> 


	<!-- JavaScripts initializations and stuff -->
	<script src="../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../assets/js/neon-demo.js"></script>
</html:html>