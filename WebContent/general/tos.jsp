<%@page import="treemas.util.constant.Global" %>
<%@page import="treemas.util.tool.ConfigurationProperties" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Term of Use</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link href="../style/style.css" rel="stylesheet" type="text/css">
    <script language="javascript" SRC="../javascript/global/global.js"></script>
    <script language="javascript" SRC="../javascript/global/disable.js"></script>
    <script language="javascript" SRC="../javascript/global/js_validator.js"></script>
</head>
<body bgcolor="#FFFFFF" oncontextmenu="return false;">
<html:hidden property="task" value="Load"/>
<bean:define id="spanString" type="java.lang.String" value=""/>
<bean:define id="useUser" property="useActiveUser" name="aboutForm" type="java.lang.String"/>
<bean:define id="usePeriodeDate" property="usePeriodeDate" name="aboutForm" type="java.lang.String"/>
<bean:define id="useServerImplement" property="useServerImplement" name="aboutForm" type="java.lang.String"/>
<%

    String title = ConfigurationProperties.getInstance().get("application.name");
    String version = ConfigurationProperties.getInstance().get("application.version");
    String build = ConfigurationProperties.getInstance().get("application.build");

    int span = 4;
    if (Global.STRING_TRUE.equals(useUser)) {
        span = span + 1;
    }
    if (Global.STRING_TRUE.equals(usePeriodeDate)) {
        span = span + 1;
    }
    if (Global.STRING_TRUE.equals(useServerImplement)) {
        span = span + 1;
    }

    spanString = span + "";
%>
<center>
    <div id="barII">
        <table border="0" cellspacing="1" cellpadding="1">
            <tr>
                <td width="25%" rowspan="<%=spanString%>"><a target="_blank" href="http://www.treemas.com"><img
                        src="../images/company/treemas.png" alt="PT Treemas Solusi Utama"
                        title="PT Treemas Solusi Utama" width="240" height="100"/></a></td>
                <td colspan="2" align="center">
                    <%=title + " Version: " + version %>
                    <br/>
                    <%= "Build ID: " + build %>
                    <br/>
                    <b>&copy;<bean:write property="tOS" name="aboutForm"/></b>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td align="left">Company Client</td>
                <td align="left"><bean:write property="companyClient" name="aboutForm"/></td>
            </tr>
            <tr>
                <td align="left">Developed By</td>
                <td align="left"><bean:write property="developer" name="aboutForm"/></td>
            </tr>
            <logic:equal value="1" property="useActiveUser" name="aboutForm">
                <tr>
                    <td align="left">Max of Active User</td>
                    <td align="left"><bean:write property="numOfActiveUser" name="aboutForm"/></td>
                </tr>
            </logic:equal>
            <logic:equal value="1" property="usePeriodeDate" name="aboutForm">
                <tr>
                    <td align="left">Application Expired Date</td>
                    <td align="left"><bean:write property="periodDate" name="aboutForm"/></td>
                </tr>
            </logic:equal>
            <logic:equal value="1" property="useServerImplement" name="aboutForm">
                <tr>
                    <td align="left">Server ID</td>
                    <td align="left"><bean:write property="serverImplement" name="aboutForm"/></td>
                </tr>
            </logic:equal>
        </table>
    </div>
</center>
</body>
</html>