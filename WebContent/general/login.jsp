<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<%@ page import="treemas.util.tool.ConfigurationProperties" %>
<%! 
	String version = ConfigurationProperties.getInstance().get("application.version"); 
	String title = ConfigurationProperties.getInstance().get("header.tittle");
%>
<html>
<head>
<html:base />
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=8;IE=edge;IE=11" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">
	
	<title><bean:message key="common.login"/> | <bean:message key="header.tittle"/></title>
	
	<!-- <link href="../style/style.css" rel="stylesheet" type="text/css">
	<script language="javascript" SRC="../javascript/global/disable.js"></script> -->
	<!-- <script language="javascript" SRC="../javascript/global/js_validator.js"></script> -->
	
	<script language="javascript" SRC="<%=request.getContextPath()%>/javascript/global/global.js"></script>
	
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/neon-core.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/neon-theme.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/neon-forms.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/custom.css">

	<script src="<%=request.getContextPath()%>/assets/js/jquery-1.11.3.min.js"></script>
<%-- <html:javascript formName="loginForm" method="validateForm" page="0" staticJavascript="false"/> --%>
	
	<!--[if lt IE 9]><script src="../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

<script type="text/javascript">
	
	var baseurl = '<%=request.getContextPath()%>' ;
	var redirect_url = '<html:rewrite action="/APPL"/>';

   function back() {
	   document.loginForm.task = 'Back';
   }
   
   function next() {	  
// 	   document.loginForm.submit();
    	$("#form_login").submit();
    	/* toastr.success("<html:messages id='msg'><bean:write name='msg'/></html:messages>", "Message", opts) */
   }
   
   function keyPress(e) {	
	  	if (e.keyCode == 13) {
	  		next();
	  	}
   }
   
</script>
</head>
<body class="page-body login-page login-form-fall" oncontextmenu="return false;">
	<logic:messagesPresent>
		<script>
			$(function(){
				var opts = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-top-full-width",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					
					toastr.error("<html:messages id='msg'><bean:write name='msg'/></html:messages>", "", opts);
			})
		</script>
	</logic:messagesPresent>
	<!-- START CONTAINER  -->
	<div class="login-container">
		<!-- START HEADER  -->
		<div class="login-header login-caret">
			<div class="login-content">
				<a href="javascript:void(0);" class="logo">
					<img src="<%=request.getContextPath()%>/images/company/treemas.png" width="220px" height="100px"/>
				</a>
<!-- 				<p class="description">Dear user, log in to access the admin area!</p> -->
	<p class="description" ><b><font color="yellow" size="4"> <bean:message key="header.tittle"/></font></b></p>
				
				<!-- progress bar indicator -->
				<div class="login-progressbar-indicator">
					<h3>43%</h3>
					<span>logging in <%=version%>.</span>
				</div>
			</div>
		</div>
		<!-- END HEADER  -->
		<div class="login-progressbar">
		
			<div></div>
		</div>
		<div class="login-form">
			<div class="login-content">
			
				<!-- <div class="form-login-error">
					<h3>Invalid login</h3>
					<p>Enter <strong>demo</strong>/<strong>demo</strong> as login and password.</p>
				</div> -->
				<html:form action="/Login"  styleId="form_login" >
				<html:hidden property="task" value="Login"/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="entypo-user"></i>
							</div>
							<input type="text" class="form-control" name="userId" id="userId" placeholder='<bean:message key="app.user.id"/>' autocomplete="off" size="30" maxlength="16" onkeypress="keyPress(event)"/>
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="entypo-key"></i>
							</div>
							<input type="password" class="form-control" name="password" id="password" placeholder='<bean:message key="app.user.password"/>' autocomplete="off" size="30" maxlength="26" onkeypress="keyPress(event)"/>
						</div>
					</div>
					<div class="form-group">
						<button type="button" onclick="javascript:next();" title="<bean:message key="common.login"/>" class="btn btn-primary btn-block btn-login">
							<i class="entypo-login" ></i><bean:message key="common.login" />
						</button>
					</div>
				</html:form>
				<div class="login-bottom-links">
<!-- 				<a href="extra-forgot-password.html" class="link">Forgot your password?</a> 
<!-- 				<br /> -->
					<a href="javascript:void(0);"><font color="yellow"> Treemas Solusi Utama</a>  - <a href="#"><%=version%></a></font>
				</div>	
			</div>
		</div>
	</div>
	<!-- END CONTAINER  -->
</body>
<!-- Bottom scripts (common) -->
	<script src="<%=request.getContextPath()%>/assets/js/gsap/TweenMax.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/joinable.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/resizeable.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/neon-api.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/jquery.validate.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/neon-login.js"></script>

	<!-- Imported scripts on this page -->
	<script src="../assets/js/jquery.bootstrap.wizard.min.js"></script>
	<script src="../assets/js/jquery.validate.min.js"></script>
	<script src="../assets/js/jquery.inputmask.bundle.js"></script>
	<script src="../assets/js/toastr.js"></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="<%=request.getContextPath()%>/assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="<%=request.getContextPath()%>/assets/js/neon-demo.js"></script>
</html>
