<%@page import="org.apache.struts.Globals" %>
<%@page import="java.util.Locale" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%
    // 	String clientWeb = "http://treemas.com";
// 	String clientWeb = "http://www.andalanfinance.com";
    String clientWeb = "http://www.bi.go.id";
    String dispLang = ((Locale) request.getSession().getAttribute(Globals.LOCALE_KEY)).getLanguage();
    String lang = "";
%>
<html>
<head>
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <script language="javascript" src="javascript/global/disable.js"></script>
    <script language="javascript" src="javascript/global/timeformat.js"></script>
    <link rel="stylesheet" href="../style/menu.css">
    <script src="javascript/global/RUN_ACT.js" type="text/javascript"></script>
    <script src="javascript/global/AC_FL.js" type="text/javascript"></script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" marginheight="0" marginwidth="0" topmargin="0" rightmargin="0">
<!-- 					<div style="position:absolute; margin-top:5;width: 50%; margin-left: 25%;margin-right: 25%"><center> -->
<!-- 						<object align="top" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" height="65" width="50%"> -->
<!-- 								<param name="movie" value="images/company/clientswf.swf"> -->
<!-- 								<param name="quality" value="high"> -->
<!-- 								<param name="menu" value="false"> -->
<!-- 								<param name="background" value="0x179D20"> -->
<!-- 								<embed src="images/company/clientswf.swf" quality="high" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" menu="false" height="65" width="50%" id="fls"/> -->
<!-- 						</object></center> -->
<!-- 					</div> -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <tr>
        <td width="65%" height="62" style="background-color: #79C4EC; background-repeat: repeat;"
            background="images/back.png" valign="middle">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="80%" style="margin-left: 5px;">
                <tr>
                   <td align="center" valign="middle" width="235px"><a href="<%=clientWeb %>" target="_blank"
                                                                        style="text-decoration: none;color:#000;"><img
                            align="top" alt="client logo" src="images/company/logoifins.png" width="180px"
                            height="60px"/></td>
                    <td width="40px"></td>
                    <td valign="bottom" style="padding-bottom:10px;">
                      <%--   <font style="font-size: 16pt; font-weight: bolder; font-family: verdana; color: #FFD700; vertical-align: bottom; text-shadow: -1px 0 #000, 0 1px #000, 1px 0 #000, 0 -1px #000;">
                            <bean:message key="header.tittle"/> --%>
                        </font>
                        <!-- 							<bean:message key="header.tittle"/>	position: absolute; left: 255px; top: 30px;  -->
                    </td>
                </tr>
            </table>

            <!-- 				&nbsp;&nbsp; -->
        </td>
        <td width="5%" style="background-repeat: repeat;" background="images/back.png">&nbsp;</td>
        <td width="30%" style="background-repeat: repeat;" background="images/back.png">
            <table width="100%" height="72" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="40" align="right" valign="middle">
                     &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                        <% if ("en".equals(dispLang)) {
                            lang = "?lang=en";
                        } else {
                            lang = "";
                        } %>

                        <a href="<%=request.getContextPath()%>/APPL.do<%=lang%>" style="text-decoration: none;"
                           target="_top" title="home">
                            <img alt="home" src="images/gif/home.gif" style="vertical-align: middle;" width="16"
                                 height="16" border="none">
                        </a>
                        <span style="vertical-align: middle;">
						        <label cursor: pointer;">
									<a href="<%=request.getContextPath()%>/APPL.do<%=lang%>"
                                       style="text-decoration: none;" target="_top" title="home">
										<font size="2" color="#000000" face="Verdana, Arial, Helvetica, sans-serif">
											<strong><bean:message key="common.home"/></strong>
										</font>
									</a>
								</label>
							</span>

                        <a href="<%=request.getContextPath()%>/appadmin/UserDetail.do" style="text-decoration: none;"
                           target="main">
                            <img alt="home" src="images/icon/IconPerson.gif" style="vertical-align: middle;" width="16"
                                 height="16" border="none">
                        </a>
                        <span style="vertical-align: middle;">
								<label cursor: pointer;">
									<a href="<%=request.getContextPath()%>/appadmin/UserDetail.do"
                                       style="text-decoration: none;" target="main">
										<font size="2" color="#000000" face="Verdana, Arial, Helvetica, sans-serif">
											<strong><bean:write name="SESS_LOGIN" property="fullName"/></strong>
										</font>
									</a>
								</label>
							</span>
                    </td>
                    <td height="40" align="center" valign="middle">
                        &nbsp;<a href="<%=request.getContextPath()%>/Login.do?task=Logout" target="_top"
                                 style="text-decoration: none;">
                        <img align="top" src="images/button/ButtonLogout.png" border="0" width="80" height="24">
                    </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <font size="2" color="#000000" face="Verdana, Arial, Helvetica, sans-serif">
                            <strong><label id="time">&nbsp;</label></strong>&nbsp;&nbsp;
                            <% if (!"en".equals(dispLang)) { %>
                            <a id="lang" href="<%=request.getContextPath() %>/APPL.do?lang=en" target="_top"><img
                                    alt="LANG ENG" src="images/icon/IconLangUSA.gif" border="0"/></a> | <img
                                alt="LANG INDO" src="images/icon/IconLangID.jpg" border="0"/>
                            <% } else { %>
                            <img alt="LANG ENG" src="images/icon/IconLangUSA.gif" border="0"/> | <a id="lang"
                                                                                                    href="<%=request.getContextPath() %>/APPL.do?lang=id"
                                                                                                    target="_top"><img
                                alt="LANG INDO" src="images/icon/IconLangID.jpg" border="0"/></a>
                            <% } %>
                            &nbsp;&nbsp;
                        </font>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
