<html>
<%
    String user = request.getParameter("userid");
    if (user == null) {
        user = (String) request.getAttribute("userid");
    }
%>
<body>
<form action="<%=request.getContextPath()%>/ChangePassword.do" method="POST">
    <input type="hidden" name="userId" value="<%=user%>"/>
    <input type="hidden" name="flagChg" value="1"/>
    <input type="hidden" name="task" value="Load"/>
    <input type="hidden" name="isExpired" value="1"/>
</form>
</body>
<script language="JavaScript">
    document.forms[0].submit();
</script>
</html>
