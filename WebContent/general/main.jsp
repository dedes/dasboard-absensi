<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%-- <html:html> --%>
<%-- 	<html:base/> --%>
<!-- 	<head> -->
<!-- 		<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon"> -->
<!-- 		<link rel="icon" href="/images/favicon.ico" type="image/x-icon"> -->
<!-- 		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"> -->
<!-- 		<meta http-equiv="X-UA-Compatible" content="IE=8;IE=edge;IE=11" /> -->
<!-- 		<meta http-equiv="Expires" content="0"> -->
<!-- 		<meta http-equiv="Pragma" content="no-cache"> -->
<%-- 		<meta http-equiv="refresh" content="<%=(session.getLastAccessedTime())%>; url=/Login.do"/> --%>
<%-- 		<title><bean:message key="header.tittle"/></title> --%>
<!-- 		<script type="text/javascript"> -->
<!-- 			window.addEventListener('unload', function() { -->
<!-- 			    var xhr = new XMLHttpRequest(); -->
<%-- 			    xhr.open('GET', '/Login.do?task=Logout', false);  --%>
<!-- 			}, false); -->
<!-- 		</script> -->
<!-- 	</head> -->
<%--  	<logic:notPresent name="contentUri">  --%>
<%--  		<bean:define id="contentUri" value="/ToDoAction.do"/> --%>
<%--  	</logic:notPresent> --%>
<!-- 	<frameset framespacing="0" border="0" frameborder="0" rows="90,*, 12"> -->
<%-- 		<frame name="header" scrolling="no" noresize target="contents" src="/header.do"> --%>
<!-- 	  	<frameset cols="22,*" id="Mainframe"> -->
<%-- 	    	<frame name="menu" id="frame_menu" target="main" scrolling="no" src="/menu.do"> --%>
<%-- 		    <frame name="main"  id="frame_main" scrolling="auto" src="/general/blank.jsp"> --%>
<!-- 	 	</frameset> -->
<%-- 	  	<frame name="footer" scrolling="no" src="/general/footer.html"  frameborder="0" /> --%>
<!-- 	  <noframes> -->
<!-- 	  <body> -->
<!-- 	  	<p>This page uses frames, but your browser doesn't support them.</p> -->
<!-- 	  </body> -->
<!-- 	  </noframes> -->
<!-- 	</frameset> -->
<%-- </html:html> --%>
<%-- <jsp:useBean id="detailManager" scope="application" class="treemas.application.feature.monitoring.details.DetailInvoiceManager"/> --%>
	

<%@ page import="treemas.util.tool.ConfigurationProperties" %>
<%! 
	String title = ConfigurationProperties.getInstance().get("header.tittle");

%>
<html:html>
<html:base/>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta http-equiv="Pragma" content="no-cache">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Invoice" />
	<meta name="author" content="" />

	<link rel="icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico">

	<title><bean:message key="overview"/> | <bean:message key="header.tittle"/></title>

	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/neon-core.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/neon-theme.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/neon-forms.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/custom.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/skins/green.css">

	<script src="<%=request.getContextPath()%>/assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="<%=request.getContextPath()%>/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
  </head>
  <body class="page-body skin-green">
	<div class="page-container">
		<jsp:include page="/general/side_menu.jsp" />
	  	<div class="main-content">
	  		<jsp:include page="/general/kepala.jsp" />
	  		<ol class="breadcrumb bc-2">
				<li>
					<a href="<%=request.getContextPath()%>/APPL.do">
						<i class="entypo-gauge"></i><bean:message key="menu.dashboard"/>
					</a>
				</li>
			</ol>
			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-primary" id="charts_env">
						<div class="panel-heading" style="background:#00a651;">
							<div class="panel-title"><span style="color:#FFFFFF;" ><bean:message key="overview"/></span></div>
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div align="center" class="form-group">
										<img src="<%=request.getContextPath() %>/images/icon/icon-magang.png" height="250px" />
										<br/>
										<label style="font-size: 28pt; font-weight: 750;"><strong><bean:message key="common.selamat"/></strong>
										<br/>
										<span style="font-size: 20pt; font-weight: 550;"><bean:write name="SESS_LOGIN" property="fullName" /></span></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  </body>
	<!-- Imported styles on this page -->
<%-- 	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/js/rickshaw/rickshaw.min.css"> --%>
	
	<!-- Bottom scripts (common) -->
	<script src="<%=request.getContextPath()%>/assets/js/gsap/TweenMax.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/joinable.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/resizeable.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/neon-api.js"></script>
<%-- 	<script src="<%=request.getContextPath()%>/assets/js/rickshaw/vendor/d3.v3.js"></script> --%>
	
	<!-- Imported scripts on this page -->
<%-- 	<script src="<%=request.getContextPath()%>/assets/js/rickshaw/rickshaw.min.js"></script> --%>
	<script src="<%=request.getContextPath()%>/assets/js/raphael-min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/js/morris.min.js"></script>
<%-- 	<script src="<%=request.getContextPath()%>/assets/js/jquery.peity.min.js"></script> --%>
<%-- 	<script src="<%=request.getContextPath()%>/assets/js/neon-charts.js"></script> --%>
<%-- 	<script src="<%=request.getContextPath()%>/assets/js/jquery.sparkline.min.js"></script> --%>

	<!-- JavaScripts initializations and stuff -->
	<script src="<%=request.getContextPath()%>/assets/js/neon-custom.js"></script>

	<!-- Demo Settings -->
	<script src="<%=request.getContextPath()%>/assets/js/neon-demo.js"></script>
	
	
</html:html>

