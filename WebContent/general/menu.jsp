<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<HTML>
<html:base/>
<HEAD>
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <script language="javascript" SRC="../javascript/global/disable.js"></script>
    <script language="javascript">
        function fmin1() {
            parent.document.getElementById("Mainframe").cols = "*,0";
            top.ContentFrame.display();
        }
        function fshow() {
            document.all.maximize.src = "../images/close_bttn.gif";
        }
        function fhide() {
            document.all.maximize.src = "../images/maximize.gif";
        }

        function disableMaxLink() {
        }
        function enableMaxLink() {
        }

        function fmax() {
            parent.document.getElementById("Mainframe").cols = "18%,*";
            MenuTree.style.display = "inline";
        }
        function checkTree() {
            if (NavigasiFrame.document.forms[0] != null) {
                enableMaxLink();
            } else {
                setTimeout("checkTree();", 250);
            }
        }
        setTimeout("checkTree();", 350);
    </script>
</HEAD>
<body style="background-color:#CACACA;"
      onload="fmax()" leftmargin="0" topmargin="2" bottommargin="0" rightmargin="5" oncontextmenu="return false;">
<div id="MenuTree" style="display: none; background-color: #FFF;">
    <iframe name="NavigasiFrame" scrolling="auto"
            frameborder="0" width="100%" height="100%"
            marginheight="0" marginwidth="0"
            src="<html:rewrite action="/tree"/>"></iframe>
</div>
</body>
</html>