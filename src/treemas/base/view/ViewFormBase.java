package treemas.base.view;

import treemas.base.feature.FeatureFormBase;
import treemas.util.constant.Global;


public abstract class ViewFormBase extends FeatureFormBase {
    public ViewFormBase() {
        this(Global.ROW_PER_PAGE);
    }

    public ViewFormBase(int pageSize) {
        super(pageSize);
        this.setTask(Global.WEB_TASK_LOAD);
    }

}
