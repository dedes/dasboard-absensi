package treemas.base.view;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.feature.FeatureActionBase;
import treemas.util.constant.Global;
import treemas.util.tool.ConfigurationProperties;

import java.util.HashMap;

public abstract class ViewActionBase extends FeatureActionBase {

    //	protected HashMap defaultPageMap = new HashMap();
    protected HashMap errorPageMap = new HashMap();
    protected ConfigurationProperties resources = ConfigurationProperties.getInstance();

    public ViewActionBase() {
        pageMap.put(null, "view");
        pageMap.put(Global.WEB_TASK_LOAD, "view");
        pageMap.put(Global.WEB_TASK_SEARCH, "view");

        exceptionMap.put(null, "view");
        exceptionMap.put(Global.WEB_TASK_LOAD, "view");
        exceptionMap.put(Global.WEB_TASK_SEARCH, "view");
    }

    public ActionForward getNextPage(String key, ActionMapping mapping) {
        String view = (String) this.pageMap.get(key);
        return mapping.findForward(view);
    }

    public ActionForward getErrorPage(String key, ActionMapping mapping) {
        String view = (String) errorPageMap.get(key);
        return mapping.findForward(view);
    }

}
