package treemas.base.view;

import treemas.base.feature.FeatureManagerBase;

public abstract class ViewManagerBase extends FeatureManagerBase {

    public static final int ERROR_HAS_CHILD = 100;

    public ViewManagerBase() {
    }

}
