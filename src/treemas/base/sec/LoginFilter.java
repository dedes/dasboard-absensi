package treemas.base.sec;

import treemas.application.login.LoginManager;
import treemas.base.sec.term.*;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.ibatis.IbatisHelper;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;

public class LoginFilter implements Filter {
    private static final String APP_NAME = "MSCMobile";
    private static final String KEY_NOT_LOGIN = "not_login_page";
    private static final String KEY_PATTERN_RESTRICTED = "pattern_restricted";
    private static final String REQUEST_CHECKED = "FL_C";
    private static final String KEY_NOT_AUTHORIZED = "not_authorized_page";
    private static final String KEY_DASHBOARD_LOGIN_URI = "dashboard_login";
    private static final String KEY_NOT_VALID = "not_valid_page";
    private static final String KEY_OUT_OF_AGREEMENT = "out_of_agreement";
    private static final String KEY_CHG_PASSWORD = "change_password_page";
    private static final String KEY_TIMEOUT_MESSAGE = "timeout_message";

    private static final String DEFAULT_EXCLUDE_SET_HEADER = "/javascript";

    private static final String DASHBOARD_PARAM_USER = "dash_user";
    private static final String DASHBOARD_PARAM_KEY = "dash_key";

    private boolean verbose = false;

    private String notLoginURI = null;
    private String[] restricteds = null;
    private String notAuthorizedURI = null;
    private String outOfAgreement = null;
    private String uriIfDashboard = null;
    private String notValidURI = null;
    private String changePasswordURI = null;
    private String timeoutMessage = null;

    private ILogger logger = LogManager.getDefaultLogger();
    private UserInterface userChk = new UserImpl(IbatisHelper.getSqlMap());

    private FilterConfig filterConfig;

    public void destroy() {

    }

    public void doFilter(
            ServletRequest req,
            ServletResponse resp,
            FilterChain chain) throws ServletException, IOException {

        if (this.logger == null) {
            this.logger = LogManager.getDefaultLogger();
        }

        if (!this.chkAppValidity(req, resp)) {
            return;
        }

        HttpServletRequest hReq = (HttpServletRequest) req;
        HttpServletResponse hResp = (HttpServletResponse) resp;
        HttpSession sess = hReq.getSession();
        String reqURI = hReq.getRequestURI();
        String ctxPath = hReq.getContextPath();

        SessionBean loginBean = (SessionBean) sess.getAttribute(Global.SESSION_LOGIN);

        boolean wasChecked = (hReq.getAttribute(REQUEST_CHECKED) != null);
        if (("/" + APP_NAME + "/blank.jsp").equals(reqURI)
                || ("/" + APP_NAME + "/audit/Auditrail.do").equals(reqURI)
                ) {
            wasChecked = true;
        }
        long start = 0;
        if (!wasChecked) {
            start = System.currentTimeMillis();
        }

        if (!wasChecked && this.isInRestricted(reqURI)) {
            this.logger.log(ILogger.LEVEL_DEBUG_LOW, hReq.getRemoteAddr() + ": ReqUri=" + reqURI);
            String dashUser = hReq.getParameter(DASHBOARD_PARAM_USER);
            String dashKey = hReq.getParameter(DASHBOARD_PARAM_KEY);

            if (dashUser != null && dashKey != null) {
                sess.setAttribute(Global.SESSION_FROM_DASHBOARD_USER, dashUser);
                sess.setAttribute(Global.SESSION_FROM_DASHBOARD_KEY, dashKey);
                sess.setAttribute(Global.SESSION_FROM_DASHBOARD_URI, reqURI + "?" + this.getQueryStr(hReq));
                hResp.sendRedirect(this.uriIfDashboard);
                return;
            }
            if (loginBean == null) {
                hResp.sendRedirect(ctxPath + this.notLoginURI);
                StringBuffer txt = new StringBuffer("Not logged in:");
                Enumeration headerNames = hReq.getHeaderNames();
                while (headerNames.hasMoreElements()) {
                    String key = (String) headerNames.nextElement();
                    txt.append("\n\t").append(key).append("=").append(hReq.getHeader(key));
                }
                this.logger.log(ILogger.LEVEL_DEBUG_LOW, txt.toString());
                return;
            } else if ("0".equals(loginBean.getChangedPassword())) {
                hResp.sendRedirect(ctxPath + this.changePasswordURI);
                return;
            } else if (!this.checkAccessUri(reqURI, loginBean.getLoginId())) {
                logger.log(ILogger.LEVEL_INFO, "Not authorized: " + reqURI);
                if (this.notAuthorizedURI != null)
                    hResp.sendRedirect(hReq.getContextPath() + this.notAuthorizedURI);
                else
                    hResp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            } else {
                LoginManager lmanager = new LoginManager();
                try {
                    boolean check = lmanager.checkUserLogin(loginBean.getUserid());
                    if (!check) {
                        hResp.sendRedirect(ctxPath + this.notLoginURI);
                        StringBuffer txt = new StringBuffer("Not logged in:");
                        Enumeration headerNames = hReq.getHeaderNames();
                        while (headerNames.hasMoreElements()) {
                            String key = (String) headerNames.nextElement();
                            txt.append("\n\t").append(key).append("=").append(hReq.getHeader(key));
                        }
                        this.logger.log(ILogger.LEVEL_DEBUG_LOW, txt.toString());
                        return;
                    }
                } catch (CoreException e) {
                    this.logger.printStackTrace(e);
                    hResp.sendRedirect(ctxPath + this.notLoginURI);
                    StringBuffer txt = new StringBuffer("Not logged in:");
                    Enumeration headerNames = hReq.getHeaderNames();
                    while (headerNames.hasMoreElements()) {
                        String key = (String) headerNames.nextElement();
                        txt.append("\n\t").append(key).append("=").append(hReq.getHeader(key));
                    }
                    this.logger.log(ILogger.LEVEL_DEBUG_LOW, txt.toString());
                    return;
                }

            }
        }

        if (!wasChecked) {
            hReq.setAttribute(REQUEST_CHECKED, "1");
            if (reqURI.indexOf(DEFAULT_EXCLUDE_SET_HEADER) == -1) {
                if (this.verbose) {
                    this.logger.log(ILogger.LEVEL_DEBUG_LOW, "Set http header for " + reqURI);
                }
                this.setNoCacheHeader(hResp);
            }
        }
        chain.doFilter(req, resp);
        if (!wasChecked) {
            if (this.logger.isLoggable(ILogger.LEVEL_DEBUG_LOW)) {
                this.logger.log(ILogger.LEVEL_DEBUG_LOW,
                        "Exectime(ms) " + reqURI + "=" + (System.currentTimeMillis() - start));
            }
        }
    }

    private boolean checkAccessUri(String uri, String loginId) {
        boolean result = false;
        try {
            result = Privilege.isAuthorizedURI(uri, loginId);
        } catch (AppException e) {
            LogManager.getDefaultLogger().log(ILogger.LEVEL_ERROR, "Check access Uri error: " + e.toString());
        }
        return result;
    }

    private boolean isInRestricted(String reqStr) {
        boolean result = false;
        if (this.restricteds != null) {
            boolean found = false;
            for (int i = 0; i < this.restricteds.length && !found; i++) {
                found = (reqStr.indexOf(this.restricteds[i]) != -1);
            }
            result = found;
        }
//    result = result && this.nonLoginPattern != null && reqStr.indexOf(this.nonLoginPattern) != -1;
        return result;
    }

    public void init(FilterConfig config) throws ServletException {
        this.filterConfig = config;

        this.notValidURI = config.getInitParameter(KEY_NOT_VALID);
        if (this.notValidURI == null) {
            this.log(ILogger.LEVEL_ERROR, "Cannot find init parameter " + KEY_NOT_VALID);
            this.notValidURI = "/error/error.jsp";
            this.log(ILogger.LEVEL_INFO, "Not valid page=" + this.notValidURI);
        }

        this.notLoginURI = config.getInitParameter(KEY_NOT_LOGIN);
        if (this.notLoginURI == null) {
            throw new ServletException("Expect init parameter " + KEY_NOT_LOGIN +
                    " for LoginFilter");
        }
        this.notAuthorizedURI = config.getInitParameter(KEY_NOT_AUTHORIZED);
        this.outOfAgreement = config.getInitParameter(KEY_OUT_OF_AGREEMENT);
        this.timeoutMessage = config.getInitParameter(KEY_TIMEOUT_MESSAGE);
        if (this.timeoutMessage == null) {
            this.timeoutMessage = "Mohon maaf, waktu Anda telah habis. Silahkan lakukan pengisian kembali."; //default
        }

        this.changePasswordURI = config.getInitParameter(KEY_CHG_PASSWORD);
        if (this.changePasswordURI == null) {
            throw new ServletException("Expect init parameter " + KEY_CHG_PASSWORD +
                    " for LoginFilter");
        }

        this.uriIfDashboard = config.getInitParameter(KEY_DASHBOARD_LOGIN_URI);
        if (this.uriIfDashboard == null) {
            this.log(ILogger.LEVEL_ERROR, "Cannot find init parameter " + KEY_DASHBOARD_LOGIN_URI);
            this.uriIfDashboard = "/Login.do";
            this.log(ILogger.LEVEL_INFO, "Dashboard login=" + this.uriIfDashboard);
        }
        String tmp = config.getInitParameter(KEY_PATTERN_RESTRICTED);
        if (tmp != null) {
            StringTokenizer st = new StringTokenizer(tmp, ",");
            ArrayList list = new ArrayList(5);
            while (st.hasMoreTokens()) {
                list.add(st.nextToken().trim());
            }
            if (list.size() > 0) {
                this.restricteds = new String[list.size()];
                list.toArray(this.restricteds);
            }
        }
        this.logger = LogManager.getDefaultLogger();
        if (this.logger != null) {
            this.verbose = (this.logger.getLogLevel() == ILogger.LEVEL_DEBUG_LOW);
        }
        this.log(ILogger.LEVEL_INFO, "LoginFilter is initialized! Verbose=" + this.verbose);
    }

    private void setNoCacheHeader(HttpServletResponse response) {
        // Set to expire far in the past.
        response.setHeader("Expires", "Sun, 1 May 1990 12:00:00 GMT");

        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");

        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");

        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");
    }

    private String getQueryStr(HttpServletRequest request) {
        StringBuffer result = new StringBuffer();
        Enumeration enumeration = request.getParameterNames();
        while (enumeration.hasMoreElements()) {
            String keyParam = (String) enumeration.nextElement();
            if (!DASHBOARD_PARAM_USER.equals(keyParam) &&
                    !DASHBOARD_PARAM_KEY.equals(keyParam)) {
                if (result.length() > 0) {
                    result.append("&");
                }
                result = result.append(keyParam).append("=").append(request.getParameter(keyParam));
            }
        }
        return result.toString();
    }

    private void log(int level, String msg) {
        if (this.logger != null) {
            this.logger.log(level, msg);
        } else {
            System.out.println(msg);
        }
    }

    private boolean chkAppValidity(ServletRequest req, ServletResponse resp) throws ServletException, IOException {
        boolean valid = true;
        try {
            HardwareMAC.checkMAC();
            DatePeriod._EDChecker();
            userChk.checkActiveUser();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch (AppException ex) {
            valid = false;
            int errorCode = ex.getErrorCode();
            HttpServletResponse hResp = (HttpServletResponse) resp;
            HttpServletRequest hReq = (HttpServletRequest) req;

            String msg = AppMessage.getMessage(errorCode);

            hReq.setAttribute("msg", msg);

            RequestDispatcher dispatcher = hReq.getRequestDispatcher(this.outOfAgreement);
            dispatcher.forward(hReq, hResp);
        }
        return valid;
    }

    private boolean isTimedout(HttpServletRequest request) {
        String sessionId = request.getRequestedSessionId();
        boolean validSessionId = request.isRequestedSessionIdValid();

        if (sessionId != null && !validSessionId)
            return true;
        else
            return false;
    }
}
