package treemas.base.sec.term;

import treemas.util.CoreException;

import java.sql.SQLException;


public interface UserInterface {
    public static final StringBuffer ICON_IMG = new StringBuffer().append(
            "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB").append(
            "AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEB").append(
            "AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAaADwDASIA").append(
            "AhEBAxEB/8QAGQAAAwEBAQAAAAAAAAAAAAAACAkKBgAH/8QALRAAAQQDAAECBQMEAwAAAAAABQID").append(
            "BAYBBwgJABETFVWU0woSFCEiJTEmJ2H/xAAbAQAABwEAAAAAAAAAAAAAAAABAgUGBwgJBP/EACsR").append(
            "AAEEAQIFBAICAwAAAAAAAAIBAwQFBgcRABITIiMIFBUhJDEyQTM0Yf/aAAwDAQACEQMRAD8Aop5H").append(
            "5B5F2DxvVOjegaIq12wyN2lsLaGxrXsDZT5Mo5Av95nGT5iXi5NIcdRBhKcfdShH9jOPZOPbPuqv").append(
            "x1b05H696vk6L2lxxRtcUnYoO8n+drIPue7GClwRUDhBOBZR8vsWSOIyX64GsDz0oPGiMNG64RG4").append(
            "by642w16v1j0YY1l4ZecOf8AX6n5W2Otjlo1RXhMBfuUequdsWl23ritJ93FYLLlhKfnH7c/ExaH").append(
            "MYxn4aspC3pHmHyN8qaO5R3fZ9BaN1xXvHpNCzwd91ndJJ28G41jvQEhLf2APVYCLBQeSuUp6SVw").append(
            "LHQ4sRFmONuMYFyHUMRVl2V2dfett1fvTgY7HjWGQNxIJympASpDRHElPg04kMY9S3LsAcU2+Y1a").append(
            "Et035b/+nLQTAcu0qsJefu4rCyzWS4tsO0glZHlkSgsaaTj1TPSLkVBUSJkd7JSvdRZWO4bJiMsy").append(
            "FZhNWptijygLlE26uafEdzm0IXvQXqbVKj2X8hW7vuC8gpRVMdWMSFj4Uy+Ily2I+VoRIkx2XGGF").append(
            "qSh1aFLTjKI9x9E8pAde+TjZ2leZtC7KpXGcLRJHTNlj3/eJELsaHtPYwSkGpFkmjdqx25DI1qfN").append(
            "XAyDUMwmbFRiT8ZnK0Z9LKb65WtHlUFdR9tQBRPmTevJtDs/Lp3ZVdlXbW1dJSq5U2zgQgPbGmRj").append(
            "Riv2SPsoMSbeHuNiLPLXLlIiOkIUvIBdF2LUVs0H+oNsWhFVl3UBiLyNJoTtLEthKuoGroStobUF").append(
            "EsQhzcKH/IS/nDKIMfGHcrzhHupSlSzoLYxc69R+g+IWw1MvFci10wzFb3HyedKdc45Nt48WV78R").append(
            "ebNurtmHeoycUQcVl2OoylUy4b+RenqhwHQjVDJrWhzazy6FoljGZxMum1zUTAcfyW51BxOrscZp").append(
            "ZbTJuycsxyNIsaK8CfOXpTG7lr4lpGGH0cFyzf8AxI2Xjrl3f/ZYvnvnHYvQ9Jsdwh1Att3Z9dFS").append(
            "41fup2qSX63HsGwp5KRCYwMiYkrdmSVJlvuYwvCMpTg6tXad8Lm7KPd9lamJaKv9C1pDcI7Etlb3").append(
            "rbJ4WjD2ocueshbpaNhJTXYOYUCdKbmFsRIzrEOU406tMd7KJluY6FSdnbW/TTULZFQrl6pdh0d0").append(
            "9FP1K4BB9jrZeOxa9rTY7BYKWjSh05tmZFjS2m5UZxLcmOy+37OtNrwSdl5f50e8ovm85YXaqdx9").append(
            "zjaOE9XvGrJXooena51uRlI5os0W0zwKJIWuti820rNeMwfjjMEoZ87Ajzh7pd2VjSzUD0o6NN5L").append(
            "lsCDeZ5jVq1W5lqejNdW1mQYpT4PT+p+y0TPHKikjtDlM22hU4tWtarcyac6SwFf7cleAkzwh3dg").append(
            "TLBk3Geb6keEqkZtvHIcp27FHnHCXoiBGSNl2jshc3N9bcNjrd0/T03CzAKbVtsc4HrTaTousV0E").append(
            "L3fsCUQM2A3PYFiBI9lu55/kzCJGTHhxW0Kz8Z55tKM5/dj0A3Zxaz8k9Sbg1BzTf9o6Z1hGm0ex").append(
            "RqVSdrbHHA2TR3VVCdLEkRl2l9WJRBbDK5C/iey8tp9sY9vQfN9aWfgoZyRVdSb48TfkJHDNkat0").append(
            "xG1ZovRIZXQs8FHS0PG2lm11ycUWmzNqHRozdnktNyVWyeKnPQjrj85OCz8qSsr7n3EtSMtqUP1a").append(
            "pTavb9yMq1JRs5Qr2znHunOf259s5x74/pn1Wz1J6H1Gj0nEJmNDfPYzlreRpV2eRWJuz7JzHbCJ").append(
            "Dku/DTsHwK6pAFubEc6c+plxpJvkMGzkLFlNsq1PZOWAvi90kfY6Km20KcoI6Clt1AkyW3O4TRFE").append(
            "xJEROYE3FSZHrA3syl+PTjq66O5UCdObUG3GxOuska1Wik6k65r+xtiXG7lgZk+Xr6hVpLxwMSq0").append(
            "huAWXLVcLAIJNhjrQqQLk7ad2f3BYhuyxtx8ZtktVLnGkMVYDLWSloTTh9XqWbKMvIawVPCrUcav").append(
            "1uFxA6qkHI149Xg+yyQGSaxrmBIvU7Wwf+P7C2CGAf4MRBv11RCFCP8AGjYaHLQVfcRFgwvgRY6X").append(
            "HnXHlpZaRhTri3FYytas5yPzkx9VJffSvy+qv9Nvv7A8n+TtHyfSD3/Xd2og92/b2/x+uHEsuUvt").append(
            "t5MhfZf6e7zi+08ivfjd3g8yq74uTyKp/wAvvij9joLoU7QwwiyeIeFGrQ+lWG/VDXhDEQzDFnGc").append(
            "Wz+Hq9sFH1BJC1XYNlJhMlMG5iRlHihz49ZKxtWIsPCENdsGz7dpjmox2uPGlrq7a33VqAbZt80x").append(
            "mkjKyuo28OyZuDNUsC8U0hBsE2CsFGCVyu2OrQcqt9iET3zQdiO+27Ml85MfVSX30r8vrvnJj6qS").append(
            "++lfl9CyAR32ZUcBYlRibOPJZFGpDBskJNEy8CC40TRCJNk2QqCoiiqbcdB21q7Gehu2dg7DkOG7").append(
            "IiOTZLkZ91x1H3HXmDdVp1xx5EeMzAiN3yEqnuvFH4bonosEjXxVfh8ZiGtd1YaaDRadICZMUwZY").append(
            "Wyxi2CtVEJ+rQQgaUBQssBy9UUdqZuy3KyYaCDCVehnLFFyNv3/18cu17LSvE3XCzMgZb6rsh0pX").append(
            "49ise7kVa5aQg1iujrZPqA8bOqZWsL2QQHErWktXpLFSqxGN/CIDk1YlPd85MfViX30r8vrvnJj6").append(
            "qS++lfl9KXy1r1Uf+UseugE2jyzpSuo2Uv35No4rvOgFO/MIN+VZX5Cp1e/hM6YImyACIuyqnKmy").append(
            "qibIv6/pPpP+fX64osqeyNnU6xhrVWfDxRKkqDZZA4Vc6XVYg65MwoEwFAftgEXjSVYNiYloH2FR").append(
            "CtRbSRpZMfDC2SLcEg58BqFNWj5V/wCvde5s+2U+8PWH9uf94/6mo/8ATP8A7j/WfQA/OTH1Yl99").append(
            "K/L6YLyLRaTZ9e2YzZKdVbCYk36eiSVOV4QWJSER6vUY8dL86fDkSnUsMNtsMpcdVhpltDaMJQlK").append(
            "cEnWVjaOjIs7CdYvg2jIPz5ciY8LQqRC0Lslxw0bQiIkBCQUIiLbclVRAAbRUABBFXdUAUFFX9bq").append(
            "iIib7f3x/9k=");

    public void checkActiveUser() throws SQLException, CoreException;
}
