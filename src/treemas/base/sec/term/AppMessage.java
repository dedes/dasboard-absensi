package treemas.base.sec.term;

import treemas.util.CoreException;
import treemas.util.format.TreemasFormatter;

import java.text.ParseException;
import java.util.Date;

public class AppMessage {

    public static String getMessage(int errorCode) {

        StringBuffer msg = new StringBuffer();

        switch (errorCode) {
            case CoreException.ERROR_EXPIRED:
                String expDateStr = null;
                try {
                    Date expDate = TreemasFormatter.parseDate(DatePeriod.ED, DatePeriod.EDF);
                    expDateStr = TreemasFormatter.formatDate(expDate, "dd MMMMM yyyy");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                msg.append("This Application has been expired since " + expDateStr);
                break;

            case CoreException.ERROR_LIMIT:
                msg.append("Active user can not exceed than " + UserImpl.LIMIT);
                break;

            case CoreException.ERROR_MAC:
                msg.append("Your Application License is not valid! Please contact PT Treemas Solusi Utama for more info.");
        }

        return msg.toString();
    }
}
