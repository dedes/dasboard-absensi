package treemas.base.sec.term;

import com.ibatis.sqlmap.client.SqlMapClient;
import treemas.util.CoreException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserImpl implements UserInterface {

    public static final int LIMIT = Integer.MAX_VALUE;
    private static SqlMapClient ibatis;

    public UserImpl() {
    }

    public UserImpl(SqlMapClient ibatis) {
        if (UserImpl.ibatis == null)
            UserImpl.ibatis = ibatis;
    }

    public void setSqlMapClient(SqlMapClient ibatis) {
        if (UserImpl.ibatis == null)
            UserImpl.ibatis = ibatis;
    }

    public void checkActiveUser() throws SQLException, CoreException {
        int jumlahAktifUser = this.countActiveUser();
        if (jumlahAktifUser > LIMIT) {
            throw new CoreException(CoreException.ERROR_LIMIT);
        }
    }

    public int getActiveUser() throws SQLException, CoreException {
        int jumlahAktifUser = this.countActiveUser();
        return jumlahAktifUser;
    }

    private int countActiveUser() throws SQLException {
        int jumlahAktifUser = -1;
        final String sqlQuery = "SELECT COUNT(1) FROM SYS_USER WHERE  active = '1'";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = ibatis.getDataSource().getConnection();
            ps = conn.prepareStatement(sqlQuery);
            rs = ps.executeQuery();

            while (rs.next()) {
                jumlahAktifUser = rs.getInt(1);
            }
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            if (conn != null)
                conn.close();
        }

        return jumlahAktifUser;
    }
}
