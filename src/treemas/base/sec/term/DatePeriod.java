package treemas.base.sec.term;

import treemas.util.CoreException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class DatePeriod {

    public static final String ED = "02122099";
    public static final String EDF = "ddMMyyyy";

    public static void _EDChecker() throws CoreException {

        Date currentDate = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat(EDF);
        Date expiredDate = null;
        try {
            expiredDate = sdf.parse(ED);
            if (currentDate.after(expiredDate)) {
                // throw new MobileSurveyException
                // (CoreException.ERROR_EXPIRED);
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
    }
}
