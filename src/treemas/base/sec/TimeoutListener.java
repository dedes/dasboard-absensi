package treemas.base.sec;

import treemas.application.login.LoginManager;
import treemas.globalbean.SessionBean;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class TimeoutListener implements HttpSessionListener {
    private LoginManager loginManager;

    public TimeoutListener() {
        System.out.println("Create Timeout Listener");
    }

    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
    }

    private synchronized void createLoginManager() {
        if (this.loginManager == null) {
            this.loginManager = new LoginManager();
        }
    }

    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        if (this.loginManager == null) {
            this.createLoginManager();
        }

        ILogger logger = LogManager.getDefaultLogger();
        SessionBean user = (SessionBean) httpSessionEvent.getSession().getAttribute(Global.SESSION_LOGIN);
        if (user == null)
            return;
        String userId = user.getLoginId();
        try {
            logger.log(ILogger.LEVEL_DEBUG_HIGH,
                    "Releasing login status for user " + userId);
            this.loginManager.updateUserLogout(user);
            user = null;
        } catch (CoreException e) {
            logger.log(ILogger.LEVEL_ERROR,
                    "Fail releasing login status: " + e.toString());
        } catch (Exception e) {
            logger.log(ILogger.LEVEL_ERROR,
                    "Exception occurred when releasing login status for user "
                            + user + ": " + e.toString());
            logger.printStackTrace(e);
        }
        httpSessionEvent.getSession().invalidate();
    }

}
