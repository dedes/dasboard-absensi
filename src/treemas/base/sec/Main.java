package treemas.base.sec;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.general.notification.dbnotif.UserNotifForm;
import treemas.application.general.notification.dbnotif.UserNotifManager;
import treemas.base.core.CoreActionBase;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;

public class Main extends CoreActionBase {

    private UserNotifManager notifManager = new UserNotifManager();

    public Main() {
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response) throws CoreException {

        String contentUri = request.getParameter("contentUri");
        if (contentUri == null && this.getLoginBean(request) == null) {
            try {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            } catch (Exception e) {
                this.logger.printStackTrace(e);
            }
            return null;
        }
        this.setInfoToPage(request);
        request.setAttribute("contentUri", contentUri);
        return mapping.findForward("main");
    }

    private void setInfoToPage(HttpServletRequest request) throws CoreException {
        String userId = this.getLoginId(request);
        List list = this.notifManager.getUserNotifByUserId(userId);
        list = BeanConverter.convertBeansToString(list, UserNotifForm.class);
        this.setUserInfo(request, list);
        String lang = (String) request.getParameter("lang");
        this.setLanguage(lang, request);
//		request.getSession().getAttribute(Global.SESSION_LOGIN);
//		(Global.HOME_USER, userId);
//		request.setAttribute(Global.HOME_INFO, list);

    }

    public void setLanguage(String lang, HttpServletRequest request) {
        if (Global.LANG_ID.equals(lang)) {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, indo);
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_ID);
        } else if (Global.LANG_EN.equals(lang)) {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, Locale.getDefault());
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_EN);
        } else {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, indo);
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_ID);
        }
    }
    
   	
  }
