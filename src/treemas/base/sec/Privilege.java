package treemas.base.sec;

import com.ibatis.sqlmap.client.SqlMapClient;
import treemas.util.AppException;
import treemas.util.ibatis.IbatisHelper;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import java.sql.SQLException;
import java.util.HashMap;

public class Privilege {
    private static ILogger logger = LogManager.getDefaultLogger();
    private static SqlMapClient ibatis = IbatisHelper.getSqlMap();

    private static ThreadLocal checkMenuParams = new ThreadLocal();
    private static ThreadLocal checkFeatureParams = new ThreadLocal();

    private Privilege() {
    }

    public static boolean isAuthorizedURI(String uri, String user)
            throws AppException {
        boolean result = true;
        if (uri == null || user == null) {
            return false;
        }

        HashMap map = (HashMap) checkMenuParams.get();
        if (map == null) {
            map = new HashMap(2, 1f);
        }
        map.put("uri", uri);
        map.put("userId", user);

        try {
            Object obj = ibatis.queryForObject("appadmin.privilege.checkMenu", map);
            result = (obj != null);
        } catch (SQLException e) {
            result = false;
            logger.printStackTrace(e);
            throw new AppException(e);
        }
        return result;
    }

    public static boolean isAuthorizedFeature(String userId, String formId,
                                              String featureId) throws AppException {
        boolean result = true;
        if (formId == null || userId == null || featureId == null) {
            return false;
        }
        HashMap map = (HashMap) checkFeatureParams.get();
        if (map == null) {
            map = new HashMap(3, 1f);
        }
        map.put("userId", userId);
        map.put("formId", formId);
        map.put("featureId", featureId);

        try {
            Object obj = ibatis.queryForObject("appadmin.privilege.checkFeature", map);
            result = (obj != null);
        } catch (SQLException e) {
            result = false;
            logger.printStackTrace(e);
            throw new AppException(e);
        }
        return result;
    }
}
