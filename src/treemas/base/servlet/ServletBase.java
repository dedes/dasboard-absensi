package treemas.base.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import com.google.common.base.Strings;

//import treemas.globalbean.SessionBean;
import treemas.globalbean.SessionBean;
import treemas.util.constant.Global;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;
import treemas.util.struts.ApplicationResources;



public abstract class ServletBase extends HttpServlet {
	protected ILogger logger = LogManager.getDefaultLogger();
//	protected MobileManager manager = new MobileManager();
	
	protected ApplicationResources resources = ApplicationResources.getInstanceEng();
	
/*	protected SessionBean getLoginBean(HttpServletRequest request) {
		SessionBean result = null;
		SessionBean bean = (SessionBean) request.getSession(true).getAttribute(
				Global.SESSION_LOGIN);
		if (bean != null) {
			result = bean;
		}
		return result;
	}
*/	
	protected ApplicationResources getResources(HttpServletRequest request){
		String lang = (String) request.getSession().getAttribute(Global.LANG);
		if(Strings.isNullOrEmpty(lang)){
			this.resources = ApplicationResources.getInstanceEng();
		}else{
			if(Global.LANG_EN.equalsIgnoreCase(lang)){
				this.resources = ApplicationResources.getInstanceEng();
			}else{
				this.resources = ApplicationResources.getInstanceEng();
			}
		}
		return this.resources;
	}
	
	protected String getValueOf(String string){
		return Strings.isNullOrEmpty(string)?"":string;
	}
	
	protected String getValueOf(String label, String string){
		return Strings.isNullOrEmpty(string)?"":string.replaceFirst("null", label);
	}
	
	protected String getLoginId(HttpServletRequest request) {
        String result = null;
        SessionBean bean = (SessionBean) request.getSession(true).getAttribute(Global.SESSION_LOGIN);
        if (bean != null) {
            result = bean.getLoginId();
        }
        result = (result == null) ? "NOTLOGIN" : result;
        return result;
    }
	
	protected String getMessage(String key, ApplicationResources resources){
		String ret = "";
		int indexCode = 0;
        if(key.contains("[")){
            indexCode = key.indexOf("[");
        }
        boolean parseCode = indexCode>0?true:false;
        
        if(parseCode){
            String code = key.substring(0,indexCode);
//            System.out.println(key.replaceAll(code, ""));
            String[] value = key.replaceAll(code, "").replaceAll("\\[", "").replaceAll("\\]", "").split(",");
//            System.out.println(code);
//            for (int i = 0; i < value.length; i++) {
//                String value1 = value[i];
//                System.out.println(value1);
//            }
            ret = resources.getMessage(code, value);
        }else{
//            System.out.println(key);
            ret = resources.getMessage(key);
        }
        
        return ret;
	}
}
