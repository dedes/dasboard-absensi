package treemas.base.setup;

import com.ibatis.sqlmap.client.SqlMapClient;
import treemas.util.auditrail.AuditTrailManager;
import treemas.util.ibatis.IbatisHelper;
import treemas.util.struts.ManagerBase;

public abstract class SetUpManagerBase extends ManagerBase {

    public static final int ERROR_HAS_CHILD = 100;

    protected SqlMapClient ibatisSqlMap = null;
    protected AuditTrailManager auditManager = new AuditTrailManager();

    public SetUpManagerBase() {

        this.ibatisSqlMap = IbatisHelper.getSqlMap();

    }
}
