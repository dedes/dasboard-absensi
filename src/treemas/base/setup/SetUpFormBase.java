package treemas.base.setup;

import treemas.base.core.CoreFormBase;
import treemas.util.constant.Global;

public abstract class SetUpFormBase extends CoreFormBase {

    public SetUpFormBase() {
        this(Global.ROW_PER_PAGE);
    }

    public SetUpFormBase(int pageSize) {
        super(pageSize);
        this.setTask(Global.WEB_TASK_LOAD);
    }

    protected boolean shallValidate() {
        String task = this.getTask();
        return Global.WEB_TASK_INSERT.equals(task) || Global.WEB_TASK_UPDATE.equals(task);
    }

    public String resolvePreviousTask() {
        String task = getTask();
        if (Global.WEB_TASK_INSERT.equals(task))
            return Global.WEB_TASK_ADD;
        else if (Global.WEB_TASK_UPDATE.equals(task))
            return Global.WEB_TASK_EDIT;
        else
            return task;
    }

}
