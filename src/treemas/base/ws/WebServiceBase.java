package treemas.base.ws;

import org.apache.struts.Globals;
import treemas.globalbean.SessionBean;
import treemas.util.constant.Global;
import treemas.util.struts.ApplicationResources;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class WebServiceBase {
    protected ApplicationResources resources = ApplicationResources.getInstance("ApplicationResources");
    protected Locale indo = new Locale("id", "ID", "ID");

    protected String getLoginId(HttpServletRequest request) {
        String result = null;
        SessionBean bean = (SessionBean) request.getSession(true).getAttribute(Global.SESSION_LOGIN);
        if (bean != null) {
            result = bean.getLoginId();
        }
        result = (result == null) ? "NOTLOGIN" : result;
        return result;
    }

    protected SessionBean getLoginBean(HttpServletRequest request) {
        SessionBean result = null;
        SessionBean bean = (SessionBean) request.getSession(true).getAttribute(
                Global.SESSION_LOGIN);
        if (bean != null) {
            result = bean;
        }
        return result;
    }

    public void setLanguage(String lang, HttpServletRequest request) {

        if (Global.LANG_ID.equals(lang)) {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, indo);
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_ID);
        } else if (Global.LANG_EN.equals(lang)) {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY,
                    Locale.getDefault());
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_EN);
        } else {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, indo);
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_ID);
        }
    }

}
