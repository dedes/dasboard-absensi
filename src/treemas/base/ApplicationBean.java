package treemas.base;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class ApplicationBean {

    private static final int MAX_CACHE_SIZE = 100;
    private static Map cacheMethods = Collections
            .synchronizedMap(new LinkedHashMap() {
                protected boolean removeEldestEntry(Map.Entry eldest) {
                    return size() > MAX_CACHE_SIZE;
                }
            });

    public static void replicationSingleParam(Object source, Object target) {
        if (source.getClass() != target.getClass()) {
            return;
        }
        try {
            Class sourceClass = source.getClass();
            Class targetClass = target.getClass();
            Method[] targetMethods = (Method[]) cacheMethods.get(targetClass
                    .getName());
            if (targetMethods == null) {
                targetMethods = targetClass.getMethods();
                cacheMethods.put(targetClass.getName(), targetMethods);
            }
            for (int i = 0; i < targetMethods.length; i++) {
                Method targetMethod = targetMethods[i];
                if (!targetMethod.getName().startsWith("set")
                        || targetMethod.getParameterTypes().length != 1
                        || !targetMethod.getReturnType().equals(Void.TYPE)) {
                    continue;
                }
                String tmpName = targetMethod.getName().substring(3);

                Method sourceMethod = null;
                try {
                    sourceMethod = sourceClass.getMethod("get" + tmpName, null);
                } catch (NoSuchMethodException e) {
                }

                Object sourceVal = sourceMethod.invoke(source, null);
                System.out.println(sourceVal);
                Class targetMethodParamClass = targetMethod.getParameterTypes()[0];
                targetMethod.invoke(target, new Object[]{sourceVal});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
