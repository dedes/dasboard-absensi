package treemas.base.report;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.feature.FeatureActionBase;
import treemas.util.constant.Global;

import java.util.HashMap;

public abstract class ReportActionBase extends FeatureActionBase {
    protected HashMap pageMap = new HashMap();
    protected HashMap exceptionMap = new HashMap();

    public ReportActionBase() {
        pageMap.put(null, "view");
        pageMap.put(Global.WEB_TASK_LOAD, "view");
        pageMap.put("Show", "view");
        pageMap.put(Global.WEB_TASK_SEARCH, "view");
        pageMap.put(Global.WEB_TASK_DELETE, "view");
        pageMap.put(Global.WEB_TASK_INSERT, "view");
        pageMap.put(Global.WEB_TASK_UPDATE, "view");
        pageMap.put(Global.WEB_TASK_BACK, "view");
        pageMap.put(Global.WEB_TASK_ADD, "edit");
        pageMap.put(Global.WEB_TASK_EDIT, "edit");

        exceptionMap.put(null, "view");
        exceptionMap.put(Global.WEB_TASK_LOAD, "view");
        exceptionMap.put("Show", "view");
        exceptionMap.put(Global.WEB_TASK_SEARCH, "view");
        exceptionMap.put(Global.WEB_TASK_DELETE, "view");
        exceptionMap.put(Global.WEB_TASK_BACK, "view");
        exceptionMap.put(Global.WEB_TASK_INSERT, "edit");
        exceptionMap.put(Global.WEB_TASK_UPDATE, "edit");
        exceptionMap.put(Global.WEB_TASK_ADD, "edit");
        exceptionMap.put(Global.WEB_TASK_EDIT, "edit");

    }

    protected void finalize() throws Throwable {
    }

    public ActionForward getNextPage(String key, ActionMapping mapping) {
        String view = (String) pageMap.get(key);
        return mapping.findForward(view);
    }

    public ActionForward getErrorPage(String key, ActionMapping mapping) {
        String view = (String) exceptionMap.get(key);
        return mapping.findForward(view);
    }

}
