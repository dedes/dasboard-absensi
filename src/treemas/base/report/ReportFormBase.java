package treemas.base.report;

import treemas.base.feature.FeatureFormBase;
import treemas.util.constant.Global;

public class ReportFormBase extends FeatureFormBase {
    public ReportFormBase() {
        this(Global.ROW_PER_PAGE);
    }

    public ReportFormBase(int pageSize) {
        super(pageSize);
        this.setTask("Show");
    }

    protected boolean shallValidate() {
        String task = this.getTask();
        return Global.WEB_TASK_INSERT.equals(task)
                || Global.WEB_TASK_UPDATE.equals(task);
    }

    public String resolvePreviousTask() {
        String task = getTask();
        if (Global.WEB_TASK_INSERT.equals(task))
            return Global.WEB_TASK_ADD;
        else if (Global.WEB_TASK_UPDATE.equals(task))
            return Global.WEB_TASK_EDIT;
        else
            return task;
    }

}
