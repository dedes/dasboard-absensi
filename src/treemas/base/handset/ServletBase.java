package treemas.base.handset;

import com.google.common.base.Strings;
import treemas.globalbean.SessionBean;
import treemas.handset.HandsetManager;
import treemas.util.constant.Global;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;
import treemas.util.struts.ApplicationResources;
import treemas.util.struts.MultipleBaseBean;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


public abstract class ServletBase extends HttpServlet {
    protected ILogger logger = LogManager.getDefaultLogger();
    //	protected MobileManager manager = new MobileManager();
    protected HandsetManager demoManager = new HandsetManager();

    protected ApplicationResources resources = ApplicationResources.getInstance();

    protected SessionBean getLoginBean(HttpServletRequest request) {
        SessionBean result = null;
        SessionBean bean = (SessionBean) request.getSession(true).getAttribute(
                Global.SESSION_LOGIN);
        if (bean != null) {
            result = bean;
        }
        return result;
    }

    protected ApplicationResources getResources(HttpServletRequest request) {
        String lang = (String) request.getSession().getAttribute(Global.LANG);
        if (Strings.isNullOrEmpty(lang)) {
            this.resources = ApplicationResources.getInstanceInd();
        } else {
            if (Global.LANG_EN.equalsIgnoreCase(lang)) {
                this.resources = ApplicationResources.getInstanceEng();
            } else {
                this.resources = ApplicationResources.getInstance();
            }
        }
        return this.resources;
    }

    protected String generateMultipleCombo(List<MultipleBaseBean> list, String value) {
        String ret = "";
        String val = this.getValueOf(value);
        String selected = "";
        if (!Strings.isNullOrEmpty(val)) {
            selected = selected + "selected";
        } else {
            selected = "";
        }
        for (MultipleBaseBean bean : list) {
            ret = ret + "<option value='" + bean.getKeyValue() + "'";
            if (!Strings.isNullOrEmpty(selected)) {
                ret = ret + " " + selected;
            }
            ret = ret + ">" + bean.getLabel() + "</option>";
        }
        return ret;
    }

    protected String getValueOf(String string) {
        return Strings.isNullOrEmpty(string) ? "" : string;
    }

    protected String getMessage(String key, ApplicationResources resources) {
        String ret = "";
        int indexCode = 0;
        if (key.contains("[")) {
            indexCode = key.indexOf("[");
        }
        boolean parseCode = indexCode > 0 ? true : false;

        if (parseCode) {
            String code = key.substring(0, indexCode);
//            System.out.println(key.replaceAll(code, ""));
            String[] value = key.replaceAll(code, "").replaceAll("\\[", "").replaceAll("\\]", "").split(",");
//            System.out.println(code);
//            for (int i = 0; i < value.length; i++) {
//                String value1 = value[i];
//                System.out.println(value1);
//            }
            ret = resources.getMessage(code, value);
        } else {
//            System.out.println(key);
            ret = resources.getMessage(key);
        }

        return ret;
    }
}
