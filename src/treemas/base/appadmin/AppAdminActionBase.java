package treemas.base.appadmin;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.constant.Global;
import treemas.util.struts.ApplicationResources;
import treemas.util.struts.PageableFormBase;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Locale;

public abstract class AppAdminActionBase extends treemas.util.struts.ActionBase {
    protected HashMap pageMap = new HashMap();
    protected HashMap exceptionMap = new HashMap();
    protected ApplicationResources resources = ApplicationResources.getInstance("ApplicationResources");
    protected Locale indo = new Locale("id", "ID", "ID");

    public AppAdminActionBase() {
        pageMap.put(null, "view");
        pageMap.put(Global.WEB_TASK_LOAD, "view");
        pageMap.put(Global.WEB_TASK_SEARCH, "view");
        pageMap.put(Global.WEB_TASK_DELETE, "view");
        pageMap.put(Global.WEB_TASK_INSERT, "view");
        pageMap.put(Global.WEB_TASK_UPDATE, "view");
        pageMap.put(Global.WEB_TASK_APPROVE, "view");
        pageMap.put(Global.WEB_TASK_REJECT, "view");
        pageMap.put(Global.WEB_TASK_DOWNLOAD, "view");
        pageMap.put(Global.WEB_TASK_GENERATE, "generate");
        pageMap.put(Global.WEB_TASK_GENERATE_XLS, "genetarexls");
        pageMap.put(Global.WEB_TASK_GENERATE_PDF, "generatepdf");
        pageMap.put(Global.WEB_TASK_BACK, "view");
        pageMap.put(Global.WEB_TASK_SYNC, "view");
        pageMap.put(Global.WEB_TASK_NEXT_ADD, "nextedit");
        pageMap.put(Global.WEB_TASK_NEXT_EDIT, "nextedit");
        pageMap.put(Global.WEB_TASK_BACK_EDIT, "edit");
        pageMap.put(Global.WEB_TASK_ADD, "edit");
        pageMap.put(Global.WEB_TASK_EDIT, "edit");
        pageMap.put(Global.WEB_TASK_DETAIL, "detail");
        pageMap.put(Global.WEB_TASK_MEMBER, "member");

        exceptionMap.put(null, "view");
        exceptionMap.put(Global.WEB_TASK_LOAD, "view");
        exceptionMap.put(Global.WEB_TASK_SEARCH, "view");
        exceptionMap.put(Global.WEB_TASK_DELETE, "view");
        exceptionMap.put(Global.WEB_TASK_BACK, "view");
        exceptionMap.put(Global.WEB_TASK_INSERT, "nextedit");
        exceptionMap.put(Global.WEB_TASK_UPDATE, "nextedit");
        exceptionMap.put(Global.WEB_TASK_APPROVE, "view");
        exceptionMap.put(Global.WEB_TASK_REJECT, "view");
        exceptionMap.put(Global.WEB_TASK_DOWNLOAD, "view");
        exceptionMap.put(Global.WEB_TASK_GENERATE, "view");
        exceptionMap.put(Global.WEB_TASK_GENERATE_XLS, "view");
        exceptionMap.put(Global.WEB_TASK_GENERATE_PDF, "view");
        exceptionMap.put(Global.WEB_TASK_SYNC, "view");
        exceptionMap.put(Global.WEB_TASK_ADD, "edit");
        exceptionMap.put(Global.WEB_TASK_EDIT, "edit");
        exceptionMap.put(Global.WEB_TASK_DETAIL, "detail");
        exceptionMap.put(Global.WEB_TASK_NEXT_ADD, "nextedit");
        exceptionMap.put(Global.WEB_TASK_NEXT_EDIT, "nextedit");
        exceptionMap.put(Global.WEB_TASK_MEMBER, "member");
    }

    protected void finalize() throws Throwable {
    }

    public ActionForward getNextPage(String key,
                                     ActionMapping mapping) {
        String view = (String) pageMap.get(key);
        return mapping.findForward(view);
    }

    public ActionForward getErrorPage(String key,
                                      ActionMapping mapping) {
        String view = (String) exceptionMap.get(key);
        return mapping.findForward(view);
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        PageableFormBase pForm = (PageableFormBase) form;
        pForm.setTask(pForm.resolvePreviousTask());
        return null;
    }

    protected String getLoginId(HttpServletRequest request) {
        String result = null;
        SessionBean bean = (SessionBean) request.getSession(true).getAttribute(Global.SESSION_LOGIN);
        if (bean != null) {
            result = bean.getLoginId();
        }
        result = (result == null) ? "NOTLOGIN" : result;
        return result;
    }


    protected SessionBean getLoginBean(HttpServletRequest request) {
        SessionBean bean = (SessionBean) request.getSession(true).getAttribute(Global.SESSION_LOGIN);
        return bean;
    }


    public void setLanguage(String lang, HttpServletRequest request) {

        if (Global.LANG_ID.equals(lang)) {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, indo);
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_ID);
        } else if (Global.LANG_EN.equals(lang)) {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, Locale.getDefault());
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_EN);
        } else {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, indo);
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_ID);
        }
    }

}
