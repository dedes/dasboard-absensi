package treemas.base.appadmin;

import treemas.util.constant.Global;
import treemas.util.struts.PageableFormBase;

public class AppAdminFormBase extends PageableFormBase {

    private String height;
    private String width;
    private String title;

    public AppAdminFormBase() {
        this(Global.ROW_PER_PAGE);
    }

    public AppAdminFormBase(int pageSize) {
        super(pageSize);
        this.setTask(Global.WEB_TASK_LOAD);
    }

    protected boolean shallValidate() {
        String task = this.getTask();
        return Global.WEB_TASK_INSERT.equals(task)
                || Global.WEB_TASK_UPDATE.equals(task);
    }

    public String resolvePreviousTask() {
        String task = getTask();
        if (Global.WEB_TASK_INSERT.equals(task) || Global.WEB_TASK_NEXT_ADD.equals(task))
            return Global.WEB_TASK_ADD;
        else if (Global.WEB_TASK_UPDATE.equals(task) || Global.WEB_TASK_NEXT_EDIT.equals(task))
            return Global.WEB_TASK_EDIT;
        else
            return task;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
