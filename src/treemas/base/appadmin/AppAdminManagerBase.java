package treemas.base.appadmin;


import com.ibatis.sqlmap.client.SqlMapClient;
import treemas.util.auditrail.AuditTrailManager;
import treemas.util.ibatis.IbatisHelper;
import treemas.util.struts.ManagerBase;

public class AppAdminManagerBase extends ManagerBase {
    public static final int ERROR_HAS_CHILD = 100;
    public static final int ERROR_IN_USE = 101;

    protected SqlMapClient ibatisSqlMap = null;
    protected AuditTrailManager auditManager = new AuditTrailManager();

    public AppAdminManagerBase() {
        this.ibatisSqlMap = IbatisHelper.getSqlMap();
    }
}
