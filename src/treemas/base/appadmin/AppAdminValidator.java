package treemas.base.appadmin;

import treemas.util.validator.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class AppAdminValidator extends Validator {

    public AppAdminValidator(HttpServletRequest request) {
        super(request);
    }

    public AppAdminValidator(Locale locale) {
        super(locale);
    }

}
