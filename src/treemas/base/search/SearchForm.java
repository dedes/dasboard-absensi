package treemas.base.search;

import treemas.util.ibatis.SearchFormParameter;

public class SearchForm extends SearchFormParameter {
    private String orderField = "10";
    private String orderby = "10";
    private String[] memberList = {};

    public String getOrderField() {
        return orderField;
    }

    public void setOrderField(String orderField) {
        this.orderField = orderField;
    }

    public String getOrderby() {
        return orderby;
    }

    public void setOrderby(String orderby) {
        this.orderby = orderby;
    }

    public String[] getMemberList() {
        return memberList;
    }

    public void setMemberList(String[] memberList) {
        this.memberList = memberList;
    }

}
