package treemas.base.lookup;

import treemas.base.feature.FeatureFormBase;
import treemas.util.constant.Global;

public abstract class LookupFormBase extends FeatureFormBase {
    public LookupFormBase() {
        this.setTask(Global.WEB_TASK_SHOW);
    }
}
