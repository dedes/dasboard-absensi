package treemas.base.feature;

import treemas.util.constant.SQLConstant;
import treemas.util.ibatis.IbatisHelper;

import java.sql.SQLException;


public class SequenceManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_SEQ;

    public int getSequenceMobileAssignmentId() throws SQLException {
        return this.getSequence(this.STRING_SQL + "seqMobileAssignmentId");
    }

    public int getSequenceHistoryMobileAssignmentId() throws SQLException {
        return this.getSequence(this.STRING_SQL + "seqHistoryMobileAssignmentId");
    }

    public int getSequenceQuestionGroup() throws SQLException {
        return this.getSequence(this.STRING_SQL + "seqQuestionGroup");
    }

    public int getSequenceQuestion() throws SQLException {
        return this.getSequence(this.STRING_SQL + "seqQuestion");
    }

    public int getSequenceOptionAnswer() throws SQLException {
        return this.getSequence(this.STRING_SQL + "seqOptionAnswer");
    }

    public int getSequenceSurveyorLocationHistory() throws SQLException {
        return this.getSequence(this.STRING_SQL + "seqLocationHistory");
    }

    public int getSequencePrintItem() throws SQLException {
        return this.getSequence(this.STRING_SQL + "seqPrintItem");
    }

    public int getSequenceAttendance() throws SQLException {
        return this.getSequence(this.STRING_SQL + "seqAttendance");
    }

    public int getSequenceRelevant() throws SQLException {
        return this.getSequence(this.STRING_SQL + "seqRelevant");
    }

    private int getSequence(String ibatisId) throws SQLException {
        Integer tmp = (Integer) IbatisHelper.getSqlMap().queryForObject(ibatisId, null);
        return tmp.intValue();
    }
}
