package treemas.base.feature;

import com.ibatis.sqlmap.client.SqlMapClient;
import treemas.util.auditrail.AuditTrailManager;
import treemas.util.ibatis.IbatisHelper;
import treemas.util.struts.ManagerBase;

public class FeatureManagerBase extends ManagerBase {
    public static final int ERROR_HAS_CHILD = 100;
    public static final int ERROR_IN_USE = 101;
    public static final int ERROR_CHANGE = 10;
    public static final int ERROR_GEOFENCE = 1000;

    protected SqlMapClient ibatisSqlMap = null;
    protected AuditTrailManager auditManager = new AuditTrailManager();

    public FeatureManagerBase() {
        this.ibatisSqlMap = IbatisHelper.getSqlMap();
    }
}
