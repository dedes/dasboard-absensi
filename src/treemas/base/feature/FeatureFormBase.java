package treemas.base.feature;

import treemas.util.constant.Global;
import treemas.util.struts.PageableFormBase;

public class FeatureFormBase extends PageableFormBase {
    private String errMapping = "view";
    private String height;
    private String width;
    private String title;
    private String active = "1";
    private String[] memberList = {};

    public FeatureFormBase() {
        this(Global.ROW_PER_PAGE);
    }

    public FeatureFormBase(int pageSize) {
        super(pageSize);
        this.setTask(Global.WEB_TASK_LOAD);
    }

    protected boolean shallValidate() {
        String task = this.getTask();
        return Global.WEB_TASK_INSERT.equals(task)
                || Global.WEB_TASK_UPDATE.equals(task);
    }

    public String resolvePreviousTask() {
        String task = getTask();
        if (Global.WEB_TASK_INSERT.equals(task) || Global.WEB_TASK_NEXT_ADD.equals(task))
            return Global.WEB_TASK_ADD;
        else if (Global.WEB_TASK_UPDATE.equals(task) || Global.WEB_TASK_NEXT_EDIT.equals(task))
            return Global.WEB_TASK_EDIT;
        else
            return task;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String[] getMemberList() {
        return memberList;
    }

    public void setMemberList(String[] memberList) {
        this.memberList = memberList;
    }

    public String getErrMapping() {
        return errMapping;
    }

    public void setErrMapping(String errMapping) {
        this.errMapping = errMapping;
    }
}
