package treemas.base.feature;

import treemas.util.validator.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class FeatureValidator extends Validator {

    public FeatureValidator(HttpServletRequest request) {
        super(request);
    }

    public FeatureValidator(Locale locale) {
        super(locale);
    }

}
