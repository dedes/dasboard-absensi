package treemas.base.inquiry;

import treemas.base.feature.FeatureFormBase;
import treemas.util.constant.Global;

public abstract class InquiryFormBase extends FeatureFormBase {
    public InquiryFormBase() {
        this(Global.ROW_PER_PAGE);
    }

    public InquiryFormBase(int pageSize) {
        super(pageSize);
        this.setTask("Show");
    }

}
