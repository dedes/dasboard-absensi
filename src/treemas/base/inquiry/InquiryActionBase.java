package treemas.base.inquiry;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.feature.FeatureActionBase;
import treemas.util.constant.Global;
import treemas.util.tool.ConfigurationProperties;

import java.util.HashMap;

public abstract class InquiryActionBase extends FeatureActionBase {

    protected HashMap pageMap = new HashMap();
    protected HashMap exceptionMap = new HashMap();
    protected ConfigurationProperties resources = ConfigurationProperties
            .getInstance();

    public InquiryActionBase() {
        pageMap.put(null, "view");
        pageMap.put(Global.WEB_TASK_LOAD, "view");
        pageMap.put(Global.WEB_TASK_SEARCH, "view");
        pageMap.put(Global.WEB_TASK_PREVIEW, "preview");
        pageMap.put(Global.WEB_TASK_GENERATE, "generate");
        pageMap.put("Show", "view");

        exceptionMap.put(null, "view");
        exceptionMap.put(Global.WEB_TASK_LOAD, "view");
        exceptionMap.put(Global.WEB_TASK_SEARCH, "view");
        exceptionMap.put(Global.WEB_TASK_PREVIEW, "preview");
        exceptionMap.put(Global.WEB_TASK_GENERATE, "view");
        exceptionMap.put("Show", "view");
    }

    protected void finalize() throws Throwable {
    }

    public ActionForward getNextPage(String key, ActionMapping mapping) {
        String view = (String) pageMap.get(key);
        return mapping.findForward(view);
    }

    public ActionForward getErrorPage(String key, ActionMapping mapping) {
        String view = (String) exceptionMap.get(key);
        return mapping.findForward(view);
    }

}
