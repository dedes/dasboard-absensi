package treemas.base.core;

import treemas.util.validator.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class CoreValidator extends Validator {

    public CoreValidator(HttpServletRequest request) {
        super(request);
    }

    public CoreValidator(Locale locale) {
        super(locale);
    }
}
