package treemas.base.core;

import com.google.common.base.Strings;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.sec.Privilege;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.constant.Global;
import treemas.util.log.ILogger;
import treemas.util.struts.ActionBase;
import treemas.util.struts.ApplicationResources;
import treemas.util.struts.PageableFormBase;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;


public abstract class CoreActionBase extends ActionBase {
    protected ApplicationResources resources = ApplicationResources.getInstance("ApplicationResources_in");
    protected Locale indo = new Locale("id", "ID", "ID");

    public CoreActionBase() {
    }


    public ActionForward execute(ActionMapping mapping,
                                 ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ActionForward result = super.execute(mapping, form, request, response);
        if (result != null && form != null) {
            CoreFormBase coreForm = (CoreFormBase) form;
            coreForm.setErrMapping(result.getName());
        }
        return result;
    }


    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request, HttpServletResponse response, AppException ex) {
        PageableFormBase pForm = (PageableFormBase) form;
        pForm.setTask(pForm.resolvePreviousTask());
        int code = ex.getErrorCode();
        switch (code) {
            case CoreManagerBase.ERROR_DB:
                this.setMessage(request, "common.dberror", null);
                this.logger.printStackTrace(ex);
                break;

            default:
                this.setMessage(request, "common.error", null);
                this.logger.printStackTrace(ex);
                break;
        }
        return this.getErrorPage((CoreFormBase) form, mapping);
    }

    protected String getLoginId(HttpServletRequest request) {
        String result = null;
        SessionBean bean = this.getLoginBean(request);
        if (bean != null) {
            result = bean.getLoginId();
        }
        result = (result == null) ? "NOTLOGIN" : result;
        return result;
    }


    protected void setUserInfo(HttpServletRequest request, List list) {
        SessionBean bean = (SessionBean) request.getSession(true).getAttribute(Global.SESSION_LOGIN);
        if (null != bean) {
            bean.setHomeinfo(list);
        }
        request.getSession(true).setAttribute("Global.SESSION_LOGIN", bean);
    }

    protected SessionBean getLoginBean(HttpServletRequest request) {
        SessionBean bean = (SessionBean) request.getSession(true).getAttribute(Global.SESSION_LOGIN);
        return bean;
    }

    protected String getLang(HttpServletRequest request) {
        String lang = this.getLoginBean(request).getDwilingual();
        if (Strings.isNullOrEmpty(lang)) {
            lang = "ID";
        }
        return lang;
    }

    public ActionForward doAction(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws AppException {
        return null;
    }

    public boolean isFeatureAllowed(String featureId, String formId, String userId) {
        boolean result = false;
        try {
            result = Privilege.isAuthorizedFeature(userId, formId, featureId);
        } catch (AppException ex) {
            this.logger.printStackTrace(ex);
        }
        return result;
    }

    public ActionForward getErrorPage(CoreFormBase form, ActionMapping mapping) {
        String view = form.getErrMapping();
        if (view == null) {
            this.logger.log(ILogger.LEVEL_WARNING, "Property errMapping is null!");
        }
        return mapping.findForward(view);
    }

    public void setLanguage(String lang, HttpServletRequest request) {

        if (Global.LANG_ID.equals(lang)) {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, indo);
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_ID);
        } else if (Global.LANG_EN.equals(lang)) {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, Locale.getDefault());
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_EN);
        } else {
            request.getSession(true).setAttribute(Globals.LOCALE_KEY, indo);
            request.getSession(true).setAttribute(Global.LANG, Global.LANG_ID);
        }
    }

    public void destroySession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session != null) {
            session.removeAttribute(Global.SESSION_LOGIN);
            this.logger.log(ILogger.LEVEL_INFO, "Invalidating session: " + session.getId());
            String savedLang = (String) request.getSession().getAttribute(Global.LANG);
            session.invalidate();

            request.getSession().setAttribute(Global.LANG, savedLang);
        }
    }


}
