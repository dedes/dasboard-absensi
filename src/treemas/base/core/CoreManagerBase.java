package treemas.base.core;

import com.ibatis.sqlmap.client.SqlMapClient;
import treemas.util.auditrail.AuditTrailManager;
import treemas.util.ibatis.IbatisHelper;
import treemas.util.struts.ManagerBase;

public abstract class CoreManagerBase extends ManagerBase {
    protected SqlMapClient ibatisSqlMap = null;
    protected AuditTrailManager auditManager = new AuditTrailManager();

    public CoreManagerBase() {

        this.ibatisSqlMap = IbatisHelper.getSqlMap();

    }

}
