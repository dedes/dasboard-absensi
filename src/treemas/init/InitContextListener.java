package treemas.init;

import treemas.util.constant.PropertiesKey;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class InitContextListener implements ServletContextListener {
    public InitContextListener() {
    }

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        servletContextEvent.getServletContext().log("Start initializing web application context");
        ServletContext context = servletContextEvent.getServletContext();
        context.setAttribute(org.apache.struts.Globals.LOCALE_KEY, "id_ID");
        String initClassName = context.getInitParameter(PropertiesKey.INIT_CLASS);
        InitializerExecutor executor = new InitializerExecutor();
        try {
            executor.executeInitializer(initClassName, this.getInitParametersAsMap(context));
            servletContextEvent.getServletContext().log("Done initializing Web application context");
        } catch (Exception e) {
            e.printStackTrace();
            context.log("failed to initializing!", e);
            throw new RuntimeException(e);
//         throw new ServletException(e);
        }
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        servletContextEvent.getServletContext().log("End of Destroying Servlet Context");
    }

    private Map getInitParametersAsMap(ServletContext context) {
        Map result = new HashMap();
        Enumeration enumeration = context.getInitParameterNames();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            result.put(key, context.getInitParameter(key));
        }
        return result;
    }

}
