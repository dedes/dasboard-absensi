package treemas.init;

import treemas.util.properties.PropertiesKey;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class InitServlet extends HttpServlet {
    public InitServlet() {
    }

    public void init() throws ServletException {
        String className = this.getInitParameter(PropertiesKey.INIT_CLASS_KEY);
        InitializerExecutor executor = new InitializerExecutor();
        try {
            executor.executeInitializer(className, this.getInitParametersAsMap());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        response.sendError(HttpServletResponse.SC_NOT_FOUND);
        response.sendRedirect(PropertiesKey.ERROR_PAGE);
    }

    private Map getInitParametersAsMap() {
        Map result = new HashMap();
        Enumeration enumeration = this.getInitParameterNames();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            result.put(key, this.getInitParameter(key));
        }
        return result;
    }

}
