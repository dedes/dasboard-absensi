package treemas.init;

import java.util.Map;

public class InitializerExecutor {
    public InitializerExecutor() {
    }

    public void executeInitializer(String initializerClassName, Map parameters) throws Exception {
        try {
            Object obj = Class.forName(initializerClassName).newInstance();
            Initializer initializer = (Initializer) obj;

            this.executeInitializer(initializer, parameters);
        } catch (ClassCastException cce) {
            throw new Exception(initializerClassName + " does not implement treemas.init.Initializer interface", cce);
        } catch (InstantiationException ie) {
            throw new Exception("Cannot instantiate initializer class", ie);
        } catch (IllegalAccessException iae) {
            throw new Exception("Cannot instantiate initializer class. " +
                    "Make sure the class has public parameterless constructor", iae);
        }
    }


    private void executeInitializer(Initializer initializer, Map parameters) throws Exception {
        try {
            initializer.doInit(parameters);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

}
