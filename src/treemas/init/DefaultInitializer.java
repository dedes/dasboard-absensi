package treemas.init;

import treemas.util.log.ILogger;
import treemas.util.log.LogManager;
import treemas.util.properties.PropertiesKey;
import treemas.util.tool.ConfigurationProperties;
import treemas.util.validator.ValidatorProperties;

import java.util.Map;


public class DefaultInitializer
        implements treemas.init.Initializer {

    public DefaultInitializer() {
    }

    public void doInit(Map parameters) throws Exception {
        this.initConfig(parameters);
        this.initLog();
    }

    private void initConfig(Map parameters) throws Exception {
        String cfgFileName = (String) parameters.get(PropertiesKey.INIT_FILE_NAME);
        if (cfgFileName != null) {
            ConfigurationProperties.initFromFile(cfgFileName);
        } else {
            String cfgResourceName = (String) parameters.get(PropertiesKey.INIT_RESOURCE_NAME);
            if (cfgResourceName != null) {
                ConfigurationProperties.initFromResource(cfgResourceName);
            } else {
                ConfigurationProperties.initFromResource(PropertiesKey.CONFIGURATION_PROPERTIES);
            }
        }

        ValidatorProperties.initFromResource(PropertiesKey.VALIDATOR_PROPERTIES);
        ValidatorProperties.loadToClassStaticProps();
    }

    private void initLog() throws Exception {
        ConfigurationProperties appProps = ConfigurationProperties.getInstance();
        String name = appProps.get(PropertiesKey.LOG_FILE_FULLNAME);
        if (name == null)
            throw new Exception("Invalid/missing " + PropertiesKey.LOG_FILE_FULLNAME + " in application config properties");
        int size = 0, limit = 0;
        try {
            size = Integer.parseInt(appProps.get(PropertiesKey.LOG_FILE_SIZE));
        } catch (Exception e) {
        }
        try {
            limit = Integer.parseInt(appProps.get(PropertiesKey.LOG_FILE_COUNT));
        } catch (Exception e) {
        }

        ILogger fileLogger = LogManager.createFileLogger(LogManager.DEFAULT_LOGGER_NAME,
                name, size, limit, true);
        String lvl = appProps.get(PropertiesKey.LOG_FILE_LEVEL);
        if (lvl != null) {
            this.setLogLevel(fileLogger, lvl);
        } else
            fileLogger.setLogLevel(ILogger.LEVEL_DEBUG_HIGH);

        LogManager.setDefaultLoggerName(LogManager.DEFAULT_LOGGER_NAME);
    }

    private void setLogLevel(ILogger logger, String logLevelStr) {
        if (logger.getLogLevelString(ILogger.LEVEL_DEBUG_HIGH).equals(logLevelStr)) {
            logger.setLogLevel(ILogger.LEVEL_DEBUG_HIGH);
        } else if (logger.getLogLevelString(ILogger.LEVEL_DEBUG_LOW).equals(logLevelStr)) {
            logger.setLogLevel(ILogger.LEVEL_DEBUG_LOW);
        } else if (logger.getLogLevelString(ILogger.LEVEL_DEBUG_MEDIUM).equals(logLevelStr)) {
            logger.setLogLevel(ILogger.LEVEL_DEBUG_MEDIUM);
        } else if (logger.getLogLevelString(ILogger.LEVEL_INFO).equals(logLevelStr)) {
            logger.setLogLevel(ILogger.LEVEL_INFO);
        } else if (logger.getLogLevelString(ILogger.LEVEL_WARNING).equals(logLevelStr)) {
            logger.setLogLevel(ILogger.LEVEL_WARNING);
        } else if (logger.getLogLevelString(ILogger.LEVEL_ERROR).equals(logLevelStr)) {
            logger.setLogLevel(ILogger.LEVEL_ERROR);
        } else {
            System.out.println("Invalid log level " + logLevelStr);
        }

    }


}
