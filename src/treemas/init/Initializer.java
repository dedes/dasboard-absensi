package treemas.init;

import java.util.Map;

public interface Initializer {
    public void doInit(Map map) throws Exception;
}
