package treemas.init;

import treemas.application.login.LoginManager;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.format.TreemasFormatter;
import treemas.util.properties.PropertiesKey;

import java.util.Map;

public class AppInit implements Initializer {
    public AppInit() {
    }

    public void doInit(Map parameters) throws Exception {
        DefaultInitializer defInit = new DefaultInitializer();
        defInit.doInit(parameters);
        LoginManager loginManager = new LoginManager();
        loginManager.updateAllLogout();
        BeanConverter.setDefaultToStringDateFormat(Global.TO_STRING_DATE_FORMAT);
        BeanConverter.setDefaultToStringNumberFormat(TreemasFormatter.NFORMAT_US_2);
        BeanConverter.setDefaultFromStringDateFormat(Global.FROM_STRING_DATE_FORMAT);
        BeanConverter.setDefaultFromStringNumberFormat(TreemasFormatter.NFORMAT_PLAIN);

        Class.forName(PropertiesKey.INIT_DATE_PROPERTIES);
        Class.forName(PropertiesKey.INIT_USER_PROPERTIES);
        Class.forName(PropertiesKey.INIT_HARDWARE_PROPERTIES);
    }

}
