package treemas.application.managementhandset;

import treemas.base.ApplicationBean;

import java.io.Serializable;
import java.util.Date;

public class ManagementHandsetBean extends ApplicationBean implements Serializable {

    private String userid;
    private String branchid;
    private Integer branchlevel;
    private String branchname;
    private String fullname;
    private String active;
    private String usrupd;
    private String job_desc;
    private String job_descName;
    private Date dtmupd;
    private String handphone_no;
    private String gcmno;
    private String groupPrivilege;
    private String isapproved;
    private String password;
    private String description;

    public ManagementHandsetBean() {
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public Date getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(Date dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getJob_desc() {
        return job_desc;
    }

    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }

    public String getHandphone_no() {
        return handphone_no;
    }

    public void setHandphone_no(String handphone_no) {
        this.handphone_no = handphone_no;
    }

    public String getGcmno() {
        return gcmno;
    }

    public void setGcmno(String gcmno) {
        this.gcmno = gcmno;
    }

    public Integer getBranchlevel() {
        return branchlevel;
    }

    public void setBranchlevel(Integer branchlevel) {
        this.branchlevel = branchlevel;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getJob_descName() {
        return job_descName;
    }

    public void setJob_descName(String job_descName) {
        this.job_descName = job_descName;
    }

    public String getGroupPrivilege() {
        return groupPrivilege;
    }

    public void setGroupPrivilege(String groupPrivilege) {
        this.groupPrivilege = groupPrivilege;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
