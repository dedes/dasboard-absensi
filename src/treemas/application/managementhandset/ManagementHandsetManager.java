package treemas.application.managementhandset;

import treemas.application.managementhandset.lookup.LuManagementHandsetForm;
import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class ManagementHandsetManager extends AppAdminManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_MANAGEMENTHANDSET;

    public PageListWrapper getAllHandset(int pageNumber, ManagementHandsetSearch search) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllHandset", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllHandset", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public ManagementHandsetBean getSingleHandset(String userId) throws CoreException {
        ManagementHandsetBean result = null;
        try {
            result = (ManagementHandsetBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleHandset", userId);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public void lockHandset(LuManagementHandsetForm form, String userUpdate) throws CoreException {
        try {
            this.lock(form, userUpdate);
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
    }

    private void lock(LuManagementHandsetForm form, String userUpdate) throws CoreException {
        try {
            ManagementHandsetBean bean = new ManagementHandsetBean();
            bean.setGcmno(form.getGcmno());
            bean.setUserid(form.getUserid());
            bean.setPassword(form.getPassword());
            bean.setDescription(form.getDescription());
            bean.setUsrupd(userUpdate);

            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.update(this.STRING_SQL + "lock", bean);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
        }
    }

    public List getHistoryList(String userid) throws AppException {
        List list = null;
        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getHistoryList", userid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return list;
    }

    public void resetHandset(ManagementHandsetForm form, String userUpdate) throws CoreException {
        try {
            this.reset(form, userUpdate);
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
    }

    private void reset(ManagementHandsetForm form, String userUpdate) throws CoreException {
        try {
            ManagementHandsetBean bean = new ManagementHandsetBean();
            bean.setGcmno(form.getGcmno());
            bean.setUserid(form.getUserid());
            bean.setPassword(form.getPassword());
            bean.setDescription(form.getDescription());
            bean.setUsrupd(userUpdate);

            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.update(this.STRING_SQL + "lock", bean);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
        }
    }

}
