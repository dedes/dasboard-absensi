package treemas.application.managementhandset.lookup;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.managementhandset.ManagementHandsetManager;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.gcm.Model;
import treemas.util.gcm.PostMan;
import treemas.util.tool.ConfigurationProperties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Random;

public class LuManagementHandsetHandler extends AppAdminActionBase {

    MultipleManager multipleManager = new MultipleManager();
    ManagementHandsetManager manager = new ManagementHandsetManager();
    LuManagementHandsetValidator validator;

    public LuManagementHandsetHandler() {
        pageMap.put(Global.WEB_TASK_LOCK, "edit");

        exceptionMap.put(Global.WEB_TASK_LOCK, "edit");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        this.validator = new LuManagementHandsetValidator(request);
        LuManagementHandsetForm frm = (LuManagementHandsetForm) form;
        frm.setTitle(Global.TITLE_APP_USER);
        frm.getPaging().calculationPage();
        String task = frm.getTask();

        if (Global.WEB_TASK_LOAD.equals(task)) {
            frm.setTitle("DETAIL HISTORY");
            this.getDetailManagementHandset(request, frm);
        } else if (Global.WEB_TASK_ADD.equals(task)) {
            frm.setTitle("LOCK HANDSET");
            this.clearField(frm);
            this.generatePassword(frm);
        } else if (Global.WEB_TASK_LOCK.equals(task)) {
            if (this.validator.validateLockHandset(new ActionMessages(), frm)) {
                frm.setTitle("LOCK HANDSET");
                this.lockHandset(request, frm);
                this.lockHandsetGCM(request, frm.getGcmno(), frm.getFullname(), frm.getPassword());
                request.setAttribute("messagesave", resources.getMessage("common.process.save"));
//				this.clearField(frm);
//				this.generatePassword(frm);
            }
        }

        return this.getNextPage(task, mapping);

    }

    private void clearField(LuManagementHandsetForm frm) {
        frm.setDescription("");
    }

    private void getDetailManagementHandset(HttpServletRequest request, LuManagementHandsetForm form)
            throws AppException {

        List list = this.manager.getHistoryList(form.getUserid());
        list = BeanConverter.convertBeansToString(list, form.getClass());
        request.setAttribute("listDetail", list);
    }

    private void generatePassword(LuManagementHandsetForm frm) {
        Random r = new Random();
        int Low = 10000000;
        int High = 99999999;
        int Result = r.nextInt(High - Low) + Low;

        frm.setPassword("" + Result);
    }

    private void lockHandset(HttpServletRequest request, LuManagementHandsetForm form) throws AppException {
        this.manager.lockHandset(form, this.getLoginId(request));
        request.setAttribute("message", resources.getMessage("common.process.save"));
//		load(request, form);
    }

    private void lockHandsetGCM(HttpServletRequest request, String regId, String user, String password) {
        PostMan postMan = new PostMan();
        if (!Strings.isNullOrEmpty(regId)) {
            Model model = postMan.generateLockHandset(regId, user, this.getLoginId(request), password);
            String apiKey = ConfigurationProperties.getInstance().get(PropertiesKey.API_KEY);
            postMan.post(apiKey, model);
        }
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                LuManagementHandsetForm frm = (LuManagementHandsetForm) form;
                try {
                    this.getDetailManagementHandset(request, frm);
                } catch (AppException e) {
//				this.logger.printStackTrace(e);
                }
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((LuManagementHandsetForm) form).setTask(((LuManagementHandsetForm) form).resolvePreviousTask());
        String task = ((LuManagementHandsetForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
