package treemas.application.managementhandset.lookup;

import org.apache.struts.action.ActionMessages;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class LuManagementHandsetValidator extends Validator {
    public LuManagementHandsetValidator(HttpServletRequest request) {
        super(request);
    }

    public LuManagementHandsetValidator(Locale locale) {
        super(locale);
    }

    public boolean validateLockHandset(ActionMessages messages, LuManagementHandsetForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.textValidator(messages, form.getDescription(), "general.desc", "description", "1", this.getStringLength(100), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
