package treemas.application.managementhandset.lookup;

import treemas.base.appadmin.AppAdminFormBase;

public class LuManagementHandsetForm extends AppAdminFormBase {

    private String userid;
    private String fullname;
    private String usrupd;
    private String dtmupd;
    private String gcmno;
    private String password;
    private String description;

    public LuManagementHandsetForm() {

    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getGcmno() {
        return gcmno;
    }

    public void setGcmno(String gcmno) {
        this.gcmno = gcmno;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
