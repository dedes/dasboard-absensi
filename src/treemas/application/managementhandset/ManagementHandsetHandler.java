package treemas.application.managementhandset;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.managementhandset.lookup.LuManagementHandsetValidator;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.gcm.Model;
import treemas.util.gcm.PostMan;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.tool.ConfigurationProperties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ManagementHandsetHandler extends AppAdminActionBase {

    MultipleManager multipleManager = new MultipleManager();
    ManagementHandsetManager manager = new ManagementHandsetManager();
    LuManagementHandsetValidator validator;

    public ManagementHandsetHandler() {
        pageMap.put(Global.WEB_TASK_RESET, "view");

        exceptionMap.put(Global.WEB_TASK_RESET, "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        this.validator = new LuManagementHandsetValidator(request);
        ManagementHandsetForm frm = (ManagementHandsetForm) form;
        frm.setTitle(Global.TITLE_MANAGEMENT_HANDSET);
        frm.getPaging().calculationPage();
        String task = frm.getTask();
        frm.getSearch().setUserMaker(this.getLoginId(request));

        if (Global.WEB_TASK_SHOW.equals(task) || Global.WEB_TASK_LOAD.equals(task) || Global.WEB_TASK_SEARCH.equals(task)) {
            this.load(request, frm);
        } else if (Global.WEB_TASK_RESET.equals(task)) {
            this.resetHandset(request, frm);
            this.resetHandsetGCM(frm.getGcmno(), frm.getFullname());
            this.load(request, frm);
        }

        return this.getNextPage(task, mapping);

    }

    private void loadJobDescDropDown(HttpServletRequest request)
            throws CoreException {
        MultipleParameterBean bean = new MultipleParameterBean();
        bean.setActive(Global.STRING_TRUE);
        bean.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getListJobDesc(bean);
        request.setAttribute("listJobDesc", list);
    }

    private void loadBranchDropDown(HttpServletRequest request) throws CoreException {
        MultipleParameterBean bean = new MultipleParameterBean();
        bean.setActive(Global.STRING_TRUE);
        bean.setValueUseKey(Global.STRING_FALSE);
        bean.setUser(this.getLoginId(request));
        List list = this.multipleManager.getListBranch(bean);
        request.setAttribute("listBranch", list);
    }

    private void load(HttpServletRequest request, ManagementHandsetForm form) throws CoreException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAllHandset(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);
        request.setAttribute("loginid", this.getLoginId(request));
        request.setAttribute("loginDesc", this.getLoginBean(request).getJobDesc());

        this.loadJobDescDropDown(request);
        this.loadBranchDropDown(request);

    }

    private void loadSingle(ManagementHandsetForm form) throws CoreException {
        ManagementHandsetBean bean = new ManagementHandsetBean();
        bean = this.manager.getSingleHandset(form.getUserid());
        BeanConverter.convertBeanToString(bean, form);

    }

    private void resetHandsetGCM(String regId, String user) {
        PostMan postMan = new PostMan();
        if (!Strings.isNullOrEmpty(regId)) {
            Model model = postMan.generateResetHandset(regId, user);
            String apiKey = ConfigurationProperties.getInstance().get(PropertiesKey.API_KEY);
            postMan.post(apiKey, model);
        }
    }

    private void resetHandset(HttpServletRequest request, ManagementHandsetForm form) throws AppException {
        form.setPassword("1");
        form.setDescription("Reset Handset");
        this.manager.resetHandset(form, this.getLoginId(request));
        request.setAttribute("message", resources.getMessage("common.process.save"));
//		load(request, form);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                ManagementHandsetForm frm = (ManagementHandsetForm) form;
                try {
                    this.loadJobDescDropDown(request);
                    this.load(request, frm);
                } catch (AppException e) {
//				this.logger.printStackTrace(e);
                }
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((ManagementHandsetForm) form).setTask(((ManagementHandsetForm) form).resolvePreviousTask());
        String task = ((ManagementHandsetForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
