package treemas.application.managementhandset;

import treemas.base.appadmin.AppAdminFormBase;

public class ManagementHandsetForm extends AppAdminFormBase {

    private String userid;
    private String branchid;
    private String branchlevel;
    private String branchname;
    private String fullname;
    private String active = "0";
    private String usrupd;
    private String job_desc;
    private String job_descName;
    private String dtmupd;
    private String handphone_no;
    private String gcmno;
    private String groupPrivilege;
    private String isapproved;
    private String password;
    private String description;

    private ManagementHandsetSearch search = new ManagementHandsetSearch();

    public ManagementHandsetForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public String getBranchlevel() {
        return branchlevel;
    }

    public void setBranchlevel(String branchlevel) {
        this.branchlevel = branchlevel;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getJob_desc() {
        return job_desc;
    }

    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }

    public String getJob_descName() {
        return job_descName;
    }

    public void setJob_descName(String job_descName) {
        this.job_descName = job_descName;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getHandphone_no() {
        return handphone_no;
    }

    public void setHandphone_no(String handphone_no) {
        this.handphone_no = handphone_no;
    }

    public String getGcmno() {
        return gcmno;
    }

    public void setGcmno(String gcmno) {
        this.gcmno = gcmno;
    }

    public String getGroupPrivilege() {
        return groupPrivilege;
    }

    public void setGroupPrivilege(String groupPrivilege) {
        this.groupPrivilege = groupPrivilege;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public ManagementHandsetSearch getSearch() {
        return search;
    }

    public void setSearch(ManagementHandsetSearch search) {
        this.search = search;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
