package treemas.application.appadmin;

import treemas.base.core.CoreManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class AppadminManager extends CoreManagerBase {

    private final String STRING_SQL = SQLConstant.SQL_APPADMIN;

    public PageListWrapper getAllAppAdministration(int pageNumber, AppadminSearch search) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(STRING_SQL + "getAllAppAdministration", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(STRING_SQL + "countAllUser", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public AppadminBean getSingleAppAdministration(String appid) throws CoreException {
        AppadminBean result = null;
        try {
            result = (AppadminBean) this.ibatisSqlMap.queryForObject(STRING_SQL + "getSingleAdministration", appid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public void insert(AppadminBean bean) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.insert(STRING_SQL + "insertAppAdministration", bean);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }
    }

    public void update(AppadminBean bean) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.update(STRING_SQL + "updateAppAdministration", bean);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }
    }

}
