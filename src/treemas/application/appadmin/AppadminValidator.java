package treemas.application.appadmin;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class AppadminValidator extends Validator {

    public AppadminValidator(HttpServletRequest request) {
        super(request);
    }

    public AppadminValidator(Locale locale) {
        super(locale);
    }

    public boolean validateAppadminAdd(ActionMessages messages, AppadminForm form, AppadminManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        try {
            AppadminBean bean = manager.getSingleAppAdministration(form.getAppid());
            if (null != bean) {
                messages.add("appid", new ActionMessage("errors.duplicate", "APPLICATION ID"));
            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }

        this.textValidator(messages, form.getAppid(), "lookup.privilege.appid", "appid", "1", this.getStringLength(10), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getAppname(), "lookup.privilege.appname", "appname", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getVersion(), "app.administration.version", "version", "1", this.getStringLength(10), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT);
        this.textValidator(messages, form.getNumberofserialdb(), "app.administration.numberofserialdb", "numberofserialdb", "1", this.getStringLength(38), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getCaptionforgroupdb(), "app.administration.captionforgroupdb", "captionforgroupdb", "1", this.getStringLength(38), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }

    public boolean validateAppadminEdit(ActionMessages messages, AppadminForm form, AppadminManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.textValidator(messages, form.getAppname(), "lookup.privilege.appname", "appname", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getVersion(), "app.administration.version", "version", "1", this.getStringLength(10), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT);
        this.textValidator(messages, form.getNumberofserialdb(), "app.administration.numberofserialdb", "numberofserialdb", "1", this.getStringLength(38), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getCaptionforgroupdb(), "app.administration.captionforgroupdb", "captionforgroupdb", "1", this.getStringLength(38), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
