package treemas.application.appadmin;

import org.apache.struts.upload.FormFile;
import treemas.base.core.CoreFormBase;

public class AppadminForm extends CoreFormBase {

    private String appid;
    private String appname;
    private String version;
    private String description;
    private String icon;
    private String sn;
    private String usrupd;
    private String virdir;
    private String numberofserialdb;
    private String captionforgroupdb;
    private String active = "0";
    private FormFile iconupload;

    private AppadminSearch search = new AppadminSearch();


    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getVirdir() {
        return virdir;
    }

    public void setVirdir(String virdir) {
        this.virdir = virdir;
    }

    public String getNumberofserialdb() {
        return numberofserialdb;
    }

    public void setNumberofserialdb(String numberofserialdb) {
        this.numberofserialdb = numberofserialdb;
    }

    public String getCaptionforgroupdb() {
        return captionforgroupdb;
    }

    public void setCaptionforgroupdb(String captionforgroupdb) {
        this.captionforgroupdb = captionforgroupdb;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public FormFile getIconupload() {
        return iconupload;
    }

    public void setIconupload(FormFile iconupload) {
        this.iconupload = iconupload;
    }

    public AppadminSearch getSearch() {
        return search;
    }

    public void setSearch(AppadminSearch search) {
        this.search = search;
    }


}
