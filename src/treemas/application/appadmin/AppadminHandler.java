package treemas.application.appadmin;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.upload.FormFile;
import treemas.base.core.CoreActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class AppadminHandler extends CoreActionBase {
    AppadminManager manager = new AppadminManager();
    AppadminValidator validator;

    @Override
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        validator = new AppadminValidator(request);
        AppadminForm frm = (AppadminForm) form;
        frm.setTitle(Global.TITLE_APP_ADMIN);
        String task = frm.getTask();
        frm.getPaging().calculationPage();
        if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)
                || Global.WEB_TASK_SHOW.equals(task)) {
            frm.setTitle(Global.TITLE_APP_ADMIN);
            this.load(request, frm);
            return mapping.findForward(Global.WEB_TASK_VIEW.toLowerCase());
        } else if (Global.WEB_TASK_ADD.equals(task)) {
            frm.setTitle(Global.TITLE_APP_ADMIN);
            this.clear(frm);
            return mapping.findForward(Global.WEB_TASK_EDIT.toLowerCase());
        } else if (Global.WEB_TASK_EDIT.equals(task)) {
            frm.setTitle(Global.TITLE_APP_ADMIN);
            this.loadSingle(frm);
            return mapping.findForward(Global.WEB_TASK_EDIT.toLowerCase());
        } else if (Global.WEB_TASK_INSERT.equals(task)) {
            if (this.validator.validateAppadminAdd(new ActionMessages(), frm, this.manager)) {
                AppadminBean adminBean = new AppadminBean();
                this.imageUpload(adminBean, frm, request);
                this.manager.insert(adminBean);
            }
            this.load(request, frm);
            return mapping.findForward(Global.WEB_TASK_VIEW.toLowerCase());
        } else if (Global.WEB_TASK_UPDATE.equals(task)) {
            if (this.validator.validateAppadminEdit(new ActionMessages(), frm, this.manager)) {
                AppadminBean adminBean = new AppadminBean();
                this.imageUpload(adminBean, frm, request);
                this.manager.update(adminBean);
            }
            this.load(request, frm);
            return mapping.findForward(Global.WEB_TASK_VIEW.toLowerCase());
        }
        return null;
    }

    private void load(HttpServletRequest request, AppadminForm form) throws AppException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;

        PageListWrapper result = this.manager.getAllAppAdministration(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);
    }

    private void loadSingle(AppadminForm form) throws CoreException {
        AppadminBean bean = new AppadminBean();
        bean = this.manager.getSingleAppAdministration(form.getAppid());
        BeanConverter.convertBeanToString(bean, form);
    }

    private void clear(AppadminForm form) throws CoreException {
        form.setAppid("");
        form.setAppname("");
        form.setActive("1");
        form.setDescription("");
        form.setVirdir("");
        form.setVersion("");
        form.setNumberofserialdb("");
        form.setCaptionforgroupdb("");
    }

    private void imageUpload(AppadminBean bean, AppadminForm frm, HttpServletRequest request) {
        try {
            FormFile file = frm.getIconupload();
            String fileName = "";
            Properties prop = new Properties();
            String propFileName = "configuration.properties";
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            prop.load(inputStream);
            if (file.getFileSize() > 0) {
                fileName = frm.getIconupload().getFileName();
//				   String filePath = request.getSession().getServletContext().getRealPath("images\\icon");
                String filePath = prop.getProperty("path.icon");
                File newFile = new File(filePath, fileName);
                if (newFile.exists()) {
                    newFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(newFile);
                fos.write(file.getFileData());
                fos.flush();
                fos.close();

                bean.setIcon(fileName);
            }
            frm.setUsrupd(this.getLoginId(request));
            if (Strings.isNullOrEmpty(frm.getCaptionforgroupdb())) {
                bean.setCaptionforgroupdb(null);
            } else {
                bean.setCaptionforgroupdb(Integer.parseInt(frm.getCaptionforgroupdb()));
            }
            if (Strings.isNullOrEmpty(frm.getNumberofserialdb())) {
                bean.setNumberofserialdb(null);
            } else {
                bean.setNumberofserialdb(Integer.parseInt(frm.getNumberofserialdb()));
            }
            bean.setAppid(frm.getAppid());
            bean.setAppname(frm.getAppname());
            bean.setDescription(frm.getDescription());
            bean.setActive(frm.getActive());
            bean.setVersion(frm.getVersion());
            bean.setVirdir(frm.getVirdir());
            bean.setSn(frm.getSn());
            if (fileName.equalsIgnoreCase("")) {
                bean.setIcon(frm.getIcon());
            }
            bean.setUsrupd(this.getLoginId(request));
        } catch (IOException e) {
            this.logger.printStackTrace(e);
        }
    }

    public Properties openProperties(String namaFile) throws IOException {
        Properties prop = new Properties();
        String propFileName = namaFile;
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        prop.load(inputStream);
        return prop;
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((AppadminForm) form).setTask(((AppadminForm) form).resolvePreviousTask());
        return mapping.findForward(Global.WEB_TASK_EDIT.toLowerCase());
    }

}
