package treemas.application.appadmin;

import java.io.Serializable;

public class AppadminBean implements Serializable {

    private String appid;
    private String appname;
    private String version;
    private String description;
    private String icon;
    private String sn;
    private String usrupd;
    private String virdir;
    private Integer numberofserialdb;
    private Integer captionforgroupdb;
    private String active;

    public AppadminBean() {
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getVirdir() {
        return virdir;
    }

    public void setVirdir(String virdir) {
        this.virdir = virdir;
    }

    public Integer getNumberofserialdb() {
        return numberofserialdb;
    }

    public void setNumberofserialdb(Integer numberofserialdb) {
        this.numberofserialdb = numberofserialdb;
    }

    public Integer getCaptionforgroupdb() {
        return captionforgroupdb;
    }

    public void setCaptionforgroupdb(Integer captionforgroupdb) {
        this.captionforgroupdb = captionforgroupdb;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
