package treemas.application.report;

import treemas.application.audit.AuditSearch;
import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.List;

public class ReportManager extends AppAdminManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_REPORT;

    public List getAllAuditReport(String tableId, String action, String transactionDate, String transactionDateAfter, String dateKey) throws AppException {
        AuditSearch search = new AuditSearch();
        search.setTableId(tableId);
        search.setAction(action);
        search.setTransactionDate(transactionDate);
        search.setTransactionDateAfter(transactionDateAfter);
        search.setDateKey(dateKey);

        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAuditReport", search);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new AppException(sqle, ERROR_DB);
        }
        return result;
    }

}
