package treemas.application.approval;

import treemas.base.search.SearchForm;


public class ApprovalSearch extends SearchForm {
    private String id;
    private String usrupd;
    private String dtmupd;

    public ApprovalSearch() {
        this.usrupd = null;
        this.dtmupd = null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }

}
