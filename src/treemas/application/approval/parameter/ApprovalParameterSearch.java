package treemas.application.approval.parameter;

import treemas.application.approval.ApprovalSearch;

public class ApprovalParameterSearch extends ApprovalSearch {
    private String data_type;
    //	private String usrupd;
//	private String dtmupd;
    private String usrapp;
    private String dtmapp;
    private String isapprove;

    public ApprovalParameterSearch() {
        super();
        this.usrapp = null;
        this.dtmapp = null;
        this.isapprove = "";
//		this.usrupd=null;
//		this.dtmupd=null;
    }

//	public String getUsrupd() {
//		return usrupd;
//	}
//
//	public void setUsrupd(String usrupd) {
//		this.usrupd = usrupd;
//	}
//
//	public String getDtmupd() {
//		return dtmupd;
//	}
//
//	public void setDtmupd(String dtmupd) {
//		this.dtmupd = dtmupd;
//	}

    public String getUsrapp() {
        return usrapp;
    }

    public void setUsrapp(String usrapp) {
        this.usrapp = usrapp;
    }

    public String getDtmapp() {
        return dtmapp;
    }

    public void setDtmapp(String dtmapp) {
        this.dtmapp = dtmapp;
    }

    public String getIsapprove() {
        return isapprove;
    }

    public void setIsapprove(String isapprove) {
        this.isapprove = isapprove;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }


}
