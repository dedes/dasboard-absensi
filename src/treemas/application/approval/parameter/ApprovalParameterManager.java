package treemas.application.approval.parameter;

import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApprovalParameterManager extends SetUpManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_GENERAL_PARAMETER;

    public List getAllWaitingApproval(ApprovalParameterSearch search) throws AppException {
        List result = null;

        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAllWaiting", search);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }


    public ApprovalParameterBean getDetailApproval(String id) throws AppException {
        ApprovalParameterBean result = null;

        try {
            result = (ApprovalParameterBean) this.ibatisSqlMap.queryForObject(STRING_SQL + "getDetailParam", id);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public int approval(int approvalType, String userId, String parameterid) throws AppException {
        int result = 0;
        try {
            Map approvalParameter = new HashMap();
            approvalParameter.put("approvalType", approvalType);
            approvalParameter.put("user", userId);
            approvalParameter.put("parameterid", parameterid);
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.update(STRING_SQL + "approval", approvalParameter);
                Integer resultCode = (Integer) approvalParameter.get("resultCode");
                result = resultCode.intValue();
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }
}
