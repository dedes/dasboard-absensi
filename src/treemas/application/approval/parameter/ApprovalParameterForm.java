package treemas.application.approval.parameter;

import treemas.base.setup.SetUpFormBase;

public class ApprovalParameterForm extends SetUpFormBase {

    private String id;
    private String paramdesc;
    private String data_type;
    private String val;
    private String isvisible;
    private String newparamdesc;
    private String newdata_type;
    private String newval;
    private String newisvisible;
    private String old_paramdesc;
    private String old_data_type;
    private String old_val;
    private String old_isvisible;
    private String isApprove;
    private String usrupd;
    private String dtmupd;
    private String usrapp;
    private String dtmapp;
    private ApprovalParameterSearch search = new ApprovalParameterSearch();

    public ApprovalParameterForm() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNewparamdesc() {
        return newparamdesc;
    }

    public void setNewparamdesc(String newparamdesc) {
        this.newparamdesc = newparamdesc;
    }

    public String getNewdata_type() {
        return newdata_type;
    }

    public void setNewdata_type(String newdata_type) {
        this.newdata_type = newdata_type;
    }

    public String getNewval() {
        return newval;
    }

    public void setNewval(String newval) {
        this.newval = newval;
    }

    public String getNewisvisible() {
        return newisvisible;
    }

    public void setNewisvisible(String newisvisible) {
        this.newisvisible = newisvisible;
    }

    public String getOld_paramdesc() {
        return old_paramdesc;
    }

    public void setOld_paramdesc(String old_paramdesc) {
        this.old_paramdesc = old_paramdesc;
    }

    public String getOld_data_type() {
        return old_data_type;
    }

    public void setOld_data_type(String old_data_type) {
        this.old_data_type = old_data_type;
    }

    public String getOld_val() {
        return old_val;
    }

    public void setOld_val(String old_val) {
        this.old_val = old_val;
    }

    public String getOld_isvisible() {
        return old_isvisible;
    }

    public void setOld_isvisible(String old_isvisible) {
        this.old_isvisible = old_isvisible;
    }

    public String getIsApprove() {
        return isApprove;
    }

    public void setIsApprove(String isApprove) {
        this.isApprove = isApprove;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getUsrapp() {
        return usrapp;
    }

    public void setUsrapp(String usrapp) {
        this.usrapp = usrapp;
    }

    public String getDtmapp() {
        return dtmapp;
    }

    public void setDtmapp(String dtmapp) {
        this.dtmapp = dtmapp;
    }

    public ApprovalParameterSearch getSearch() {
        return search;
    }

    public void setSearch(ApprovalParameterSearch search) {
        this.search = search;
    }

    public String getParamdesc() {
        return paramdesc;
    }

    public void setParamdesc(String paramdesc) {
        this.paramdesc = paramdesc;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getIsvisible() {
        return isvisible;
    }

    public void setIsvisible(String isvisible) {
        this.isvisible = isvisible;
    }


}
