package treemas.application.approval.parameter;

import java.io.Serializable;
import java.util.Date;

public class ApprovalParameterBean implements Serializable {

    private String id;
    private String newparamdesc;
    private String newdata_type;
    private String newval;
    private String newisvisible;
    private String old_paramdesc;
    private String old_data_type;
    private String old_val;
    private String old_isvisible;
    private String isApprove;
    private String usrupd;
    private Date dtmupd;
    private String usrapp;
    private Date dtmapp;

    public ApprovalParameterBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNewparamdesc() {
        return newparamdesc;
    }

    public void setNewparamdesc(String newparamdesc) {
        this.newparamdesc = newparamdesc;
    }

    public String getNewdata_type() {
        return newdata_type;
    }

    public void setNewdata_type(String newdata_type) {
        this.newdata_type = newdata_type;
    }

    public String getNewval() {
        return newval;
    }

    public void setNewval(String newval) {
        this.newval = newval;
    }

    public String getNewisvisible() {
        return newisvisible;
    }

    public void setNewisvisible(String newisvisible) {
        this.newisvisible = newisvisible;
    }

    public String getOld_paramdesc() {
        return old_paramdesc;
    }

    public void setOld_paramdesc(String old_paramdesc) {
        this.old_paramdesc = old_paramdesc;
    }

    public String getOld_data_type() {
        return old_data_type;
    }

    public void setOld_data_type(String old_data_type) {
        this.old_data_type = old_data_type;
    }

    public String getOld_val() {
        return old_val;
    }

    public void setOld_val(String old_val) {
        this.old_val = old_val;
    }

    public String getOld_isvisible() {
        return old_isvisible;
    }

    public void setOld_isvisible(String old_isvisible) {
        this.old_isvisible = old_isvisible;
    }

    public String getIsApprove() {
        return isApprove;
    }

    public void setIsApprove(String isApprove) {
        this.isApprove = isApprove;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public Date getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(Date dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getUsrapp() {
        return usrapp;
    }

    public void setUsrapp(String usrapp) {
        this.usrapp = usrapp;
    }

    public Date getDtmapp() {
        return dtmapp;
    }

    public void setDtmapp(Date dtmapp) {
        this.dtmapp = dtmapp;
    }

}
