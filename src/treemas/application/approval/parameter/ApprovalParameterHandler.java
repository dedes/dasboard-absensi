package treemas.application.approval.parameter;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.base.setup.SetUpActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ApprovalParameterHandler extends SetUpActionBase {

    ApprovalParameterManager manager = new ApprovalParameterManager();
    ApprovalParameterValidator validator;

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        this.validator = new ApprovalParameterValidator(request);
        ApprovalParameterForm frm = (ApprovalParameterForm) form;
        String task = frm.getTask();

        if (Global.WEB_TASK_LOAD.equals(task) || Global.WEB_TASK_SEARCH.equals(task)) {
            this.load(request, frm);
        } else if (Global.WEB_TASK_APPROVE.equals(task)) {
            this.approval(request, frm, 1);
        } else if (Global.WEB_TASK_REJECT.equals(task)) {
            this.approval(request, frm, -1);
        }

        return this.getNextPage(task, mapping);

    }

    private void load(HttpServletRequest request, ApprovalParameterForm form)
            throws AppException {
        List list = this.manager.getAllWaitingApproval(form.getSearch());
        request.setAttribute("list", BeanConverter.convertBeansToString(list, form.getClass()));
    }

    private ApprovalParameterBean getDetailApproval(ApprovalParameterForm form)
            throws AppException {
        ApprovalParameterBean bean = this.manager.getDetailApproval(form.getId());
        return bean;
    }

    private void approval(HttpServletRequest request, ApprovalParameterForm form, int value)
            throws AppException {
        try {
            String[] member = form.getSearch().getMemberList();
            for (int i = 0; i < member.length; i++) {
                this.manager.approval(value, this.getLoginId(request), member[i]);
            }
            if (1 == value)
                request.setAttribute("message", resources.getMessage("common.approve", member.length + " Data"));
            else if (-1 == value)
                request.setAttribute("message", resources.getMessage("common.reject", member.length + "Data "));
            else
                request.setAttribute("message", resources.getMessage("common.invalid.approval"));
            load(request, form);
        } catch (CoreException e) {
            throw e;
        }
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((ApprovalParameterForm) form).setTask(((ApprovalParameterForm) form).resolvePreviousTask());

        String task = ((ApprovalParameterForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
