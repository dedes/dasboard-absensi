package treemas.application.approval.parameter;

import org.apache.struts.action.ActionMessages;
import treemas.util.CoreException;
import treemas.util.validator.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class ApprovalParameterValidator extends Validator {

    public ApprovalParameterValidator(HttpServletRequest request) {
        super(request);
    }

    public ApprovalParameterValidator(Locale locale) {
        super(locale);
    }

    public boolean validateParameterEdit(ActionMessages messages, ApprovalParameterForm form) throws CoreException {
        boolean ret = true;
//		if (messages == null) 
//			messages = new ActionMessages();
//		
//		this.textValidator(messages, form.getId(), "general.id","id", "1", this.getStringLength(10), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
//		
//		this.textValidator(messages, form.getParamdesc(), "general.keterangan", "paramdesc", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);		
//		
//		if(form.getData_type().equalsIgnoreCase("0")){
//			messages.add("data_type", new ActionMessage("common.harusPilih", "Data Type"));
//		}
//	    else if (form.getData_type().equalsIgnoreCase("N")) {
//	    	if(!form.getVal().matches("^[0-9]*$")) {
//		    	messages.add("val", new ActionMessage("errors.integer2.bulat", "Value"));
//		    	}
//	    	if(form.getVal().isEmpty()) {	
//	    		messages.add("val", new ActionMessage("errors.required", "Value"));
//	    	}
//		} else if (form.getData_type().equalsIgnoreCase("C")) {
//			this.textValidator(messages, form.getVal(), "general.nilai", "val", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);		
//
//		} else if (form.getData_type().equalsIgnoreCase("D")) {
//			dateValidator(messages,  form.getVal(), "general.nilai", "val", ValidatorGlobal.TM_NOT_CHECKED_LENGTH,ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_DATE_DMY);
//		}
//		if (messages != null && !messages.isEmpty()) 
//			throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
//		
        return ret;
    }
}
