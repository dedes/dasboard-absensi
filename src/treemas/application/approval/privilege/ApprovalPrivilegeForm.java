package treemas.application.approval.privilege;

import treemas.base.appadmin.AppAdminFormBase;

public class ApprovalPrivilegeForm extends AppAdminFormBase {
    private String groupid;
    private String appid;
    private String groupname;
    private String appname;
    private String isapproved;
    private String usrupd;
    private String dtmupd;
    private String usract;
    private String oldcount;
    private String newcount;

    private ApprovalPrivilegeSearch search = new ApprovalPrivilegeSearch();

    public ApprovalPrivilegeForm() {
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getUsract() {
        return usract;
    }

    public void setUsract(String usract) {
        this.usract = usract;
    }

    public String getOldcount() {
        return oldcount;
    }

    public void setOldcount(String oldcount) {
        this.oldcount = oldcount;
    }

    public String getNewcount() {
        return newcount;
    }

    public void setNewcount(String newcount) {
        this.newcount = newcount;
    }

    public ApprovalPrivilegeSearch getSearch() {
        return search;
    }

    public void setSearch(ApprovalPrivilegeSearch search) {
        this.search = search;
    }


}
