package treemas.application.approval.privilege;

import treemas.application.privilege.PrivilegeBean;
import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.auditrail.AuditTrailManager;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApprovalPrivilegeManager extends AppAdminManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_PRIVILEGE;
    private AuditTrailManager auditManager = new AuditTrailManager();

    public PageListWrapper getAllPrivilege(int pageNumber, ApprovalPrivilegeSearch search) throws AppException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllMenuUnapprove", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllMenuUnapprove", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public PrivilegeBean getDetailApproval(String groupid) throws AppException {
        PrivilegeBean bean = new PrivilegeBean();

        try {
            bean = (PrivilegeBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSinglePrivilege", groupid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return bean;
    }

    public String getAppId(String groupid) throws AppException {
        String result = null;

        try {
            result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getAppId", groupid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public Integer approval(Integer flag, String userid, String groupid) throws AppException {
        Integer result = 0;
        Map paramApproval = new HashMap();
        try {
            paramApproval.put("flagCode", flag);
            paramApproval.put("userid", userid);
            paramApproval.put("groupid", groupid);

            try {
                this.ibatisSqlMap.startTransaction();

                this.ibatisSqlMap.update(STRING_SQL + "approval", paramApproval);

                this.ibatisSqlMap.commitTransaction();
                result = (Integer) paramApproval.get("resultCode");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqe) {
            throw new CoreException(sqe, ERROR_DB);
        }

        return result;
    }

//	public List getAllMenuPrivilege(String applicationId)  throws AppException {
//		List list = null;
//		
//		try{
//			list = this.ibatisSqlMap.queryForList(this.STRING_SQL+"getAllMenuApplication",applicationId);
//		} catch (SQLException e) {
//			this.logger.printStackTrace(e);
//			throw new AppException(ERROR_DB);
//		}
//		
//		return list;
//	}
//	
//	public String getFolderRoot(String applicationId) throws AppException{
//		String result = null;
//		try{
//			result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL+"getFolderRoot",applicationId);
//		}catch (SQLException e) {
//			this.logger.printStackTrace(e);
//			throw new AppException(ERROR_DB);
//		}
//		return result;
//	}
//	
//	public String[]  getAllMenuId(String appid, String groupid) throws AppException{
//		String[]  result = null;
//		Map param = new HashMap();
//		param.put("groupid", groupid);
//		param.put("appid", appid);
//		try{
//			List list = this.ibatisSqlMap.queryForList(this.STRING_SQL+"getListMenuId",param);
//			result = new String[list.size()];
//			list.toArray(result);
//		}catch (SQLException e) {
//			this.logger.printStackTrace(e);
//			throw new AppException(ERROR_DB);
//		}
//		return result;
//	}
//	
//	/*
//	 * INSERT DATA TO PRIVILEGEGROUP WAITING FOR APPROVAL
//	 * */
//	public void insertNewPrivilege(PrivilegeForm form, String userUpdate) throws AppException{
//		PrivilegeBean bean = new PrivilegeBean();
//		BeanConverter.convertBeanFromString(form, bean);
//		bean.setUsrupd(userUpdate);
//		bean.setActive("1");
//		bean.setIsapproved("0");
//		
//		try {
//	        this.ibatisSqlMap.insert(this.STRING_SQL+"insertPrivilegeNew",bean);
//	        this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), bean, userUpdate, Global.ACTION_ADD_NEW_PRIVILEGE);
//			
//		} catch (SQLException e) {
//			this.logger.printStackTrace(e);
//			throw new AppException(ERROR_DB);
//		}
//		
//	}
//
//	/*
//	 * INSERT DATA TO PRIVILEGEMENU WAITING FOR APPROVAL
//	 * */
//	public void insertMenuPrivilege(PrivilegeForm form, String userUpdate)throws AppException{
//		String groupid = form.getGroupid();
//		String appid = form.getAppid();
//		String[] menuId = form.getMenuidlist();
//		String updatemenu = "1";
//		try{
//			try{
//				this.ibatisSqlMap.startTransaction();
//				this.ibatisSqlMap.startBatch ();
//				for (int i = 0; i < menuId.length; i++) {
//					PrivilegeMenuBean bean = new PrivilegeMenuBean();
//					bean.setMenu(groupid, appid, userUpdate, updatemenu);
//					bean.setMenuid(menuId[i]);
//					this.ibatisSqlMap.insert(this.STRING_SQL+"insertMenuNew", bean);
//				}
//				this.ibatisSqlMap.executeBatch();
//				this.ibatisSqlMap.commitTransaction();
//				
//			}finally{
//					this.ibatisSqlMap.endTransaction();
//			}
//			
//			for (int i = 0; i < menuId.length; i++) {
//				PrivilegeMenuBean bean = new PrivilegeMenuBean();
//				bean.setMenu(groupid, appid, userUpdate, updatemenu);
//				bean.setMenuid(menuId[i]);
//				this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), bean, userUpdate, Global.ACTION_ADD_NEW_MENU_PRIVILEGE);
//			}
//		} catch (SQLException e) {
//			this.logger.printStackTrace(e);
//			throw new AppException(ERROR_DB);
//		}
//	}
//	
//	public void updateExistingPrivilege(PrivilegeForm form)throws AppException{
//		try {
//	        this.ibatisSqlMap.update(this.STRING_SQL+"updateFlag",form.getGroupid());
//		} catch (SQLException e) {
//			this.logger.printStackTrace(e);
//			throw new AppException(ERROR_DB);
//		}
//	}
}
