package treemas.application.approval.privilege;

import java.io.Serializable;
import java.util.Date;

public class ApprovalPrivilegeBean implements Serializable {
    private String groupid;
    private String appid;
    private String groupname;
    private String appname;
    private String isapproved;
    private String usrupd;
    private Date dtmupd;
    private String usract;
    private Integer oldcount;
    private Integer newcount;

    public ApprovalPrivilegeBean() {
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public Date getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(Date dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getUsract() {
        return usract;
    }

    public void setUsract(String usract) {
        this.usract = usract;
    }

    public Integer getOldcount() {
        return oldcount;
    }

    public void setOldcount(Integer oldcount) {
        this.oldcount = oldcount;
    }

    public Integer getNewcount() {
        return newcount;
    }

    public void setNewcount(Integer newcount) {
        this.newcount = newcount;
    }


}
