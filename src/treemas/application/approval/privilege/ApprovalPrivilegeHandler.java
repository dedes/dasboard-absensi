package treemas.application.approval.privilege;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.privilege.menu.PrivilegeMenuManager;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ApprovalPrivilegeHandler extends AppAdminActionBase {

    ApprovalPrivilegeManager manager = new ApprovalPrivilegeManager();
    PrivilegeMenuManager menumanager = new PrivilegeMenuManager();
    MultipleManager multipleManager = new MultipleManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        ApprovalPrivilegeForm frm = (ApprovalPrivilegeForm) form;
        String task = frm.getTask();
        int page = frm.getPaging().getCurrentPageNo();
        frm.getPaging().calculationPage();

        this.loadAppAdmin(request);
        this.loadAppGroup(request);

        if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)
                || Global.WEB_TASK_SHOW.equals(task)) {
            this.load(request, frm);
        } else if (Global.WEB_TASK_DETAIL.equals(task)) {
        } else if (Global.WEB_TASK_APPROVE.equals(task)) {
            this.approval(request, frm, 1);
        } else if (Global.WEB_TASK_REJECT.equals(task)) {
            this.approval(request, frm, -1);
        }

        return this.getNextPage(task, mapping);

    }

    private void approval(HttpServletRequest request, ApprovalPrivilegeForm form, int flag) throws AppException {
        String[] member = form.getSearch().getMemberList();
        for (int i = 0; i < member.length; i++) {
            this.manager.approval(flag, this.getLoginId(request), member[i]);
        }
        if (1 == flag)
            request.setAttribute("message", resources.getMessage("common.approve", member.length + " Data"));
        else if (-1 == flag)
            request.setAttribute("message", resources.getMessage("common.reject", member.length + "Data "));
        else
            request.setAttribute("message", resources.getMessage("common.invalid.approval"));

        this.load(request, form);
    }

    private void loadAppAdmin(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getAppAdmin(param);
        request.setAttribute("listAppAdmin", list);
    }

    private void loadAppGroup(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getAppGroup(param);
        request.setAttribute("listAppGroup", list);
    }

    private void load(HttpServletRequest request, ApprovalPrivilegeForm form) throws AppException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;

        PageListWrapper result = this.manager.getAllPrivilege(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);

        this.loadAppAdmin(request);
        this.loadAppGroup(request);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            case ErrorCode.ERR_WS_ERROR:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((ApprovalPrivilegeForm) form).setTask(((ApprovalPrivilegeForm) form).resolvePreviousTask());

        String task = ((ApprovalPrivilegeForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
