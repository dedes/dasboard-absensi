package treemas.application.approval.privilege;

import treemas.application.approval.ApprovalSearch;

public class ApprovalPrivilegeSearch extends ApprovalSearch {

    private String groupid;
    private String appid;
    private String groupname;
    private String appname;

    public ApprovalPrivilegeSearch() {
        super();
        this.groupid = null;
        this.appid = null;
        this.groupname = null;
        this.appname = null;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }


}
