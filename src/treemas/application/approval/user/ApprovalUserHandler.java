package treemas.application.approval.user;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ApprovalUserHandler extends AppAdminActionBase {

    ApprovalUserManager manager = new ApprovalUserManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        ApprovalUserForm frm = (ApprovalUserForm) form;
        String task = frm.getTask();
        int page = frm.getPaging().getCurrentPageNo();
        frm.getPaging().calculationPage();

        if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)
                || Global.WEB_TASK_SHOW.equals(task)) {
            this.load(request, frm);
        } else if (Global.WEB_TASK_DETAIL.equals(task)) {
        } else if (Global.WEB_TASK_APPROVE.equals(task)) {
            this.approval(request, frm, 1);
        } else if (Global.WEB_TASK_REJECT.equals(task)) {
            this.approval(request, frm, -1);
        }

        return this.getNextPage(task, mapping);

    }

    private void approval(HttpServletRequest request, ApprovalUserForm form, int flag) throws AppException {
        String[] member = form.getSearch().getMemberList();
        for (int i = 0; i < member.length; i++) {
            this.manager.approval(member[i], this.getLoginId(request), flag);
        }
        if (1 == flag)
            request.setAttribute("message", resources.getMessage("common.approve", member.length + " Data"));
        else if (-1 == flag)
            request.setAttribute("message", resources.getMessage("common.reject", member.length + " Data"));
        else
            request.setAttribute("message", resources.getMessage("common.invalid.approval"));

        this.load(request, form);
    }


    private void load(HttpServletRequest request, ApprovalUserForm form) throws AppException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;

        PageListWrapper result = this.manager.getAllUserUnapprove(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            case ErrorCode.ERR_WS_ERROR:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((ApprovalUserForm) form).setTask(((ApprovalUserForm) form).resolvePreviousTask());

        String task = ((ApprovalUserForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
