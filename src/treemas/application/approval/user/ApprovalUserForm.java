package treemas.application.approval.user;

import treemas.base.appadmin.AppAdminFormBase;

public class ApprovalUserForm extends AppAdminFormBase {
    private String userid;
    private String branchid;
    private String branchlevel;
    private String branchname;
    private String fullname;
    private String active;
    private String sqlpassword;
    private String secquestion;
    private String secanswer;
    private String usercrt;
    private String dtmcrt;
    private String usrupd;
    private String dtmupd;
    private String islogin;
    private String lastlogin;
    private String ispasschg;
    private String user_ed;
    private String password_ed;
    private String wrongpass_count;
    private String wrongpass_time;
    private String islocked;
    private String timeslocked;
    private String locked_time;
    private String last_passchg;
    private String job_desc;
    private String job_descName;
    private String job_area;
    private String isapproved;
    /**
     * Property of user contact
     */
    private String email;
    private String phone_no;
    private String handphone_no;
    private String isUseBB;
    private String bbpin;
    /**
     * Property of if use handset
     */
    private String isUseMobile;
    private String handset_code;
    private String handset_imei;
    private String handset_version;
    private String gcmno;


    private String groupPrivilege;
    private String usract;
    private String countdiff;
    private String allfield;

    private ApprovalUserSearch search = new ApprovalUserSearch();

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public String getBranchlevel() {
        return branchlevel;
    }

    public void setBranchlevel(String branchlevel) {
        this.branchlevel = branchlevel;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSqlpassword() {
        return sqlpassword;
    }

    public void setSqlpassword(String sqlpassword) {
        this.sqlpassword = sqlpassword;
    }

    public String getSecquestion() {
        return secquestion;
    }

    public void setSecquestion(String secquestion) {
        this.secquestion = secquestion;
    }

    public String getSecanswer() {
        return secanswer;
    }

    public void setSecanswer(String secanswer) {
        this.secanswer = secanswer;
    }

    public String getUsercrt() {
        return usercrt;
    }

    public void setUsercrt(String usercrt) {
        this.usercrt = usercrt;
    }

    public String getDtmcrt() {
        return dtmcrt;
    }

    public void setDtmcrt(String dtmcrt) {
        this.dtmcrt = dtmcrt;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getIslogin() {
        return islogin;
    }

    public void setIslogin(String islogin) {
        this.islogin = islogin;
    }

    public String getLastlogin() {
        return lastlogin;
    }

    public void setLastlogin(String lastlogin) {
        this.lastlogin = lastlogin;
    }

    public String getIspasschg() {
        return ispasschg;
    }

    public void setIspasschg(String ispasschg) {
        this.ispasschg = ispasschg;
    }

    public String getUser_ed() {
        return user_ed;
    }

    public void setUser_ed(String user_ed) {
        this.user_ed = user_ed;
    }

    public String getPassword_ed() {
        return password_ed;
    }

    public void setPassword_ed(String password_ed) {
        this.password_ed = password_ed;
    }

    public String getWrongpass_count() {
        return wrongpass_count;
    }

    public void setWrongpass_count(String wrongpass_count) {
        this.wrongpass_count = wrongpass_count;
    }

    public String getWrongpass_time() {
        return wrongpass_time;
    }

    public void setWrongpass_time(String wrongpass_time) {
        this.wrongpass_time = wrongpass_time;
    }

    public String getIslocked() {
        return islocked;
    }

    public void setIslocked(String islocked) {
        this.islocked = islocked;
    }

    public String getTimeslocked() {
        return timeslocked;
    }

    public void setTimeslocked(String timeslocked) {
        this.timeslocked = timeslocked;
    }

    public String getLocked_time() {
        return locked_time;
    }

    public void setLocked_time(String locked_time) {
        this.locked_time = locked_time;
    }

    public String getLast_passchg() {
        return last_passchg;
    }

    public void setLast_passchg(String last_passchg) {
        this.last_passchg = last_passchg;
    }

    public String getJob_desc() {
        return job_desc;
    }

    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }

    public String getJob_descName() {
        return job_descName;
    }

    public void setJob_descName(String job_descName) {
        this.job_descName = job_descName;
    }

    public String getJob_area() {
        return job_area;
    }

    public void setJob_area(String job_area) {
        this.job_area = job_area;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getHandphone_no() {
        return handphone_no;
    }

    public void setHandphone_no(String handphone_no) {
        this.handphone_no = handphone_no;
    }

    public String getIsUseBB() {
        return isUseBB;
    }

    public void setIsUseBB(String isUseBB) {
        this.isUseBB = isUseBB;
    }

    public String getBbpin() {
        return bbpin;
    }

    public void setBbpin(String bbpin) {
        this.bbpin = bbpin;
    }

    public String getIsUseMobile() {
        return isUseMobile;
    }

    public void setIsUseMobile(String isUseMobile) {
        this.isUseMobile = isUseMobile;
    }

    public String getHandset_code() {
        return handset_code;
    }

    public void setHandset_code(String handset_code) {
        this.handset_code = handset_code;
    }

    public String getHandset_imei() {
        return handset_imei;
    }

    public void setHandset_imei(String handset_imei) {
        this.handset_imei = handset_imei;
    }

    public String getHandset_version() {
        return handset_version;
    }

    public void setHandset_version(String handset_version) {
        this.handset_version = handset_version;
    }

    public String getGcmno() {
        return gcmno;
    }

    public void setGcmno(String gcmno) {
        this.gcmno = gcmno;
    }

    public String getGroupPrivilege() {
        return groupPrivilege;
    }

    public void setGroupPrivilege(String groupPrivilege) {
        this.groupPrivilege = groupPrivilege;
    }

    public String getUsract() {
        return usract;
    }

    public void setUsract(String usract) {
        this.usract = usract;
    }

    public String getCountdiff() {
        return countdiff;
    }

    public void setCountdiff(String countdiff) {
        this.countdiff = countdiff;
    }

    public String getAllfield() {
        return allfield;
    }

    public void setAllfield(String allfield) {
        this.allfield = allfield;
    }

    public ApprovalUserSearch getSearch() {
        return search;
    }

    public void setSearch(ApprovalUserSearch search) {
        this.search = search;
    }


}
