package treemas.application.approval.user;

import java.io.Serializable;
import java.util.Date;

public class ApprovalUserBean implements Serializable {
    private String userid;
    private String branchid;
    private Integer branchlevel;
    private String branchname;
    private String fullname;
    private String active;
    private String sqlpassword;
    private String secquestion;
    private String secanswer;
    private String usercrt;
    private Date dtmcrt;
    private String usrupd;
    private Date dtmupd;
    private String islogin;
    private String lastlogin;
    private String ispasschg;
    private Date user_ed;
    private Date password_ed;
    private Integer wrongpass_count;
    private Date wrongpass_time;
    private String islocked;
    private Integer timeslocked;
    private Date locked_time;
    private Date last_passchg;
    private String job_desc;
    private String job_descName;
    private String job_area;
    private String isapproved;
    /**
     * Property of user contact
     */
    private String email;
    private String phone_no;
    private String handphone_no;
    private String isUseBB;
    private String bbpin;
    /**
     * Property of if use handset
     */
    private String isUseMobile;
    private String handset_code;
    private String handset_imei;
    private String handset_version;
    private String gcmno;


    private String groupPrivilege;
    private String usract;
    private Integer countdiff;
    private Integer allfield;

    public Integer getCountdiff() {
        return countdiff;
    }

    public void setCountdiff(Integer countdiff) {
        this.countdiff = countdiff;
    }

    public Integer getAllfield() {
        return allfield;
    }

    public void setAllfield(Integer allfield) {
        this.allfield = allfield;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public Integer getBranchlevel() {
        return branchlevel;
    }

    public void setBranchlevel(Integer branchlevel) {
        this.branchlevel = branchlevel;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSqlpassword() {
        return sqlpassword;
    }

    public void setSqlpassword(String sqlpassword) {
        this.sqlpassword = sqlpassword;
    }

    public String getSecquestion() {
        return secquestion;
    }

    public void setSecquestion(String secquestion) {
        this.secquestion = secquestion;
    }

    public String getSecanswer() {
        return secanswer;
    }

    public void setSecanswer(String secanswer) {
        this.secanswer = secanswer;
    }

    public String getUsercrt() {
        return usercrt;
    }

    public void setUsercrt(String usercrt) {
        this.usercrt = usercrt;
    }

    public Date getDtmcrt() {
        return dtmcrt;
    }

    public void setDtmcrt(Date dtmcrt) {
        this.dtmcrt = dtmcrt;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public Date getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(Date dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getIslogin() {
        return islogin;
    }

    public void setIslogin(String islogin) {
        this.islogin = islogin;
    }

    public String getLastlogin() {
        return lastlogin;
    }

    public void setLastlogin(String lastlogin) {
        this.lastlogin = lastlogin;
    }

    public String getIspasschg() {
        return ispasschg;
    }

    public void setIspasschg(String ispasschg) {
        this.ispasschg = ispasschg;
    }

    public Date getUser_ed() {
        return user_ed;
    }

    public void setUser_ed(Date user_ed) {
        this.user_ed = user_ed;
    }

    public Date getPassword_ed() {
        return password_ed;
    }

    public void setPassword_ed(Date password_ed) {
        this.password_ed = password_ed;
    }

    public Integer getWrongpass_count() {
        return wrongpass_count;
    }

    public void setWrongpass_count(Integer wrongpass_count) {
        this.wrongpass_count = wrongpass_count;
    }

    public Date getWrongpass_time() {
        return wrongpass_time;
    }

    public void setWrongpass_time(Date wrongpass_time) {
        this.wrongpass_time = wrongpass_time;
    }

    public String getIslocked() {
        return islocked;
    }

    public void setIslocked(String islocked) {
        this.islocked = islocked;
    }

    public Integer getTimeslocked() {
        return timeslocked;
    }

    public void setTimeslocked(Integer timeslocked) {
        this.timeslocked = timeslocked;
    }

    public Date getLocked_time() {
        return locked_time;
    }

    public void setLocked_time(Date locked_time) {
        this.locked_time = locked_time;
    }

    public Date getLast_passchg() {
        return last_passchg;
    }

    public void setLast_passchg(Date last_passchg) {
        this.last_passchg = last_passchg;
    }

    public String getJob_desc() {
        return job_desc;
    }

    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }

    public String getJob_descName() {
        return job_descName;
    }

    public void setJob_descName(String job_descName) {
        this.job_descName = job_descName;
    }

    public String getJob_area() {
        return job_area;
    }

    public void setJob_area(String job_area) {
        this.job_area = job_area;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getHandphone_no() {
        return handphone_no;
    }

    public void setHandphone_no(String handphone_no) {
        this.handphone_no = handphone_no;
    }

    public String getIsUseBB() {
        return isUseBB;
    }

    public void setIsUseBB(String isUseBB) {
        this.isUseBB = isUseBB;
    }

    public String getBbpin() {
        return bbpin;
    }

    public void setBbpin(String bbpin) {
        this.bbpin = bbpin;
    }

    public String getIsUseMobile() {
        return isUseMobile;
    }

    public void setIsUseMobile(String isUseMobile) {
        this.isUseMobile = isUseMobile;
    }

    public String getHandset_code() {
        return handset_code;
    }

    public void setHandset_code(String handset_code) {
        this.handset_code = handset_code;
    }

    public String getHandset_imei() {
        return handset_imei;
    }

    public void setHandset_imei(String handset_imei) {
        this.handset_imei = handset_imei;
    }

    public String getHandset_version() {
        return handset_version;
    }

    public void setHandset_version(String handset_version) {
        this.handset_version = handset_version;
    }

    public String getGcmno() {
        return gcmno;
    }

    public void setGcmno(String gcmno) {
        this.gcmno = gcmno;
    }

    public String getGroupPrivilege() {
        return groupPrivilege;
    }

    public void setGroupPrivilege(String groupPrivilege) {
        this.groupPrivilege = groupPrivilege;
    }

    public String getUsract() {
        return usract;
    }

    public void setUsract(String usract) {
        this.usract = usract;
    }


}
