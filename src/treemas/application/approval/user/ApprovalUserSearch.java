package treemas.application.approval.user;

import treemas.application.approval.ApprovalSearch;

public class ApprovalUserSearch extends ApprovalSearch {

    String userid;
    String fullname;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }


}
