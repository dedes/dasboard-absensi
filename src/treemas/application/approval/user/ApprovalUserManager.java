package treemas.application.approval.user;

import treemas.application.general.lookup.approval.user.LookUpUserBean;
import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.auditrail.AuditTrailManager;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApprovalUserManager extends AppAdminManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_USER;
    private AuditTrailManager auditManager = new AuditTrailManager();

    public PageListWrapper getAllUserUnapprove(int pageNumber, ApprovalUserSearch search) throws AppException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllUserUnapprove", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllUserUnapprove", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }


    public Integer approval(String memberlist, String usrupd, int flag) throws AppException {
        Integer result = 0;
        Map paramApproval = new HashMap();
        try {
            paramApproval.put("flagCode", flag);
            paramApproval.put("userid", memberlist);
            paramApproval.put("usrupd", usrupd);
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.update(STRING_SQL + "approval", paramApproval);
                this.ibatisSqlMap.commitTransaction();
                result = (Integer) paramApproval.get("resultCode");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqe) {
            throw new CoreException(sqe, ERROR_DB);
        }

        return result;
    }


    public LookUpUserBean getDetailApproval(String userId) throws AppException {
        LookUpUserBean result = null;

        try {
            result = (LookUpUserBean) this.ibatisSqlMap.queryForObject(STRING_SQL + "getDetailUser", userId);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }


}
