package treemas.application.menubaru;

import java.sql.SQLException;
import java.util.List;

import treemas.application.feature.question.group.QuestionGroupBean;
import treemas.application.general.parameter.ParameterBean;
import treemas.base.core.CoreManagerBase;
import treemas.base.feature.FeatureManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

public class MenuBaruManager extends FeatureManagerBase {

	public PageListWrapper getAllRecords(Integer searchType,
            String searchValue)
            throws AppException {
		
			PageListWrapper result = new PageListWrapper();
			
			SearchFormParameter param = new SearchFormParameter();
			searchValue = searchValue != null ? searchValue.trim() : searchValue;
			//param.setPage(numberPage);
	        param.setSearchType1(searchType);
	        param.setSearchValue1(searchValue);
			
			try {
	            List list = this.ibatisSqlMap.queryForList("menubaru.getAllDataMenuBaru",param);
	            result.setResultList(list);
	        } catch (SQLException sqle) {
	            throw new AppException(sqle);
	        }
			//System.out.println("test1=" + result.get(0));
	        return result;
	}
	
	

    public void DeleteMenuBaru(String id) throws AppException {
        try {
            this.ibatisSqlMap.delete("menubaru.deletebyId", id);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }
    
    public MenuBaruBean getSingleRecords(String id)
            throws AppException {		
    		MenuBaruBean result = null;
    		try {
                result = (MenuBaruBean) this.ibatisSqlMap.queryForObject("menubaru.getSingleDataMenuBaru",id );
                System.out.println(result);
            } catch (SQLException sqle) {
                this.logger.printStackTrace(sqle);
                throw new CoreException(sqle, ERROR_DB);
            }
	        return result;
	}
    
    
    public void updateMenuBaru(MenuBaruBean menuBaruBean,
            String id) throws CoreException {    	
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.update("menubaru.updateMenuBaru", menuBaruBean);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            throw new CoreException(se, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }
	
    
    public void insertMenuBaru(MenuBaruBean menuBaruBean
            ) throws CoreException {    	
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.insert("menubaru.insertMenuBaru", menuBaruBean);
                this.ibatisSqlMap.commitTransaction();
/*                this.auditManager.auditAdd(this.ibatisSqlMap
                        .getCurrentConnection(), schemeBean, loginId, screenId);*/
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            throw new CoreException(se, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }
     
	
	
}
