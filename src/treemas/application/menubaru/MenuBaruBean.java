package treemas.application.menubaru;

import java.io.Serializable;


public class MenuBaruBean implements Serializable{
	private String id;
	private String nama;
	private String alamat;
	private String pendidikan;
	private String umur;
	
	public String getUmur() {
		return umur;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setUmur(String umur) {
		this.umur = umur;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getPendidikan() {
		return pendidikan;
	}
	public void setPendidikan(String pendidikan) {
		this.pendidikan = pendidikan;
	}
	
	
	
	

}
