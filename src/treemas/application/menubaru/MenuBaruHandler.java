package treemas.application.menubaru;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.google.common.base.Strings;

import treemas.application.about.AboutForm;
import treemas.application.feature.branch.BranchBean;
import treemas.application.feature.form.scheme.SchemeForm;
import treemas.application.feature.question.group.QuestionGroupBean;
import treemas.application.general.parameter.ParameterForm;
import treemas.application.general.parameter.ParameterManager;
import treemas.application.login.LoginForm;
import treemas.base.core.CoreActionBase;
import treemas.base.feature.FeatureActionBase;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.format.TreemasFormatter;
import treemas.util.geofence.google.Southwest;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

public class MenuBaruHandler extends FeatureActionBase {

    MenuBaruManager manager = new MenuBaruManager();

	public ActionForward doAction(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws CoreException, AppException {
		MenuBaruForm frm = (MenuBaruForm) form;
		String task = frm.getTask();
		System.out.println(task);		
		if (task.equals("Load") || task.equals("Search")) {
			this.load(request, frm);
			System.out.println(TreemasFormatter.formatDate(new Date(),
                    TreemasFormatter.DFORMAT_YMDHHMMSS));
		} else if(task.equals("Delete")){
			this.delete(request, frm);
			this.load(request, frm);
		} else if (task.equals("Edit")) {
			MenuBaruBean menuBaruBean = this.manager
                    .getSingleRecords(frm.getId());
			frm.fromBean(menuBaruBean);
			frm.getPaging().setDispatch(null);
		} else if (task.equals("Add")) {
			frm.setAlamat("");
			frm.setId("");;
			frm.setNama("");
			frm.setUmur("");
			frm.setPendidikan("");
		} else if (task.equals("Save")) {
			MenuBaruBean bean = new MenuBaruBean();
            frm.toBean(bean);
            manager.insertMenuBaru(bean);
            this.load(request, frm);
            request.setAttribute("message", resources.getMessage("common.process.save"));
		} else if (task.equals("Update")) {
			MenuBaruBean menuBaruBean = new MenuBaruBean();
			frm.toBean(menuBaruBean);
            this.manager.updateMenuBaru(menuBaruBean, frm.getId());
            this.load(request, frm);
            request.setAttribute("message", resources.getMessage("common.process.update"));
		} else
			this.load(request, frm);
		
		this.load(request, frm);
		System.out.println(mapping);
		System.out.println(mapping.findForward(task));
		return this.getNextPage(task, mapping);
        
    }
	
	private void load(HttpServletRequest request, MenuBaruForm form)
            throws AppException {
		//form.setSearchType("20");
		Integer searchType = null;
		if(!Strings.isNullOrEmpty(form.getSearchType()))
			searchType = Integer.parseInt(form.getSearchType());
		else
			searchType = 10;
			
		System.out.println(form.getSearchValue());
		PageListWrapper result = this.manager.getAllRecords(searchType, form.getSearchValue());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, MenuBaruForm.class);
        /*schemeForm.getPaging().setCurrentPageNo(targetPageNo);
        schemeForm.getPaging().setTotalRecord(result.getTotalRecord());*/
        request.setAttribute("list", list);                
    }
	
	
	private void delete(HttpServletRequest request, MenuBaruForm form)
            throws AppException {
        String param = "";
        param = form.getId();
        this.manager.DeleteMenuBaru(param);
//        this.load(request, form);
    }
	
	public ActionForward getNextPage(String key, ActionMapping mapping) {
        String view = (String) pageMap.get(key);
        return mapping.findForward(view);
    }
	
	

}


