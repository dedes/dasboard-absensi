package treemas.application.feature.branch;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;

public class BranchPolygonAreaBean implements Serializable, Auditable {
    private static final String[] AUDIT_COLUMNS = new String[]{"branchId",
            "sequence", "latitude", "longitude"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{"BRANCHID",
            "SEQUENCE", "LATITUDE", "LONGITUDE"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_BRANCH_POLYGON;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"branchId"};

    private String branchId;
    private Double latitude;
    private Double longitude;
    private Integer sequence;

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }
}
