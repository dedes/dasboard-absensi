package treemas.application.feature.branch;


import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.FeatureManager;
import treemas.application.general.sync.SyncDataExecutor;
import treemas.base.feature.FeatureActionBase;
import treemas.base.sec.term.UserImpl;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.format.TreemasDecimalFormatter;
import treemas.util.geofence.BoundBean;
import treemas.util.geofence.GeofenceBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class BranchHandler extends FeatureActionBase {
    public static final String WEB_TASK_GEOFENCE_LOAD = "Geofence";
    public static final String WEB_TASK_GEOFENCE_SAVE = "GeofenceSave";
    public static final String WEB_TASK_SYNC = "Sync";
    private final String screenId = "";
    TreemasDecimalFormatter convertDecimal = TreemasDecimalFormatter.getInstance();
    private BranchManager manager = new BranchManager();
    private FeatureManager commonManager = new FeatureManager();
    private Map excConverterMap = new HashMap();
    private BranchValidator bValidator;

    public BranchHandler() {
        this.excConverterMap.put("southBound", convertDecimal);
        this.excConverterMap.put("westBound", convertDecimal);
        this.excConverterMap.put("northBound", convertDecimal);
        this.excConverterMap.put("eastBound", convertDecimal);
        this.excConverterMap.put("latitude", convertDecimal);
        this.excConverterMap.put("longitude", convertDecimal);

        this.pageMap.put(WEB_TASK_GEOFENCE_LOAD, "geofence");
        this.pageMap.put(WEB_TASK_GEOFENCE_SAVE, "geofence");
        this.pageMap.put(WEB_TASK_SYNC, "view");
        this.exceptionMap.put(WEB_TASK_GEOFENCE_LOAD, "geofence");
        this.exceptionMap.put(WEB_TASK_GEOFENCE_SAVE, "geofence");
        this.exceptionMap.put(WEB_TASK_SYNC, "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.bValidator = new BranchValidator(request);
        BranchForm frm = (BranchForm) form;
        String task = frm.getTask();

        if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)) {
            this.loadList(request, frm);
        } else if (Global.WEB_TASK_ADD.equals(task)) {
            frm.setBranchId("");
            frm.setBranchName("");
            frm.setBranchAddress("");
            frm.setParentId("");
            frm.setParentName("");
        } else if (Global.WEB_TASK_INSERT.equals(task)) {
            if (this.bValidator.validateBranchInsert(new ActionMessages(), frm, this.manager)) {
                BranchBean bean = new BranchBean();

                frm.toBean(bean);
                manager.insertBranch(bean, this.getLoginId(request), this.screenId);
                this.loadList(request, frm);
                request.setAttribute("message", resources.getMessage("common.process.save"));
            }
        } else if (Global.WEB_TASK_DELETE.equals(task)) {
            try {
                manager.deleteBranch(frm.getBranchId(), this.getLoginId(request), this.screenId);
            } finally {
                this.loadList(request, frm);
            }
        } else if (Global.WEB_TASK_EDIT.equals(task)) {
            BranchBean branchBean = this.manager.getBranchInformation(
                    frm.getBranchId());
            BeanConverter.convertBeanToString(branchBean, frm, excConverterMap);
            this.loadBranchPOIList(request, branchBean.getBranchId());
        } else if (Global.WEB_TASK_UPDATE.equals(task)) {
            if (this.bValidator.validateBranchEdit(new ActionMessages(), frm)) {
                BranchBean bean = new BranchBean();
                frm.toBean(bean);

                manager.updateBranch(bean, this.getLoginId(request),
                        this.screenId);

                this.loadList(request, frm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
            }
        } else if (WEB_TASK_GEOFENCE_LOAD.equals(task)) {
            BranchBean branchBean = this.manager.getBranchInformation(
                    frm.getBranchId());
            frm.fromBean(branchBean);

            this.loadPolygon(request, frm);
            this.loadBranchPOIList(request, null);
        } else if (WEB_TASK_GEOFENCE_SAVE.equals(task)) {
            try {
                String path = frm.getPath();
                this.manager.insertPolygon(frm.getBranchId(), path, this.getLoginId(request));
            } finally {
                this.loadPolygon(request, frm);
            }
        } else if (WEB_TASK_SYNC.equals(task)) {
            SyncDataExecutor sync = new SyncDataExecutor();
            sync.updBranches();
            this.loadList(request, frm);
            this.setMessage(request, "common.sync.success", null);
        }

        return this.getNextPage(task, mapping);

    }

    private void loadList(HttpServletRequest request, BranchForm branchForm) throws CoreException {
        int searchType = Integer.parseInt(branchForm.getSearchType());

        List list = this.manager.getBranchList(searchType, branchForm.getSearchValue());
        list = BeanConverter.convertBeansToString(list, BranchForm.class);
        request.setAttribute("listBranch", list);
    }

    private void loadBranchPOIList(HttpServletRequest request, String branchId) throws CoreException {

        List list = commonManager.getBranchLocation(branchId);
        list = BeanConverter.convertBeansToStringMap(list, excConverterMap);
        request.setAttribute(Global.REQUEST_ATTR_BRANCH_POI, list);

        String img64 = UserImpl.ICON_IMG.toString();
        request.setAttribute(Global.REQUEST_ATTR_IMG_ICON, img64);
    }

    private void loadPolygon(HttpServletRequest request, BranchForm branchForm) throws CoreException {
        List list = this.manager.getPolygonArea(branchForm.getBranchId());
        branchForm.setBound(this.getBound(list));

        list = BeanConverter.convertBeansToStringMap(list, excConverterMap);
        request.setAttribute("listPath", list);
    }

    private BoundBean getBound(List list) throws CoreException {
        BoundBean result = new BoundBean();

        if (list == null || list.size() == 0)
            return null;

        GeofenceBean bean = (GeofenceBean) list.get(0);
        double minLat = bean.getLatitude();
        double maxLat = bean.getLatitude();
        double minLng = bean.getLongitude();
        double maxLng = bean.getLongitude();

        for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
            GeofenceBean tempBean = (GeofenceBean) iterator.next();

            if (tempBean.getLatitude() < minLat) {
                minLat = tempBean.getLatitude();
            }

            if (tempBean.getLongitude() < minLng) {
                minLng = tempBean.getLongitude();
            }

            if (tempBean.getLatitude() > maxLat) {
                maxLat = tempBean.getLatitude();
            }

            if (tempBean.getLongitude() > maxLng) {
                maxLng = tempBean.getLongitude();
            }
        }

        result.setSouthBound(new Double(minLat));
        result.setNorthBound(new Double(maxLat));
        result.setWestBound(new Double(minLng));
        result.setEastBound(new Double(maxLng));

        return result;
    }


    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case BranchManager.ERROR_OPENFIRE:
                this.setMessage(request, "openfire.error", null);
                break;

            case BranchManager.ERROR_DATA_EXISTS:
                this.setMessage(request, "common.dataexists", new Object[]{ex.getUserObject()});
                break;

            case BranchManager.ERROR_HAS_CHILD:
                this.setMessage(request, "common.haschild", null);
                break;

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((BranchForm) form).setTask(((BranchForm) form).resolvePreviousTask());
        String task = ((BranchForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
