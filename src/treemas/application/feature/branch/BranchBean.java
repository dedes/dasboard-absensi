package treemas.application.feature.branch;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;

public class BranchBean implements Serializable, Auditable {
    private static final String[] AUDIT_COLUMNS = new String[]{"branchId", "branchName", "branchAddress", "parentId", "latitude", "longitude"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{"BRANCHID", "BRANCHNAME", "BRANCHADDR", "PARENTID", "LATITUDE", "LONGITUDE"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_BRANCH;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"branchId"};

    private String branchId;
    private String branchName;
    private String branchAddress;
    private String parentId;
    private String parentName;
    private Integer branchLevel;
    private Double latitude;
    private Double longitude;

    public BranchBean() {
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getBranchLevel() {
        return branchLevel;
    }

    public void setBranchLevel(Integer branchLevel) {
        this.branchLevel = branchLevel;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

}
