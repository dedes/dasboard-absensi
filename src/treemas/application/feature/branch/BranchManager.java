package treemas.application.feature.branch;

import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.openfire.OpenfireException;
import treemas.util.openfire.user.OpenfireUserGroupManager;
import treemas.util.tool.ConfigurationProperties;
import treemas.util.tool.Tools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class BranchManager extends FeatureManagerBase {
    static final int SEARCH_BY_BRANCH_ID = 10;
    static final int SEARCH_BY_BRANCH_NAME = 20;
    static final String OPENFIRE_ENABLED = "1";
    static final int ERROR_OPENFIRE = 101;
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_BRANCH;

    public BranchManager() {
    }

    public void deleteBranch(String branchId, String loginId, String screenId)
            throws CoreException {
        BranchBean branchBean = new BranchBean();
        branchBean.setBranchId(branchId);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Deleting branch "
                        + branchId);

                this.auditManager.auditDelete(this.ibatisSqlMap
                        .getCurrentConnection(), branchBean, loginId, screenId);

                this.ibatisSqlMap.delete(this.STRING_SQL + "deleteBranch",
                        branchId);

                if (OPENFIRE_ENABLED.equals(ConfigurationProperties.getInstance().get(
                        PropertiesKey.OPENFIRE_ENABLED))) {
                    OpenfireUserGroupManager of = new OpenfireUserGroupManager();
                    of.deleteGroupProp(Global.PREFIX_OF_BRANCH_GROUP + branchId);
                    of.deleteGroup(Global.PREFIX_OF_BRANCH_GROUP + branchId);
                }

                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Deleted branch "
                        + branchId);
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (OpenfireException oe) {
            throw new CoreException(oe, ERROR_OPENFIRE);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND) {
                throw new CoreException("", sqle, ERROR_HAS_CHILD,
                        branchId);
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public List getBranchList(int searchType, String searchValue) throws CoreException {
        if (searchType != SEARCH_BY_BRANCH_ID
                && searchType != SEARCH_BY_BRANCH_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: "
                    + searchType);

        SearchFormParameter param = new SearchFormParameter();
        searchValue = searchValue != null ? searchValue.trim() : searchValue;
        param.setSearchType1(searchType);
        param.setSearchValue1(searchValue);

        List branchList = null;
        try {
            branchList = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_DB);
        }

        return branchList;
    }

    public BranchBean getBranchInformation(String branchId) throws CoreException {
        BranchBean result = null;
        try {
            result = (BranchBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getBranchInformation", branchId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }


    public void insertBranch(BranchBean bean, String loginId,
                             String screenId)
            throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Inserting branch "
                        + bean.getBranchId());

                this.auditManager.auditAdd(this.ibatisSqlMap
                        .getCurrentConnection(), bean, loginId, screenId);

                this.ibatisSqlMap.insert(this.STRING_SQL + "insertNewBranch",
                        bean);

                if (OPENFIRE_ENABLED.equals(ConfigurationProperties.getInstance().get(
                        PropertiesKey.OPENFIRE_ENABLED))) {
                    OpenfireUserGroupManager of = new OpenfireUserGroupManager();
                    of.addGroup(Global.PREFIX_OF_BRANCH_GROUP + bean.getBranchId(), null);
                    of.addGroupPropDisplayName(Global.PREFIX_OF_BRANCH_GROUP + bean.getBranchId(), true);
                    of.addGroupPropShowInRoster(Global.PREFIX_OF_BRANCH_GROUP + bean.getBranchId());
                }

                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Inserted branch "
                        + bean.getBranchId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (OpenfireException oe) {
            throw new CoreException(oe, ERROR_OPENFIRE);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new CoreException("", sqle, ERROR_DATA_EXISTS, bean
                        .getBranchId());
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void updateBranch(BranchBean bean, String loginId,
                             String screenId) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Updating Branch "
                        + bean.getBranchId());

                this.auditManager.auditEdit(this.ibatisSqlMap
                        .getCurrentConnection(), bean, loginId, screenId);

                this.ibatisSqlMap.update(this.STRING_SQL + "updateBranch", bean);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Updated Branch "
                        + bean.getBranchId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new CoreException("", sqle, ERROR_DATA_EXISTS, bean
                        .getBranchId());
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public List getPolygonArea(String branchId) throws CoreException {
        List result = null;

        try {
            result = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getBranchPath", branchId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

        return result;
    }

    public void insertPolygon(String branchId, String path, String userId)
            throws CoreException {
        try {
            List list = this.convertPathToList(path);
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Deleting branch's polygon area "
                        + branchId + " by " + userId);

                this.ibatisSqlMap.delete(this.STRING_SQL + "deleteBranchPath", branchId);

                this.logger.log(ILogger.LEVEL_INFO, "Deleted branch's polygon area "
                        + branchId + " by " + userId);

                if (list != null && list.size() > 0) {
                    Iterator iter = list.iterator();
                    this.logger.log(ILogger.LEVEL_INFO, "Inserting branch's polygon area "
                            + branchId);
                    while (iter.hasNext()) {
                        BranchPolygonAreaBean bean = (BranchPolygonAreaBean) iter.next();
                        bean.setBranchId(branchId);

                        this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), bean, userId, "");
                        this.ibatisSqlMap.insert(this.STRING_SQL + "insertBranchPath", bean);
                    }
                    this.logger.log(ILogger.LEVEL_INFO, "Inserted branch's polygon area "
                            + branchId);
                }

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new CoreException("", sqle, ERROR_DATA_EXISTS, branchId);
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    private List convertPathToList(String path) {
        if (path == null || path.length() == 0)
            return null;

        String[] pathArr = Tools.split(path, "|");

        List result = new ArrayList(pathArr.length);

        for (int i = 0; i < pathArr.length; i++) {
            String[] coord = Tools.split(pathArr[i], ","); //path = lat,lng
            BranchPolygonAreaBean bean = new BranchPolygonAreaBean();
            bean.setLatitude(new Double(coord[0]));
            bean.setLongitude(new Double(coord[1]));
            bean.setSequence(new Integer(coord[2]));
            result.add(bean);
        }

        return result;
    }

    public void syncBranch() throws CoreException {
        try {
            this.logger.log(ILogger.LEVEL_INFO, "Synchronising branches from core system.");
            this.ibatisSqlMap.update(this.STRING_SQL + "syncBranch", null);
            this.logger.log(ILogger.LEVEL_INFO, "Synchronising branches from core system...finished");
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_DB);
        }
    }

    public BranchBean getSingleBranch(String branchId) throws CoreException {
        BranchBean result = null;
        try {
            result = (BranchBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getSingleBranch", branchId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }
}
