package treemas.application.feature.branch;

import treemas.base.feature.FeatureFormBase;
import treemas.util.geofence.BoundBean;

public class BranchForm extends FeatureFormBase {
    private String branchId;
    private String branchName;
    private String branchAddress;
    private String parentId;
    private String parentName;
    private String branchLevel;
    private String latitude;
    private String longitude;
    private String searchType = "10";
    private String searchValue;

    private BoundBean bound;
    private String path;

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getBranchLevel() {
        return branchLevel;
    }

    public void setBranchLevel(String branchLevel) {
        this.branchLevel = branchLevel;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public BoundBean getBound() {
        return bound;
    }

    public void setBound(BoundBean bound) {
        this.bound = bound;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIndentation() {
        int level = Integer.parseInt(branchLevel) - 1;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < Math.pow(level, 1); i++) {
            sb.append("&nbsp;");
        }

        return sb.toString();
    }
}
