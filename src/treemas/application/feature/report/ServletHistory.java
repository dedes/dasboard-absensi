package treemas.application.feature.report;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import treemas.application.feature.mobileapp.history.HistoryBean;
import treemas.application.feature.mobileapp.history.HistoryForm;
import treemas.application.feature.mobileapp.history.HistoryManager;
import treemas.base.handset.ServletBase;
import treemas.util.log.ILogger;

public class ServletHistory<JasperPrint> extends ServletBase {
	private HistoryManager manager = new HistoryManager();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		HistoryForm frm = new HistoryForm();

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		String timeStamp = dateFormat.format(new Date());
		String ApprovalNo = req.getParameter("ApprovalNo");
		String ApprovalID = req.getParameter("ApprovalID");
		String ApprovalSchemeID = req.getParameter("ApprovalSchemeID");
		String ApprovalStatus = req.getParameter("ApprovalStatus");
		String TransactionNo = req.getParameter("TransactionNo");
		String IsApprovedByMobile = req.getParameter("IsApprovedByMobile");
		String IsFinal = req.getParameter("IsFinal");
		
			try {
				String filePath = getServletContext().getRealPath(
						"/report/excel/template_audit.xls");
				FileInputStream fileIn = new FileInputStream(filePath);
				POIFSFileSystem myFileSystem = new POIFSFileSystem(fileIn);
				HSSFWorkbook workbook = new HSSFWorkbook(myFileSystem);
				HSSFSheet sheet = workbook.getSheetAt(0);
				HSSFRow row = sheet.createRow((short) 0);
				row.createCell(0).setCellValue("No");
				row.createCell(1).setCellValue("ApprovalNo");
				row.createCell(2).setCellValue("ApprovalID");
				row.createCell(3).setCellValue("IsFinal");
				row.createCell(4).setCellValue("SecurityCode");
				row.createCell(5).setCellValue("ApprovalSchemeID");
				row.createCell(6).setCellValue("RequestDate");
				row.createCell(7).setCellValue("IsViaSMS");
				row.createCell(8).setCellValue("IsViaEmail");
				row.createCell(9).setCellValue("IsViaFax");
				row.createCell(10).setCellValue("AreaFaxNo");
				row.createCell(11).setCellValue("FaxNo");
				row.createCell(12).setCellValue("UserRequest");
				row.createCell(13).setCellValue("ApprovalDate");
				row.createCell(14).setCellValue("ApprovalStatus");
				row.createCell(15).setCellValue("ApprovalResult");
				row.createCell(16).setCellValue("ApprovalNote");
				row.createCell(17).setCellValue("LimitValue");
				row.createCell(18).setCellValue("ApprovalValue");
				row.createCell(19).setCellValue("IsForward");
				row.createCell(20).setCellValue("UserApproval");
				row.createCell(21).setCellValue("UserSecurityCode");
				row.createCell(22).setCellValue("ApprovalRoleID");
				row.createCell(23).setCellValue("TransactionNo");
				row.createCell(24).setCellValue("WOA");
				row.createCell(25).setCellValue("IsEverRejected");
				row.createCell(26).setCellValue("ClientIP");
				row.createCell(27).setCellValue("Argumentasi");
				row.createCell(28).setCellValue("ApprovalValuePencentage");
				row.createCell(29).setCellValue("ApprovalValuePercentage");
				row.createCell(30).setCellValue("IsApprovedByMobile");
				row.createCell(31).setCellValue("dtmupd");
				row.createCell(32).setCellValue("UsrUpd");
				row.createCell(33).setCellValue("Approvalanswer");
				row.createCell(34).setCellValue("Approvalnoteanswer");
				row.createCell(35).setCellValue("DateApproval");
				row.createCell(36).setCellValue("UserApprovalnext");
				
				for (int i = 1; i <= sheet.getLastRowNum(); i++) {
					if (null != sheet.getRow(i)) {
						sheet.removeRow(sheet.getRow(i));
					}
				}

				List<HistoryBean> list = this.manager.getAllHistory(ApprovalNo, ApprovalID, ApprovalSchemeID, ApprovalStatus, TransactionNo, IsApprovedByMobile, IsFinal );
				if (list.size() != 0) {
					int rowIdx = 1;
					for (HistoryBean bean : list) {
						HSSFRow rows = sheet.createRow(rowIdx);
						rows.createCell(0).setCellValue(rowIdx);
						rows.createCell(1).setCellValue(bean.getApprovalNo());
						rows.createCell(2).setCellValue(bean.getApprovalID());
						rows.createCell(3).setCellValue(bean.getIsFinal());
						rows.createCell(4).setCellValue(bean.getSecurityCode());
						rows.createCell(5).setCellValue(bean.getApprovalSchemeID());
						rows.createCell(6).setCellValue(bean.getRequestDate());
						rows.createCell(7).setCellValue(bean.getIsViaSMS());
						rows.createCell(8).setCellValue(bean.getIsViaEmail());
						rows.createCell(9).setCellValue(bean.getIsViaFax());
						rows.createCell(10).setCellValue(bean.getAreaFaxNo());
						rows.createCell(11).setCellValue(bean.getFaxNo());
						rows.createCell(12).setCellValue(bean.getUserRequest());
						rows.createCell(13).setCellValue(bean.getApprovalDate());
						
						rows.createCell(14).setCellValue(bean.getApprovalStatus());
						rows.createCell(15).setCellValue(bean.getApprovalResult());
						rows.createCell(16).setCellValue(bean.getApprovalNote());
						rows.createCell(17).setCellValue(bean.getLimitValue());
						rows.createCell(18).setCellValue(bean.getApprovalValue());
						rows.createCell(19).setCellValue(bean.getIsForward());
						rows.createCell(20).setCellValue(bean.getUserApproval());
						rows.createCell(21).setCellValue(bean.getUserSecurityCode());
						rows.createCell(22).setCellValue(bean.getApprovalRoleID());
						rows.createCell(23).setCellValue(bean.getTransactionNo());
						rows.createCell(24).setCellValue(bean.getWOA());
						rows.createCell(25).setCellValue(bean.getIsEverRejected());
						rows.createCell(26).setCellValue(bean.getClientIP());
						rows.createCell(27).setCellValue(bean.getArgumentasi());
						rows.createCell(28).setCellValue(bean.getApprovalValuePencentage());
						rows.createCell(29).setCellValue(bean.getApprovalValuePercentage());
						rows.createCell(30).setCellValue(bean.getIsApprovedByMobile());
						rows.createCell(31).setCellValue(bean.getDtmupd());
						rows.createCell(32).setCellValue(bean.getUsrUpd ());
						rows.createCell(33).setCellValue(bean.getApprovalanswer());
						rows.createCell(34).setCellValue(bean.getApprovalnoteanswer ());
						rows.createCell(35).setCellValue(bean.getDateApproval());
						rows.createCell(36).setCellValue(bean.getUserApprovalnext());
					
						rowIdx++;
					}

				}
				fileIn.close();

				FileOutputStream fileOut = new FileOutputStream(filePath);
				workbook.write(fileOut);
				fileOut.close();
				resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				String timeStamp1 = null;
				resp.setHeader("Content-Disposition",
						"attachment; filename=report_audittrailWatchlist_"
								+ timeStamp1 + ".xls");
				resp.setHeader("Cache-Control", "public");
				resp.setHeader("Pragma", "public");
				resp.setHeader("CookiesConfigureNoCache", "false");

				OutputStream outputStream = resp.getOutputStream();
				workbook.write(outputStream);

				outputStream.flush();
				outputStream.close();
				this.logger.log(ILogger.LEVEL_INFO,
						"Your excel file has been generated!");

			}catch (Exception ex) {
				System.out.println(ex);
			}
		}

	}


