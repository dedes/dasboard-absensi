package treemas.application.feature.report;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import treemas.application.feature.mobileapp.approvaltype.ApprovalTypeBean;
import treemas.application.feature.mobileapp.approvaltype.ApprovalTypeForm;
import treemas.application.feature.mobileapp.approvaltype.ApprovalTypeManager;

import treemas.base.handset.ServletBase;
import treemas.util.log.ILogger;

public class ServletApprovalType<JasperPrint> extends ServletBase {
	private ApprovalTypeManager manager = new ApprovalTypeManager();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ApprovalTypeForm frm = new ApprovalTypeForm();

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		String timeStamp = dateFormat.format(new Date());
		String ApprovalSchemeID = req.getParameter("ApprovalSchemeID");
		String ApprovalTypeID = req.getParameter("ApprovalTypeID");
		String ApprovalSchemeName = req.getParameter("ApprovalSchemeName");
		
	
		
			try {
				String filePath = getServletContext().getRealPath(
						"/report/excel/template_audit.xls");
				FileInputStream fileIn = new FileInputStream(filePath);
				POIFSFileSystem myFileSystem = new POIFSFileSystem(fileIn);
				HSSFWorkbook workbook = new HSSFWorkbook(myFileSystem);
				HSSFSheet sheet = workbook.getSheetAt(0);
				HSSFRow row = sheet.createRow((short) 0);
				row.createCell(0).setCellValue("No");
				row.createCell(1).setCellValue("ApprovalSchemeID");
				row.createCell(2).setCellValue("ApprovalTypeID");
				row.createCell(3).setCellValue("ApprovalSchemeName");
				

				for (int i = 1; i <= sheet.getLastRowNum(); i++) {
					if (null != sheet.getRow(i)) {
						sheet.removeRow(sheet.getRow(i));
					}
				}

				List<ApprovalTypeBean> list = this.manager.getAllApprovalType(ApprovalSchemeID, ApprovalTypeID, ApprovalSchemeName);
				if (list.size() != 0) {
					int rowIdx = 1;
					for (ApprovalTypeBean bean : list) {
						HSSFRow rows = sheet.createRow(rowIdx);
						rows.createCell(0).setCellValue(rowIdx);
						rows.createCell(1).setCellValue(bean.getApprovalSchemeID());
						rows.createCell(2).setCellValue(bean.getApprovalTypeID());
						rows.createCell(3).setCellValue(bean.getApprovalSchemeName());
					
					
						rowIdx++;
					}

				}
				fileIn.close();

				FileOutputStream fileOut = new FileOutputStream(filePath);
				workbook.write(fileOut);
				fileOut.close();
				resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				String timeStamp1 = null;
				resp.setHeader("Content-Disposition",
						"attachment; filename=report_audittrailWatchlist_"
								+ timeStamp1 + ".xls");
				resp.setHeader("Cache-Control", "public");
				resp.setHeader("Pragma", "public");
				resp.setHeader("CookiesConfigureNoCache", "false");

				OutputStream outputStream = resp.getOutputStream();
				workbook.write(outputStream);

				outputStream.flush();
				outputStream.close();
				this.logger.log(ILogger.LEVEL_INFO,
						"Your excel file has been generated!");

			}catch (Exception ex) {
				System.out.println(ex);
			}
		}

	}


