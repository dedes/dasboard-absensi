package treemas.application.feature.report;

import treemas.application.feature.inquiry.assignment.AssignmentSearch;
import treemas.base.feature.FeatureManagerBase;
import treemas.util.AppException;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.List;

public class ReportManager extends FeatureManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_FEATURE_REPORT;

    public List getAllAssignmentReport(String branch, String searchValue1, String tglStart, String tglEnd,
                                       String status, String status2hidden, String scheme, String userId,
                                       String orderField, String sortDesc, String searchType1, String searchType2) throws AppException {
        AssignmentSearch search = new AssignmentSearch();
        search.setBranch(branch);
        search.setSearchValue1(searchValue1);
        search.setTglStart(tglStart);
        search.setTglEnd(tglEnd);
        search.setStatus(status);
        search.setStatus2hidden(status2hidden);
        search.setScheme(scheme);
        search.setUserId(userId);
        search.setOrderField(orderField);
        search.setSortDesc(sortDesc);
        search.setSearchType1(Integer.parseInt(searchType1));
        search.setSearchType2(Integer.parseInt(searchType2));

        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAssignmentReport", search);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new AppException(sqle, ERROR_DB);
        }
        return result;
    }
}
