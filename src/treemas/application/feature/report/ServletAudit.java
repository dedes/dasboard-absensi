package treemas.application.feature.report;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import treemas.application.feature.mobileapp.auditrail.AuditrailBean;
import treemas.application.feature.mobileapp.auditrail.AuditrailForm;
import treemas.application.feature.mobileapp.auditrail.AuditrailManager;
import treemas.base.handset.ServletBase;
import treemas.util.AppException;
import treemas.util.log.ILogger;

public class ServletAudit<JasperPrint> extends ServletBase {
	private AuditrailManager manager = new AuditrailManager();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		AuditrailForm frm = new AuditrailForm();

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		String timeStamp = dateFormat.format(new Date());
		String AUDITRAILID = req.getParameter("AUDITRAILID");
		String DateBefore = req.getParameter("datebefore");
		String DateAfter = req.getParameter("dateafter");
		String transactionDate = req.getParameter("transactionDate");
		String ApprovalSchemeID = req.getParameter("ApprovalSchemeID");
		String ApprovalNO = req.getParameter("ApprovalNO");
		String ApprovalID = req.getParameter("ApprovalID");
		/*String reportFile = getServletContext().getRealPath("/report/Report_auditUser_Log.jasper");
		System.out.println(reportFile);
		*/
		System.out.println(DateAfter+"servletA");
		System.out.println(DateBefore+"servletB");
		/*  HashMap hm = new HashMap();
	        // Create an Exporter
			net.sf.jasperreports.engine.JasperPrint jasperPrint = null;
			try {
				try {
					List listDataSource = this.manager.getAllAuditrail(AUDITRAILID, transactionDate, ACTION, dateBefore, dateAfter, ApprovalNO);
					jasperPrint = JasperFillManager.fillReport(reportFile, hm, new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource(listDataSource));
				} catch (AppException e1) {
					e1.printStackTrace();
				}
				
			} catch (JRException e) {
				e.printStackTrace();
			}
	
		*/
			try {
				String filePath = getServletContext().getRealPath(
						"/report/excel/template_audit.xls");
				FileInputStream fileIn = new FileInputStream(filePath);
				POIFSFileSystem myFileSystem = new POIFSFileSystem(fileIn);
				HSSFWorkbook workbook = new HSSFWorkbook(myFileSystem);
				HSSFSheet sheet = workbook.getSheetAt(0);
				HSSFRow row = sheet.createRow((short) 0);
				row.createCell(0).setCellValue("No");
				row.createCell(1).setCellValue("AUDITRAILID");
				row.createCell(2).setCellValue("TransactionDate");
				row.createCell(3).setCellValue("ACTION");
				row.createCell(4).setCellValue("ApprovalNO");
				row.createCell(5).setCellValue("ApprovalID");
				row.createCell(6).setCellValue("ApprovalSchemeID");
				row.createCell(7).setCellValue("UserRequest");
				row.createCell(8).setCellValue("TransactionNo");
				row.createCell(9).setCellValue("UserApproval");
				row.createCell(10).setCellValue("ApprovalStatus");
				row.createCell(11).setCellValue("ApprovalResult");

				for (int i = 1; i <= sheet.getLastRowNum(); i++) {
					if (null != sheet.getRow(i)) {
						sheet.removeRow(sheet.getRow(i));
					}
				}

				List<AuditrailBean> list = this.manager.getAllAuditrail(AUDITRAILID, ApprovalNO, ApprovalID, DateBefore, DateAfter, ApprovalSchemeID);
				if (list.size() != 0) {
					int rowIdx = 1;
					for (AuditrailBean bean : list) {
						HSSFRow rows = sheet.createRow(rowIdx);
						rows.createCell(0).setCellValue(rowIdx);
						rows.createCell(1).setCellValue(bean.getAUDITRAILID());
						rows.createCell(2).setCellValue(bean.getTransactionDate());
						rows.createCell(3).setCellValue(bean.getACTION());
						rows.createCell(4).setCellValue(bean.getApprovalNO());
						rows.createCell(5).setCellValue(bean.getApprovalID());
						rows.createCell(6).setCellValue(bean.getApprovalSchemeID());
						rows.createCell(7).setCellValue(bean.getUserRequest());
						rows.createCell(8).setCellValue(bean.getTransactionNo());
						rows.createCell(9).setCellValue(bean.getUserApproval());
						rows.createCell(10).setCellValue(bean.getApprovalStatus());
						rows.createCell(11).setCellValue(bean.getApprovalResult());
					
						rowIdx++;
					}

				}
				fileIn.close();

				FileOutputStream fileOut = new FileOutputStream(filePath);
				workbook.write(fileOut);
				fileOut.close();
				resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				String timeStamp1 = null;
				resp.setHeader("Content-Disposition",
						"attachment; filename=report_audittrailWatchlist_"
								+ timeStamp1 + ".xls");
				resp.setHeader("Cache-Control", "public");
				resp.setHeader("Pragma", "public");
				resp.setHeader("CookiesConfigureNoCache", "false");

				OutputStream outputStream = resp.getOutputStream();
				workbook.write(outputStream);

				outputStream.flush();
				outputStream.close();
				this.logger.log(ILogger.LEVEL_INFO,
						"Your excel file has been generated!");

			}catch (Exception ex) {
				System.out.println(ex);
			}
		}

	}




