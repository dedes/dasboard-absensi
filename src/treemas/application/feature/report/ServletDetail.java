package treemas.application.feature.report;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import treemas.application.feature.mobileapp.detail.DetailBean;
import treemas.application.feature.mobileapp.detail.DetailForm;
import treemas.application.feature.mobileapp.detail.DetailManager;
import treemas.application.report.ReportManager;
import treemas.base.handset.ServletBase;
import treemas.util.log.ILogger;

public class ServletDetail<JasperPrint> extends ServletBase {
	private DetailManager manager = new DetailManager();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		DetailForm frm = new DetailForm();

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		String timeStamp = dateFormat.format(new Date());
		String ApprovalSchemeID = req.getParameter("ApprovalSchemeID");
		String TransactionNoLabel = req.getParameter("TransactionNoLabel");
		String TransactionNo = req.getParameter("TransactionNo");
		String OptionalLabel1 = req.getParameter("OptionalLabel1");
	
		
			try {
				String filePath = getServletContext().getRealPath(
						"/report/excel/template_audit.xls");
				FileInputStream fileIn = new FileInputStream(filePath);
				POIFSFileSystem myFileSystem = new POIFSFileSystem(fileIn);
				HSSFWorkbook workbook = new HSSFWorkbook(myFileSystem);
				HSSFSheet sheet = workbook.getSheetAt(0);
				HSSFRow row = sheet.createRow((short) 0);
				row.createCell(0).setCellValue("No");
				row.createCell(1).setCellValue("ApprovalSchemeID");
				row.createCell(2).setCellValue("TransactionNoLabel");
				row.createCell(3).setCellValue("TransactionNo");
				row.createCell(4).setCellValue("OptionalLabel1");
				row.createCell(5).setCellValue("OptionalSQL1");
				row.createCell(6).setCellValue("OptionalLabel2");
				row.createCell(7).setCellValue("OptionalSQL2");
				row.createCell(8).setCellValue("NoteLabel");
				row.createCell(9).setCellValue("NoteSQLCmd");
				row.createCell(10).setCellValue("DtmUpd");
				row.createCell(11).setCellValue("getUsrUpd");
				
				row.createCell(12).setCellValue("getOptionalSQL3");
				row.createCell(13).setCellValue("getOptionalSQL3");
				row.createCell(14).setCellValue("getOptionalLabel4");
				row.createCell(15).setCellValue("getOptionalSQL4");
				row.createCell(16).setCellValue("getOptionalLabel5");
				row.createCell(17).setCellValue("getOptionalSQL5");
				row.createCell(18).setCellValue("getOptionalLabel6");
				row.createCell(19).setCellValue("getOptionalSQL6");
				row.createCell(20).setCellValue("getOptionalLabel7");
				row.createCell(21).setCellValue("getOptionalSQL7");
				row.createCell(22).setCellValue("getOptionalLabel8");
				row.createCell(23).setCellValue("getOptionalSQL8");
				row.createCell(24).setCellValue("getOptionalLabel9");
				row.createCell(25).setCellValue("getOptionalSQL9");
				row.createCell(26).setCellValue("getOptionalLabel0");
				row.createCell(27).setCellValue("getOptionalSQL10");
				row.createCell(28).setCellValue("getOptionalLabel1");
				row.createCell(29).setCellValue("getOptionalSQL11");
				row.createCell(30).setCellValue("getOptionalLabel2");
				row.createCell(31).setCellValue("getOptionalSQL12");
				row.createCell(32).setCellValue("getOptionalLabel3");
				row.createCell(33).setCellValue("getOptionalSQL13");
				row.createCell(34).setCellValue("getOptionalLabel4");
				row.createCell(35).setCellValue("getOptionalSQL14");
				row.createCell(36).setCellValue("getOptionalLabel5");
				row.createCell(37).setCellValue("getOptionalSQL15");
				


				
				
				
				
				for (int i = 1; i <= sheet.getLastRowNum(); i++) {
					if (null != sheet.getRow(i)) {
						sheet.removeRow(sheet.getRow(i));
					}
				}

				List<DetailBean> list = this.manager.getAllDetail(ApprovalSchemeID, TransactionNoLabel, TransactionNo, OptionalLabel1);
				if (list.size() != 0) {
					int rowIdx = 1;
					for (DetailBean bean : list) {
						HSSFRow rows = sheet.createRow(rowIdx);
						rows.createCell(0).setCellValue(rowIdx);
						rows.createCell(1).setCellValue(bean.getApprovalSchemeID());
						rows.createCell(2).setCellValue(bean.getTransactionNoLabel());
						rows.createCell(3).setCellValue(bean.getTransactionNo());
						rows.createCell(4).setCellValue(bean.getOptionalLabel1());
						rows.createCell(5).setCellValue(bean.getOptionalSQL1());
						rows.createCell(6).setCellValue(bean.getOptionalLabel2());
						rows.createCell(7).setCellValue(bean.getOptionalSQL2());
						rows.createCell(8).setCellValue(bean.getNoteLabel());
						rows.createCell(9).setCellValue(bean.getNoteSQLCmd());
						rows.createCell(10).setCellValue(bean.getDtmUpd());
						rows.createCell(11).setCellValue(bean.getUsrUpd());
						
						rows.createCell(12).setCellValue(bean.getOptionalSQL3());
						rows.createCell(13).setCellValue(bean.getOptionalLabel3());
						rows.createCell(14).setCellValue(bean.getOptionalSQL4());
						rows.createCell(15).setCellValue(bean.getOptionalLabel4());
						rows.createCell(16).setCellValue(bean.getOptionalSQL5());
						rows.createCell(17).setCellValue(bean.getOptionalLabel5());
						rows.createCell(18).setCellValue(bean.getOptionalSQL6());
						rows.createCell(19).setCellValue(bean.getOptionalLabel6());
						rows.createCell(20).setCellValue(bean.getOptionalSQL7());
						rows.createCell(21).setCellValue(bean.getOptionalLabel7());
						rows.createCell(22).setCellValue(bean.getOptionalSQL8());
						rows.createCell(23).setCellValue(bean.getOptionalLabel8());
						rows.createCell(24).setCellValue(bean.getOptionalSQL9());
						rows.createCell(25).setCellValue(bean.getOptionalLabel9());
						rows.createCell(26).setCellValue(bean.getOptionalSQL10());
						rows.createCell(27).setCellValue(bean.getOptionalLabel10());
						rows.createCell(28).setCellValue(bean.getOptionalSQL11());
						rows.createCell(29).setCellValue(bean.getOptionalLabel1());
						rows.createCell(30).setCellValue(bean.getOptionalSQL12());
						rows.createCell(31).setCellValue(bean.getOptionalLabel12());
						rows.createCell(32).setCellValue(bean.getOptionalSQL13());
						rows.createCell(33).setCellValue(bean.getOptionalLabel13());
						rows.createCell(34).setCellValue(bean.getOptionalSQL14());
						rows.createCell(35).setCellValue(bean.getOptionalLabel4());
						rows.createCell(36).setCellValue(bean.getOptionalSQL15());
						rows.createCell(37).setCellValue(bean.getOptionalLabel5());
					
					
						rowIdx++;
					}

				}
				fileIn.close();

				FileOutputStream fileOut = new FileOutputStream(filePath);
				workbook.write(fileOut);
				fileOut.close();
				resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				String timeStamp1 = null;
				resp.setHeader("Content-Disposition",
						"attachment; filename=report_audittrailWatchlist_"
								+ timeStamp1 + ".xls");
				resp.setHeader("Cache-Control", "public");
				resp.setHeader("Pragma", "public");
				resp.setHeader("CookiesConfigureNoCache", "false");

				OutputStream outputStream = resp.getOutputStream();
				workbook.write(outputStream);

				outputStream.flush();
				outputStream.close();
				this.logger.log(ILogger.LEVEL_INFO,
						"Your excel file has been generated!");

			}catch (Exception ex) {
				System.out.println(ex);
			}
		}

	}


