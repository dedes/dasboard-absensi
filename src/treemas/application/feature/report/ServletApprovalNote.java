package treemas.application.feature.report;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import treemas.application.feature.mobileapp.approvalnote.ApprovalNoteBean;
import treemas.application.feature.mobileapp.approvalnote.ApprovalNoteForm;
import treemas.application.feature.mobileapp.approvalnote.ApprovalNoteManager;
import treemas.base.handset.ServletBase;
import treemas.util.log.ILogger;

public class ServletApprovalNote<JasperPrint> extends ServletBase {
	private ApprovalNoteManager manager = new ApprovalNoteManager();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ApprovalNoteForm frm = new ApprovalNoteForm();

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		String timeStamp = dateFormat.format(new Date());
		String ApprovalNo = req.getParameter("ApprovalNo");
		String Dtmupd = req.getParameter("Dtmupd");
		String UsrUpd = req.getParameter("UsrUpd");
		String ApprovalNote = req.getParameter("ApprovalNote");
	
		
			try {
				String filePath = getServletContext().getRealPath(
						"/report/excel/template_audit.xls");
				FileInputStream fileIn = new FileInputStream(filePath);
				POIFSFileSystem myFileSystem = new POIFSFileSystem(fileIn);
				HSSFWorkbook workbook = new HSSFWorkbook(myFileSystem);
				HSSFSheet sheet = workbook.getSheetAt(0);
				HSSFRow row = sheet.createRow((short) 0);
				row.createCell(0).setCellValue("No");
				row.createCell(1).setCellValue("ApprovalNo");
				row.createCell(2).setCellValue("dtmupd");
				row.createCell(3).setCellValue("UsrUpd");
				row.createCell(4).setCellValue("ApprovalNote");
				

				for (int i = 1; i <= sheet.getLastRowNum(); i++) {
					if (null != sheet.getRow(i)) {
						sheet.removeRow(sheet.getRow(i));
					}
				}

				List<ApprovalNoteBean> list = this.manager.getAllApprovalNote (ApprovalNo );
				if (list.size() != 0) {
					int rowIdx = 1;
					for (ApprovalNoteBean bean : list) {
						HSSFRow rows = sheet.createRow(rowIdx);
						rows.createCell(0).setCellValue(rowIdx);
						rows.createCell(1).setCellValue(bean.getApprovalNo());
						rows.createCell(2).setCellValue(bean.getDtmupd());
						rows.createCell(3).setCellValue(bean.getUsrUpd());
						rows.createCell(4).setCellValue(bean.getApprovalNote());
						
					
						rowIdx++;
					}

				}
				fileIn.close();

				FileOutputStream fileOut = new FileOutputStream(filePath);
				workbook.write(fileOut);
				fileOut.close();
				resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				String timeStamp1 = null;
				resp.setHeader("Content-Disposition",
						"attachment; filename=report_audittrailWatchlist_"
								+ timeStamp1 + ".xls");
				resp.setHeader("Cache-Control", "public");
				resp.setHeader("Pragma", "public");
				resp.setHeader("CookiesConfigureNoCache", "false");

				OutputStream outputStream = resp.getOutputStream();
				workbook.write(outputStream);

				outputStream.flush();
				outputStream.close();
				this.logger.log(ILogger.LEVEL_INFO,
						"Your excel file has been generated!");

			}catch (Exception ex) {
				System.out.println(ex);
			}
		}

	}



