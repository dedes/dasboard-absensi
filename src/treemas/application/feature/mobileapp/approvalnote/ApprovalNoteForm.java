package treemas.application.feature.mobileapp.approvalnote;


import treemas.base.appadmin.AppAdminFormBase;
import treemas.util.beanaction.BeanConverter;

public class ApprovalNoteForm extends AppAdminFormBase{
	private String ApprovalNo;
	private String dtmupd;
	private String UsrUpd;
	private String ApprovalNote;
	public String  getApprovalNo() {
		return ApprovalNo;
	}
	public void setApprovalNo(String approvalNo) {
		ApprovalNo = approvalNo;
	}
	public String getDtmupd() {
		return dtmupd;
	}
	public void setDtmupd(String dtmupd) {
		this.dtmupd = dtmupd;
	}
	public String getUsrUpd() {
		return UsrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		UsrUpd = usrUpd;
	}
	public String getApprovalNote() {
		return ApprovalNote;
	}
	public void setApprovalNote(String approvalNote) {
		ApprovalNote = approvalNote;
	}
	public ApprovalNoteBean toBean() {
		ApprovalNoteBean bean = new ApprovalNoteBean();
        BeanConverter.convertBeanFromString(this, bean);
     
        return bean;
    }
	
    private ApprovalNoteSearch search = new ApprovalNoteSearch();
	public ApprovalNoteSearch getSearch() {
		return search;
	}
	public void setSearch(ApprovalNoteSearch search) {
		this.search = search;
		
		

	}
}
