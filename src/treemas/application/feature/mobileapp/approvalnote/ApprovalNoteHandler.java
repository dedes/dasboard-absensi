package treemas.application.feature.mobileapp.approvalnote;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import treemas.application.feature.mobileapp.approval.ApprovalManager;
import treemas.application.feature.mobileapp.approval.ApprovalValidator;
import treemas.application.feature.mobileapp.approvaltype.ApprovalTypeValidator;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

public class ApprovalNoteHandler extends AppAdminActionBase {

	ApprovalNoteManager manager = new ApprovalNoteManager();
	ApprovalNoteValidator validator;
	
	public ActionForward doAction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppException {
		
	
		ApprovalNoteForm frm = (ApprovalNoteForm) form;
		frm.setTitle(Global.TITLE_APP_APPROVALNOTE);
		frm.getPaging().calculationPage();
		String task = frm.getTask();
		if (Global.WEB_TASK_SHOW.equals(task)
				|| Global.WEB_TASK_LOAD.equals(task)
				|| Global.WEB_TASK_SEARCH.equals(task)) {
			this.load(request, frm);
			} 
				
		return this.getNextPage(task, mapping);

	}
	private ApprovalNoteBean convertToBean(ApprovalNoteForm form) throws AppException {
		ApprovalNoteBean bean = form.toBean();
		return bean;
	}
	private void load(HttpServletRequest request, ApprovalNoteForm form)
			throws CoreException {
		int pageNumber = form.getPaging().getTargetPageNo();
		if (Global.WEB_TASK_SEARCH.equals(form.getTask())
				|| Global.WEB_TASK_SHOW.equals(form.getTask()))
			pageNumber = 1;
		PageListWrapper result = this.manager.getAllApprovalNote(pageNumber,
				form.getSearch());
		List list = result.getResultList();
		list = BeanConverter.convertBeansToString(list, form.getClass());
		form.getPaging().setCurrentPageNo(pageNumber);
		form.getPaging().setTotalRecord(result.getTotalRecord());
		request.setAttribute("list", list);

	}

	public ActionForward handleException(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
			msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			this.saveErrors(request, msgs);
			break;
		default:
			super.handleException(mapping, form, request, response, ex);
			break;
		}
		((ApprovalNoteForm) form).setTask(((ApprovalNoteForm) form)
				.resolvePreviousTask());

		String task = ((ApprovalNoteForm) form).getTask();
		return this.getErrorPage(task, mapping);
	}





	}


