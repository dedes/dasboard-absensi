package treemas.application.feature.mobileapp.approvalnote;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.google.common.base.Strings;


import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

public class ApprovalNoteManager extends AppAdminManagerBase  {
	private final String STRING_SQL = SQLConstant.SQL_APPROVALNOTE ;

	public PageListWrapper getAllApprovalNote(int pageNumber,
			ApprovalNoteSearch ApprovalNoteSearch) throws CoreException {
		PageListWrapper result = new PageListWrapper();
		ApprovalNoteSearch.setPage(pageNumber);		
	
		
		
		try {
			this.logger.log(ILogger.LEVEL_INFO,
					"Execute Query Get All ApprovalNote");
			List list = this.ibatisSqlMap.queryForList(this.STRING_SQL
					+ "getAllApprovalNote", ApprovalNoteSearch);
			Integer count = (Integer) this.ibatisSqlMap.queryForObject(
					this.STRING_SQL + "countAllApprovalNote", ApprovalNoteSearch);
			result.setResultList(list);
			result.setTotalRecord(count.intValue());
			this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM,
					"Execute Query Get All ApprovalNote : " + ApprovalNoteSearch);
			this.logger.log(ILogger.LEVEL_INFO,
					"Execute Query Get All ApprovalNote Sukses");
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return result;
	}
	
	public List getAllApprovalNote(String ApprovalNo) throws CoreException {
		List list = null;
		
		Map param = new HashedMap();
		String no = !Strings.isNullOrEmpty(ApprovalNo) ? ApprovalNo : "";
		
		param.put("ApprovalNo", no);
		
	
		
		
	
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get AllApprovalNote");
			list  = this.ibatisSqlMap.queryForList(this.STRING_SQL+ "getAllNote", param);
	
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return list;
	}
	
	

	

}





	