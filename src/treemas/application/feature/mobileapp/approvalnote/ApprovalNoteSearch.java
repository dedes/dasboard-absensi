package treemas.application.feature.mobileapp.approvalnote;

import treemas.base.search.SearchForm;

public class ApprovalNoteSearch extends SearchForm {
	private String ApprovalNo;
	private String dtmupd;
	private String UsrUpd;
	private String ApprovalNote;
	public String getApprovalNo() {
		return ApprovalNo;
	}
	public void setApprovalNo(String approvalNo) {
		ApprovalNo = approvalNo;
	}
	public String getDtmupd() {
		return dtmupd;
	}
	public void setDtmupd(String dtmupd) {
		this.dtmupd = dtmupd;
	}
	public String getUsrUpd() {
		return UsrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		UsrUpd = usrUpd;
	}
	public String getApprovalNote() {
		return ApprovalNote;
	}
	public void setApprovalNote(String approvalNote) {
		ApprovalNote = approvalNote;
	}

}
