package treemas.application.feature.mobileapp.detail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.google.common.base.Strings;

import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.SQLErrorCode;
import treemas.util.log.ILogger;
import treemas.util.openfire.user.OpenfireUserGroupManager;
import treemas.util.paging.PageListWrapper;
import treemas.util.properties.PropertiesKey;
import treemas.util.tool.ConfigurationProperties;

public class DetailManager extends AppAdminManagerBase {

	private final String STRING_SQL = SQLConstant.SQL_DETAIL;

	public PageListWrapper getAllDetail(int pageNumber,
			DetailSearch search) throws CoreException {
		PageListWrapper result = new PageListWrapper();
		search.setPage(pageNumber);
		this.logger.log(ILogger.LEVEL_INFO,
		"Execute Query get All Detail ");
		try {
			this.logger.log(ILogger.LEVEL_INFO,
					"Execute Query Get All Detail");
			List list = this.ibatisSqlMap.queryForList(this.STRING_SQL
					+ "getAllDetail", search);
			Integer count = (Integer) this.ibatisSqlMap.queryForObject(
					this.STRING_SQL + "countAllDetail", search);
			result.setResultList(list);
			result.setTotalRecord(count.intValue());
			this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM,
					"Execute Query Get All Detail : " + search);
			this.logger.log(ILogger.LEVEL_INFO,
					"Execute Query Get All Detail Sukses");
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return result;
	}
	
	public List getAllDetail(String ApprovalSchemeID,  String TransactionNoLabel,String TransactionNo,String OptionalLabel1) throws CoreException {
		List list = null;
		
		Map param = new HashedMap();
		String Id = !Strings.isNullOrEmpty(ApprovalSchemeID) ? ApprovalSchemeID : "";
		String label = !Strings.isNullOrEmpty(TransactionNoLabel) ? TransactionNoLabel : "";
		String actionno = !Strings.isNullOrEmpty(TransactionNo) ? TransactionNo : "";
		String label1= !Strings.isNullOrEmpty(OptionalLabel1) ? OptionalLabel1 : "";
	
		
		
		System.out.println(ApprovalSchemeID);
		System.out.println(TransactionNoLabel);
		System.out.println(TransactionNo);
		System.out.println(OptionalLabel1);
		
		
		param.put("ApprovalSchemeID", Id);
		param.put("TransactionNoLabel", label);
		param.put("TransactionNo", actionno);
		param.put("OptionalLabel1", label1);
	
		
	
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get All Detail");
			list  = this.ibatisSqlMap.queryForList(this.STRING_SQL+ "getAllReport", param);
	
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return list;
	}
	   public  DetailBean getSingleDetail(String  ApprovalSchemeID) throws CoreException {
	    	 DetailBean result = null;
	        try {
	            result = (DetailBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleDetail",  ApprovalSchemeID);
	        } catch (SQLException e) {
	            this.logger.printStackTrace(e);
	            throw new CoreException(ERROR_DB);
	        }

	        return result;
	    }
	   
	   public void update(DetailBean bean) throws CoreException {
           Map paramMap = new HashMap();
           paramMap.put("ApprovalSchemeID", bean.getApprovalSchemeID());
           paramMap.put("TransactionNoLabel", bean.getTransactionNoLabel());
           paramMap.put("action", "EDIT");
           try {
              
               this.ibatisSqlMap.update(this.STRING_SQL + "update", bean);
           } catch (SQLException e) {
               this.logger.printStackTrace(e);
               throw new CoreException(ERROR_DB);
           }
           }
       
	    
	 public void delete(DetailBean beanDetail) throws CoreException {
         String ApprovalSchemeID = beanDetail.getApprovalSchemeID();
     
         try {
             this.ibatisSqlMap.startTransaction();
             this.ibatisSqlMap.delete(this.STRING_SQL + "delete", ApprovalSchemeID);
             this.ibatisSqlMap.commitTransaction();
             this.logger.log(ILogger.LEVEL_INFO, "Deleted Detail " + beanDetail.getApprovalSchemeID());
         } catch (SQLException sqle) {
             this.logger.printStackTrace(sqle);
             if (sqle.getErrorCode() == SQLErrorCode.ERROR_CHILD_FOUND) {
                 throw new CoreException("", sqle, ERROR_HAS_CHILD, ApprovalSchemeID);
             } else {
                 throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
             }
         }  catch (Exception e) {
             this.logger.printStackTrace(e);
             throw new CoreException(e, ERROR_UNKNOWN);
         } finally {
             try {
                 this.ibatisSqlMap.endTransaction();
             } catch (Exception e) {
                 e.printStackTrace();
             }

         }
         
	 }

}


	