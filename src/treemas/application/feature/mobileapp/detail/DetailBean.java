package treemas.application.feature.mobileapp.detail;

import treemas.base.ApplicationBean;

public class DetailBean extends ApplicationBean{
	
	private String ApprovalSchemeID;
    private String TransactionNoLabel;
    private String TransactionNo;
    private String OptionalLabel1;
    private String OptionalSQL1;
    private String OptionalLabel2;
    private String OptionalSQL2;
    private String NoteLabel;
    private String NoteSQLCmd;
    private String dtmUpd;
    private String usrUpd;
    
    private String OptionalLabel3;
    private String OptionalSQL3;
    private String OptionalLabel4;
    private String OptionalSQL4;
    private String OptionalLabel5;
    private String OptionalSQL5;
    private String OptionalLabel6;
    private String OptionalSQL6;
    private String OptionalLabel7;
    private String OptionalSQL7;
    private String OptionalLabel8;
    private String OptionalSQL8;
    private String OptionalLabel9;
    private String OptionalSQL9;
    private String OptionalLabel10;
    private String OptionalSQL10;
    private String OptionalLabel11;
    private String OptionalSQL11;
    private String OptionalLabel12;
    private String OptionalSQL12;
    private String OptionalLabel13;
    private String OptionalSQL13;
    private String OptionalLabel14;
    private String OptionalSQL14;
    private String OptionalLabel15;
    private String OptionalSQL15;
    
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public String getTransactionNoLabel() {
		return TransactionNoLabel;
	}
	public void setTransactionNoLabel(String transactionNoLabel) {
		TransactionNoLabel = transactionNoLabel;
	}
	public String getTransactionNo() {
		return TransactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		TransactionNo = transactionNo;
	}
	public String getOptionalLabel1() {
		return OptionalLabel1;
	}
	public void setOptionalLabel1(String optionalLabel1) {
		OptionalLabel1 = optionalLabel1;
	}
	public String getOptionalSQL1() {
		return OptionalSQL1;
	}
	public void setOptionalSQL1(String optionalSQL1) {
		OptionalSQL1 = optionalSQL1;
	}
	public String getOptionalLabel2() {
		return OptionalLabel2;
	}
	public void setOptionalLabel2(String optionalLabel2) {
		OptionalLabel2 = optionalLabel2;
	}
	public String getOptionalSQL2() {
		return OptionalSQL2;
	}
	public void setOptionalSQL2(String optionalSQL2) {
		OptionalSQL2 = optionalSQL2;
	}
	public String getNoteLabel() {
		return NoteLabel;
	}
	public void setNoteLabel(String noteLabel) {
		NoteLabel = noteLabel;
	}
	public String getNoteSQLCmd() {
		return NoteSQLCmd;
	}
	public void setNoteSQLCmd(String noteSQLCmd) {
		NoteSQLCmd = noteSQLCmd;
	}
	public String getDtmUpd() {
		return dtmUpd;
	}
	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}
	public String getUsrUpd() {
		return usrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}
	public String getOptionalLabel3() {
		return OptionalLabel3;
	}
	public void setOptionalLabel3(String optionalLabel3) {
		OptionalLabel3 = optionalLabel3;
	}
	public String getOptionalSQL3() {
		return OptionalSQL3;
	}
	public void setOptionalSQL3(String optionalSQL3) {
		OptionalSQL3 = optionalSQL3;
	}
	public String getOptionalLabel4() {
		return OptionalLabel4;
	}
	public void setOptionalLabel4(String optionalLabel4) {
		OptionalLabel4 = optionalLabel4;
	}
	public String getOptionalSQL4() {
		return OptionalSQL4;
	}
	public void setOptionalSQL4(String optionalSQL4) {
		OptionalSQL4 = optionalSQL4;
	}
	public String getOptionalLabel5() {
		return OptionalLabel5;
	}
	public void setOptionalLabel5(String optionalLabel5) {
		OptionalLabel5 = optionalLabel5;
	}
	public String getOptionalSQL5() {
		return OptionalSQL5;
	}
	public void setOptionalSQL5(String optionalSQL5) {
		OptionalSQL5 = optionalSQL5;
	}
	public String getOptionalLabel6() {
		return OptionalLabel6;
	}
	public void setOptionalLabel6(String optionalLabel6) {
		OptionalLabel6 = optionalLabel6;
	}
	public String getOptionalSQL6() {
		return OptionalSQL6;
	}
	public void setOptionalSQL6(String optionalSQL6) {
		OptionalSQL6 = optionalSQL6;
	}
	public String getOptionalLabel7() {
		return OptionalLabel7;
	}
	public void setOptionalLabel7(String optionalLabel7) {
		OptionalLabel7 = optionalLabel7;
	}
	public String getOptionalSQL7() {
		return OptionalSQL7;
	}
	public void setOptionalSQL7(String optionalSQL7) {
		OptionalSQL7 = optionalSQL7;
	}
	public String getOptionalLabel8() {
		return OptionalLabel8;
	}
	public void setOptionalLabel8(String optionalLabel8) {
		OptionalLabel8 = optionalLabel8;
	}
	public String getOptionalSQL8() {
		return OptionalSQL8;
	}
	public void setOptionalSQL8(String optionalSQL8) {
		OptionalSQL8 = optionalSQL8;
	}
	public String getOptionalLabel9() {
		return OptionalLabel9;
	}
	public void setOptionalLabel9(String optionalLabel9) {
		OptionalLabel9 = optionalLabel9;
	}
	public String getOptionalSQL9() {
		return OptionalSQL9;
	}
	public void setOptionalSQL9(String optionalSQL9) {
		OptionalSQL9 = optionalSQL9;
	}
	public String getOptionalLabel10() {
		return OptionalLabel10;
	}
	public void setOptionalLabel10(String optionalLabel10) {
		OptionalLabel10 = optionalLabel10;
	}
	public String getOptionalSQL10() {
		return OptionalSQL10;
	}
	public void setOptionalSQL10(String optionalSQL10) {
		OptionalSQL10 = optionalSQL10;
	}
	public String getOptionalLabel11() {
		return OptionalLabel11;
	}
	public void setOptionalLabel11(String optionalLabel11) {
		OptionalLabel11 = optionalLabel11;
	}
	public String getOptionalSQL11() {
		return OptionalSQL11;
	}
	public void setOptionalSQL11(String optionalSQL11) {
		OptionalSQL11 = optionalSQL11;
	}
	public String getOptionalLabel12() {
		return OptionalLabel12;
	}
	public void setOptionalLabel12(String optionalLabel12) {
		OptionalLabel12 = optionalLabel12;
	}
	public String getOptionalSQL12() {
		return OptionalSQL12;
	}
	public void setOptionalSQL12(String optionalSQL12) {
		OptionalSQL12 = optionalSQL12;
	}
	public String getOptionalLabel13() {
		return OptionalLabel13;
	}
	public void setOptionalLabel13(String optionalLabel13) {
		OptionalLabel13 = optionalLabel13;
	}
	public String getOptionalSQL13() {
		return OptionalSQL13;
	}
	public void setOptionalSQL13(String optionalSQL13) {
		OptionalSQL13 = optionalSQL13;
	}
	public String getOptionalLabel14() {
		return OptionalLabel14;
	}
	public void setOptionalLabel14(String optionalLabel14) {
		OptionalLabel14 = optionalLabel14;
	}
	public String getOptionalSQL14() {
		return OptionalSQL14;
	}
	public void setOptionalSQL14(String optionalSQL14) {
		OptionalSQL14 = optionalSQL14;
	}
	public String getOptionalLabel15() {
		return OptionalLabel15;
	}
	public void setOptionalLabel15(String optionalLabel15) {
		OptionalLabel15 = optionalLabel15;
	}
	public String getOptionalSQL15() {
		return OptionalSQL15;
	}
	public void setOptionalSQL15(String optionalSQL15) {
		OptionalSQL15 = optionalSQL15;
	}

}
