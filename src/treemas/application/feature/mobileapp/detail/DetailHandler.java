package treemas.application.feature.mobileapp.detail;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import treemas.application.feature.mobileapp.usermobile.UserMobileValidator;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

public class DetailHandler extends AppAdminActionBase {
	  //  private DetailValidator validator;
		DetailManager manager = new DetailManager();
		DetailValidator validator;
		
		public ActionForward doAction(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response)
				throws AppException {
			
			//this.validator = new DetailValidator(request);
			
			DetailForm frm = (DetailForm) form;
			frm.setTitle(Global.TITLE_APP_DETAIL);
			frm.getPaging().calculationPage();
			String task = frm.getTask();
			if (Global.WEB_TASK_SHOW.equals(task)
					|| Global.WEB_TASK_LOAD.equals(task)
					|| Global.WEB_TASK_SEARCH.equals(task)) {
				this.load(request, frm);
				} else if (Global.WEB_TASK_DELETE.equals(task)) {
		            this.delete(request, frm);
		            this.load(request, frm);
		        } else if (Global.WEB_TASK_EDIT.equals(task)) { 
		            frm.setTitle(Global.TITLE_APP_USER);
		            this.loadSingle(frm);
		            
		        }else if (Global.WEB_TASK_UPDATE.equals(task)) {            
	                this.updateDetail(request, frm);
	            this.load(request, frm);
		        }	
			return this.getNextPage(task, mapping);
		
		}
		
		  private void clearField(DetailForm frm) {
		        frm.setApprovalSchemeID("");
		        frm.setTransactionNoLabel("");
		        frm.setTransactionNo("");
		        frm.setOptionalLabel1("");
		        frm.setOptionalSQL1("");
		        frm.setOptionalLabel2("");
		        frm.setOptionalSQL2("");
		        frm.setNoteLabel("");
		        frm.setNoteSQLCmd("");
		        frm.setDtmUpd("");
		        frm.setUsrUpd("");
		     
		    }
		
		private DetailBean convertToBean(DetailForm form) throws AppException {
			DetailBean bean = form.toBean();
			return bean;
		}
		private void load(HttpServletRequest request, DetailForm form)
				throws CoreException {
			int pageNumber = form.getPaging().getTargetPageNo();
			if (Global.WEB_TASK_SEARCH.equals(form.getTask())
					|| Global.WEB_TASK_SHOW.equals(form.getTask()))
				pageNumber = 1;
			PageListWrapper result = this.manager.getAllDetail(pageNumber,
					form.getSearch());
			List list = result.getResultList();
			list = BeanConverter.convertBeansToString(list, form.getClass());
			form.getPaging().setCurrentPageNo(pageNumber);
			form.getPaging().setTotalRecord(result.getTotalRecord());
			request.setAttribute("list", list);

		}
		  private void updateDetail(HttpServletRequest request, DetailForm form) throws AppException {
		    	DetailBean bean = this.convertToBean(form, this.getLoginId(request), 0);
		     
		        this.manager.update(bean);
		       

		    }
		 private void loadSingle(DetailForm form) throws CoreException {
			 DetailBean bean = new DetailBean();
		        bean = this.manager.getSingleDetail(form.getApprovalSchemeID());
		        BeanConverter.convertBeanToString(bean, form);

		    }
		 private void delete(HttpServletRequest request, DetailForm form) throws AppException {
		    	DetailBean bean = this.convertToBean(form, this.getLoginId(request), -1);
		      
		        this.manager.delete(bean); 
		        
		    }
		 
		  private  DetailBean convertToBean( DetailForm form, String updateDetail, int flag) throws AppException {
			  DetailBean bean = form.toBean();
		       
		        return bean;
		    }

		public ActionForward handleException(ActionMapping mapping,
				ActionForm form, HttpServletRequest request,
				HttpServletResponse response, AppException ex) {
			Integer code = ex.getErrorCode();
			ActionMessages msgs = null;
			Object obj = ex.getUserObject();
			if (obj instanceof ActionMessages)
				msgs = (ActionMessages) obj;
			switch (code) {
			case ErrorCode.ERR_VALIDATION_PAGE_DATA:
				this.saveErrors(request, msgs);
				break;
			default:
				super.handleException(mapping, form, request, response, ex);
				break;
			}
			((DetailForm) form).setTask(((DetailForm) form)
					.resolvePreviousTask());

			String task = ((DetailForm) form).getTask();
			return this.getErrorPage(task, mapping);
		}

	}
