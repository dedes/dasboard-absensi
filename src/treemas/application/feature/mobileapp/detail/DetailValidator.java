package treemas.application.feature.mobileapp.detail;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessages;

import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

public class DetailValidator extends Validator {
	public DetailValidator(HttpServletRequest request) {
		super(request);
	}

	public DetailValidator(Locale locale) {
		super(locale);
	}

	
	public boolean validateDetailEdit(ActionMessages messages,
			DetailForm form, DetailManager manager) throws CoreException {
		boolean ret = true;
		if (messages == null)
			messages = new ActionMessages();

		this.textValidator(messages, form.getTransactionNoLabel(), "Detail.TransactionNoLabel",
				"TransactionNoLabel", "1", this.getStringLength(150),
				ValidatorGlobal.TM_CHECKED_LENGTH_RANGE,
				ValidatorGlobal.TM_FLAG_MANDATORY,
				ValidatorGlobal.TM_FLAG_FORMAT_CHAR);

		if (messages != null && !messages.isEmpty())
			throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA,
					messages);

		return ret;
	}

}


