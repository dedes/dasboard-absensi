package treemas.application.feature.mobileapp.detail;

import treemas.base.search.SearchForm;

public class DetailSearch extends SearchForm {

	private String ApprovalSchemeID;
    private String TransactionNoLabel;
    private String TransactionNo;
    private String OptionalLabel1;
    private String OptionalSQL1;
    private String OptionalLabel2;
    private String OptionalSQL2;
    private String NoteLabel;
    private String NoteSQLCmd;
    private String dtmUpd;
    private String usrUpd;
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public String getTransactionNoLabel() {
		return TransactionNoLabel;
	}
	public void setTransactionNoLabel(String transactionNoLabel) {
		TransactionNoLabel = transactionNoLabel;
	}
	public String getTransactionNo() {
		return TransactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		TransactionNo = transactionNo;
	}
	public String getOptionalLabel1() {
		return OptionalLabel1;
	}
	public void setOptionalLabel1(String optionalLabel1) {
		OptionalLabel1 = optionalLabel1;
	}
	public String getOptionalSQL1() {
		return OptionalSQL1;
	}
	public void setOptionalSQL1(String optionalSQL1) {
		OptionalSQL1 = optionalSQL1;
	}
	public String getOptionalLabel2() {
		return OptionalLabel2;
	}
	public void setOptionalLabel2(String optionalLabel2) {
		OptionalLabel2 = optionalLabel2;
	}
	public String getOptionalSQL2() {
		return OptionalSQL2;
	}
	public void setOptionalSQL2(String optionalSQL2) {
		OptionalSQL2 = optionalSQL2;
	}
	public String getNoteLabel() {
		return NoteLabel;
	}
	public void setNoteLabel(String noteLabel) {
		NoteLabel = noteLabel;
	}
	public String getNoteSQLCmd() {
		return NoteSQLCmd;
	}
	public void setNoteSQLCmd(String noteSQLCmd) {
		NoteSQLCmd = noteSQLCmd;
	}
	public String getDtmUpd() {
		return dtmUpd;
	}
	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}
	public String getUsrUpd() {
		return usrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}
}
