package treemas.application.feature.mobileapp.auditrail;

import treemas.base.search.SearchForm;

public class AuditrailSearch extends SearchForm {
	private String AUDITRAILID;
    private String transactionDate;
	private String transactionDateAfter;
	private String dateKey = "";
	private String ACTION;
    private String ApprovalNO;
    private String ApprovalID;
    private String ApprovalSchemeID;
    private String UserRequest;
    private String TransactionNo;
    private String UserApproval;
    private String ApprovalStatus;
    private String ApprovalResult;
	public String getAUDITRAILID() {
		return AUDITRAILID;
	}
	public void setAUDITRAILID(String aUDITRAILID) {
		AUDITRAILID = aUDITRAILID;
	}
	
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	public String getDateKey() {
		return dateKey;
	}
	public void setDateKey(String dateKey) {
		this.dateKey = dateKey;
	}
	public String getTransactionDateAfter() {
		return transactionDateAfter;
	}
	public void setTransactionDateAfter(String transactionDateAfter) {
		this.transactionDateAfter = transactionDateAfter;
	}

	public String getACTION() {
		return ACTION;
	}
	public void setACTION(String aCTION) {
		ACTION = aCTION;
	}
	public String getApprovalNO() {
		return ApprovalNO;
	}
	public void setApprovalNO(String approvalNO) {
		ApprovalNO = approvalNO;
	}
	public String getApprovalID() {
		return ApprovalID;
	}
	public void setApprovalID(String approvalID) {
		ApprovalID = approvalID;
	}
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public String getUserRequest() {
		return UserRequest;
	}
	public void setUserRequest(String userRequest) {
		UserRequest = userRequest;
	}
	public String getTransactionNo() {
		return TransactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		TransactionNo = transactionNo;
	}
	public String getUserApproval() {
		return UserApproval;
	}
	public void setUserApproval(String userApproval) {
		UserApproval = userApproval;
	}
	public String getApprovalStatus() {
		return ApprovalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		ApprovalStatus = approvalStatus;
	}
	public String getApprovalResult() {
		return ApprovalResult;
	}
	public void setApprovalResult(String approvalResult) {
		ApprovalResult = approvalResult;
	}
}
