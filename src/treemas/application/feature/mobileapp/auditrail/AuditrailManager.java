package treemas.application.feature.mobileapp.auditrail;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.google.common.base.Strings;

import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

public class AuditrailManager  extends AppAdminManagerBase{
	private final String STRING_SQL = SQLConstant.SQL_AUDITRAIL;

	public PageListWrapper getAllAuditrail(int pageNumber,
			AuditrailSearch auditrailSearch) throws CoreException {
		PageListWrapper result = new PageListWrapper();
		auditrailSearch.setPage(pageNumber);		
	
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get All Auditrail");
			List list = this.ibatisSqlMap.queryForList(this.STRING_SQL
					+ "getAllAuditrail", auditrailSearch);
			Integer count = (Integer) this.ibatisSqlMap.queryForObject(
					this.STRING_SQL + "countAllAuditrail", auditrailSearch);
			result.setResultList(list);
			result.setTotalRecord(count.intValue());
			this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM,
					"Execute Query Get All Auditrail : " + auditrailSearch);
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get All Auditrail Sukses");
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}
		System.out.println(auditrailSearch.getTransactionDate());
		System.out.println(auditrailSearch.getTransactionDateAfter());
		return result;
	}
	
			
	
	
	public List getAllAuditrail(String AUDITRAILID,  String ApprovalNO, String ApprovalID, String DateBefore,  String DateAfter, String ApprovalSchemeID ) throws CoreException {
		List list = null;
		
		Map param = new HashedMap();
		String Id = !Strings.isNullOrEmpty(AUDITRAILID) ? AUDITRAILID : "";
		String apr = !Strings.isNullOrEmpty(ApprovalNO) ? ApprovalNO : "";
		String act = !Strings.isNullOrEmpty(ApprovalID) ? ApprovalID : "";
		String app= !Strings.isNullOrEmpty(ApprovalSchemeID) ? ApprovalSchemeID : "";
		String transactionDate= !Strings.isNullOrEmpty(DateBefore) ? DateBefore : "";
		String transactionDateAfter= !Strings.isNullOrEmpty(DateAfter) ? DateAfter : "";
		
	
		
		param.put("AUDITRAILID", Id);
		param.put("ApprovalNO", apr);
		param.put("ApprovalID", act);
		param.put("ApprovalSchemeID", app);
		param.put("transactionDate", transactionDate);		
		param.put("transactionDateAfter", transactionDateAfter);
		

		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get All Auditrail");
			list  = this.ibatisSqlMap.queryForList(this.STRING_SQL+ "getAllReport", param);
			
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return list;
	}
	
	

	

}



