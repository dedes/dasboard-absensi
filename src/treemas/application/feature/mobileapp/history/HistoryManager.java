package treemas.application.feature.mobileapp.history;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.google.common.base.Strings;


import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

public class HistoryManager  extends AppAdminManagerBase{
	private final String STRING_SQL = SQLConstant.SQL_HISTORY;

	public PageListWrapper getAllHistory(int pageNumber,
		HistorySearch search) throws CoreException {
		PageListWrapper result = new PageListWrapper();
		search.setPage(pageNumber);
		this.logger.log(ILogger.LEVEL_INFO,
		"Execute Query get All History ");
		try {
			this.logger.log(ILogger.LEVEL_INFO,
					"Execute Query Get All History");
			List list = this.ibatisSqlMap.queryForList(this.STRING_SQL
					+ "getAllHistory", search);
			Integer count = (Integer) this.ibatisSqlMap.queryForObject(
					this.STRING_SQL + "countAllHistory", search);
			result.setResultList(list);
			result.setTotalRecord(count.intValue());
			this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM,
					"Execute Query Get AllHistory : " + search);
			this.logger.log(ILogger.LEVEL_INFO,
					"Execute Query Get AllHistory Sukses");
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return result;
	}
	
	public List getAllHistory(String ApprovalNo,  String ApprovalID,String ApprovalSchemeID, String ApprovalStatus, String TransactionNo, String IsApprovedByMobile, String IsFinal) throws CoreException {
		List list = null;
		
		Map param = new HashedMap();
		String Id = !Strings.isNullOrEmpty(ApprovalNo) ? ApprovalNo : "";
		String label = !Strings.isNullOrEmpty(ApprovalID) ? ApprovalID : "";
		String actionno = !Strings.isNullOrEmpty(ApprovalSchemeID) ? ApprovalSchemeID : "";
		String app= !Strings.isNullOrEmpty(ApprovalStatus) ? ApprovalStatus : "";
		String st= !Strings.isNullOrEmpty(TransactionNo) ? TransactionNo : "";
		String by= !Strings.isNullOrEmpty(IsApprovedByMobile) ? IsApprovedByMobile : "";
		String fin= !Strings.isNullOrEmpty(IsFinal) ? IsFinal : "";
	
		
		
		System.out.println(ApprovalNo);
		System.out.println(ApprovalID);
		System.out.println(ApprovalSchemeID);
		System.out.println(ApprovalStatus);
		System.out.println(TransactionNo);
		System.out.println(IsApprovedByMobile);
		System.out.println(IsFinal);
		
		
		
		param.put("ApprovalNo", Id);
		param.put("ApprovalID", label);
		param.put("ApprovalSchemeID", actionno);
		param.put("ApprovalStatus", app);
		param.put("TransactionNo", st);
		param.put("IsApprovedByMobile", by);
		param.put("IsFinal", fin);
		
	
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get All History");
			list  = this.ibatisSqlMap.queryForList(this.STRING_SQL+ "getAllReport", param);
	
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return list;
	}
	
	

	

}




