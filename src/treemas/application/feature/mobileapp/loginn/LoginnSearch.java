package  treemas.application.feature.mobileapp.loginn;

import treemas.base.search.SearchForm;

public class LoginnSearch extends SearchForm{
	private String AUDITRAILID;
    private String transactionDate;
  	private String transactionDateAfter;
    private String LOGINID;
    private String ACTIVITY;
    private String USRUPD;
    private String DTMUPD;
	private String dateKey = "";
	public String getAUDITRAILID() {
		return AUDITRAILID;
	}
	public void setAUDITRAILID(String aUDITRAILID) {
		AUDITRAILID = aUDITRAILID;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getTransactionDateAfter() {
		return transactionDateAfter;
	}
	public void setTransactionDateAfter(String transactionDateAfter) {
		this.transactionDateAfter = transactionDateAfter;
	}
	public String getLOGINID() {
		return LOGINID;
	}
	public void setLOGINID(String lOGINID) {
		LOGINID = lOGINID;
	}
	public String getACTIVITY() {
		return ACTIVITY;
	}
	public void setACTIVITY(String aCTIVITY) {
		ACTIVITY = aCTIVITY;
	}
	public String getUSRUPD() {
		return USRUPD;
	}
	public String getDateKey() {
		return dateKey;
	}
	public void setDateKey(String dateKey) {
		this.dateKey = dateKey;
	}
	public void setUSRUPD(String uSRUPD) {
		USRUPD = uSRUPD;
	}
	public String getDTMUPD() {
		return DTMUPD;
	}
	public void setDTMUPD(String dTMUPD) {
		DTMUPD = dTMUPD;
	}
    
}