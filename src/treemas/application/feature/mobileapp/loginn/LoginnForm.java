package treemas.application.feature.mobileapp.loginn;


import treemas.base.appadmin.AppAdminFormBase;
import treemas.util.beanaction.BeanConverter;

public class LoginnForm extends AppAdminFormBase{

	private String AUDITRAILID;
    private String transactionDate;
    private String LOGINID;
    private String ACTIVITY;
    private String USRUPD;
    private String DTMUPD;
    private String dateKey;
	public String getAUDITRAILID() {
		return AUDITRAILID;
	}
	public void setAUDITRAILID(String aUDITRAILID) {
		AUDITRAILID = aUDITRAILID;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getLOGINID() {
		return LOGINID;
	}
	public void setLOGINID(String lOGINID) {
		LOGINID = lOGINID;
	}
	public String getACTIVITY() {
		return ACTIVITY;
	}
	public void setACTIVITY(String aCTIVITY) {
		ACTIVITY = aCTIVITY;
	}
	public String getUSRUPD() {
		return USRUPD;
	}
	public void setUSRUPD(String uSRUPD) {
		USRUPD = uSRUPD;
	}
	public String getDTMUPD() {
		return DTMUPD;
	}
	public void setDTMUPD(String dTMUPD) {
		DTMUPD = dTMUPD;
	}
	public String getDateKey() {
		return dateKey;
	}
	public void setDateKey(String dateKey) {
		this.dateKey = dateKey;
	}
	public LoginnBean toBean() {
		LoginnBean bean = new LoginnBean();
        BeanConverter.convertBeanFromString(this, bean);
     
        return bean;
    }
	
    private LoginnSearch search = new LoginnSearch();
	public LoginnSearch getSearch() {
		return search;
	}
	public void setSearch(LoginnSearch search) {
		this.search = search;
		
		

}
}

