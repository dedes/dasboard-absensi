package treemas.application.feature.mobileapp.loginn;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.google.common.base.Strings;


import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

public class LoginnManager extends AppAdminManagerBase{
	private final String STRING_SQL = SQLConstant.SQL_LOGINN ;

	public PageListWrapper getAllLoginn(int pageNumber,
			LoginnSearch LoginnSearch) throws CoreException {
		PageListWrapper result = new PageListWrapper();
		LoginnSearch.setPage(pageNumber);
		this.logger.log(ILogger.LEVEL_INFO,
		"Execute Query get All Loginn ");
		
		
		try {
			this.logger.log(ILogger.LEVEL_INFO,
					"Execute Query Get All Loginn");
			List list = this.ibatisSqlMap.queryForList(this.STRING_SQL
					+ "getAllLoginn", LoginnSearch);
			Integer count = (Integer) this.ibatisSqlMap.queryForObject(
					this.STRING_SQL + "countAllLoginn", LoginnSearch);
			result.setResultList(list);
			result.setTotalRecord(count.intValue());
			this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM,
					"Execute Query Get All Loginn : " + LoginnSearch);
			this.logger.log(ILogger.LEVEL_INFO,
					"Execute Query Get All Loginn Sukses");
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return result;
	}
	
	public List getAllLoginn(String AUDITRAILID, String LOGINID, String DateBefore,  String DateAfter, String ACTIVITY) throws CoreException {
		List list = null;
		
		Map param = new HashedMap();
		String Id = !Strings.isNullOrEmpty(AUDITRAILID) ? AUDITRAILID : "";
		String log = !Strings.isNullOrEmpty(LOGINID) ? LOGINID : "";
		String act= !Strings.isNullOrEmpty(ACTIVITY) ? ACTIVITY : "";
		String transactionDate= !Strings.isNullOrEmpty(DateBefore) ? DateBefore : "";
		String transactionDateAfter= !Strings.isNullOrEmpty(DateAfter) ? DateAfter : "";

		
		
		
		System.out.println(AUDITRAILID);
		System.out.println(transactionDate);
		System.out.println(LOGINID);
		System.out.println(ACTIVITY);
		param.put("transactionDate", transactionDate);		
		param.put("transactionDateAfter", transactionDateAfter);

		
		
		param.put("AUDITRAILID", Id);
		param.put("LOGINID", log);
		param.put("ACTIVITY", act);
		param.put("transactionDate", transactionDate);		
		param.put("transactionDateAfter", transactionDateAfter);
	
		
		
	
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get AllLoginn");
			list  = this.ibatisSqlMap.queryForList(this.STRING_SQL+ "getAllauditlogin", param);
	
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return list;
	}
	
	

	

}




