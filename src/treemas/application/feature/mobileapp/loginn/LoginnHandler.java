package treemas.application.feature.mobileapp.loginn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import com.google.common.base.Strings;

import treemas.application.audit.AuditForm;
import treemas.application.feature.mobileapp.usermobile.UserMobileValidator;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

public class LoginnHandler  extends AppAdminActionBase {
	 //  private AuditrailValidator validator;
		LoginnManager manager = new LoginnManager();
		LoginnValidator validator;
		
		public ActionForward doAction(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response)
				throws AppException {
			
			//this.validator = new AuditrailValidator(request);
			
			LoginnForm frm = (LoginnForm) form;
			frm.setTitle(Global.TITLE_APP_LOGINN);
			frm.getPaging().calculationPage();
			String task = frm.getTask();
			if (Global.WEB_TASK_SHOW.equals(task)
					|| Global.WEB_TASK_LOAD.equals(task)
					|| Global.WEB_TASK_SEARCH.equals(task)) {
				this.load(request, frm);
				} 
		
			return this.getNextPage(task, mapping);

		}
		private LoginnBean convertToBean(LoginnForm form) throws AppException {
			LoginnBean bean = form.toBean();
			return bean;
		}
		private void load(HttpServletRequest request, LoginnForm form) throws AppException {
			int pageNumber = form.getPaging().getTargetPageNo();
			if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
				pageNumber = 1;
			
		
				String before = form.getSearch().getTransactionDate();
				String after = form.getSearch().getTransactionDateAfter();
				if(Strings.isNullOrEmpty(before) && Strings.isNullOrEmpty(after)){
					PageListWrapper result = this.manager.getAllLoginn(pageNumber,
							form.getSearch());
					List list = result.getResultList();
					list = BeanConverter.convertBeansToString(list, form.getClass());
					form.getPaging().setCurrentPageNo(pageNumber);
					form.getPaging().setTotalRecord(result.getTotalRecord());
					request.setAttribute("list", list);
					}
				else if(!Strings.isNullOrEmpty(before) && !Strings.isNullOrEmpty(after)){
					PageListWrapper result = this.manager.getAllLoginn(pageNumber,
							form.getSearch());
					List list = result.getResultList();
					list = BeanConverter.convertBeansToString(list, form.getClass());
					form.getPaging().setCurrentPageNo(pageNumber);
					form.getPaging().setTotalRecord(result.getTotalRecord());
					request.setAttribute("list", list);
					}	
				else if(!Strings.isNullOrEmpty(before) || !Strings.isNullOrEmpty(after)){
					if (!Strings.isNullOrEmpty(before)){
						form.getSearch().setTransactionDateAfter(before);	
					}
					if (!Strings.isNullOrEmpty(after)){
						form.getSearch().setTransactionDate(after);
					}
					
					
					PageListWrapper result = this.manager.getAllLoginn(pageNumber,
							form.getSearch());
					List list = result.getResultList();
					list = BeanConverter.convertBeansToString(list, form.getClass());
					form.getPaging().setCurrentPageNo(pageNumber);
					form.getPaging().setTotalRecord(result.getTotalRecord());
					request.setAttribute("list", list);
					}
				
				
			

		}
		 protected String getLang(HttpServletRequest request){
			   String lang = this.getLoginBean(request).getDwilingual();
			   if(Strings.isNullOrEmpty(lang)){
				   lang = "ID";
			   }
			   return lang;
		   }
		public ActionForward handleException(ActionMapping mapping,
				ActionForm form, HttpServletRequest request,
				HttpServletResponse response, AppException ex) {
			Integer code = ex.getErrorCode();
			ActionMessages msgs = null;
			Object obj = ex.getUserObject();
			if (obj instanceof ActionMessages)
				msgs = (ActionMessages) obj;
			switch (code) {
			case ErrorCode.ERR_VALIDATION_PAGE_DATA:
				this.saveErrors(request, msgs);
				break;
			default:
				super.handleException(mapping, form, request, response, ex);
				break;
			}
			((LoginnForm) form).setTask(((LoginnForm) form)
					.resolvePreviousTask());

			String task = ((LoginnForm) form).getTask();
			return this.getErrorPage(task, mapping);
		}
	}



