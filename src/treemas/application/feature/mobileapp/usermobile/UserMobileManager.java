package treemas.application.feature.mobileapp.usermobile;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.constant.GlobalTable;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

public class UserMobileManager extends AppAdminManagerBase {
	private final String STRING_SQL = SQLConstant.SQL_MOBAPP_USER;

	public PageListWrapper getAllUser(int pageNumber, UserMobileSearch search) throws CoreException {
		PageListWrapper result = new PageListWrapper();
		search.setPage(pageNumber);
		
		try {
			List list = this.ibatisSqlMap.queryForList(this.STRING_SQL+"getAllUser",search);
			Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL+"countAllUser",search);
			result.setResultList(list);
			result.setTotalRecord(count.intValue());
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}
		
		return result;
	}	
	
	public void releaseLogin(String loginId, String userUpdate) throws CoreException {
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute query Release Login");
			this.releaseLogin(loginId, userUpdate, Global.RELEASE_LOGIN);
			this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "Execute query Release Login : "+loginId);
		} catch (AppException e) {
			this.logger.printStackTrace(e);
		}
	}
	private void releaseLogin(String loginId, String userUpdate, String type) throws CoreException {
		try {
			Map paramMap = new HashMap();
			paramMap.put("loginId", loginId);
			paramMap.put("userUpdate", loginId);
			paramMap.put("type", type);
			try{
				this.ibatisSqlMap.startTransaction();
				this.ibatisSqlMap.update(this.STRING_SQL+"releaseLogin", paramMap);			
				this.ibatisSqlMap.commitTransaction();
			}finally{
				this.ibatisSqlMap.endTransaction();
			}
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
		} /*catch (AppException e) {
			this.logger.printStackTrace(e);
		}*/
	}
	
	public void releaseLock(String loginId, String userUpdate) throws CoreException {
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute query Release Lock");
			this.releaseLock(loginId, userUpdate, Global.RELEASE_LOCKED);
			this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "Execute query Release Lock : "+loginId);
			this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "Execute query Release Lock " + loginId + " Sukses");
		} catch (AppException e) {
			this.logger.printStackTrace(e);
		}
	}
	private void releaseLock(String loginId, String userUpdate, String type) throws CoreException {
		try {
			Map paramMap = new HashMap();
			paramMap.put("loginId", loginId);
			paramMap.put("userUpdate", loginId);
			paramMap.put("type", type);
			try{
				this.ibatisSqlMap.startTransaction();
				this.ibatisSqlMap.update(this.STRING_SQL+"releaseLock", paramMap);			
				this.ibatisSqlMap.commitTransaction();
			}finally{
				this.ibatisSqlMap.endTransaction();
			}
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
		} /*catch (AppException e) {
			this.logger.printStackTrace(e);
		}*/
	}
}
