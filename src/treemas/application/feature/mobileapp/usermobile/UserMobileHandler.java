package treemas.application.feature.mobileapp.usermobile;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import treemas.application.user.UserForm;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

public class UserMobileHandler extends AppAdminActionBase {
	
	UserMobileManager manager = new UserMobileManager();
	UserMobileValidator validator;

	  public UserMobileHandler() {
	        pageMap.put(Global.WEB_TASK_RELEASE, "view");
	        pageMap.put(Global.WEB_TASK_RELEASE_LOCK, "view");
	        pageMap.put(Global.WEB_TASK_ENABLE, "view");
	        pageMap.put(Global.WEB_TASK_DISABLE, "view");
	        pageMap.put(Global.WEB_TASK_RESET, "view");

	        exceptionMap.put(Global.WEB_TASK_RELEASE, "view");
	        exceptionMap.put(Global.WEB_TASK_RELEASE_LOCK, "view");
	        exceptionMap.put(Global.WEB_TASK_ENABLE, "view");
	        exceptionMap.put(Global.WEB_TASK_DISABLE, "view");
	        exceptionMap.put(Global.WEB_TASK_RESET, "view");
	    }
	
	@Override
	public ActionForward doAction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppException {

		this.validator = new UserMobileValidator(request);
		UserMobileForm frm = (UserMobileForm) form;
		frm.setTitle(Global.TITLE_MOB_USER);
		frm.getPaging().calculationPage();
		String task = frm.getTask();
		if (Global.WEB_TASK_SHOW.equals(task) || Global.WEB_TASK_LOAD.equals(task) || Global.WEB_TASK_SEARCH.equals(task)) {
			this.load(request, frm);
		}else if(Global.WEB_TASK_RELEASE.equals(task)){
			this.releaseLogin(request, frm);
			this.load(request, frm);
		}else if(Global.WEB_TASK_RELEASE_LOCK.equals(task)){
			this.releaseLock(request, frm);
			this.load(request, frm);
		}
		
		return this.getNextPage(task, mapping);
	}
	
	private void load(HttpServletRequest request, UserMobileForm form) throws CoreException {
		int pageNumber = form.getPaging().getTargetPageNo();
		if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
			pageNumber = 1;
		PageListWrapper result = this.manager.getAllUser(pageNumber, form.getSearch());
		List list = result.getResultList();
		list = BeanConverter.convertBeansToString(list, form.getClass());
		form.getPaging().setCurrentPageNo(pageNumber);
		form.getPaging().setTotalRecord(result.getTotalRecord());
		request.setAttribute("list", list);	
	}
	private void releaseLogin (HttpServletRequest request, UserMobileForm form) throws CoreException{
		this.manager.releaseLogin(form.getLoginid(), this.getLoginId(request));
	}
	private void releaseLock(HttpServletRequest request, UserMobileForm form) throws CoreException{
		this.manager.releaseLock(form.getLoginid(), this.getLoginId(request));
	}
	
	public ActionForward handleException(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
			msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			UserMobileForm frm =  (UserMobileForm)form;
			try {
				this.load(request,frm);
				
			} catch (AppException e) {
//				this.logger.printStackTrace(e);
			}
			this.saveErrors(request, msgs);
			break;
		default:
			super.handleException(mapping, form, request, response, ex);
			break;
		}
		((UserMobileForm) form).setTask(((UserMobileForm) form).resolvePreviousTask());

		String task = ((UserMobileForm) form).getTask();
		return this.getErrorPage(task, mapping);
	}

}
