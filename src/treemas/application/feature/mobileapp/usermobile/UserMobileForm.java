package treemas.application.feature.mobileapp.usermobile;

import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.appadmin.AppAdminFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class UserMobileForm extends AppAdminFormBase {
	
	private String loginid;
	private String fullname;
	private String active;
	private String sqlpassword;
	private String usercrt;
	private String dtmcrt;
	private String usrupd;
	private String dtmupd;
	private String islogin;
	private String lastlogin;
	private String wrongpass_count;
	private String wrongpass_time;
	private String islocked;
	private String handset_imei;
	
	private UserMobileSearch search = new UserMobileSearch();


	public String getLoginid() {
		return loginid;
	}

	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}


	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getSqlpassword() {
		return sqlpassword;
	}

	public void setSqlpassword(String sqlpassword) {
		this.sqlpassword = sqlpassword;
	}

	public String getUsercrt() {
		return usercrt;
	}

	public void setUsercrt(String usercrt) {
		this.usercrt = usercrt;
	}

	public String getDtmcrt() {
		return dtmcrt;
	}

	public void setDtmcrt(String dtmcrt) {
		this.dtmcrt = dtmcrt;
	}

	public String getUsrupd() {
		return usrupd;
	}

	public void setUsrupd(String usrupd) {
		this.usrupd = usrupd;
	}

	public String getDtmupd() {
		return dtmupd;
	}

	public void setDtmupd(String dtmupd) {
		this.dtmupd = dtmupd;
	}

	public String getIslogin() {
		return islogin;
	}

	public void setIslogin(String islogin) {
		this.islogin = islogin;
	}

	public String getLastlogin() {
		return lastlogin;
	}

	public void setLastlogin(String lastlogin) {
		this.lastlogin = lastlogin;
	}

	public String getWrongpass_count() {
		return wrongpass_count;
	}

	public void setWrongpass_count(String wrongpass_count) {
		this.wrongpass_count = wrongpass_count;
	}

	public String getWrongpass_time() {
		return wrongpass_time;
	}

	public void setWrongpass_time(String wrongpass_time) {
		this.wrongpass_time = wrongpass_time;
	}

	public String getIslocked() {
		return islocked;
	}

	public void setIslocked(String islocked) {
		this.islocked = islocked;
	}

	public String getHandset_imei() {
		return handset_imei;
	}

	public void setHandset_imei(String handset_imei) {
		this.handset_imei = handset_imei;
	}

	public UserMobileSearch getSearch() {
		return search;
	}

	public void setSearch(UserMobileSearch search) {
		this.search = search;
	}
	
	public UserMobileBean toBean(){
		UserMobileBean bean = new UserMobileBean();
		BeanConverter.convertBeanFromString(this, bean);
		return bean;
	}

}
