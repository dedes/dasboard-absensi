package treemas.application.feature.mobileapp.usermobile;

import java.io.Serializable;
import java.util.Date;

import treemas.base.ApplicationBean;

public class UserMobileBean extends ApplicationBean implements Serializable {
	
	private String loginid;
	private String fullname;
	private String active;
	private String sqlpassword;
	private String usercrt;
	private Date dtmcrt;
	private String usrupd;
	private Date dtmupd;
	private String islogin;
	private String lastlogin;
	private Integer wrongpass_count;
	private Date wrongpass_time;
	private String islocked;
	private String handset_imei;
	
	public UserMobileBean() {
	}


	public String getLoginid() {
		return loginid;
	}


	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}


	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getSqlpassword() {
		return sqlpassword;
	}

	public void setSqlpassword(String sqlpassword) {
		this.sqlpassword = sqlpassword;
	}

	public String getUsercrt() {
		return usercrt;
	}

	public void setUsercrt(String usercrt) {
		this.usercrt = usercrt;
	}

	public Date getDtmcrt() {
		return dtmcrt;
	}

	public void setDtmcrt(Date dtmcrt) {
		this.dtmcrt = dtmcrt;
	}

	public String getUsrupd() {
		return usrupd;
	}

	public void setUsrupd(String usrupd) {
		this.usrupd = usrupd;
	}

	public Date getDtmupd() {
		return dtmupd;
	}

	public void setDtmupd(Date dtmupd) {
		this.dtmupd = dtmupd;
	}

	public String getIslogin() {
		return islogin;
	}

	public void setIslogin(String islogin) {
		this.islogin = islogin;
	}

	public String getLastlogin() {
		return lastlogin;
	}

	public void setLastlogin(String lastlogin) {
		this.lastlogin = lastlogin;
	}

	public Integer getWrongpass_count() {
		return wrongpass_count;
	}

	public void setWrongpass_count(Integer wrongpass_count) {
		this.wrongpass_count = wrongpass_count;
	}

	public Date getWrongpass_time() {
		return wrongpass_time;
	}

	public void setWrongpass_time(Date wrongpass_time) {
		this.wrongpass_time = wrongpass_time;
	}

	public String getIslocked() {
		return islocked;
	}

	public void setIslocked(String islocked) {
		this.islocked = islocked;
	}

	public String getHandset_imei() {
		return handset_imei;
	}

	public void setHandset_imei(String handset_imei) {
		this.handset_imei = handset_imei;
	}
	
	
}
