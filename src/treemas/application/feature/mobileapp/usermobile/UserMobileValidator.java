package treemas.application.feature.mobileapp.usermobile;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import treemas.util.validator.Validator;


public class UserMobileValidator extends Validator {

	public UserMobileValidator(HttpServletRequest request) {
		super(request);
	}

	public UserMobileValidator(Locale locale) {
		super(locale);
	}
	

}
