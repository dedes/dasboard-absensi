package treemas.application.feature.mobileapp.approvaltype;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.google.common.base.Strings;

import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;


	public class ApprovalTypeManager extends AppAdminManagerBase {

		private final String STRING_SQL = SQLConstant.SQL_APPROVALTYPE;

		public PageListWrapper getAllApprovalType(int pageNumber,
				ApprovalTypeSearch search) throws CoreException {
			PageListWrapper result = new PageListWrapper();
			search.setPage(pageNumber);
			this.logger.log(ILogger.LEVEL_INFO,
			"Execute Query get All ApprovalType ");
			try {
				this.logger.log(ILogger.LEVEL_INFO,
						"Execute Query Get All ApprovalType");
				List list = this.ibatisSqlMap.queryForList(this.STRING_SQL
						+ "getAllApprovalType", search);
				Integer count = (Integer) this.ibatisSqlMap.queryForObject(
						this.STRING_SQL + "countAllApprovalType", search);
				result.setResultList(list);
				result.setTotalRecord(count.intValue());
				this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM,
						"Execute Query Get All ApprovalType : " + search);
				this.logger.log(ILogger.LEVEL_INFO,
						"Execute Query Get All ApprovalType Sukses");
			} catch (SQLException e) {
				this.logger.printStackTrace(e);
				throw new CoreException(ERROR_DB);
			}

			return result;
		}
		
		public List getAllApprovalType(String ApprovalSchemeID,  String ApprovalTypeID,String ApprovalSchemeName) throws CoreException {
			List list = null;
			
			Map param = new HashedMap();
			String Id = !Strings.isNullOrEmpty(ApprovalSchemeID) ? ApprovalSchemeID : "";
			String app = !Strings.isNullOrEmpty(ApprovalTypeID) ? ApprovalTypeID : "";
			String name = !Strings.isNullOrEmpty(ApprovalSchemeName) ? ApprovalSchemeName : "";
			
		
			
			
			System.out.println(ApprovalSchemeID);
			System.out.println(ApprovalTypeID);
			System.out.println(ApprovalSchemeName);
			
			
			
			param.put("ApprovalSchemeID", Id);
			param.put("ApprovalTypeID", app);
			param.put("ApprovalSchemeName", name);
		
		
			
		
			try {
				this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get All ApprovalType");
				list  = this.ibatisSqlMap.queryForList(this.STRING_SQL+ "getAllType", param);
		
			} catch (SQLException e) {
				this.logger.printStackTrace(e);
				throw new CoreException(ERROR_DB);
			}

			return list;
		}
		
		

		

	}



