package treemas.application.feature.mobileapp.approvaltype;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import treemas.application.feature.mobileapp.auditrailtask.AuditrailTaskValidator;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;


	public class ApprovalTypeHandler extends AppAdminActionBase {
		  //  private DetailValidator validator;
		ApprovalTypeManager manager = new ApprovalTypeManager();
		ApprovalTypeValidator validator;
			
			public ActionForward doAction(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws AppException {
				
				//this.validator = new DetailValidator(request);
				
				ApprovalTypeForm frm = (ApprovalTypeForm) form;
				frm.setTitle(Global.TITLE_APP_APPROVALTYPE);
				frm.getPaging().calculationPage();
				String task = frm.getTask();
				if (Global.WEB_TASK_SHOW.equals(task)
						|| Global.WEB_TASK_LOAD.equals(task)
						|| Global.WEB_TASK_SEARCH.equals(task)) {
					this.load(request, frm);
					} 
						
				return this.getNextPage(task, mapping);

			}
			private ApprovalTypeBean convertToBean(ApprovalTypeForm form) throws AppException {
				ApprovalTypeBean bean = form.toBean();
				return bean;
			}
			private void load(HttpServletRequest request, ApprovalTypeForm form)
					throws CoreException {
				int pageNumber = form.getPaging().getTargetPageNo();
				if (Global.WEB_TASK_SEARCH.equals(form.getTask())
						|| Global.WEB_TASK_SHOW.equals(form.getTask()))
					pageNumber = 1;
				PageListWrapper result = this.manager.getAllApprovalType(pageNumber,
						form.getSearch());
				List list = result.getResultList();
				list = BeanConverter.convertBeansToString(list, form.getClass());
				form.getPaging().setCurrentPageNo(pageNumber);
				form.getPaging().setTotalRecord(result.getTotalRecord());
				request.setAttribute("list", list);

			}

			public ActionForward handleException(ActionMapping mapping,
					ActionForm form, HttpServletRequest request,
					HttpServletResponse response, AppException ex) {
				Integer code = ex.getErrorCode();
				ActionMessages msgs = null;
				Object obj = ex.getUserObject();
				if (obj instanceof ActionMessages)
					msgs = (ActionMessages) obj;
				switch (code) {
				case ErrorCode.ERR_VALIDATION_PAGE_DATA:
					this.saveErrors(request, msgs);
					break;
				default:
					super.handleException(mapping, form, request, response, ex);
					break;
				}
				((ApprovalTypeForm) form).setTask(((ApprovalTypeForm) form)
						.resolvePreviousTask());

				String task = ((ApprovalTypeForm) form).getTask();
				return this.getErrorPage(task, mapping);
			}

		}

