package treemas.application.feature.mobileapp.approvaltype;


import treemas.base.appadmin.AppAdminFormBase;
import treemas.util.beanaction.BeanConverter;

public class ApprovalTypeForm extends AppAdminFormBase{
	private String ApprovalSchemeID;
    private String ApprovalTypeID;
    private String ApprovalSchemeName;
	
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public String getApprovalTypeID() {
		return ApprovalTypeID;
	}
	public void setApprovalTypeID(String approvalTypeID) {
		ApprovalTypeID = approvalTypeID;
	}
	public String getApprovalSchemeName() {
		return ApprovalSchemeName;
	}
	public void setApprovalSchemeName(String approvalSchemeName) {
		ApprovalSchemeName = approvalSchemeName;
	}
	public ApprovalTypeBean toBean() {
		ApprovalTypeBean bean = new ApprovalTypeBean();
        BeanConverter.convertBeanFromString(this, bean);
     
        return bean;
    }
	
    private ApprovalTypeSearch search = new ApprovalTypeSearch();
	public ApprovalTypeSearch getSearch() {
		return search;
	}
	public void setSearch(ApprovalTypeSearch search) {
		this.search = search;
		
		

	}
}

