package treemas.application.feature.mobileapp.approvaltype;

import treemas.base.search.SearchForm;

public class ApprovalTypeSearch extends SearchForm {

	private String ApprovalSchemeID;
    private String ApprovalTypeID;
    private String ApprovalSchemeName;
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public String getApprovalTypeID() {
		return ApprovalTypeID;
	}
	public void setApprovalTypeID(String approvalTypeID) {
		ApprovalTypeID = approvalTypeID;
	}
	public String getApprovalSchemeName() {
		return ApprovalSchemeName;
	}
	public void setApprovalSchemeName(String approvalSchemeName) {
		ApprovalSchemeName = approvalSchemeName;
	}
    
}