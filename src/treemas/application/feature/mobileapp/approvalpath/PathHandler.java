package treemas.application.feature.mobileapp.approvalpath;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import com.google.common.base.Strings;
import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import treemas.application.feature.mobileapp.approvaltype.ApprovalTypeValidator;
import treemas.application.user.UserBean;
import treemas.application.user.UserForm;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

public class PathHandler  extends AppAdminActionBase {
	 //  private AuditrailValidator validator;
	PathManager manager = new PathManager();
	PathValidator validator;
	
	public ActionForward doAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws AppException {
		
		//this.validator = new AuditrailValidator(request);
		
		PathForm frm = (PathForm) form;
		frm.setTitle(Global.TITLE_APP_APPROVALPATH);
		frm.getPaging().calculationPage();
		String task = frm.getTask();
		if (Global.WEB_TASK_SHOW.equals(task)
				|| Global.WEB_TASK_LOAD.equals(task)
				|| Global.WEB_TASK_SEARCH.equals(task)) {
			this.load(request, frm);
			} 
				
		return this.getNextPage(task, mapping);

	}
	private PathBean convertToBean(PathForm form) throws AppException {
		PathBean bean = form.toBean();
		return bean;
	}
	private void load(HttpServletRequest request, PathForm form)
			throws CoreException {
		int pageNumber = form.getPaging().getTargetPageNo();
		if (Global.WEB_TASK_SEARCH.equals(form.getTask())
				|| Global.WEB_TASK_SHOW.equals(form.getTask()))
			pageNumber = 1;
		PageListWrapper result = this.manager.getAllPath(pageNumber,
				form.getSearch());
		List list = result.getResultList();
		list = BeanConverter.convertBeansToString(list, form.getClass());
		form.getPaging().setCurrentPageNo(pageNumber);
		form.getPaging().setTotalRecord(result.getTotalRecord());
		request.setAttribute("list", list);

	}
	
	

	public ActionForward handleException(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
			msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			this.saveErrors(request, msgs);
			break;
		default:
			super.handleException(mapping, form, request, response, ex);
			break;
		}
		((PathForm) form).setTask(((PathForm) form)
				.resolvePreviousTask());

		String task = ((PathForm) form).getTask();
		return this.getErrorPage(task, mapping);
	}
}
