package treemas.application.feature.mobileapp.approvalpath;

import treemas.base.ApplicationBean;

	public class PathBean  extends ApplicationBean {
		
		private String ApprovalSchemeID;
	    private String ApprovalSeqNum;
	    private String LoginID;
	    private String LimitValue;
	    private String ProcessDuration;
	    private String AutoUser;
	    private String DtmUpd;
	    private String UsrUpd;
	    private String LimitPercent;
	    private String IsLimitByPercent;
		public String getApprovalSchemeID() {
			return ApprovalSchemeID;
		}
		public void setApprovalSchemeID(String approvalSchemeID) {
			ApprovalSchemeID = approvalSchemeID;
		}
		public String getApprovalSeqNum() {
			return ApprovalSeqNum;
		}
		public void setApprovalSeqNum(String approvalSeqNum) {
			ApprovalSeqNum = approvalSeqNum;
		}
		public String getLoginID() {
			return LoginID;
		}
		public void setLoginID(String loginID) {
			LoginID = loginID;
		}
		public String getLimitValue() {
			return LimitValue;
		}
		public void setLimitValue(String limitValue) {
			LimitValue = limitValue;
		}
		public String getProcessDuration() {
			return ProcessDuration;
		}
		public void setProcessDuration(String processDuration) {
			ProcessDuration = processDuration;
		}
		public String getAutoUser() {
			return AutoUser;
		}
		public void setAutoUser(String autoUser) {
			AutoUser = autoUser;
		}
		public String getDtmUpd() {
			return DtmUpd;
		}
		public void setDtmUpd(String dtmUpd) {
			DtmUpd = dtmUpd;
		}
		public String getUsrUpd() {
			return UsrUpd;
		}
		public void setUsrUpd(String usrUpd) {
			UsrUpd = usrUpd;
		}
		public String getLimitPercent() {
			return LimitPercent;
		}
		public void setLimitPercent(String limitPercent) {
			LimitPercent = limitPercent;
		}
		public String getIsLimitByPercent() {
			return IsLimitByPercent;
		}
		public void setIsLimitByPercent(String isLimitByPercent) {
			IsLimitByPercent = isLimitByPercent;
		}
	  
	}
