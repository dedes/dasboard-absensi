package treemas.application.feature.mobileapp.approvalpath;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessages;


import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

public class PathValidator extends Validator {
	public PathValidator(HttpServletRequest request) {
		super(request);
	}

	public PathValidator(Locale locale) {
		super(locale);
	}

	
	public boolean validatePathEdit(ActionMessages messages,
			PathForm form, PathManager manager) throws CoreException {
		boolean ret = true;
		if (messages == null)
			messages = new ActionMessages();

		this.textValidator(messages, form.getApprovalSeqNum(), "Path.ApprovalSeqNum",
				"ApprovalSeqNum", "1", this.getStringLength(150),
				ValidatorGlobal.TM_CHECKED_LENGTH_RANGE,
				ValidatorGlobal.TM_FLAG_MANDATORY,
				ValidatorGlobal.TM_FLAG_FORMAT_CHAR);

		if (messages != null && !messages.isEmpty())
			throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA,
					messages);

		return ret;
	}

}



