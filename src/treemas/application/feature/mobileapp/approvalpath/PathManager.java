package treemas.application.feature.mobileapp.approvalpath;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.google.common.base.Strings;


import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

public class PathManager extends AppAdminManagerBase {

	private final String STRING_SQL = SQLConstant.SQL_APPROVALPATH;

	public PageListWrapper getAllPath(int pageNumber,
			PathSearch search) throws CoreException {
		PageListWrapper result = new PageListWrapper();
		search.setPage(pageNumber);
		this.logger.log(ILogger.LEVEL_INFO,
		"Execute Query get All Path ");
		try {
			this.logger.log(ILogger.LEVEL_INFO,
					"Execute Query Get All Path");
			List list = this.ibatisSqlMap.queryForList(this.STRING_SQL
					+ "getAllPath", search);
			Integer count = (Integer) this.ibatisSqlMap.queryForObject(
					this.STRING_SQL + "countAllPath", search);
			result.setResultList(list);
			result.setTotalRecord(count.intValue());
			this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM,
					"Execute Query Get All Path: " + search);
			this.logger.log(ILogger.LEVEL_INFO,
					"Execute Query Get All Path Sukses");
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return result;
	}
	
	public List getAllPath(String ApprovalSchemeID,  String ApprovalSeqNum,String LoginID,String LimitValue, String ProcessDuration) throws CoreException {
		List list = null;
		
		Map param = new HashedMap();
		String Id = !Strings.isNullOrEmpty(ApprovalSchemeID) ? ApprovalSchemeID : "";
		String app = !Strings.isNullOrEmpty(ApprovalSeqNum) ? ApprovalSeqNum : "";
		String log = !Strings.isNullOrEmpty(LoginID) ? LoginID : "";
		String lm= !Strings.isNullOrEmpty(LimitValue) ? LimitValue : "";
		String pro= !Strings.isNullOrEmpty(ProcessDuration) ? ProcessDuration : "";
	
		
		
		System.out.println(ApprovalSchemeID);
		System.out.println(ApprovalSeqNum);
		System.out.println(LoginID);
		System.out.println(LimitValue);
		System.out.println(ProcessDuration);
		
		
		param.put("ApprovalSchemeID", Id);
		param.put("ApprovalSeqNum", app);
		param.put("LoginID", log);
		param.put("LimitValue", lm);
		param.put("ProcessDuration", pro);
		
	
		
	
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get All Path");
			list  = this.ibatisSqlMap.queryForList(this.STRING_SQL+ "getAllPathScheme", param);
	
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return list;
	}
	
	

	

}



