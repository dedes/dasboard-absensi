package  treemas.application.feature.mobileapp.approval;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import treemas.application.feature.mobileapp.approvaltype.ApprovalTypeValidator;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

public class ApprovalHandler  extends AppAdminActionBase {
	 //  private DetailValidator validator;
	ApprovalManager manager = new ApprovalManager();
	ApprovalValidator validator;
	
	public ActionForward doAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws AppException {
		
		//this.validator = new DetailValidator(request);
		
		ApprovalForm frm = (ApprovalForm) form;
		frm.setTitle(Global.TITLE_APP_APPROVAL);
		frm.getPaging().calculationPage();
		String task = frm.getTask();
		if (Global.WEB_TASK_SHOW.equals(task)
				|| Global.WEB_TASK_LOAD.equals(task)
				|| Global.WEB_TASK_SEARCH.equals(task)) {
			this.load(request, frm);
			} 
				
		return this.getNextPage(task, mapping);

	}
	private ApprovalBean convertToBean(ApprovalForm form) throws AppException {
		ApprovalBean bean = form.toBean();
		return bean;
	}
	private void load(HttpServletRequest request, ApprovalForm form)
			throws CoreException {
		int pageNumber = form.getPaging().getTargetPageNo();
		if (Global.WEB_TASK_SEARCH.equals(form.getTask())
				|| Global.WEB_TASK_SHOW.equals(form.getTask()))
			pageNumber = 1;
		PageListWrapper result = this.manager.getAllApproval(pageNumber,
				form.getSearch());
		List list = result.getResultList();
		list = BeanConverter.convertBeansToString(list, form.getClass());
		form.getPaging().setCurrentPageNo(pageNumber);
		form.getPaging().setTotalRecord(result.getTotalRecord());
		request.setAttribute("list", list);

	}

	public ActionForward handleException(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
			msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			this.saveErrors(request, msgs);
			break;
		default:
			super.handleException(mapping, form, request, response, ex);
			break;
		}
		((ApprovalForm) form).setTask(((ApprovalForm) form)
				.resolvePreviousTask());

		String task = ((ApprovalForm) form).getTask();
		return this.getErrorPage(task, mapping);
	}

}


