package treemas.application.feature.mobileapp.approval;

import treemas.base.search.SearchForm;

public class ApprovalSearch extends SearchForm {
	private String ApprovalNo;
	private String ApprovalID;
	private String IsFinal;
	private String SecurityCode;
	private String ApprovalSchemeID;
	private String RequestDate;
	private String IsViaSMS;
	private String IsViaEmail;
	private String IsViaFax;
	private String AreaFaxNo;
	private String FaxNo;
	private String UserRequest;
	private String ApprovalDate;
	private String ApprovalStatus;
	private String ApprovalResult;
	private String ApprovalNote;
	private String LimitValue;
	private String ApprovalValue;
	private String IsForward;
	private String UserApproval;
	private String UserSecurityCode;
	private String ApprovalRoleID;
	private String TransactionNo;
	private String WOA;
	private String IsEverRejected;
	private String ClientIP;
	private String Argumentasi;
	private String ApprovalValuePencentage;
	private String ApprovalValuePercentage;
	private String IsApprovedByMobile;
	private String dtmupd;
	private String UsrUpd ;
	public String getApprovalNo() {
		return ApprovalNo;
	}
	public void setApprovalNo(String approvalNo) {
		ApprovalNo = approvalNo;
	}
	public String getApprovalID() {
		return ApprovalID;
	}
	public void setApprovalID(String approvalID) {
		ApprovalID = approvalID;
	}
	public String getIsFinal() {
		return IsFinal;
	}
	public void setIsFinal(String isFinal) {
		IsFinal = isFinal;
	}
	public String getSecurityCode() {
		return SecurityCode;
	}
	public void setSecurityCode(String securityCode) {
		SecurityCode = securityCode;
	}
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public String getRequestDate() {
		return RequestDate;
	}
	public void setRequestDate(String requestDate) {
		RequestDate = requestDate;
	}
	public String getIsViaSMS() {
		return IsViaSMS;
	}
	public void setIsViaSMS(String isViaSMS) {
		IsViaSMS = isViaSMS;
	}
	public String getIsViaEmail() {
		return IsViaEmail;
	}
	public void setIsViaEmail(String isViaEmail) {
		IsViaEmail = isViaEmail;
	}
	public String getIsViaFax() {
		return IsViaFax;
	}
	public void setIsViaFax(String isViaFax) {
		IsViaFax = isViaFax;
	}
	public String getAreaFaxNo() {
		return AreaFaxNo;
	}
	public void setAreaFaxNo(String areaFaxNo) {
		AreaFaxNo = areaFaxNo;
	}
	public String getFaxNo() {
		return FaxNo;
	}
	public void setFaxNo(String faxNo) {
		FaxNo = faxNo;
	}
	public String getUserRequest() {
		return UserRequest;
	}
	public void setUserRequest(String userRequest) {
		UserRequest = userRequest;
	}
	public String getApprovalDate() {
		return ApprovalDate;
	}
	public void setApprovalDate(String approvalDate) {
		ApprovalDate = approvalDate;
	}
	public String getApprovalStatus() {
		return ApprovalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		ApprovalStatus = approvalStatus;
	}
	public String getApprovalResult() {
		return ApprovalResult;
	}
	public void setApprovalResult(String approvalResult) {
		ApprovalResult = approvalResult;
	}
	public String getApprovalNote() {
		return ApprovalNote;
	}
	public void setApprovalNote(String approvalNote) {
		ApprovalNote = approvalNote;
	}
	public String getLimitValue() {
		return LimitValue;
	}
	public void setLimitValue(String limitValue) {
		LimitValue = limitValue;
	}
	public String getApprovalValue() {
		return ApprovalValue;
	}
	public void setApprovalValue(String approvalValue) {
		ApprovalValue = approvalValue;
	}
	public String getIsForward() {
		return IsForward;
	}
	public void setIsForward(String isForward) {
		IsForward = isForward;
	}
	public String getUserApproval() {
		return UserApproval;
	}
	public void setUserApproval(String userApproval) {
		UserApproval = userApproval;
	}
	public String getUserSecurityCode() {
		return UserSecurityCode;
	}
	public void setUserSecurityCode(String userSecurityCode) {
		UserSecurityCode = userSecurityCode;
	}
	public String getApprovalRoleID() {
		return ApprovalRoleID;
	}
	public void setApprovalRoleID(String approvalRoleID) {
		ApprovalRoleID = approvalRoleID;
	}
	public String getTransactionNo() {
		return TransactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		TransactionNo = transactionNo;
	}
	public String getWOA() {
		return WOA;
	}
	public void setWOA(String wOA) {
		WOA = wOA;
	}
	public String getIsEverRejected() {
		return IsEverRejected;
	}
	public void setIsEverRejected(String isEverRejected) {
		IsEverRejected = isEverRejected;
	}
	public String getClientIP() {
		return ClientIP;
	}
	public void setClientIP(String clientIP) {
		ClientIP = clientIP;
	}
	public String getArgumentasi() {
		return Argumentasi;
	}
	public void setArgumentasi(String argumentasi) {
		Argumentasi = argumentasi;
	}
	public String getApprovalValuePencentage() {
		return ApprovalValuePencentage;
	}
	public void setApprovalValuePencentage(String approvalValuePencentage) {
		ApprovalValuePencentage = approvalValuePencentage;
	}
	
	public String getApprovalValuePercentage() {
		return ApprovalValuePercentage;
	}
	public void setApprovalValuePercentage(String approvalValuePercentage) {
		ApprovalValuePercentage = approvalValuePercentage;
	}
	public String getIsApprovedByMobile() {
		return IsApprovedByMobile;
	}
	public void setIsApprovedByMobile(String isApprovedByMobile) {
		IsApprovedByMobile = isApprovedByMobile;
	}
	public String getDtmupd() {
		return dtmupd;
	}
	public void setDtmupd(String dtmupd) {
		this.dtmupd = dtmupd;
	}
	public String getUsrUpd() {
		return UsrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		UsrUpd = usrUpd;
	}
	
}
