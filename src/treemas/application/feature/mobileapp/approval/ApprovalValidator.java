package treemas.application.feature.mobileapp.approval;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessages;


import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

public class ApprovalValidator extends Validator {
	public ApprovalValidator(HttpServletRequest request) {
		super(request);
	}

	public ApprovalValidator(Locale locale) {
		super(locale);
	}

	
	public boolean validateApprovalEdit(ActionMessages messages,
			ApprovalForm form,ApprovalManager manager) throws CoreException {
		boolean ret = true;
		if (messages == null)
			messages = new ActionMessages();

		this.textValidator(messages, form.getApprovalID(), "Approval.ApprovalNo",
				"ApprovalNo", "1", this.getStringLength(150),
				ValidatorGlobal.TM_CHECKED_LENGTH_RANGE,
				ValidatorGlobal.TM_FLAG_MANDATORY,
				ValidatorGlobal.TM_FLAG_FORMAT_CHAR);

		if (messages != null && !messages.isEmpty())
			throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA,
					messages);

		return ret;
	}

}


