package treemas.application.feature.mobileapp.auditrailtask;

import treemas.base.ApplicationBean;

public class AuditrailTaskBean extends ApplicationBean {
	private String AUDITRAILID;
    private String ApprovalNO;
    private String ApprovalID;
    private String ApprovalSchemeID;
    private String UserRequest;
    private String ApprovalAnswer;
    private String ApprovalNoteAnswer;
    private String UserApprovalNext;
    private String transactionDate;
    private String dateKey;
	public String getDateKey() {
		return dateKey;
	}
	public void setDateKey(String dateKey) {
		this.dateKey = dateKey;
	}
	public String getAUDITRAILID() {
		return AUDITRAILID;
	}
	public void setAUDITRAILID(String aUDITRAILID) {
		AUDITRAILID = aUDITRAILID;
	}
	public String getApprovalNO() {
		return ApprovalNO;
	}
	public void setApprovalNO(String approvalNO) {
		ApprovalNO = approvalNO;
	}
	public String getApprovalID() {
		return ApprovalID;
	}
	public void setApprovalID(String approvalID) {
		ApprovalID = approvalID;
	}
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public String getUserRequest() {
		return UserRequest;
	}
	public void setUserRequest(String userRequest) {
		UserRequest = userRequest;
	}

	public String getApprovalAnswer() {
		return ApprovalAnswer;
	}
	public void setApprovalAnswer(String approvalAnswer) {
		ApprovalAnswer = approvalAnswer;
	}
	public String getApprovalNoteAnswer() {
		return ApprovalNoteAnswer;
	}
	public void setApprovalNoteAnswer(String approvalNoteAnswer) {
		ApprovalNoteAnswer = approvalNoteAnswer;
	}
	public String getUserApprovalNext() {
		return UserApprovalNext;
	}
	public void setUserApprovalNext(String userApprovalNext) {
		UserApprovalNext = userApprovalNext;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
 
}
