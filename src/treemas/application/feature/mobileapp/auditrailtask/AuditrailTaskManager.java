package treemas.application.feature.mobileapp.auditrailtask;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.google.common.base.Strings;

import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

public class AuditrailTaskManager  extends AppAdminManagerBase{
	private final String STRING_SQL = SQLConstant.SQL_AUDITRAILTASK;

	public PageListWrapper getAllAuditrailTask(int pageNumber,
			AuditrailTaskSearch auditrailTaskSearch) throws CoreException {
		PageListWrapper result = new PageListWrapper();
		auditrailTaskSearch.setPage(pageNumber);		
		
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get All AuditrailTask");
			List list = this.ibatisSqlMap.queryForList(this.STRING_SQL
					+ "getAllAuditrailTask", auditrailTaskSearch);
			Integer count = (Integer) this.ibatisSqlMap.queryForObject(
					this.STRING_SQL + "countAllAuditrailTask", auditrailTaskSearch);
			result.setResultList(list);
			result.setTotalRecord(count.intValue());
			this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM,
					"Execute Query Get All AuditrailTask : " + auditrailTaskSearch);
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get All AuditrailTask Sukses");
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return result;
	}
	
	
						
	
	
	public List getAllAuditrailTask(String AUDITRAILID,  String ApprovalNO, String ApprovalID, String DateBefore,  String DateAfter, String ApprovalSchemeID) throws CoreException {
		List list = null;
		
		Map param = new HashedMap();
		String Id = !Strings.isNullOrEmpty(AUDITRAILID) ? AUDITRAILID : "";
		String apr = !Strings.isNullOrEmpty(ApprovalNO) ? ApprovalNO : "";
		String act = !Strings.isNullOrEmpty(ApprovalID) ? ApprovalID : "";
		String app= !Strings.isNullOrEmpty(ApprovalSchemeID) ? ApprovalSchemeID : "";
		String transactionDate= !Strings.isNullOrEmpty(DateBefore) ? DateBefore : "";
		String transactionDateAfter= !Strings.isNullOrEmpty(DateAfter) ? DateAfter : "";
		
		
		param.put("AUDITRAILID", Id);
		param.put("ApprovalNO", apr);
		param.put("ApprovalID", act);
		param.put("ApprovalSchemeID", app);
		param.put("transactionDate", transactionDate);		
		param.put("transactionDateAfter", transactionDateAfter);
	
		
	
		
	
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute Query Get All AuditrailTask");
			list  = this.ibatisSqlMap.queryForList(this.STRING_SQL+ "getAllTask", param);
	
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new CoreException(ERROR_DB);
		}

		return list;
	}
	
	

	

}



