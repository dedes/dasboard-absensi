package treemas.application.feature.link;

import treemas.base.setup.SetUpFormBase;

public class LinkForm extends SetUpFormBase {

    private String id;
    private String paramdesc;
    private String data_type;
    private String val;
    private String isvisible;
    private String isApprove;
    private String usrupd;
    private String dtmupd;
    private String usrapp;
    private String dtmapp;


    public LinkForm() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParamdesc() {
        return paramdesc;
    }

    public void setParamdesc(String paramdesc) {
        this.paramdesc = paramdesc;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getIsvisible() {
        return isvisible;
    }

    public void setIsvisible(String isvisible) {
        this.isvisible = isvisible;
    }

    public String getIsApprove() {
        return isApprove;
    }

    public void setIsApprove(String isApprove) {
        this.isApprove = isApprove;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getUsrapp() {
        return usrapp;
    }

    public void setUsrapp(String usrapp) {
        this.usrapp = usrapp;
    }

    public String getDtmapp() {
        return dtmapp;
    }

    public void setDtmapp(String dtmapp) {
        this.dtmapp = dtmapp;
    }


}
