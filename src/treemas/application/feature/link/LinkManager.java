package treemas.application.feature.link;

import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LinkManager extends SetUpManagerBase {
    public static final String STRING_SQL = SQLConstant.SQL_FEATURE_LINK;

    public List getListAbout() throws AppException {
        List result = null;

        Map param = new HashMap();
        param.put("user", Global.ABOUT_USER);
        param.put("period", Global.ABOUT_PERIOD);
        param.put("machine", Global.ABOUT_MACHINE);
        param.put("company", Global.ABOUT_COMPANY_CLIENT);

        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAbout", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public List getAllParameter() throws AppException {
        List result = null;

        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAll");
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public List getAllVisibleParameter() throws AppException {
        List result = null;

        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAllVisible");
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public LinkBean getSingleParameter(String id) throws CoreException {
        LinkBean result = null;

        try {
            result = (LinkBean) this.ibatisSqlMap.queryForObject(STRING_SQL + "getSingleParam", id);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public void insertParameter(String userId, LinkBean bean) throws AppException {
        try {
            bean.setUsrupd(userId);
            this.ibatisSqlMap.insert(STRING_SQL + "insert", bean);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public void updateParameter(String userId, LinkBean bean) throws AppException {
        try {
            bean.setUsrupd(userId);
            this.ibatisSqlMap.update(STRING_SQL + "update", bean);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public String getValueParameter(String id) throws AppException {
        LinkBean bean = this.getSingleParameter(id);
        String result = bean.getVal();
        return result;
    }
}
