package treemas.application.feature.view.assignmentsurveyor;

public class OptionAnswer {

    private int optionId;
    private String optionLabel;
    private String description;
    private boolean selected;

    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }

    public String getOptionLabel() {
        return optionLabel;
    }

    public void setOptionLabel(String optionLabel) {
        this.optionLabel = optionLabel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


//	private Integer mobileAssignmentId;
//	private Integer questionGroupId;
//	private String questionGroupName;
//	private Integer questionId;
//	private String questionLabel;
//	private Integer questionOrder;
//	private String textAnswer;
//	private Integer isVisible;
//	private String isVisibleQA;
//	private String relQuestions;
//	private String relTextAnswer;
//	private String relOptionAnswerId;
//	private String answerType;
//	private String optionAnswerId;
//	private String optionAnswerText;
//	private String optionAnswerLabel;
//	private String isMandatory;
//	private String oldOptionAnswerLabel;
//	private String oldTextAnswer;
//	private String finalOptionAnswerLabel;
//	private String finalTextAnswer;
//	private String finalUseImage;
//	private String paramName;
//
//	
//
//	
//	public Integer getMobileAssignmentId() {
//		return mobileAssignmentId;
//	}
//	public void setMobileAssignmentId(Integer mobileAssignmentId) {
//		this.mobileAssignmentId = mobileAssignmentId;
//	}
//	public Integer getQuestionGroupId() {
//		return questionGroupId;
//	}
//	public void setQuestionGroupId(Integer questionGroupId) {
//		this.questionGroupId = questionGroupId;
//	}
//	public String getQuestionGroupName() {
//		return questionGroupName;
//	}
//	public void setQuestionGroupName(String questionGroupName) {
//		this.questionGroupName = questionGroupName;
//	}
//	public Integer getQuestionId() {
//		return questionId;
//	}
//	public void setQuestionId(Integer questionId) {
//		this.questionId = questionId;
//	}
//	public String getQuestionLabel() {
//		return questionLabel;
//	}
//	public void setQuestionLabel(String questionLabel) {
//		this.questionLabel = questionLabel;
//	}
//	public Integer getQuestionOrder() {
//		return questionOrder;
//	}
//	public void setQuestionOrder(Integer questionOrder) {
//		this.questionOrder = questionOrder;
//	}
//	public String getTextAnswer() {
//		return textAnswer;
//	}
//	public void setTextAnswer(String textAnswer) {
//		this.textAnswer = textAnswer;
//	}
//	public Integer getIsVisible() {
//		return isVisible;
//	}
//	public void setIsVisible(Integer isVisible) {
//		this.isVisible = isVisible;
//	}
//	public String getIsVisibleQA() {
//		return isVisibleQA;
//	}
//	public void setIsVisibleQA(String isVisibleQA) {
//		this.isVisibleQA = isVisibleQA;
//	}
//	public String getRelQuestions() {
//		return relQuestions;
//	}
//	public void setRelQuestions(String relQuestions) {
//		this.relQuestions = relQuestions;
//	}
//	public String getRelTextAnswer() {
//		return relTextAnswer;
//	}
//	public void setRelTextAnswer(String relTextAnswer) {
//		this.relTextAnswer = relTextAnswer;
//	}
//	public String getRelOptionAnswerId() {
//		return relOptionAnswerId;
//	}
//	public void setRelOptionAnswerId(String relOptionAnswerId) {
//		this.relOptionAnswerId = relOptionAnswerId;
//	}
//	public String getAnswerType() {
//		return answerType;
//	}
//	public void setAnswerType(String answerType) {
//		this.answerType = answerType;
//	}
//	public String getOptionAnswerId() {
//		return optionAnswerId;
//	}
//	public void setOptionAnswerId(String optionAnswerId) {
//		this.optionAnswerId = optionAnswerId;
//	}
//	public String getOptionAnswerText() {
//		return optionAnswerText;
//	}
//	public void setOptionAnswerText(String optionAnswerText) {
//		this.optionAnswerText = optionAnswerText;
//	}
//	public String getOptionAnswerLabel() {
//		return optionAnswerLabel;
//	}
//	public void setOptionAnswerLabel(String optionAnswerLabel) {
//		this.optionAnswerLabel = optionAnswerLabel;
//	}
//	public String getIsMandatory() {
//		return isMandatory;
//	}
//	public void setIsMandatory(String isMandatory) {
//		this.isMandatory = isMandatory;
//	}
//	public String getOldOptionAnswerLabel() {
//		return oldOptionAnswerLabel;
//	}
//	public void setOldOptionAnswerLabel(String oldOptionAnswerLabel) {
//		this.oldOptionAnswerLabel = oldOptionAnswerLabel;
//	}
//	public String getOldTextAnswer() {
//		return oldTextAnswer;
//	}
//	public void setOldTextAnswer(String oldTextAnswer) {
//		this.oldTextAnswer = oldTextAnswer;
//	}
//	public String getFinalOptionAnswerLabel() {
//		return finalOptionAnswerLabel;
//	}
//	public void setFinalOptionAnswerLabel(String finalOptionAnswerLabel) {
//		this.finalOptionAnswerLabel = finalOptionAnswerLabel;
//	}
//	public String getFinalTextAnswer() {
//		return finalTextAnswer;
//	}
//	public void setFinalTextAnswer(String finalTextAnswer) {
//		this.finalTextAnswer = finalTextAnswer;
//	}
//	public String getFinalUseImage() {
//		return finalUseImage;
//	}
//	public void setFinalUseImage(String finalUseImage) {
//		this.finalUseImage = finalUseImage;
//	}
//	public String getParamName() {
//		return paramName;
//	}
//	public void setParamName(String paramName) {
//		this.paramName = paramName;
//	}


}
