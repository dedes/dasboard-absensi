package treemas.application.feature.view.assignmentsurveyor;

import treemas.base.view.ViewManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public class ViewAssignmentSurveyorManager extends ViewManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_VIEW_ASSIGNMENT_SURVEYOR;
    private static final String FALSE = "0";

    public static Vector splitToVector(String original, String delimeter) {

        Vector nodes = new Vector();
        int index = original.indexOf(delimeter);

        while (index >= 0) {
            nodes.addElement(original.substring(0, index));
            original = original.substring(index + delimeter.length());
            index = original.indexOf(delimeter);
        }

        nodes.addElement(original);

        return nodes;
    }

    public static String[] split(String original, String delimeter) {

        Vector nodes = splitToVector(original, delimeter);
        String[] result = new String[nodes.size()];
        if (nodes.size() > 0) {
            for (int loop = 0; loop < nodes.size(); loop++) {
                result[loop] = (String) nodes.elementAt(loop);
            }
        }
        return result;
    }

    public ViewAssignmentSurveyorBean getHeader(Integer mobileAssignmentId)
            throws CoreException {

        ViewAssignmentSurveyorBean bean = null;

        try {
            bean = (ViewAssignmentSurveyorBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getHeader", mobileAssignmentId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return bean;
    }

    public List getQuestion(Integer mobileAssignmentId) throws CoreException {
        List list = null;

        try {
            this.logger.log(ILogger.LEVEL_INFO, "getQuestion");
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL
                    + "getQuestion", mobileAssignmentId);

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return list;
    }

    public List getQA(Integer mobileAssigmentId) throws CoreException {
        List QA = null;
        try {
            this.logger.log(ILogger.LEVEL_INFO, "getQA");
            QA = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getQA",
                    mobileAssigmentId);

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return QA;
    }

    public List getOptionAnswer(Integer mobileAssigmentId) throws CoreException {
        List optionAnswer = null;
        try {
            this.logger.log(ILogger.LEVEL_INFO, "getOptionAnswer");
            optionAnswer = this.ibatisSqlMap.queryForList(this.STRING_SQL
                    + "getOptionAnswer", mobileAssigmentId);

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return optionAnswer;
    }

    public List getAnswerNotOption(Integer mobileAssigmentId)
            throws CoreException {
        List optionAnswer = null;
        try {
            this.logger.log(ILogger.LEVEL_INFO, "getOptionAnswer");
            optionAnswer = this.ibatisSqlMap.queryForList(this.STRING_SQL
                    + "getAnswer", mobileAssigmentId);

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return optionAnswer;
    }

    public List doGetRelAnswer(int mobileAssignmentId, int questionGroupId,
                               int relQuestionId, int questionId) throws CoreException {
        List result = null;

        HashMap relQuestionMap = new HashMap();
        relQuestionMap.put("mobileAssignmentId",
                new Integer(mobileAssignmentId));
        relQuestionMap.put("questionGroupId", new Integer(questionGroupId));
        relQuestionMap.put("relQuestionId", new Integer(relQuestionId));
        relQuestionMap.put("questionId", new Integer(questionId));

        try {
            // get user profile
            this.logger.log(ILogger.LEVEL_INFO, "Search Relevant Answer ");
            result = this.ibatisSqlMap.queryForList(this.STRING_SQL
                    + "getRelAnswer", relQuestionMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return result;
    }

    public void submitDraftAnswer(ViewAssignmentSurveyorBean assignmentSurveyorBean)
            throws CoreException {

        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Insert Answer ");
                this.ibatisSqlMap.insert(this.STRING_SQL + "insertAnswer", assignmentSurveyorBean);
                this.ibatisSqlMap.commitTransaction();
                this.logger
                        .log(ILogger.LEVEL_INFO, "Finished Inserted Answer ");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void submitDependency(ViewAssignmentSurveyorBean assignmentSurveyorBean)
            throws CoreException {

        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Update Answer ");
                this.ibatisSqlMap.update(this.STRING_SQL + "updateAnswer", assignmentSurveyorBean);
                this.ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Finished Update Answer ");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void submitWithoutDependency(ViewAssignmentSurveyorBean assignmentSurveyorBean)
            throws CoreException {

        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Insert Answer ");
                this.ibatisSqlMap.update(this.STRING_SQL + "insertAnswer", assignmentSurveyorBean);
                this.ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Finished Insert Answer ");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void updateStatusSubmit(Integer mai) throws CoreException {

        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Update Status Submit");
                this.ibatisSqlMap.update(
                        this.STRING_SQL + "updateStatusSubmit", mai);
                this.ibatisSqlMap.commitTransaction();
                this.logger
                        .log(ILogger.LEVEL_INFO, "Finished Submited Answer ");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void updateStatusDraft(Integer mai) throws CoreException {

        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Update Status Draft");
                this.ibatisSqlMap.update(this.STRING_SQL + "updateStatusDraft",
                        mai);
                this.ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Finished Draft Answer ");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void updateInvisibleDependecy(int mobileAssignmentId,
                                         int questionGroupId, int relQuestionId) throws CoreException {
        ViewAssignmentSurveyorBean result = null;

        HashMap relQuestionMap = new HashMap();
        relQuestionMap.put("mobileAssignmentId",
                new Integer(mobileAssignmentId));
        relQuestionMap.put("questionGroupId", new Integer(questionGroupId));
        relQuestionMap.put("relQuestionId", new Integer(relQuestionId));

        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO,
                        "Update Visibility Dependecy");
                this.ibatisSqlMap.update(this.STRING_SQL
                        + "updateInvisibleDependency", relQuestionMap);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void updateVisibleDependecy(int mobileAssignmentId,
                                       int questionGroupId, int relQuestionId) throws CoreException {
        ViewAssignmentSurveyorBean result = null;

        HashMap relQuestionMap = new HashMap();
        relQuestionMap.put("mobileAssignmentId",
                new Integer(mobileAssignmentId));
        relQuestionMap.put("questionGroupId", new Integer(questionGroupId));
        relQuestionMap.put("relQuestionId", new Integer(relQuestionId));

        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO,
                        "Update Visibility Dependecy");
                this.ibatisSqlMap.update(this.STRING_SQL
                        + "updateVisibleDependency", relQuestionMap);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public Integer CheckAssignmentId(Integer mobileAssignmentId) {
        Integer exists = new Integer(0);
        try {
            exists = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL
                    + "checkMobileAssignmentId", mobileAssignmentId);
        } catch (SQLException e) {
            System.out.println("Failed to checkMobileAssignmentId:  "
                    + e.getMessage());
            e.printStackTrace();
            this.logger.printStackTrace(e);
        }
        return exists;
    }

    public void deleteMultipleAnswer(ViewAssignmentSurveyorBean assignmentSurveyorBean)
            throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Delete Multiple Answer ");
                this.ibatisSqlMap.delete(this.STRING_SQL + "deleteMultiple",
                        assignmentSurveyorBean);
                this.ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO,
                        "Finished Delete Multiple Answer ");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void deleteAnswerDraft(Integer mobileAssignmentId)
            throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO,
                        "Delete Answer before submited");
                this.ibatisSqlMap.delete(this.STRING_SQL + "deleteAnswerDraft",
                        mobileAssignmentId);
                this.ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Finished Delete Answer ");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void insertMultipleAnswer(ViewAssignmentSurveyorBean assignmentSurveyorBean)
            throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "insert Multiple Answer ");
                this.ibatisSqlMap.update(this.STRING_SQL + "insertAnswer", assignmentSurveyorBean);
                this.ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO,
                        "Finished insert Multiple Answer ");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

}
