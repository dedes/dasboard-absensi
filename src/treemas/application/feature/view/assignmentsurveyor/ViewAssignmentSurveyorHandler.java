package treemas.application.feature.view.assignmentsurveyor;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import treemas.base.view.ViewActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.AnswerType;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.format.TreemasDateFormatter;
import treemas.util.format.TreemasFormatter;
import treemas.util.tool.ConfigurationProperties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class ViewAssignmentSurveyorHandler extends ViewActionBase {

    List questionList = null;
    int indexUpload = 0;
    private List formFiles = new ArrayList();
    private Map excConverterMap = new HashMap();
    private Map cordConverterMap = new HashMap();

    private ViewAssignmentSurveyorManager manager = new ViewAssignmentSurveyorManager();

    public ViewAssignmentSurveyorHandler() {

        this.pageMap.put("LoadPreviousQuestion", "view");
        this.pageMap.put("LoadNextQuestion", "view");
        this.exceptionMap.put("Submit", "view");
        this.exceptionMap.put("SubmitDependecy", "view");

        TreemasDateFormatter convertDMYTime = TreemasDateFormatter.getInstance();
        this.excConverterMap.put("assignmentDate", convertDMYTime);
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        ViewAssignmentSurveyorForm viewAssignmentSurveyorForm = (ViewAssignmentSurveyorForm) form;
        String action = viewAssignmentSurveyorForm.getTask();
        Integer mobileAssignmentId =
                viewAssignmentSurveyorForm.getMobileAssignmentId();
        viewAssignmentSurveyorForm.getPaging().calculationPage();

        if (viewAssignmentSurveyorForm.getCurrPage() == null) {
            viewAssignmentSurveyorForm.setCurrPage(new Integer(0));
            viewAssignmentSurveyorForm.setTotPage(new Integer(0));
        }

        if (viewAssignmentSurveyorForm.getTextAnswer() == null) {
            viewAssignmentSurveyorForm.setTextAnswer("");
        }

        if (Global.WEB_TASK_LOAD.equals(action)) {
            this.getHeader(viewAssignmentSurveyorForm, mobileAssignmentId);
            this.loadQuestion(request, mobileAssignmentId, viewAssignmentSurveyorForm);

        } else if (Global.WEB_TASK_BACK.equals(action)) {
            return this.doBack(request);

        } else if ("Submit".equals(action)) {
            try {
                submitAnswer(request, mobileAssignmentId, form);
                this.manager.updateStatusSubmit(mobileAssignmentId);
            } catch (Exception e) {
                this.logger.printStackTrace(e);
            }
        } else if ("SubmitDependecy".equals(action)) {
            try {
                this.manager.updateStatusSubmit(mobileAssignmentId);
            } catch (Exception e) {
                this.logger.printStackTrace(e);
            }
        } else if ("LoadPreviousQuestion".equals(action)) {
            this.getHeader(viewAssignmentSurveyorForm, mobileAssignmentId);
            this.loadQuestion(request, mobileAssignmentId, viewAssignmentSurveyorForm);
            this.cekIndek(request, mobileAssignmentId, viewAssignmentSurveyorForm, action);

        } else if ("LoadNextQuestion".equals(action)) {
            this.getHeader(viewAssignmentSurveyorForm, mobileAssignmentId);
            this.loadQuestion(request, mobileAssignmentId, viewAssignmentSurveyorForm);
            this.cekIndek(request, mobileAssignmentId, viewAssignmentSurveyorForm, action);
        }
        viewAssignmentSurveyorForm.setPath(mapping.getPath());
        return this.getNextPage(action, mapping);
    }

    private void getHeader(ViewAssignmentSurveyorForm viewAssignmentSurveyorForm, Integer mobileAssignmentId) throws CoreException {
        ViewAssignmentSurveyorBean bean = null;
        bean = this.manager.getHeader(mobileAssignmentId);
        if (bean != null) {
            BeanConverter.convertBeanToString(bean, viewAssignmentSurveyorForm, excConverterMap);
        }
    }

    private void loadQuestion(HttpServletRequest request, Integer mobileAssignmentId, ViewAssignmentSurveyorForm myForm)
            throws CoreException {
        List listQuestion = null;
        List optTemp = null;
        List AnswerNotOption = null;
        ViewAssignmentSurveyorBean viewAssignmentSurveyorBean = null;
        listQuestion = this.manager.getQuestion(mobileAssignmentId);
        int Exist = this.manager.CheckAssignmentId(mobileAssignmentId).intValue();
        if (Exist == 0) {
            if (listQuestion != null) {
                for (int i = 0; i < listQuestion.size(); i++) {

                    viewAssignmentSurveyorBean = (ViewAssignmentSurveyorBean) listQuestion.get(i);
                    viewAssignmentSurveyorBean.setMobileAssignmentId(mobileAssignmentId);

                    if (viewAssignmentSurveyorBean.getOptionAnswerId() == null)
                        viewAssignmentSurveyorBean.setOptionAnswerId("");
                    if (viewAssignmentSurveyorBean.getImagePath() == null) viewAssignmentSurveyorBean.setImagePath("");
                    if (viewAssignmentSurveyorBean.getOptionAnswerLabel() == null)
                        viewAssignmentSurveyorBean.setOptionAnswerLabel("");
                    if (viewAssignmentSurveyorBean.getTextAnswer() == null)
                        viewAssignmentSurveyorBean.setTextAnswer("");
                    if (viewAssignmentSurveyorBean.getSelectedOption() == null)
                        viewAssignmentSurveyorBean.setSelectedOption("");

                    this.manager.submitDraftAnswer(viewAssignmentSurveyorBean);

                }
                this.manager.updateStatusDraft(mobileAssignmentId);
            }
        }
        try {
            optTemp = this.manager.getOptionAnswer(mobileAssignmentId);
            AnswerNotOption = this.manager.getAnswerNotOption(mobileAssignmentId);
        } catch (CoreException e) {
            e.printStackTrace();
            this.logger.printStackTrace(e);
        }

        listQuestion = questionListUpdate(listQuestion, mobileAssignmentId, optTemp, AnswerNotOption);
        this.questionList = listQuestion;
        request.setAttribute("listQuestion", listQuestion);


    }

    public List questionListUpdate(List Q, Integer mobileAssignmentId, List optTemp, List answer) {

        List listQuestion = Q;
        List listQuestionAnswer = new ArrayList();
        List optionList = new ArrayList();

        int questionId = 0;
        int questionGroupId = 0;
        String answerType = "";
        String questionLabel = "";
        String answerOptSelected = "";
        ViewAssignmentSurveyorBean viewAssignmentSurveyorBean = null;

        ViewAssignmentSurveyorBean tempOptBean = null;
        ViewAssignmentSurveyorBean tempAnswerBean = null;

        for (int j = 0; j < listQuestion.size(); j++) {
            viewAssignmentSurveyorBean = (ViewAssignmentSurveyorBean) listQuestion.get(j);
            questionId = viewAssignmentSurveyorBean.getQuestionId().intValue();
            questionGroupId = viewAssignmentSurveyorBean.getQuestionGroupId().intValue();
            questionLabel = viewAssignmentSurveyorBean.getQuestionLabel();
            answerType = viewAssignmentSurveyorBean.getAnswerType();
            viewAssignmentSurveyorBean.setSelectedOption(answerOptSelected);
                    /*
					 * ANSWER TYPE MULTIPLE
					 * */
            if (AnswerType.MULTIPLE.equals(answerType)
                    || AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(answerType)
                    || AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(answerType)
                    || AnswerType.RADIO.equals(answerType)
                    || AnswerType.RADIO_WITH_DESCRIPTION.equals(answerType)
                    || AnswerType.DROPDOWN.equals(answerType)
                    || AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(answerType)) {

                for (int k = 0; k < optTemp.size(); k++) {

                    tempOptBean = (ViewAssignmentSurveyorBean) optTemp.get(k);
                    int qGroupId = tempOptBean.getQuestionGroupId().intValue();
                    int qId = tempOptBean.getQuestionId().intValue();
                    String ansType = tempOptBean.getAnswerType();

                    if (questionGroupId == qGroupId && questionId == qId && answerType.equals(ansType)) {
                        String optIDtemp = tempOptBean.getOptionAnswerId();
                        String optTextTemp = tempOptBean.getOptionAnswerText();

                        if (optIDtemp != null && optTextTemp != "") {
                            answerOptSelected = optIDtemp + "~" + optTextTemp + "#" + answerOptSelected;
                            viewAssignmentSurveyorBean.setSelectedOption(answerOptSelected);
                        }
                        viewAssignmentSurveyorBean.setIsVisibleQA(tempOptBean.getIsVisibleQA());
                    }
                }
                listQuestionAnswer.add(viewAssignmentSurveyorBean);
            } else {
                for (int l = 0; l < answer.size(); l++) {
                    tempAnswerBean = (ViewAssignmentSurveyorBean) answer.get(l);
                    int qAGroupId = tempAnswerBean.getQuestionGroupId().intValue();
                    int qAId = tempAnswerBean.getQuestionId().intValue();

                    if (questionGroupId == qAGroupId && questionId == qAId) {
                        listQuestionAnswer.add(tempAnswerBean);
                    }
                }
            }

        }


        listQuestion = listQuestionAnswer;
        for (int b = 0; b < listQuestion.size(); b++) {
            viewAssignmentSurveyorBean = (ViewAssignmentSurveyorBean) listQuestion.get(b);
            this.logger.log(this.logger.LEVEL_INFO, "Question Id: "
                    + viewAssignmentSurveyorBean.getQuestionId()
                    + "\nQuestion Label: "
                    + viewAssignmentSurveyorBean.getQuestionLabel()
                    + "\nAnswer Text: "
                    + viewAssignmentSurveyorBean.getTextAnswer()
                    + "\nOption Label: "
                    + viewAssignmentSurveyorBean.getOptionAnswerLabel()
                    + "\nSelectedOption: "
                    + viewAssignmentSurveyorBean.getSelectedOption());

        }
        return listQuestion;
    }

    private void submitOnlyText(String paramName, HttpServletRequest request, ViewAssignmentSurveyorBean viewBean) throws CoreException {
        viewBean.setTextAnswer(request.getParameter(paramName));
        this.manager.submitWithoutDependency(viewBean);
    }

    private void submitSingleChoise(String paramName, HttpServletRequest request, ViewAssignmentSurveyorBean viewBean) throws CoreException {
        String tempValue = request.getParameter(paramName);
        String splitValue[] = null;
        if (tempValue != null) {
            splitValue = ViewAssignmentSurveyorManager.split(tempValue, "#");
            for (int a = 0; a < splitValue.length; a++) {
                viewBean.setOptionAnswerLabel(splitValue[1]);
                viewBean.setOptionAnswerId(splitValue[0]);
            }
        }
        this.manager.submitWithoutDependency(viewBean);
    }

    private void submitSingleChoiseWithDescription(String paramName, HttpServletRequest request, ViewAssignmentSurveyorBean viewBean) throws CoreException {
        String tempValue = request.getParameter(paramName);
        String splitValue[] = null;
        String desc = paramName + "Desc";
        viewBean.setTextAnswer(request.getParameter(desc));
        if (tempValue != null) {
            splitValue = ViewAssignmentSurveyorManager.split(tempValue, "#");
            for (int a = 0; a < splitValue.length; a++) {
                viewBean.setOptionAnswerLabel(splitValue[1]);
                viewBean.setOptionAnswerId(splitValue[0]);
            }
        }
        this.manager.submitWithoutDependency(viewBean);

    }


    private void submitMultipleWithoutDescription(String paramName, HttpServletRequest request, ViewAssignmentSurveyorBean viewBean) throws CoreException {
        String tempValue[] = request.getParameterValues(paramName);
        String splitValue[] = null;
        if (tempValue != null) {
            for (int j = 0; j < tempValue.length; j++) {
                splitValue = ViewAssignmentSurveyorManager.split(
                        tempValue[j], "#");
                for (int a = 0; a < splitValue.length; a++) {
                    viewBean.setOptionAnswerLabel(splitValue[1]);
                    viewBean.setOptionAnswerId(splitValue[0]);
                }
                this.manager.submitWithoutDependency(viewBean);
            }
        }
    }

    private void submitMultipleWithDescription(String paramName, HttpServletRequest request, ViewAssignmentSurveyorBean viewBean) throws CoreException {
        String desc = paramName + "Desc";
        String tempValue[] = request.getParameterValues(paramName);
        String splitValue[] = null;
        viewBean.setTextAnswer(request.getParameter(desc));
        if (tempValue != null) {
            for (int j = 0; j < tempValue.length; j++) {
                splitValue = ViewAssignmentSurveyorManager.split(tempValue[j], "#");
                for (int a = 0; a < splitValue.length; a++) {
                    viewBean.setOptionAnswerLabel(splitValue[1]);
                    viewBean.setOptionAnswerId(splitValue[0]);

                }
                this.manager.submitWithoutDependency(viewBean);

            }
        }
    }

    private void submitImage(ViewAssignmentSurveyorForm form, String paramName, HttpServletRequest request, ViewAssignmentSurveyorBean viewBean, int index) throws CoreException {

        List listImage = form.getListQuestion();

        paramName = paramName + viewBean.getAnswerType();
        viewBean.setTextAnswer(request.getParameter(paramName));

        ViewAssignmentSurveyorForm vasf = (ViewAssignmentSurveyorForm) listImage.get(index);
        FormFile myFile = vasf.getImage();
        String fileName = myFile.getFileName();
        try {

            String filePath = ConfigurationProperties.getInstance().get(PropertiesKey.ANSWER_IMAGE_PATH);
            StringBuffer surveyId = new StringBuffer(String.valueOf(viewBean.getMobileAssignmentId()))
                    .append("_").append(viewBean.getQuestionGroupId())
                    .append("_").append(viewBean.getQuestionId());

            if (!fileName.equals("")) {
                fileName = writeImageToFile(filePath, myFile.getFileData(), surveyId.toString());
                viewBean.setImagePath(fileName);
            }
            request.setAttribute("fileName", fileName);
            this.manager.submitWithoutDependency(viewBean);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            this.logger.printStackTrace(e);
        } catch (IOException e) {
            e.printStackTrace();
            this.logger.printStackTrace(e);
        }
    }

    public void submitAnswer(HttpServletRequest request, Integer mobileAssignmentId, ActionForm form) throws CoreException {
        ViewAssignmentSurveyorForm frm = (ViewAssignmentSurveyorForm) form;

        List listQuestion = this.questionList;

        this.manager.deleteAnswerDraft(mobileAssignmentId);
        ViewAssignmentSurveyorBean viewBean = null;
        ViewAssignmentSurveyorManager vasb = new ViewAssignmentSurveyorManager();
        Integer indx = new Integer(1);
        if (listQuestion != null) {
            for (int i = 0; i < listQuestion.size(); i++) {

                viewBean = (ViewAssignmentSurveyorBean) listQuestion.get(i);
                String paramName = "Soal" + (i + indx.intValue());
                viewBean.setMobileAssignmentId(mobileAssignmentId);

                if (viewBean.getOptionAnswerId() == null) viewBean.setOptionAnswerId("");
                if (viewBean.getImagePath() == null) viewBean.setImagePath("");
                if (viewBean.getOptionAnswerLabel() == null) viewBean.setOptionAnswerLabel("");

                viewBean.setLAC(null);
                viewBean.setGpsLatitude(null);
                viewBean.setGpsLongitude(null);
                viewBean.setMCC(null);
                viewBean.setMNC(null);
                viewBean.setLAC(null);
                viewBean.setCellId(null);
                viewBean.setIsGps(null);
                viewBean.setAccuracy(null);
                viewBean.setLov(null);

                boolean saveImage = false;
                boolean saveMultipleDescription = false;
                boolean saveMultiple = false;
                boolean saveText = false;

                if (viewBean.getAnswerType().equals(AnswerType.TEXT) ||
                        viewBean.getAnswerType().equals(AnswerType.NUMERIC) ||
                        viewBean.getAnswerType().equals(AnswerType.DECIMAL) ||
                        viewBean.getAnswerType().equals(AnswerType.DATE) ||
                        viewBean.getAnswerType().equals(AnswerType.DATE_TIME) ||
                        viewBean.getAnswerType().equals(AnswerType.TIME) ||
                        viewBean.getAnswerType().equals(AnswerType.GEODATA_TIME) ||
                        viewBean.getAnswerType().equals(AnswerType.GPS_TIME) ||
                        viewBean.getAnswerType().equals(AnswerType.LOOKUP) ||
                        viewBean.getAnswerType().equals(AnswerType.RESPONDEN) ||
                        viewBean.getAnswerType().equals(AnswerType.LOOKUP_WITH_FILTER) ||
                        viewBean.getAnswerType().equals(AnswerType.NUMERIC_WITH_CALCULATION) ||
                        viewBean.getAnswerType().equals(AnswerType.ACCOUNTING)) {
                    paramName = paramName + viewBean.getAnswerType();
                    saveText = true;
                } else if (viewBean.getAnswerType().equals(AnswerType.MULTIPLE) ||
                        viewBean.getAnswerType().equals(AnswerType.RADIO) ||
                        viewBean.getAnswerType().equals(AnswerType.DROPDOWN) ||
                        viewBean.getAnswerType().equals(AnswerType.MULTIPLE_LOOKUP_WITH_FILTER)) {
                    paramName = paramName + viewBean.getAnswerType();
                    saveMultiple = true;
                } else if (viewBean.getAnswerType().equals(AnswerType.MULTIPLE_WITH_DESCRIPTION) ||
                        viewBean.getAnswerType().equals(AnswerType.RADIO_WITH_DESCRIPTION) ||
                        viewBean.getAnswerType().equals(AnswerType.DROPDOWN_WITH_DESCRIPTION) ||
                        viewBean.getAnswerType().equals(AnswerType.MULTIPLE_ONE_DESCRIPTION)) {
                    paramName = paramName + viewBean.getAnswerType();
                    saveMultipleDescription = true;
                } else if (viewBean.getAnswerType().equals(AnswerType.IMAGE)
                        || viewBean.getAnswerType().equals(AnswerType.DRAWING)
                        || viewBean.getAnswerType().equals(AnswerType.SIGNATURE)
                        || viewBean.getAnswerType().equals(AnswerType.IMAGE_WITH_GEODATA)
                        || viewBean.getAnswerType().equals(AnswerType.IMAGE_WITH_GEODATA_GPS)
                        || viewBean.getAnswerType().equals(AnswerType.IMAGE_WITH_TIME)
                        || viewBean.getAnswerType().equals(AnswerType.IMAGE_WITH_GEODATA_TIME)
                        || viewBean.getAnswerType().equals(AnswerType.IMAGE_WITH_GPS_TIME)) {
                    saveImage = true;
                }

                if (saveText) {
                    this.submitOnlyText(paramName, request, viewBean);
                } else if (saveMultiple) {
                    this.submitMultipleWithoutDescription(paramName, request, viewBean);
                } else if (saveMultipleDescription) {
                    this.submitMultipleWithDescription(paramName, request, viewBean);
                } else if (saveImage) {
                    this.submitImage(frm, paramName, request, viewBean, i);
                }


            }
        }

    }


    public void submitAnswerDependency(HttpServletRequest request, Integer mobileAssignmentId, ViewAssignmentSurveyorForm myForm, int currPage) throws CoreException {
        List listQuestion = this.questionList;
        ViewAssignmentSurveyorBean test = null;
        ViewAssignmentSurveyorManager vasb = new ViewAssignmentSurveyorManager();

        Integer indx = new Integer(1);
        if (listQuestion != null) {
            for (int i = 0; i < listQuestion.size(); i++) {
                if (currPage == i) {
                    test = (ViewAssignmentSurveyorBean) listQuestion.get(i);
                    String paramName = "Soal" + (i + indx.intValue());

                    test.setMobileAssignmentId(mobileAssignmentId);

                    if (test.getOptionAnswerId() == null) {
                        test.setOptionAnswerId("");
                    }

                    if (test.getImagePath() == null) {
                        test.setImagePath("");
                    }

                    if (test.getOptionAnswerLabel() == null) {
                        test.setOptionAnswerLabel("");
                    }
                    String optAnswerLabel = test.getOptionAnswerLabel();
                    String txtAnswer = "";
                    test.setLAC(null);
                    test.setGpsLatitude(null);
                    test.setGpsLongitude(null);
                    test.setMCC(null);
                    test.setMNC(null);
                    test.setLAC(null);
                    test.setCellId(null);
                    test.setIsGps(null);
                    test.setAccuracy(null);
                    test.setLov(null);

                    // --Text Field--
                    if (test.getAnswerType().equals(AnswerType.TEXT)) {
                        paramName = paramName + AnswerType.TEXT;
                        test.setTextAnswer(request.getParameter(paramName));
                        txtAnswer = test.getTextAnswer();
                        this.manager.submitDependency(test);

                        // --Multiple--
                    } else if (test.getAnswerType().equals(AnswerType.MULTIPLE)) {
                        paramName = paramName + AnswerType.MULTIPLE;
                        String choiceVal = "";
                        String tempValue[] = request.getParameterValues(paramName);
                        String splitValue[] = null;
                        if (tempValue != null) {
                            this.manager.deleteMultipleAnswer(test);
                            for (int j = 0; j < tempValue.length; j++) {
                                System.out.println("option value 002: " + tempValue[j]);
                                splitValue = ViewAssignmentSurveyorManager.split(tempValue[j], "#");
                                for (int a = 0; a < splitValue.length; a++) {
                                    test.setOptionAnswerLabel(splitValue[1]);
                                    test.setOptionAnswerId(splitValue[0]);
                                }
                                this.manager.insertMultipleAnswer(test);

                            }
                        }

                        // --Multiple Desc--
                    } else if (test.getAnswerType().equals(
                            AnswerType.MULTIPLE_WITH_DESCRIPTION)) {
                        paramName = paramName
                                + AnswerType.MULTIPLE_WITH_DESCRIPTION;
                        String choiceVal = "";
                        String tempValue[] = request
                                .getParameterValues(paramName);
                        String splitValue[] = null;
                        if (tempValue != null) {
                            for (int j = 0; j < tempValue.length; j++) {
                                System.out.println("option value 003: " + tempValue[j]);
                                splitValue = ViewAssignmentSurveyorManager.split(tempValue[j], "#");
                                for (int a = 0; a < splitValue.length; a++) {
                                    test.setOptionAnswerLabel(splitValue[1]);
                                    test.setOptionAnswerId(splitValue[0]);
                                }
                                this.manager.submitDependency(test);

                            }
                        }

                        // --Radio--
                    } else if (test.getAnswerType().equals(AnswerType.RADIO)) {
                        paramName = paramName + AnswerType.RADIO;
                        String radioVal = "";
                        String tempValue = request.getParameter(paramName);
                        String splitValue[] = null;
                        if (tempValue != null) {
                            System.out.println("option value 004: " + tempValue);
                            splitValue = ViewAssignmentSurveyorManager.split(tempValue, "#");
                            for (int a = 0; a < splitValue.length; a++) {
                                test.setOptionAnswerLabel(splitValue[1]);
                                test.setOptionAnswerId(splitValue[0]);
                                radioVal = radioVal + splitValue[0] + "~"
                                        + splitValue[1] + ";";
                            }
                        }
                        this.manager.submitDependency(test);

                        // --Radio Desc--
                    } else if (test.getAnswerType().equals(
                            AnswerType.RADIO_WITH_DESCRIPTION)) {
                        paramName = paramName
                                + AnswerType.RADIO_WITH_DESCRIPTION;
                        String radioVal = "";
                        String tempValue = request.getParameter(paramName);
                        String splitValue[] = null;
                        if (tempValue != null) {
                            System.out.println("option value 005: " + tempValue);
                            splitValue = ViewAssignmentSurveyorManager.split(tempValue, "#");
                            for (int a = 0; a < splitValue.length; a++) {
                                test.setOptionAnswerLabel(splitValue[1]);
                                test.setOptionAnswerId(splitValue[0]);
                            }
                        }
                        this.manager.submitDependency(test);

                        // --Dropdown--
                    } else if (test.getAnswerType().equals(AnswerType.DROPDOWN)) {
                        paramName = paramName + AnswerType.DROPDOWN;
                        String tempValue = request.getParameter(paramName);
                        String splitValue[] = null;
                        if (tempValue != null) {
                            System.out.println("option value 006: " + tempValue);
                            splitValue = ViewAssignmentSurveyorManager.split(tempValue, "#");
                            for (int a = 0; a < splitValue.length; a++) {
                                test.setOptionAnswerLabel(splitValue[1]);
                                test.setOptionAnswerId(splitValue[0]);
                            }
                        }
                        this.manager.submitDependency(test);

                        // --Dropdown Desc--
                    } else if (test.getAnswerType().equals(
                            AnswerType.DROPDOWN_WITH_DESCRIPTION)) {
                        paramName = paramName
                                + AnswerType.DROPDOWN_WITH_DESCRIPTION;
                        String tempValue = request.getParameter(paramName);
                        String splitValue[] = null;
                        if (tempValue != null) {
                            System.out.println("option value 007: " + tempValue);
                            splitValue = ViewAssignmentSurveyorManager.split(tempValue, "#");
                            for (int a = 0; a < splitValue.length; a++) {
                                test.setOptionAnswerLabel(splitValue[1]);
                                test.setOptionAnswerId(splitValue[0]);
                            }
                        }
                        this.manager.submitDependency(test);

                        // --Numeric--
                    } else if (test.getAnswerType().equals(AnswerType.NUMERIC)) {
                        paramName = paramName + AnswerType.NUMERIC;
                        test.setTextAnswer(request.getParameter(paramName));
                        txtAnswer = test.getTextAnswer();
                        this.manager.submitDependency(test);

                        // --Decimal--
                    } else if (test.getAnswerType().equals(AnswerType.DECIMAL)) {
                        paramName = paramName + AnswerType.DECIMAL;
                        test.setTextAnswer(request.getParameter(paramName));
                        txtAnswer = test.getTextAnswer();
                        this.manager.submitDependency(test);
                        txtAnswer = test.getTextAnswer();

                        // --Date--
                    } else if (test.getAnswerType().equals(AnswerType.DATE)) {
                        paramName = paramName + AnswerType.DATE;
                        test.setTextAnswer(request.getParameter(paramName));
                        txtAnswer = test.getTextAnswer();
                        this.manager.submitDependency(test);

                        // --DateTime
                    } else if (test.getAnswerType().equals(AnswerType.DATE_TIME)) {
                        paramName = paramName + AnswerType.DATE_TIME;
                        test.setTextAnswer(request.getParameter(paramName));
                        txtAnswer = test.getTextAnswer();
                        this.manager.submitDependency(test);

                        // --image--
                    } else if (test.getAnswerType().equals(AnswerType.IMAGE)) {

                        paramName = paramName + AnswerType.IMAGE;
                        test.setTextAnswer(request.getParameter(paramName));
                        txtAnswer = test.getTextAnswer();
                        String pathImage = "";

                        FormFile myFile = myForm.getImage();
                        String fileName = "";
                        if (myFile != null) {
                            fileName = myFile.getFileName();
                        }

                        System.out.println("fileName: " + fileName);
                        try {

                            String filePath = ConfigurationProperties.getInstance().get(PropertiesKey.ANSWER_IMAGE_PATH);
                            StringBuffer surveyId = new StringBuffer(
                                    String.valueOf(test.getMobileAssignmentId()))
                                    .append("_")
                                    .append(test.getQuestionGroupId())
                                    .append("_").append(test.getQuestionId());

                            if (!fileName.equals("")) {
                                System.out.println("Server path:" + filePath);
                                // Create file
                                fileName = writeImageToFile(filePath,
                                        myFile.getFileData(),
                                        surveyId.toString());
                                test.setImagePath(fileName);

                            }
                            pathImage = test.getImagePath();
                            request.setAttribute("fileName", fileName);
                            this.manager.submitDependency(test);

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            this.logger.printStackTrace(e);
                        } catch (IOException e) {
                            e.printStackTrace();
                            this.logger.printStackTrace(e);
                        }

                        // --image geodata-
                    } else if (test.getAnswerType().equals(
                            AnswerType.IMAGE_WITH_GEODATA)) {
                        paramName = paramName + AnswerType.IMAGE_WITH_GEODATA;

                        FormFile myFile = myForm.getImage();
                        // String contentType = myFile.getContentType();
                        int sizeFile = myFile.getFileSize();
                        System.out.println("file size: " + sizeFile);
                        String fileName = myFile.getFileName();
                        System.out.println("myFileName: " + fileName);
                        try {
                            byte[] fileData = myFile.getFileData();
                            System.out.println("fileData: " + fileData);
                            String filePath = getServlet().getServletContext()
                                    .getRealPath("/");
                            System.out.println("filePath: " + filePath);
                            test.setImagePath(filePath);

                            if (!fileName.equals("")) {
                                System.out.println("Server path:" + filePath);
                                // Create file

                                File fileToCreate = new File(filePath, fileName);
                                // If file does not exists create file

                                if (!fileToCreate.exists()) {

                                    FileOutputStream fileOutStream = new FileOutputStream(fileToCreate);

                                    fileOutStream.write(myFile.getFileData());

                                    fileOutStream.flush();

                                    fileOutStream.close();

                                }

                            }
                            request.setAttribute("fileName", fileName);

                        } catch (FileNotFoundException e) {
                            System.out.println(e.getMessage());
                            e.printStackTrace();
                        } catch (IOException e) {
                            System.out.println(e.getMessage());
                            e.printStackTrace();
                        }
                        this.manager.submitDependency(test);

                        // --Lookup--
                    } else if (test.getAnswerType().equals(AnswerType.LOOKUP)) {
                        paramName = paramName + AnswerType.LOOKUP;
                        test.setTextAnswer(request.getParameter(paramName));
                        txtAnswer = test.getTextAnswer();
                        this.manager.submitDependency(test);

                        // --lookup with Filter--
                    } else if (test.getAnswerType().equals(
                            AnswerType.LOOKUP_WITH_FILTER)) {
                        paramName = paramName + AnswerType.LOOKUP_WITH_FILTER;
                        test.setTextAnswer(request.getParameter(paramName));
                        txtAnswer = test.getTextAnswer();
                        this.manager.submitDependency(test);

                        // --Drawing--
                    } else if (test.getAnswerType().equals(AnswerType.DRAWING)) {
                        paramName = paramName + AnswerType.DRAWING;
                        test.setTextAnswer(request.getParameter(paramName));
                        txtAnswer = test.getTextAnswer();
                        this.manager.submitDependency(test);

                    }

                }

            }
        }

    }

    private String writeImageToFile(String imagePath, byte[] image,
                                    String surveyId) throws IOException {
        String dateFormat = "yyyy-MM-dd";
        String date = TreemasFormatter.formatDate(new Date(), dateFormat);

        imagePath = imagePath + date + System.getProperty("file.separator");

        FileOutputStream fileOutStream = null;
        File directory = new File(imagePath);

        if (!directory.exists())
            directory.mkdirs();

        StringBuffer imageFile = new StringBuffer().append(imagePath)
                .append(surveyId).append("_").append(new Date().getTime())
                .append(".jpg");

        fileOutStream = new FileOutputStream(imageFile.toString());

        fileOutStream.write(image);

        fileOutStream.flush();

        fileOutStream.close();

        return imageFile.toString();
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();

        switch (code) {
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        String task = ((ViewAssignmentSurveyorForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

    public void cekIndek(HttpServletRequest request,
                         Integer mobileAssignmentId, ViewAssignmentSurveyorForm frm,
                         String act) {
        int idx = frm.getCurrPage().intValue();
        int totalSoal = frm.getTotPage().intValue();
        int page = 0;
        ViewAssignmentSurveyorBean test;
        String isVisibleQuestAnswer = "";
        String answrPrevQuest = "";
        String tempRelQuestId = "";
        String questionLabel = "";
        int relQuestionId = 0;
        int questionGroupId = 0;
        String arrRelQuest[] = null;
        boolean tampil = false;
        try {
            List listQuestion = this.questionList;
            submitAnswerDependency(request, mobileAssignmentId, frm, idx);
            if (idx < totalSoal) {
                if (act.equalsIgnoreCase("LoadPreviousQuestion")) {
                    page = idx - 1;
                } else {
                    page = idx + 1;
                }

                for (int i = 0; i < listQuestion.size(); i++) {
                    test = (ViewAssignmentSurveyorBean) listQuestion.get(i);
                    questionGroupId = test.getQuestionGroupId().intValue();
                    relQuestionId = test.getQuestionId().intValue();
                    questionLabel = test.getQuestionLabel();
                    tempRelQuestId = test.getRelQuestions();
                    if (i == page && test.getRelQuestions() != null) {
                        tempRelQuestId = test.getRelQuestions();
                        String jumRelQuest[] = this.manager.split(tempRelQuestId, "#");
                        if (jumRelQuest.length > 1) {
                            for (int j = 0; j < jumRelQuest.length; j++) {
                                String isiJumRelQuest = jumRelQuest[j].toString();
                                arrRelQuest = this.manager.split(isiJumRelQuest, "~");
                                int questGroupId = Integer.parseInt(arrRelQuest[0].toString());
                                int relQuestId = Integer.parseInt(arrRelQuest[1].toString());
                                List listRelAnswer = this.manager.doGetRelAnswer(mobileAssignmentId.intValue(), questGroupId, relQuestId, test.getQuestionId().intValue());
                                if (listRelAnswer != null) {
                                    for (int a = 0; a < listRelAnswer.size(); a++) {
                                        ViewAssignmentSurveyorBean test2 = (ViewAssignmentSurveyorBean) listRelAnswer.get(a);
                                        isVisibleQuestAnswer = test2.getIsVisibleQA();
                                        String relTxtAnswr = test2.getRelTextAnswer();
                                        if (AnswerType.DROPDOWN.equals(test2.getAnswerType())
                                                || AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(test2.getAnswerType())
                                                || AnswerType.MULTIPLE.equals(test2.getAnswerType())
                                                || AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(test2.getAnswerType())) {
                                            answrPrevQuest = test2.getOptionAnswerId();
                                            if ("1".equals(isVisibleQuestAnswer)) {
                                                if (answrPrevQuest.equals(test2.getRelOptionAnswerId())) {
                                                    tampil = true;
                                                    break;
                                                } else {
                                                    tampil = false;
                                                }
                                            } else {
                                                tampil = false;
                                            }
                                        } else {
                                            answrPrevQuest = test2.getTextAnswer();
                                            if ((answrPrevQuest != null && "1".equals(isVisibleQuestAnswer))
                                                    || ((answrPrevQuest == null || ""
                                                    .equals(answrPrevQuest)) && "1"
                                                    .equals(isVisibleQuestAnswer))) {
                                                if (answrPrevQuest.equals(relTxtAnswr)) {
                                                    tampil = true;
                                                    break;
                                                } else {
                                                    tampil = false;

                                                }
                                            } else {
                                                tampil = false;
                                            }
                                        }
                                    }
                                }

                            }
                            break;
                        } else {
                            String isiJumRelQuest = jumRelQuest[0].toString();
                            arrRelQuest = this.manager.split(isiJumRelQuest, "~");
                            int questGroupId = Integer.parseInt(arrRelQuest[0].toString());
                            int relQuestId = Integer.parseInt(arrRelQuest[1].toString());

                            List listRelAnswer = this.manager.doGetRelAnswer(
                                    mobileAssignmentId.intValue(), questGroupId,
                                    relQuestId, test.getQuestionId().intValue());
                            if (listRelAnswer != null) {
                                for (int a = 0; a < listRelAnswer.size(); a++) {
                                    ViewAssignmentSurveyorBean test2 = (ViewAssignmentSurveyorBean) listRelAnswer.get(a);
                                    isVisibleQuestAnswer = test2.getIsVisibleQA();
                                    String relTxtAnswr = test2
                                            .getRelTextAnswer();
                                    if (AnswerType.DROPDOWN.equals(test2
                                            .getAnswerType())
                                            || AnswerType.DROPDOWN_WITH_DESCRIPTION
                                            .equals(test2
                                                    .getAnswerType())
                                            || AnswerType.MULTIPLE.equals(test2
                                            .getAnswerType())
                                            || AnswerType.MULTIPLE_WITH_DESCRIPTION
                                            .equals(test2
                                                    .getAnswerType())) {
                                        answrPrevQuest = test2.getOptionAnswerId();
                                        if ("1".equals(isVisibleQuestAnswer)) {
                                            if (answrPrevQuest.equals(test2.getRelOptionAnswerId())) {
                                                tampil = true;
                                                break;
                                            } else {
                                                tampil = false;
                                                break;
                                            }
                                        } else {// jika visible=0
                                            tampil = false;
                                            break;
                                        }
                                    } else {
                                        answrPrevQuest = test2.getTextAnswer();
                                        if ((answrPrevQuest != null && "1".equals(isVisibleQuestAnswer))
                                                || ((answrPrevQuest == null || "".equals(answrPrevQuest)) &&
                                                "1".equals(isVisibleQuestAnswer))) {
                                            if (answrPrevQuest.equals(relTxtAnswr)) {
                                                tampil = true;
                                                break;
                                            } else {
                                                tampil = false;
                                                break;
                                            }
                                        } else {// jika visible=0
                                            tampil = false;
                                            break;
                                        }
                                    }
                                }
                            }

                        }

                    } else {
                        tampil = true;

                    }
                }

                if (tampil != false) {
                    System.out.println("relQuestionId yg akan diupdate visibility-nya: "
                            + relQuestionId
                            + "\n Soal: "
                            + questionLabel);
                    this.manager.updateVisibleDependecy(mobileAssignmentId.intValue(), questionGroupId, relQuestionId);
                    frm.setCurrPage(new Integer(page));
                } else {
                    System.out.println("relQuestionId yg akan diupdate visibility-nya: "
                            + relQuestionId
                            + "\n Soal: "
                            + questionLabel);
                    this.manager.updateInvisibleDependecy(mobileAssignmentId.intValue(),
                            questionGroupId, relQuestionId);
                    if (act.equalsIgnoreCase("LoadPreviousQuestion")) {
                        page = page - 1;
                    } else {
                        page = page + 1;
                    }
                    frm.setCurrPage(new Integer(page));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            this.logger.printStackTrace(e);
        }

    }

}
