package treemas.application.feature.view.assignmentsurveyor;

import org.apache.struts.upload.FormFile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ViewAssignmentSurveyorBean implements Serializable {
    private Integer currPage;
    private Integer totPage;
    private Integer indeks;


    private Integer mobileAssignmentId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;
    private Date rfaDate;
    private Date assignmentDate;
    private Date retrieveDate;
    private Date submitDate;
    private Date finalizationDate;
    private String surveyor;
    private String schemeId;
    private String schemeDescription;
    private String status;
    private String priority;
    private String notes;
    private String branchId;
    private String branchName;

    private Integer questionGroupId;
    private Integer questionId;
    private Integer questionOrder;
    private Integer isVisible;
    private String questionGroupName;
    private String relQuestions;
    private String relTextAnswer;
    private String relOptionAnswerId;
    private String questionLabel;
    private String answerType;
    private String optionAnswerId;
    private String optionAnswerText;
    private String optionAnswerLabel;
    private String isVisibleQA;
    private String lookupCode;
    private String lookupAnswer;
    private String isMandatory;
    private String maxLength;
    private String textAnswer;
    private String hasImage;
    private String imagePath;
    private Double gpsLatitude;
    private Double gpsLongitude;
    private String isGps;
    private Integer accuracy;
    private String oldOptionAnswerLabel;
    private String oldTextAnswer;
    private String finalOptionAnswerLabel;
    private String finalTextAnswer;
    private String finalUseImage;
    private String paramName;
    private String provider;
    private Integer MCC;
    private Integer MNC;
    private Integer LAC;
    private Integer cellId;
    private String lov;
    private FormFile image;
    private List optionAnswer = new ArrayList();
    private String selectedOption;
    private List imageList;


    public Integer getIndeks() {
        return indeks;
    }

    public void setIndeks(Integer indeks) {
        indeks = indeks;
    }

    public String getRelQuestions() {
        return relQuestions;
    }

    public void setRelQuestions(String relQuestions) {
        this.relQuestions = relQuestions;
    }

    public FormFile getImage() {
        return image;
    }

    public void setImage(FormFile image) {
        this.image = image;
    }

    public String getLov() {
        return lov;
    }

    public void setLov(String lov) {
        this.lov = lov;
    }

    public Integer getMCC() {
        return MCC;
    }

    public void setMCC(Integer mCC) {
        MCC = mCC;
    }

    public Integer getMNC() {
        return MNC;
    }

    public void setMNC(Integer mNC) {
        MNC = mNC;
    }

    public Integer getLAC() {
        return LAC;
    }

    public void setLAC(Integer lAC) {
        LAC = lAC;
    }

    public Integer getCellId() {
        return cellId;
    }

    public void setCellId(Integer cellId) {
        this.cellId = cellId;
    }

    public String getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }

    public String getLookupAnswer() {
        return lookupAnswer;
    }

    public void setLookupAnswer(String lookupAnswer) {
        this.lookupAnswer = lookupAnswer;
    }

    public String getLookupCode() {
        return lookupCode;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public Integer getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(Integer mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public Date getRfaDate() {
        return rfaDate;
    }

    public void setRfaDate(Date rfaDate) {
        this.rfaDate = rfaDate;
    }

    public Date getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(Date assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public Date getRetrieveDate() {
        return retrieveDate;
    }

    public void setRetrieveDate(Date retrieveDate) {
        this.retrieveDate = retrieveDate;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public Date getFinalizationDate() {
        return finalizationDate;
    }

    public void setFinalizationDate(Date finalizationDate) {
        this.finalizationDate = finalizationDate;
    }

    public String getSurveyor() {
        return surveyor;
    }

    public void setSurveyor(String surveyor) {
        this.surveyor = surveyor;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getOptionAnswerId() {
        return optionAnswerId;
    }

    public void setOptionAnswerId(String optionAnswerId) {
        this.optionAnswerId = optionAnswerId;
    }

    public String getOptionAnswerLabel() {
        return optionAnswerLabel;
    }

    public void setOptionAnswerLabel(String optionAnswerLabel) {
        this.optionAnswerLabel = optionAnswerLabel;
    }

    public String getTextAnswer() {
        return textAnswer;
    }

    public void setTextAnswer(String textAnswer) {
        this.textAnswer = textAnswer;
    }

    public String getHasImage() {
        return hasImage;
    }

    public void setHasImage(String hasImage) {
        this.hasImage = hasImage;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public String getIsGps() {
        return isGps;
    }

    public void setIsGps(String isGps) {
        this.isGps = isGps;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public String getOldOptionAnswerLabel() {
        return oldOptionAnswerLabel;
    }

    public void setOldOptionAnswerLabel(String oldOptionAnswerLabel) {
        this.oldOptionAnswerLabel = oldOptionAnswerLabel;
    }

    public String getOldTextAnswer() {
        return oldTextAnswer;
    }

    public void setOldTextAnswer(String oldTextAnswer) {
        this.oldTextAnswer = oldTextAnswer;
    }

    public String getFinalOptionAnswerLabel() {
        return finalOptionAnswerLabel;
    }

    public void setFinalOptionAnswerLabel(String finalOptionAnswerLabel) {
        this.finalOptionAnswerLabel = finalOptionAnswerLabel;
    }

    public String getFinalTextAnswer() {
        return finalTextAnswer;
    }

    public void setFinalTextAnswer(String finalTextAnswer) {
        this.finalTextAnswer = finalTextAnswer;
    }

    public String getFinalUseImage() {
        return finalUseImage;
    }

    public void setFinalUseImage(String finalUseImage) {
        this.finalUseImage = finalUseImage;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Integer getTotPage() {
        return totPage;
    }

    public void setTotPage(Integer totPage) {
        this.totPage = totPage;
    }

    public Integer getCurrPage() {
        return currPage;
    }

    public void setCurrPage(Integer currPage) {
        this.currPage = currPage;
    }

    public Integer getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(Integer questionOrder) {
        this.questionOrder = questionOrder;
    }

    public Integer getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(Integer isVisible) {
        this.isVisible = isVisible;
    }

    public String getRelTextAnswer() {
        return relTextAnswer;
    }

    public void setRelTextAnswer(String relTextAnswer) {
        this.relTextAnswer = relTextAnswer;
    }

    public String getRelOptionAnswerId() {
        return relOptionAnswerId;
    }

    public void setRelOptionAnswerId(String relOptionAnswerId) {
        this.relOptionAnswerId = relOptionAnswerId;
    }

    public String getOptionAnswerText() {
        return optionAnswerText;
    }

    public void setOptionAnswerText(String optionAnswerText) {
        this.optionAnswerText = optionAnswerText;
    }

    public String getIsVisibleQA() {
        return isVisibleQA;
    }

    public void setIsVisibleQA(String isVisibleQA) {
        this.isVisibleQA = isVisibleQA;
    }

    public List getOptionAnswer() {
        return optionAnswer;
    }

    public void setOptionAnswer(List optionAnswer) {
        this.optionAnswer = optionAnswer;
    }


    public String getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(String selectedOption) {
        this.selectedOption = selectedOption;
    }

    public List getImageList() {
        return imageList;
    }

    public void setImageList(List imageList) {
        this.imageList = imageList;
    }

}
