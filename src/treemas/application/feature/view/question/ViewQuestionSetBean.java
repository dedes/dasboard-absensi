package treemas.application.feature.view.question;

import java.io.Serializable;

public class ViewQuestionSetBean implements Serializable {
    private String schemeId;
    private String schemeDescription;
    private Integer questionGroupId;
    private String questionGroupName;
    private Integer questionId;
    private String questionLabel;
    private String answerType;
    private String optionAnswer;
    private String isMandatory;
    private Integer lineSeqOrder;

    private String isHaveAnswer;

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getOptionAnswer() {
        return optionAnswer;
    }

    public void setOptionAnswer(String optionAnswer) {
        this.optionAnswer = optionAnswer;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public Integer getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(Integer lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

    public String getIsHaveAnswer() {
        return isHaveAnswer;
    }

    public void setIsHaveAnswer(String isHaveAnswer) {
        this.isHaveAnswer = isHaveAnswer;
    }
}
