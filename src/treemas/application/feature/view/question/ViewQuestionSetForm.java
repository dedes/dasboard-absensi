package treemas.application.feature.view.question;

import treemas.base.view.ViewFormBase;

public class ViewQuestionSetForm extends ViewFormBase {
    private String schemeId;
    private String schemeDescription;
    private String questionGroupId;
    private String questionGroupName;
    private String questionId;
    private String questionLabel;
    private String answerType;
    private String optionAnswer;
    private String isMandatory;
    private String lineSeqOrder;

    private String isHaveAnswer;

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getOptionAnswer() {
        return optionAnswer;
    }

    public void setOptionAnswer(String optionAnswer) {
        this.optionAnswer = optionAnswer;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(String lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

    public String getIsHaveAnswer() {
        return isHaveAnswer;
    }

    public void setIsHaveAnswer(String isHaveAnswer) {
        this.isHaveAnswer = isHaveAnswer;
    }
}
