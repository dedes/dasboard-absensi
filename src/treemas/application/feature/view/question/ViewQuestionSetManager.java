package treemas.application.feature.view.question;

import treemas.base.view.ViewManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.Tools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ViewQuestionSetManager extends ViewManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_VIEW_QUESTIONS;

    public ViewQuestionSetBean getHeader(String schemeId)
            throws CoreException {
        ViewQuestionSetBean bean = null;

        try {
            bean = (ViewQuestionSetBean) this.ibatisSqlMap.
                    queryForObject(this.STRING_SQL + "getHeader", schemeId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return bean;
    }

    public PageListWrapper getListQuestionSet(String schemeId)
            throws CoreException {
        PageListWrapper result = new PageListWrapper();

        try {
            List listQuestionGroup = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getQuestionGroup", schemeId);

            List list = this.getQuestionSet(listQuestionGroup);
            result.setResultList(list);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return result;
    }

    private List getQuestionSet(List listQuestionGroup) throws SQLException {
        List list = new ArrayList();

        for (Iterator iterator = listQuestionGroup.iterator(); iterator.hasNext(); ) {
            Integer questionGroupId = (Integer) iterator.next();
            List tempList = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getQuestionSet", questionGroupId);

            for (Iterator iterator2 = tempList.iterator(); iterator2.hasNext(); ) {
                ViewQuestionSetBean tempBean = (ViewQuestionSetBean) iterator2.next();
                String optionAnswer = tempBean.getOptionAnswer();
                if (optionAnswer != null && "1".equals(tempBean.getIsHaveAnswer())) {
                    optionAnswer = this.formatOptionAnswer(optionAnswer);
                    tempBean.setOptionAnswer(optionAnswer);
                } else if (optionAnswer != null && "0".equals(tempBean.getIsHaveAnswer())) { //lookup
                    optionAnswer = this.formatTextLookup(optionAnswer);
                    tempBean.setOptionAnswer(optionAnswer);
                }
            }
            list.add(tempList);
        }

        return list;
    }


    private String formatOptionAnswer(String input) {
        StringBuffer output = new StringBuffer();

        String[] temp = Tools.split(input, "#");
        for (int i = 0; i < temp.length; i++) {
            String[] tmp = Tools.split(temp[i], "~");
            if (i > 0)
                output.append(" | ");
            output.append(tmp[1]);
        }

        return output.toString();
    }


    private String formatTextLookup(String lookupCode) throws SQLException {
        String tableName = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getLookupInfo", lookupCode);
        String result = null;

        if (tableName != null) {
            StringBuffer output = new StringBuffer("Lookup table: ");
            output.append(tableName);
            result = output.toString();
        }

        return result;
    }
}
