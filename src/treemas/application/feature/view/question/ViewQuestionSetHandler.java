package treemas.application.feature.view.question;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.view.ViewActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.List;

public class ViewQuestionSetHandler extends ViewActionBase {

    private ViewQuestionSetManager manager = new ViewQuestionSetManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        ViewQuestionSetForm viewQuestionSetForm = (ViewQuestionSetForm) form;
        String action = viewQuestionSetForm.getTask();
        String schemeId = viewQuestionSetForm.getSchemeId();
        viewQuestionSetForm.getPaging().calculationPage();

        if (Global.WEB_TASK_LOAD.equals(action)) {
            this.getHeader(viewQuestionSetForm, schemeId);
            this.load(request, schemeId);
        }

        return this.getNextPage(action, mapping);
    }

    private void getHeader(ViewQuestionSetForm viewQuestionSetForm,
                           String schemeId) throws CoreException {
        ViewQuestionSetBean bean = null;
        bean = this.manager.getHeader(schemeId);
        if (bean != null) {
            BeanConverter.convertBeanToString(bean, viewQuestionSetForm);
        }
    }

    private void load(HttpServletRequest request, String schemeId)
            throws CoreException {
        PageListWrapper result = this.manager.getListQuestionSet(schemeId);
        List list = result.getResultList();
        if (list != null) {
            for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
                List tempList = (List) iterator.next();
                tempList = BeanConverter.convertBeansToString(tempList,
                        ViewQuestionSetForm.class);
            }
        }
        request.setAttribute("listQuestionSet", list);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();

        switch (code) {
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        String task = ((ViewQuestionSetForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
