package treemas.application.feature.view.activity;

import java.io.Serializable;

public class ReportBranchActivityBean implements Serializable {
    private String surveyor;
    private String nama;
    private String dayactivity;
    private String lastLog;
    private String lastActivityLog;

    public String getSurveyor() {
        return surveyor;
    }

    public void setSurveyor(String surveyor) {
        this.surveyor = surveyor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getLastLog() {
        return lastLog;
    }

    public void setLastLog(String lastLog) {
        this.lastLog = lastLog;
    }

    public String getLastActivityLog() {
        return lastActivityLog;
    }

    public void setLastActivityLog(String lastActivityLog) {
        this.lastActivityLog = lastActivityLog;
    }

    public String getDayactivity() {
        return dayactivity;
    }

    public void setDayactivity(String dayactivity) {
        this.dayactivity = dayactivity;
    }

}
