package treemas.application.feature.view.activity;

import treemas.base.inquiry.InquiryFormBase;

public class ReportBranchActivityForm extends InquiryFormBase {
    private String surveyor;
    private String nama;
    private String lastLog;
    private String lastActivityLog;
    private String searchBranch;
    private String searchBranchName;
    private String searchDate;
    private String filter;
    private String fullName;

    public String getSearchBranch() {
        return searchBranch;
    }

    public void setSearchBranch(String searchBranch) {
        this.searchBranch = searchBranch;
    }

    public String getSurveyor() {
        return surveyor;
    }

    public void setSurveyor(String surveyor) {
        this.surveyor = surveyor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getLastLog() {
        return lastLog;
    }

    public void setLastLog(String lastLog) {
        this.lastLog = lastLog;
    }

    public String getLastActivityLog() {
        return lastActivityLog;
    }

    public void setLastActivityLog(String lastActivityLog) {
        this.lastActivityLog = lastActivityLog;
    }

    public String getSearchBranchName() {
        return searchBranchName;
    }

    public void setSearchBranchName(String searchBranchName) {
        this.searchBranchName = searchBranchName;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

}
