package treemas.application.feature.view.activity;

import com.google.common.base.Strings;
import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.format.TreemasFormatter;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class ReportBranchActivityManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_REPORT;

    public List getReportBranchList(String branch) throws CoreException {
        List reportList = null;
        try {
            reportList = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getReportBranchActivity", branch);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_DB);
        }

        return reportList;
    }

//	public List getDetailReportBranchList(String branch, String user) throws CoreException {
//		List reportList = null;
//		Map param = new HashMap();
//		param.put("branch", branch);
//		param.put("user", user);
//		try {
//				reportList = this.ibatisSqlMap.queryForList(
//						this.STRING_SQL+"getDetailReportBranchActivity",param);
//		}
//		catch (SQLException e) {
//			this.logger.printStackTrace(e);
//			throw new CoreException(e, ERROR_DB);
//		}
//
//		return reportList;
//	}

    public PageListWrapper getDetailActivity(String branch, String user, Integer targetPage, ReportBranchActivityForm reportForm) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        SearchFormParameter param = new SearchFormParameter();
        List reportList = null;

        param.setPage(targetPage);
        Map paramMap = param.toMap();
        paramMap.put("branch", branch);
        paramMap.put("user", user);

        String filter = null;
        String date = null;
        Date input = null;

        if (!Strings.isNullOrEmpty(reportForm.getFilter())) {
            filter = reportForm.getFilter();
            if (filter.equals("0")) {
                filter = null;
            }
        }

        if (!Strings.isNullOrEmpty(reportForm.getSearchDate())) {
            date = reportForm.getSearchDate();
            try {
                input = TreemasFormatter.parseDate(date, TreemasFormatter.DFORMAT_DMY);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            filter = null;
            date = null;
        }

        paramMap.put("filter", filter);
        paramMap.put("date", input);

        try {
            reportList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getDetailReportBranchActivity", paramMap);
            Integer size = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getDetailCnt", paramMap);
            result.setResultList(reportList);
            result.setTotalRecord(size.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_DB);
        }

        return result;
    }


    public String getFirstMultiBranchByUserId(String userId) {
        String branchId = null;
        try {
            branchId = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getFirstMultiBranchByUserId", userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return branchId;
    }

    public String getFirstMultiLabelByUserId(String userId) {
        String branchId = null;
        try {
            branchId = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getFirstMultiLabelByUserId", userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return branchId;
    }

    public String getFullName(String userId) {
        String fullName = null;
        try {
            fullName = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getFullName", userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fullName;
    }
}
