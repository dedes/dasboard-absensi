package treemas.application.feature.view.activity;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.feature.general.ComboManager;
import treemas.base.report.ReportActionBase;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class ReportBranchActivityHandler extends ReportActionBase {
    private ReportBranchActivityManager manager = new ReportBranchActivityManager();
    private ComboManager comboManager = new ComboManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        ReportBranchActivityForm frm = (ReportBranchActivityForm) form;
        frm.getPaging().calculationPage();
        String task = frm.getTask();
        if (Global.WEB_TASK_LOAD.equals(task) ||
                Global.WEB_TASK_SEARCH.equals(task)) {

            String branch = frm.getSearchBranch();
            String user = frm.getSurveyor();
            String label = this.getFirstMultiLabelByLogin(user);
            String fullName = this.getFullName(user);
            frm.setSurveyor(user);
            frm.setSearchBranch(branch);
            frm.setSearchBranchName(label);
            frm.setFullName(user + " - " + fullName);

            this.loadListData(request, frm);

        }
        return this.getNextPage(task, mapping);
    }

    private void loadListData(HttpServletRequest request, ReportBranchActivityForm reportForm) throws CoreException {

        String branch = reportForm.getSearchBranch();
        String user = reportForm.getSurveyor();

        reportForm.setSurveyor(user);
        reportForm.setSearchBranch(branch);
        Integer targetPage = reportForm.getPaging().getTargetPageNo();
        PageListWrapper listWrapper = this.manager.getDetailActivity(branch, user, targetPage, reportForm);
        List list = listWrapper.getResultList();
//		list = BeanConverter.convertBeansToString(list, ReportForm.class);
        PagingInfo page = reportForm.getPaging();
        page.setTotalRecord(listWrapper.getTotalRecord());
        page.setCurrentPageNo(targetPage);

        request.setAttribute("listDetailReportBranch", list);
    }

    private void loadBranch(HttpServletRequest request, String branchId)
            throws CoreException {
        List list = comboManager.getComboCabangByLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadMultiBranch(HttpServletRequest request, String userId)
            throws CoreException {
        List list = comboManager.getComboMultiCabangByLogin(userId);
        request.setAttribute("comboBranch", list);
    }

    private String getFirstMultiBranchByLogin(String userId)
            throws CoreException {
        String branchId = null;
        branchId = this.manager.getFirstMultiBranchByUserId(userId);
        return branchId;
    }

    private String getFirstMultiLabelByLogin(String userId)
            throws CoreException {
        String branchId = null;
        branchId = this.manager.getFirstMultiLabelByUserId(userId);
        return branchId;
    }

    private String getFullName(String userId)
            throws CoreException {
        String fullName = null;
        fullName = this.manager.getFullName(userId);
        return fullName;
    }
}
