package treemas.application.feature.view.assignment;

import com.google.common.base.Strings;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.CharacterData;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import treemas.application.feature.FeatureManager;
import treemas.application.feature.task.MobileAssignmentHistoryForm;
import treemas.base.sec.term.UserImpl;
import treemas.base.view.ViewActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.format.TreemasDateFormatter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewAssignmentHandler extends ViewActionBase {
    private Map excConverterMap = new HashMap();
    private ViewAssignmentManager manager = new ViewAssignmentManager();
//	private Map cordConverterMap = new HashMap();

    public ViewAssignmentHandler() {
        this.pageMap.put("ViewAnswer", "answer");
        this.pageMap.put("Map", "map");
        this.pageMap.put("Print", "print");

        this.exceptionMap.put("ViewAnswer", "view");
        this.exceptionMap.put("Map", "view");
        this.exceptionMap.put("Print", "view");

        TreemasDateFormatter convertDMYTime = TreemasDateFormatter.getInstance();
        this.excConverterMap.put("assignmentDate", convertDMYTime);
        this.excConverterMap.put("retrieveDate", convertDMYTime);
        this.excConverterMap.put("submitDate", convertDMYTime);
        this.excConverterMap.put("finalizationDate", convertDMYTime);

        TreemasDateFormatter convertDecimal = TreemasDateFormatter.getInstance();
        this.excConverterMap.put("gpsLatitude", convertDecimal);
        this.excConverterMap.put("gpsLongitude", convertDecimal);
    }

    public static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        ViewAssignmentForm viewAssignmentForm = (ViewAssignmentForm) form;
        String action = viewAssignmentForm.getTask();
        Integer mobileAssignmentId = new Integer(
                viewAssignmentForm.getMobileAssignmentId());
        viewAssignmentForm.getPaging().calculationPage();

        if (!"/nc/View".equals(mapping.getPath())) {
            if (!this.manager.checkIllegalAccess(
                    this.getLoginId(request),
                    this.getLoginBean(request).getBranchId(), mobileAssignmentId)) {
                return mapping.findForward("not_authorized");
            }
            if (!this.manager.checkIllegalAccess(this.getLoginId(request),
                    mobileAssignmentId)) {
                return mapping.findForward("not_authorized");
            }
        }
        if (Global.WEB_TASK_LOAD.equals(action)) {
//			this.loadHistory(request, mobileAssignmentId);
            this.getHeader(viewAssignmentForm, mobileAssignmentId);
            this.loadJawaban(request, mobileAssignmentId);
            String addr = viewAssignmentForm.getCustomerAddress();
            Map map = new HashMap();
            if (null != viewAssignmentForm.getGpsLatitude() && null != viewAssignmentForm.getGpsLongitude()) {
                map.put("lat", viewAssignmentForm.getGpsLatitude());
                map.put("lng", viewAssignmentForm.getGpsLongitude());
            } else if (!Strings.isNullOrEmpty(addr)) {
                addr = addr.replaceAll(" ", "%20").trim();
                try {
                    map = this.convertGPSLocation(addr);
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                }
            }
            request.setAttribute("location", map);

            viewAssignmentForm.setCustomerAddressView(addr.trim());

        } else if (Global.WEB_TASK_BACK.equals(action)) {
            return this.doBack(request);
        } else if ("ViewAnswer".equals(action)) {
//			this.getHeader(viewAssignmentForm, mobileAssignmentId);
//			this.loadJawaban(request, mobileAssignmentId);

            // if (viewAssignmentForm.getAssignmentDate() == null) {
            // return mapping.findForward("answer");
            // }
            // else {
            // return mapping.findForward("verification");
            // }
            return mapping.findForward("answer");
        } else if ("Print".equals(action)) {
            String reportFileName = this.manager.getReportFileForScheme(viewAssignmentForm.getSchemeId());
            List QA = this.manager.getQandA(new Integer(viewAssignmentForm.getMobileAssignmentId()));
            request.setAttribute("surveyId", viewAssignmentForm.getMobileAssignmentId());
            request.setAttribute("reportFileName", reportFileName);
            request.setAttribute("form", QA);
        } else if ("Map".equals(action)) {
            String tempLatitude = viewAssignmentForm.getGpsLatitude();
            String tempLongitude = viewAssignmentForm.getGpsLongitude();
            String tempAccuracy = viewAssignmentForm.getAccuracy();
            String tempQuestionGroupId = viewAssignmentForm.getQuestionGroupId();
            String tempQuestionId = viewAssignmentForm.getQuestionId();

            this.getHeader(viewAssignmentForm, mobileAssignmentId);
            this.loadBranchPOIList(request, null);

            viewAssignmentForm.setGpsLatitude(tempLatitude);
            viewAssignmentForm.setGpsLongitude(tempLongitude);
            viewAssignmentForm.setQuestionGroupId(tempQuestionGroupId);
            viewAssignmentForm.setQuestionId(tempQuestionId);
            viewAssignmentForm.setAccuracy(tempAccuracy);
            try {
                this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, BeanUtils.describe(viewAssignmentForm).toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (Global.WEB_TASK_VIEW.equals(action)) {

            this.getHeader(viewAssignmentForm, mobileAssignmentId);
            this.loadJawabanCollection(request, mobileAssignmentId);
            String addr = viewAssignmentForm.getCustomerAddress();
            Map map = new HashMap();
            if (null != viewAssignmentForm.getGpsLatitude() && null != viewAssignmentForm.getGpsLongitude()) {
                map.put("lat", viewAssignmentForm.getGpsLatitude());
                map.put("lng", viewAssignmentForm.getGpsLongitude());
            }
            request.setAttribute("location", map);
            viewAssignmentForm.setCustomerAddressView(addr.trim());
            action = Global.WEB_TASK_LOAD;
        }
        // else if ("PrintPDF".equals(action)){
        // request.setAttribute("param1",
        // viewAssignmentForm.getMobileAssignmentId());
        // request.setAttribute("file",
        // manager.getReportFileForScheme(viewAssignmentForm));
        // manager.printPDF(request,viewAssignmentForm);
        // }
        viewAssignmentForm.setPath(mapping.getPath());

        return this.getNextPage(action, mapping);
    }

    private void getHeader(ViewAssignmentForm viewAssignmentForm,
                           Integer mobileAssignmentId) throws CoreException {
        ViewAssignmentBean bean = null;
        bean = this.manager.getHeader(mobileAssignmentId);
        if (bean != null) {
            BeanConverter.convertBeanToString(bean, viewAssignmentForm, excConverterMap);
        }
    }

    private void loadBranchPOIList(HttpServletRequest request, String branchId) throws CoreException {
        FeatureManager commonManager = new FeatureManager();
        List list = commonManager.getBranchLocation(branchId);
        list = BeanConverter.convertBeansToStringMap(list, excConverterMap);
        request.setAttribute(Global.REQUEST_ATTR_BRANCH_POI, list);

        String img64 = UserImpl.ICON_IMG.toString();
        request.setAttribute(Global.REQUEST_ATTR_IMG_ICON, img64);
    }

    private void loadHistory(HttpServletRequest request,
                             Integer mobileAssignmentId) throws CoreException {
        List list = this.manager.getHistory(mobileAssignmentId);
        if (list != null) {
            list = BeanConverter.convertBeansToString(list, MobileAssignmentHistoryForm.class);
        }
        request.setAttribute("listHistory", list);
    }

    private void loadJawaban(HttpServletRequest request,
                             Integer mobileAssignmentId) throws CoreException {
        PageListWrapper result = this.manager
                .getListAnswers(mobileAssignmentId);
        List listJawaban = result.getResultList();
        if (listJawaban != null) {
            listJawaban = BeanConverter.convertBeansToString(listJawaban, ViewAssignmentForm.class, excConverterMap);
        }
        request.setAttribute("listJawaban", listJawaban);
    }

    private void loadJawabanCollection(HttpServletRequest request,
                                       Integer mobileAssignmentId) throws CoreException {
        PageListWrapper result = this.manager
                .getListAnswersCollection(mobileAssignmentId);
        List listJawaban = result.getResultList();
        if (listJawaban != null) {
            listJawaban = BeanConverter.convertBeansToString(listJawaban, ViewAssignmentForm.class, excConverterMap);
        }
        request.setAttribute("listJawaban", listJawaban);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();

        switch (code) {
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        String task = ((ViewAssignmentForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

    private String getAddressByGpsCoordinates(String address, String lng,
                                              String lat) throws MalformedURLException, IOException,
            org.json.simple.parser.ParseException {
        URL url = new URL("http://maps.google.com/maps/api/geocode/xml?address=" + address + "&sensor=false");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        String formattedAddress = "";

        try {
            InputStream in = url.openStream();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(in));
            String result, line = reader.readLine();
            result = line;
            while ((line = reader.readLine()) != null) {
                result += line;
            }

            JSONParser parser = new JSONParser();
            JSONObject rsp = (JSONObject) parser.parse(result);

            if (rsp.containsKey("results")) {
                JSONArray matches = (JSONArray) rsp.get("results");
                JSONObject data = (JSONObject) matches.get(0);
                formattedAddress = (String) data.get("formatted_address");
            }

            return "";
        } finally {
            urlConnection.disconnect();
            return formattedAddress;
        }
    }

    public Map convertGPSLocation(String address) throws ProtocolException,
            IOException, ParserConfigurationException, SAXException {
        Map param = new HashMap();
        URL url = new URL(
                "http://maps.google.com/maps/api/geocode/xml?address=" + address + "&sensor=false");
        HttpURLConnection urlConnection = (HttpURLConnection) url
                .openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.connect();

        System.out.println("Response Code: " + urlConnection.getResponseCode());
        String ret = "";

        if (urlConnection.getResponseCode() == 200) {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (((HttpURLConnection) urlConnection).getInputStream())));
            String out = "";
            String output = null;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                out += output;
            }
            ret = out;

            DocumentBuilder db = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(ret));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("geometry");

            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);

                NodeList name = element.getElementsByTagName("lat");
                Element line = (Element) name.item(0);
                System.out.println("lat: " + getCharacterDataFromElement(line));
                param.put("lat", getCharacterDataFromElement(line));

                NodeList title = element.getElementsByTagName("lng");
                line = (Element) title.item(0);
                System.out.println("lng: " + getCharacterDataFromElement(line));
                param.put("lng", getCharacterDataFromElement(line));

            }
        }

        return param;
    }
}
