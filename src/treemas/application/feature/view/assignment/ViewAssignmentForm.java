package treemas.application.feature.view.assignment;

import com.google.common.base.Strings;
import treemas.base.view.ViewFormBase;
import treemas.util.constant.AnswerType;

public class ViewAssignmentForm extends ViewFormBase {
    private String mobileAssignmentId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;
    private String customerHP;
    private String rfaDate;
    private String assignmentDate;
    private String retrieveDate;
    private String submitDate;
    private String finalizationDate;
    private String surveyorId;
    private String surveyor;
    private String schemeId;
    private String schemeDescription;
    private String status;
    private String priority;
    private String notes;
    private String branchId;
    private String branchName;

    private String questionGroupId;
    private String questionGroupName;
    private String questionId;
    private String questionLabel;
    private String answerType;
    private String optionAnswerId;
    private String optionAnswerLabel;
    private String textAnswer;
    private String hasImage;
    private String imagePath;
    private String gpsLatitude;// = "106,828783";
    private String gpsLongitude;// = "-6,163110";
    private String isGps;
    private String accuracy;
    private String oldOptionAnswerLabel;
    private String oldTextAnswer;
    private String finalOptionAnswerLabel;
    private String finalTextAnswer;
    private String finalUseImage;

    private String path; //action path
    private String result;
    private String applNo;

    private Double mcc;
    private Double mnc;
    private Double lac;
    private Double cellId;

    private String taskView;

    private String customerAddressView;

    private String schemeRole;
    
    //Irfan 17-12-2018
    
    private String rt;
    private String rw;
    private String kelurahan;
    private String kabupaten;
    private String city;
    private String propinsi;
    private String kodePos;
    private String negara;
    private String kecamatan;
    
    //End

    public String getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(String mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getRfaDate() {
        return rfaDate;
    }

    public void setRfaDate(String rfaDate) {
        this.rfaDate = rfaDate;
    }

    public String getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(String assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public String getRetrieveDate() {
        return retrieveDate;
    }

    public void setRetrieveDate(String retrieveDate) {
        this.retrieveDate = retrieveDate;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getFinalizationDate() {
        return finalizationDate;
    }

    public void setFinalizationDate(String finalizationDate) {
        this.finalizationDate = finalizationDate;
    }

    public String getSurveyor() {
        return surveyor;
    }

    public void setSurveyor(String surveyor) {
        this.surveyor = surveyor;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getOptionAnswerId() {
        return optionAnswerId;
    }

    public void setOptionAnswerId(String optionAnswerId) {
        this.optionAnswerId = optionAnswerId;
    }

    public String getOptionAnswerLabel() {
        return optionAnswerLabel;
    }

    public void setOptionAnswerLabel(String optionAnswerLabel) {
        this.optionAnswerLabel = optionAnswerLabel;
    }

    public String getTextAnswer() {
        return textAnswer;
    }

    public void setTextAnswer(String textAnswer) {
        this.textAnswer = textAnswer;
    }

    public String getHasImage() {
        return hasImage;
    }

    public void setHasImage(String hasImage) {
        this.hasImage = hasImage;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getGpsLatitude() {

        if (Strings.isNullOrEmpty(String.valueOf(this.gpsLatitude))) {
//			this.gpsLatitude = "106.828783";
//			this.gpsLatitude = "0";
        }
        return gpsLatitude;
    }

    public void setGpsLatitude(String gpsLatitude) {
        if (null != gpsLatitude)
            this.gpsLatitude = gpsLatitude.replace(",", ".");
        else
            this.gpsLatitude = gpsLatitude;
    }

    public String getGpsLongitude() {
        if (Strings.isNullOrEmpty(String.valueOf(this.gpsLongitude))) {
//			this.gpsLongitude = "-6.163110";
            this.gpsLongitude = "0";
        }
        return gpsLongitude;
    }

    public void setGpsLongitude(String gpsLongitude) {
        if (null != gpsLongitude)
            this.gpsLongitude = gpsLongitude.replace(",", ".");
        else
            this.gpsLongitude = gpsLongitude;
    }

    public String getIsGps() {
        return isGps;
    }

    public void setIsGps(String isGps) {
        this.isGps = isGps;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getOldOptionAnswerLabel() {
        return oldOptionAnswerLabel;
    }

    public void setOldOptionAnswerLabel(String oldOptionAnswerLabel) {
        this.oldOptionAnswerLabel = oldOptionAnswerLabel;
    }

    public String getOldTextAnswer() {
        return oldTextAnswer;
    }

    public void setOldTextAnswer(String oldTextAnswer) {
        this.oldTextAnswer = oldTextAnswer;
    }

    public String getFinalOptionAnswerLabel() {
        return finalOptionAnswerLabel;
    }

    public void setFinalOptionAnswerLabel(String finalOptionAnswerLabel) {
        this.finalOptionAnswerLabel = finalOptionAnswerLabel;
    }

    public String getFinalTextAnswer() {
        return finalTextAnswer;
    }

    public void setFinalTextAnswer(String finalTextAnswer) {
        this.finalTextAnswer = finalTextAnswer;
    }

    public String getFinalUseImage() {
        return finalUseImage;
    }

    public void setFinalUseImage(String finalUseImage) {
        this.finalUseImage = finalUseImage;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getApplNo() {
        return applNo;
    }

    public void setApplNo(String applNo) {
        this.applNo = applNo;
    }

    public Double getMcc() {
        return mcc;
    }

    public void setMcc(Double mcc) {
        this.mcc = mcc;
    }

    public Double getMnc() {
        return mnc;
    }

    public void setMnc(Double mnc) {
        this.mnc = mnc;
    }

    public Double getLac() {
        return lac;
    }

    public void setLac(Double lac) {
        this.lac = lac;
    }

    public Double getCellId() {
        return cellId;
    }

    public void setCellId(Double cellId) {
        this.cellId = cellId;
    }

    public String getSurveyorId() {
        return surveyorId;
    }

    public void setSurveyorId(String surveyorId) {
        this.surveyorId = surveyorId;
    }

    public String getCustomerHP() {
        return customerHP;
    }

    public void setCustomerHP(String customerHP) {
        this.customerHP = customerHP;
    }

    public String getCustomerAddressView() {
        return customerAddressView;
    }

    public void setCustomerAddressView(String customerAddressView) {
        this.customerAddressView = customerAddressView;
    }

    public String getTaskView() {
        return taskView;
    }

    public void setTaskView(String taskView) {
        this.taskView = taskView;
    }

    public String getIsImage() {
        if (AnswerType.IMAGE.equals(this.answerType) ||
                AnswerType.DRAWING.equals(this.answerType) ||
                AnswerType.SIGNATURE.equals(this.answerType) ||
                AnswerType.IMAGE_WITH_GEODATA.equals(this.answerType) ||
                AnswerType.IMAGE_WITH_GEODATA_GPS.equals(this.answerType) ||
                AnswerType.IMAGE_WITH_TIME.equals(this.answerType) ||
                AnswerType.IMAGE_WITH_GEODATA_TIME.equals(this.answerType) ||
                AnswerType.IMAGE_WITH_GPS_TIME.equals(this.answerType)) {
            return "1";
        } else return "0";
    }

    public String getSchemeRole() {
        return schemeRole;
    }

    public void setSchemeRole(String schemeRole) {
        this.schemeRole = schemeRole;
    }

    //IRFAN
	public String getRt() {
		return rt;
	}

	public void setRt(String rt) {
		this.rt = rt;
	}

	public String getRw() {
		return rw;
	}

	public void setRw(String rw) {
		this.rw = rw;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getKabupaten() {
		return kabupaten;
	}

	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPropinsi() {
		return propinsi;
	}

	public void setPropinsi(String propinsi) {
		this.propinsi = propinsi;
	}

	public String getKodePos() {
		return kodePos;
	}

	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}

	public String getNegara() {
		return negara;
	}

	public void setNegara(String negara) {
		this.negara = negara;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}
	
    
    //END

}
