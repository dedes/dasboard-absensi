package treemas.application.feature.view.assignment;

import treemas.application.feature.FeatureManager;
import treemas.application.feature.task.MobileLocationBean;
import treemas.base.view.ViewManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.geofence.CellIdLookupManager;
import treemas.util.geofence.GeofenceBean;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

public class ViewAssignmentManager extends ViewManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_VIEW_ASSIGNMENT;
    private static final String FALSE = "0";
    private FeatureManager commonManager = new FeatureManager();

    public ViewAssignmentBean getHeader(Integer mobileAssignmentId)
            throws CoreException {

        ViewAssignmentBean bean = null;

        try {
            bean = (ViewAssignmentBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getHeader", mobileAssignmentId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return bean;
    }

    public PageListWrapper getListAnswers(Integer mobileAssignmentId)
            throws CoreException {

        PageListWrapper result = new PageListWrapper();

        boolean saveImageToDatabase = ("1".equals(ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_DATABASE)));

        try {
            List list = null;
            if (saveImageToDatabase) {
                list = this.ibatisSqlMap.queryForList(
                        this.STRING_SQL + "getListAnswersImgDb",
                        mobileAssignmentId);
            } else {
                list = this.ibatisSqlMap.queryForList(
                        this.STRING_SQL + "getListAnswers",
                        mobileAssignmentId);
            }

            for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
                ViewAssignmentBean bean = (ViewAssignmentBean) iterator.next();
                if (FALSE.equals(bean.getIsGps())) {
                    boolean missingCoordinate = (bean.getGpsLatitude() == null && bean
                            .getGpsLongitude() == null) ? true : false;

                    if (missingCoordinate) {
                        bean.setMobileAssignmentId(mobileAssignmentId);
                        this.getMissingCoordinate(bean);
                    }
                }
            }

            result.setResultList(list);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return result;
    }

    private void getMissingCoordinate(ViewAssignmentBean bean)
            throws SQLException {
        MobileLocationBean location = (MobileLocationBean) this.ibatisSqlMap
                .queryForObject(this.STRING_SQL + "getLbs", bean);
        // CoordinateBean coordinateBean = CellIdLookup.getCoordinate(
        // location.getMCC(), location.getMNC(),
        // location.getLAC(), location.getCellId());

        CellIdLookupManager cidManager = new CellIdLookupManager();
        GeofenceBean coordinateBean = cidManager.geocodeByCellId(
                location.getMCC(), location.getMNC(), location.getLAC(),
                location.getCellId());

        if (coordinateBean != null) {
            bean.setGpsLatitude(new Double(coordinateBean.getLatitude()));
            bean.setGpsLongitude(new Double(coordinateBean.getLongitude()));
            bean.setAccuracy(new Integer(coordinateBean.getAccuracy()));
            bean.setProvider(coordinateBean.getProvider());
            this.ibatisSqlMap.update(this.STRING_SQL + "updateCoordinate",
                    bean);
        }
    }

    public List getHistory(Integer mobileAssignmentId) throws CoreException {

        List list = null;

        try {
            list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getHistory", mobileAssignmentId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return list;
    }

    public String getReportFileForScheme(String schemeId) throws CoreException {
        String retFile = null;

        try {
            retFile = (String) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getFileReport", schemeId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return retFile;
    }

    public List getQandA(Integer mobileAssignmentId) throws CoreException {
        List list = null;

        try {
            list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getQandA", mobileAssignmentId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return list;
    }

    public boolean checkIllegalAccess(String userId, String loginBranch, Integer mobileAssignmentId) throws CoreException {
        boolean valid = true;

        if (valid)
            valid = this.commonManager.isInChildLoginBranch(loginBranch,
                    mobileAssignmentId);

        if (valid)
            valid = this.commonManager.isInDistributedTemplate(userId,
                    mobileAssignmentId);

        return valid;
    }

    public boolean checkIllegalAccess(String userId, Integer mobileAssignmentId)
            throws CoreException {
        boolean valid = true;

        // if (valid)
        // valid = this.commonManager.isInChildLoginBranch(loginBranch,
        // mobileAssignmentId);

        if (valid)
            valid = this.commonManager.isInDistributedTemplate(userId,
                    mobileAssignmentId);

        return valid;
    }

    public PageListWrapper getListAnswersCollection(Integer mobileAssignmentId) throws CoreException {
        PageListWrapper result = new PageListWrapper();

        boolean saveImageToDatabase = ("1".equals(ConfigurationProperties
                .getInstance().get(PropertiesKey.IMAGE_DATABASE)));

        try {
            List list = null;
            if (saveImageToDatabase) {
                list = this.ibatisSqlMap.queryForList(
                        this.STRING_SQL + "getListCollectionAnswers",
                        mobileAssignmentId);
            } else {
                list = this.ibatisSqlMap.queryForList(
                        this.STRING_SQL + "getListAnswersCollectionImgDb",
                        mobileAssignmentId);
            }

            for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
                ViewAssignmentBean bean = (ViewAssignmentBean) iterator.next();
                if (FALSE.equals(bean.getIsGps())) {
                    boolean missingCoordinate = (bean.getGpsLatitude() == null && bean
                            .getGpsLongitude() == null) ? true : false;

                    if (missingCoordinate) {
                        bean.setMobileAssignmentId(mobileAssignmentId);
                        this.getMissingCoordinate(bean);
                    }
                }
            }

            result.setResultList(list);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return result;
    }

	/*
     * public void printPDF(HttpServletRequest request, ViewAssignmentForm
	 * viewAssignmentForm) {
	 * 
	 * try { // set engine EngineConfig config = new EngineConfig();
	 * ReportEngine engine = new ReportEngine(config); // set path source report
	 * designer String report = "D:\\birt_report\\default_report.rptdesign";
	 * String pdf = "C:\\ronald_pdf.pdf"; IReportRunnable design =
	 * engine.openReportDesign(report); IRunAndRenderTask taskReport =
	 * engine.createRunAndRenderTask(design); // set parameter in report
	 * designer if available
	 * 
	 * // save report format PDF // set destination path HTMLRenderOption
	 * options_PDF = new HTMLRenderOption(); options_PDF.setOutputFileName(pdf);
	 * // set output format file
	 * options_PDF.setOutputFormat(HTMLRenderOption.OUTPUT_FORMAT_PDF);
	 * //reportContext.getDesignHandle().getMasterPages().get(0).setProperty //
	 * ("orientation","portrait"); options_PDF.setEmbeddable(false);
	 * taskReport.setRenderOption(options_PDF);
	 * 
	 * // run taskReport.run();
	 * 
	 * // save report format HTML // set destination path // HTMLRenderOption
	 * options_HTML = new HTMLRenderOption(); //
	 * options_HTML.setOutputFileName("/reportICOSSME.html"); // //set output
	 * format file // options_HTML.setOutputFormat("html"); //
	 * options_HTML.setEmbeddable(false); //
	 * taskReport.setRenderOption(options_HTML); // //run // taskReport.run();
	 * 
	 * // close taskReport.close(); engine.destroy();
	 * 
	 * // download file // File f = new File(pdf);
	 * 
	 * } catch (Exception ex) { ex.printStackTrace(); } }
	 */
}
