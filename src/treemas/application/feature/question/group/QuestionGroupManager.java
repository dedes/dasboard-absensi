package treemas.application.feature.question.group;

import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.CoreException;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.util.List;


public class QuestionGroupManager extends FeatureManagerBase {
    static final int SEARCH_BY_QUESTION_GROUP_NAME = 10;
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_QUESTION_GROUP;

    public PageListWrapper getAllQuestionGroup(int numberPage,
                                               int srcType, String srcValue, String usage_type) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        if (srcType != SEARCH_BY_QUESTION_GROUP_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: " +
                    srcType);

        SearchFormParameter param = new SearchFormParameter();
        srcValue = srcValue != null ? srcValue.trim() : srcValue;
        param.setPage(numberPage);
        param.setSearchType1(srcType);
        param.setSearchValue1(srcValue);
        param.setSearchValue2(usage_type);
        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", param);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", param);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public QuestionGroupBean getSingleQuestionGroup(Integer questionGroupId)
            throws CoreException {
        QuestionGroupBean result = null;
        try {
            result = (QuestionGroupBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "get", questionGroupId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public void updateQuestionGroup(QuestionGroupBean questionGroupBean,
                                    String loginId, String screenId) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Updating Question Group "
                        + questionGroupBean.getQuestionGroupId());

                this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), questionGroupBean, loginId, screenId);
                this.ibatisSqlMap.update(this.STRING_SQL + "update", questionGroupBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", questionGroupBean);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Updated Question Group "
                        + questionGroupBean.getQuestionGroupId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            throw new CoreException(se, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void insertQuestionGroup(QuestionGroupBean questionGroupBean,
                                    String loginId, String screenId) throws CoreException {
        try {
            try {
                boolean noSequence = "0".equals(ConfigurationProperties.getInstance().
                        get(PropertiesKey.DB_SEQUENCE));

                if (!noSequence) {
                    SequenceManager seqManager = new SequenceManager();
                    questionGroupBean.setQuestionGroupId(
                            new Integer(seqManager.getSequenceQuestionGroup()));
                }

                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Inserting Question Group "
                        + questionGroupBean.getQuestionGroupId());

                this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), questionGroupBean, loginId, screenId);
                this.ibatisSqlMap.insert(this.STRING_SQL + "insert", questionGroupBean);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Inserted Question Group "
                        + questionGroupBean.getQuestionGroupId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void deleteQuestionGroup(Integer questionGroupId, String loginId, String screenId)
            throws CoreException {
        QuestionGroupBean questionGroupBean = new QuestionGroupBean();
        questionGroupBean.setQuestionGroupId(questionGroupId);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Deleting Question Group "
                        + questionGroupId);

                this.auditManager.auditDelete(this.ibatisSqlMap.getCurrentConnection(), questionGroupBean, loginId, screenId);
                this.ibatisSqlMap.delete(this.STRING_SQL + "delete", questionGroupBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", questionGroupBean);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Deleted Question Group "
                        + questionGroupId);
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND) {
                throw new CoreException("", sqle, ERROR_HAS_CHILD, questionGroupId);

            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public QuestionGroupBean getSingleQuestionGroupName(String questionGroupName)
            throws CoreException {
        QuestionGroupBean result = null;
        try {
            result = (QuestionGroupBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getGroupName", questionGroupName);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }
}
