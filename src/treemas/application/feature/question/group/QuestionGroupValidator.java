package treemas.application.feature.question.group;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class QuestionGroupValidator extends Validator {
    public QuestionGroupValidator(HttpServletRequest request) {
        super(request);
    }

    public QuestionGroupValidator(Locale locale) {
        super(locale);
    }

    public boolean validateQuestionGroupAdd(ActionMessages messages, QuestionGroupForm form, QuestionGroupManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        try {
            QuestionGroupBean bean = manager.getSingleQuestionGroupName(form.getQuestionGroupName());
            if (null != bean) {
                messages.add("questionGroupName", new ActionMessage("errors.duplicate", "Question Group Name"));
            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }

        this.textValidator(messages, form.getQuestionGroupName(), "feature.question.group.name", "questionGroupName", "1", this.getStringLength(20), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getUsage_type(), "feature.question.group.type", "usage_Type", "1", this.getStringLength(5), ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_ALLOW_ZERO);

        if (!Strings.isNullOrEmpty(form.getUsage_type())) {
            String usage_type = form.getUsage_type();
            if (Global.GROUP_TOTAL.equalsIgnoreCase(usage_type)) {
                this.textValidator(messages, form.getTotalvalue(), "common.label.max.value", "totalvalue", "1", this.getStringLength(20), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL);
            }
        }

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }

    public boolean validateQuestionGroupEdit(ActionMessages messages, QuestionGroupForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.textValidator(messages, form.getQuestionGroupName(), "feature.question.group.name", "questionGroupName", "1", this.getStringLength(20), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getUsage_type(), "feature.question.group.type", "usage_Type", "1", this.getStringLength(5), ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_ALLOW_ZERO);

        if (!Strings.isNullOrEmpty(form.getUsage_type())) {
            String usage_type = form.getUsage_type();
            if (Global.GROUP_TOTAL.equalsIgnoreCase(usage_type)) {
                this.textValidator(messages, form.getTotalvalue(), "common.label.max.value", "totalvalue", "1", this.getStringLength(20), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL);
            }
        }

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
