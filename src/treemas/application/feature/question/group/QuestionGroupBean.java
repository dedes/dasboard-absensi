package treemas.application.feature.question.group;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;

public class QuestionGroupBean implements Serializable, Auditable {
    private static final String[] AUDIT_COLUMNS = new String[]{
            "questionGroupId", "questionGroupName", "active", "usage_type", "totalvalue"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{
            "QUESTION_GROUP_ID", "QUESTION_GROUP_NAME", "ACTIVE", "USAGE_TYPE", "TOTALVALUE"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_QUESTION_GROUP;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"questionGroupId"};

    private Integer questionGroupId;
    private String questionGroupName;
    private String active;
    private String usage_type;
    private Double totalvalue;

    /*TODO
     * COMMIT THIS
     * */
    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getUsage_type() {
        return usage_type;
    }

    public void setUsage_type(String usage_type) {
        this.usage_type = usage_type;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public Double getTotalvalue() {
        return totalvalue;
    }

    public void setTotalvalue(Double totalvalue) {
        this.totalvalue = totalvalue;
    }

}
