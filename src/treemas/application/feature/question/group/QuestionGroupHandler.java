package treemas.application.feature.question.group;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.general.ComboManager;
import treemas.application.general.sync.SyncDataExecutor;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class QuestionGroupHandler extends FeatureActionBase {
    static final String WEB_TASK_SYNC = "Sync";
    private static final String[] SAVE_PARAMETERS = {"paging.currentPageNo",
            "paging.totalRecord", "paging.dispatch", "srcValue", "srcType"};
    private final String screenId = "";
    private QuestionGroupManager manager = new QuestionGroupManager();
    private QuestionGroupValidator qValidator;
    private ComboManager comboManager = new ComboManager();

    public QuestionGroupHandler() {
        this.pageMap.put(WEB_TASK_SYNC, "view");
        this.exceptionMap.put(WEB_TASK_SYNC, "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.qValidator = new QuestionGroupValidator(request);
        QuestionGroupForm questionGroupForm = (QuestionGroupForm) form;
        questionGroupForm.getPaging().calculationPage();
        String action = questionGroupForm.getTask();
        /*TODO
		 * COMMIT THIS
		 * */
        this.loadComboUsageType(request);
        if (Global.WEB_TASK_LOAD.equals(action)
                || Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadList(request, questionGroupForm);
            questionGroupForm.fillCurrentUri(request, SAVE_PARAMETERS);
        } else if (Global.WEB_TASK_ADD.equals(action)) {
            questionGroupForm.setQuestionGroupName("");
            questionGroupForm.setActive("1");
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.qValidator.validateQuestionGroupAdd(new ActionMessages(), questionGroupForm, this.manager)) {
                QuestionGroupBean questionGroupBean = new QuestionGroupBean();
                questionGroupForm.toBean(questionGroupBean);
                this.manager.insertQuestionGroup(questionGroupBean,
                        this.getLoginId(request), this.screenId);
                this.loadList(request, questionGroupForm);
                request.setAttribute("message", resources.getMessage("common.process.add"));
            }
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
            QuestionGroupBean questionGroupBean = this.manager
                    .getSingleQuestionGroup(new Integer(questionGroupForm
                            .getQuestionGroupId()));
            questionGroupForm.fromBean(questionGroupBean);
            questionGroupForm.getPaging().setDispatch(null);
        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
            if (this.qValidator.validateQuestionGroupEdit(new ActionMessages(), questionGroupForm)) {
                QuestionGroupBean questionGroupBean = new QuestionGroupBean();
                questionGroupForm.toBean(questionGroupBean);
                this.manager.updateQuestionGroup(questionGroupBean,
                        this.getLoginId(request), this.screenId);
                this.loadList(request, questionGroupForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
            }
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = questionGroupForm.getPaging();
            try {
                this.manager.deleteQuestionGroup(
                        new Integer(questionGroupForm.getQuestionGroupId()),
                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.loadList(request, questionGroupForm);
                page.setDispatch(null);
            }
        } else if (WEB_TASK_SYNC.equals(action)) {
            SyncDataExecutor sync = new SyncDataExecutor();
            sync.updOptionAnswers();
            this.setMessage(request, "common.sync.success", null);
            this.loadList(request, questionGroupForm);
        }
        return this.getNextPage(action, mapping);
    }

    private void loadList(HttpServletRequest request,
                          QuestionGroupForm questionGroupForm) throws CoreException {
        int targetPageNo = questionGroupForm.getPaging().getTargetPageNo();
        int searchType = Integer.parseInt(questionGroupForm.getSrcType());
        if (Global.WEB_TASK_SEARCH.equals(questionGroupForm.getTask())) {
            targetPageNo = 1;
        }
        PageListWrapper result = this.manager.getAllQuestionGroup(targetPageNo,
                searchType, questionGroupForm.getSrcValue(), questionGroupForm.getSrcValue2());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, QuestionGroupForm.class);
        questionGroupForm.getPaging().setCurrentPageNo(targetPageNo);
        questionGroupForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listQuestionGroup", list);
    }

    private void loadComboUsageType(HttpServletRequest request) throws CoreException {
//		List list = this.comboManager.getComboUsageType();
//		request.setAttribute("listUsageType", list);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case QuestionGroupManager.ERROR_HAS_CHILD:
                this.setMessage(request, "common.haschild", null);
                break;

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((QuestionGroupForm) form).setTask(((QuestionGroupForm) form)
                .resolvePreviousTask());
        String task = ((QuestionGroupForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
