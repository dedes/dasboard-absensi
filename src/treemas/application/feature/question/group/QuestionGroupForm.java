package treemas.application.feature.question.group;

import treemas.base.feature.FeatureFormBase;

public class QuestionGroupForm extends FeatureFormBase {
    /*TODO
     * COMMIT THIS
     * */
    private String questionGroupId;
    private String questionGroupName;
    private String active;
    private String usage_type;
    private String totalvalue;
    private String srcType = "10";
    private String srcValue;
    private String srcValue2;

    public QuestionGroupForm() {
        active = "0";
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = (questionGroupName == null) ? null : questionGroupName.trim();
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSrcType() {
        return srcType;
    }

    public void setSrcType(String srcType) {
        this.srcType = srcType;
    }

    public String getSrcValue() {
        return srcValue;
    }

    public void setSrcValue(String srcValue) {
        this.srcValue = (srcValue == null) ? null : srcValue.trim();
    }

    public String getUsage_type() {
        return usage_type;
    }

    public void setUsage_type(String usage_type) {
        this.usage_type = usage_type;
    }

    public String getTotalvalue() {
        return totalvalue;
    }

    public void setTotalvalue(String totalvalue) {
        this.totalvalue = totalvalue;
    }

    public String getSrcValue2() {
        return srcValue2;
    }

    public void setSrcValue2(String srcValue2) {
        this.srcValue2 = srcValue2;
    }

}
