package treemas.application.feature.question;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;

public class OptionAnswerBean implements Serializable, Auditable {
    private static final String[] AUDIT_COLUMNS = new String[]{
            "questionGroupId", "questionId", "optionAnswerId",
            "optionAnswerLabel", "isActive", "lineSeqOrder", "legacyCode"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{
            "QUESTION_GROUP_ID", "QUESTION_ID", "OPTION_ANSWER_ID",
            "OPTION_ANSWER_LABEL", "ACTIVE", "LINE_SEQ_ORDER", "LEGACY_CODE"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_QUESTION_OPTIONS;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{
            "questionGroupId", "questionId", "optionAnswerId"};

    private Integer questionGroupId;
    private Integer questionId;
    private Integer optionAnswerId;
    private String optionAnswerLabel;
    private String isActive;
    private Integer lineSeqOrder;
    private String legacyCode;
    private String branch_id;
    

    public String getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}

	public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public Integer getOptionAnswerId() {
        return optionAnswerId;
    }

    public void setOptionAnswerId(Integer optionAnswerId) {
        this.optionAnswerId = optionAnswerId;
    }

    public String getOptionAnswerLabel() {
        return optionAnswerLabel;
    }

    public void setOptionAnswerLabel(String optionAnswerLabel) {
        this.optionAnswerLabel = optionAnswerLabel;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public Integer getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(Integer lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

    public String getLegacyCode() {
        return legacyCode;
    }

    public void setLegacyCode(String legacyCode) {
        this.legacyCode = legacyCode;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }
}
