package treemas.application.feature.question.question;

import treemas.base.feature.FeatureFormBase;

public class QuestionForm extends FeatureFormBase {
    /*TODO
     * COMMIT THIS*/
    private static final long serialVersionUID = 1L;
    private String questionGroupId;
    private String questionGroupName;
    private String questionId;
    private String questionLabel;
    private String answerType;
    private String answerTypeName;
    private String isActive;
    private String isMandatory;
    private String lineSeqOrder;
    private String paramName;
    private String lookupCode;
    private String maxLength;
    private String relQuestionGroupId;
    private String relQuestionId;
    private String relQuestionLabel;
    private String relAnswerTypeId;
    private String relOptionAnswerId;
    private String relTextAnswer;
    private String isVisible;
    private String regexPattern;
    private String isReadonly;
    private String optionAnswerId;
    private String optionAnswerLabel;
    private String legacyCode;
    private String usage_type;
    private String minval;
    private String maxval;
    private String use_min;
    private String min_choose;
    private String searchType = "10";
    private String searchValue;
    private String isHaveAnswer;
    
    private String branch_id;
    

    public String getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}

	public QuestionForm() {
        isActive = "0";
        isMandatory = "0";
        isVisible = "0";
        isReadonly = "0";
        use_min = "0";
        minval = "0";
        maxval = "0";
        min_choose = "0";
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = (questionLabel == null) ? null : questionLabel
                .trim();
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getAnswerTypeName() {
        return answerTypeName;
    }

    public void setAnswerTypeName(String answerTypeName) {
        this.answerTypeName = answerTypeName;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(String lineSeqOrder) {
        this.lineSeqOrder = (lineSeqOrder == null) ? null : lineSeqOrder.trim();
    }

    public String getOptionAnswerId() {
        return optionAnswerId;
    }

    public void setOptionAnswerId(String optionAnswerId) {
        this.optionAnswerId = optionAnswerId;
    }

    public String getOptionAnswerLabel() {
        return optionAnswerLabel;
    }

    public void setOptionAnswerLabel(String optionAnswerLabel) {
        this.optionAnswerLabel = optionAnswerLabel;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = (searchValue == null) ? null : searchValue.trim();
    }

    public void prepQuestionFormForAddQuestion() {
        this.setQuestionLabel("");
        this.setAnswerType("");
        this.setIsActive("1");
        this.setIsMandatory("0");
        this.setRelQuestionGroupId("");
        this.setRelQuestionId("");
        this.setRelQuestionLabel("");
        this.setRelOptionAnswerId("");
        this.setRelTextAnswer("");
        this.setIsReadonly("0");
        this.setIsVisible("1");
        this.setRegexPattern("");
        this.setUse_min("0");
    }

    public void prepQuestionFormForAddAnswer() {
        this.setOptionAnswerLabel("");
        this.setIsActive("1");
    }

    public String getIsHaveAnswer() {
        return isHaveAnswer;
    }

    public void setIsHaveAnswer(String isHaveAnswer) {
        this.isHaveAnswer = isHaveAnswer;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getLookupCode() {
        return lookupCode;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }

    public String getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }

    public String getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(String isVisible) {
        this.isVisible = isVisible;
    }

    public String getRegexPattern() {
        return regexPattern;
    }

    public void setRegexPattern(String regexPattern) {
        this.regexPattern = regexPattern;
    }

    public String getIsReadonly() {
        return isReadonly;
    }

    public void setIsReadonly(String isReadonly) {
        this.isReadonly = isReadonly;
    }

    public String getRelQuestionGroupId() {
        return relQuestionGroupId;
    }

    public void setRelQuestionGroupId(String relQuestionGroupId) {
        this.relQuestionGroupId = relQuestionGroupId;
    }

    public String getRelQuestionId() {
        return relQuestionId;
    }

    public void setRelQuestionId(String relQuestionId) {
        this.relQuestionId = relQuestionId;
    }

    public String getRelQuestionLabel() {
        return relQuestionLabel;
    }

    public void setRelQuestionLabel(String relQuestionLabel) {
        this.relQuestionLabel = relQuestionLabel;
    }

    public String getRelAnswerTypeId() {
        return relAnswerTypeId;
    }

    public void setRelAnswerTypeId(String relAnswerTypeId) {
        this.relAnswerTypeId = relAnswerTypeId;
    }

    public String getRelOptionAnswerId() {
        return relOptionAnswerId;
    }

    public void setRelOptionAnswerId(String relOptionAnswerId) {
        this.relOptionAnswerId = relOptionAnswerId;
    }

    public String getRelTextAnswer() {
        return relTextAnswer;
    }

    public void setRelTextAnswer(String relTextAnswer) {
        this.relTextAnswer = relTextAnswer;
    }

    /**
     * @return the legacyCode
     */
    public String getLegacyCode() {
        return legacyCode;
    }

    /**
     * @param legacyCode the legacyCode to set
     */
    public void setLegacyCode(String legacyCode) {
        this.legacyCode = legacyCode;
    }

    public String getMinval() {
        return minval;
    }

    public void setMinval(String minval) {
        this.minval = minval;
    }

    public String getMaxval() {
        return maxval;
    }

    public void setMaxval(String maxval) {
        this.maxval = maxval;
    }

    public String getUse_min() {
        return use_min;
    }

    public void setUse_min(String use_min) {
        this.use_min = use_min;
    }

    public String getMin_choose() {
        return min_choose;
    }

    public void setMin_choose(String min_choose) {
        this.min_choose = min_choose;
    }

    public String getUsage_type() {
        return usage_type;
    }

    public void setUsage_type(String usage_type) {
        this.usage_type = usage_type;
    }
}
