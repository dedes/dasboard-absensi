package treemas.application.feature.question.question;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.AnswerType;
import treemas.util.constant.Global;

import java.io.Serializable;

public class QuestionBean implements Serializable, Auditable {
    private static final String[] AUDIT_COLUMNS = new String[]{
            "questionGroupId", "questionId", "questionLabel", "answerType",
            "isActive", "isMandatory", "lineSeqOrder", "paramName",
            "lookupCode", "maxLength", "isVisible", "regexPattern",
            "minval", "maxval", "use_min", "min_choose", "isReadonly"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{
            "QUESTION_GROUP_ID", "QUESTION_ID", "QUESTION_LABEL",
            "ANSWER_TYPE", "IS_ACTIVE", "IS_MANDATORY", "LINE_SEQ_ORDER",
            "PARAM_NAME", "LOOKUP_CODE", "MAX_LENGTH", "IS_VISIBLE",
            "REGEX_PATTERN", "MINVAL", "MAXVAL", "USE_MIN", "MIN_CHOOSE", "IS_READONLY"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_QUESTION;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"questionGroupId", "questionId"};
    private Integer questionGroupId;
    private Integer questionId;
    private String questionLabel;
    private String answerType;
    private String answerTypeName;
    private String isActive;
    private String isMandatory;
    private Integer lineSeqOrder;
    private String paramName;
    private String lookupCode;
    private Integer maxLength;
    private Integer relQuestionGroupId;
    private Integer relQuestionId;
    private String relQuestionLabel;
    private String relAnswerTypeId;
    private Integer relOptionAnswerId;
    private String relTextAnswer;
    private String isVisible;
    private String regexPattern;
    private String isReadonly;
    /*TODO
     * COMMIT THIS*/
    private Double minval;
    private Double maxval;
    private String use_min;
    private Integer min_choose;
    // private Integer optionAnswerId;
    // private String optionAnswerLabel;
    private String branch_id;
    
    

    public String getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}

	public static String[] getAuditDbColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getAnswerTypeName() {
        return answerTypeName;
    }

    public void setAnswerTypeName(String answerTypeName) {
        this.answerTypeName = answerTypeName;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public Integer getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(Integer lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getLookupCode() {
        return lookupCode;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }

    public String getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(String isVisible) {
        this.isVisible = isVisible;
    }

    public String getRegexPattern() {
        return regexPattern;
    }

    public void setRegexPattern(String regexPattern) {
        this.regexPattern = regexPattern;
    }

    public String getIsReadonly() {
        return isReadonly;
    }

    public void setIsReadonly(String isReadonly) {
        this.isReadonly = isReadonly;
    }

    public Integer getRelQuestionGroupId() {
        return relQuestionGroupId;
    }

    public void setRelQuestionGroupId(Integer relQuestionGroupId) {
        this.relQuestionGroupId = relQuestionGroupId;
    }

    public Integer getRelQuestionId() {
        return relQuestionId;
    }

    public void setRelQuestionId(Integer relQuestionId) {
        this.relQuestionId = relQuestionId;
    }

    public String getRelQuestionLabel() {
        return relQuestionLabel;
    }

    public void setRelQuestionLabel(String relQuestionLabel) {
        this.relQuestionLabel = relQuestionLabel;
    }

    public String getRelAnswerTypeId() {
        return relAnswerTypeId;
    }

    public void setRelAnswerTypeId(String relAnswerTypeId) {
        this.relAnswerTypeId = relAnswerTypeId;
    }

    public Integer getRelOptionAnswerId() {
        return relOptionAnswerId;
    }

    public void setRelOptionAnswerId(Integer relOptionAnswerId) {
        this.relOptionAnswerId = relOptionAnswerId;
    }

    public String getRelTextAnswer() {
        return relTextAnswer;
    }

    public void setRelTextAnswer(String relTextAnswer) {
        this.relTextAnswer = relTextAnswer;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public String getIsMultiple() {
        if (AnswerType.MULTIPLE.equals(answerType)
                || AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(answerType)
                || AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(answerType)
                || AnswerType.MULTIPLE_LOOKUP_WITH_FILTER.equals(answerType)) {
            return "1";
        } else {
            return "0";
        }
    }

    public String getIsHaveAnswer() {
        if (AnswerType.MULTIPLE.equals(answerType)
                || AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(answerType)
                || AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(answerType)
                || AnswerType.MULTIPLE_LOOKUP_WITH_FILTER.equals(answerType)
                || AnswerType.RADIO.equals(answerType)
                || AnswerType.RADIO_WITH_DESCRIPTION.equals(answerType)
                || AnswerType.DROPDOWN.equals(answerType)
                || AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(answerType)) {
            return "1";
        } else
            return "0";
    }

    public Double getMinval() {
        return minval;
    }

    public void setMinval(Double minval) {
        this.minval = minval;
    }

    public Double getMaxval() {
        return maxval;
    }

    public void setMaxval(Double maxval) {
        this.maxval = maxval;
    }

    public String getUse_min() {
        return use_min;
    }

    public void setUse_min(String use_min) {
        this.use_min = use_min;
    }

    public Integer getMin_choose() {
        return min_choose;
    }

    public void setMin_choose(Integer min_choose) {
        this.min_choose = min_choose;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }
}
