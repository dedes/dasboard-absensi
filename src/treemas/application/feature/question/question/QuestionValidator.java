package treemas.application.feature.question.question;

import org.apache.struts.action.ActionMessages;
import treemas.util.CoreException;
import treemas.util.constant.AnswerType;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/*TODO
 * COMMIT THIS*/
public class QuestionValidator extends Validator {
    public QuestionValidator(HttpServletRequest request) {
        super(request);
    }

    public QuestionValidator(Locale locale) {
        super(locale);
    }

    public boolean validateQuestionAdd(ActionMessages messages, QuestionForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

//		questionLabel
        this.textValidator(messages, form.getQuestionLabel(), "feature.question.label", "questionLabel", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
//		answerType combo
        this.comboValidator(messages, form.getAnswerType(), "1", this.getStringLength(3), "feature.question.type", "answerType", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
//		lineSeqOrder
        this.textValidator(messages, form.getLineSeqOrder(), "feature.question.sequence.order", "lineSeqOrder", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
//		maxLength
        if (AnswerType.TEXT.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.NUMERIC.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.DECIMAL.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.NUMERIC_WITH_CALCULATION.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.ACCOUNTING.equalsIgnoreCase(form.getAnswerType())) {
            this.textValidator(messages, form.getMaxLength(), "feature.question.max.length", "maxLength", "1", this.getStringLength(4), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        } else if (AnswerType.TRESHOLD.equalsIgnoreCase(form.getAnswerType())) {
            this.textValidator(messages, form.getMaxLength(), "feature.question.max.length", "maxLength", "1", this.getStringLength(4), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
            this.textValidator(messages, form.getMinval(), "feature.question.min.value", "minval", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL_ALLOW_ZERO);
            this.textValidator(messages, form.getMaxval(), "feature.question.max.value", "maxval", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL);
            if (null != messages && messages.isEmpty()) {
                this.rangeValidatorMinMax(messages, "feature.question.max.value", "maxval", form.getMinval(), form.getMaxval(), ValidatorGlobal.TM_OPERATOR_KURANG_DARI);
            }
        } else if ((AnswerType.MULTIPLE.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.MULTIPLE_LOOKUP_WITH_FILTER.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.MULTIPLE_ONE_DESCRIPTION.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.MULTIPLE_WITH_DESCRIPTION.equalsIgnoreCase(form.getAnswerType()))
                && Global.STRING_TRUE.equalsIgnoreCase(form.getUse_min())) {
            this.textValidator(messages, form.getMin_choose(), "feature.question.min.choose", "min_choose", this.getStringLength(1), this.getStringLength(5), ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        }

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }

    public boolean validateQuestionRank(ActionMessages messages, QuestionForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.textValidator(messages, form.getQuestionLabel(), "feature.question.label", "questionLabel", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.comboValidator(messages, form.getAnswerType(), "1", this.getStringLength(3), "feature.question.type", "answerType", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getLineSeqOrder(), "feature.question.sequence.order", "lineSeqOrder", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }

    public boolean validateQuestionEdit(ActionMessages messages, QuestionForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

//		questionLabel
        this.textValidator(messages, form.getQuestionLabel(), "feature.question.label", "questionLabel", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
//		answerType combo
        this.comboValidator(messages, form.getAnswerType(), "1", this.getStringLength(3), "feature.question.type", "answerType", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
//		lineSeqOrder
        this.textValidator(messages, form.getLineSeqOrder(), "feature.question.sequence.order", "lineSeqOrder", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
//		maxLength
        if (AnswerType.TEXT.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.NUMERIC.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.DECIMAL.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.NUMERIC_WITH_CALCULATION.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.ACCOUNTING.equalsIgnoreCase(form.getAnswerType())) {
            this.textValidator(messages, form.getMaxLength(), "feature.question.max.length", "maxLength", "1", this.getStringLength(4), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        } else if (AnswerType.TRESHOLD.equalsIgnoreCase(form.getAnswerType())) {
            this.textValidator(messages, form.getMaxLength(), "feature.question.max.length", "maxLength", "1", this.getStringLength(4), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
            this.textValidator(messages, form.getMinval(), "feature.question.min.value", "minval", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL_ALLOW_ZERO);
            this.textValidator(messages, form.getMaxval(), "feature.question.max.value", "maxval", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL);
            if (null != messages && messages.isEmpty()) {
                this.rangeValidatorMinMax(messages, "feature.question.max.value", "maxval", form.getMinval(), form.getMaxval(), ValidatorGlobal.TM_OPERATOR_KURANG_DARI);
            }
        } else if ((AnswerType.MULTIPLE.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.MULTIPLE_LOOKUP_WITH_FILTER.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.MULTIPLE_ONE_DESCRIPTION.equalsIgnoreCase(form.getAnswerType())
                || AnswerType.MULTIPLE_WITH_DESCRIPTION.equalsIgnoreCase(form.getAnswerType()))
                && Global.STRING_TRUE.equalsIgnoreCase(form.getUse_min())) {
            this.textValidator(messages, form.getMin_choose(), "feature.question.min.choose", "min_choose", this.getStringLength(1), this.getStringLength(5), ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        }

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }

    public boolean validateAnswerAdd(ActionMessages messages, QuestionForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.textValidator(messages, form.getOptionAnswerLabel(), "common.label", "optionAnswerLabel", "1", this.getStringLength(30), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getLineSeqOrder(), "feature.sequence.order", "lineSeqOrder", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
 //       this.textValidator(messages, form.getLegacyCode(), "feature.question.options.legacy", "legacyCode", "1", this.getStringLength(15), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }

    public boolean validateAnswerEdit(ActionMessages messages, QuestionForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.textValidator(messages, form.getOptionAnswerLabel(), "common.label", "optionAnswerLabel", "1", this.getStringLength(30), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getLineSeqOrder(), "feature.sequence.order", "lineSeqOrder", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
  //      this.textValidator(messages, form.getLegacyCode(), "feature.question.options.legacy", "legacyCode", "1", this.getStringLength(15), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
