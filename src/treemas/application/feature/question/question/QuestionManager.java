package treemas.application.feature.question.question;

import treemas.application.feature.question.OptionAnswerBean;
import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.CoreException;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class QuestionManager extends FeatureManagerBase {
    static final int SEARCH_BY_QUESTION_OR_ANSWER_LABEL = 10;
    /*TODO
     * COMMIT THIS IBATIS*/
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_QUESTION;

    public PageListWrapper getAllQuestion(int numberPage,
                                          int searchType, String searchValue,
                                          Integer questionGroupId) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        if (searchType != SEARCH_BY_QUESTION_OR_ANSWER_LABEL)
            throw new IllegalArgumentException("Invalid searchType parameter: " +
                    searchType);

        SearchFormParameter param = new SearchFormParameter();
        searchValue = searchValue != null ? searchValue.trim() : searchValue;
        param.setPage(numberPage);
        param.setSearchType1(searchType);
        param.setSearchValue1(searchValue);
        Map paramMap = param.toMap();
        paramMap.put("questionGroupId", questionGroupId);
        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", paramMap);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", paramMap);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public PageListWrapper getAllOptionAnswer(int numberPage,
                                              int searchType, String searchValue,
                                              Integer questionGroupId, Integer questionId,
                                              String userid) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        if (searchType != SEARCH_BY_QUESTION_OR_ANSWER_LABEL)
            throw new IllegalArgumentException("Invalid searchType parameter: " +
                    searchType);

        SearchFormParameter param = new SearchFormParameter();
        QuestionForm form = new QuestionForm();
        searchValue = searchValue != null ? searchValue.trim() : searchValue;
        param.setPage(numberPage);
        param.setSearchType1(searchType);
        param.setSearchValue1(searchValue);
        
/*      sysuser bean =  this.ibatisSqlMap.queryForList( this.STRING_SQL + "getsingle", userid);
*/        
        Map paramMap = param.toMap();
        paramMap.put("questionGroupId", questionGroupId);
        paramMap.put("questionId", questionId);
        paramMap.put("userid", userid);
        
        try {     		
        		List list = this.ibatisSqlMap.queryForList(
                        this.STRING_SQL + "getAllOptionAnswer", paramMap);
                result.setResultList(list);

                Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                        this.STRING_SQL + "cntAllOptionAnswer", paramMap);
                result.setTotalRecord(tmp.intValue());
        		        
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }
    
    //IRFAN
    
/*    	public PageListWrapper getAllOptionAnswerBranch(int numberPage,
            int searchType, String searchValue,
            Integer questionGroupId, Integer questionId,
            String userid) throws CoreException {
			PageListWrapper result = new PageListWrapper();
			if (searchType != SEARCH_BY_QUESTION_OR_ANSWER_LABEL)throw new IllegalArgumentException("Invalid searchType parameter: " + searchType);

				SearchFormParameter param = new SearchFormParameter();
				searchValue = searchValue != null ? searchValue.trim() : searchValue;
				param.setPage(numberPage);
				param.setSearchType1(searchType);
				param.setSearchValue1(searchValue);
   
				Map paramMap = param.toMap();
				paramMap.put("questionGroupId", questionGroupId);
				paramMap.put("questionId", questionId);
				paramMap.put("userid", userid);

				try {

					List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllOptionAnswerBranch", paramMap);
					result.setResultList(list);

					Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "cntAllOptionAnswerBranch", paramMap);
					result.setTotalRecord(tmp.intValue());

				} catch (SQLException sqle) {
					this.logger.printStackTrace(sqle);
					throw new CoreException(sqle, ERROR_DB);
				}
				return result;
			}*/
  //END IRFAN
 
    public QuestionBean getSingleQuestion(Integer questionGroupId, Integer questionId)
            throws CoreException {
        QuestionBean result = null;
        try {
            QuestionBean param = new QuestionBean();
            param.setQuestionGroupId(questionGroupId);
            param.setQuestionId(questionId);
            result = (QuestionBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "get", param);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public OptionAnswerBean getSingleOptionAnswer(Integer questionGroupId,
                                                  Integer questionId, Integer optionAnswerId) throws CoreException {
        OptionAnswerBean result = null;
        try {
            OptionAnswerBean param = new OptionAnswerBean();
            param.setQuestionGroupId(questionGroupId);
            param.setQuestionId(questionId);
            param.setOptionAnswerId(optionAnswerId);
            result = (OptionAnswerBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getOptionAnswer", param);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public void updateQuestion(QuestionBean questionBean,
                               String loginId, String screenId) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Updating Question "
                        + questionBean.getQuestionId() + " / Question Group Id "
                        + questionBean.getQuestionGroupId());

                this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), questionBean, loginId, screenId);
                this.ibatisSqlMap.update(this.STRING_SQL + "update", questionBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", questionBean.getQuestionGroupId());
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Updated Question "
                        + questionBean.getQuestionId() + " / Question Group Id "
                        + questionBean.getQuestionGroupId());
                this.logger.log(ILogger.LEVEL_INFO, "Updated Scheme Last Update for Question Group Id: " +
                        questionBean.getQuestionGroupId() + "/ to: " + new java.util.Date().toString());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            if (se.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new CoreException("", se, ERROR_DATA_EXISTS, questionBean
                        .getParamName());
            } else {
                throw new CoreException(se.toString(), se, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void updateOptionAnswer(OptionAnswerBean optionAnswerBean,
                                   String loginId, String screenId) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Updating Option Answer "
                        + optionAnswerBean.getOptionAnswerId() + " / Question Group Id "
                        + optionAnswerBean.getQuestionGroupId() + " / Question Id "
                        + optionAnswerBean.getQuestionId());

                this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), optionAnswerBean, loginId, screenId);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateOptionAnswer", optionAnswerBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", optionAnswerBean.getQuestionGroupId());
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Updated Option Answer "
                        + optionAnswerBean.getOptionAnswerId() + " / Question Group Id "
                        + optionAnswerBean.getQuestionGroupId() + " / Question Id "
                        + optionAnswerBean.getQuestionId());

                this.logger.log(ILogger.LEVEL_INFO, "Updated Scheme Last Update for Question Group Id: " +
                        optionAnswerBean.getQuestionGroupId() + "/ to: " + new java.util.Date().toString());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            throw new CoreException(se, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void insertQuestion(QuestionBean questionBean,
                               String loginId, String screenId) throws CoreException {
        try {
            try {
                boolean noSequence = "0".equals(ConfigurationProperties.getInstance().
                        get(PropertiesKey.DB_SEQUENCE));

                if (!noSequence) {
                    SequenceManager seqManager = new SequenceManager();
                    questionBean.setQuestionId(
                            new Integer(seqManager.getSequenceQuestion()));
                }

                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Inserting Question "
                        + questionBean.getQuestionId() + " / Question Group Id "
                        + questionBean.getQuestionGroupId());

                this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), questionBean, loginId, screenId);
                this.ibatisSqlMap.insert(this.STRING_SQL + "insert", questionBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", questionBean.getQuestionGroupId());
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Inserted Question "
                        + questionBean.getQuestionId() + " / Question Group Id "
                        + questionBean.getQuestionGroupId());
                this.logger.log(ILogger.LEVEL_INFO, "Updated Scheme Last Update for Question Group Id: " +
                        questionBean.getQuestionGroupId() + "/ to: " + new java.util.Date().toString());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new CoreException("", sqle, ERROR_DATA_EXISTS, questionBean
                        .getParamName());
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }


    public void insertRankQuestion(QuestionBean questionBean, String loginId, String screenId) throws CoreException {
        try {
            try {
                boolean noSequence = "0".equals(ConfigurationProperties.getInstance().get(PropertiesKey.DB_SEQUENCE));

                if (!noSequence) {
                    SequenceManager seqManager = new SequenceManager();
                    questionBean.setQuestionId(
                            new Integer(seqManager.getSequenceQuestion()));
                }
                HashMap param_question_answer = new HashMap();
                param_question_answer.put("question_group_id", questionBean.getQuestionGroupId());
                param_question_answer.put("question_label", questionBean.getQuestionLabel());
                param_question_answer.put("answer_type", questionBean.getAnswerType());
                param_question_answer.put("line_seq_order", questionBean.getLineSeqOrder());

                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Inserting Question "
                        + questionBean.getQuestionId() + " / Question Group Id "
                        + questionBean.getQuestionGroupId());

                this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), questionBean, loginId, screenId);
                this.ibatisSqlMap.update(this.STRING_SQL + "addQuestionRank", param_question_answer);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Inserted Question "
                        + questionBean.getQuestionId() + " / Question Group Id "
                        + questionBean.getQuestionGroupId());
                this.logger.log(ILogger.LEVEL_INFO, "Updated Scheme Last Update for Question Group Id: " +
                        questionBean.getQuestionGroupId() + "/ to: " + new java.util.Date().toString());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new CoreException("", sqle, ERROR_DATA_EXISTS, questionBean
                        .getParamName());
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }


    public void insertOptionAnswer(OptionAnswerBean optionAnswerBean,
                                   String loginId, String screenId) throws CoreException {
        try {
            try {
                boolean noSequence = "0".equals(ConfigurationProperties.getInstance().
                        get(PropertiesKey.DB_SEQUENCE));

                if (!noSequence) {
                    SequenceManager seqManager = new SequenceManager();
                    optionAnswerBean.setOptionAnswerId(
                            new Integer(seqManager.getSequenceOptionAnswer()));
                }

                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Inserting Option Answer "
                        + optionAnswerBean.getOptionAnswerId() + " / Question Group Id "
                        + optionAnswerBean.getQuestionGroupId() + " / Question Id "
                        + optionAnswerBean.getQuestionId());

                this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), optionAnswerBean, loginId, screenId);
                this.ibatisSqlMap.insert(this.STRING_SQL + "insertOptionAnswer", optionAnswerBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", optionAnswerBean.getQuestionGroupId());
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Inserted Option Answer "
                        + optionAnswerBean.getOptionAnswerId() + " / Question Group Id "
                        + optionAnswerBean.getQuestionGroupId() + " / Question Id "
                        + optionAnswerBean.getQuestionId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void deleteQuestion(Integer questionGroupId, Integer questionId,
                               String loginId, String screenId)
            throws CoreException {
        QuestionBean questionBean = new QuestionBean();
        questionBean.setQuestionGroupId(questionGroupId);
        questionBean.setQuestionId(questionId);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Deleting Question "
                        + questionId + " / Question Group Id "
                        + questionGroupId);

                this.auditManager.auditDelete(this.ibatisSqlMap.getCurrentConnection(), questionBean, loginId, screenId);
                this.ibatisSqlMap.delete(this.STRING_SQL + "delete", questionBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", questionBean.getQuestionGroupId());
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Deleted Question "
                        + questionId + " / Question Group Id "
                        + questionGroupId);
                this.logger.log(ILogger.LEVEL_INFO, "Updated Scheme Last Update for Question Group Id: " +
                        questionBean.getQuestionGroupId() + "/ to: " + new java.util.Date().toString());

            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void deleteOptionAnswer(Integer questionGroupId, Integer questionId,
                                   Integer optionAnswerId, String loginId, String screenId)
            throws CoreException {
        OptionAnswerBean optionAnswerBean = new OptionAnswerBean();
        optionAnswerBean.setQuestionGroupId(questionGroupId);
        optionAnswerBean.setQuestionId(questionId);
        optionAnswerBean.setOptionAnswerId(optionAnswerId);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Deleting Option Answer "
                        + optionAnswerBean.getOptionAnswerId() + " / Question Group Id "
                        + optionAnswerBean.getQuestionGroupId() + " / Question Id "
                        + optionAnswerBean.getQuestionId());

                this.auditManager.auditDelete(this.ibatisSqlMap.getCurrentConnection(), optionAnswerBean, loginId, screenId);
                this.ibatisSqlMap.delete(this.STRING_SQL + "deleteOptionAnswer", optionAnswerBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", optionAnswerBean.getQuestionGroupId());
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Deleted Option Answer "
                        + optionAnswerBean.getOptionAnswerId() + " / Question Group Id "
                        + optionAnswerBean.getQuestionGroupId() + " / Question Id "
                        + optionAnswerBean.getQuestionId());

            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public String getHeaderForQuestion(Integer questionGroupId) throws CoreException {
        String questionGroupName = null;
        try {
            questionGroupName = (String) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getHeaderForQuestion", questionGroupId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return questionGroupName;
    }


    public QuestionForm getHeaderForOptionAnswer(Integer questionId)
            throws CoreException {
        QuestionForm result = null;
        try {
            result = (QuestionForm) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getHeaderForOptionAnswer", questionId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }


    public String getQuestionUsageType(Integer questionGroupId) throws CoreException {
        String usageType = null;
        try {
            usageType = (String) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getUsageType", questionGroupId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return usageType;
    }


    public void deleteRankQuestion(Integer questionGroupId, Integer questionId,
                                   String loginId, String screenId)
            throws CoreException {
        QuestionBean questionBean = new QuestionBean();
        questionBean.setQuestionGroupId(questionGroupId);
        questionBean.setQuestionId(questionId);
        HashMap param_UIDQA = new HashMap();
        param_UIDQA.put("question_group_id", questionGroupId);
        param_UIDQA.put("question_id", questionId);
        param_UIDQA.put("flag", 1);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Deleting Question "
                        + questionId + " / Question Group Id "
                        + questionGroupId);

                this.auditManager.auditDelete(this.ibatisSqlMap.getCurrentConnection(), questionBean, loginId, screenId);
                this.ibatisSqlMap.update(this.STRING_SQL + "uidqa", param_UIDQA);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Deleted Question "
                        + questionId + " / Question Group Id "
                        + questionGroupId);
                this.logger.log(ILogger.LEVEL_INFO, "Updated Scheme Last Update for Question Group Id: " +
                        questionBean.getQuestionGroupId() + "/ to: " + new java.util.Date().toString());

            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

}
