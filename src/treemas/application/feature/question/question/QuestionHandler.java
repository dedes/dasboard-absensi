package treemas.application.feature.question.question;

import com.google.common.base.Strings;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.general.ComboManager;
import treemas.application.feature.question.OptionAnswerBean;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.AnswerType;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;


public class QuestionHandler extends FeatureActionBase {

    public static final String TASK_UPDATE_RANK = "UpdateRank";
    private static final String[] SAVE_PARAMETERS = {
            "paging.currentPageNo", "paging.totalRecord",
            "paging.dispatch", "searchValue",
            "searchType", "questionGroupId", "previousUri"};
    private final String TASK_GO_TO_ANSWER = "OptionAnswer";
    private final String TASK_LOAD_ANSWER = "OptionAnswerLoad";
    private final String TASK_SEARCH_ANSWER = "OptionAnswerSearch";
    private final String TASK_EDIT_ANSWER = "OptionAnswerEdit";
    private final String TASK_ADD_ANSWER = "OptionAnswerAdd";
    private final String TASK_UPDATE_ANSWER = "OptionAnswerUpdate";
    private final String TASK_INSERT_ANSWER = "OptionAnswerSave";
    private final String TASK_DELETE_ANSWER = "OptionAnswerDelete";
    private final String TASK_BACK_TO_QUESTION = "BackToQuestion";
    private final String TASK_VIEW_RANK = "LoadRank";
    private final String TASK_SEARCH_RANK = "SearchRank";
    private final String TASK_ADD_RANK = "AddRank";
    private final String TASK_EDIT_RANK = "EditRank";
    private final String TASK_SAVE_RANK = "SaveRank";
    private final String TASK_DELETE_RANK = "DeleteRank";
    private final String screenId = "";
    private ComboManager comboManager = new ComboManager();
    private QuestionManager manager = new QuestionManager();
    private QuestionValidator qValidator;

    public QuestionHandler() {
        this.pageMap.put(TASK_VIEW_RANK, "viewrank");
        this.pageMap.put(TASK_SEARCH_RANK, "viewrank");
        this.pageMap.put(TASK_EDIT_RANK, "editrank");
        this.pageMap.put(TASK_ADD_RANK, "editrank");
        this.pageMap.put(TASK_SAVE_RANK, "viewrank");
        this.pageMap.put(TASK_UPDATE_RANK, "viewrank");
        this.pageMap.put(TASK_DELETE_RANK, "viewrank");
        this.pageMap.put(TASK_GO_TO_ANSWER, "optionAnswerList");
        this.pageMap.put(TASK_LOAD_ANSWER, "optionAnswerList");
        this.pageMap.put(TASK_SEARCH_ANSWER, "optionAnswerList");
        this.pageMap.put(TASK_ADD_ANSWER, "optionAnswerEdit");
        this.pageMap.put(TASK_DELETE_ANSWER, "optionAnswerList");
        this.pageMap.put(TASK_INSERT_ANSWER, "optionAnswerList");
        this.pageMap.put(TASK_EDIT_ANSWER, "optionAnswerEdit");
        this.pageMap.put(TASK_UPDATE_ANSWER, "optionAnswerList");
        this.pageMap.put(TASK_BACK_TO_QUESTION, "view");

        this.exceptionMap.put(TASK_VIEW_RANK, "viewrank");
        this.exceptionMap.put(TASK_SEARCH_RANK, "viewrank");
        this.exceptionMap.put(TASK_EDIT_RANK, "editrank");
        this.exceptionMap.put(TASK_ADD_RANK, "editrank");
        this.exceptionMap.put(TASK_SAVE_RANK, "editrank");
        this.exceptionMap.put(TASK_UPDATE_RANK, "editrank");
        this.exceptionMap.put(TASK_DELETE_RANK, "viewrank");
        this.exceptionMap.put(TASK_GO_TO_ANSWER, "optionAnswerList");
        this.exceptionMap.put(TASK_LOAD_ANSWER, "optionAnswerList");
        this.exceptionMap.put(TASK_SEARCH_ANSWER, "optionAnswerList");
        this.exceptionMap.put(TASK_ADD_ANSWER, "optionAnswerEdit");
        this.exceptionMap.put(TASK_DELETE_ANSWER, "optionAnswerList");
        this.exceptionMap.put(TASK_INSERT_ANSWER, "optionAnswerEdit");
        this.exceptionMap.put(TASK_EDIT_ANSWER, "optionAnswerEdit");
        this.exceptionMap.put(TASK_UPDATE_ANSWER, "optionAnswerEdit");
        this.exceptionMap.put(TASK_BACK_TO_QUESTION, "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.qValidator = new QuestionValidator(request);
        QuestionForm questionForm = (QuestionForm) form;
        questionForm.getPaging().calculationPage();
        String action = questionForm.getTask();

        String usage_type = this.getUsageType(questionForm);
        questionForm.setUsage_type(usage_type);

        if (Global.WEB_TASK_LOAD.equals(action)
                || Global.WEB_TASK_SEARCH.equals(action)
                || TASK_VIEW_RANK.equals(action)
                || TASK_SEARCH_RANK.equals(action)) {
            this.getHeaderForQuestion(questionForm);
            this.loadListQuestion(request, questionForm);
            questionForm.fillCurrentUri(request, SAVE_PARAMETERS);
        } else if (Global.WEB_TASK_ADD.equals(action)
                || TASK_ADD_RANK.equals(action)) {
            this.getHeaderForQuestion(questionForm);
            this.loadAnswerType(request, questionForm);
            this.loadLookupOption(request, questionForm.getAnswerType());
            questionForm.prepQuestionFormForAddQuestion();
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.qValidator.validateQuestionAdd(new ActionMessages(), questionForm)) {
                QuestionBean questionBean = new QuestionBean();
                if (Strings.isNullOrEmpty(questionForm.getMaxLength())) {
                    questionForm.setMaxLength(null);
                }
//				questionForm.toBean(questionBean);
                BeanConverter.convertBeanFromString(questionForm, questionBean, null);
                try {
                    this.manager.insertQuestion(questionBean,
                            this.getLoginId(request), this.screenId);
                    this.getHeaderForQuestion(questionForm);
                    this.loadListQuestion(request, questionForm);
                } catch (CoreException me) {
                    this.loadAnswerType(request, questionForm);
                    this.loadLookupOption(request, questionForm.getAnswerType());
                    this.logger.printStackTrace(me);
                    throw me;
                }
                request.setAttribute("message", resources.getMessage("common.process.save"));
            }
        } else if (Global.WEB_TASK_EDIT.equals(action)
                || TASK_EDIT_RANK.equals(action)) {
            this.getHeaderForQuestion(questionForm);
            this.loadAnswerType(request, questionForm);
            QuestionBean questionBean = this.manager.getSingleQuestion(
                    new Integer(questionForm.getQuestionGroupId()),
                    new Integer(questionForm.getQuestionId()));
            this.loadLookupOption(request, questionBean.getAnswerType());
            questionForm.fromBean(questionBean);
            questionForm.getPaging().setDispatch(null);
        } else if (Global.WEB_TASK_UPDATE.equals(action)
                || TASK_UPDATE_RANK.equals(action)) {
            if (this.qValidator.validateQuestionEdit(new ActionMessages(), questionForm)) {
                QuestionBean questionBean = new QuestionBean();
                if (Strings.isNullOrEmpty(questionForm.getMaxLength())) {
                    questionForm.setMaxLength(null);
                }
				questionForm.toBean(questionBean);

                BeanConverter.convertBeanFromString(questionForm, questionBean, null);
                try {
                    this.manager.updateQuestion(questionBean, this.getLoginId(request),
                            this.screenId);
                    this.getHeaderForQuestion(questionForm);
                    this.loadListQuestion(request, questionForm);
                } catch (CoreException me) {
                    this.loadAnswerType(request, questionForm);
                    this.loadLookupOption(request, questionBean.getAnswerType());
                    this.logger.printStackTrace(me);
                    throw me;
                }
                request.setAttribute("message", resources.getMessage("common.process.update"));
            }
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = questionForm.getPaging();
            try {
                this.manager.deleteQuestion(new Integer(questionForm.getQuestionGroupId()),
                        new Integer(questionForm.getQuestionId()),
                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.getHeaderForQuestion(questionForm);
                this.loadListQuestion(request, questionForm);
                page.setDispatch(null);
            }
        } else if (Global.WEB_TASK_BACK.equals(action)) {
            return this.doBack(request);
        } else if (TASK_GO_TO_ANSWER.equals(action)) {
            PagingInfo page = questionForm.getPaging();
            HttpSession session = request.getSession();
            session.setAttribute(Global.SESSION_PAGE_QUESTION, page.clone());
            session.setAttribute(Global.SESSION_SEARCH_TYPE_QUESTION, questionForm.getSearchType());
            session.setAttribute(Global.SESSION_SEARCH_VALUE_QUESTION, questionForm.getSearchValue());
            page.setCurrentPageNo(1);
            page.setDispatch("Go");
            page.calculationPage();
            questionForm.setSearchValue("");
            this.getHeaderForOptionAnswer(questionForm);
            this.loadListAnswer(request, questionForm);
        } else if (TASK_LOAD_ANSWER.equals(action) ||
                TASK_SEARCH_ANSWER.equals(action)) {
            this.getHeaderForOptionAnswer(questionForm);
            this.loadListAnswer(request, questionForm);
        } else if (TASK_EDIT_ANSWER.equals(action)) {
            OptionAnswerBean optionAnswerBean = this.manager.getSingleOptionAnswer(
                    new Integer(questionForm.getQuestionGroupId()),
                    new Integer(questionForm.getQuestionId()),
                    new Integer(questionForm.getOptionAnswerId()));
            questionForm.fromBean(optionAnswerBean);
            questionForm.getPaging().setDispatch(null);
            this.getHeaderForOptionAnswer(questionForm);
        } else if (TASK_UPDATE_ANSWER.equals(action)) {
            if (this.qValidator.validateAnswerAdd(new ActionMessages(), questionForm)) {
                OptionAnswerBean optionAnswerBean = new OptionAnswerBean();
                questionForm.toBean(optionAnswerBean);
                this.manager.updateOptionAnswer(optionAnswerBean, this.getLoginId(request),
                        this.screenId);
                this.getHeaderForOptionAnswer(questionForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
                this.loadListAnswer(request, questionForm);
            }
        } else if (TASK_ADD_ANSWER.equals(action)) {
            questionForm.prepQuestionFormForAddQuestion();
            this.getHeaderForOptionAnswer(questionForm);
        } else if (TASK_INSERT_ANSWER.equals(action)) {
            if (this.qValidator.validateAnswerEdit(new ActionMessages(), questionForm)) {
                OptionAnswerBean optionAnswerBean = new OptionAnswerBean();
                questionForm.toBean(optionAnswerBean);
                this.manager.insertOptionAnswer(optionAnswerBean, this
                        .getLoginId(request), this.screenId);
                this.getHeaderForOptionAnswer(questionForm);
                request.setAttribute("message", resources.getMessage("common.process.add"));
                this.loadListAnswer(request, questionForm);
            }

        } else if (TASK_BACK_TO_QUESTION.equals(action)) {
            HttpSession session = request.getSession();
            PagingInfo page = (PagingInfo) session.getAttribute(Global.SESSION_PAGE_QUESTION);
            String searchType = (String) session.getAttribute(Global.SESSION_SEARCH_TYPE_QUESTION);
            String searchValue = (String) session.getAttribute(Global.SESSION_SEARCH_VALUE_QUESTION);

            questionForm.setPaging(page);
            questionForm.setSearchType(searchType);
            questionForm.setSearchValue(searchValue);
            session.removeAttribute(Global.SESSION_PAGE_QUESTION);
            session.removeAttribute(Global.SESSION_SEARCH_TYPE_QUESTION);
            session.removeAttribute(Global.SESSION_SEARCH_VALUE_QUESTION);

            this.getHeaderForQuestion(questionForm);
            this.loadListQuestion(request, questionForm);
        } else if (TASK_DELETE_ANSWER.equals(action)) {
            PagingInfo page = questionForm.getPaging();
            try {
                this.manager.deleteOptionAnswer(new Integer(questionForm.getQuestionGroupId()),
                        new Integer(questionForm.getQuestionId()),
                        new Integer(questionForm.getOptionAnswerId()),
                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.getHeaderForOptionAnswer(questionForm);
                this.loadListAnswer(request, questionForm);
                page.setDispatch(null);
            }
        } else if (TASK_SAVE_RANK.equals(action)) {
            if (this.qValidator.validateQuestionRank(new ActionMessages(), questionForm)) {
                QuestionBean questionBean = new QuestionBean();
                if (Strings.isNullOrEmpty(questionForm.getMaxLength())) {
                    questionForm.setMaxLength(null);
                }

                BeanConverter.convertBeanFromString(questionForm, questionBean, null);
                try {
                    this.manager.insertRankQuestion(questionBean, this.getLoginId(request), this.screenId);
                    this.getHeaderForQuestion(questionForm);
                    this.loadListQuestion(request, questionForm);
                } catch (CoreException me) {
                    this.loadAnswerType(request, questionForm);
                    this.loadLookupOption(request, questionForm.getAnswerType());
                    this.logger.printStackTrace(me);
                    throw me;
                }
                request.setAttribute("message", resources.getMessage("common.process.save"));
            }
        } else if (TASK_DELETE_RANK.equals(action)) {
            PagingInfo page = questionForm.getPaging();
            try {
                this.manager.deleteRankQuestion(new Integer(questionForm.getQuestionGroupId()),
                        new Integer(questionForm.getQuestionId()),
                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.getHeaderForQuestion(questionForm);
                this.loadListQuestion(request, questionForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private String getUsageType(QuestionForm questionForm) throws CoreException {
        String ret = null;

        String question_group_id = questionForm.getQuestionGroupId();
        String usage_type = questionForm.getUsage_type();

        if (!Strings.isNullOrEmpty(usage_type)) {
            return usage_type;
        }

        if (Strings.isNullOrEmpty(usage_type) && !Strings.isNullOrEmpty(question_group_id)) {
            ret = this.manager.getQuestionUsageType(new Integer(question_group_id));
        }

        return ret;
    }

    private void loadListQuestion(HttpServletRequest request, QuestionForm questionForm)
            throws CoreException {
        int targetPageNo = 1;
        if (null != questionForm.getPaging()) {
            targetPageNo = questionForm.getPaging().getTargetPageNo();
        }
        String parse = "0";
        if (!Strings.isNullOrEmpty(questionForm.getSearchType())) {
            parse = questionForm.getSearchType();
        }
        int searchType = Integer.parseInt(parse);
        if (Global.WEB_TASK_SEARCH.equals(questionForm.getTask())) {
            targetPageNo = 1;
        }
        PageListWrapper result = this.manager.getAllQuestion(targetPageNo,
                searchType, questionForm.getSearchValue(),
                new Integer(questionForm.getQuestionGroupId()));
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, QuestionForm.class);
        questionForm.getPaging().setCurrentPageNo(targetPageNo);
        questionForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listQuestion", list);
    }

    private void loadListAnswer(HttpServletRequest request, QuestionForm questionForm)
            throws CoreException {
        int targetPageNo = questionForm.getPaging().getTargetPageNo();
        int searchType = Integer.parseInt(questionForm.getSearchType());
        if (TASK_SEARCH_ANSWER.equals(questionForm.getTask())) {
            targetPageNo = 1;
        }
        String userId = this.getLoginId(request);
     // IRFAN   
        /*if (questionForm.getQuestionGroupId().equals("1088")&&questionForm.getQuestionId().equals("1736")){
        	PageListWrapper result = this.manager.getAllOptionAnswerBranch(targetPageNo,
                    searchType, questionForm.getSearchValue(),
                    new Integer(questionForm.getQuestionGroupId()),
                    new Integer(questionForm.getQuestionId()),
                    this.getLoginId(request));
            List list = result.getResultList();
            list = BeanConverter.convertBeansToString(list, QuestionForm.class);
            questionForm.getPaging().setCurrentPageNo(targetPageNo);
            questionForm.getPaging().setTotalRecord(result.getTotalRecord());
            request.setAttribute("listOptionAnswer", list);
        }else{      */  	
        PageListWrapper result = this.manager.getAllOptionAnswer(targetPageNo,
                searchType, questionForm.getSearchValue(),
                new Integer(questionForm.getQuestionGroupId()),
                new Integer(questionForm.getQuestionId()),
                this.getLoginId(request));
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, QuestionForm.class);
        questionForm.getPaging().setCurrentPageNo(targetPageNo);
        questionForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listOptionAnswer", list);
    }
  //  }
    
    // END IRFAN 
    private void loadAnswerType(HttpServletRequest request, QuestionForm form) throws CoreException {
        List list = comboManager.getComboAnswerType(form.getUsage_type());
        request.setAttribute("listAnswerType", list);
    }

    private void loadLookupOption(HttpServletRequest request, String answerType) throws CoreException {
        String param = null;
        if (AnswerType.LOOKUP.equals(answerType)) {
            param = "0";
        } else if (AnswerType.LOOKUP_WITH_FILTER.equals(answerType)) {
            param = "1";
        }
        List list = comboManager.getComboLookup(param);
        request.setAttribute("listLookup", list);
    }

    private void getHeaderForQuestion(QuestionForm questionForm) throws CoreException {
        String questionGroupName = this.manager.getHeaderForQuestion(
                new Integer(questionForm.getQuestionGroupId()));
        questionForm.setQuestionGroupName(questionGroupName);
    }

    private void getHeaderForOptionAnswer(QuestionForm questionForm) throws CoreException {
        QuestionForm headerForm = this.manager.getHeaderForOptionAnswer(
                new Integer(questionForm.getQuestionId()));
        questionForm.setQuestionGroupName(headerForm.getQuestionGroupName());
        questionForm.setQuestionLabel(headerForm.getQuestionLabel());
    }

    public String resolvePreviousTask(ActionForm form) {
        QuestionForm questionForm = (QuestionForm) form;
        String task = questionForm.getTask();
        if (TASK_INSERT_ANSWER.equals(task))
            return TASK_ADD_ANSWER;
        else if (TASK_UPDATE_ANSWER.equals(task))
            return TASK_EDIT_ANSWER;
        else if (TASK_SAVE_RANK.equals(task))
            return TASK_ADD_RANK;
        else if (TASK_UPDATE_RANK.equals(task))
            return TASK_EDIT_RANK;
        else
            return questionForm.resolvePreviousTask();
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        QuestionForm questionForm = (QuestionForm) form;
        switch (code) {
            case QuestionManager.ERROR_HAS_CHILD:
                this.setMessage(request, "common.haschild", null);
                break;
            case QuestionManager.ERROR_DATA_EXISTS:
                this.setMessage(request, "setting.question.param.dataexists",
                        new Object[]{ex.getUserObject()});
                break;
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                try {
                    this.getHeaderForQuestion(questionForm);
                    this.loadAnswerType(request, questionForm);
                    this.loadLookupOption(request, questionForm.getAnswerType());
                } catch (CoreException e) {
                    e.printStackTrace();
                    this.logger.printStackTrace(e);
                }
                break;
            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((QuestionForm) form).setTask(this.resolvePreviousTask(questionForm));
        String task = ((QuestionForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
