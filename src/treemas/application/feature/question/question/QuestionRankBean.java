package treemas.application.feature.question.question;

import java.io.Serializable;

public class QuestionRankBean implements Serializable {
    private Integer option_answer_id;
    private Integer question_group_id;
    private String option_answer_label;
    private String line_seq_order;
    private String active;
    private String legacy_code;

    public Integer getOption_answer_id() {
        return option_answer_id;
    }

    public void setOption_answer_id(Integer option_answer_id) {
        this.option_answer_id = option_answer_id;
    }

    public Integer getQuestion_group_id() {
        return question_group_id;
    }

    public void setQuestion_group_id(Integer question_group_id) {
        this.question_group_id = question_group_id;
    }

    public String getOption_answer_label() {
        return option_answer_label;
    }

    public void setOption_answer_label(String option_answer_label) {
        this.option_answer_label = option_answer_label;
    }

    public String getLine_seq_order() {
        return line_seq_order;
    }

    public void setLine_seq_order(String line_seq_order) {
        this.line_seq_order = line_seq_order;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getLegacy_code() {
        return legacy_code;
    }

    public void setLegacy_code(String legacy_code) {
        this.legacy_code = legacy_code;
    }


}
