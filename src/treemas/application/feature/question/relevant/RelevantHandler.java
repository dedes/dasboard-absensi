package treemas.application.feature.question.relevant;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class RelevantHandler extends FeatureActionBase {
    private final String screenId = "";
    private RelevantManager manager = new RelevantManager();

    public RelevantHandler() {
        this.exceptionMap.put(Global.WEB_TASK_INSERT, "view");
        this.exceptionMap.put(Global.WEB_TASK_DELETE, "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        RelevantForm relevantForm = (RelevantForm) form;
        relevantForm.getPaging().calculationPage();
        String action = relevantForm.getTask();

        if (Global.WEB_TASK_LOAD.equals(action) ||
                Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadHeader(relevantForm);
            this.loadList(request, relevantForm);
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            RelevantBean relevantBean = new RelevantBean();
            relevantBean.fromForm(relevantForm);
            try {
                this.manager.insert(relevantBean,
                        this.getLoginId(request), this.screenId);
                relevantForm.clearFormAfterInsert();
            } catch (CoreException me) {
                throw me;
            } finally {
                this.loadHeader(relevantForm);
                this.loadList(request, relevantForm);
            }
            request.setAttribute("message", resources.getMessage("common.process.save"));
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = relevantForm.getPaging();
            try {
                this.manager.delete(new Integer(relevantForm.getRelSeqno()),
                        this.getLoginId(request), this.screenId);
            } catch (CoreException me) {
                throw me;
            } finally {
                this.loadHeader(relevantForm);
                this.loadList(request, relevantForm);
                page.setDispatch(null);
            }
        } else if (Global.WEB_TASK_BACK.equals(action)) {
            return this.doBack(request);
        }

        return this.getNextPage(action, mapping);
    }

    private void loadHeader(RelevantForm relevantForm) throws CoreException {
        RelevantBean bean = this.manager.getHeader(
                new Integer(relevantForm.getQuestionGroupId()),
                new Integer(relevantForm.getQuestionId()));
        relevantForm.setQuestionGroupName(bean.getQuestionGroupName());
        relevantForm.setQuestionLabel(bean.getQuestionLabel());
    }

    private void loadList(HttpServletRequest request, RelevantForm relForm)
            throws CoreException {
        PageListWrapper result = this.manager.getAll(
                new Integer(relForm.getQuestionGroupId()),
                new Integer(relForm.getQuestionId()));
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, RelevantForm.class);
        request.setAttribute("list", list);
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();

        switch (code) {
            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((RelevantForm) form).setTask(((RelevantForm) form).resolvePreviousTask());
        String task = ((RelevantForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
