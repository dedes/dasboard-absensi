package treemas.application.feature.question.relevant;

import treemas.base.feature.FeatureFormBase;

public class RelevantForm extends FeatureFormBase {
    private String questionGroupId;
    private String questionId;
    private String relTextAnswer;
    private String relQuestionGroupId;
    private String relQuestionId;
    private String relOptionAnswerId;
    private String relSeqno;
    private String questionGroupName;
    private String questionLabel;
    private String relQuestionGroupName;
    private String relQuestionLabel;
    private String relAnswerTypeId;

    public String getRelTextAnswer() {
        return relTextAnswer;
    }

    public void setRelTextAnswer(String relTextAnswer) {
        this.relTextAnswer = relTextAnswer;
    }

    public String getRelSeqno() {
        return relSeqno;
    }

    public void setRelSeqno(String relSeqno) {
        this.relSeqno = relSeqno;
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getRelQuestionGroupName() {
        return relQuestionGroupName;
    }

    public void setRelQuestionGroupName(String relQuestionGroupName) {
        this.relQuestionGroupName = relQuestionGroupName;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = (questionLabel == null) ? null : questionLabel.trim();
    }

    public String getRelQuestionGroupId() {
        return relQuestionGroupId;
    }

    public void setRelQuestionGroupId(String relQuestionGroupId) {
        this.relQuestionGroupId = relQuestionGroupId;
    }

    public String getRelQuestionId() {
        return relQuestionId;
    }

    public void setRelQuestionId(String relQuestionId) {
        this.relQuestionId = relQuestionId;
    }

    public String getRelQuestionLabel() {
        return relQuestionLabel;
    }

    public void setRelQuestionLabel(String relQuestionLabel) {
        this.relQuestionLabel = relQuestionLabel;
    }

    public String getRelAnswerTypeId() {
        return relAnswerTypeId;
    }

    public void setRelAnswerTypeId(String relAnswerTypeId) {
        this.relAnswerTypeId = relAnswerTypeId;
    }

    public String getRelOptionAnswerId() {
        return relOptionAnswerId;
    }

    public void setRelOptionAnswerId(String relOptionAnswerId) {
        this.relOptionAnswerId = relOptionAnswerId;
    }

    public void clearFormAfterInsert() {
        this.relAnswerTypeId = "";
        this.relOptionAnswerId = "";
        this.relQuestionGroupId = "";
        this.relQuestionGroupName = "";
        this.relQuestionId = "";
        this.relQuestionLabel = "";
        this.relTextAnswer = "";
    }
}
