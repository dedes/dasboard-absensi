package treemas.application.feature.question.relevant;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;

public class RelevantBean implements Serializable, Auditable {
    private static final String[] AUDIT_COLUMNS = new String[]{"relSeqno",
            "questionGroupId", "questionId", "relQuestionGroupId",
            "relQuestionId", "relOptionAnswerId", "relTextAnswer"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{
            "REL_SEQNO", "QUESTION_GROUP_ID", "QUESTION_ID",
            "REL_QUESTION_GROUP_ID", "REL_QUESTION_ID", "REL_OPTION_ANSWER_ID",
            "REL_TEXT_ANSWER"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_QUESTION_RELEVANT;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"relSeqno"};

    private Integer questionGroupId;
    private Integer questionId;
    private String relTextAnswer;
    private Integer relQuestionGroupId;
    private Integer relQuestionId;
    private Integer relOptionAnswerId;
    private Integer relSeqno;
    private String questionGroupName;
    private String questionLabel;
    private String relQuestionGroupName;
    private String relQuestionLabel;
    private String relAnswerTypeId;

    public String getRelTextAnswer() {
        return relTextAnswer;
    }

    public void setRelTextAnswer(String relTextAnswer) {
        this.relTextAnswer = relTextAnswer;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public Integer getRelSeqno() {
        return relSeqno;
    }

    public void setRelSeqno(Integer relSeqno) {
        this.relSeqno = relSeqno;
    }

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getRelQuestionGroupName() {
        return relQuestionGroupName;
    }

    public void setRelQuestionGroupName(String relQuestionGroupName) {
        this.relQuestionGroupName = relQuestionGroupName;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public Integer getRelQuestionGroupId() {
        return relQuestionGroupId;
    }

    public void setRelQuestionGroupId(Integer relQuestionGroupId) {
        this.relQuestionGroupId = relQuestionGroupId;
    }

    public Integer getRelQuestionId() {
        return relQuestionId;
    }

    public void setRelQuestionId(Integer relQuestionId) {
        this.relQuestionId = relQuestionId;
    }

    public String getRelQuestionLabel() {
        return relQuestionLabel;
    }

    public void setRelQuestionLabel(String relQuestionLabel) {
        this.relQuestionLabel = relQuestionLabel;
    }

    public String getRelAnswerTypeId() {
        return relAnswerTypeId;
    }

    public void setRelAnswerTypeId(String relAnswerTypeId) {
        this.relAnswerTypeId = relAnswerTypeId;
    }

    public Integer getRelOptionAnswerId() {
        return relOptionAnswerId;
    }

    public void setRelOptionAnswerId(Integer relOptionAnswerId) {
        this.relOptionAnswerId = relOptionAnswerId;
    }

    public void fromForm(RelevantForm relevantForm) {
        this.questionGroupId = new Integer(relevantForm.getQuestionGroupId());
        this.questionId = new Integer(relevantForm.getQuestionId());
        this.relTextAnswer = relevantForm.getRelTextAnswer();
        this.relQuestionGroupId = new Integer(
                relevantForm.getRelQuestionGroupId());
        this.relQuestionId = new Integer(relevantForm.getRelQuestionId());
        String optId = relevantForm.getRelOptionAnswerId();
        if (!"".equals(optId)) {
            this.relOptionAnswerId = new Integer(optId);
        }
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }
}
