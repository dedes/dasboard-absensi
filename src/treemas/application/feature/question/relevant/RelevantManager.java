package treemas.application.feature.question.relevant;

import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.CoreException;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RelevantManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_QUESTION_RELEVANT;

    public RelevantBean getHeader(Integer questionGroupId, Integer questionId) throws CoreException {
        RelevantBean result = null;
        Map paramMap = new HashMap();
        paramMap.put("questionGroupId", questionGroupId);
        paramMap.put("questionId", questionId);
        try {
            result = (RelevantBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getHeader", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public PageListWrapper getAll(
            Integer questionGroupId, Integer questionId) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        Map paramMap = new HashMap();
        paramMap.put("questionGroupId", questionGroupId);
        paramMap.put("questionId", questionId);
        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", paramMap);
            result.setResultList(list);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public void insert(RelevantBean relBean,
                       String loginId, String screenId) throws CoreException {
        try {
            try {
                boolean noSequence = "0".equals(ConfigurationProperties.getInstance().
                        get(PropertiesKey.DB_SEQUENCE));
                if (!noSequence) {
                    SequenceManager seqManager = new SequenceManager();
                    relBean.setRelSeqno((
                            new Integer(seqManager.getSequenceRelevant())));
                }

                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Inserting Relevant "
                        + relBean.getRelSeqno());

                this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), relBean, loginId, screenId);
                this.ibatisSqlMap.insert(this.STRING_SQL + "insert", relBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", relBean.getRelSeqno());
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Inserted Relevant "
                        + relBean.getRelSeqno());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void delete(Integer relSeqno,
                       String loginId, String screenId)
            throws CoreException {
        RelevantBean relBean = new RelevantBean();
        relBean.setRelSeqno(relSeqno);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Deleting Relevant "
                        + relSeqno);

                this.auditManager.auditDelete(this.ibatisSqlMap.getCurrentConnection(), relBean, loginId, screenId);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", relSeqno);
                this.ibatisSqlMap.delete(this.STRING_SQL + "delete", relSeqno);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Deleted Relevant "
                        + relSeqno);
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }
}