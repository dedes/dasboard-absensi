package treemas.application.feature.general;

import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ComboManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_COMMON;

    public List getComboSurveyor() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allSurveyor", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboActiveScheme() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allActiveScheme", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboScheme() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allScheme", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboScheme(String userId) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getSchemeByUser", userId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboUndistScheme(String userId) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getUndistSchemeByUser", userId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboPriority() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allPriority", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboAnswerType() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allAnswerType", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboAnswerType(String usage_type) throws CoreException {
        List result = null;
        Map param = new HashMap();
        param.put("usage_type", usage_type);
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allAnswerType", param);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }


    /*TODO
     * COMMIT THIS and SQL: ibatis/feature/sql/common/combo-ibatis-cfg.xml
     * */
    public List getComboUsageType() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allUsageType");
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboStatusSurvey() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allStatusSurvey", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboCabangByLogin(String branchId) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getCabangByLogin", branchId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboMultiCabangByLogin(String userId) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getMultiCabangByLogin", userId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboCabangLogin(String branchId) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getCabangLogin", branchId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboMultiCabangLogin(String userId) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getMultiCabangByLogin", userId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboAllCabang() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allCabang", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboLookup(String param) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allLookupOption", param);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboPrintItemType() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "allPrintItemType", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getCollectionArea(String branchId) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getCollectionArea", branchId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

        return result;
    }

    public List getCollectorName(String branchId, String collectionArea) throws CoreException {
        List result = null;
        Map paramMap = new HashMap();
        paramMap.put("branchId", branchId);
        paramMap.put("collectionArea", collectionArea);
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getCollectorName", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

        return result;
    }

    public List getEmplJob() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getEmplJob", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getSurveyorName(String branchId, String surveyorArea) throws CoreException {
        List result = null;
        Map paramMap = new HashMap();
        paramMap.put("branchId", branchId);
        paramMap.put("surveyorArea", surveyorArea);
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getSurveyorName", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

        return result;
    }

    public List getComboSchemeCollection() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getSchemeCollection");
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }


    public List getComboSchemeSurvey() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getSchemeSurvey");
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getComboSchemeSurveyByUser(String userId) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getSchemeSurveyByUser", userId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }
}
