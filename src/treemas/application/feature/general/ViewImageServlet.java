package treemas.application.feature.general;

import treemas.util.constant.PropertiesKey;
import treemas.util.ibatis.IbatisHelper;
import treemas.util.tool.ConfigurationProperties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;


public class ViewImageServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        if (req.getParameter("mid") == null &&
                req.getParameter("gid") == null &&
                req.getAttribute("qid") == null)
            return;

        boolean saveImageToDatabase =
                ("1".equals(ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_DATABASE)));

        if (!saveImageToDatabase) {
            this.loadImageFromFile(req, resp);
        } else {
            this.loadImageFromDatabase(req, resp);
        }
    }

    private void loadImageFromDatabase(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection conn = null;
        try {
            try {
                conn = IbatisHelper.getSqlMap().getDataSource().getConnection();
                Blob img;
                byte[] imgData = null;

                StringBuffer sql = new StringBuffer("Select image_blob from mobile_survey_result_detail where ").
                        append("MOBILE_ASSIGNMENT_ID = ? AND QUESTION_GROUP_ID = ? AND QUESTION_ID = ?");
                PreparedStatement ps = conn.prepareStatement(sql.toString());
                ps.setString(1, req.getParameter("mid"));
                ps.setString(2, req.getParameter("gid"));
                ps.setString(3, req.getParameter("qid"));
                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    img = rs.getBlob(1);
                    imgData = img.getBytes(1, (int) img.length());
                }

                rs.close();
                ps.close();

                // display the image
                resp.setContentType("image/jpeg");
                OutputStream o = resp.getOutputStream();
                o.write(imgData);

                o.flush();
                o.close();
            } finally {
                conn.close();
            }
        } catch (SQLException sqle) {
            throw new ServletException(sqle);
        }
    }

    private void loadImageFromFile(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection conn = null;
        try {
            try {
                conn = IbatisHelper.getSqlMap().getDataSource().getConnection();

                String imagePath = null;

                StringBuffer sql = new StringBuffer("Select image_path from mobile_survey_result_detail where ").
                        append("MOBILE_ASSIGNMENT_ID = ? AND QUESTION_GROUP_ID = ? AND QUESTION_ID = ?");
                PreparedStatement ps = conn.prepareStatement(sql.toString());
                ps.setString(1, req.getParameter("mid"));
                ps.setString(2, req.getParameter("gid"));
                ps.setString(3, req.getParameter("qid"));
                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    imagePath = rs.getString(1);
                }

                rs.close();
                ps.close();

                File imageFile = new File(imagePath);
                FileInputStream in = new FileInputStream(imageFile);

                // display the image
                resp.setContentType("image/jpeg");
                OutputStream o = resp.getOutputStream();
                // Copy the contents of the file to the output stream
                byte[] buf = new byte[1024];
                int count = 0;
                while ((count = in.read(buf)) >= 0) {
                    o.write(buf, 0, count);
                }
                in.close();
                o.flush();
                o.close();
            } finally {
                conn.close();
            }
        } catch (SQLException sqle) {
            throw new ServletException(sqle);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

}
