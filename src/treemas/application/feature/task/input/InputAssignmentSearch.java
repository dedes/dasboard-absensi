package treemas.application.feature.task.input;

import treemas.base.search.SearchForm;

public class InputAssignmentSearch extends SearchForm {
    private String status;
    private String scheme;
    private String priority;
    private String branchId;
    private String userId;

    public InputAssignmentSearch() {
        super.setSearchType1(10);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
