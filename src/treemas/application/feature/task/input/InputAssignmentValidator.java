package treemas.application.feature.task.input;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class InputAssignmentValidator extends Validator {
    private InputAssignmentManager manager = new InputAssignmentManager();

    public InputAssignmentValidator(HttpServletRequest request) {
        super(request);
    }

    public InputAssignmentValidator(Locale locale) {
        super(locale);
    }

    public boolean validateAssignmentInsert(ActionMessages messages, InputAssignmentForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        try {
            InputAssignmentBean bean = manager.getSingleAssignment(form.getMobileAssignmentId());
            if (null != bean) {
                messages.add("mobileAssignmentId", new ActionMessage("errors.duplicate", "mobileAssignmentId"));
                throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }

        this.textValidator(messages, form.getNik(), "app.assignment.nik", "nik", "1", "30", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getCustomerName(), "common.name", "customerName", "1", "30", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);

        this.phoneValidator(messages, form.getCustomerPhone(), "app.assignment.customerphone", "customerPhone", ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT, ValidatorGlobal.TM_TYPE_PHONE);
        this.phoneValidator(messages, form.getOfficePhone(), "app.assignment.officephone", "officePhone", ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT, ValidatorGlobal.TM_TYPE_PHONE);
        this.phoneValidator(messages, form.getCustomerHandPhone(), "app.assignment.cellularphone", "customerHandPhone", ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT, ValidatorGlobal.TM_TYPE_PONSEL);

        this.textValidator(messages, form.getAddressStreet1(), "app.assignment.customeraddress", "customerAddress", "1", "500", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getNoaddr(), "app.assignment.addressnumber", "noaddr", "1", "10", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getRt(), "app.assignment.rt", "rt", "1", "6", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getRw(), "app.assignment.rt", "rt", "1", "6", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getPropinsi(), "app.assignment.province", "propinsi", "1", "150", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getKodePos(), "app.assignment.postcode", "postcode", "1", "6", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getNegara(), "app.assignment.country", "negara", "1", "150", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getGpsLatitude(), "app.assignment.latitude", "gpsLatitude", "1", "32", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL_ALLOW_ZERO);
        this.textValidator(messages, form.getGpsLongitude(), "app.assignment.longitude", "gpsLongitude", "1", "32", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL_ALLOW_ZERO);
        this.textValidator(messages, form.getCity(), "app.assignment.city", "city", "1", "150", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);

        this.textValidator(messages, form.getSurveyorId(), "app.assginment.surveyorid", "surveyorId", "1", "15", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);

        this.comboValidator(messages, form.getPriority(), "1", "1", "app.assignment.priority", "priority", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
        return ret;

    }

    public boolean validateAssignmentEdit(ActionMessages messages, InputAssignmentForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.textValidator(messages, form.getNik(), "app.assignment.nik", "nik", "1", "30", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getCustomerName(), "common.name", "customerName", "1", "30", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);

        this.phoneValidator(messages, form.getCustomerPhone(), "app.assignment.customerphone", "customerPhone", ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT, ValidatorGlobal.TM_TYPE_PHONE);
        this.phoneValidator(messages, form.getOfficePhone(), "app.assignment.officephone", "officePhone", ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT, ValidatorGlobal.TM_TYPE_PHONE);
        this.phoneValidator(messages, form.getCustomerHandPhone(), "app.assignment.cellularphone", "customerHandPhone", ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT, ValidatorGlobal.TM_TYPE_PONSEL);

        this.textValidator(messages, form.getAddressStreet1(), "app.assignment.customeraddress", "customerAddress", "1", "500", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getNoaddr(), "app.assignment.addressnumber", "noaddr", "1", "10", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getRt(), "app.assignment.rt", "rt", "1", "6", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getRw(), "app.assignment.rt", "rt", "1", "6", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getPropinsi(), "app.assignment.province", "propinsi", "1", "150", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getKodePos(), "app.assignment.postcode", "postcode", "1", "6", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getNegara(), "app.assignment.country", "negara", "1", "150", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getGpsLatitude(), "app.assignment.latitude", "gpsLatitude", "1", "32", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL_ALLOW_ZERO);
        this.textValidator(messages, form.getGpsLongitude(), "app.assignment.longitude", "gpsLongitude", "1", "32", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL_ALLOW_ZERO);
        this.textValidator(messages, form.getCity(), "app.assignment.city", "city", "1", "150", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);

        this.textValidator(messages, form.getSurveyorId(), "app.assginment.surveyorid", "surveyorId", "1", "15", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);

        this.comboValidator(messages, form.getPriority(), "1", "1", "app.assignment.priority", "priority", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
        return ret;

    }
}	
