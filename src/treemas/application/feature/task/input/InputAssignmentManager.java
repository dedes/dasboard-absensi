package treemas.application.feature.task.input;

import treemas.application.feature.task.MobileAssignmentHistoryBean;
import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.handset.ResultDetailBean;
import treemas.util.CoreException;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.ConfigurationProperties;
import treemas.util.tool.Tools;

import java.sql.SQLException;
import java.util.List;

public class InputAssignmentManager extends FeatureManagerBase {
    static final int SEARCH_BY_CUSTOMER_NAME = 10;
    static final int SEARCH_BY_SURVEYOR_NAME = 20;
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_ASSIGNMENT;
    private static final String INPUT = "I";
    private static final String NEED_REVISION = "V";

    public PageListWrapper getAllAssignment(int numberPage, InputAssignmentSearch searchParam) throws CoreException {
        PageListWrapper result = new PageListWrapper();

        int searchType = searchParam.getSearchType1();
        String searchValue = searchParam.getSearchValue1();
        if (searchType != SEARCH_BY_CUSTOMER_NAME
                && searchType != SEARCH_BY_SURVEYOR_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: " + searchType);

        searchValue = searchValue != null ? searchValue.trim() : searchValue;
        searchParam.setPage(numberPage);
        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL
                    + "getAll", searchParam);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", searchParam);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public InputAssignmentBean getSingleAssignment(String mobileAssignmentId)
            throws CoreException {
        InputAssignmentBean result = null;
        try {
            result = (InputAssignmentBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getSingle", mobileAssignmentId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public void updateAssignment(InputAssignmentBean inputAssignmentBean,
                                 String loginId, String screenId) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Updating Assignment "
                        + inputAssignmentBean.getMobileAssignmentId());

                // this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(),
                // inputAssignmentBean, loginId, screenId);
                this.ibatisSqlMap.update(this.STRING_SQL + "update",
                        inputAssignmentBean);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Updated Assignment "
                        + inputAssignmentBean.getMobileAssignmentId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            throw new CoreException(se, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void insertAssignment(InputAssignmentBean inputAssignmentBean,
                                 String loginId, String screenId) throws CoreException {
        try {
            try {
                boolean noSequence = "0".equals(ConfigurationProperties
                        .getInstance().get(PropertiesKey.DB_SEQUENCE));

                if (!noSequence) {
                    SequenceManager sequenceManager = new SequenceManager();
                    int newMobileAssignmentId = sequenceManager
                            .getSequenceMobileAssignmentId();
                    inputAssignmentBean.setMobileAssignmentId(new Integer(
                            newMobileAssignmentId));
                }
                inputAssignmentBean.setUsrcrt(loginId);

                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Inserting Assignment "
                        + inputAssignmentBean.getMobileAssignmentId());

                // this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(),
                // inputAssignmentBean, loginId, screenId);
                this.ibatisSqlMap.insert(this.STRING_SQL + "insert",
                        inputAssignmentBean);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Inserted Assignment "
                        + inputAssignmentBean.getMobileAssignmentId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void deleteAssignment(String mobileAssignmentId, String loginId,
                                 String screenId) throws CoreException {
        InputAssignmentBean inputAssignmentBean = new InputAssignmentBean();
        inputAssignmentBean.setSchemeId(mobileAssignmentId);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Deleting Assignment "
                        + mobileAssignmentId);

                // this.auditManager.auditDelete(this.ibatisSqlMap.getCurrentConnection(),
                // inputAssignmentBean, loginId, screenId);
                this.ibatisSqlMap.delete(this.STRING_SQL + "delete",
                        inputAssignmentBean);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Deleted Assignment "
                        + mobileAssignmentId);
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND) {
                throw new CoreException("", sqle, ERROR_HAS_CHILD,
                        mobileAssignmentId);

            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void requestAssignment(String[] arrPilih, String userLogin)
            throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();

                MobileAssignmentHistoryBean paramBean = new MobileAssignmentHistoryBean();
                paramBean.setUsrupd(userLogin);

                this.getMobileAssignmentId(arrPilih, paramBean);

                this.ibatisSqlMap.commitTransaction();
                // this.ibatisSqlMap.endTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (CoreException me) {
            throw me;
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    private void getMobileAssignmentId(String[] arrPilih,
                                       MobileAssignmentHistoryBean paramBean) throws CoreException,
            SQLException, Exception {
        for (int i = 0; i < arrPilih.length; i++) {
            Integer mobileAssignmentId = Integer.valueOf(arrPilih[i]);
            paramBean.setMobileAssignmentId(mobileAssignmentId);
            this.update(paramBean);
        }

        this.logger.log(ILogger.LEVEL_INFO,
                "Requested Assignment: " + Tools.implode(arrPilih, ","));
    }

    private void update(MobileAssignmentHistoryBean paramBean)
            throws CoreException, SQLException, Exception {
        try {
            String status = this.getStatus(paramBean.getMobileAssignmentId());
            if (!INPUT.equals(status) && !NEED_REVISION.equals(status)) {
                this.logger.log(
                        ILogger.LEVEL_INFO,
                        "Status mobile assignment id: "
                                + paramBean.getMobileAssignmentId()
                                + " sudah berubah (" + status + ").");
                throw new CoreException(ERROR_CHANGE);
            }

            this.ibatisSqlMap.update(this.STRING_SQL + "updRequest",
                    paramBean.getMobileAssignmentId());
            this.insertHistory(paramBean);

            this.logger.log(
                    ILogger.LEVEL_INFO,
                    "Requested Assignment: "
                            + paramBean.getMobileAssignmentId() + " by "
                            + paramBean.getUsrupd());
        } catch (CoreException me) {
            this.logger.printStackTrace(me);
            throw me;
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new CoreException("", sqle, ERROR_DATA_EXISTS, "");
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    private String getStatus(Integer mobileAssignmentId) throws SQLException {
        String result = null;
        result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL
                + "getStatus", mobileAssignmentId);
        return result;
    }

    private void insertHistory(MobileAssignmentHistoryBean paramBean)
            throws SQLException, Exception {
        boolean noSequence = "0".equals(ConfigurationProperties.getInstance()
                .get(PropertiesKey.DB_SEQUENCE));

        if (!noSequence) {
            SequenceManager sequenceManager = new SequenceManager();
            int historySeqno = sequenceManager
                    .getSequenceHistoryMobileAssignmentId();
            paramBean.setAssignmentHistorySeqno(new Integer(historySeqno));
        }
        this.ibatisSqlMap.update(this.STRING_SQL + "insertHistory", paramBean);
    }

    public InputAssignmentBean getCabangLogin(String branchId)
            throws CoreException {
        InputAssignmentBean result = null;
        try {
            result = (InputAssignmentBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getCabangLogin", branchId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    private void setResultDetailBean(ResultDetailBean resultDetailBean,
                                     Integer mobileAssignmentId, Integer questionGroupId,
                                     Integer questionId, String textAnswer) {
        resultDetailBean.setMobileAssignmentId(mobileAssignmentId);
        resultDetailBean.setQuestionGroupId(questionGroupId);
        resultDetailBean.setQuestionId(questionId);
        if (textAnswer == null || "".equals(textAnswer)) {
            resultDetailBean.setTextAnswer("-");
        } else {
            resultDetailBean.setTextAnswer(textAnswer);
        }
    }

    public String getRegId(String userid) throws CoreException {
        String result = null;
        try {
            result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL
                    + "getRegId", userid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
        }
        return result;
    }
}
