package treemas.application.feature.task.input;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.general.ComboManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.format.TreemasAddressFormatter;
import treemas.util.gcm.PostMan;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class InputAssignmentHandler extends FeatureActionBase {
    private ComboManager comboManager = new ComboManager();
    private PostMan postMan = new PostMan();
    private InputAssignmentManager manager = new InputAssignmentManager();
    private InputAssignmentValidator validator;

    public InputAssignmentHandler() {

        this.pageMap.put("Input", "edit");
        this.pageMap.put("SaveInput", "edit");
        this.pageMap.put("Request", "view");

        this.exceptionMap.put("Input", "edit");
        this.exceptionMap.put("SaveInput", "edit");
        this.exceptionMap.put("Request", "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.validator = new InputAssignmentValidator(request);
        InputAssignmentForm inputAssignmentForm = (InputAssignmentForm) form;
        inputAssignmentForm.getPaging().calculationPage();
        String action = inputAssignmentForm.getTask();
        this.loadStatus(request);
        if (Global.WEB_TASK_SHOW.equals(action)) {
            this.loadComboScheme(request);
            this.loadPriority(request);
            String userIdLogin = this.getLoginBean(request).getLoginId();
            this.loadLoginMultiBranch(request, userIdLogin);
        } else if (Global.WEB_TASK_LOAD.equals(action) ||
                Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadList(request, inputAssignmentForm);
        } else if (Global.WEB_TASK_ADD.equals(action)) {
            this.loadComboActiveSchemeForAdd(request);
            this.loadPriority(request);
            inputAssignmentForm.prepFormForEdit();
            String userIdLogin = this.getLoginBean(request).getLoginId();
            this.loadLoginMultiBranch(request, userIdLogin);
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.validator.validateAssignmentInsert(new ActionMessages(), inputAssignmentForm)) {
                String regex = "[0-9.]*";
                String latString = inputAssignmentForm.getGpsLatitude().replaceAll("-", "");
                String longString = inputAssignmentForm.getGpsLongitude().replaceAll("-", "");
                if (!Strings.isNullOrEmpty(latString) && !Strings.isNullOrEmpty(longString)) {
                    boolean latTrue = false;
                    boolean longTrue = false;
                    if (latString.matches(regex)) {
                        latTrue = true;
                    }
                    if (longString.matches(regex)) {
                        longTrue = true;
                    }

                    if (!(latTrue && longTrue)) {
                        throw new CoreException(this.manager.ERROR_GEOFENCE);
                    }
                } else {
                    Map gpsMap = this.googleConvert(inputAssignmentForm);
                    String lng = (String) gpsMap.get("lng");
                    String lat = (String) gpsMap.get("lat");
                    inputAssignmentForm.setGpsLatitude(lat);
                    inputAssignmentForm.setGpsLongitude(lng);
                }

                String address = this.generateAddress(inputAssignmentForm);
                inputAssignmentForm.setCustomerAddress(address);

                InputAssignmentBean inputAssignmentBean = new InputAssignmentBean();
                inputAssignmentForm.toBean(inputAssignmentBean);

                try {
                    this.manager.insertAssignment(inputAssignmentBean, this.getLoginId(request), inputAssignmentForm.getCustomerName());
                    this.setMessage(request, "common.success", null);

//					String regId = this.manager.getRegId(inputAssignmentBean.getSurveyorId());
//					if(!Strings.isNullOrEmpty(regId)){
//						Model model =  this.postMan.generateInput(regId, String.valueOf(inputAssignmentBean.getMobileAssignmentId()), this.getLoginBean(request).getFullName());
//						String apiKey = ConfigurationProperties.getInstance().get(PropertiesKey.API_KEY);
//						this.postMan.post(apiKey, model);
//					}
                    this.loadComboActiveSchemeForAdd(request);
                    this.loadPriority(request);
                    inputAssignmentForm.prepFormForEdit();
                    String userIdLogin = this.getLoginBean(request).getLoginId();
                    this.loadLoginMultiBranch(request, userIdLogin);
                } catch (CoreException me) {
                    this.loadComboActiveSchemeForAdd(request);
                    this.loadPriority(request);
                    String userIdLogin = this.getLoginBean(request).getLoginId();
                    this.loadLoginMultiBranch(request, userIdLogin);

                    throw me;
                }
            }
            this.loadList(request, inputAssignmentForm);
        } else if ("Input".equals(action)) {
            this.loadComboActiveSchemeForAdd(request);
            this.loadPriority(request);
            inputAssignmentForm.prepFormForEdit();
            String userIdLogin = this.getLoginBean(request).getLoginId();
            this.loadLoginMultiBranch(request, userIdLogin);
        } else if ("SaveInput".equals(action)) {
            String regex = "[0-9.]*";
            String latString = inputAssignmentForm.getGpsLatitude().replaceAll("-", "");
            String longString = inputAssignmentForm.getGpsLongitude().replaceAll("-", "");
            if (!Strings.isNullOrEmpty(latString) && !Strings.isNullOrEmpty(longString)) {
                boolean latTrue = false;
                boolean longTrue = false;
                if (latString.matches(regex)) {
                    latTrue = true;
                }
                if (longString.matches(regex)) {
                    longTrue = true;
                }

                if (!(latTrue && longTrue)) {
                    throw new CoreException(this.manager.ERROR_GEOFENCE);
                }
            } else {
                Map gpsMap = this.googleConvert(inputAssignmentForm);
                String lng = (String) gpsMap.get("lng");
                String lat = (String) gpsMap.get("lat");
                inputAssignmentForm.setGpsLatitude(lat);
                inputAssignmentForm.setGpsLongitude(lng);
            }

            String address = this.generateAddress(inputAssignmentForm);
            inputAssignmentForm.setCustomerAddress(address);

            InputAssignmentBean inputAssignmentBean = new InputAssignmentBean();
            inputAssignmentForm.toBean(inputAssignmentBean);
            try {
                this.manager.insertAssignment(inputAssignmentBean, this.getLoginId(request), inputAssignmentForm.getCustomerName());
                this.setMessage(request, "common.success", null);

//				String regId = this.manager.getRegId(inputAssignmentBean.getSurveyorId());
//				if(!Strings.isNullOrEmpty(regId)){
//					Model model =  this.postMan.generateInput(regId, String.valueOf(inputAssignmentBean.getMobileAssignmentId()), this.getLoginBean(request).getFullName());
//					String apiKey = ConfigurationProperties.getInstance().get(PropertiesKey.API_KEY);
//					this.postMan.post(apiKey, model);
//				}
                this.loadComboActiveSchemeForAdd(request);
                this.loadPriority(request);
                inputAssignmentForm.prepFormForEdit();
                inputAssignmentForm.setTask("Input");
                String userIdLogin = this.getLoginBean(request).getLoginId();
                this.loadLoginMultiBranch(request, userIdLogin);
            } catch (CoreException me) {
                this.loadComboActiveSchemeForAdd(request);
                this.loadPriority(request);
                String userIdLogin = this.getLoginBean(request).getLoginId();
                this.loadLoginMultiBranch(request, userIdLogin);

                throw me;
            }
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
            InputAssignmentBean inputAssignmentBean = this.manager.getSingleAssignment(inputAssignmentForm.getMobileAssignmentId());
            inputAssignmentForm.fromBean(inputAssignmentBean);
            inputAssignmentForm.getPaging().setDispatch(null);
            this.loadLoginMultiBranch(request, this.getLoginId(request));
            this.loadComboActiveSchemeForAdd(request);
            this.loadPriority(request);
        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
            if (this.validator.validateAssignmentEdit(new ActionMessages(), inputAssignmentForm)) {
                String address = this.generateAddress(inputAssignmentForm);
                inputAssignmentForm.setCustomerAddress(address);
                InputAssignmentBean inputAssignmentBean = new InputAssignmentBean();
                inputAssignmentForm.toBean(inputAssignmentBean);
                this.manager.updateAssignment(inputAssignmentBean, this.getLoginId(request), inputAssignmentForm.getMobileAssignmentId());
            }
            this.loadList(request, inputAssignmentForm);
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
        } else if ("Request".equals(action)) {
            String[] arrPilih = inputAssignmentForm.getPilih();
            try {
                this.manager.requestAssignment(arrPilih, this.getLoginId(request));
                this.setMessage(request, "common.success", null);
            } catch (CoreException me) {
                throw me;
            } finally {
                this.loadList(request, inputAssignmentForm);
            }
        }

        return this.getNextPage(action, mapping);
    }


    private void loadComboScheme(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboScheme(this.getLoginId(request));
        request.setAttribute("comboScheme", list);
    }

    private void loadComboActiveSchemeForAdd(HttpServletRequest request)
            throws CoreException {
//		List list = comboManager.getComboActiveScheme();
        List list = comboManager.getComboSchemeSurvey();
        request.setAttribute("comboActiveScheme", list);
    }

    private void loadPriority(HttpServletRequest request) throws CoreException {
        List list = comboManager.getComboPriority();
        request.setAttribute("comboPriority", list);
    }

    private void loadStatus(HttpServletRequest request) throws CoreException {
        List list = comboManager.getComboStatusSurvey();
        request.setAttribute("comboStatus", list);
    }

    private void loadBranch(HttpServletRequest request, String branchId) throws CoreException {
        List list = comboManager.getComboCabangLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadLoginMultiBranch(HttpServletRequest request, String userId)
            throws CoreException {
        List list = comboManager.getComboMultiCabangLogin(userId);
        request.setAttribute("comboBranch", list);
    }

    private void loadLoginBranch(HttpServletRequest request, InputAssignmentForm inputAssignmentForm, String branchId)
            throws CoreException {
        InputAssignmentBean bean = this.manager.getCabangLogin(branchId);
        inputAssignmentForm.setBranchId(bean.getBranchId());
        inputAssignmentForm.setBranchName(bean.getBranchName());
    }

    private String generateAddress(InputAssignmentForm form) {
        return TreemasAddressFormatter.generateAddress(form.getAddressStreet1(), form.getAddressStreet2(),
                form.getNoaddr(), form.getKelurahan(), form.getKecamatan(), form.getKabupaten(),
                form.getRt(), form.getRw(), form.getCity(), form.getKodePos(), form.getPropinsi(), form.getNegara());
    }

    private Map googleConvert(InputAssignmentForm form) {
        return TreemasAddressFormatter.googleConvert(this.logger, form.getAddressStreet1(), form.getAddressStreet2(),
                form.getNoaddr(), form.getKecamatan(), form.getKabupaten(),
                form.getCity(), form.getKodePos(), form.getPropinsi(), form.getNegara());
    }

    private void loadList(HttpServletRequest request, InputAssignmentForm inputAssignmentForm)
            throws CoreException {
        int targetPageNo = inputAssignmentForm.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(inputAssignmentForm.getTask())) {
            targetPageNo = 1;
        }
        inputAssignmentForm.getSearch().setUserId(this.getLoginId(request));
        PageListWrapper result = this.manager.getAllAssignment(targetPageNo,
                inputAssignmentForm.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, InputAssignmentForm.class/*, excConverterMap*/);
        inputAssignmentForm.getPaging().setCurrentPageNo(targetPageNo);
        inputAssignmentForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listAssignment", list);

        this.loadComboScheme(request);
        this.loadPriority(request);
        this.loadLoginMultiBranch(request, this.getLoginBean(request).getLoginId());
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();

        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case InputAssignmentManager.ERROR_GEOFENCE:
                Object[] o = new Object[1];
                o[0] = "GPS Location";
                try {
                    this.loadComboScheme(request);
                    this.loadPriority(request);
                    String userIdLogin = this.getLoginBean(request).getLoginId();
                    this.loadLoginMultiBranch(request, userIdLogin);
                    this.setMessage(request, "errors.mask", o);
                } catch (CoreException e) {
                    e.printStackTrace();
                }

                break;
            case InputAssignmentManager.ERROR_HAS_CHILD:
                this.setMessage(request, "common.haschild", null);
                break;

            case InputAssignmentManager.ERROR_CHANGE:
                this.setMessage(request, "common.concurrent", null);
                break;

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                try {
                    this.loadLoginMultiBranch(request, this.getLoginId(request));
                    this.loadComboActiveSchemeForAdd(request);
                    this.loadPriority(request);
                } catch (CoreException e) {
                    e.printStackTrace();
                    this.logger.printStackTrace(e);
                }
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((InputAssignmentForm) form).setTask(((InputAssignmentForm) form).resolvePreviousTask());
        String task = ((InputAssignmentForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
