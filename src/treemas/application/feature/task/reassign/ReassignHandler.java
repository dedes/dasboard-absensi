package treemas.application.feature.task.reassign;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.feature.general.ComboManager;
import treemas.application.feature.task.input.InputAssignmentManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.gcm.Model;
import treemas.util.gcm.PostMan;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.ConfigurationProperties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReassignHandler extends FeatureActionBase {
    private ReassignManager manager = new ReassignManager();
    private ComboManager comboManager = new ComboManager();
    private InputAssignmentManager assignmentManager = new InputAssignmentManager();
    private PostMan postMan = new PostMan();

    public ReassignHandler() {
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        ReassignForm reassignForm = (ReassignForm) form;
        reassignForm.getPaging().calculationPage();
        String action = reassignForm.getTask();

        if (Global.WEB_TASK_SHOW.equals(action)) {
//			this.loadLoginBranch(request, this.getLoginBean(request).getBranchId());
            this.loadLoginMultiBranch(request, this.getLoginBean(request).getLoginId());
            this.loadComboScheme(request);
            this.loadPriority(request);
        } else if (Global.WEB_TASK_LOAD.equals(action) ||
                Global.WEB_TASK_SEARCH.equals(action)) {
            reassignForm.getSearch().getSurveyorId();
            this.loadList(request, reassignForm);
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
            this.loadDetailList(request, reassignForm);
            this.loadPriority(request);
        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
            try {
                String[] keys = new String[reassignForm.getPilih().length];
                String[] surveyors = new String[reassignForm.getPilih().length];
                Map map = new HashMap();
                String tempSVY = "";
                List listData = new ArrayList();
                List listSvy = new ArrayList();

                for (int i = 0; i < reassignForm.getPilih().length; i++) {
                    String[] temp = reassignForm.getPilih()[i].split(";");
                    String msid = temp[0].trim();
                    String svy = temp[1].trim();
                    keys[i] = msid;
                    surveyors[i] = svy;

                    if (i == 0) {
                        tempSVY = svy;
                        listData.add(msid);
                    } else if (i == reassignForm.getPilih().length - 1) {
                        if (svy.equalsIgnoreCase(tempSVY)) {
                            listData.add(msid);
                            map.put(tempSVY, listData);
                            listSvy.add(tempSVY);
                        } else {
                            map.put(tempSVY, listData);
                            listSvy.add(tempSVY);
                            listData = new ArrayList();
                            tempSVY = msid;
                            listData.add(msid);
                            map.put(tempSVY, listData);
                            listSvy.add(tempSVY);
                        }
                    } else {
                        if (svy.equalsIgnoreCase(tempSVY)) {
                            listData.add(msid);
                        } else {
                            listSvy.add(tempSVY);
                            map.put(tempSVY, listData);
                            listSvy.add(tempSVY);
                            listData = new ArrayList();
                            tempSVY = msid;
                            listData.add(msid);
                        }
                    }

                }

                this.manager.update(reassignForm.getSurveyorId(),
                        reassignForm.getNotes(),
                        reassignForm.getPriorityId(),
                        keys, surveyors,
                        this.getLoginId(request));

                if (surveyors.length > 0) {

                    String apiKey = ConfigurationProperties.getInstance().get(PropertiesKey.API_KEY);
                    String regId = this.assignmentManager.getRegId(reassignForm.getSurveyorId());
                    if (!Strings.isNullOrEmpty(regId)) {
                        Model modelN = this.postMan.generateReassign(regId, keys, this.getLoginBean(request).getFullName());
                        this.postMan.post(apiKey, modelN);
                    }

                    for (Object data : listSvy) {
                        String surveyor = String.valueOf(data);
                        List list = (List) map.get(surveyor);
                        if (null != list) {
                            String[] arr = new String[list.size()];//id assignment
                            list.toArray(arr);
                            //infor user
                            String regIdOld = this.assignmentManager.getRegId(surveyor);
                            if (!Strings.isNullOrEmpty(regId)) {
                                Model modelL = this.postMan.generateDeleteReassignment(regId, arr, this.getLoginBean(request).getFullName());
                                this.postMan.post(apiKey, modelL);
                            }
                        }
                    }


                }

                this.setMessage(request, "common.success", null);
                this.loadList(request, reassignForm);
            } catch (CoreException me) {
                this.loadDetailList(request, reassignForm);
                throw me;
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadLoginBranch(HttpServletRequest request, String branchId)
            throws CoreException {
        List list = comboManager.getComboCabangLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadLoginMultiBranch(HttpServletRequest request, String userId)
            throws CoreException {
        List list = comboManager.getComboMultiCabangLogin(userId);
        request.setAttribute("comboBranch", list);
    }

    private void loadComboScheme(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboScheme(this.getLoginId(request)); //getComboScheme();
        request.setAttribute("comboScheme", list);
    }

    private void loadPriority(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboPriority();
        request.setAttribute("comboPriority", list);
    }

    private void loadList(HttpServletRequest request, ReassignForm reassignForm)
            throws CoreException {
        int targetPageNo = reassignForm.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(reassignForm.getTask())) {
            targetPageNo = 1;
        }
        reassignForm.getSearch().setUserId(this.getLoginId(request));
        PageListWrapper result = this.manager.getAllAssignment(targetPageNo,
                reassignForm.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, ReassignForm.class);
        reassignForm.getPaging().setCurrentPageNo(targetPageNo);
        reassignForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listAssignment", list);

//		this.loadLoginBranch(request, this.getLoginBean(request).getBranchId());
        this.loadLoginMultiBranch(request, this.getLoginBean(request).getLoginId());
        this.loadComboScheme(request);
        this.loadPriority(request);
    }

    private void loadDetailList(HttpServletRequest request, ReassignForm reassignForm)
            throws CoreException {
        String[] key = new String[reassignForm.getPilih().length];
        for (int i = 0; i < reassignForm.getPilih().length; i++) {
            String[] temp = reassignForm.getPilih()[i].split(";");
            key[i] = temp[0].trim();
        }

        List list = this.manager.getSelectedAssignment(key);
        if (list.size() > 0) {
            list = BeanConverter.convertBeansToString(list,
                    ReassignForm.class);
        }
        request.setAttribute("listAssignment", list);
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();

        switch (code) {
            case ReassignManager.ERROR_CHANGE:
                this.setMessage(request, "common.concurrent", null);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((ReassignForm) form).setTask(((ReassignForm) form).resolvePreviousTask());
        String task = ((ReassignForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
