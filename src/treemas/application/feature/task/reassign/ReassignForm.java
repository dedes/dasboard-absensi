package treemas.application.feature.task.reassign;

import treemas.base.feature.FeatureFormBase;

public class ReassignForm extends FeatureFormBase {
    private ReassignSearch search = new ReassignSearch();

    private String mobileAssignmentId;
    private String surveyorId;
    private String surveyorName;
    private String status;
    private String statusDescription;
    private String customerName;
    private String customerAddress;
    private String customerPhone;
    private String schemeId;
    private String schemeDescription;
    private String priorityId;
    private String priorityDescription;
    private String rfaDate;
    private String assignmentDate;
    private String submitDate;
    private String retrieveDate;
    private String notes;
    private String branchId;
    private String branchName;

    private String[] pilih;

    public ReassignSearch getSearch() {
        return search;
    }

    public void setSearch(ReassignSearch search) {
        this.search = search;
    }

    public String getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(String mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public String getSurveyorId() {
        return surveyorId;
    }

    public void setSurveyorId(String surveyorId) {
        this.surveyorId = surveyorId;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    public String getPriorityDescription() {
        return priorityDescription;
    }

    public void setPriorityDescription(String priorityDescription) {
        this.priorityDescription = priorityDescription;
    }

    public String getRfaDate() {
        return rfaDate;
    }

    public void setRfaDate(String rfaDate) {
        this.rfaDate = rfaDate;
    }

    public String getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(String assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getRetrieveDate() {
        return retrieveDate;
    }

    public void setRetrieveDate(String retrieveDate) {
        this.retrieveDate = retrieveDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String[] getPilih() {
        return pilih;
    }

    public void setPilih(String[] pilih) {
        this.pilih = pilih;
    }

    public String getUrlPilih() {
        String temp = null;
        if (pilih != null && pilih.length > 0) {
            int len = this.pilih.length;

            if (len > 0) {
                StringBuffer sb = new StringBuffer();
                sb.append("&pilih=");
                for (int i = 0; i < len; i++) {
                    String[] temp2 = this.pilih[i].split(";");
                    sb.append(temp2[1].trim());
                    if (i < len - 1) {
                        sb.append(";");
                    }
                }
                temp = sb.toString();
            }
        } else {
            temp = "";
        }
        return temp;
    }
}
