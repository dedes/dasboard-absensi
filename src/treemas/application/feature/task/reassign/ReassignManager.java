package treemas.application.feature.task.reassign;

import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.Tools;

import java.sql.SQLException;
import java.util.List;


public class ReassignManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_REASSIGNMENT;

    public PageListWrapper getAllAssignment(int numberPage,
                                            ReassignSearch searchParam) throws CoreException {
        PageListWrapper result = new PageListWrapper();

        searchParam.setPage(numberPage);
        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", searchParam);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", searchParam);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public List getSelectedAssignment(String[] selectedAssignment)
            throws CoreException {
        List result = null;
        String sqlKeys = "('" + Tools.implode(selectedAssignment, "','") + "')";
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "selected", sqlKeys);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return result;
    }

    public void update(String toSurveyor, String notes, String priorityId, String[] keys,
                       String[] lastSurveyors, String loginId) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.getSurvey(toSurveyor, notes, priorityId, keys, lastSurveyors, loginId);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (CoreException ce) {
            throw ce;
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            throw new CoreException(se, ERROR_DB);
        }
    }

    private void getSurvey(String toSurveyor, String notes, String priorityId, String[] keys,
                           String[] lastSurveyors, String loginId) throws CoreException {
        for (int i = 0; i < keys.length; i++) {
            String mobileAssignmentId = keys[i];
            String lastSurveyor = lastSurveyors[i];
            this.doUpdate(toSurveyor, notes, priorityId, mobileAssignmentId, lastSurveyor, loginId);
        }
    }

    private void doUpdate(String toSurveyor, String notes, String priorityId, String mobileAssignmentId,
                          String lastSurveyor, String loginId) throws CoreException {
        try {
            this.logger.log(ILogger.LEVEL_INFO, "Updating reassign ...loginId="
                    + loginId + " survey id: " + mobileAssignmentId);

            String surveyor = this.getSurveyor(mobileAssignmentId);
            if (!surveyor.equals(lastSurveyor)) {
                this.logger.log(ILogger.LEVEL_INFO,
                        "Staff Appraisal di Task realokasi telah berubah...loginId= "
                                + loginId + " appSeqNo " + mobileAssignmentId);
                throw new CoreException(ERROR_CHANGE);
            }

            ReassignBean paramBean = new ReassignBean();
            paramBean.setMobileAssignmentId(Integer.valueOf(mobileAssignmentId));
            paramBean.setSurveyorId(toSurveyor);
            paramBean.setNotes(notes);
            paramBean.setPriorityId(priorityId);

            this.ibatisSqlMap.update(this.STRING_SQL + "update", paramBean);

            this.logger.log(ILogger.LEVEL_INFO, "Updated reassign ...loginId="
                    + loginId + " survey id: " + mobileAssignmentId);
        } catch (CoreException ce) {
            throw ce;
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            throw new CoreException(se, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
    }

    private String getSurveyor(String mobileAssignmentId) throws SQLException {
        Integer maId = new Integer(mobileAssignmentId);
        return (String) this.ibatisSqlMap.queryForObject(
                this.STRING_SQL + "getSurveyor", maId);
    }
}
