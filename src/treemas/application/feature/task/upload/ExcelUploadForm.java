package treemas.application.feature.task.upload;

import org.apache.struts.upload.FormFile;
import treemas.base.feature.FeatureFormBase;


public class ExcelUploadForm extends FeatureFormBase {
    FormFile excelFile;
    private String schemeId;

    public FormFile getExcelFile() {
        return excelFile;
    }

    public void setExcelFile(FormFile excelFile) {
        this.excelFile = excelFile;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }
}
