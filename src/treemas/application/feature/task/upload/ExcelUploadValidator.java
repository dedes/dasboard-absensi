package treemas.application.feature.task.upload;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class ExcelUploadValidator extends Validator {

    public ExcelUploadValidator(HttpServletRequest request) {
        super(request);
    }

    public ExcelUploadValidator(Locale locale) {
        super(locale);
    }

    boolean getMaxLength(ActionMessages messages, int dataLength, int row, String excelColumn, int maxLength, String path) {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        if (dataLength > maxLength) {
            messages.add("", new ActionMessage("upload.errorexcel.max.length", new Object[]{row, excelColumn, maxLength, path}));

            ret = false;
        }
        return ret;
    }

    boolean getRequiredData(ActionMessages messages, int dataLength, int row, String excelColumn, String path) {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        if (dataLength < 1) {
            messages.add("", new ActionMessage("upload.errorexcel.data.required", new Object[]{row, excelColumn, 1, path}));
            ret = false;
        }
        return ret;
    }

    boolean getNumericType(ActionMessages messages, int dataLength, String data, int row, String excelColumn, String path) {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        int i = 0;
        int intParse = 0;
        if (dataLength > 0) {
            i = 0;
            while (i < dataLength) {
                try {
                    intParse = Integer.parseInt(data.substring(i, i + 1));
                    i++;
                } catch (Exception e) {
                    messages.add("", new ActionMessage("upload.errorexcel.data.numeric", new Object[]{row, excelColumn, path}));
                    ret = false;
                    i = dataLength;
                }
            }
        }
        return ret;
    }

    boolean getLatitudeType(ActionMessages messages, int dataLength, Double data, int row, String excelColumn, String path) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.latitudeLongitudeValidator(messages, data.toString(), path, "", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_LATITUDE);
        if (messages != null && !messages.isEmpty()) {
            messages.clear();
            messages.add("", new ActionMessage("upload.errorexcel.latitude.data", new Object[]{row, excelColumn, path}));
            ret = false;
        }

        return ret;
    }

    boolean getLongitudeType(ActionMessages messages, int dataLength, Double data, int row, String excelColumn, String path) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.latitudeLongitudeValidator(messages, data.toString(), path, "", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_LONGITUDE);
        if (messages != null && !messages.isEmpty()) {
            messages.clear();
            messages.add("", new ActionMessage("upload.errorexcel.longitude.data", new Object[]{row, excelColumn, path}));
            ret = false;
        }

        return ret;
    }

    public boolean validateSchemeId(ActionMessages messages, ExcelUploadForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.comboValidator(messages, form.getSchemeId(), "1", "20", "app.privilege.form", "schemeId", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
        return ret;

    }
}	
