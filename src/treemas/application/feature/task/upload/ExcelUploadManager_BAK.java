package treemas.application.feature.task.upload;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.struts.upload.FormFile;
import treemas.application.feature.task.AssignmentBean;
import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ExcelUploadManager_BAK extends FeatureManagerBase {
    static final int ERROR_INVALID_SPREADSHEET = 1;
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_ASSIGNMENT_UPLOAD;
    private static final int NUMBER_OF_CELL_IN_EXCEL = 7;
    private static final String ERROR_SPREADSHEET = "Invalid spreadsheet";
    private static final int NUMBER_ROW_IN_VALIDATION = 1000;
    private static final int SURVEYOR_COLUMN = 3;
    private static final int FORM_COLUMN = 4;
    private static final int PRIORITY_COLUMN = 5;
    private static final String[] TEMPLATE_HEADER = {"Customer Name", "Customer Address", "Customer Phone", "Surveyor", "Form", "Priority", "Notes"};
    private static final int[] HEADER_COLUMN_WIDTH = {30 * 256, 50 * 256, 20 * 256, 30 * 256, 20 * 256, 10 * 256, 50 * 256};
    private SequenceManager sequenceManager = new SequenceManager();

    public void processSpreadSheetFile(FormFile uploadedFile, String clientAddress) throws CoreException {
        this.logger.log(ILogger.LEVEL_INFO, "Processing uploaded spreadsheet file: " +
                uploadedFile.getFileName() + " from " + clientAddress);
        try {
            List listofAssignment = this.parseSpreadsheetToAssignmentBeans(uploadedFile);

            try {
                this.ibatisSqlMap.startTransaction();

                for (Iterator iterator = listofAssignment.iterator(); iterator
                        .hasNext(); ) {
                    AssignmentBean bean = (AssignmentBean) iterator.next();
                    Integer mobileAssignmentId = new Integer(sequenceManager.getSequenceMobileAssignmentId());
                    bean.setMobileAssignmentId(mobileAssignmentId);
                    this.saveAssignment(bean);
                }

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (IOException ioe) {
            this.logger.printStackTrace(ioe);
            String message = ioe.getMessage();
            if (ERROR_SPREADSHEET.equals(message))
                throw new CoreException(ERROR_INVALID_SPREADSHEET, uploadedFile);
            else
                throw new CoreException(ioe, ERROR_UNKNOWN);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
    }

    private List parseSpreadsheetToAssignmentBeans(FormFile uploadedFile) throws IOException {
        List result = new ArrayList();

        HSSFWorkbook wb = new HSSFWorkbook(uploadedFile.getInputStream());

        if (wb.getNumberOfSheets() < 1)
            this.logger.log(ILogger.LEVEL_INFO, "No sheet found");

        for (int k = 0; k < 1; k++) {
            HSSFSheet sheet = wb.getSheetAt(k);
            int rows = sheet.getPhysicalNumberOfRows();

            this.logger.log(ILogger.LEVEL_INFO, "Sheet " + k + " \"" + wb.getSheetName(k) + "\" has " + rows
                    + " row(s).");

            for (int r = 1; r < rows; r++) {
                HSSFRow row = sheet.getRow(r);
                if (row == null) {
                    continue;
                }

                int cells = row.getPhysicalNumberOfCells();

                if (cells != NUMBER_OF_CELL_IN_EXCEL)
                    throw new IOException(ERROR_SPREADSHEET);

                AssignmentBean bean = new AssignmentBean();
                for (int c = 0; c < cells; c++) {
                    HSSFCell cell = row.getCell(c);
                    String value = null;

                    switch (cell.getCellType()) {
                        case HSSFCell.CELL_TYPE_NUMERIC:
                            value = "" + cell.getNumericCellValue();
                            break;

                        case HSSFCell.CELL_TYPE_STRING:
                            value = "" + cell.getStringCellValue();
                            break;

                        default:
                    }

                    switch (c) {
                        case SURVEYOR_COLUMN:
                        case FORM_COLUMN:
                        case PRIORITY_COLUMN:
                            String[] temp = value.split("-");
                            value = temp[0].trim();
                            break;
                    }

                    switch (c) {
                        case 0:
                            bean.setCustomerName(value);
                            break;
                        case 1:
                            bean.setCustomerAddress(value);
                            break;
                        case 2:
                            bean.setCustomerPhone(value);
                            break;
                        case 3:
                            bean.setSurveyorId(value);
                            break;
                        case 4:
                            bean.setSchemeId(value);
                            break;
                        case 5:
                            bean.setPriorityId(value);
                            break;
                        case 6:
                            bean.setNotes(value);
                            break;
                    }
                }
                result.add(bean);
            }
        }

        return result;
    }

    private void saveAssignment(AssignmentBean bean) throws SQLException {
        this.ibatisSqlMap.insert(this.STRING_SQL + "insert", bean);
    }

    public HSSFWorkbook createXlsTemplate(String branchId) throws CoreException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        try {
            HSSFSheet sheet = workbook.createSheet("Add Survey");
            this.createHeader(workbook, sheet);
            this.setTextDataFormat(workbook, sheet);

            HSSFDataValidation surveyorValidation = this.createSurveyorValidation(workbook, branchId);
            HSSFDataValidation formValidation = this.createFormValidation(workbook);
            HSSFDataValidation priorityValidation = this.createPriorityValidation(workbook);

            sheet.addValidationData(surveyorValidation);
            sheet.addValidationData(formValidation);
            sheet.addValidationData(priorityValidation);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return workbook;
    }

    private void createHeader(HSSFWorkbook workbook, HSSFSheet sheet) {
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBoldweight((short) 1000);
        style.setFont(font);

        HSSFRow row = sheet.createRow(0);

        for (int i = 0; i < TEMPLATE_HEADER.length; i++) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue(TEMPLATE_HEADER[i]);
            cell.setCellStyle(style);
            sheet.setColumnWidth(i, HEADER_COLUMN_WIDTH[i]);
        }
    }

    private void setTextDataFormat(HSSFWorkbook workbook, HSSFSheet sheet) {
        DataFormat format = workbook.createDataFormat();
        HSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(format.getFormat("@"));

        for (int i = 0; i < TEMPLATE_HEADER.length; i++) {
            sheet.setDefaultColumnStyle(i, style);
        }
    }

    private HSSFDataValidation createPriorityValidation(HSSFWorkbook workbook) throws SQLException {
        CellRangeAddressList addressList = new CellRangeAddressList(1, NUMBER_ROW_IN_VALIDATION, PRIORITY_COLUMN, PRIORITY_COLUMN);
        DVConstraint dvConstraint = DVConstraint
                .createExplicitListConstraint(this.getPriority());
        HSSFDataValidation dataValidation = new HSSFDataValidation(addressList,
                dvConstraint);
        dataValidation.setSuppressDropDownArrow(false);
        return dataValidation;
    }

    private HSSFDataValidation createFormValidation(HSSFWorkbook workbook) throws SQLException {
        CellRangeAddressList addressList = new CellRangeAddressList(1, NUMBER_ROW_IN_VALIDATION, FORM_COLUMN, FORM_COLUMN);

        HSSFSheet sheet = workbook.createSheet("DataForm");

        List listForm = this.getForm();
        int size = listForm.size();

        for (int i = 0; i < size; i++) {
            HSSFRow row = sheet.createRow(i);
            HSSFCell cell = row.createCell(0);

            String form = (String) listForm.get(i);
            cell.setCellValue(form);
            CellStyle style = workbook.createCellStyle();
            style.setLocked(true);
            cell.setCellStyle(style);
        }

        sheet.protectSheet("");

        DVConstraint dvConstraint = DVConstraint
                .createFormulaListConstraint("'DataForm'!$A$1:$A$" + size);
        HSSFDataValidation dataValidation = new HSSFDataValidation(addressList,
                dvConstraint);
        dataValidation.setSuppressDropDownArrow(false);
        dataValidation.setEmptyCellAllowed(false);
        return dataValidation;
    }

    private HSSFDataValidation createSurveyorValidation(HSSFWorkbook workbook, String branchId) throws SQLException {
        CellRangeAddressList addressList = new CellRangeAddressList(1, NUMBER_ROW_IN_VALIDATION, SURVEYOR_COLUMN, SURVEYOR_COLUMN);

        HSSFSheet sheet = workbook.createSheet("DataSurveyor");

        List listSurveyor = this.getSurveyorByCabang(branchId);
        int size = listSurveyor.size();

        for (int i = 0; i < size; i++) {
            HSSFRow row = sheet.createRow(i);
            HSSFCell cell = row.createCell(0);

            String surveyor = (String) listSurveyor.get(i);
            cell.setCellValue(surveyor);
            CellStyle style = workbook.createCellStyle();
            style.setLocked(true);
            cell.setCellStyle(style);
        }

        sheet.protectSheet("");

        DVConstraint dvConstraint = DVConstraint
                .createFormulaListConstraint("'DataSurveyor'!$A$1:$A$" + size);
        HSSFDataValidation dataValidation = new HSSFDataValidation(addressList,
                dvConstraint);
        dataValidation.setSuppressDropDownArrow(false);
        dataValidation.setEmptyCellAllowed(false);
        return dataValidation;
    }

    private List getSurveyorByCabang(String branchId) throws SQLException {
//		List cabangList = this.ibatisSqlMap.queryForList(this.STRING_SQL+"getCabang", branchId);
//		String[] cabangArray = new String[cabangList.size()];
//		cabangList.toArray(cabangArray);
//		String cabangDelimited = Tool.implode(cabangArray, ",");
//		
//		this.logger.log(ILogger.LEVEL_DEBUG_HIGH, "Parameter Cabang getSurveyor: " + cabangDelimited);
        List surveyorList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getSurveyor", branchId /*cabangDelimited*/);

        return surveyorList;
    }

    private List getForm() throws SQLException {
        List formList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getScheme", null);
        return formList;
    }

    private String[] getPriority() throws SQLException {
        List priorityList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getPriority", null);

        String[] priorityArray = new String[priorityList.size()];
        priorityList.toArray(priorityArray);

        return priorityArray;
    }
}
