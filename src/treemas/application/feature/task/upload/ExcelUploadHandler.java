package treemas.application.feature.task.upload;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.general.ComboManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.struts.ApplicationResources;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class ExcelUploadHandler extends FeatureActionBase {
    private static ApplicationResources resources = ApplicationResources.getInstance();
    private final String screenId = "";

    private ExcelUploadManager manager;
    private ComboManager comboManager = new ComboManager();
    private ExcelUploadValidator validator;

    public ExcelUploadHandler() {
        this.pageMap.put("Show", "view");
        this.pageMap.put("SaveExcel", "view");
        this.pageMap.put("Download", "view");

        this.exceptionMap.put("Show", "view");
        this.exceptionMap.put("SaveExcel", "view");
        this.exceptionMap.put("Download", "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.manager = new ExcelUploadManager(request);
        this.validator = new ExcelUploadValidator(request);
        ExcelUploadForm excelUploadForm = (ExcelUploadForm) form;
        String action = excelUploadForm.getTask();
        String schemeId = excelUploadForm.getSchemeId();
        this.loadComboScheme(request);

        if (Global.WEB_TASK_SHOW.equals(action) || Global.WEB_TASK_LOAD.equals(action)) {
        } else if ("SaveExcel".equals(action)) {
            String clientAddress = request.getRemoteAddr();
            String branchId = this.getLoginBean(request).getBranchId();
            String userUpload = this.getLoginBean(request).getUserid();
            manager.processSpreadSheetFile(excelUploadForm.getExcelFile(), clientAddress, branchId, userUpload);
            this.setMessage(request, "common.success", null);
        } else if ("Download".equals(action)) {
            if (this.validator.validateSchemeId(new ActionMessages(), excelUploadForm)) {
                String branchId = this.getLoginBean(request).getBranchId();
                HSSFWorkbook workbook = this.manager.createXlsTemplate(branchId, schemeId);
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                Date now = new Date();
                String strDate = df.format(now);

                df.format(now);
                String fileName = resources.getMessage("upload.survey.file.name", new Object[]{strDate});
                response.setContentType("application/vnd.ms-excel");
                response.setHeader("content-disposition", "attachment; filename=" + fileName);
                OutputStream out = null;
                try {
                    try {
                        out = response.getOutputStream();
                        workbook.write(out);
                    } finally {
                        if (out != null) {
                            out.flush();
                            out.close();
                        }
                    }
                } catch (IOException e) {
                    this.logger.printStackTrace(e);
                    throw new CoreException(e, ExcelUploadManager.ERROR_UNKNOWN);
                }
                return null;
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadComboScheme(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboSchemeSurvey();
        request.setAttribute("comboScheme", list);
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;

        this.manager = new ExcelUploadManager(request);

        switch (code) {
            case ExcelUploadManager.ERROR_INVALID_SPREADSHEET:
                this.setMessage(request, "upload.errorexcel", null);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadManager.ERROR_NULL_DATA_EXCEL:
                this.setMessage(request, "upload.errorexcel.nodata", null);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadManager.ERROR_LAT_LONG_DATA:
                this.setMessage(request, "upload.errorexcel.latlong.data", null);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadManager.ERROR_MAX_DATA_LENGTH:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadManager.ERROR_TYPE_DATA_NUMERIC:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadManager.ERROR_REQUIRED_DATA:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadManager.ERROR_MAX_TYPE_DATA_LAT_LONG:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((ExcelUploadForm) form).setTask(((ExcelUploadForm) form).resolvePreviousTask());
        String task = ((ExcelUploadForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
