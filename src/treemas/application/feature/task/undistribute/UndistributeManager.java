package treemas.application.feature.task.undistribute;

import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.CoreException;
import treemas.util.constant.PropertiesKey;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UndistributeManager extends FeatureManagerBase {
    static final int SEARCH_BY_CUSTOMER_NAME = 10;
    static final int SEARCH_BY_SURVEYOR_NAME = 20;
    static final int ERROR_WEB_SERVICE = 500;

    private static final String PENDING = "N";

    public PageListWrapper getAllAssignment(int numberPage,
                                            UndistributeSearch searchParam) throws CoreException {
        PageListWrapper result = new PageListWrapper();

        int searchType = searchParam.getSearchType1();
        String searchValue = searchParam.getSearchValue1();
        if (searchType != SEARCH_BY_CUSTOMER_NAME &&
                searchType != SEARCH_BY_SURVEYOR_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: " +
                    searchType);

        searchValue = searchValue != null ? searchValue.trim() : searchValue;
        searchParam.setPage(numberPage);
        try {
            List list = this.ibatisSqlMap.queryForList(
                    "mss.survey.undistribute.getAll", searchParam);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    "mss.survey.undistribute.cntAll", searchParam);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public void deleteAssignment(String mobileAssignmentId, String loginId, String screenId)
            throws CoreException {
        UndistributeBean bean = new UndistributeBean();
        Integer surveyId = new Integer(mobileAssignmentId);
        bean.setMobileAssignmentId(surveyId);
        try {
            try {
                this.ibatisSqlMap.startTransaction();

                String status = this.getStatus(surveyId);
                if (!PENDING.equals(status)) {
                    this.logger.log(ILogger.LEVEL_INFO,
                            "Status mobile assignment id: " + mobileAssignmentId +
                                    " sudah berubah (" + status + ").");
                    throw new CoreException(ERROR_CHANGE);
                }

                this.logger.log(ILogger.LEVEL_INFO, "Undistributing Assignment " + mobileAssignmentId);
                this.ibatisSqlMap.update("mss.survey.undistribute.undistribute", surveyId);
                //this.ibatisSqlMap.update("mss.survey.undistribute.cancelDKHC", surveyId);
                Map paramMap = new HashMap();
                boolean noSequence = "0".equals(ConfigurationProperties.getInstance().
                        get(PropertiesKey.DB_SEQUENCE));
                if (!noSequence) {
                    SequenceManager sequenceManager = new SequenceManager();
                    int historySeqno = sequenceManager.getSequenceHistoryMobileAssignmentId();
                    paramMap.put("assignmentHistorySeqno", new Integer(historySeqno));
                }
                paramMap.put("mobileAssignmentId", surveyId);
                paramMap.put("usrupd", loginId);
                this.ibatisSqlMap.insert("mss.survey.undistribute.undistributeHistory", paramMap);

                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Undistributed Assignment "
                        + mobileAssignmentId);
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (CoreException me) {
            throw me;
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    private String getStatus(Integer mobileAssignmentId) throws SQLException {
        String result = null;
        result = (String) this.ibatisSqlMap.queryForObject(
                "mss.survey.inputAssignment.getStatus", mobileAssignmentId);
        return result;
    }

    public UndistributeBean getCabangLogin(String branchId) throws CoreException {
        UndistributeBean result = null;
        try {
            result = (UndistributeBean) this.ibatisSqlMap.queryForObject(
                    "mss.survey.inputAssignment.getCabangLogin", branchId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }
}
