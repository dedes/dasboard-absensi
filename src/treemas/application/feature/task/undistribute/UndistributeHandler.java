package treemas.application.feature.task.undistribute;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.feature.general.ComboManager;
import treemas.application.feature.task.input.InputAssignmentManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.format.TreemasDateFormatter;
import treemas.util.gcm.Model;
import treemas.util.gcm.PostMan;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.PagingInfo;
import treemas.util.tool.ConfigurationProperties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UndistributeHandler extends FeatureActionBase {
    private final String screenId = "";
    private ComboManager comboManager = new ComboManager();
    private Map excConverterMap = new HashMap();
    private UndistributeManager manager = new UndistributeManager();
    private InputAssignmentManager assignmentManager = new InputAssignmentManager();
    private PostMan postMan = new PostMan();

    public UndistributeHandler() {
        TreemasDateFormatter convertDMYTime = TreemasDateFormatter.getInstance();
        this.excConverterMap.put("assignmentDate", convertDMYTime);
        this.excConverterMap.put("retrieveDate", convertDMYTime);
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        UndistributeForm undistributeForm = (UndistributeForm) form;
        undistributeForm.getPaging().calculationPage();
        String action = undistributeForm.getTask();

        if (Global.WEB_TASK_SHOW.equals(action)) {
            this.loadComboUndistScheme(request);
            this.loadPriority(request);
            String userIdLogin = this.getLoginBean(request).getLoginId();
            this.loadMultiBranch(request, userIdLogin);
        } else if (Global.WEB_TASK_LOAD.equals(action) ||
                Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadList(request, undistributeForm);
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = undistributeForm.getPaging();
            try {
                this.manager.deleteAssignment(undistributeForm.getMobileAssignmentId(),
                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }

                String regId = this.assignmentManager.getRegId(undistributeForm.getSurveyorId());
                if (!Strings.isNullOrEmpty(regId)) {
                    Model model = this.postMan.generateDelete(regId, undistributeForm.getMobileAssignmentId(), this.getLoginBean(request).getFullName());
                    String apiKey = ConfigurationProperties.getInstance().get(PropertiesKey.API_KEY);
                    this.postMan.post(apiKey, model);
                }

            } finally {
                this.loadList(request, undistributeForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

//	private void loadComboScheme (HttpServletRequest request)
//			throws CoreException {
//		List list = comboManager.getComboScheme(this.getLoginId(request));
//		request.setAttribute("comboScheme", list);
//	}

    private void loadComboUndistScheme(HttpServletRequest request)
            throws CoreException {
//		List list = comboManager.getComboUndistScheme(this.getLoginId(request));
//		request.setAttribute("comboScheme", list);
        List list = comboManager.getComboScheme(this.getLoginId(request)); //getComboScheme();
        request.setAttribute("comboScheme", list);
    }

    private void loadPriority(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboPriority();
        request.setAttribute("comboPriority", list);
    }

    private void loadBranch(HttpServletRequest request, String branchId)
            throws CoreException {
        List list = comboManager.getComboCabangByLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadMultiBranch(HttpServletRequest request, String userId)
            throws CoreException {
//		List list = comboManager.getComboMultiCabangByLogin(userId);
//		request.setAttribute("comboBranch", list);
        List list = comboManager.getComboMultiCabangLogin(userId);
        request.setAttribute("comboBranch", list);
    }

    private void loadList(HttpServletRequest request, UndistributeForm undistributeForm)
            throws CoreException {
        int targetPageNo = undistributeForm.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(undistributeForm.getTask())) {
            targetPageNo = 1;
        }
        undistributeForm.getSearch().setUserId(this.getLoginId(request));
        PageListWrapper result = this.manager.getAllAssignment(targetPageNo,
                undistributeForm.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, UndistributeForm.class, excConverterMap);
        undistributeForm.getPaging().setCurrentPageNo(targetPageNo);
        undistributeForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listAssignment", list);

        this.loadComboUndistScheme(request);
        this.loadPriority(request);
//		this.loadBranch(request, this.getLoginBean(request).getBranchId());
        this.loadMultiBranch(request, this.getLoginBean(request).getLoginId());
    }

    ;

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();

        switch (code) {
            case UndistributeManager.ERROR_CHANGE:
                this.setMessage(request, "common.concurrent", null);
                break;

            case UndistributeManager.ERROR_WEB_SERVICE:
                this.setMessage(request, "common.webservice", null);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((UndistributeForm) form).setTask(((UndistributeForm) form).resolvePreviousTask());
        String task = ((UndistributeForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
