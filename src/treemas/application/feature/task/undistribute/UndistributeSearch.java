package treemas.application.feature.task.undistribute;

import treemas.util.ibatis.SearchFormParameter;

public class UndistributeSearch extends SearchFormParameter {
    private String scheme;
    private String priority;
    private String branchId;
    private String userId;

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
