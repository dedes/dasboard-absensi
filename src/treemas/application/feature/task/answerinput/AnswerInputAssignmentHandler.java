package treemas.application.feature.task.answerinput;

import com.google.gson.GsonBuilder;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.general.ComboManager;
import treemas.application.feature.task.answerinput.list.FeatQuestionGroup;
import treemas.application.feature.task.input.InputAssignmentManager;
import treemas.application.user.UserManager;
import treemas.base.feature.FeatureActionBase;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.format.TreemasAddressFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class AnswerInputAssignmentHandler extends FeatureActionBase {

    private AnswerInputAssignmentManager manager = new AnswerInputAssignmentManager();
    private ComboManager comboManager = new ComboManager();
    private UserManager managerUser = new UserManager();
    private MapManager mapManager = new MapManager();
    private AnswerInputAssignmentValidator validator;

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        AnswerInputAssignmentForm frm = (AnswerInputAssignmentForm) form;
        this.validator = new AnswerInputAssignmentValidator(request);
        String task = frm.getTask();
        frm.getPaging().calculationPage();
        SessionBean loginBean = this.getLoginBean(request);
        String userIdLogin = loginBean.getLoginId();
        String branchIdLogin = loginBean.getBranchId();

        if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)
                || Global.WEB_TASK_VIEW.equals(task)) {
            this.loadComboScheme(request);
            this.loadComboActiveSchemeForAdd(request);
            this.loadPriority(request);
            this.loadStatus(request);
            request.removeAttribute("mapper");
        } else if (Global.WEB_TASK_ADD.equals(task)) {
            //next to answer
            Integer seq = this.manager.getNextSequence();
            frm.setSeq_id(seq.toString());
            frm.setNextGroup("1");
            this.loadSeparatedGroup(request, frm);
        } else if (Global.WEB_TASK_BACK_ADD.equals(task)) {
            //next to answer
            this.loadSeparatedGroup(request, frm);
            this.cloneMap(request, frm.getJsonAnswer());
            this.mapManager.save(this.mapManager.putJsonToMap(frm.getJsonAnswer()), this.getLoginId(request), this.manager, Integer.parseInt(frm.getSeq_id()));
        } else if (Global.WEB_TASK_NEXT_ADD.equals(task)) {
            //next to answer
            this.loadSeparatedGroup(request, frm);
            this.cloneMap(request, frm.getJsonAnswer());
            this.mapManager.save(this.mapManager.putJsonToMap(frm.getJsonAnswer()), this.getLoginId(request), this.manager, Integer.parseInt(frm.getSeq_id()));
        } else if (Global.WEB_TASK_INSERT.equals(task)) {
            //save header answer
            this.mapManager.save(this.mapManager.putJsonToMap(frm.getJsonAnswer()), this.getLoginId(request), this.manager, Integer.parseInt(frm.getSeq_id()));
        }

        return this.getNextPage(task, mapping);

    }

    private void cloneMap(HttpServletRequest request, String json) {
//		if(!Strings.isNullOrEmpty(json)){
//			Map mapReq = (Map) request.getSession().getAttribute("mapper");
//			if(null==mapReq)
//				mapReq = ne w HashMap();
//			Map map = this.mapManager.putJsonToMap(json);
//			this.mapManager.addNewMapToMap(mapReq, map);
//			request.getSession().removeAttribute("mapper");
//			request.getSession().setAttribute("mapper", mapReq);
//		}
    }

    private void loadComboScheme(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboScheme(this.getLoginId(request));
        request.setAttribute("comboScheme", list);
    }

    private void loadComboActiveSchemeForAdd(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboSchemeSurvey();
        request.setAttribute("comboActiveScheme", list);
    }

    private void loadStatus(HttpServletRequest request) throws CoreException {
        List list = comboManager.getComboStatusSurvey();
        request.setAttribute("comboStatus", list);
    }

    private void loadPriority(HttpServletRequest request) throws CoreException {
        List list = comboManager.getComboPriority();
        request.setAttribute("comboPriority", list);
    }

    private void loadSeparatedGroup(HttpServletRequest request, AnswerInputAssignmentForm form) throws CoreException {
        Integer group = Integer.parseInt(form.getNextGroup());
        FeatQuestionGroup fqg = this.manager.getSeparatedGroup(form.getSchemeId(), group);
        Map map = this.manager.getSeparatedAnswer(form.getSchemeId(), group, fqg.getQuestionGroupId(), form.getSeq_id(), this.getLoginId(request), form.getTask(), this.mapManager);
        form.setFeatQuestionGroup(fqg);
        form.setNextGroup(String.valueOf(group + 1));

        request.setAttribute("mapper", map);
    }

    private void loadQuestion(HttpServletRequest request, String schemeId)
            throws CoreException {
        List list = this.manager.getListQuestionSet(schemeId);
        GsonBuilder gson = new GsonBuilder();
        String json = gson.create().toJson(list, FeatQuestionGroup.class);
        request.setAttribute("jsonQuestion", json);
        request.setAttribute("listQuestionSet", list);
    }


    private String generateAddress(AnswerInputAssignmentForm form) {
        return TreemasAddressFormatter.generateAddress(form.getAddressStreet1(), form.getAddressStreet2(),
                form.getNoaddr(), form.getKelurahan(), form.getKecamatan(), form.getKabupaten(),
                form.getRt(), form.getRw(), form.getCity(), form.getKodePos(), form.getPropinsi(), form.getNegara());
    }

    private Map googleConvert(AnswerInputAssignmentForm form) {
        return TreemasAddressFormatter.googleConvert(this.logger, form.getAddressStreet1(), form.getAddressStreet2(),
                form.getNoaddr(), form.getKecamatan(), form.getKabupaten(),
                form.getCity(), form.getKodePos(), form.getPropinsi(), form.getNegara());
    }


    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case InputAssignmentManager.ERROR_GEOFENCE:
                Object[] o = new Object[1];
                o[0] = "GPS Location";
                try {
                    this.loadComboScheme(request);
                    this.loadPriority(request);
                    String userIdLogin = this.getLoginBean(request).getLoginId();
                    this.setMessage(request, "errors.mask", o);
                } catch (CoreException e) {
                    e.printStackTrace();
                }

                break;
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((AnswerInputAssignmentForm) form).setTask(((AnswerInputAssignmentForm) form).resolvePreviousTask());

        String task = ((AnswerInputAssignmentForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
