package treemas.application.feature.task.answerinput;

public class AnswerForm {
    private String question_group_id;
    private String question_id;
    private String answer_type_id;
    private String text_answer;
    private String combo_answer;
    private String longitude;
    private String latitude;
    private String image_base64;
    private String mnc;
    private String mcc;
    private String lac;
    private String cellid;
    private String accuracy;
    private String provider;
    private String timestamp;

    public AnswerForm(String question_group_id, String question_id, String answer_type_id) {
        this.question_group_id = question_group_id;
        this.question_id = question_id;
        this.answer_type_id = answer_type_id;
    }

    public AnswerForm() {
    }

    public String getQuestion_group_id() {
        return question_group_id;
    }

    public void setQuestion_group_id(String question_group_id) {
        this.question_group_id = question_group_id;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getAnswer_type_id() {
        return answer_type_id;
    }

    public void setAnswer_type_id(String answer_type_id) {
        this.answer_type_id = answer_type_id;
    }

    public String getText_answer() {
        return text_answer;
    }

    public void setText_answer(String text_answer) {
        this.text_answer = text_answer;
    }

    public String getCombo_answer() {
        return combo_answer;
    }

    public void setCombo_answer(String combo_answer) {
        this.combo_answer = combo_answer;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getImage_base64() {
        return image_base64;
    }

    public void setImage_base64(String image_base64) {
        this.image_base64 = image_base64;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getLac() {
        return lac;
    }

    public void setLac(String lac) {
        this.lac = lac;
    }

    public String getCellid() {
        return cellid;
    }

    public void setCellid(String cellid) {
        this.cellid = cellid;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


}
