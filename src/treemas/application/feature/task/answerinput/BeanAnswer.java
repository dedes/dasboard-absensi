package treemas.application.feature.task.answerinput;

import treemas.util.constant.AnswerType;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BeanAnswer implements Comparable, Serializable {

    private String task;
    private String nextGroup;

    private String user;
    private Date create_date;

    private Integer temp_id;
    private String scheme_id;
    private Integer question_id;
    private Integer question_group_id;
    private String group_usage_type;
    private String question_code;
    private String text_answer;
    private String text_answer_text;
    private String answer_type;
    private List text_answers;
    private List text_answer_texts;

    private String option_id;
    private String option_text;
    private String text;
    private String text_image;
    private String geo_text_image;
    private String date_time_text;

    private Integer seq_id;

    private String isList;
    private String option_ids;
    private String option_texts;

    public BeanAnswer() {
    }

    public BeanAnswer(Integer seq_id, String scheme_id, String answer_type, Integer question_group_id, Integer question_id) {
        this.seq_id = seq_id;
        this.scheme_id = scheme_id;
        this.answer_type = answer_type;
        this.question_id = question_id;
        this.question_group_id = question_group_id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getNextGroup() {
        return nextGroup;
    }

    public void setNextGroup(String nextGroup) {
        this.nextGroup = nextGroup;
    }

    public Integer getTemp_id() {
        return temp_id;
    }

    public void setTemp_id(Integer temp_id) {
        this.temp_id = temp_id;
    }

    public String getScheme_id() {
        return scheme_id;
    }

    public void setScheme_id(String scheme_id) {
        this.scheme_id = scheme_id;
    }

    public Integer getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(Integer question_id) {
        this.question_id = question_id;
    }

    public Integer getQuestion_group_id() {
        return question_group_id;
    }

    public void setQuestion_group_id(Integer question_group_id) {
        this.question_group_id = question_group_id;
    }

    public String getGroup_usage_type() {
        return group_usage_type;
    }

    public void setGroup_usage_type(String group_usage_type) {
        this.group_usage_type = group_usage_type;
    }

    public String getQuestion_code() {
        return question_code;
    }

    public void setQuestion_code(String question_code) {
        this.question_code = question_code;
    }

    public String getText_answer() {
        return text_answer;
    }

    public void setText_answer(String text_answer) {
        this.text_answer = text_answer;
    }

    public String getText_answer_text() {
        return text_answer_text;
    }

    public void setText_answer_text(String text_answer_text) {
        this.text_answer_text = text_answer_text;
    }

    public String getAnswer_type() {
        return answer_type;
    }

    public void setAnswer_type(String answer_type) {
        this.answer_type = answer_type;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Integer getSeq_id() {
        return seq_id;
    }

    public void setSeq_id(Integer seq_id) {
        this.seq_id = seq_id;
    }

    public List getText_answers() {
        return text_answers;
    }

    public void setText_answers(List text_answers) {
        this.text_answers = text_answers;
    }

    public List getText_answer_texts() {
        return text_answer_texts;
    }

    public void setText_answer_texts(List text_answer_texts) {
        this.text_answer_texts = text_answer_texts;
    }

    public String getOption_id() {
        return option_id;
    }

    public void setOption_id(String option_id) {
        this.option_id = option_id;
    }

    public String getOption_text() {
        return option_text;
    }

    public void setOption_text(String option_text) {
        this.option_text = option_text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText_image() {
        return text_image;
    }

    public void setText_image(String text_image) {
        this.text_image = text_image;
    }

    public String getGeo_text_image() {
        return geo_text_image;
    }

    public void setGeo_text_image(String geo_text_image) {
        this.geo_text_image = geo_text_image;
    }

    public String getDate_time_text() {
        return date_time_text;
    }

    public void setDate_time_text(String date_time_text) {
        this.date_time_text = date_time_text;
    }

    public String getIsList() {
        return isList;
    }

    public void setIsList(String isList) {
        this.isList = isList;
    }

    public String getOption_ids() {
        return option_ids;
    }

    public void setOption_ids(String option_ids) {
        this.option_ids = option_ids;
    }

    public String getOption_texts() {
        return option_texts;
    }

    public void setOption_texts(String option_texts) {
        this.option_texts = option_texts;
    }


    public int compareTo(Object o) {
        BeanAnswer baTemp = (BeanAnswer) o;
        return this.question_id.compareTo(baTemp.getQuestion_id());
    }

    public BeanAnswer cloneBean() {
        BeanAnswer ans = new BeanAnswer(this.seq_id, this.scheme_id, this.answer_type, this.question_group_id, this.question_id);
        ans.setCreate_date(this.create_date);
        ans.setUser(this.user);
        ans.setGroup_usage_type(this.group_usage_type);
        ans.setNextGroup(this.nextGroup);
        ans.setQuestion_code(this.question_code);
        ans.setTask(this.task);
        return ans;
    }

    public Map copyToMap() {
        Map param = new HashMap();
        param.put("scheme_id", this.scheme_id);
        param.put("question_id", this.question_id);
        param.put("question_group_id", this.question_group_id);
        param.put("group_usage_type", this.group_usage_type);
        param.put("question_code", this.question_code);
        param.put("text", this.text);
        param.put("text_image", this.text_image);
        param.put("geo_text_image", this.geo_text_image);
        param.put("date_time_text", this.date_time_text);
        param.put("option_id", this.option_id);
        param.put("option_text", this.option_text);
        param.put("answer_type", this.answer_type);
        param.put("user", this.user);
        param.put("create_date", this.create_date);
        param.put("seq_id", this.seq_id);
        return param;
    }

    @Override
    public String toString() {
        if (AnswerType.TEXT.equals(this.answer_type)
                || AnswerType.NUMERIC.equals(this.answer_type)
                || AnswerType.NUMERIC_WITH_CALCULATION.equals(this.answer_type)
                || AnswerType.DECIMAL.equals(this.answer_type)
                || AnswerType.RESPONDEN.equals(this.answer_type)
                || AnswerType.TRESHOLD.equals(this.answer_type)
                || AnswerType.ACCOUNTING.equals(this.answer_type)) {
            return this.answer_type + " - " + this.text;
        } else if (AnswerType.DATE.equals(this.answer_type)
                || AnswerType.DATE_TIME.equals(this.answer_type)
                || AnswerType.TIME.equals(this.answer_type)) {
            return this.answer_type + " - " + this.date_time_text;
        } else if (AnswerType.IMAGE.equals(this.answer_type)
                || AnswerType.IMAGE_WITH_GEODATA.equals(this.answer_type)
                || AnswerType.IMAGE_WITH_GEODATA_GPS.equals(this.answer_type)
                || AnswerType.DRAWING.equals(this.answer_type)
                || AnswerType.SIGNATURE.equals(this.answer_type)) {
            return this.answer_type + " - " + this.geo_text_image + ":" + this.text_image;
        } else if (AnswerType.IMAGE_WITH_GEODATA_TIME.equals(this.answer_type)
                || AnswerType.IMAGE_WITH_GPS_TIME.equals(this.answer_type)
                || AnswerType.IMAGE_WITH_TIME.equals(this.answer_type)) {
            return this.answer_type + " - " + this.geo_text_image + "~" + this.date_time_text + ":" + this.text_image;
        } else if (AnswerType.GEODATA_TIME.equals(this.answer_type)
                || AnswerType.GPS_TIME.equals(this.answer_type)) {
            return this.answer_type + " - " + this.geo_text_image + ":" + this.date_time_text;
        } else if (AnswerType.DROPDOWN.equals(this.answer_type)
                || AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(this.answer_type)
                || AnswerType.RADIO.equals(this.answer_type)
                || AnswerType.RADIO_WITH_DESCRIPTION.equals(this.answer_type)) {
            return this.answer_type + " - " + this.option_id + ":" + this.text;
        } else if (AnswerType.MULTIPLE.equals(this.answer_type)
                || AnswerType.MULTIPLE_LOOKUP_WITH_FILTER.equals(this.answer_type)
                || AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(this.answer_type)
                || AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(this.answer_type)) {
            return this.answer_type + " - " + this.option_id + ":" + this.option_text;
        } else {
            return "LOOK UP TYPE";
        }
    }


}
