package treemas.application.feature.task.answerinput;

import treemas.application.feature.task.answerinput.list.FeatQuestion;
import treemas.application.feature.task.answerinput.list.FeatQuestionGroup;
import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnswerInputAssignmentManager extends FeatureManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_FEATURE_INPUT_ANSWER_ASSIGNMENT;

    public void deleteHeaderTemp(String userID) throws CoreException {
        try {
            this.ibatisSqlMap.delete(this.STRING_SQL + "deleteHeaderTemp", userID);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
    }

    public void headerInput(AnswerInputAssignmentHeaderBean bean) throws CoreException {
//		#MOBILE_ASSIGNMENT_ID#, #BRANCH_ID#, #USER_ID#, #APPL_NO#, #RFA_DATE#, 
//    	#ASSIGNMENT_DATE#, #RETRIEVE_DATE#, #SUBMIT_DATE#, #FINALIZATION_DATE#, 
//    	#ASSIGNMENT_STATUS#, #CUSTOMER_NAME#, #CUSTOMER_ADDRESS#, #CUSTOMER_PHONE#, 
//    	#SCHEME_ID#, #PRIORITY_ID#, #NOTES#, #DTMCRT#, #RESULT#, #IS_NC#, #BATCH_NO#, 
//    	#FLAG_TOTAL_REKAP#, #HANDSET_VERSION#, #CUSTOMER_HP#, #GPSLATITUDE#, 
//    	#GPSLONGITUDE#, #OFFICE_PHONE#, #ADDRESSSTREET1#, #ADDRESSSTREET2#, 
//    	#NOADDR#, #RT#, #RW#, #KELURAHAN#, #KABUPATEN#, #CITY#, #PROPINSI#, 
//    	#KODEPOS#, #NEGARA#, #NIK#, #KECAMATAN#, #USRCRT#
        try {
            this.ibatisSqlMap.insert(this.STRING_SQL + "headerInput", bean);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
    }


    public Integer getCurrentSequence() throws CoreException {
        Integer ret = null;
        try {
            ret = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getCurrentSequence");
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return ret;
    }

    public Integer getNextSequence() throws CoreException {
        Integer ret = null;
        try {
            ret = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getValueSequence");
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return ret;
    }


    public void insertTempAnswer(List list) throws CoreException {
        try {
            this.ibatisSqlMap.startTransaction();
            for (Object o : list) {
                BeanAnswer bean = (BeanAnswer) o;
                Map param = bean.copyToMap();
//				this.ibatisSqlMap.update(STRING_SQL+"insertTempAnswer",bean);
                this.ibatisSqlMap.update(STRING_SQL + "uitrd", param);
            }
            this.ibatisSqlMap.commitTransaction();
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        } finally {
            try {
                this.ibatisSqlMap.endTransaction();
            } catch (SQLException e) {
                this.logger.printStackTrace(e);
                throw new CoreException(ERROR_DB);
            }
        }
    }

    public FeatQuestionGroup getSeparatedGroup(String schemeId, Integer nextGroup) throws CoreException {
        FeatQuestionGroup featQuestionGroup = null;
        Map param = new HashMap();
        param.put("schemeId", schemeId);
        param.put("nextGroup", nextGroup);
        try {
            featQuestionGroup = (FeatQuestionGroup) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSeparatedGroup", param);
            if (null != featQuestionGroup) {
                this.getQuestionSet(featQuestionGroup);
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return featQuestionGroup;
    }

    public Map getSeparatedAnswer(String schemeId, Integer nextGroup, Integer groupId, String seqId, String user, String task, MapManager mapManager) throws CoreException {
        Map retMap = new HashMap();
        Map param = new HashMap();
        List list = null;
        param.put("schemeId", schemeId);
        param.put("groupId", groupId);
        param.put("seqId", seqId);
        param.put("userId", user);
        param.put("nextGroup", nextGroup);
        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAnswerSet", param);
            if (null != list) {
                if (!list.isEmpty()) {
                    retMap = mapManager.convertListToMap(list);

                    retMap.put("task", task);
                    retMap.put("nextGroup", nextGroup);
                    retMap.put("schemeId", schemeId);
                    retMap.put("seq_id", seqId);
                }
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return retMap;
    }

    public List getSeparatedAnswer(String schemeId, Integer nextGroup, Integer groupId, String seqId, String user, String task) throws CoreException {
        Map retMap = new HashMap();
        Map param = new HashMap();
        List list = null;
        param.put("schemeId", schemeId);
        param.put("groupId", groupId);
        param.put("seqId", seqId);
        param.put("userId", user);
        param.put("nextGroup", nextGroup);
        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAnswerSet", param);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return list;
    }


    public FeatQuestionGroup getQuestionByGroup(FeatQuestionGroup featQuestionGroup) throws CoreException {
        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getQuestionSet", featQuestionGroup.getQuestionGroupId());
            if (null != list) {
                List nList = new ArrayList();
                for (Object o : list) {
                    FeatQuestion fq = (FeatQuestion) o;
                    if (Global.STRING_TRUE.equalsIgnoreCase(fq.getIsHaveAnswer())) {
                        List listQuestionAnswer = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getQuestionAnswer", featQuestionGroup.getQuestionGroupId());
                        fq.setListQuestionAnswer(listQuestionAnswer);
                    }
                    nList.add(fq);
                }
                featQuestionGroup.setQuestionGroupSet(nList);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            this.logger.printStackTrace(e);
        }
        return featQuestionGroup;
    }

    private List getQuestionAnswerByAnswer(Integer questionId) throws CoreException {
        return null;
    }

    private List getQuestionRelevantByAnswer(Integer questionId, String answer) throws CoreException {
        return null;
    }

    public List getListQuestionSet(String schemeId) throws CoreException {
        List list = null;
        try {
            List listQuestionGroup = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getQuestionGroup", schemeId);
            if (!listQuestionGroup.isEmpty())
                list = this.getQuestionSet(listQuestionGroup);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return list;
    }


    private void getQuestionSet(FeatQuestionGroup featQuestionGroup) throws CoreException {//input list question group
        try {
            List listQuestion = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getQuestionSet", featQuestionGroup.getQuestionGroupId());
            List featQuestionList = null == listQuestion ? null : new ArrayList();
            for (Object oQ : listQuestion) {
                FeatQuestion featQuestion = (FeatQuestion) oQ;
                if (Global.STRING_TRUE.equalsIgnoreCase(featQuestion.getIsHaveAnswer())) {
                    List listQuestionAnswer = this.getQuestionAnswer(featQuestion.getQuestionId());
                    featQuestion.setListQuestionAnswer(listQuestionAnswer);
                }
                featQuestionList.add(featQuestion);
            }
            featQuestionGroup.setQuestionGroupSet(featQuestionList);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
    }


    private List getQuestionSet(List list) throws CoreException {//input list question group
        List result = null == list ? null : new ArrayList();
        try {
            for (Object o : list) {
                FeatQuestionGroup featQuestionGroup = (FeatQuestionGroup) o;
                List listQuestion = this.ibatisSqlMap.queryForList(
                        this.STRING_SQL + "getQuestionSet",
                        featQuestionGroup.getQuestionGroupId());

                List featQuestionList = null == listQuestion ? null : new ArrayList();

                for (Object oQ : listQuestion) {
                    FeatQuestion featQuestion = (FeatQuestion) oQ;
                    if (Global.STRING_TRUE.equalsIgnoreCase(featQuestion.getIsHaveAnswer())) {
                        List listQuestionAnswer = this.getQuestionAnswer(featQuestion.getQuestionId());
                        featQuestion.setListQuestionAnswer(listQuestionAnswer);
                    }
                    featQuestionList.add(featQuestion);
                }

                featQuestionGroup.setQuestionGroupSet(featQuestionList);
                result.add(featQuestionGroup);
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return result;
    }

    private List getQuestionAnswer(Integer questionId) throws CoreException {//input list question
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getQuestionAnswer", questionId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return result;
    }
    //
    // public PageListWrapper getAllUnapprove(int pageNumber,
    // AnswerInputAssignmentSearch search, String branchId) throws AppException
    // {
    // PageListWrapper result = new PageListWrapper();
    // search.setPage(pageNumber);
    // search.setBranchId(branchId);
    //
    // try {
    // List list =
    // this.ibatisSqlMap.queryForList(this.STRING_SQL+"getAllUnapprove",search);
    // Integer count = (Integer)
    // this.ibatisSqlMap.queryForObject(this.STRING_SQL+"countAllUnapprove",search);
    // result.setResultList(list);
    // result.setTotalRecord(count.intValue());
    // } catch (SQLException e) {
    // this.logger.printStackTrace(e);
    // throw new AppException(ERROR_DB);
    // }
    //
    // return result;
    // }
    //
    // public void insertHistory(String assignmentStatus, String usrupd, String
    // mobileAssignmentId) throws AppException {
    // try {
    // // int seqNo = this.AssignmentHistorySeqNo();
    // String temp = this.getNotesUserCreate(mobileAssignmentId);
    // String notesUserCreate [] = temp.split("\\|");
    // String notes = notesUserCreate[0];
    // String usrcrt = notesUserCreate[1];
    //
    // Map approvalParameter = new HashMap();
    // approvalParameter.put("assignmentStatus", assignmentStatus);
    // approvalParameter.put("usrupd", usrupd);
    // approvalParameter.put("mobileAssignmentId", mobileAssignmentId);
    // approvalParameter.put("notes", notes);
    // approvalParameter.put("usrcrt", usrcrt);
    // // approvalParameter.put("seqNo", seqNo);
    // try {
    // this.ibatisSqlMap.startTransaction();
    // this.ibatisSqlMap.insert(STRING_SQL+"insertHistory", approvalParameter);
    // this.ibatisSqlMap.commitTransaction();
    // } finally {
    // this.ibatisSqlMap.endTransaction();
    // }
    // } catch (SQLException e) {
    // this.logger.printStackTrace(e);
    // throw new AppException(ERROR_DB);
    // }
    // }
    //
    // public void updateApproval(String branchId, String Status, String
    // mobileAssignmentId) throws CoreException {
    // AnswerInputAssignmentBean bean = new AnswerInputAssignmentBean();
    // int id = Integer.parseInt(mobileAssignmentId);
    //
    // bean.setBranchId(branchId);
    // bean.setAssignmentStatus(Status);
    // bean.setMobileAssignmentId(id);
    // try {
    // try {
    // this.ibatisSqlMap.startTransaction();
    // this.logger.log(ILogger.LEVEL_INFO, "Updating Assignment Status "
    // + bean.getMobileAssignmentId());
    //
    // this.ibatisSqlMap.update(this.STRING_SQL + "updateStatus",
    // bean);
    // this.ibatisSqlMap.commitTransaction();
    //
    // this.logger.log(ILogger.LEVEL_INFO, "Updated Assignment Status "
    // + bean.getMobileAssignmentId());
    // } finally {
    // this.ibatisSqlMap.endTransaction();
    // }
    // } catch (SQLException se) {
    // this.logger.printStackTrace(se);
    // throw new CoreException(se, ERROR_DB);
    // } catch (Exception e) {
    // this.logger.printStackTrace(e);
    // throw new CoreException(e, ERROR_UNKNOWN);
    // }
    // }
    //
    // // public int AssignmentHistorySeqNo() throws AppException {
    // // int result = 0;
    // // try {
    // // try {
    // // this.ibatisSqlMap.startTransaction();
    // // result =
    // (Integer)this.ibatisSqlMap.queryForObject(STRING_SQL+"AssignmentHistorySeqNo");
    // // this.ibatisSqlMap.commitTransaction();
    // // } finally {
    // // this.ibatisSqlMap.endTransaction();
    // // }
    // // } catch (SQLException e) {
    // // this.logger.printStackTrace(e);
    // // throw new AppException(ERROR_DB);
    // // }
    // //
    // // return result;
    // // }
    //
    // public String getNotesUserCreate(String mobileAssignmentId) throws
    // AppException {
    // String result = "";
    // try {
    // try {
    // this.ibatisSqlMap.startTransaction();
    // result = (String)
    // this.ibatisSqlMap.queryForObject(STRING_SQL+"getNotesUserCreate",
    // mobileAssignmentId);
    // this.ibatisSqlMap.commitTransaction();
    // } finally {
    // this.ibatisSqlMap.endTransaction();
    // }
    // } catch (SQLException e) {
    // this.logger.printStackTrace(e);
    // throw new AppException(ERROR_DB);
    // }
    //
    // return result;
    // }
    //
    // public List getUserNEmail() throws SQLException {
    // List userList =
    // this.ibatisSqlMap.queryForList(this.STRING_SQL+"getUserNEmail");
    // return userList;
    // }
    //
    // public List getEmailMessage(String usrcrt) throws SQLException {
    // List mList =
    // this.ibatisSqlMap.queryForList(this.STRING_SQL+"getEmailMessage",usrcrt);
    // return mList;
    // }
    //
    // public List getSurveyorId(List<Integer> assignments) throws SQLException
    // {
    // return this.ibatisSqlMap.queryForList(this.STRING_SQL+"getSurveyorID",
    // assignments);
    // }
}
