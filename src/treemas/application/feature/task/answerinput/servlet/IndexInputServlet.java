package treemas.application.feature.task.answerinput.servlet;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.general.ComboManager;
import treemas.application.feature.task.answerinput.AnswerInputAssignmentForm;
import treemas.application.feature.task.answerinput.AnswerInputAssignmentHeaderBean;
import treemas.application.feature.task.answerinput.AnswerInputAssignmentManager;
import treemas.application.feature.task.answerinput.AnswerInputAssignmentValidator;
import treemas.base.handset.ServletBase;
import treemas.globalbean.SessionBean;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.geofence.google.GoogleAddressFormater;
import treemas.util.http.HTTPConstant;
import treemas.util.struts.MultipleBaseBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class IndexInputServlet extends ServletBase {
    private boolean enEnabled = true;
    private boolean deEnabled = true;

    private String url = "/servlet/inputanswer/";
    private String delimiter = ":";
    private String rkm = "RKM Android User";

    private ComboManager comboManager = new ComboManager();
    private AnswerInputAssignmentManager manager = new AnswerInputAssignmentManager();
    private AnswerInputAssignmentValidator validator;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SessionBean sessionBean = this.getLoginBean(request);
        String uri = request.getScheme() + "://" + request.getServerName()
                + ":" + request.getServerPort() +
                request.getRequestURI();//.replace(this.url, "");
        String ret = "";
        if (null != sessionBean) {
            ServletContext sc = getServletContext();
            String json = request.getParameter("headerJson");
            AnswerInputAssignmentForm form = new AnswerInputAssignmentForm();
            if (!Strings.isNullOrEmpty(json)) {
                form = new Gson().fromJson(json, AnswerInputAssignmentForm.class);
            }

            this.validator = new AnswerInputAssignmentValidator(request);
            ActionMessages messages = new ActionMessages();
            try {
                if (this.validator.validateAssignmentInsert(messages, form)) {
                    AnswerInputAssignmentHeaderBean bean = new AnswerInputAssignmentHeaderBean();
                    String msidTem = "";
                    Date date = new Date();
                    int year = date.getYear() + 1900;
                    int month = date.getMonth() + 1;
                    int dt = date.getDate();
                    msidTem = sessionBean.getUserid() + "" + year + "" + month + "" + dt;
                    BeanConverter.convertBeanFromString(form, bean);
                    bean.setMobileAssignmentId(msidTem);
                    bean.setAssignmentDate(null == bean.getAssignmentDate() ? date : bean.getAssignmentDate());
                    bean.setRetrieveDate(null == bean.getRetrieveDate() ? date : bean.getRetrieveDate());
//						bean.setSubmitDate(null==bean.getSubmitDate()?date:bean.getSubmitDate());
                    bean.setSurveyorId(sessionBean.getUserid());
                    bean.setUsrcrt(sessionBean.getUserid());
                    bean.setBranchId(sessionBean.getBranchId());
                    String address = GoogleAddressFormater.formatAddress(form.getAddressStreet1(),
                            form.getAddressStreet2(), form.getNoaddr(), form.getRt(), form.getRw(),
                            form.getKelurahan(), form.getKecamatan(), form.getKabupaten(), form.getCity(),
                            form.getPropinsi(), form.getNegara(), form.getKodePos());
                    System.out.println(address);
                    bean.setCustomerAddress(address);
                    this.manager.headerInput(bean);
                    request.setAttribute("form", form);
                    RequestDispatcher rd = request.getRequestDispatcher("/servlet/survey/" + form.getSchemeId() + "?group=1");
                    rd.forward(request, response);
                }
            } catch (CoreException e) {
                this.logger.printStackTrace(e);
                Map errorMap = new HashMap();
                Iterator properties = messages.properties();
                while (properties.hasNext()) {
                    String property = String.valueOf(properties.next());
                    Iterator values = messages.get(property);
                    properties.remove();
                    while (values.hasNext()) {
                        String val = String.valueOf(values.next());
                        values.remove();
                        errorMap.put(property, this.getMessage(val, this.getResources(request)));
                    }
                }
                request.setAttribute("errorForm", errorMap);
            }
            //		ret = ret +uri+ "servlet/inputanswer";
            request.setAttribute("form", form);
            doGet(request, response);
        } else {
            ret = ret + uri + "/general/blank.jsp";
            response.sendRedirect(ret);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AnswerInputAssignmentForm beanForm = new AnswerInputAssignmentForm();
        Map errorMap = new HashMap();
        String uri = request.getScheme() + "://" + request.getServerName()
                + ":" + request.getServerPort() +
                request.getRequestURI();//.replace(this.url, "");
        String ret = "";
        Object o = request.getAttribute("form");
        Object oError = request.getAttribute("errorForm");
        if (null != o) {
            beanForm = (AnswerInputAssignmentForm) o;
        }

        if (null != oError) {
            errorMap = (Map) oError;
        }

        PrintWriter out = response.getWriter();
        SessionBean sessionBean = this.getLoginBean(request);
        if (null != sessionBean) {

            String user = sessionBean.getLoginId();

            response.setContentType("text/html");
            if (Strings.isNullOrEmpty(user)) {
                ret = ret + uri.replace(this.url, "") + "/general/blank.jsp";
                response.sendRedirect(ret);
            } else {
                try {
//					ret = ret +uri+ this.url;
                    ret = ret + uri;
                    List<MultipleBaseBean> listScheme = this.comboManager.getComboSchemeSurveyByUser(user);
                    List<MultipleBaseBean> listPriority = this.comboManager.getComboPriority();
                    String title = Global.TITLE_FEATURE_ANSWER_INPUT;
                    out.print(HTTPConstant.DOC_TYPE);
                    out.print(HTTPConstant.HEAD_START);
                    out.print(HTTPConstant.TITLE.replaceAll("#TITLE#", title));
                    out.print(HTTPConstant.SCRIPT_BASIC);
                    out.print(HTTPConstant.SCRIPT_START);
                    String script = ""
                            + "$.fn.serializeObject = function(){"
                            + "	var o = {};"
                            + "	var a = this.serializeArray();"
                            + "	$.each(a, function() {"
                            + "		if (o[this.name] !== undefined) {"
                            + "			if (!o[this.name].push) {"
                            + "				o[this.name] = [o[this.name]];"
                            + "			}"
                            + "				o[this.name].push(this.value || '');"
                            + "			} else {"
                            + "				o[this.name] = this.value || '';"
                            + "			}"
                            + "		});"
                            + "	return o;"
                            + "};"
                            + "function movePage(taskID) {"
                            + "    if (taskID == 'Add' || taskID == 'Save' || taskID == 'Update' || taskID == 'SaveInput') {"
                            + "       if (document.forms[0].onsubmit == null || document.forms[0].onsubmit()) {"
                            + "           document.forms[0].task.value = taskID;"
                            + "		   	  document.forms[0].headerJson.value = JSON.stringify($(\"#answerSurveyAssignmentForm\").serializeObject()); "
                            + "           document.forms[0].submit();"
                            + "       }"
                            + "    }"
                            + "    else if (taskID=='Load') {"
                            + "        document.forms[0].task.value = taskID;"
                            + "		   document.forms[0].headerJson.value = JSON.stringify($(\"#answerSurveyAssignmentForm\").serializeObject()); "
                            + "        document.forms[0].submit();"
                            + "    }"
                            + "}";
                    out.print(script);
                    out.print(HTTPConstant.SCRIPT_END);
                    out.print(HTTPConstant.HEAD_END);
                    out.print(HTTPConstant.BODY_START);
                    out.print(HTTPConstant.DIV_CENTER_START);
                    out.print(HTTPConstant.TITLE_HEAD.replaceAll("#TITLE#", this.getResources(request).getMessage("app.assignment.title")));
                    String form = ""
                            + "<form id='answerSurveyAssignmentForm' method='POST' action='" + ret + "'>"
                            + "		<input name='seq_id' value='' type='hidden'>"
                            + "		<input name='task' value='' type='hidden'>"
                            + "		<input name='surveyorId' value='" + this.getValueOf(user) + "' type='hidden'>"
                            + "		<input name='headerJson' value='' type='hidden'>"
                            + "		<table class='tableview' border='0' cellpadding='3' cellspacing='1' width='95%'>"
                            + "			<tbody>"
                            + "				<tr><td class='headercol' colspan='4' align='center'>" + this.getResources(request).getMessage("app.assignment.customerdetail") + "</td></tr>"
                            + "				<tr><td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.nik") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='nik' maxlength='30' size='25' value='" + this.getValueOf(beanForm.getNik()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>*) " + this.getValueOf(String.valueOf(errorMap.get("nik"))) + "</font></td>"
                            + "					<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("common.name") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='customerName' maxlength='60' size='35' value='" + this.getValueOf(beanForm.getCustomerName()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>*) " + this.getValueOf(String.valueOf(errorMap.get("customerName"))) + "</font></td>"
                            + "				</tr>"
                            + "				<tr>"
                            + "					<td class='Edit-Left-Text'>app.assignment.priority</td>"
                            + "					<td class='Edit-Right-Text-II' colspan='3'>"
                            + "	        			<select name='priority' class='TextBox'><option value=''>" + this.getResources(request).getMessage("common.choose.one") + "</option>"
                            + "							" + this.generateMultipleCombo(listPriority, this.getValueOf(beanForm.getPriority()))
                            + "						</select> <font color='#FF0000'>*) " + this.getValueOf(String.valueOf(errorMap.get("priority"))) + "</font>"
                            + "					</td>"
                            + "				</tr>"
                            + "				<tr><td class='headercol' colspan='4' align='center'>" + this.getResources(request).getMessage("app.assignment.phonetitle") + "</td></tr>"
                            + "				<tr>"
                            + "					<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.customerphone") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='customerPhone' maxlength='20' size='30' value='" + this.getValueOf(beanForm.getCustomerPhone()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>" + this.getValueOf(String.valueOf(errorMap.get("customerPhone"))) + "</font>"
                            + "					</td>"
                            + "					<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.officephone") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='officePhone' maxlength='20' size='30' value='" + this.getValueOf(beanForm.getOfficePhone()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>" + this.getValueOf(String.valueOf(errorMap.get("officePhone"))) + "</font>"
                            + "					</td>"
                            + "				</tr>"
                            + "				<tr>"
                            + "					<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.cellularphone") + "</td>"
                            + "					<td class='Edit-Right-Text-II' colspan='3'>"
                            + "						<input name='customerHandPhone' maxlength='20' size='30' value='" + this.getValueOf(beanForm.getCustomerHandPhone()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>" + this.getValueOf(String.valueOf(errorMap.get("customerHandPhone"))) + "</font>"
                            + "					</td>"
                            + "				</tr>"
                            + "				<tr><td class='headercol' colspan='4' align='center'>" + this.getResources(request).getMessage("app.assignment.customeraddresstitle") + "</td></tr>"
                            + "			    <tr>"
                            + "			      	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.customeraddress") + "</td>"
                            + "					<td class='Edit-Right-Text-II' colspan='3'>"
                            + "						<input name='addressStreet1' maxlength='80' size='80' value='" + this.getValueOf(beanForm.getAddressStreet1()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>*) " + this.getValueOf(String.valueOf(errorMap.get("addressStreet1"))) + "</font>"
                            + "						<br><br>"
                            + "						<input name='addressStreet2' maxlength='80' size='80' value='" + this.getValueOf(beanForm.getAddressStreet2()) + "' class='TextBox' type='text'>"
                            + "					</td>"
                            + "				</tr>"
                            + "			    <tr>"
                            + "			      	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.addressnumber") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='noaddr' maxlength='10' size='10' value='" + this.getValueOf(beanForm.getNoaddr()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>*) " + this.getValueOf(String.valueOf(errorMap.get("noaddr"))) + "</font>"
                            + "					</td>"
                            + "			      	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.rt") + "/" + this.getResources(request).getMessage("app.assignment.rw") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='rt' maxlength='3' size='3' value='" + this.getValueOf(beanForm.getRt()) + "' class='TextBox' type='text'>&nbsp;/&nbsp;"
                            + "				        <input name='rw' maxlength='3' size='3' value='" + this.getValueOf(beanForm.getRw()) + "' class='TextBox' type='text'>"
                            + "				        <font color='#FF0000'>" + this.getValueOf(String.valueOf(errorMap.get("rt"))) + " " + this.getValueOf(String.valueOf(errorMap.get("rw"))) + "</font>"
                            + "					</td>"
                            + "				</tr>"
                            + "			    <tr>"
                            + "			      	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.kelurahan") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='kelurahan' maxlength='30' size='30' value='" + this.getValueOf(beanForm.getKelurahan()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>" + this.getValueOf(String.valueOf(errorMap.get("kelurahan"))) + "</font>"
                            + "					</td>"
                            + "			      	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.kecamatan") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='kecamatan' maxlength='30' size='30' value='" + this.getValueOf(beanForm.getKecamatan()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>" + this.getValueOf(String.valueOf(errorMap.get("kecamatan"))) + "</font>"
                            + "					</td>"
                            + "				</tr>"
                            + "			    <tr>"
                            + "			      	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.kabupaten") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='kabupaten' maxlength='30' size='30' value='" + this.getValueOf(beanForm.getKabupaten()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>" + this.getValueOf(String.valueOf(errorMap.get("kabupaten"))) + "</font>"
                            + "					</td>"
                            + "			      	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.city") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='city' maxlength='50' size='30' value='" + this.getValueOf(beanForm.getCity()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>*) " + this.getValueOf(String.valueOf(errorMap.get("city"))) + "</font>"
                            + "					</td>"
                            + "				</tr>"
                            + "			    <tr>"
                            + "			      	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.province") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='propinsi' maxlength='50' size='30' value='" + this.getValueOf(beanForm.getPropinsi()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>*) " + this.getValueOf(String.valueOf(errorMap.get("propinsi"))) + "</font>"
                            + "					</td>"
                            + "					<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.postcode") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='kodePos' maxlength='5' size='5' value='" + this.getValueOf(beanForm.getKodePos()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>*) " + this.getValueOf(String.valueOf(errorMap.get("kodePos"))) + "</font>"
                            + "					</td>"
                            + "				</tr>"
                            + "			    <tr>"
                            + "			      	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.country") + "</td>"
                            + "					<td class='Edit-Right-Text-II' colspan='3'>"
                            + "						<input name='negara' maxlength='30' size='20' value='" + this.getValueOf(beanForm.getNegara()) + "' class='TextBox' type='text'>"
                            + "						<font color='#FF0000'>*) " + this.getValueOf(String.valueOf(errorMap.get("negara"))) + "</font>"
                            + "					</td>"
                            + "				</tr>"
                            + "				<tr>"
                            + "			      	<td class='Edit-Right-Text' colspan='4' align='left'><font color='#FF0000'>" + this.getResources(request).getMessage("app.assignment.coordinatemessage") + "</font>"
                            + "				      	<div id='googleText' style=\"text-decoration:none; font-family: 'Californian FB'; display: inline; font-size: large;\">"
                            + "					      	<a href='http://www.maps.google.com' target='_blank' title='Click to visit www.maps.google.com'>"
                            + "					      		<font color='#186DEE'>G</font><font color='#DB4632'>o</font><font color='#FFBB07'>o</font><font color='#186DEE'>g</font><font color='#049F5C'>l</font><font color='#D5402D'>e</font>"
                            + "					      	</a>"
                            + "				      	</div>"
                            + "			      	</td>"
                            + "			    </tr>"
                            + "				<tr>"
                            + "				   	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.latitude") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='gpsLatitude' maxlength='10' size='10' value='" + this.getValueOf(beanForm.getGpsLatitude()) + "' onkeydown='javascript:return numericInputKoma(event);' class='TextBox' type='text'>"
                            + "						" + this.getResources(request).getMessage("app.assignment.coordinateformat")
                            + "						<font color='#FF0000'>" + this.getValueOf(String.valueOf(errorMap.get("gpsLatitude"))) + "</font>"
                            + "					</td>"
                            + "					<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.longitude") + "</td>"
                            + "					<td class='Edit-Right-Text-II'>"
                            + "						<input name='gpsLongitude' maxlength='10' size='10' value='" + this.getValueOf(beanForm.getGpsLongitude()) + "' onkeydown='javascript:return numericInputKoma(event);' class='TextBox' type='text'>"
                            + "						" + this.getResources(request).getMessage("app.assignment.coordinateformat")
                            + "						<font color='#FF0000'>" + this.getValueOf(String.valueOf(errorMap.get("gpsLongitude"))) + "</font>"
                            + "					</td>"
                            + "				</tr>"
                            + "				<tr>"
                            + "				   	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.scheme") + "</td>"
                            + "					<td class='Edit-Right-Text-II' colspan='3'>"
                            + "				    	<select name='schemeId' class='TextBox'>"
                            + "						" + this.generateMultipleCombo(listScheme, this.getValueOf(beanForm.getSchemeId()))
                            + "						</select>"
                            + "				       	<font color='#FF0000'>*) " + this.getValueOf(String.valueOf(errorMap.get("schemeId"))) + "</font>"
                            + "					</td>"
                            + "				</tr>"
                            + "				<tr>"
                            + "				   	<td class='Edit-Left-Text'>" + this.getResources(request).getMessage("app.assignment.notes") + "</td>"
                            + "					<td class='Edit-Right-Text-II' colspan='3'><textarea name='notes' cols='50%' rows='2' onkeypress='maxLengthTextArea(this, 1000)' class='TextBox' value='" + this.getValueOf(beanForm.getNotes()) + "'></textarea></td>"
                            + "				</tr>"
                            + "  			</tbody></table>"
                            + "		" + HTTPConstant.DIV_END
                            + "	    <table border='0' cellpadding='0' cellspacing='0' width='95%'>"
                            + "			<tbody><tr>"
                            + "				<td height='30' width='50%'>&nbsp;</td>"
                            + "			    <td align='right' width='50%'>"
                            + "			        <a href=\"javascript:movePage('Add');\">"
                            + "			         	<img src='../../images/button/ButtonNext.png' alt='" + this.getResources(request).getMessage("tooltip.action") + "' title='" + this.getResources(request).getMessage("tooltip.action") + "' border='0' height='31' width='100'>"
                            + "			        </a>"
                            + "			     </td>"
                            + "			</tr></tbody>"
                            + "		</table>"
                            + "<form>";
                    out.print(form);
                    out.print(HTTPConstant.BODY_END);
                    out.print(HTTPConstant.HTML_END);
                } catch (CoreException e) {
                    e.printStackTrace();
                    this.logger.printStackTrace(e);
                }
            }
        } else {
            ret = ret + uri + "/general/blank.jsp";
            response.sendRedirect(ret);
        }
    }
}
