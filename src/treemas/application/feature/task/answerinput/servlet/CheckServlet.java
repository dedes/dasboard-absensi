package treemas.application.feature.task.answerinput.servlet;

import com.google.gson.GsonBuilder;
import treemas.application.feature.task.answerinput.AnswerInputAssignmentManager;
import treemas.base.handset.ServletBase;
import treemas.util.CoreException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class CheckServlet extends ServletBase {
    private boolean enEnabled = true;
    private boolean deEnabled = true;

    private String url = "/m/servletcheck/";
    private String delimiter = ":";
    private String rkm = "RKM Android User";

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String message = "User not authorized";
        PrintWriter out = resp.getWriter();
        System.out.println("Android calling " + url + ".... method GET");

        try {
            String schemeId = req.getParameter("scheme");
            System.out.println(schemeId);
            AnswerInputAssignmentManager answerInputAssignmentManager = new AnswerInputAssignmentManager();
            List list = answerInputAssignmentManager.getListQuestionSet(schemeId);
            resp.setStatus(HttpServletResponse.SC_OK);
            GsonBuilder gson = new GsonBuilder();
            String json = gson.create().toJson(list, List.class);
            out.print(json);
        } catch (CoreException ex) {
            ex.printStackTrace();
            resp.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

}
