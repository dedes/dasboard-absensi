package treemas.application.feature.task.answerinput.servlet;

import com.google.common.base.Strings;
import treemas.application.feature.general.ComboManager;
import treemas.application.feature.task.answerinput.AnswerInputAssignmentForm;
import treemas.application.feature.task.answerinput.AnswerInputAssignmentManager;
import treemas.application.feature.task.answerinput.AnswerInputAssignmentValidator;
import treemas.application.feature.task.answerinput.list.FeatQuestion;
import treemas.application.feature.task.answerinput.list.FeatQuestionGroup;
import treemas.base.handset.ServletBase;
import treemas.globalbean.SessionBean;
import treemas.util.CoreException;
import treemas.util.constant.AnswerType;
import treemas.util.constant.Global;
import treemas.util.http.HTTPConstant;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class HeaderServlet extends ServletBase {
    private boolean enEnabled = true;
    private boolean deEnabled = true;

    private String url = "/servlet/survey/";
    private String delimiter = ":";
    private String rkm = "RKM Android User";

    private AnswerInputAssignmentValidator validator;

    private ComboManager comboManager = new ComboManager();
    private AnswerInputAssignmentManager manager = new AnswerInputAssignmentManager();

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String schemeId = request.getPathInfo().replaceAll("/", "");
        AnswerInputAssignmentForm beanForm = null;
        String group = request.getParameter("group");
        Object o = request.getAttribute("form");
        if (null != o) {
            beanForm = (AnswerInputAssignmentForm) o;
        }

        if (null != beanForm) {
            try {
                schemeId = schemeId.equalsIgnoreCase(beanForm.getSchemeId()) ? schemeId : beanForm.getSchemeId();
                FeatQuestionGroup fqg = this.manager.getSeparatedGroup(schemeId, Integer.parseInt(group));
                fqg = this.manager.getQuestionByGroup(fqg);
                request.setAttribute("questionset", fqg);
            } catch (CoreException e) {
                e.printStackTrace();
                this.logger.printStackTrace(e);
            }
        }
        doGet(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String uri = request.getScheme() + "://" + request.getServerName()
                + ":" + request.getServerPort() +
                request.getRequestURI();//.replace(this.url, "");
        String ret = "";
        SessionBean sessionBean = this.getLoginBean(request);

        if (null != sessionBean) {
            String user = sessionBean.getLoginId();
            response.setContentType("text/html");
            if (Strings.isNullOrEmpty(user)) {
                ret = ret + uri.replace(this.url, "") + "/general/blank.jsp";
                response.sendRedirect(ret);
            } else {
                FeatQuestionGroup fqg = new FeatQuestionGroup();
                Object qSet = request.getAttribute("questionset");
                if (null != qSet) {
                    fqg = (FeatQuestionGroup) qSet;
                }

                ret = ret + uri;
                String title = Global.TITLE_FEATURE_ANSWER_INPUT;
                out.print(HTTPConstant.DOC_TYPE);
                out.print(HTTPConstant.HEAD_START);
                out.print(HTTPConstant.TITLE.replaceAll("#TITLE#", title));
                out.print(HTTPConstant.SCRIPT_BASIC);
                out.print(HTTPConstant.SCRIPT_START);
                String script = ""
                        + "$.fn.serializeObject = function(){"
                        + "	var o = {};"
                        + "	var a = this.serializeArray();"
                        + "	$.each(a, function() {"
                        + "		if (o[this.name] !== undefined) {"
                        + "			if (!o[this.name].push) {"
                        + "				o[this.name] = [o[this.name]];"
                        + "			}"
                        + "				o[this.name].push(this.value || '');"
                        + "			} else {"
                        + "				o[this.name] = this.value || '';"
                        + "			}"
                        + "		});"
                        + "	return o;"
                        + "};"
                        + "function movePage(taskID) {"
                        + "	var form = document.forms[0];"
                        + "	form.task.value = task;"
                        + "	if(page!=0)"
                        + "		form.nextGroup.value = page;"
                        + "	if(null==form.jsonAnswer.value || undefined == form.jsonAnswer.value)"
                        + "		form.jsonAnswer.value = '';"
                        + "	form.jsonAnswer.value = JSON.stringify($('form').serializeObject());"
                        + "	form.submit();"
                        + "}";
                out.print(script);
                out.print(HTTPConstant.SCRIPT_END);
                out.print(HTTPConstant.HEAD_END);
                out.print(HTTPConstant.BODY_START);
                out.print(HTTPConstant.DIV_CENTER_START);
                out.print(HTTPConstant.TITLE_HEAD.replaceAll("#TITLE#", this.getResources(request).getMessage("app.assignment.title")));
                String form = ""
                        + "<table width='95%' border='0' cellpadding='3' cellspacing='1' class='tableview'>"
                        + "	<tr><td class='Edit-Left-Text'>" + this.getResources(request).getMessage("feature.question.group") + "</td>"
                        + "	<td class='Edit-Right-Text'><strong>" + fqg.getQuestionGroupName() + "</strong></td></tr></table>"
                        + "	<form id='answerForm' method='POST' action='" + ret + "'>"
                        + "		<input name='headerJson' value='' type='hidden'>"
                        + " 	<table width='95%' border='0' cellpadding='3' cellspacing='1' class='tableview'>"
                        + "			<tr align='center' class='headercol'><td width='5%' align='center'>&nbsp;</td>"
                        + "				<td width='30%' align='center'>" + this.getResources(request).getMessage("feature.question") + "</td>"
                        + "				<td width='60%' align='center'>" + this.getResources(request).getMessage("common.answer") + "</td></tr>";
                List<FeatQuestion> list = fqg.getQuestionGroupSet();
                int index = 0;
                for (FeatQuestion fq : list) {
                    Integer questionId = fq.getQuestionId();
                    String questionLabel = fq.getQuestionLabel();
                    String answerType = fq.getAnswerType();
                    Integer maxLength = fq.getMaxLength();
                    String regex = fq.getRegexPattern();

                    boolean isReadOnly = Global.STRING_TRUE.equalsIgnoreCase(fq.getIsReadOnly()) ? true : false;
                    boolean isUseMin = Global.STRING_TRUE.equalsIgnoreCase(fq.getUseMin()) ? true : false;
                    boolean isVisible = Global.STRING_TRUE.equalsIgnoreCase(fq.getIsVisible()) ? true : false;
                    boolean isMandatori = Global.STRING_TRUE.equalsIgnoreCase(fq.getIsMandatory()) ? true : false;
                    boolean isHaveAnswer = Global.STRING_TRUE.equalsIgnoreCase(fq.getIsHaveAnswer()) ? true : false;
                    String classColor = (index % 2 == 0) ? "evalcol" : "oddcol";
                    form = form
                            + "<tr class='" + classColor + "'><td align='right'>" + String.format("%1$3d.", (index + 1)) + "</td>"
                            + "<td>" + questionLabel;
                    if (isMandatori) {
                        form = form + "<font color='#FF0000' title='MandatoryField'>*)</font>";
                    }
                    form = form + "</td>";
                    /*
					 * REGEX BELUM
					 * VALIDASI BELUM
					 * */
                    if (AnswerType.TEXT.equalsIgnoreCase(answerType) || AnswerType.RESPONDEN.equalsIgnoreCase(answerType)) {
                        form = form + "<input type='text' class='TextBox' size='" + (null != maxLength ? "maxlength='" + maxLength + "' size='" + maxLength + "'" : "") + "'/>";
                    } else if (AnswerType.NUMERIC.equalsIgnoreCase(answerType) || AnswerType.NUMERIC_WITH_CALCULATION.equalsIgnoreCase(answerType)
                            || AnswerType.ACCOUNTING.equalsIgnoreCase(answerType) || AnswerType.TRESHOLD.equalsIgnoreCase(answerType)
                            || AnswerType.DECIMAL.equalsIgnoreCase(answerType)) {
                        String returnFormat = "";
                        if (AnswerType.DECIMAL.equalsIgnoreCase(answerType) || AnswerType.TRESHOLD.equalsIgnoreCase(answerType)) {
                            returnFormat = "onkeydown='javascript:return numericInputKoma(event);'";
                        } else {
                            returnFormat = "onkeydown='javascript:return numericInput(event);'";
                        }
                        form = form + "<input type='text' class='TextBox' size='" + (null != maxLength ? "maxlength='" + maxLength + "' size='" + maxLength + "'" : "") + "'/>";
                    }

                    form = form + "<td></td></tr>";
                    index++;
                }
                form = form
                        + "  	</tbody></table>"
                        + "		" + HTTPConstant.DIV_END
                        + "	    <table border='0' cellpadding='0' cellspacing='0' width='95%'>"
                        + "			<tbody><tr>"
                        + "				<td height='30' width='50%'>&nbsp;</td>"
                        + "			    <td align='right' width='50%'>"
                        + "			        <a href=\"javascript:movePage('Add');\">"
                        + "			         	<img src='../../images/button/ButtonNext.png' alt='" + this.getResources(request).getMessage("tooltip.action") + "' title='" + this.getResources(request).getMessage("tooltip.action") + "' border='0' height='31' width='100'>"
                        + "			        </a>"
                        + "			     </td>"
                        + "			</tr></tbody>"
                        + "		</table>"
                        + "<form>";
                out.print(form);
                out.print(HTTPConstant.BODY_END);
                out.print(HTTPConstant.HTML_END);
            }
        } else {
            ret = ret + uri + "/general/blank.jsp";
            response.sendRedirect(ret);
        }

    }
}
