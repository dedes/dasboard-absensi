package treemas.application.feature.task.answerinput.list;

import java.io.Serializable;
import java.util.List;

public class FeatQuestionGroup implements Serializable {
    private String schemeId;
    private Integer questionGroupId;
    private Integer schemeLineSeqOrder;
    private String questionGroupName;
    private String usageType;
    private Double totalValue;
    private Integer rownum;
    private Integer countGroup;
    private Integer beforeGroup;
    private Integer nextGroup;
    private List<FeatQuestion> questionGroupSet;

    public FeatQuestionGroup() {
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public Integer getSchemeLineSeqOrder() {
        return schemeLineSeqOrder;
    }

    public void setSchemeLineSeqOrder(Integer schemeLineSeqOrder) {
        this.schemeLineSeqOrder = schemeLineSeqOrder;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public List<FeatQuestion> getQuestionGroupSet() {
        return questionGroupSet;
    }

    public void setQuestionGroupSet(List<FeatQuestion> questionGroupSet) {
        this.questionGroupSet = questionGroupSet;
    }

    public Integer getRownum() {
        return rownum;
    }

    public void setRownum(Integer rownum) {
        this.rownum = rownum;
    }

    public Integer getCountGroup() {
        return countGroup;
    }

    public void setCountGroup(Integer countGroup) {
        this.countGroup = countGroup;
    }

    public Integer getBeforeGroup() {
        return beforeGroup;
    }

    public void setBeforeGroup(Integer beforeGroup) {
        this.beforeGroup = beforeGroup;
    }

    public Integer getNextGroup() {
        return nextGroup;
    }

    public void setNextGroup(Integer nextGroup) {
        this.nextGroup = nextGroup;
    }


}
