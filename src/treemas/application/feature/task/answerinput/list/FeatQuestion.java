package treemas.application.feature.task.answerinput.list;

import java.io.Serializable;
import java.util.List;

public class FeatQuestion implements Serializable {
    private Integer questionId;
    private String questionLabel;
    private String answerType;
    private String isActive;
    private Integer questionLineSeqOrder;
    private String isMandatory;
    private String paramName;
    private String lookUpCode;
    private Integer maxLength;
    private String isVisible;
    private String regexPattern;
    private String isReadOnly;
    private Double minval;
    private Double maxval;
    private String useMin;
    private Integer minChoose;
    private String isHaveAnswer;
    private List<FeatQuestionAnswer> listQuestionAnswer;

    public FeatQuestion() {
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public Integer getQuestionLineSeqOrder() {
        return questionLineSeqOrder;
    }

    public void setQuestionLineSeqOrder(Integer questionLineSeqOrder) {
        this.questionLineSeqOrder = questionLineSeqOrder;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getLookUpCode() {
        return lookUpCode;
    }

    public void setLookUpCode(String lookUpCode) {
        this.lookUpCode = lookUpCode;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public String getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(String isVisible) {
        this.isVisible = isVisible;
    }

    public String getRegexPattern() {
        return regexPattern;
    }

    public void setRegexPattern(String regexPattern) {
        this.regexPattern = regexPattern;
    }

    public String getIsReadOnly() {
        return isReadOnly;
    }

    public void setIsReadOnly(String isReadOnly) {
        this.isReadOnly = isReadOnly;
    }

    public Double getMinval() {
        return minval;
    }

    public void setMinval(Double minval) {
        this.minval = minval;
    }

    public Double getMaxval() {
        return maxval;
    }

    public void setMaxval(Double maxval) {
        this.maxval = maxval;
    }

    public Integer getMinChoose() {
        return minChoose;
    }

    public void setMinChoose(Integer minChoose) {
        this.minChoose = minChoose;
    }

    public String getIsHaveAnswer() {
        return isHaveAnswer;
    }

    public void setIsHaveAnswer(String isHaveAnswer) {
        this.isHaveAnswer = isHaveAnswer;
    }

    public List<FeatQuestionAnswer> getListQuestionAnswer() {
        return listQuestionAnswer;
    }

    public void setListQuestionAnswer(List<FeatQuestionAnswer> listQuestionAnswer) {
        this.listQuestionAnswer = listQuestionAnswer;
    }

    public String getUseMin() {
        return useMin;
    }

    public void setUseMin(String useMin) {
        this.useMin = useMin;
    }
}
