package treemas.application.feature.task.answerinput.ws;

import treemas.application.feature.general.ComboManager;
import treemas.application.feature.task.answerinput.AnswerInputAssignmentManager;
import treemas.util.CoreException;

import java.util.List;

public class WSHandler {

    private AnswerInputAssignmentManager manager = new AnswerInputAssignmentManager();
    private ComboManager comboManager = new ComboManager();

    public WSHandler() {
    }

    public List loadComboScheme(String userId) throws CoreException {
        return this.comboManager.getComboScheme(userId);
    }

    public List loadComboActiveSchemeForAdd(String userId) throws CoreException {
        return this.comboManager.getComboScheme(userId);
    }

}
