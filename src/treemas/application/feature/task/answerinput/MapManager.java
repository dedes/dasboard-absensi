package treemas.application.feature.task.answerinput;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import treemas.util.CoreException;
import treemas.util.constant.AnswerType;
import treemas.util.constant.Global;

import java.util.*;

public class MapManager {
    public static final String TEXT_INDEX = "text";
    public static final String ANSWER_SEPARATOR_ID = "_";
    public static final String ANSWER_SEPARATOR_ANSWKEY = "#";
    public static final String ANSWER_SEPARATOR = ",";
    public static final String TEXT_IMAGE_BASE64 = "data:image/jpeg;base64,";
    public static final String TEXT_IMAGE_LATITUDE = "LAT";
    public static final String TEXT_IMAGE_LONGITUDE = "LGT";

    public Map putJsonToMap(String json) {
        Gson gson = new GsonBuilder().create();
        Map map = null;

        map = gson.fromJson(json, HashMap.class);

        return map;
    }

    public void addNewMapToMap(Map map, Map param) {
        if (null == map) {
            map = new HashMap();
        }

        map.putAll(param);

    }


    public Map convertListToMap(List list) throws CoreException {
        Map map = null;

        if (list != null) {
            if (!list.isEmpty()) {
                map = new HashMap();

                for (Object o : list) {
                    BeanAnswer bean = (BeanAnswer) o;
                    String idAnswer = bean.getQuestion_code();
                    String idAnswerText = ANSWER_SEPARATOR_ID
                            + bean.getAnswer_type()
                            + ANSWER_SEPARATOR_ANSWKEY
                            + bean.getQuestion_group_id()
                            + ANSWER_SEPARATOR_ANSWKEY
                            + bean.getQuestion_id()
                            + TEXT_INDEX
                            + ANSWER_SEPARATOR_ID;
                    boolean isList = Global.STRING_TRUE.equals(bean.getIsList());
                    String answerType = bean.getAnswer_type();
                    String answer;
                    String answerText;

                    String text_image = null;
                    String geo_text_image = null;
                    String date_time_text = null;
                    String option_ids = null;
                    String option_texts = null;


                    if (AnswerType.MULTIPLE.equals(answerType)
                            || AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(answerType)
                            || AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(answerType)) {
                        option_ids = bean.getOption_ids();
                        option_texts = bean.getOption_texts();
                        boolean isNotNull = !Strings.isNullOrEmpty(option_ids);
                        boolean isSeparated = false;
                        if (isNotNull) {
                            isSeparated = option_ids.contains(ANSWER_SEPARATOR);
                        }
                        if (isList && isNotNull && isSeparated) {//choose morethan one
                            option_ids = bean.getOption_ids();
                            option_texts = bean.getOption_texts();
                            String[] datas = option_ids.split(ANSWER_SEPARATOR);
                            String[] data_texts = null;
                            if (!Strings.isNullOrEmpty(option_texts))
                                data_texts = option_texts.split(ANSWER_SEPARATOR);

                            List dataList = new ArrayList(Arrays.asList(datas));
                            map.put(idAnswer, dataList);
                            if (AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(answerType)) {
                                map.put(idAnswerText, new ArrayList(Arrays.asList(data_texts)));
                            }

                            if (AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(answerType)) {
                                for (int i = 0; i < datas.length; i++) {
                                    if ("999".equals(datas[i])) {
                                        map.put(idAnswerText, data_texts[0]);
                                    }
                                }
                            }
                        } else {//choose only one
                            option_ids = bean.getOption_ids();
                            option_texts = bean.getOption_texts();
                            map.put(idAnswer, option_ids);
                            if (AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(answerType)) {
                                map.put(idAnswerText, option_texts);
                            } else if (AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(answerType)
                                    && "999".equals(option_ids)) {
                                map.put(idAnswerText, option_texts);
                            }
                        }
                    } else if (AnswerType.RADIO.equals(answerType)
                            || AnswerType.RADIO_WITH_DESCRIPTION.equals(answerType)
                            || AnswerType.DROPDOWN.equals(answerType)
                            || AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(answerType)) {
                        boolean isNotNull = !Strings.isNullOrEmpty(option_ids);
                        boolean isSeparated = false;
                        option_ids = bean.getOption_ids();
                        option_texts = bean.getOption_texts();
                        map.put(idAnswer, option_ids);
                        if ((AnswerType.RADIO_WITH_DESCRIPTION.equals(answerType) || AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(answerType))
                                && "999".equals(option_ids)) {
                            map.put(idAnswerText, option_texts);
                        }
                    } else if (AnswerType.IMAGE.equals(answerType) || AnswerType.SIGNATURE.equals(answerType) || AnswerType.DRAWING.equals(answerType)
                            || AnswerType.IMAGE_WITH_GEODATA.equals(answerType) || AnswerType.IMAGE_WITH_GEODATA_GPS.equals(answerType)
                            || AnswerType.IMAGE_WITH_GEODATA_TIME.equals(answerType) || AnswerType.IMAGE_WITH_GPS_TIME.equals(answerType)
                            || AnswerType.IMAGE_WITH_TIME.equals(answerType)) {
//	    				data:image/jpeg;base64,
                        text_image = bean.getText_image();
                        if (!Strings.isNullOrEmpty(text_image)) {
                            text_image = TEXT_IMAGE_BASE64 + text_image;
                        }
                        map.put(idAnswer, text_image);
                        if (AnswerType.IMAGE_WITH_GEODATA.equals(answerType) || AnswerType.IMAGE_WITH_GEODATA_GPS.equals(answerType)
                                || AnswerType.IMAGE_WITH_GEODATA_TIME.equals(answerType) || AnswerType.IMAGE_WITH_GPS_TIME.equals(answerType)) {
                            geo_text_image = bean.getGeo_text_image();
                            if (!Strings.isNullOrEmpty(geo_text_image)) {
                                if (geo_text_image.contains(TEXT_IMAGE_LATITUDE)) {
                                    geo_text_image = geo_text_image.replaceFirst(TEXT_IMAGE_LATITUDE, "").replaceAll(TEXT_IMAGE_LONGITUDE, ",");
                                    map.put(idAnswerText, geo_text_image);
                                }
                            }
                        } else if (AnswerType.IMAGE_WITH_TIME.equals(answerType)) {
                            date_time_text = bean.getDate_time_text();
                            map.put(idAnswerText, date_time_text);
                        }
                    } else if (AnswerType.DATE.equals(answerType) || AnswerType.DATE_TIME.equals(answerType) || AnswerType.TIME.equals(answerType)) {
                        date_time_text = bean.getDate_time_text();
                        String date = null;
                        String time = null;
                        if (!Strings.isNullOrEmpty(date_time_text)) {
                            if (AnswerType.DATE.equals(answerType)) {
                                date = date_time_text;
                            } else if (AnswerType.TIME.equals(answerType)) {
                                time = date_time_text;
                            } else {
                                if (date_time_text.contains(ANSWER_SEPARATOR)) {
                                    String[] datas = date_time_text.split(ANSWER_SEPARATOR);
                                    date = datas[0];
                                    time = datas[1];
                                }
                            }
                        }
                        map.put(idAnswer, date);
                        map.put(idAnswerText, time);
                    } else if (AnswerType.GEODATA_TIME.equals(bean.getAnswer_type())
                            || AnswerType.GPS_TIME.equals(bean.getAnswer_type())) {
                        geo_text_image = bean.getGeo_text_image();
                        if (!Strings.isNullOrEmpty(geo_text_image)) {
                            if (geo_text_image.contains(TEXT_IMAGE_LATITUDE)) {
                                geo_text_image = geo_text_image.replaceFirst(TEXT_IMAGE_LATITUDE, "").replaceAll(TEXT_IMAGE_LONGITUDE, ",");
                                map.put(idAnswer, geo_text_image);
                            }
                        }
                        date_time_text = bean.getDate_time_text();
                        if (!Strings.isNullOrEmpty(date_time_text)) {
                            map.put(idAnswerText, date_time_text);
                        }
                    } else {
                        map.put(idAnswer, bean.getText());
                    }
                }
            }
        }

        return map;
    }

    public void save(Map map, String user, AnswerInputAssignmentManager manager, Integer seq_id) throws CoreException {
        if (null != map) {
            if (!map.isEmpty()) {
                Date current = new Date();
                String schemeId = (String) map.get("schemeId");

                map.remove("task");
                map.remove("nextGroup");
                map.remove("schemeId");
                map.remove("seq_id");

                List listAnswer = new ArrayList();
                String idTemp = null;
                Object[] keys = map.keySet().toArray();
                Object[] values = map.values().toArray();
                for (int i = 0; i < keys.length; i++) {
                    String key = (String) keys[i];
                    String answKey = key.replaceAll(MapManager.ANSWER_SEPARATOR_ID, "");
                    String[] answKeys = answKey.split(MapManager.ANSWER_SEPARATOR_ANSWKEY);

                    String answerType = answKeys[0];
                    String groupId = answKeys[1];
                    String id = answKeys[2].replaceAll("text", "");
                    if (null == idTemp) {
                        idTemp = id;
                    }

                    if (null != values[i]) {
                        if (values[i].getClass().equals(ArrayList.class) || values[i].getClass().equals(List.class)) {
                            BeanAnswer beanAnswer = new BeanAnswer(seq_id, schemeId, answerType, Integer.parseInt(groupId), Integer.parseInt(id));
                            beanAnswer.setQuestion_code(key);
                            beanAnswer.setCreate_date(current);
                            beanAnswer.setUser(user);
                            beanAnswer.setText_answers(((ArrayList) values[i]));
                            listAnswer.add(beanAnswer);
                        } else {
                            BeanAnswer beanAnswer = new BeanAnswer(seq_id, schemeId, answerType, Integer.parseInt(groupId), Integer.parseInt(id));
                            beanAnswer.setQuestion_code(key);
                            beanAnswer.setCreate_date(current);
                            beanAnswer.setUser(user);
                            beanAnswer.setText_answer(String.valueOf(values[i]));
                            listAnswer.add(beanAnswer);
                        }
                    }
                }

                if (!listAnswer.isEmpty()) {
                    Collections.sort(listAnswer);
                    manager.insertTempAnswer(this.listClearing(listAnswer));
                }

            }
        }
    }

    public List listClearing(List list) {
        if (null == list)
            return null;

        List fixList = new ArrayList();
        List ret = new ArrayList();
        int index = 0;
        int max = list.size();

        BeanAnswer temp = null;
        for (int i = 0; i < max; i++) {
            BeanAnswer bean = (BeanAnswer) list.get(i);
            if (null == temp) {
                temp = bean;
            }
            if ((temp.getQuestion_code().replaceAll(ANSWER_SEPARATOR_ID, "") + "text").equals(bean.getQuestion_code().replaceAll(ANSWER_SEPARATOR_ID, ""))) {
                temp.setText_answer_texts(bean.getText_answers());
                temp.setText_answer_text(bean.getText_answer());
                index = index - 1;
                ret.remove(index);
                ret.add(index, temp);
            } else {
                ret.add(index, bean);
            }

            temp = bean;
            index++;
        }

        if (!ret.isEmpty()) {
            for (Object o : ret) {
                BeanAnswer bean = (BeanAnswer) o;

                if (AnswerType.MULTIPLE.equals(bean.getAnswer_type())
                        || AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(bean.getAnswer_type())
                        || AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(bean.getAnswer_type())) {
                    if (null != bean.getText_answers()) {//choose morethan one
                        List listAnswer = bean.getText_answers();
                        for (int i = 0; i < listAnswer.size(); i++) {
                            BeanAnswer newBean = bean.cloneBean();
                            String val = String.valueOf(listAnswer.get(i));
                            String valText = null;
                            if (AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(bean.getAnswer_type())) {
                                valText = String.valueOf(bean.getText_answer_texts().get(i));
                            } else if (AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(bean.getAnswer_type())
                                    && val.equals("999")) {
                                valText = bean.getText_answer_text();
                            }
                            newBean.setOption_id(val);
                            newBean.setOption_text(valText);
                            fixList.add(newBean);
                        }
                    } else {//choose only one
                        String val = bean.getText_answer();
                        String valText = null;
                        if (AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(bean.getAnswer_type())) {
                            valText = bean.getText_answer_text();
                        } else if (AnswerType.MULTIPLE_ONE_DESCRIPTION.equals(bean.getAnswer_type())
                                && val.equals("999")) {
                            valText = bean.getText_answer_text();
                        }
                        bean.setOption_id(val);
                        bean.setOption_text(valText);
                        fixList.add(bean);
                    }
                } else if (AnswerType.RADIO.equals(bean.getAnswer_type())
                        || AnswerType.RADIO_WITH_DESCRIPTION.equals(bean.getAnswer_type())
                        || AnswerType.DROPDOWN.equals(bean.getAnswer_type())
                        || AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(bean.getAnswer_type())) {
                    if (null != bean.getText_answers()) {//choose morethan one
                        List listAnswer = bean.getText_answers();
                        for (int i = 0; i < listAnswer.size(); i++) {
                            BeanAnswer newBean = bean.cloneBean();
                            String val = String.valueOf(listAnswer.get(i));
                            String valText = null;
                            if ((AnswerType.RADIO_WITH_DESCRIPTION.equals(bean.getAnswer_type())
                                    || AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(bean.getAnswer_type()))
                                    && val.equals("999")) {
                                valText = bean.getText_answer_text();
                            }
                            newBean.setOption_id(val);
                            newBean.setOption_text(valText);
                            fixList.add(newBean);
                        }
                    } else {//choose only one
                        String val = bean.getText_answer();
                        String valText = null;
                        if ((AnswerType.RADIO_WITH_DESCRIPTION.equals(bean.getAnswer_type())
                                || AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(bean.getAnswer_type()))
                                && val.equals("999")) {
                            valText = bean.getText_answer_text();
                        }
                        bean.setOption_id(val);
                        bean.setOption_text(valText);
                        fixList.add(bean);
                    }
                } else if (AnswerType.IMAGE.equals(bean.getAnswer_type()) || AnswerType.SIGNATURE.equals(bean.getAnswer_type()) || AnswerType.DRAWING.equals(bean.getAnswer_type())
                        || AnswerType.IMAGE_WITH_GEODATA.equals(bean.getAnswer_type()) || AnswerType.IMAGE_WITH_GEODATA_GPS.equals(bean.getAnswer_type())
                        || AnswerType.IMAGE_WITH_GEODATA_TIME.equals(bean.getAnswer_type()) || AnswerType.IMAGE_WITH_GPS_TIME.equals(bean.getAnswer_type())
                        || AnswerType.IMAGE_WITH_TIME.equals(bean.getAnswer_type())) {
//    				data:image/jpeg;base64,
                    String imageBase64 = bean.getText_answer();
                    if (!Strings.isNullOrEmpty(imageBase64)) {
                        imageBase64 = imageBase64.replaceFirst("data:image/jpeg;base64,", "");
                    }
                    bean.setText_image(imageBase64);
                    if (AnswerType.IMAGE_WITH_GEODATA.equals(bean.getAnswer_type()) || AnswerType.IMAGE_WITH_GEODATA_GPS.equals(bean.getAnswer_type())
                            || AnswerType.IMAGE_WITH_GEODATA_TIME.equals(bean.getAnswer_type()) || AnswerType.IMAGE_WITH_GPS_TIME.equals(bean.getAnswer_type())) {
                        bean.setGeo_text_image(bean.getText_answer_text());
                    }
                    if (AnswerType.IMAGE_WITH_TIME.equals(bean.getAnswer_type())) {
                        bean.setDate_time_text(bean.getText_answer_text());
                    }
                    fixList.add(bean);
                } else if (AnswerType.DATE.equals(bean.getAnswer_type())
                        || AnswerType.DATE_TIME.equals(bean.getAnswer_type())
                        || AnswerType.TIME.equals(bean.getAnswer_type())) {
                    String date = bean.getText_answer();
                    if (AnswerType.DATE_TIME.equals(bean.getAnswer_type())) {
                        String time = bean.getText_answer_text();
                        date = date + ANSWER_SEPARATOR + time;
                    } else if (AnswerType.TIME.equals(bean.getAnswer_type())) {
                        String time = bean.getText_answer_text();
                        date = time;
                        bean.setQuestion_code(ANSWER_SEPARATOR_ID
                                + bean.getAnswer_type()
                                + ANSWER_SEPARATOR_ANSWKEY
                                + bean.getQuestion_group_id()
                                + ANSWER_SEPARATOR_ANSWKEY
                                + bean.getQuestion_id()
                                + ANSWER_SEPARATOR_ID);
                    }
                    bean.setDate_time_text(date);
                    fixList.add(bean);
                } else if (AnswerType.GEODATA_TIME.equals(bean.getAnswer_type())
                        || AnswerType.GPS_TIME.equals(bean.getAnswer_type())) {
                    bean.setDate_time_text(bean.getText_answer_text());
                    bean.setGeo_text_image(bean.getText_answer());
                    fixList.add(bean);
                } else {
                    bean.setText(bean.getText_answer());
                    fixList.add(bean);

                }
            }
        }

        return fixList;
    }
}
