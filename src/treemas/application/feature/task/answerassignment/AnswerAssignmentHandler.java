package treemas.application.feature.task.answerassignment;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.feature.general.ComboManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.format.TreemasDateFormatter;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AnswerAssignmentHandler extends FeatureActionBase {

    private Map excConverterMap = new HashMap();
    private AnswerAssignmentManager manager = new AnswerAssignmentManager();
    private ComboManager comboManager = new ComboManager();

    public AnswerAssignmentHandler() {
        TreemasDateFormatter convertDMYTime = TreemasDateFormatter.getInstance();
        // this.excConverterMap.put("rfaDate", convertDMYTime);
        this.excConverterMap.put("assignmentDate", convertDMYTime);
        this.excConverterMap.put("retrieveDate", convertDMYTime);
        this.excConverterMap.put("submitDate", convertDMYTime);
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        AnswerAssignmentForm answerAssignmentForm = (AnswerAssignmentForm) form;
        answerAssignmentForm.getPaging().calculationPage();
        String action = answerAssignmentForm.getTask();
        answerAssignmentForm.getSearch().setUserLogin(this.getLoginId(request));
        if (Global.WEB_TASK_SHOW.equals(action)) {
            this.loadComboScheme(request);
            this.loadPriority(request);
        } else if (Global.WEB_TASK_LOAD.equals(action)
                || Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadList(request, answerAssignmentForm);
        }
        System.out.println("ini adalah parent selesai");
        return this.getNextPage(action, mapping);
    }

    private void loadComboScheme(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboScheme();
        request.setAttribute("comboScheme", list);
    }

    private void loadPriority(HttpServletRequest request) throws CoreException {
        List list = comboManager.getComboPriority();
        request.setAttribute("comboPriority", list);
    }


    private void loadList(HttpServletRequest request,
                          AnswerAssignmentForm answerAssignmentForm) throws CoreException {

        int targetPageNo = answerAssignmentForm.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(answerAssignmentForm.getTask())) {
            targetPageNo = 1;
        }
        PageListWrapper result = this.manager.getAllAssignmentSurveyor(
                targetPageNo, answerAssignmentForm.getSearch(),
                this.getLoginId(request));
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list,
                AnswerAssignmentForm.class, excConverterMap);
        answerAssignmentForm.getPaging().setCurrentPageNo(targetPageNo);
        answerAssignmentForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listAnswerAssignment", list);
        this.loadComboScheme(request);
        this.loadPriority(request);
    }

    // ------------------------------------------------

    // // - LOAD DETAIL SURVEY -
    // private void loadDetailList (HttpServletRequest request,
    // AnswerAssignmentForm answerAssignmentForm)
    // throws CoreException {
    //
    // }
    //
    // //----------------------------------------------------------
    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();

        switch (code) {
            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((AnswerAssignmentForm) form).setTask(((AnswerAssignmentForm) form)
                .resolvePreviousTask());
        String task = ((AnswerAssignmentForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
