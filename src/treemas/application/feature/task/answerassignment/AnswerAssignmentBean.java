package treemas.application.feature.task.answerassignment;

import treemas.application.feature.task.AssignmentBean;


public class AnswerAssignmentBean extends AssignmentBean {

    private int questionGroupId;
    private int questionId;
    private int optionAnswerId;
    private String lookupId;
    private String textAnswer;
    private String questionText;
    private String optionAnswerText;
    private String imagePath;
    private double gpsLatitude;
    private double gpsLongitude;
    private int mcc;
    private int mnc;
    private int lac;
    private int cellId;
    private String isGPS;
    private int accuracy;
    private int oldOptionAnswerId;
    private String oldOptionAnswerText;
    private String oldTextAnswer;
    private String oldLookupId;
    private String finOptionAnswerId;
    private String finOptionAnswerText;
    private String finTextAnswer;
    private String finLookupId;
    private String finUseImage;
    private String provider;


    public int getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(int questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getOptionAnswerId() {
        return optionAnswerId;
    }

    public void setOptionAnswerId(int optionAnswerId) {
        this.optionAnswerId = optionAnswerId;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public String getTextAnswer() {
        return textAnswer;
    }

    public void setTextAnswer(String textAnswer) {
        this.textAnswer = textAnswer;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getOptionAnswerText() {
        return optionAnswerText;
    }

    public void setOptionAnswerText(String optionAnswerText) {
        this.optionAnswerText = optionAnswerText;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(int gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(int gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public int getMnc() {
        return mnc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }

    public int getLac() {
        return lac;
    }

    public void setLac(int lac) {
        this.lac = lac;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public String getIsGPS() {
        return isGPS;
    }

    public void setIsGPS(String isGPS) {
        this.isGPS = isGPS;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public int getOldOptionAnswerId() {
        return oldOptionAnswerId;
    }

    public void setOldOptionAnswerId(int oldOptionAnswerId) {
        this.oldOptionAnswerId = oldOptionAnswerId;
    }

    public String getOldOptionAnswerText() {
        return oldOptionAnswerText;
    }

    public void setOldOptionAnswerText(String oldOptionAnswerText) {
        this.oldOptionAnswerText = oldOptionAnswerText;
    }

    public String getOldTextAnswer() {
        return oldTextAnswer;
    }

    public void setOldTextAnswer(String oldTextAnswer) {
        this.oldTextAnswer = oldTextAnswer;
    }

    public String getOldLookupId() {
        return oldLookupId;
    }

    public void setOldLookupId(String oldLookupId) {
        this.oldLookupId = oldLookupId;
    }

    public String getFinOptionAnswerId() {
        return finOptionAnswerId;
    }

    public void setFinOptionAnswerId(String finOptionAnswerId) {
        this.finOptionAnswerId = finOptionAnswerId;
    }

    public String getFinOptionAnswerText() {
        return finOptionAnswerText;
    }

    public void setFinOptionAnswerText(String finOptionAnswerText) {
        this.finOptionAnswerText = finOptionAnswerText;
    }

    public String getFinTextAnswer() {
        return finTextAnswer;
    }

    public void setFinTextAnswer(String finTextAnswer) {
        this.finTextAnswer = finTextAnswer;
    }

    public String getFinLookupId() {
        return finLookupId;
    }

    public void setFinLookupId(String finLookupId) {
        this.finLookupId = finLookupId;
    }

    public String getFinUseImage() {
        return finUseImage;
    }

    public void setFinUseImage(String finUseImage) {
        this.finUseImage = finUseImage;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
