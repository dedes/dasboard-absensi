package treemas.application.feature.task.answerassignment;

import treemas.util.ibatis.SearchParameter;

public class AnswerAssignmentSearch extends SearchParameter {
    private String tglStart;
    private String tglEnd;
    private String status;
    private String status2hidden;
    private String scheme;
    private String priority;
    private String orderField;
    private String branch;
    private String userLogin;

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getTglStart() {
        return tglStart;
    }

    public void setTglStart(String tglStart) {
        this.tglStart = tglStart;
    }

    public String getTglEnd() {
        return tglEnd;
    }

    public void setTglEnd(String tglEnd) {
        this.tglEnd = tglEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus2hidden() {
        return status2hidden;
    }

    public void setStatus2hidden(String status2hidden) {
        this.status2hidden = status2hidden;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getOrderField() {
        return orderField;
    }

    public void setOrderField(String orderField) {
        this.orderField = orderField;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
