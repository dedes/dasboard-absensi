package treemas.application.feature.task.answerassignment;

import treemas.base.feature.FeatureFormBase;

public class AnswerAssignmentForm extends FeatureFormBase {

    private int questionId;
    private int optionAnswerId;
    private String lookupId;
    private String textAnswer;
    private String questionText;
    private String optionAnswerText;
    private String imagePath;
    private int gpsLatitude;
    private int gpsLongitude;
    private int mcc;
    private int mnc;
    private int lac;
    private int cellId;
    private String isGPS;
    private int accuracy;
    private int oldOptionAnswerId;
    private String oldOptionAnswerText;
    private String oldTextAnswer;
    private String oldLookupId;
    private String finOptionAnswerId;
    private String finOptionAnswerText;
    private String finTextAnswer;
    private String finLookupId;
    private String finUseImage;
    private String provider;
    private String mobileAssignmentId;
    private String surveyorId;
    private String surveyorName;
    private String status;
    private String statusDescription;
    private String customerName;
    private String customerAddress;
    private String customerPhone;
    private String schemeId;
    private String schemeDescription;
    private String priorityId;
    private String priorityDescription;
    private String rfaDate;
    private String assignmentDate;
    private String submitDate;
    private String retrieveDate;
    private String notes;
    private String branchId;
    private String branchName;

    private AnswerAssignmentSearch search = new AnswerAssignmentSearch();

    private int questionGroupId;

    public int getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(int questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getOptionAnswerId() {
        return optionAnswerId;
    }

    public void setOptionAnswerId(int optionAnswerId) {
        this.optionAnswerId = optionAnswerId;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public String getTextAnswer() {
        return textAnswer;
    }

    public void setTextAnswer(String textAnswer) {
        this.textAnswer = textAnswer;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getOptionAnswerText() {
        return optionAnswerText;
    }

    public void setOptionAnswerText(String optionAnswerText) {
        this.optionAnswerText = optionAnswerText;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(int gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public int getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(int gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public int getMnc() {
        return mnc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }

    public int getLac() {
        return lac;
    }

    public void setLac(int lac) {
        this.lac = lac;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public String getIsGPS() {
        return isGPS;
    }

    public void setIsGPS(String isGPS) {
        this.isGPS = isGPS;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public int getOldOptionAnswerId() {
        return oldOptionAnswerId;
    }

    public void setOldOptionAnswerId(int oldOptionAnswerId) {
        this.oldOptionAnswerId = oldOptionAnswerId;
    }

    public String getOldOptionAnswerText() {
        return oldOptionAnswerText;
    }

    public void setOldOptionAnswerText(String oldOptionAnswerText) {
        this.oldOptionAnswerText = oldOptionAnswerText;
    }

    public String getOldTextAnswer() {
        return oldTextAnswer;
    }

    public void setOldTextAnswer(String oldTextAnswer) {
        this.oldTextAnswer = oldTextAnswer;
    }

    public String getOldLookupId() {
        return oldLookupId;
    }

    public void setOldLookupId(String oldLookupId) {
        this.oldLookupId = oldLookupId;
    }

    public String getFinOptionAnswerId() {
        return finOptionAnswerId;
    }

    public void setFinOptionAnswerId(String finOptionAnswerId) {
        this.finOptionAnswerId = finOptionAnswerId;
    }

    public String getFinOptionAnswerText() {
        return finOptionAnswerText;
    }

    public void setFinOptionAnswerText(String finOptionAnswerText) {
        this.finOptionAnswerText = finOptionAnswerText;
    }

    public String getFinTextAnswer() {
        return finTextAnswer;
    }

    public void setFinTextAnswer(String finTextAnswer) {
        this.finTextAnswer = finTextAnswer;
    }

    public String getFinLookupId() {
        return finLookupId;
    }

    public void setFinLookupId(String finLookupId) {
        this.finLookupId = finLookupId;
    }

    public String getFinUseImage() {
        return finUseImage;
    }

    public void setFinUseImage(String finUseImage) {
        this.finUseImage = finUseImage;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(String mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public String getSurveyorId() {
        return surveyorId;
    }

    public void setSurveyorId(String surveyorId) {
        this.surveyorId = surveyorId;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    public String getPriorityDescription() {
        return priorityDescription;
    }

    public void setPriorityDescription(String priorityDescription) {
        this.priorityDescription = priorityDescription;
    }

    public String getRfaDate() {
        return rfaDate;
    }

    public void setRfaDate(String rfaDate) {
        this.rfaDate = rfaDate;
    }

    public String getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(String assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getRetrieveDate() {
        return retrieveDate;
    }

    public void setRetrieveDate(String retrieveDate) {
        this.retrieveDate = retrieveDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public AnswerAssignmentSearch getSearch() {
        return search;
    }

    public void setSearch(AnswerAssignmentSearch search) {
        this.search = search;
    }
}
