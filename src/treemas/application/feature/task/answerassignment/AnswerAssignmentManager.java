package treemas.application.feature.task.answerassignment;

import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class AnswerAssignmentManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_ANSWER_ASSIGNMENT;

    public PageListWrapper getAllAssignmentSurveyor(int numberPage,
                                                    AnswerAssignmentSearch searchParam, String userLogin) throws CoreException {
        PageListWrapper result = new PageListWrapper();

        String searchValue = searchParam.getSearchValue1();


        searchValue = searchValue != null ? searchValue.trim().toUpperCase() : searchValue;
        searchParam.setPage(numberPage);
        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAllTaskList", searchParam);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", searchParam);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }
}
