package treemas.application.feature.task.approval;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.general.ComboManager;
import treemas.application.feature.task.input.InputAssignmentManager;
import treemas.application.user.UserBean;
import treemas.application.user.UserManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.gcm.Model;
import treemas.util.gcm.PostMan;
import treemas.util.mail.MailBean;
import treemas.util.mail.MailSender;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.ApplicationResources;
import treemas.util.tool.ConfigurationProperties;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ApprovalAssignmentHandler extends FeatureActionBase {

    private ApprovalAssignmentManager manager = new ApprovalAssignmentManager();
    private ComboManager comboManager = new ComboManager();
    private MailSender mail = new MailSender();
    private UserManager managerUser = new UserManager();
    private ApplicationResources resources = ApplicationResources.getInstance();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        ApprovalAssignmentForm frm = (ApprovalAssignmentForm) form;
        String task = frm.getTask();
        frm.getPaging().calculationPage();

        if (Global.WEB_TASK_LOAD.equals(task) || Global.WEB_TASK_SEARCH.equals(task) || Global.WEB_TASK_VIEW.equals(task)) {
            this.load(request, frm);
        } else if (Global.WEB_TASK_APPROVE.equals(task)) {
            this.approval(request, frm, "A");
        } else if (Global.WEB_TASK_REJECT.equals(task)) {
            this.approval(request, frm, "J");
        }

        return this.getNextPage(task, mapping);

    }

    private void load(HttpServletRequest request, ApprovalAssignmentForm form)
            throws AppException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;

        PageListWrapper result = this.manager.getAllUnapprove(pageNumber, form.getSearch(), this.getLoginBean(request).getBranchId());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);

        this.loadComboScheme(request);
        this.loadComboPIC(request);
    }

    private void loadComboScheme(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboScheme(this.getLoginId(request));
        request.setAttribute("comboScheme", list);
    }

    private void loadComboPIC(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getSurveyorName(this.getLoginBean(request).getBranchId(), "");
        request.setAttribute("comboPIC", list);
    }

    private void approval(HttpServletRequest request, ApprovalAssignmentForm form, String value)
            throws AppException {
        try {
            String[] member = form.getSearch().getMemberList();
            InputAssignmentManager inputAssignmentManager = new InputAssignmentManager();
            PostMan postMan = new PostMan();

            for (int i = 0; i < member.length; i++) {
                this.manager.updateApproval(this.getLoginBean(request).getBranchId(), value, member[i]);
                this.manager.insertHistory(value, this.getLoginId(request), member[i]);
//				List surveyorID = null;
//				try {
//					surveyorID = this.manager.getSurveyorId(member[i]);
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//				String regId = inputAssignmentManager.getRegId((String)surveyorID.get(0));
//				if(!Strings.isNullOrEmpty(regId)){
//					Model model =  postMan.generateInput(regId, String.valueOf(form.getMobileAssignmentId()), this.getLoginBean(request).getFullName());
//					String apiKey = ConfigurationProperties.getInstance().get(PropertiesKey.API_KEY);
//					postMan.post(apiKey, model);
//				}
            }

            if ("A".equals(value)) {
                List listUserID = null;
                try {
                    String assignments = "";
                    List<Integer> list = new ArrayList<Integer>();
                    for (int i = 0; i < member.length; i++) {
                        list.add(Integer.parseInt(member[i]));
                    }
                    listUserID = this.manager.getSurveyorId(list);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < listUserID.size(); i++) {
                    ApprovalAssignmentBean bean = (ApprovalAssignmentBean) listUserID.get(i);
                    String regId = inputAssignmentManager.getRegId(bean.getSurveyorId());
                    if (!Strings.isNullOrEmpty(regId)) {
                        Model model = postMan.generateInput(regId, bean.getCount(), this.getLoginBean(request).getFullName());
                        String apiKey = ConfigurationProperties.getInstance().get(PropertiesKey.API_KEY);
                        postMan.post(apiKey, model);
                    }
                }
                request.setAttribute("message", resources.getMessage("common.approve", member.length + " Data"));
            } else if ("J".equals(value)) {

                UserBean beanUser = new UserBean();
                beanUser = this.managerUser.getSingleUser(this.getLoginId(request));

                MailBean bMail = new MailBean();
                bMail.setFrom(new InternetAddress(ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_SENDER_EMAIL)));

                List userNemail = null;
                List emailMessage = null;
                String userCreate = "";
                String userName = "";
                String email = "";
                String schemeDesc = "";
                String applNo = "";
                String custName = "";
                try {
                    userNemail = this.manager.getUserNEmail();
                    for (int i = 0; i < userNemail.size(); i++) {
                        ApprovalAssignmentBean beanUE = (ApprovalAssignmentBean) userNemail.get(i);
                        userCreate = beanUE.getUsrcrt();
                        userName = beanUE.getUsrname();
                        email = beanUE.getEmail();

                        bMail.setMessage(resources.getMessage("mail.survey.header", new Object[]{userName}));
                        bMail.setTo(new InternetAddress(email));

                        emailMessage = this.manager.getEmailMessage(userCreate);
                        for (int j = 0; j < emailMessage.size(); j++) {
                            ApprovalAssignmentBean beanEM = (ApprovalAssignmentBean) emailMessage.get(j);
                            schemeDesc = beanEM.getSchemeDescription();
                            applNo = beanEM.getApplNo();
                            custName = beanEM.getCustomerName();

                            bMail.setMessage(bMail.getMessage() + " " + (j + 1) + ". " + applNo + " - " + custName + " - " + schemeDesc + " - " + userCreate + "<br>");
                        }
                        bMail.setMessage(bMail.getMessage() + resources.getMessage("mail.survey.description"));
                        bMail.setMessage(bMail.getMessage() + resources.getMessage("mail.survey.end", new Object[]{this.getLoginId(request), beanUser.getFullname()}));

                        this.mail.send(bMail);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                request.setAttribute("message", resources.getMessage("common.reject", member.length + "Data "));
            } else {
                request.setAttribute("message", resources.getMessage("common.invalid.approval"));
            }
            load(request, form);
        } catch (CoreException e) {
            throw e;
        } catch (AddressException e) {
            e.printStackTrace();
        }
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((ApprovalAssignmentForm) form).setTask(((ApprovalAssignmentForm) form).resolvePreviousTask());

        String task = ((ApprovalAssignmentForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
