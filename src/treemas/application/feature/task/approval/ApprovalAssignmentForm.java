package treemas.application.feature.task.approval;

import treemas.base.feature.FeatureFormBase;


public class ApprovalAssignmentForm extends FeatureFormBase {

    private String mobileAssignmentId;
    private String assignmentStatus;
    private String notes;
    private String usrupd;
    private String branchId;
    private String branchName;
    private String surveyorId;
    private String schemeId;
    private String surveyorName;
    private String schemeDescription;
    private String applNo;
    private String customerName;
    private String customerAddress;
    private String schemeRole;

    private ApprovalAssignmentSearch search = new ApprovalAssignmentSearch();

    public ApprovalAssignmentForm() {
    }

    public String getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(String mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public String getAssignmentStatus() {
        return assignmentStatus;
    }

    public void setAssignmentStatus(String assignmentStatus) {
        this.assignmentStatus = assignmentStatus;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public ApprovalAssignmentSearch getSearch() {
        return search;
    }

    public void setSearch(ApprovalAssignmentSearch search) {
        this.search = search;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getSurveyorId() {
        return surveyorId;
    }

    public void setSurveyorId(String surveyorId) {
        this.surveyorId = surveyorId;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getApplNo() {
        return applNo;
    }

    public void setApplNo(String applNo) {
        this.applNo = applNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getSchemeRole() {
        return schemeRole;
    }

    public void setSchemeRole(String schemeRole) {
        this.schemeRole = schemeRole;
    }

}
