package treemas.application.feature.task.approval;

import treemas.base.feature.FeatureManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApprovalAssignmentManager extends FeatureManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_FEATURE_APPROVAL_ASSIGNMENT;

    public PageListWrapper getAllUnapprove(int pageNumber, ApprovalAssignmentSearch search, String branchId) throws AppException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);
        search.setBranchId(branchId);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllUnapprove", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllUnapprove", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public void insertHistory(String assignmentStatus, String usrupd, String mobileAssignmentId) throws AppException {
        try {
//			int seqNo = this.AssignmentHistorySeqNo();
            String temp = this.getNotesUserCreate(mobileAssignmentId);
            String notesUserCreate[] = temp.split("\\|");
            String notes = notesUserCreate[0];
            String usrcrt = notesUserCreate[1];

            Map approvalParameter = new HashMap();
            approvalParameter.put("assignmentStatus", assignmentStatus);
            approvalParameter.put("usrupd", usrupd);
            approvalParameter.put("mobileAssignmentId", mobileAssignmentId);
            approvalParameter.put("notes", notes);
            approvalParameter.put("usrcrt", usrcrt);
//			approvalParameter.put("seqNo", seqNo);
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.insert(STRING_SQL + "insertHistory", approvalParameter);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public void updateApproval(String branchId, String Status, String mobileAssignmentId) throws CoreException {
        ApprovalAssignmentBean bean = new ApprovalAssignmentBean();
        int id = Integer.parseInt(mobileAssignmentId);

        bean.setBranchId(branchId);
        bean.setAssignmentStatus(Status);
        bean.setMobileAssignmentId(id);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Updating Assignment Status "
                        + bean.getMobileAssignmentId());

                this.ibatisSqlMap.update(this.STRING_SQL + "updateStatus",
                        bean);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Updated Assignment Status "
                        + bean.getMobileAssignmentId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            throw new CoreException(se, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

//	public int AssignmentHistorySeqNo() throws AppException {
//		int result = 0;
//		try {
//			try {
//				this.ibatisSqlMap.startTransaction();
//				result = (Integer)this.ibatisSqlMap.queryForObject(STRING_SQL+"AssignmentHistorySeqNo");
//				this.ibatisSqlMap.commitTransaction();
//			} finally {
//				this.ibatisSqlMap.endTransaction();
//			}
//		} catch (SQLException e) {
//			this.logger.printStackTrace(e);
//			throw new AppException(ERROR_DB);
//		}
//		
//		return result;
//	}

    public String getNotesUserCreate(String mobileAssignmentId) throws AppException {
        String result = "";
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                result = (String) this.ibatisSqlMap.queryForObject(STRING_SQL + "getNotesUserCreate", mobileAssignmentId);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public List getUserNEmail() throws SQLException {
        List userList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getUserNEmail");
        return userList;
    }

    public List getEmailMessage(String usrcrt) throws SQLException {
        List mList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getEmailMessage", usrcrt);
        return mList;
    }

    public List getSurveyorId(List<Integer> assignments) throws SQLException {
        return this.ibatisSqlMap.queryForList(this.STRING_SQL + "getSurveyorID", assignments);
    }
}
