package treemas.application.feature.task.approval;

import treemas.application.approval.ApprovalSearch;

public class ApprovalAssignmentSearch extends ApprovalSearch {

    private String surveyorId;
    private String schemeId;
    private String surveyorName;
    private String schemeName;
    private String branchId;
    private String customerName;
    private String schemeRole;

    public ApprovalAssignmentSearch() {
        super();
        this.surveyorId = null;
        this.schemeId = null;
        this.surveyorName = null;
        this.schemeName = null;
        this.branchId = null;
        this.customerName = null;
        this.schemeRole = null;
    }

    public String getSurveyorId() {
        return surveyorId;
    }

    public void setSurveyorId(String surveyorId) {
        this.surveyorId = surveyorId;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSchemeRole() {
        return schemeRole;
    }

    public void setSchemeRole(String schemeRole) {
        this.schemeRole = schemeRole;
    }


}
