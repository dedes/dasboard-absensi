package treemas.application.feature.task;

import java.io.Serializable;

public class MobileAssignmentHistoryForm implements Serializable {
    private String assignmentHistorySeqno;
    private String mobileAssignmentId;
    private String assignmentStatus;
    private String assignmentStatusDescription;
    private String notes;
    private String usrupd;
    private String usrupdName;
    private String dtmupd;

    public String getAssignmentHistorySeqno() {
        return assignmentHistorySeqno;
    }

    public void setAssignmentHistorySeqno(String assignmentHistorySeqno) {
        this.assignmentHistorySeqno = assignmentHistorySeqno;
    }

    public String getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(String mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public String getAssignmentStatus() {
        return assignmentStatus;
    }

    public void setAssignmentStatus(String assignmentStatus) {
        this.assignmentStatus = assignmentStatus;
    }

    public String getAssignmentStatusDescription() {
        return assignmentStatusDescription;
    }

    public void setAssignmentStatusDescription(
            String assignmentStatusDescription) {
        this.assignmentStatusDescription = assignmentStatusDescription;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getUsrupdName() {
        return usrupdName;
    }

    public void setUsrupdName(String usrupdName) {
        this.usrupdName = usrupdName;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }
}
