package treemas.application.feature.task;

import treemas.util.geofence.GeofenceBean;
import treemas.util.tool.Tools;

public class MobileLocationBean extends GeofenceBean {
    private int MCC;
    private int MNC;
    private int LAC;
    private int cellId;

    public MobileLocationBean() {
    }

    public MobileLocationBean(String delimitedLocation) {
        String[] data = delimitedLocation.split(",");
        int splitCount = data.length;
        if (splitCount != 6) {
            throw new IllegalArgumentException("Invalid location string");
        }
        this.latitude = Double.parseDouble(data[0]);
        this.longitude = Double.parseDouble(data[1]);

        if (Tools.isInteger(data[2])) {
            this.cellId = Integer.parseInt(data[2]);
        } else {
            this.cellId = -1;
        }

        if (Tools.isInteger(data[3])) {
            this.MCC = Integer.parseInt(data[3]);
        } else {
            if ("ffff".equals(data[3])) {
                this.MCC = 510; // SET to INDONESIA MCC -- 510
            } else {
                this.MCC = -1;
            }
        }

        String mnc = Tools.replace(data[4].toUpperCase(), "F", "");
        if (Tools.isInteger(mnc)) {
            this.MNC = Integer.parseInt(mnc);
        } else {
            this.MNC = -1;
        }

        String temp = data[5];
        if (temp != null && !"".equals(temp) && !"null".equals(temp)) {
            if (Tools.isInteger(temp))
                this.LAC = Integer.parseInt(temp);
            else
                this.LAC = -1;
        }
    }

    public MobileLocationBean(int MCC, int MNC, int LAC, int cellId) {
        this.MCC = MCC;
        this.MNC = MNC;
        this.LAC = LAC;
        this.cellId = cellId;
    }

    public MobileLocationBean(double latitude, double longitude, int MCC,
                              int MNC, int LAC, int cellId) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.MCC = MCC;
        this.MNC = MNC;
        this.LAC = LAC;
        this.cellId = cellId;
    }

    public MobileLocationBean(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getMCC() {
        return MCC;
    }

    public void setMCC(int mcc) {
        MCC = mcc;
    }

    public int getMNC() {
        return MNC;
    }

    public void setMNC(int mnc) {
        MNC = mnc;
    }

    public int getLAC() {
        return LAC;
    }

    public void setLAC(int lac) {
        LAC = lac;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

}
