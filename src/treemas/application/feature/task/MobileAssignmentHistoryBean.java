package treemas.application.feature.task;

import java.io.Serializable;
import java.util.Date;

public class MobileAssignmentHistoryBean implements Serializable {
    private Integer assignmentHistorySeqno;
    private Integer mobileAssignmentId;
    private String assignmentStatus;
    private String assignmentStatusDescription;
    private String notes;
    private String usrupd;
    private String usrupdName;
    private Date dtmupd;

    public Integer getAssignmentHistorySeqno() {
        return assignmentHistorySeqno;
    }

    public void setAssignmentHistorySeqno(Integer assignmentHistorySeqno) {
        this.assignmentHistorySeqno = assignmentHistorySeqno;
    }

    public Integer getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(Integer mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public String getAssignmentStatus() {
        return assignmentStatus;
    }

    public void setAssignmentStatus(String assignmentStatus) {
        this.assignmentStatus = assignmentStatus;
    }

    public String getAssignmentStatusDescription() {
        return assignmentStatusDescription;
    }

    public void setAssignmentStatusDescription(
            String assignmentStatusDescription) {
        this.assignmentStatusDescription = assignmentStatusDescription;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getUsrupdName() {
        return usrupdName;
    }

    public void setUsrupdName(String usrupdName) {
        this.usrupdName = usrupdName;
    }

    public Date getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(Date dtmupd) {
        this.dtmupd = dtmupd;
    }
}
