package treemas.application.feature.task.pretask;

import treemas.base.search.SearchForm;


public class PreTaskSearch extends SearchForm {
    private String tglStart;
    private String tglEnd;
    private String scheme;
    private String priority;
    private String branch;
    private String userId;

    public String getTglStart() {
        return tglStart;
    }

    public void setTglStart(String tglStart) {
        this.tglStart = tglStart;
    }

    public String getTglEnd() {
        return tglEnd;
    }

    public void setTglEnd(String tglEnd) {
        this.tglEnd = tglEnd;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
