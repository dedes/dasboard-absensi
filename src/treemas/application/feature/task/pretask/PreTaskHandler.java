package treemas.application.feature.task.pretask;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.feature.general.ComboManager;
import treemas.application.feature.task.AssignmentBean;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.format.TreemasDateFormatter;
import treemas.util.format.TreemasDecimalFormatter;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PreTaskHandler extends FeatureActionBase {
    private Map excConverterMap = new HashMap();
    private Map cordConverterMap = new HashMap();
    private PreTaskManager manager = new PreTaskManager();
    private ComboManager comboManager = new ComboManager();

    public PreTaskHandler() {
        TreemasDateFormatter convertDMYTime = TreemasDateFormatter.getInstance();
        this.excConverterMap.put("assignmentDate", convertDMYTime);
        this.excConverterMap.put("retrieveDate", convertDMYTime);
        this.excConverterMap.put("submitDate", convertDMYTime);

        TreemasDecimalFormatter convertDecimal = TreemasDecimalFormatter.getInstance();
        this.cordConverterMap.put("gpsLatitude", convertDecimal);
        this.cordConverterMap.put("gpsLongitude", convertDecimal);
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        PreCommitteeForm precommitteeForm = (PreCommitteeForm) form;
        precommitteeForm.getPaging().calculationPage();
        String action = precommitteeForm.getTask();

        if (Global.WEB_TASK_SHOW.equals(action)) {
            String loginBranch = "";
//				this.getLoginBean(request).getBranchId();
            this.loadBranch(request, loginBranch);
            precommitteeForm.getSearch().setBranch(loginBranch);

            this.loadComboScheme(request);
            this.loadPriority(request);
        } else if (Global.WEB_TASK_LOAD.equals(action) ||
                Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadList(request, precommitteeForm);
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
            Integer mobileAssignmentId = new Integer(precommitteeForm.getMobileAssignmentId());
            this.getHeader(precommitteeForm, mobileAssignmentId);
            this.loadJawaban(precommitteeForm, mobileAssignmentId);
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            try {
                this.manager.update(precommitteeForm.getListAnswer(),
                        precommitteeForm.getMobileAssignmentId());
            } catch (CoreException me) {
                Integer mobileAssignmentId = new Integer(precommitteeForm.getMobileAssignmentId());
                this.getHeader(precommitteeForm, mobileAssignmentId);
                this.loadJawaban(precommitteeForm, mobileAssignmentId);
                throw me;
            }
            this.loadList(request, precommitteeForm);
        }

        return this.getNextPage(action, mapping);
    }

    private void loadBranch(HttpServletRequest request, String branchId)
            throws CoreException {
        List list = comboManager.getComboCabangByLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadComboScheme(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboScheme(this.getLoginId(request));
        request.setAttribute("comboScheme", list);
    }

    private void loadPriority(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboPriority();
        request.setAttribute("comboPriority", list);
    }

    private void loadList(HttpServletRequest request, PreCommitteeForm precommitteeForm)
            throws CoreException {
        int targetPageNo = precommitteeForm.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(precommitteeForm.getTask())) {
            targetPageNo = 1;
        }
        precommitteeForm.getSearch().setUserId(this.getLoginId(request));
        PageListWrapper result = this.manager.getAllPrecommittee(targetPageNo,
                precommitteeForm.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, PreCommitteeForm.class, excConverterMap);
        precommitteeForm.getPaging().setCurrentPageNo(targetPageNo);
        precommitteeForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listAssignment", list);

        String loginBranch = "";
//			this.getLoginBean(request).getBranchId();
        this.loadBranch(request, loginBranch);

        this.loadComboScheme(request);
        this.loadPriority(request);
    }

    private void getHeader(PreCommitteeForm precommitteeForm, Integer mobileAssignmentId) throws CoreException {
        AssignmentBean bean = null;
        bean = this.manager.getHeader(mobileAssignmentId);
        if (bean != null) {
            BeanConverter.convertBeanToString(bean, precommitteeForm, excConverterMap);
        }
    }

    private void loadJawaban(PreCommitteeForm precommitteeForm, Integer mobileAssignmentId) throws CoreException {
        List result = this.manager.getListAnswers(mobileAssignmentId);

        if (result != null)
            precommitteeForm.setListSize(String.valueOf(result.size()));

        precommitteeForm.setListAnswer(result);
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();

        switch (code) {
            case PreTaskManager.ERROR_CHANGE:
                this.setMessage(request, "common.concurrent", null);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((PreCommitteeForm) form).setTask(((PreCommitteeForm) form).resolvePreviousTask());
        String task = ((PreCommitteeForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
