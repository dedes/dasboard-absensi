package treemas.application.feature.task.pretask;

import treemas.util.constant.AnswerType;

import java.io.Serializable;


public class PreTaskResultBean implements Serializable {
    private Integer mobileAssignmentId;
    private Integer questionGroupId;
    private String questionGroupName;
    private Integer questionId;
    private String questionLabel;
    private String answerType;
    private Integer optionAnswerId;
    private String optionAnswerLabel;
    private String textAnswer;
    private String hasImage;
    private String imagePath;
    private Double gpsLatitude;
    private Double gpsLongitude;
    private String isGps;
    private Integer accuracy;
    private String lookupId;

    private Integer oldOptionAnswerId;
    private String oldOptionAnswerLabel;
    private String oldTextAnswer;
    private String oldLookupId;
    private Integer finalOptionAnswerId;
    private String finalOptionAnswerLabel;
    private String finalTextAnswer;
    private String finalLookupId;
    private String finalUseImage;

    private String isUsingOldAnswer;
    private String isOldOption;
    private String provider;

    public Integer getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(Integer mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public Integer getOptionAnswerId() {
        return optionAnswerId;
    }

    public void setOptionAnswerId(Integer optionAnswerId) {
        this.optionAnswerId = optionAnswerId;
    }

    public String getOptionAnswerLabel() {
        return optionAnswerLabel;
    }

    public void setOptionAnswerLabel(String optionAnswerLabel) {
        this.optionAnswerLabel = optionAnswerLabel;
    }

    public String getTextAnswer() {
        return textAnswer;
    }

    public void setTextAnswer(String textAnswer) {
        this.textAnswer = textAnswer;
    }

    public String getHasImage() {
        return hasImage;
    }

    public void setHasImage(String hasImage) {
        this.hasImage = hasImage;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public String getIsGps() {
        return isGps;
    }

    public void setIsGps(String isGps) {
        this.isGps = isGps;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public Integer getOldOptionAnswerId() {
        return oldOptionAnswerId;
    }

    public void setOldOptionAnswerId(Integer oldOptionAnswerId) {
        this.oldOptionAnswerId = oldOptionAnswerId;
    }

    public String getOldOptionAnswerLabel() {
        return oldOptionAnswerLabel;
    }

    public void setOldOptionAnswerLabel(String oldOptionAnswerLabel) {
        this.oldOptionAnswerLabel = oldOptionAnswerLabel;
    }

    public String getOldTextAnswer() {
        return oldTextAnswer;
    }

    public void setOldTextAnswer(String oldTextAnswer) {
        this.oldTextAnswer = oldTextAnswer;
    }

    public String getOldLookupId() {
        return oldLookupId;
    }

    public void setOldLookupId(String oldLookupId) {
        this.oldLookupId = oldLookupId;
    }

    public Integer getFinalOptionAnswerId() {
        return finalOptionAnswerId;
    }

    public void setFinalOptionAnswerId(Integer finalOptionAnswerId) {
        this.finalOptionAnswerId = finalOptionAnswerId;
    }

    public String getFinalOptionAnswerLabel() {
        return finalOptionAnswerLabel;
    }

    public void setFinalOptionAnswerLabel(String finalOptionAnswerLabel) {
        this.finalOptionAnswerLabel = finalOptionAnswerLabel;
    }

    public String getFinalTextAnswer() {
        return finalTextAnswer;
    }

    public void setFinalTextAnswer(String finalTextAnswer) {
        this.finalTextAnswer = finalTextAnswer;
    }

    public String getFinalLookupId() {
        return finalLookupId;
    }

    public void setFinalLookupId(String finalLookupId) {
        this.finalLookupId = finalLookupId;
    }

    public String getFinalUseImage() {
        return finalUseImage;
    }

    public void setFinalUseImage(String finalUseImage) {
        this.finalUseImage = finalUseImage;
    }

    public String getIsUsingOldAnswer() {
        return isUsingOldAnswer;
    }

    public void setIsUsingOldAnswer(String isUsingOldAnswer) {
        this.isUsingOldAnswer = isUsingOldAnswer;
    }

    public String getIsOldOption() {
        return isOldOption;
    }

    public void setIsOldOption(String isOldOption) {
        this.isOldOption = isOldOption;
    }

    public String getIsImage() {
        if (AnswerType.IMAGE.equals(this.answerType) ||
                AnswerType.IMAGE_WITH_GEODATA.equals(this.answerType) ||
                AnswerType.DRAWING.equals(this.answerType) ||
                AnswerType.SIGNATURE.equals(this.answerType)) {
            return "1";
        } else return "0";
    }

    public void setSelectedFinalAnswer() {
        if ("1".equals(this.isUsingOldAnswer)) {
            if (AnswerType.IMAGE.equals(this.answerType) ||
                    AnswerType.IMAGE_WITH_GEODATA.equals(this.answerType) ||
                    AnswerType.DRAWING.equals(this.answerType)) {
                this.finalUseImage = "0";
            } else {
                if (this.oldOptionAnswerId.intValue() == 0) // untuk nilai 0 krn di form yg empty string diconvert ke 0
                    this.oldOptionAnswerId = null;

                this.finalTextAnswer = this.oldTextAnswer;
                this.finalOptionAnswerId = this.oldOptionAnswerId;
                this.finalOptionAnswerLabel = this.oldOptionAnswerLabel;
                this.finalLookupId = this.oldLookupId;
            }
        } else {
            if (AnswerType.IMAGE.equals(this.answerType) ||
                    AnswerType.IMAGE_WITH_GEODATA.equals(this.answerType) ||
                    AnswerType.DRAWING.equals(this.answerType)) {
                this.finalUseImage = "1";
            } else {
                if (this.optionAnswerId.intValue() == 0) // untuk nilai 0 krn di form yg empty string diconvert ke 0
                    this.optionAnswerId = null;

                this.finalTextAnswer = this.textAnswer;
                this.finalOptionAnswerId = this.optionAnswerId;
                this.finalOptionAnswerLabel = this.optionAnswerLabel;
                this.finalLookupId = this.lookupId;
            }
        }
    }

    public String getIsSameAnswer() {
        if (AnswerType.TEXT.equals(this.answerType) ||
                AnswerType.NUMERIC.equals(this.answerType) ||
                AnswerType.DECIMAL.equals(this.answerType) ||
                AnswerType.DATE.equals(this.answerType) ||
                AnswerType.TIME.equals(this.answerType) ||
                AnswerType.DATE_TIME.equals(this.answerType)) {

            String lama = (this.oldTextAnswer == null) ? "" : this.oldTextAnswer;
            String baru = (this.textAnswer == null) ? "" : this.textAnswer;

            if (lama.toUpperCase().equals(baru.toUpperCase())) {
                return "1";
            } else {
                return "0";
            }
        } else if (AnswerType.DROPDOWN.equals(this.answerType) ||
                AnswerType.RADIO.equals(this.answerType) ||
                AnswerType.MULTIPLE.equals(this.answerType)) {
            Integer lama = (this.oldOptionAnswerId == null) ? new Integer(0) : this.oldOptionAnswerId;
            Integer baru = (this.optionAnswerId == null) ? new Integer(0) : this.optionAnswerId;
            if (lama.intValue() == baru.intValue()) {
                return "1";
            } else {
                return "0";
            }
        } else if (AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(this.answerType) ||
                AnswerType.RADIO_WITH_DESCRIPTION.equals(this.answerType) ||
                AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(this.answerType)) {
            Integer lama = (this.oldOptionAnswerId == null) ? new Integer(0) : this.oldOptionAnswerId;
            Integer baru = (this.optionAnswerId == null) ? new Integer(0) : this.optionAnswerId;
            if (lama.intValue() == baru.intValue()) {
                String lm = (this.oldTextAnswer == null) ? "" : this.oldTextAnswer;
                String br = (this.textAnswer == null) ? "" : this.textAnswer;
                if (lm.toUpperCase().equals(br.toUpperCase())) {
                    return "1";
                } else {
                    return "0";
                }
            } else {
                return "0";
            }
        } else if (AnswerType.LOOKUP.equals(this.answerType)) {
            String lama = (this.oldLookupId == null) ? "" : this.oldLookupId;
            String baru = (this.lookupId == null) ? "" : this.lookupId;
            if (lama.toUpperCase().equals(baru.toUpperCase())) {
                return "1";
            } else {
                return "0";
            }
        } else if (AnswerType.IMAGE.equals(this.answerType) ||
                AnswerType.IMAGE_WITH_GEODATA.equals(this.answerType) ||
                AnswerType.DRAWING.equals(this.answerType)) {
            if ("0".equals(this.hasImage)) {
                return "1";
            } else {
                return "0";
            }
        } else {
            return "0";
        }
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}