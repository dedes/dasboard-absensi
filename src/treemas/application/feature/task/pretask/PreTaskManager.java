package treemas.application.feature.task.pretask;

import treemas.application.feature.task.AssignmentBean;
import treemas.application.feature.task.MobileLocationBean;
import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.AnswerType;
import treemas.util.constant.SQLConstant;
import treemas.util.geofence.CellIdLookupManager;
import treemas.util.geofence.GeofenceBean;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;


public class PreTaskManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_PRETASK;
    private static final String FALSE = "0";
    private static final String PRECOMMITTEE_STATUS = "P";

    public PageListWrapper getAllPrecommittee(int numberPage,
                                              PreTaskSearch searchParam) throws CoreException {
        PageListWrapper result = new PageListWrapper();

        String searchValue = searchParam.getSearchValue1();

        searchValue = searchValue != null ? searchValue.trim().toUpperCase()
                : searchValue;
        searchParam.setPage(numberPage);
        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", searchParam);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", searchParam);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public AssignmentBean getHeader(Integer mobileAssignmentId) throws CoreException {
        AssignmentBean bean = null;
        try {
            bean = (AssignmentBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getHeader", mobileAssignmentId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return bean;
    }

    public List getListAnswers(Integer mobileAssignmentId) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getListAnswers", mobileAssignmentId);

            for (Iterator iterator = result.iterator(); iterator.hasNext(); ) {
                PreTaskResultBean bean = (PreTaskResultBean) iterator.next();
                if (FALSE.equals(bean.getIsGps())) {
                    boolean missingCoordinate = (bean.getGpsLatitude() == null &&
                            bean.getGpsLongitude() == null) ? true : false;

                    if (missingCoordinate) {
                        bean.setMobileAssignmentId(mobileAssignmentId);
                        this.getMissingCoordinate(bean);
                    }
                }
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return result;
    }

    private void getMissingCoordinate(PreTaskResultBean bean) throws SQLException {
        MobileLocationBean location = (MobileLocationBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getLbs", bean);
        CellIdLookupManager cidManager = new CellIdLookupManager();
        GeofenceBean coordinateBean = cidManager.geocodeByCellId(
                location.getMCC(), location.getMNC(),
                location.getLAC(), location.getCellId());

        if (coordinateBean != null) {
            bean.setGpsLatitude(new Double(coordinateBean.getLatitude()));
            bean.setGpsLongitude(new Double(coordinateBean.getLongitude()));
            bean.setAccuracy(new Integer(coordinateBean.getAccuracy()));
            bean.setProvider(coordinateBean.getProvider());
            this.ibatisSqlMap.update(this.STRING_SQL + "updateCoordinate", bean);
        }
    }

    public void update(List listPrecommitteeResult, String mobileAssignmentId) throws CoreException {
        try {
            this.logger.log(ILogger.LEVEL_INFO, "Updating precommittee answer for assignmentid: " + mobileAssignmentId);
            try {
                this.ibatisSqlMap.startTransaction();

                Integer taskId = new Integer(mobileAssignmentId);

                String status = (String) this.ibatisSqlMap.queryForObject(
                        this.STRING_SQL + "getStatusAssignment", taskId);

                if (!PRECOMMITTEE_STATUS.equals(status)) {
                    throw new CoreException(ERROR_CHANGE);
                }

                this.ibatisSqlMap.update(this.STRING_SQL + "updateStatus", taskId);

                for (Iterator iterator = listPrecommitteeResult.iterator(); iterator
                        .hasNext(); ) {
                    PreTaskResultBean bean = (PreTaskResultBean) iterator.next();

                    bean.setSelectedFinalAnswer(); //call function to set final answer
                    this.savePrecommittee(bean);
                }

                this.ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Updated precommittee answer for assignmentid: "
                        + mobileAssignmentId + "");
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (CoreException me) {
            throw me;
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            throw new CoreException(se, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    private void savePrecommittee(PreTaskResultBean bean) throws SQLException {
        String answerType = bean.getAnswerType();
        if (AnswerType.TEXT.equals(answerType) ||
                AnswerType.NUMERIC.equals(answerType) ||
                AnswerType.ACCOUNTING.equals(answerType) ||
                AnswerType.NUMERIC_WITH_CALCULATION.equals(answerType) ||
                AnswerType.DECIMAL.equals(answerType) ||
                AnswerType.DATE.equals(answerType) ||
                AnswerType.TIME.equals(answerType) ||
                AnswerType.DATE_TIME.equals(answerType) ||
                AnswerType.DROPDOWN.equals(answerType) ||
                AnswerType.DROPDOWN_WITH_DESCRIPTION.equals(answerType) ||
                AnswerType.RADIO.equals(answerType) ||
                AnswerType.RADIO_WITH_DESCRIPTION.equals(answerType) ||
                AnswerType.LOOKUP.equals(answerType) ||
                AnswerType.IMAGE.equals(answerType) ||
                AnswerType.IMAGE_WITH_GEODATA.equals(answerType) ||
                AnswerType.DRAWING.equals(answerType) ||
                AnswerType.SIGNATURE.equals(answerType)) {
            this.ibatisSqlMap.update(this.STRING_SQL + "savePrecommittee", bean);
        } else if (AnswerType.MULTIPLE.equals(answerType) ||
                AnswerType.MULTIPLE_WITH_DESCRIPTION.equals(answerType)) {
            if ("1".equals(bean.getIsOldOption())) {
                this.ibatisSqlMap.update(this.STRING_SQL + "savePrecommitteeOldOption", bean);
            } else {
                this.ibatisSqlMap.update(this.STRING_SQL + "savePrecommitteeNewOption", bean);
            }
        }
    }
}
