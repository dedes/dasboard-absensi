package treemas.application.feature.task.uploadcollection;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.general.ComboManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class ExcelUploadCollectionHandler extends FeatureActionBase {
    private final String screenId = "";
    private ExcelUploadCollectionManager manager;
    private ComboManager comboManager = new ComboManager();
    private ExcelUploadCollectionValidator validator;

    public ExcelUploadCollectionHandler() {
        this.pageMap.put("Show", "view");
        this.pageMap.put("SaveExcel", "view");
        this.pageMap.put("Download", "view");

        this.exceptionMap.put("Show", "view");
        this.exceptionMap.put("SaveExcel", "view");
        this.exceptionMap.put("Download", "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.manager = new ExcelUploadCollectionManager(request);
        this.validator = new ExcelUploadCollectionValidator(request);
        ExcelUploadCollectionForm excelUploadForm = (ExcelUploadCollectionForm) form;
        String action = excelUploadForm.getTask();
        String schemeId = excelUploadForm.getSchemeId();
        this.loadComboScheme(request);

        if (Global.WEB_TASK_SHOW.equals(action) || Global.WEB_TASK_LOAD.equals(action)) {
        } else if ("SaveExcel".equals(action)) {
            if (this.validator.validateSchemeId(new ActionMessages(), excelUploadForm)) {
                String clientAddress = request.getRemoteAddr();
                String branchId = this.getLoginBean(request).getBranchId();
                String userCreate = this.getLoginBean(request).getUserid();
                manager.processSpreadSheetFile(excelUploadForm.getExcelFile(), clientAddress, branchId, userCreate, schemeId);
                this.setMessage(request, "common.success", null);
            }
        } else if ("Download".equals(action)) {
            if (this.validator.validateSchemeId(new ActionMessages(), excelUploadForm)) {
                String branchId = this.getLoginBean(request).getBranchId();
                HSSFWorkbook workbook = this.manager.createXlsTemplate(branchId, schemeId);
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                Date now = new Date();
                String strDate = df.format(now);

                df.format(now);
                String fileName = resources.getMessage("upload.collection.file.name", new Object[]{strDate, schemeId});
                response.setContentType("application/vnd.ms-excel");
                response.setHeader("content-disposition", "attachment; filename=" + fileName);
                OutputStream out = null;
                try {
                    try {
                        out = response.getOutputStream();
                        workbook.write(out);
                    } finally {
                        if (out != null) {
                            out.flush();
                            out.close();
                        }
                    }
                } catch (IOException e) {
                    this.logger.printStackTrace(e);
                    throw new CoreException(e, ExcelUploadCollectionManager.ERROR_UNKNOWN);
                }
                return null;
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadComboScheme(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboSchemeCollection();
        request.setAttribute("comboScheme", list);
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        this.manager = new ExcelUploadCollectionManager(request);

        switch (code) {
            case ExcelUploadCollectionManager.ERROR_INVALID_SPREADSHEET:
                this.setMessage(request, "upload.errorexcel", null);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadCollectionManager.ERROR_NULL_DATA_EXCEL:
                this.setMessage(request, "upload.errorexcel.nodata", null);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadCollectionManager.ERROR_LAT_LONG_DATA:
                this.setMessage(request, "upload.errorexcel.latlong.data", null);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadCollectionManager.ERROR_MAX_DATA_LENGTH:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadCollectionManager.ERROR_TYPE_DATA:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadCollectionManager.ERROR_REQUIRED_DATA:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadCollectionManager.ERROR_MAX_TYPE_DATA_LAT_LONG:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadCollectionManager.ERROR_ROW_COUNT_EXCEL:
                this.setMessage(request, "upload.errorexcel.row", null);
                this.logger.printStackTrace(ex);
                break;

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            case ExcelUploadCollectionManager.ERROR_HEADER:
                this.setMessage(request, "upload.errorexcel.header.match", null);
                this.logger.printStackTrace(ex);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((ExcelUploadCollectionForm) form).setTask(((ExcelUploadCollectionForm) form).resolvePreviousTask());
        String task = ((ExcelUploadCollectionForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
