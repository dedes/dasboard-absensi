package treemas.application.feature.task.uploadcollection;

import com.google.common.base.Strings;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.upload.FormFile;
import treemas.application.feature.task.AssignmentBean;
import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;
import treemas.util.struts.ApplicationResources;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;


public class ExcelUploadCollectionManager extends FeatureManagerBase {
    static final int ERROR_INVALID_SPREADSHEET = 1;
    static final int ERROR_NULL_DATA_EXCEL = 2;
    static final int ERROR_LAT_LONG_DATA = 3;
    static final int ERROR_MAX_DATA_LENGTH = 4;
    static final int ERROR_TYPE_DATA = 5;
    static final int ERROR_REQUIRED_DATA = 6;
    static final int ERROR_MAX_TYPE_DATA_LAT_LONG = 7;
    static final int ERROR_ROW_COUNT_EXCEL = 8;
    static final int ERROR_HEADER = 9;
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_ASSIGNMENT_UPLOAD;
    //	private static final int NUMBER_OF_CELL_IN_EXCEL = 11;
    private static final int NUMBER_OF_CELL_IN_EXCEL = 24;
    private static final String ERROR_SPREADSHEET = "Invalid spreadsheet";
    private static final String ERROR_NULL_DATA = "Null Data";
    private static final String ERROR_DATA_LAT_LONG = "Error Data Latitude or Longitude";
    private static final String ERROR_MAX_LENGTH = "Error max data length";
    private static final String ERROR_DATA = "Error type data";
    private static final String ERROR_DATA_REQUIRED = "Error data required";
    private static final String ERROR_MAX_TYPE_LAT_LONG = "Error Type Data or Maximum/Minimum value of Latitude or Longitude";
    private static final String ERROR_ROW_COUNT = "Row number at Add Collection didn't match with row number at Collection Data";
    private static final String ERROR_HEADER_MATCH = "Error header match";
    private static final String COLLECTION_ROLE = "CF";
    private static final int SHEET_1 = 1;
    private static final int SHEET_2 = 2;
    private static final String COLUMN_A = "A";
    private static final String COLUMN_B = "B";
    private static final String COLUMN_C = "C";
    private static final String COLUMN_D = "D";
    private static final String COLUMN_E = "E";
    private static final String COLUMN_F = "F";
    private static final String COLUMN_G = "G";
    private static final String COLUMN_H = "H";
    private static final String COLUMN_I = "I";
    private static final String COLUMN_J = "J";
    private static final String COLUMN_K = "K";
    private static final String COLUMN_L = "L";
    private static final String COLUMN_M = "M";
    private static final String COLUMN_N = "N";
    private static final String COLUMN_O = "O";
    private static final String COLUMN_P = "P";
    private static final String COLUMN_Q = "Q";
    private static final String COLUMN_R = "R";
    private static final String COLUMN_S = "S";
    private static final String COLUMN_T = "T";
    private static final String COLUMN_U = "U";
    private static final String COLUMN_V = "V";
    private static final String COLUMN_W = "W";
    private static final String COLUMN_X = "X";
    private static final int NUMBER_ROW_IN_VALIDATION = 1000;
    //	private static final int PIC_COLUMN = 8;
//	private static final int SCHEME_COLUMN = 9;
    private static final int STREET1_COLUMN = 5;
    private static final int STREET2_COLUMN = 6;
    private static final int NUMBER_COLUMN = 7;
    private static final int RT_COLUMN = 8;
    private static final int RW_COLUMN = 9;
    private static final int KEL_COLUMN = 10;
    private static final int KEC_COLUMN = 11;
    private static final int KAB_COLUMN = 12;
    private static final int KOTA_COLUMN = 13;
    private static final int PROP_COLUMN = 14;
    private static final int POS_COLUMN = 15;
    private static final int NGR_COLUMN = 16;
    private static final int LAT_COLUMN = 17;
    private static final int LONG_COLUMN = 18;
    private static final int PIC_COLUMN = 19;
    private static final int SCHEME_COLUMN = 20;
    private static final int PRIORITY_COLUMN = 23;
    private static final int[] HEADER_COLUMN_WIDTH = {20 * 256, 50 * 256, 20 * 256, 20 * 256, 20 * 256, 30 * 256, 30 * 256, 30 * 256, 5 * 256, 5 * 256, 20 * 256, 30 * 256,
            30 * 256, 30 * 256, 30 * 256, 10 * 256, 30 * 256, 20 * 256, 20 * 256, 20 * 256, 30 * 256, 20 * 256, 50 * 256, 20 * 256, 20 * 256};
    private static String NIK;
    private static String CUSTOMER_NAME;
    private static String TELEPHONE;
    private static String OFFICE_PHONE;
    private static String CELLULAR_PHONE;
    private static String STREET1;
    private static String STREET2;
    private static String NUMBER;
    private static String RT;
    private static String RW;
    private static String SUB_DISTRICTS;
    private static String DISTRICTS;
    private static String REGION;
    private static String CITY;
    private static String PROVINCE;
    private static String POST_CODE;
    private static String COUNTRY;
    private static String GPS_LATITUDE;
    private static String GPS_LONGITUDE;
    private static String PIC;
    private static String SCHEME;
    private static String NOTES;
    private static String APPL_NO;
    private static String PRIORITY;
    private static String[] TEMPLATE_HEADER;
    private static ApplicationResources resources;
    private SequenceManager sequenceManager = new SequenceManager();
    private ExcelUploadCollectionValidator validator;

    public ExcelUploadCollectionManager(HttpServletRequest request) {
        this.validator = new ExcelUploadCollectionValidator(request);
        String lang = (String) request.getSession().getAttribute(Global.LANG);
        if (Strings.isNullOrEmpty(lang)) {
            this.resources = ApplicationResources.getInstanceInd();
        } else {
            if (Global.LANG_EN.equalsIgnoreCase(lang)) {
                this.resources = ApplicationResources.getInstanceEng();
            } else {
                this.resources = ApplicationResources.getInstance();
            }
        }

        ExcelUploadCollectionManager.NIK = resources.getMessage("app.assignment.nik");
        ExcelUploadCollectionManager.CUSTOMER_NAME = resources.getMessage("feature.approval.customer.name");
        ExcelUploadCollectionManager.TELEPHONE = resources.getMessage("app.assignment.customerphone");
        ExcelUploadCollectionManager.OFFICE_PHONE = resources.getMessage("app.assignment.officephone");
        ExcelUploadCollectionManager.CELLULAR_PHONE = resources.getMessage("app.assignment.cellularphone");
        ExcelUploadCollectionManager.STREET1 = resources.getMessage("app.assignment.address.street1");
        ExcelUploadCollectionManager.STREET2 = resources.getMessage("app.assignment.address.street2");
        ExcelUploadCollectionManager.NUMBER = resources.getMessage("app.assignment.addressnumber");
        ExcelUploadCollectionManager.RT = resources.getMessage("app.assignment.rt");
        ExcelUploadCollectionManager.RW = resources.getMessage("app.assignment.rw");
        ExcelUploadCollectionManager.SUB_DISTRICTS = resources.getMessage("app.assignment.kelurahan");
        ExcelUploadCollectionManager.DISTRICTS = resources.getMessage("app.assignment.kecamatan");
        ExcelUploadCollectionManager.REGION = resources.getMessage("app.assignment.kabupaten");
        ExcelUploadCollectionManager.CITY = resources.getMessage("app.assignment.city");
        ExcelUploadCollectionManager.PROVINCE = resources.getMessage("app.assignment.province");
        ExcelUploadCollectionManager.POST_CODE = resources.getMessage("app.assignment.postcode");
        ExcelUploadCollectionManager.COUNTRY = resources.getMessage("app.assignment.country");
        ExcelUploadCollectionManager.GPS_LATITUDE = resources.getMessage("app.assignment.latitude");
        ExcelUploadCollectionManager.GPS_LONGITUDE = resources.getMessage("app.assignment.longitude");
        ExcelUploadCollectionManager.PIC = resources.getMessage("app.assignment.pic");
        ExcelUploadCollectionManager.SCHEME = resources.getMessage("app.assignment.scheme");
        ExcelUploadCollectionManager.NOTES = resources.getMessage("app.assignment.notes");
        ExcelUploadCollectionManager.APPL_NO = resources.getMessage("feature.assignment.application.no");
        ExcelUploadCollectionManager.PRIORITY = resources.getMessage("app.assignment.priority");
        String[] arrytemp = {NIK, CUSTOMER_NAME, TELEPHONE, OFFICE_PHONE, CELLULAR_PHONE, STREET1, STREET2, NUMBER, RT, RW,
                SUB_DISTRICTS, DISTRICTS, REGION, CITY, PROVINCE, POST_CODE, COUNTRY, GPS_LATITUDE,
                GPS_LONGITUDE, PIC, SCHEME, NOTES, APPL_NO, PRIORITY};
        ExcelUploadCollectionManager.TEMPLATE_HEADER = arrytemp;

    }

    public void processSpreadSheetFile(FormFile uploadedFile, String clientAddress, String branchId, String userCreate, String schemeId) throws CoreException {
        ActionMessages messages = new ActionMessages();

        this.logger.log(ILogger.LEVEL_INFO, "Processing uploaded spreadsheet file: " +
                uploadedFile.getFileName() + " from " + clientAddress);
        try {
            List listofAssignment = this.parseSpreadsheetToAssignmentBeans(uploadedFile);
            List collectionData = this.getQuestionLabel(schemeId);
            List listofDataCollection = this.parseSpreadsheetToAssignmentDataCollection(uploadedFile, collectionData);

            try {
                this.ibatisSqlMap.startTransaction();
                int row = 1;
                for (Iterator iterator = listofAssignment.iterator(); iterator
                        .hasNext(); ) {
                    AssignmentBean bean = (AssignmentBean) iterator.next();

                    if (!this.validator.getRequiredData(messages, bean.getNik().length(), row + 1, COLUMN_A, NIK)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getCustomerName().length(), row + 1, COLUMN_B, CUSTOMER_NAME)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getAddressStreet1().length(), row + 1, COLUMN_F, STREET1)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getNoaddr().length(), row + 1, COLUMN_H, NUMBER)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getNoaddr().length(), row + 1, COLUMN_H, NUMBER)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getKota().length(), row + 1, COLUMN_N, CITY)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getPropinsi().length(), row + 1, COLUMN_O, PROVINCE)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getKodepos().length(), row + 1, COLUMN_P, POST_CODE)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getNegara().length(), row + 1, COLUMN_Q, COUNTRY)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getSurveyorId().length(), row + 1, COLUMN_T, PIC)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getSchemeId().length(), row + 1, COLUMN_U, SCHEME)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getApplNo().length(), row + 1, COLUMN_W, APPL_NO)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    } else if (!this.validator.getRequiredData(messages, bean.getPriorityId().length(), row + 1, COLUMN_X, PRIORITY)) {
                        throw new IOException(ERROR_DATA_REQUIRED);
                    }

                    if (!this.validator.getMaxLength(messages, bean.getNik().length(), row + 1, COLUMN_A, 30, NIK)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getCustomerName().length(), row + 1, COLUMN_B, 30, CUSTOMER_NAME)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getCustomerPhone().length(), row + 1, COLUMN_C, 10, TELEPHONE)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getOfficePhone().length(), row + 1, COLUMN_D, 10, OFFICE_PHONE)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getCustomerHandPhone().length(), row + 1, COLUMN_E, 12, CELLULAR_PHONE)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getAddressStreet1().length(), row + 1, COLUMN_F, 80, STREET1)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getAddressStreet2().length(), row + 1, COLUMN_G, 80, STREET2)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getNoaddr().length(), row + 1, COLUMN_H, 10, NUMBER)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getRt().length(), row + 1, COLUMN_I, 6, RT)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getRw().length(), row + 1, COLUMN_J, 6, RW)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getKelurahan().length(), row + 1, COLUMN_K, 150, SUB_DISTRICTS)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getKecamatan().length(), row + 1, COLUMN_L, 150, DISTRICTS)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getKabupaten().length(), row + 1, COLUMN_M, 150, REGION)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getKota().length(), row + 1, COLUMN_N, 150, CITY)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getPropinsi().length(), row + 1, COLUMN_O, 150, PROVINCE)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getKodepos().length(), row + 1, COLUMN_P, 6, POST_CODE)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getNegara().length(), row + 1, COLUMN_Q, 150, COUNTRY)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getGpsLatitude().toString().length(), row + 1, COLUMN_R, 32, GPS_LATITUDE)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getGpsLongitude().toString().length(), row + 1, COLUMN_S, 32, GPS_LONGITUDE)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getSurveyorId().length(), row + 1, COLUMN_T, 15, PIC)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getSchemeId().length(), row + 1, COLUMN_U, 5, SCHEME)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getNotes().length(), row + 1, COLUMN_V, 500, NOTES)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getApplNo().length(), row + 1, COLUMN_W, 20, APPL_NO)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    } else if (!this.validator.getMaxLength(messages, bean.getPriorityId().length(), row + 1, COLUMN_X, 20, PRIORITY)) {
                        throw new IOException(ERROR_MAX_LENGTH);
                    }

                    if (!this.validator.getNumericType(messages, bean.getCustomerPhone().length(), bean.getCustomerPhone(), row + 1, COLUMN_C, TELEPHONE)) {
                        throw new IOException(ERROR_DATA);
                    } else if (!this.validator.getNumericType(messages, bean.getOfficePhone().length(), bean.getOfficePhone(), row + 1, COLUMN_D, OFFICE_PHONE)) {
                        throw new IOException(ERROR_DATA);
                    } else if (!this.validator.getNumericType(messages, bean.getCustomerHandPhone().length(), bean.getCustomerHandPhone(), row + 1, COLUMN_E, CELLULAR_PHONE)) {
                        throw new IOException(ERROR_DATA);
                    } else if (!this.validator.getNumericType(messages, bean.getRt().length(), bean.getRt(), row + 1, COLUMN_I, RT)) {
                        throw new IOException(ERROR_DATA);
                    } else if (!this.validator.getNumericType(messages, bean.getRw().length(), bean.getRw(), row + 1, COLUMN_J, RW)) {
                        throw new IOException(ERROR_DATA);
                    } else if (!this.validator.getNumericType(messages, bean.getKodepos().length(), bean.getKodepos(), row + 1, COLUMN_P, POST_CODE)) {
                        throw new IOException(ERROR_DATA);
                    }

                    if (!this.validator.getLatitudeType(messages, bean.getGpsLatitude().toString().length(), bean.getGpsLatitude(), row + 1, COLUMN_R, GPS_LATITUDE)) {
                        throw new IOException(ERROR_MAX_TYPE_LAT_LONG);
                    } else if (!this.validator.getLongitudeType(messages, bean.getGpsLongitude().toString().length(), bean.getGpsLongitude(), row + 1, COLUMN_S, GPS_LONGITUDE)) {
                        throw new IOException(ERROR_MAX_TYPE_LAT_LONG);
                    }
                    row++;
                }

                row = 1;
                for (Iterator iterator = listofDataCollection.iterator(); iterator
                        .hasNext(); ) {

                    String[] dataCollection = listofDataCollection.get(row - 1).toString().split(",,");

//					cek data label per kolom
                    for (int i = 0; i < dataCollection.length; i++) {

                        String[] labelCollection = dataCollection[i].split("\\|");
                        String[] answerLengthNLabel = labelCollection[0].split("=");
                        int answerLength = 0;
                        String answerData = "";
                        if (2 > answerLengthNLabel.length) {
                            answerLength = 0;
                        } else {
                            answerLength = answerLengthNLabel[1].length();
                            answerData = answerLengthNLabel[1];
                        }

                        String answerType = labelCollection[1];
                        int intMaxLength = Integer.parseInt(labelCollection[2]);
                        String questionLabel = labelCollection[3];
                        int columnNumber = Integer.parseInt(labelCollection[4]);
                        String columnLetter = CellReference.convertNumToColString(columnNumber);

                        if (!this.validator.getRequiredData(messages, answerLength, row + 1, columnLetter, questionLabel)) {
                            throw new IOException(ERROR_DATA_REQUIRED);
                        }

                        if (!this.validator.getMaxLength(messages, answerLength, row + 1, columnLetter, intMaxLength, questionLabel)) {
                            throw new IOException(ERROR_MAX_LENGTH);
                        }

                        if ("008".equals(answerType) || "027".equals(answerType)
                                || "028".equals(answerType)) {
                            if (!this.validator.getNumericTypeCollection(messages, answerData, row + 1, columnLetter, questionLabel)) {
                                throw new IOException(ERROR_DATA);
                            }
                        } else if ("009".equals(answerType)) {
                            if (!this.validator.getDecimalTypeCollection(messages, answerData, row + 1, columnLetter, questionLabel)) {
                                throw new IOException(ERROR_DATA);
                            }
                        } else if ("010".equals(answerType)) {
                            if (!this.validator.getDateTypeCollection(messages, answerData, row + 1, columnLetter, questionLabel)) {
                                throw new IOException(ERROR_DATA);
                            }
                        } else if ("011".equals(answerType)) {
                            if (!this.validator.getTimeTypeCollection(messages, answerData, row + 1, columnLetter, questionLabel)) {
                                throw new IOException(ERROR_DATA);
                            }
                        } else if ("012".equals(answerType)) {
                            if (!this.validator.getDateTimeTypeCollection(messages, answerData, row + 1, columnLetter, questionLabel)) {
                                throw new IOException(ERROR_DATA);
                            }
                        }
                    }
                    row++;
                    iterator.next();
                }

                row = 1;
                for (Iterator iterator = listofAssignment.iterator(); iterator
                        .hasNext(); ) {
                    Integer mobileAssignmentId = new Integer(sequenceManager.getSequenceMobileAssignmentId());
                    AssignmentBean bean = (AssignmentBean) iterator.next();
                    bean.setMobileAssignmentId(mobileAssignmentId);
                    bean.setBranchId(branchId);
                    bean.setUsrcrt(userCreate);

                    this.saveAssignment(bean);
//				for (Iterator iterator2 = listofDataCollection.iterator(); iterator2
//						.hasNext();) {
                    Map collection = new HashMap();
                    String[] dataCollection = listofDataCollection.get(row - 1).toString().split(",,");

//					cek data label per kolom
                    for (int i = 0; i < dataCollection.length; i++) {

                        String[] labelCollection = dataCollection[i].split("\\|");
                        String[] answerLengthNLabel = labelCollection[0].split("=");

                        String answerData = "";
                        if (2 == answerLengthNLabel.length) {
                            answerData = answerLengthNLabel[1];
                        }

                        String answerType = labelCollection[1];
                        int questionGroupId = Integer.parseInt(labelCollection[5]);
                        int questionId = 0;
                        if (dataCollection.length == (i + 1)) {
                            String labelColl[] = labelCollection[6].split(",");
                            questionId = Integer.parseInt(labelColl[0]);
                        } else {
                            questionId = Integer.parseInt(labelCollection[6]);
                        }

                        collection.put("mobileAssignmentId", mobileAssignmentId);
                        collection.put("answerType", answerType);
                        collection.put("answerData", answerData);
                        collection.put("questionGroupId", questionGroupId);
                        collection.put("questionId", questionId);

                        this.saveDataCollection(collection);
                    }
//					iterator2.next();
//				}
                    row = row + 1;
                }
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (IOException ioe) {
            this.logger.printStackTrace(ioe);
            String message = ioe.getMessage();
            if (ERROR_SPREADSHEET.equals(message))
                throw new CoreException(ERROR_INVALID_SPREADSHEET, uploadedFile);
            else if (ERROR_NULL_DATA.equals(message))
                throw new CoreException(ERROR_NULL_DATA_EXCEL, uploadedFile);
            else if (ERROR_DATA_LAT_LONG.equals(message))
                throw new CoreException(ERROR_LAT_LONG_DATA, uploadedFile);
            else if (ERROR_MAX_LENGTH.equals(message))
                throw new CoreException(ERROR_MAX_DATA_LENGTH, messages);
            else if (ERROR_DATA.equals(message))
                throw new CoreException(ERROR_TYPE_DATA, messages);
            else if (ERROR_DATA_REQUIRED.equals(message))
                throw new CoreException(ERROR_REQUIRED_DATA, messages);
            else if (ERROR_MAX_TYPE_LAT_LONG.equals(message))
                throw new CoreException(ERROR_MAX_TYPE_DATA_LAT_LONG, messages);
            else if (ERROR_ROW_COUNT.equals(message))
                throw new CoreException(ERROR_ROW_COUNT_EXCEL, uploadedFile);
            else if (ERROR_HEADER_MATCH.equals(message))
                throw new CoreException(ERROR_HEADER, messages);
            else
                throw new CoreException(ioe, ERROR_UNKNOWN);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
    }

    private List parseSpreadsheetToAssignmentBeans(FormFile uploadedFile) throws IOException, CoreException {
        List result = new ArrayList();
        HSSFWorkbook wb = new HSSFWorkbook(uploadedFile.getInputStream());
        AssignmentBean bean = new AssignmentBean();

        if (wb.getNumberOfSheets() < 1)
            this.logger.log(ILogger.LEVEL_INFO, "No sheet found");

        for (int k = 0; k < 1; k++) {
            HSSFSheet sheet = wb.getSheetAt(k);
            int rows = sheet.getPhysicalNumberOfRows();

            if (rows < 2) {
                throw new IOException(ERROR_NULL_DATA);
            }

            //count row at sheet 2
            HSSFSheet sheet2 = wb.getSheetAt(k + 1);
            int rows2 = sheet2.getPhysicalNumberOfRows();

            if (rows != rows2) {
                throw new IOException(ERROR_ROW_COUNT);
            }


            this.logger.log(ILogger.LEVEL_INFO, "Sheet " + k + " \"" + wb.getSheetName(k) + "\" has " + rows
                    + " row(s).");

            HSSFRow row = sheet.getRow(0);
            if (row == null) {
                continue;
            }

            int cells = row.getPhysicalNumberOfCells();

            if (cells != NUMBER_OF_CELL_IN_EXCEL)
                throw new IOException(ERROR_SPREADSHEET);

            for (int r = 1; r < rows; r++) {
                row = sheet.getRow(r);
                bean = new AssignmentBean();

                for (int c = 0; c < cells; c++) {
                    HSSFCell cell = row.getCell(c);

                    if (null == cell || ("").equals(cell.toString())) {
                        cell = row.createCell(c);
                        cell.setCellType(HSSFCell.CELL_TYPE_BLANK);
                    }

                    String value = null;


                    switch (cell.getCellType()) {
                        case HSSFCell.CELL_TYPE_NUMERIC:
                            value = "" + cell.getNumericCellValue();
                            break;

                        case HSSFCell.CELL_TYPE_STRING:
                            value = "" + cell.getStringCellValue();
                            break;

                        case HSSFCell.CELL_TYPE_BLANK:
                            value = "";
                            break;

                        default:
                    }

                    switch (c) {
                        case PIC_COLUMN:
                        case SCHEME_COLUMN:
                        case PRIORITY_COLUMN:
                            String[] temp = value.split("-");
                            value = temp[0].trim();
                            break;
                    }

                    switch (c) {
                        case STREET1_COLUMN:
                            bean.setCustomerAddress(value);
                            break;
                    }
                    switch (c) {
                        case STREET2_COLUMN:
                        case NUMBER_COLUMN:
                            bean.setCustomerAddress(bean.getCustomerAddress() + " " + value);
                            break;
                    }
                    switch (c) {
                        case RT_COLUMN:
                            bean.setCustomerAddress(bean.getCustomerAddress() + ", RT/RW:" + value);
                            break;
                    }
                    switch (c) {
                        case RW_COLUMN:
                            bean.setCustomerAddress(bean.getCustomerAddress() + "/" + value);
                            break;
                    }
                    switch (c) {
                        case KEL_COLUMN:
                            bean.setCustomerAddress(bean.getCustomerAddress() + ", Kel:" + value);
                            break;
                    }
                    switch (c) {
                        case KEC_COLUMN:
                            bean.setCustomerAddress(bean.getCustomerAddress() + ", Kec:" + value);
                            break;
                    }
                    switch (c) {
                        case KAB_COLUMN:
                            bean.setCustomerAddress(bean.getCustomerAddress() + ", Kab:" + value);
                            break;
                    }
                    switch (c) {
                        case KOTA_COLUMN:
                            bean.setCustomerAddress(bean.getCustomerAddress() + ", Kota:" + value);
                            break;
                    }
                    switch (c) {
                        case PROP_COLUMN:
                            bean.setCustomerAddress(bean.getCustomerAddress() + ", " + value);
                            break;
                    }
                    switch (c) {
                        case NGR_COLUMN:
                            bean.setCustomerAddress(bean.getCustomerAddress() + ", " + value);
                            break;
                    }
                    switch (c) {
                        case POS_COLUMN:
                            bean.setCustomerAddress(bean.getCustomerAddress() + ", " + value);
                            break;
                    }
                    switch (c) {
                        case LAT_COLUMN:
                            try {
                                bean.setGpsLatitude(Double.parseDouble(value));
                                break;
                            } catch (Exception e) {
                                throw new IOException(ERROR_DATA_LAT_LONG);
                            }
                    }
                    switch (c) {
                        case LONG_COLUMN:
                            try {
                                bean.setGpsLongitude(Double.parseDouble(value));
                                break;
                            } catch (Exception e) {
                                throw new IOException(ERROR_DATA_LAT_LONG);
                            }
                    }
                    switch (c) {
                        case 0:
                            bean.setNik(value);
                            break;
                        case 1:
                            bean.setCustomerName(value);
                            break;
                        case 2:
                            bean.setCustomerPhone(value);
                            break;
                        case 3:
                            bean.setOfficePhone(value);
                            break;
                        case 4:
                            bean.setCustomerHandPhone(value);
                            break;
                        case 5:
                            bean.setAddressStreet1(value);
                            break;
                        case 6:
                            bean.setAddressStreet2(value);
                            break;
                        case 7:
                            bean.setNoaddr(value);
                            break;
                        case 8:
                            bean.setRt(value);
                            break;
                        case 9:
                            bean.setRw(value);
                            break;
                        case 10:
                            bean.setKelurahan(value);
                            break;
                        case 11:
                            bean.setKecamatan(value);
                            break;
                        case 12:
                            bean.setKabupaten(value);
                            break;
                        case 13:
                            bean.setKota(value);
                            break;
                        case 14:
                            bean.setPropinsi(value);
                            break;
                        case 15:
                            bean.setKodepos(value);
                            break;
                        case 16:
                            bean.setNegara(value);
                            break;
                        case 17:
                            bean.setGpsLatitude(Double.parseDouble(value));
                            break;
                        case 18:
                            bean.setGpsLongitude(Double.parseDouble(value));
                            break;
                        case 19:
                            bean.setSurveyorId(value);
                            break;
                        case 20:
                            bean.setSchemeId(value);
                            break;
                        case 21:
                            bean.setNotes(value);
                            break;
                        case 22:
                            bean.setApplNo(value);
                            break;
                        case 23:
                            bean.setPriorityId(value);
                            break;
                    }
                }
                result.add(bean);
            }
        }

        return result;
    }

    private List parseSpreadsheetToAssignmentDataCollection(FormFile uploadedFile, List collectionData) throws IOException, CoreException {
        List result = new ArrayList();
        HSSFWorkbook wb = new HSSFWorkbook(uploadedFile.getInputStream());
        AssignmentBean bean = new AssignmentBean();
        Map hashCollection = new HashMap();

        if (wb.getNumberOfSheets() < 1)
            this.logger.log(ILogger.LEVEL_INFO, "No sheet found");

        for (int k = 1; k < 2; k++) {
            HSSFSheet sheet = wb.getSheetAt(k);
            int rows = sheet.getPhysicalNumberOfRows();

            if (rows < 2) {
                throw new IOException(ERROR_NULL_DATA);
            }

            this.logger.log(ILogger.LEVEL_INFO, "Sheet " + k + " \"" + wb.getSheetName(k) + "\" has " + rows
                    + " row(s).");

            HSSFRow row = sheet.getRow(0);
            if (row == null) {
                continue;
            }

            int cells = row.getPhysicalNumberOfCells();

            int numberOfCell = collectionData.size();
            if (cells != numberOfCell) {
                throw new IOException(ERROR_SPREADSHEET);
            } else {
                if (!this.validator.matchingHeader(new ActionMessages(), collectionData, row)) {
                    throw new IOException(ERROR_HEADER_MATCH);
                }
            }


            for (int r = 1; r < rows; r++) {
                row = sheet.getRow(r);
                hashCollection = new HashMap();

                for (int c = 0; c < cells; c++) {
                    HSSFCell cell = row.getCell(c);

                    if (null == cell || ("").equals(cell.toString())) {
                        cell = row.createCell(c);
                        cell.setCellType(HSSFCell.CELL_TYPE_BLANK);
                    }

                    String value = null;


                    switch (cell.getCellType()) {
                        case HSSFCell.CELL_TYPE_NUMERIC:
                            value = "" + cell.getNumericCellValue();
                            break;

                        case HSSFCell.CELL_TYPE_STRING:
                            value = "" + cell.getStringCellValue();
                            break;

                        case HSSFCell.CELL_TYPE_BLANK:
                            value = "";
                            break;

                        default:
                    }
                    String[] resultCollection = collectionData.get(c).toString().split("\\|");
                    String label = resultCollection[0];
                    hashCollection.put("'" + label + "'", value + "|" + resultCollection[1] + "|" + resultCollection[2] + "|" + label + "|" + c
                            + "|" + resultCollection[3] + "|" + resultCollection[4] + ",");
                }
                result.add(hashCollection);
            }
        }

        return result;
    }

    private void saveAssignment(AssignmentBean bean) throws SQLException {
        this.ibatisSqlMap.insert(this.STRING_SQL + "insert", bean);
    }

    private void saveDataCollection(Map collection) throws SQLException {
        this.ibatisSqlMap.insert(this.STRING_SQL + "insertCollection", collection);
    }

    public HSSFWorkbook createXlsTemplate(String branchId, String schemeId) throws CoreException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        try {
            HSSFSheet sheet = workbook.createSheet("Add Collection");
            this.createHeader(workbook, sheet, SHEET_1, schemeId);
            this.setTextDataFormat(workbook, sheet, SHEET_1);

            HSSFSheet sheetColl = workbook.createSheet("Collection Data");
            this.createHeader(workbook, sheetColl, SHEET_2, schemeId);
            this.setTextDataFormat(workbook, sheetColl, SHEET_2);

            HSSFDataValidation collectionValidation = this.createCollectorValidation(workbook, branchId);
            HSSFDataValidation formValidation = this.createFormValidation(workbook, schemeId);
            HSSFDataValidation priorityValidation = this.createPriorityValidation(workbook);

            sheet.addValidationData(collectionValidation);
            sheet.addValidationData(formValidation);
            sheet.addValidationData(priorityValidation);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return workbook;
    }

    private void createHeader(HSSFWorkbook workbook, HSSFSheet sheet, int type, String schemeId) {
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBoldweight((short) 1000);
        style.setFont(font);

        HSSFRow row = sheet.createRow(0);

        if (SHEET_1 == type) {
            for (int i = 0; i < TEMPLATE_HEADER.length; i++) {
                HSSFCell cell = row.createCell(i);
                cell.setCellValue(TEMPLATE_HEADER[i]);
                cell.setCellStyle(style);
                sheet.setColumnWidth(i, HEADER_COLUMN_WIDTH[i]);
            }
        } else if (SHEET_2 == type) {
            List listCollection = null;
            try {
                listCollection = this.getQuestionLabel(schemeId);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            int size = listCollection.size();

            for (int i = 0; i < size; i++) {
                HSSFCell cell = row.createCell(i);

                String[] result = listCollection.get(i).toString().split("\\|");
                String label = result[0];
                cell.setCellValue(label);
                cell.setCellStyle(style);
                sheet.setColumnWidth(i, 20 * 256);
            }
        }

    }

    private void setTextDataFormat(HSSFWorkbook workbook, HSSFSheet sheet, int type) {
        DataFormat format = workbook.createDataFormat();
        HSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(format.getFormat("@"));

        if (SHEET_1 == type) {
            for (int i = 0; i < TEMPLATE_HEADER.length; i++) {
                sheet.setDefaultColumnStyle(i, style);
            }
        } else if (SHEET_2 == type) {
            Integer collection = 0;
            try {
                collection = this.countQuestionLabel();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < collection; i++) {
                sheet.setDefaultColumnStyle(i, style);
            }
        }
    }

    private HSSFDataValidation createPriorityValidation(HSSFWorkbook workbook) throws SQLException {
        CellRangeAddressList addressList = new CellRangeAddressList(1, NUMBER_ROW_IN_VALIDATION, PRIORITY_COLUMN, PRIORITY_COLUMN);

        HSSFSheet sheet = workbook.createSheet("Priority");

        List listForm = this.getPriority();
        int size = listForm.size();

        for (int i = 0; i < size; i++) {
            HSSFRow row = sheet.createRow(i);
            HSSFCell cell = row.createCell(0);

            String form = (String) listForm.get(i);
            cell.setCellValue(form);
            CellStyle style = workbook.createCellStyle();
            style.setLocked(true);
            cell.setCellStyle(style);
        }

        sheet.protectSheet("");

        DVConstraint dvConstraint = DVConstraint
                .createFormulaListConstraint("'Priority'!$A$1:$A$" + size);
        HSSFDataValidation dataValidation = new HSSFDataValidation(addressList,
                dvConstraint);
        dataValidation.setSuppressDropDownArrow(false);
        dataValidation.setEmptyCellAllowed(false);
        return dataValidation;
    }

//	private HSSFDataValidation createPriorityValidation(HSSFWorkbook workbook) throws SQLException {
//		CellRangeAddressList addressList = new CellRangeAddressList(1, NUMBER_ROW_IN_VALIDATION, PRIORITY_COLUMN, PRIORITY_COLUMN);
//		DVConstraint dvConstraint = DVConstraint
//				.createExplicitListConstraint(this.getPriority());
//		HSSFDataValidation dataValidation = new HSSFDataValidation(addressList,
//				dvConstraint);
//		dataValidation.setSuppressDropDownArrow(false);
//		return dataValidation;
//	}

    private HSSFDataValidation createFormValidation(HSSFWorkbook workbook, String schemeId) throws SQLException {
        CellRangeAddressList addressList = new CellRangeAddressList(1, NUMBER_ROW_IN_VALIDATION, SCHEME_COLUMN, SCHEME_COLUMN);

        HSSFSheet sheet = workbook.createSheet("DataForm");

        List listForm = this.getForm(schemeId);
        int size = listForm.size();

        for (int i = 0; i < size; i++) {
            HSSFRow row = sheet.createRow(i);
            HSSFCell cell = row.createCell(0);

            String form = (String) listForm.get(i);
            cell.setCellValue(form);
            CellStyle style = workbook.createCellStyle();
            style.setLocked(true);
            cell.setCellStyle(style);
        }

        sheet.protectSheet("");

        DVConstraint dvConstraint = DVConstraint
                .createFormulaListConstraint("'DataForm'!$A$1:$A$" + size);
        HSSFDataValidation dataValidation = new HSSFDataValidation(addressList,
                dvConstraint);
        dataValidation.setSuppressDropDownArrow(false);
        dataValidation.setEmptyCellAllowed(false);
        return dataValidation;
    }

    private HSSFDataValidation createCollectorValidation(HSSFWorkbook workbook, String branchId) throws SQLException {
        CellRangeAddressList addressList = new CellRangeAddressList(1, NUMBER_ROW_IN_VALIDATION, PIC_COLUMN, PIC_COLUMN);

        HSSFSheet sheet = workbook.createSheet("DataCollector");

        List listcollection = this.getCollectorByCabang(branchId);
        int size = listcollection.size();

        for (int i = 0; i < size; i++) {
            HSSFRow row = sheet.createRow(i);
            HSSFCell cell = row.createCell(0);

            String collection = (String) listcollection.get(i);
            cell.setCellValue(collection);
            CellStyle style = workbook.createCellStyle();
            style.setLocked(true);
            cell.setCellStyle(style);
        }

        sheet.protectSheet("");

        DVConstraint dvConstraint = DVConstraint
                .createFormulaListConstraint("'DataCollector'!$A$1:$A$" + size);
        HSSFDataValidation dataValidation = new HSSFDataValidation(addressList,
                dvConstraint);
        dataValidation.setSuppressDropDownArrow(false);
        dataValidation.setEmptyCellAllowed(false);
        return dataValidation;
    }

    private List getCollectorByCabang(String branchId) throws SQLException {
        List collectionList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getCollector", branchId);
        return collectionList;
    }

    private List getForm(String schemeId) throws SQLException {
        Map schemeRole = new HashMap();
        schemeRole.put("schemeRole", COLLECTION_ROLE);
        schemeRole.put("schemeId", schemeId);

        List formList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getScheme", schemeRole);
        return formList;
    }

    private List getPriority() throws SQLException {
        List priorityList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getPriority", null);

        return priorityList;
    }

//	private String[] getPriority() throws SQLException {
//		List priorityList = this.ibatisSqlMap.queryForList(this.STRING_SQL+"getPriority", null);
//		
//		String[] priorityArray = new String[priorityList.size()];
//		priorityList.toArray(priorityArray);
//		
//		return priorityArray;
//	}


    private List getQuestionLabel(String schemeId) throws SQLException {
        List collList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getQuestionLabel", schemeId);
        return collList;
    }

    private Integer countQuestionLabel() throws SQLException {
        Integer collection = 0;
        collection = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countQuestionLabel");
        return collection;
    }
}
