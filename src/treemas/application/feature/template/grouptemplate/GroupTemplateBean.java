package treemas.application.feature.template.grouptemplate;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;


public class GroupTemplateBean implements Serializable, Auditable {
    private static final String[] AUDIT_COLUMNS = new String[]{"groupid", "appid", "schemeId"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{"GROUPID", "APPID", "SCHEME_ID"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_GROUP_TEMPLATE;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"groupid", "appid", "schemeId"};

    private String groupid;
    private String groupname;
    private String appid;
    private String appname;
    private String usrUpd;
    private String schemeId;
    private String schemeDescription;

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getUsrUpd() {
        return usrUpd;
    }

    public void setUsrUpd(String usrUpd) {
        this.usrUpd = usrUpd;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }
}
