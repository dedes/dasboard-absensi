package treemas.application.feature.template.grouptemplate;

import treemas.base.feature.FeatureFormBase;


public class GroupTemplateForm extends FeatureFormBase {
    private String groupid;
    private String groupname;
    private String appid;
    private String appname;
    private String usrUpd;

    private String schemeId;
    private String schemeDescription;

    private String[] schemeList;

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getUsrUpd() {
        return usrUpd;
    }

    public void setUsrUpd(String usrUpd) {
        this.usrUpd = usrUpd;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String[] getSchemeList() {
        return schemeList;
    }

    public void setSchemeList(String[] schemeList) {
        this.schemeList = schemeList;
    }
}
