package treemas.application.feature.template.grouptemplate;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.feature.FeatureActionBase;
import treemas.base.feature.FeatureManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GroupTemplateHandler extends FeatureActionBase {

    private GroupTemplateManager manager = new GroupTemplateManager();

    public GroupTemplateHandler() {
        pageMap.put(Global.WEB_TASK_INSERT, "group");
        exceptionMap.put(Global.WEB_TASK_INSERT, "view");
        exceptionMap.put(Global.WEB_TASK_ADD, "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        GroupTemplateForm templateForm = (GroupTemplateForm) form;
        templateForm.setUsrUpd(this.getLoginId(request));
        String task = templateForm.getTask();

        if (Global.WEB_TASK_LOAD.equals(task)) {
            this.load(templateForm, request);
        } else if (Global.WEB_TASK_INSERT.equals(task)) {
            this.save(templateForm);
        }

        return this.getNextPage(task, mapping);
    }

    private void load(GroupTemplateForm templateForm, HttpServletRequest request) throws CoreException {
        templateForm.setAppname(this.manager.getApplicationName(templateForm.getAppid()));

        String[] templateList = this.manager.getTemplateOfGroup(templateForm.getGroupid(), templateForm.getAppid());
        templateForm.setSchemeList(templateList);

        List list = this.manager.getAll();
        request.setAttribute("listTemplate", list);
    }

    private void save(GroupTemplateForm templateForm) throws CoreException {
        GroupTemplateBean bean = new GroupTemplateBean();
        templateForm.toBean(bean);
        this.manager.insert(bean, templateForm.getSchemeList());
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        super.handleException(mapping, form, request, response, ex);

        GroupTemplateForm templateForm = (GroupTemplateForm) form;
        String task = templateForm.getTask();
        int errorCode = ex.getErrorCode();

        try {
            if (Global.WEB_TASK_ADD.equals(task)) {
                this.load(templateForm, request);
            }
        } catch (CoreException aie) {
            this.logger.printStackTrace(aie);
        }

        switch (errorCode) {
            case FeatureManagerBase.ERROR_DATA_EXISTS:
                this.setMessage(request, "common.dataexists", new Object[]{ex.getUserObject()});
                break;
            case FeatureManagerBase.ERROR_HAS_CHILD:
                this.setMessage(request, "appmgr.group.haschild", null);
                break;
            case FeatureManagerBase.ERROR_DB:
                this.setMessage(request, "common.dberror", null);
                break;
            default:
                this.setMessage(request, "common.error", null);
                this.logger.printStackTrace(ex);
                break;
        }
        return this.getErrorPage(task, mapping);
    }
}
