package treemas.application.feature.template.undistributegrouptemplate;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.feature.FeatureActionBase;
import treemas.base.feature.FeatureManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class UndistributeGroupTemplateHandler extends FeatureActionBase {
    private UndistributeGroupTemplateManager manager = new UndistributeGroupTemplateManager();

    public UndistributeGroupTemplateHandler() {
        pageMap.put(Global.WEB_TASK_INSERT, "group");
        exceptionMap.put(Global.WEB_TASK_INSERT, "view");
        exceptionMap.put(Global.WEB_TASK_ADD, "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        UndistributeGroupTemplateForm templateForm = (UndistributeGroupTemplateForm) form;
        templateForm.setUsrUpd(this.getLoginId(request));
        String task = templateForm.getTask();

        if (Global.WEB_TASK_LOAD.equals(task)) {
            this.load(templateForm, request);
        } else if (Global.WEB_TASK_INSERT.equals(task)) {
            this.save(templateForm);
        }

        return this.getNextPage(task, mapping);
    }

    private void load(UndistributeGroupTemplateForm templateForm, HttpServletRequest request) throws CoreException {
        templateForm.setApplicationName(this.manager.getApplicationName(
                templateForm.getApplicationId()));

        String[] templateList = this.manager.getTemplateOfGroup(
                templateForm.getGroupId(), templateForm.getApplicationId());
        templateForm.setSchemeList(templateList);

        List list = this.manager.getAll();
        request.setAttribute("listTemplate", list);
    }

    private void save(UndistributeGroupTemplateForm templateForm) throws CoreException {
        UndistributeGroupTemplateBean bean = new UndistributeGroupTemplateBean();
        templateForm.toBean(bean);
        this.manager.insert(bean, templateForm.getSchemeList());
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        super.handleException(mapping, form, request, response, ex);

        UndistributeGroupTemplateForm templateForm = (UndistributeGroupTemplateForm) form;
        String task = templateForm.getTask();
        int errorCode = ex.getErrorCode();

        try {
            if (Global.WEB_TASK_ADD.equals(task)) {
                this.load(templateForm, request);
            }
        } catch (CoreException aie) {
            this.logger.printStackTrace(aie);
        }

        switch (errorCode) {
            case FeatureManagerBase.ERROR_DATA_EXISTS:
                this.setMessage(request, "common.dataexists", new Object[]{ex.getUserObject()});
                break;
            case FeatureManagerBase.ERROR_HAS_CHILD:
                this.setMessage(request, "appmgr.group.haschild", null);
                break;
            case FeatureManagerBase.ERROR_DB:
                this.setMessage(request, "common.dberror", null);
                break;
            default:
                this.setMessage(request, "common.error", null);
                this.logger.printStackTrace(ex);
                break;
        }
        return this.getErrorPage(task, mapping);
    }
}
