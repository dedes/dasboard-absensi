package treemas.application.feature.template.undistributegrouptemplate;

import treemas.base.feature.FeatureFormBase;

public class UndistributeGroupTemplateForm extends FeatureFormBase {
    private String groupId;
    private String groupName;
    private String applicationId;
    private String applicationName;
    private String usrUpd;

    private String schemeId;
    private String schemeDescription;

    private String[] schemeList;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getUsrUpd() {
        return usrUpd;
    }

    public void setUsrUpd(String usrUpd) {
        this.usrUpd = usrUpd;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String[] getSchemeList() {
        return schemeList;
    }

    public void setSchemeList(String[] schemeList) {
        this.schemeList = schemeList;
    }
}
