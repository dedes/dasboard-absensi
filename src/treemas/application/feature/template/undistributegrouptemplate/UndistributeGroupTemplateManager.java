package treemas.application.feature.template.undistributegrouptemplate;

import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.log.ILogger;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UndistributeGroupTemplateManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_GROUP_TEMPLATE_UNDISTRIBUTE;

    public List getAll() throws CoreException {
        List list = null;
        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAll", null);
        } catch (SQLException sqlEx) {
            this.logger.printStackTrace(sqlEx);
            throw new CoreException(sqlEx, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return list;
    }

    public String[] getTemplateOfGroup(String groupId, String applicationId) throws CoreException {
        List list = null;
        String[] result = null;
        Map map = new HashMap();
        map.put("applicationId", applicationId);
        map.put("groupId", groupId);

        try {
            list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getTemplateUndistList", map);
            if (list != null && list.size() > 0) {
                result = new String[list.size()];
                list.toArray(result);
            }
        } catch (SQLException sqlEx) {
            this.logger.printStackTrace(sqlEx);
            throw new CoreException(sqlEx, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return result;
    }

    public String getApplicationName(String appId) throws CoreException {
        String appName = null;
        try {
            appName = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getAppName", appId);
        } catch (SQLException sqlEx) {
            this.logger.printStackTrace(sqlEx);
            throw new CoreException(sqlEx, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return appName;
    }

    public void insert(UndistributeGroupTemplateBean bean, String[] templateList) throws CoreException {
        try {
            try {
                this.logger.log(ILogger.LEVEL_INFO,
                        "Template saving for group: " +
                                bean.getGroupId() + "/" + bean.getApplicationId());
                this.ibatisSqlMap.startTransaction();

                this.ibatisSqlMap.delete(this.STRING_SQL + "insert-delTemplateUndistList", bean);
                if (templateList != null && templateList.length > 0) {
                    for (int i = 0; i < templateList.length; i++) {
                        bean.setSchemeId(templateList[i]);
                        this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(),
                                bean, bean.getUsrUpd(), bean.getGroupId());
                        this.ibatisSqlMap.insert(this.STRING_SQL + "insert", bean);
                    }
                }

                this.ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO,
                        "Template saved for group: " +
                                bean.getGroupId() + "/" + bean.getApplicationId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqlEx) {
            this.logger.printStackTrace(sqlEx);
            String errMsg = null;
            String usrObject = null;
            int errorCode = ERROR_DB;
            switch (sqlEx.getErrorCode()) {
                case OracleErrorCode.ERROR_DUPLICATE_KEY:
                    errorCode = ERROR_DATA_EXISTS;
                    usrObject = bean.getGroupId() + "/" + bean.getApplicationId() + "/" + bean.getSchemeId();
                    break;
                case OracleErrorCode.ERROR_CANNOT_INSERT_NULL:
                    errorCode = ERROR_UNKNOWN;
                    errMsg = sqlEx.getMessage();
                    break;
                default:
                    errorCode = ERROR_DB;
                    errMsg = sqlEx.getMessage();
                    break;
            }
            throw new CoreException(errMsg, sqlEx, errorCode, usrObject);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
    }
}
