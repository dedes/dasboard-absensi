package treemas.application.feature.template.undistributegrouptemplate;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;

public class UndistributeGroupTemplateBean implements Serializable, Auditable {
    private static final String[] AUDIT_COLUMNS = new String[]{"groupId", "applicationId", "schemeId"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{"GROUPID", "APPLICATIONID", "SCHEME_ID"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_GROUP_TEMPLATE_UNDISTRIBUTE;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"groupId", "applicationId", "schemeId"};

    private String groupId;
    private String groupName;
    private String applicationId;
    private String applicationName;
    private String usrUpd;
    private String schemeId;
    private String schemeDescription;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getUsrUpd() {
        return usrUpd;
    }

    public void setUsrUpd(String usrUpd) {
        this.usrUpd = usrUpd;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }
}
