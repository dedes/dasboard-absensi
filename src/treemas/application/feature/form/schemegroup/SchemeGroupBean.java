package treemas.application.feature.form.schemegroup;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;

public class SchemeGroupBean implements Serializable, Auditable {
    private static final String[] AUDIT_COLUMNS = new String[]{"schemeId",
            "questionGroupId", "lineSeqOrder"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{
            "SCHEME_ID", "QUESTION_GROUP_ID", "LINE_SEQ_ORDER"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_SCHEME_GROUP;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{
            "schemeId", "questionGroupId"};

    private String schemeId;
    private String schemeDescription;
    private Integer questionGroupId;
    private String questionGroupName;
    private Integer lineSeqOrder;


    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public Integer getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(Integer lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

}
