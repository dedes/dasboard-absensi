package treemas.application.feature.form.schemegroup;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class SchemeGroupValidator extends Validator {

    public SchemeGroupValidator(HttpServletRequest request) {
        super(request);
    }

    public SchemeGroupValidator(Locale locale) {
        super(locale);
    }

    public boolean validateSchemeInsert(ActionMessages messages, SchemeGroupForm form, SchemeGroupManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        Integer countSeq = 0;

        try {
            countSeq = manager.countAvailableSequenceOrder(form.getSchemeId(), form.getLineSeqOrder());
            if (countSeq > 0) {
                messages.add("lineSeqOrder", new ActionMessage("setting.sequenceorder.dataexists", "SEQUENCE ORDER"));
            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }

        this.textValidator(messages, form.getQuestionGroupName(), "feature.question.group", "questionGroupName", "1", this.getStringLength(20), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
        this.textValidator(messages, form.getLineSeqOrder(), "feature.sequence.order", "lineSeqOrder", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
