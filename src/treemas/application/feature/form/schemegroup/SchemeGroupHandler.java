package treemas.application.feature.form.schemegroup;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.log.ILogger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class SchemeGroupHandler extends FeatureActionBase {
    private final String screenId = "";
    private final String TASK_SHOW_QUESTION_GROUP = "QuestionGroupLoad";
    private final String TASK_INSERT_QUESTION_GROUP = "QuestionGroupSave";
    private final String TASK_DELETE_QUESTION_GROUP = "QuestionGroupDelete";
    private final String TASK_UPDATE_QUESTION_GROUP = "QuestionGroupUpdate";
    private SchemeGroupManager manager = new SchemeGroupManager();
    private SchemeGroupValidator sValidator;

    public SchemeGroupHandler() {
        this.pageMap.put("QuestionGroupLoad", "view");
        this.pageMap.put("QuestionGroupSave", "view");
        this.pageMap.put("QuestionGroupDelete", "view");
        this.pageMap.put("QuestionGroupUpdate", "view");

        this.exceptionMap.put("QuestionGroupLoad", "view");
        this.exceptionMap.put("QuestionGroupSave", "view");
        this.exceptionMap.put("QuestionGroupDelete", "view");
        this.exceptionMap.put("QuestionGroupUpdate", "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.sValidator = new SchemeGroupValidator(request);
        SchemeGroupForm schemeGroupForm = (SchemeGroupForm) form;
        String action = schemeGroupForm.getTask();

        if (TASK_SHOW_QUESTION_GROUP.equals(action)) {
            this.getSchemeHeader(schemeGroupForm);
            this.loadAvailableQuestionGroup(request, schemeGroupForm);
            this.loadQuestionGroupScheme(request, schemeGroupForm);
        } else if (TASK_INSERT_QUESTION_GROUP.equals(action)) {
            try {
                if (this.sValidator.validateSchemeInsert(new ActionMessages(), schemeGroupForm, this.manager)) {
                    this.manager.insertQuestionGroupScheme(
                            schemeGroupForm.getSchemeId(),
                            new Integer(schemeGroupForm.getQuestionGroupId()),
                            new Integer(schemeGroupForm.getLineSeqOrder()),
                            this.getLoginId(request), this.screenId);
                    schemeGroupForm.setQuestionGroupId("");
                    schemeGroupForm.setQuestionGroupName("");
                    schemeGroupForm.setLineSeqOrder("");
                    request.setAttribute("message", resources.getMessage("common.process.save"));
                }
            } finally {
                this.getSchemeHeader(schemeGroupForm);
                this.loadAvailableQuestionGroup(request, schemeGroupForm);
                this.loadQuestionGroupScheme(request, schemeGroupForm);
            }
        } else if (TASK_DELETE_QUESTION_GROUP.equals(action)) {
            try {
                this.manager.deleteQuestionGroupScheme(
                        schemeGroupForm.getSchemeId(),
                        new Integer(schemeGroupForm.getSelectedQuestionGroupId()),
                        this.getLoginId(request), this.screenId);
            } finally {
                this.getSchemeHeader(schemeGroupForm);
                this.loadAvailableQuestionGroup(request, schemeGroupForm);
                this.loadQuestionGroupScheme(request, schemeGroupForm);
            }
        } else if (TASK_UPDATE_QUESTION_GROUP.equals(action)) {
            try {
                List list = schemeGroupForm.getListQuestionGroup();
                list = BeanConverter.convertBeansFromString(list, SchemeGroupBean.class);
                this.manager.updateSequenceQuestionGroup(list,
                        schemeGroupForm.getSchemeId(), this.getLoginId(request));
                request.setAttribute("message", resources.getMessage("common.process.update"));
            } finally {
                this.getSchemeHeader(schemeGroupForm);
                this.loadAvailableQuestionGroup(request, schemeGroupForm);
                this.loadQuestionGroupScheme(request, schemeGroupForm);
            }
        } else if (Global.WEB_TASK_BACK.equals(action)) {
            String prevUri = schemeGroupForm.getPreviousUri();
            if (prevUri != null && !prevUri.equals("")) {
                String ctxPath = request.getContextPath();
                if (prevUri.startsWith(ctxPath)) {
                    prevUri = prevUri.substring(ctxPath.length());
                }
                return new ActionForward(prevUri, true);
            } else {
                this.logger.log(ILogger.LEVEL_ERROR,
                        "Cannot find (or empty) 'previousUri' in form");
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadQuestionGroupScheme(HttpServletRequest request, SchemeGroupForm schemeGroupForm)
            throws CoreException {
        List list = this.manager.getQuestionGroupScheme(schemeGroupForm.getSchemeId());
        list = BeanConverter.convertBeansToString(list, SchemeGroupForm.class);
        schemeGroupForm.setListQuestionGroup(list);
    }

    private void loadAvailableQuestionGroup(HttpServletRequest request, SchemeGroupForm schemeGroupForm)
            throws CoreException {
        List list = this.manager.getAvailableQuestionGroup(schemeGroupForm.getSchemeId());
        request.setAttribute("comboQuestionGroup", list);
    }

    private void getSchemeHeader(SchemeGroupForm schemeGroupForm) throws CoreException {
        String schemeDescription = this.manager.getSchemeDescription(
                schemeGroupForm.getSchemeId());
        schemeGroupForm.setSchemeDescription(schemeDescription);
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;

        switch (code) {
            case SchemeGroupManager.ERROR_DATA_EXISTS:
                this.setMessage(request, "setting.schemegroup.dataexists",
                        new Object[]{ex.getUserObject()});
                break;

            case SchemeGroupManager.ERROR_HAS_CHILD:
                this.setMessage(request, "common.haschild", null);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
        }

        ((SchemeGroupForm) form).setTask(TASK_SHOW_QUESTION_GROUP);
        String task = ((SchemeGroupForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
