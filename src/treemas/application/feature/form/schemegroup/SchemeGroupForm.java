package treemas.application.feature.form.schemegroup;

import treemas.base.feature.FeatureFormBase;

import java.util.ArrayList;
import java.util.List;

public class SchemeGroupForm extends FeatureFormBase {
    private String schemeId;
    private String schemeDescription;
    private String questionGroupId;
    private String questionGroupName;
    private String lineSeqOrder;

    private String selectedQuestionGroupId;
    private List listQuestionGroup;

    public List getListQuestionGroup() {
        return listQuestionGroup;
    }

    public void setListQuestionGroup(List listQuestionGroup) {
        this.listQuestionGroup = listQuestionGroup;
    }

    public SchemeGroupForm getData(int idx) {
        if (this.listQuestionGroup == null) {
            this.listQuestionGroup = new ArrayList(idx + 1);
        }
        while (this.listQuestionGroup.size() < (idx + 1)) {
            this.listQuestionGroup.add(new SchemeGroupForm());
        }
        return (SchemeGroupForm) this.listQuestionGroup.get(idx);
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(String lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

    public String getSelectedQuestionGroupId() {
        return selectedQuestionGroupId;
    }

    public void setSelectedQuestionGroupId(String selectedQuestionGroupId) {
        this.selectedQuestionGroupId = selectedQuestionGroupId;
    }
}
