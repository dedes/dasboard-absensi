package treemas.application.feature.form.schemegroup;

import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.log.ILogger;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SchemeGroupManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_SCHEME_GROUP;

    public List getQuestionGroupScheme(String schemeId) throws CoreException {
        List list = null;
        try {
            list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getQuestionGroupScheme", schemeId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return list;
    }

    public String getSchemeDescription(String schemeId) throws CoreException {
        String schemeDescription = null;
        try {
            schemeDescription = (String) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getSchemeDescription", schemeId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return schemeDescription;
    }

    public List getAvailableQuestionGroup(String schemeId) throws CoreException {
        List list = null;
        try {
            list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAvailableQuestionGroup", schemeId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return list;
    }

    public void insertQuestionGroupScheme(String schemeId,
                                          Integer questionGroupId, Integer lineSeqOrder,
                                          String loginId, String screenId) throws CoreException {
        SchemeGroupBean schemeGroupBean = new SchemeGroupBean();
        schemeGroupBean.setSchemeId(schemeId);
        schemeGroupBean.setQuestionGroupId(questionGroupId);
        schemeGroupBean.setLineSeqOrder(lineSeqOrder);
        try {
            try {
                this.ibatisSqlMap.startTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Inserting Question Group Scheme "
                        + schemeId + " / " + questionGroupId);

                this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), schemeGroupBean, loginId, screenId);
                this.ibatisSqlMap.insert(this.STRING_SQL + "insertQuestionGroupScheme", schemeGroupBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateLastUpdateScheme", schemeId);

                this.logger.log(ILogger.LEVEL_INFO, "Inserted Question Group Scheme "
                        + schemeId + " / " + questionGroupId);

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new CoreException("", sqle, ERROR_DATA_EXISTS,
                        questionGroupId);
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void deleteQuestionGroupScheme(String schemeId, Integer questionGroupId,
                                          String loginId, String screenId) throws CoreException {
        SchemeGroupBean schemeGroupBean = new SchemeGroupBean();
        schemeGroupBean.setSchemeId(schemeId);
        schemeGroupBean.setQuestionGroupId(questionGroupId);
        try {
            try {
                this.ibatisSqlMap.startTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Deleting Question Group Scheme "
                        + schemeId + " / " + questionGroupId);

                this.auditManager.auditDelete(this.ibatisSqlMap.getCurrentConnection(), schemeGroupBean, loginId, "");
                this.ibatisSqlMap.delete(this.STRING_SQL + "deleteQuestionGroupScheme", schemeGroupBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateLastUpdateScheme", schemeId);
                this.logger.log(ILogger.LEVEL_INFO, "Deleted Question Group Scheme "
                        + schemeId + " / " + questionGroupId);

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND) {
                throw new CoreException("", sqle, ERROR_HAS_CHILD, questionGroupId);
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void updateSequenceQuestionGroup(List listQuestionGroup, String schemeId, String loginId)
            throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Updating question group sequence, scheme "
                        + schemeId);
                for (int i = 0; i < listQuestionGroup.size(); i++) {
                    SchemeGroupBean paramBean = (SchemeGroupBean) listQuestionGroup.get(i);
                    this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), paramBean, loginId, "");
                    this.ibatisSqlMap.update(this.STRING_SQL + "updateSequence", paramBean);
                }
                this.ibatisSqlMap.update(this.STRING_SQL + "updateLastUpdateScheme", schemeId);
                this.logger.log(ILogger.LEVEL_INFO, "Updated question group sequence, scheme "
                        + schemeId);

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public Integer countAvailableSequenceOrder(String schemeId, String sequence) throws CoreException {
        Integer countSeq = 0;
        Map param = new HashMap();
        param.put("schemeId", schemeId);
        param.put("sequence", sequence);
        try {
            countSeq = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "countSequenceId", param);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return countSeq;
    }

}
