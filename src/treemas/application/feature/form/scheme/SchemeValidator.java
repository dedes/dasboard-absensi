package treemas.application.feature.form.scheme;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class SchemeValidator extends Validator {

    public SchemeValidator(HttpServletRequest request) {
        super(request);
    }

    public SchemeValidator(Locale locale) {
        super(locale);
    }

    public boolean validateSchemeInsert(ActionMessages messages, SchemeForm form, SchemeFormManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        try {
            if (!Strings.isNullOrEmpty(form.getSchemeId())) {
                SchemeFormBean bean = manager.getSingleScheme(form.getSchemeId());
                if (null != bean) {
                    messages.add("schemeId", new ActionMessage("errors.duplicate", "SCHEME ID"));
                }
            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }

        this.textValidator(messages, form.getSchemeId(), "common.formid", "schemeId", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getSchemeDescription(), "feature.scheme.description", "schemeDescription", "1", this.getStringLength(30), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }

    public boolean validateSchemeEdit(ActionMessages messages, SchemeForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.textValidator(messages, form.getSchemeDescription(), "feature.scheme.description", "schemeDescription", "1", this.getStringLength(30), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
