package treemas.application.feature.form.scheme;

import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.databases.SQLErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class SchemeFormManager extends FeatureManagerBase {
    public static final int ERROR_IN_USE = 101;
    static final int SEARCH_BY_SCHEME_ID = 10;
    static final int SEARCH_BY_SCHEME_DESC = 20;
    static final int ERROR_WEB_SERVICE = 500;
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_SCHEME;

    public PageListWrapper getAllScheme(int numberPage, int searchType,
                                        String searchValue) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        if (searchType != SEARCH_BY_SCHEME_ID
                && searchType != SEARCH_BY_SCHEME_DESC)
            throw new IllegalArgumentException("Invalid searchType parameter: "
                    + searchType);

        SearchFormParameter param = new SearchFormParameter();
        searchValue = searchValue != null ? searchValue.trim() : searchValue;
        param.setPage(numberPage);
        param.setSearchType1(searchType);
        param.setSearchValue1(searchValue);
        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", param);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", param);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public SchemeFormBean getSingleScheme(String schemeId) throws CoreException {
        SchemeFormBean result = null;
        try {
            result = (SchemeFormBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "get", schemeId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public void updateScheme(SchemeFormBean schemeBean, String loginId,
                             String screenId) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Updating Scheme "
                        + schemeBean.getSchemeId());

                this.auditManager.auditEdit(this.ibatisSqlMap
                        .getCurrentConnection(), schemeBean, loginId, screenId);
                this.ibatisSqlMap.update(this.STRING_SQL + "update",
                        schemeBean);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Updated Scheme "
                        + schemeBean.getSchemeId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException se) {
            this.logger.printStackTrace(se);
            throw new CoreException(se, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void insertScheme(SchemeFormBean schemeBean, String loginId,
                             String screenId) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Inserting Scheme "
                        + schemeBean.getSchemeId());

                this.auditManager.auditAdd(this.ibatisSqlMap
                        .getCurrentConnection(), schemeBean, loginId, screenId);
                this.ibatisSqlMap.insert(this.STRING_SQL + "insert",
                        schemeBean);

                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Inserted Scheme "
                        + schemeBean.getSchemeId());
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY || sqle.getErrorCode() == SQLErrorCode.ERROR_DUPLICATE_KEY) {
                throw new CoreException("", sqle, ERROR_DATA_EXISTS, schemeBean
                        .getSchemeId());
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void deleteScheme(String schemeId, String loginId, String screenId)
            throws CoreException {
        SchemeFormBean schemeBean = new SchemeFormBean();
        schemeBean.setSchemeId(schemeId);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Deleting Scheme " + schemeId);

                this.auditManager.auditDelete(this.ibatisSqlMap
                        .getCurrentConnection(), schemeBean, loginId, screenId);
                this.ibatisSqlMap.delete(this.STRING_SQL + "delete", schemeBean);
                this.ibatisSqlMap.commitTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Deleted Scheme " + schemeId);
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND || sqle.getErrorCode() == SQLErrorCode.ERROR_CHILD_FOUND) {
                throw new CoreException("", sqle, ERROR_HAS_CHILD, schemeId);

            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public boolean isNonActiveable(String schemeId) throws CoreException {
        boolean result = false;
        try {
            Integer cnt = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "cntActive",
                    schemeId);
            result = (cnt.intValue() == 0);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }
/*
    public void copyFileToServer(SchemeBean schemeBean, FormFile file,
			String action) {
		String filelocation = null;
		String filename = null;
		if (file != null) {
			filelocation = Global.REPORT_LOCATION_FOLDER + File.separator
					+ file.getFileName();

			if (!file.getFileName().endsWith("rptdesign")
					&& !"".equals(file.getFileName())) {
				throw new IllegalArgumentException(
						"report file must be rptdesign file");
			} else if ("".equals(file.getFileName())
					&& Global.WEB_TASK_INSERT.equals(action)) {
				filelocation = Global.REPORT_LOCATION_FOLDER + File.separator
						+ Global.DEFAULT_REPORT_FILE;
				filename = Global.DEFAULT_REPORT_FILE;
			} else if ("".equals(file.getFileName())
					&& Global.WEB_TASK_UPDATE.equals(action)) {
				
				//kalau tidak ada perubahan file ketika edit 
				filelocation = schemeBean.getOldSchemeReportFile();
				filename = schemeBean.getOldSchemeReportFileName();
			}

			else {

				File f = new File(filelocation);
				filename = file.getFileName();
				try {
					FileOutputStream fos = new FileOutputStream(f);
					fos.write(file.getFileData());
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// set file location dan file name ke bean
//		schemeBean.setSchemeReportFile(filelocation);
//		schemeBean.setSchemeReportFileName(filename);
//		System.out.println("file location: " + filelocation);
//		System.out.println("filename: " + filename);
	}

//	public void handleUploadFile(SchemeBean schemeBean, SchemeForm schemeForm,
//			String action) {
//		System.out.println("schemeForm.getFile(): " + schemeForm.getFile());
//		FormFile file = schemeForm.getFile();
//		this.copyFileToServer(schemeBean, file, action);
//		schemeForm.setSchemeReportFile(file.getFileName());
//	}
*/

}
