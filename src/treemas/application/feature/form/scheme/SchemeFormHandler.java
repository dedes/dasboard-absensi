package treemas.application.feature.form.scheme;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class SchemeFormHandler extends FeatureActionBase {
    private static final String[] SAVE_PARAMETERS = {
            "paging.currentPageNo", "paging.totalRecord",
            "paging.dispatch", "searchValue",
            "searchType"};
    private final String screenId = "";
    private SchemeFormManager manager = new SchemeFormManager();
    private SchemeValidator sValidator;

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.sValidator = new SchemeValidator(request);
        SchemeForm schemeForm = (SchemeForm) form;
        schemeForm.getPaging().calculationPage();
        String action = schemeForm.getTask();

        if (Global.WEB_TASK_LOAD.equals(action) ||
                Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadList(request, schemeForm);
            schemeForm.fillCurrentUri(request, SAVE_PARAMETERS);
        } else if (Global.WEB_TASK_ADD.equals(action)) {
            schemeForm.setSchemeId("");
            schemeForm.setSchemeDescription("");
            schemeForm.setActive("1");
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.sValidator.validateSchemeInsert(new ActionMessages(), schemeForm, this.manager)) {
                SchemeFormBean schemeBean = new SchemeFormBean();
                schemeForm.toBean(schemeBean);

                this.manager.insertScheme(schemeBean, this.getLoginId(request), this.screenId);
                this.loadList(request, schemeForm);
                request.setAttribute("messagesave", resources.getMessage("common.process.save"));
            }

        } else if (Global.WEB_TASK_EDIT.equals(action)) {
            SchemeFormBean schemeBean = this.manager.getSingleScheme(schemeForm.getSchemeId());
            schemeForm.fromBean(schemeBean);
            schemeForm.getPaging().setDispatch(null);

        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
            if (this.sValidator.validateSchemeEdit(new ActionMessages(), schemeForm)) {
                String activeOri = schemeForm.getActiveOri();
                if (!activeOri.equals(schemeForm.getActive())) {
                    boolean isNonactiveable = this.manager.isNonActiveable(schemeForm.getSchemeId());
                    if (!isNonactiveable) {
                        throw new CoreException(SchemeFormManager.ERROR_IN_USE);
                    }
                }

                SchemeFormBean schemeBean = new SchemeFormBean();
                schemeForm.toBean(schemeBean);

                this.manager.updateScheme(schemeBean, this.getLoginId(request),
                        this.screenId);
                this.loadList(request, schemeForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
            }
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = schemeForm.getPaging();
            try {
                this.manager.deleteScheme(schemeForm.getSchemeId(),
                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.loadList(request, schemeForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadList(HttpServletRequest request, SchemeForm schemeForm)
            throws CoreException {
        int targetPageNo = schemeForm.getPaging().getTargetPageNo();
        int searchType = Integer.parseInt(schemeForm.getSearchType());
        if (Global.WEB_TASK_SEARCH.equals(schemeForm.getTask())) {
            targetPageNo = 1;
        }
        PageListWrapper result = this.manager.getAllScheme(targetPageNo,
                searchType, schemeForm.getSearchValue());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, SchemeForm.class);
        schemeForm.getPaging().setCurrentPageNo(targetPageNo);
        schemeForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listScheme", list);
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case SchemeFormManager.ERROR_IN_USE:
                this.setMessage(request, "setting.scheme.inuse",
                        new Object[]{ex.getUserObject()});
                request.setAttribute("schemeinuse", resources.getMessage("setting.scheme.inuse"));
                break;

            case SchemeFormManager.ERROR_DATA_EXISTS:
                this.setMessage(request, "setting.scheme.dataexists",
                        new Object[]{ex.getUserObject()});
                break;

            case SchemeFormManager.ERROR_HAS_CHILD:
                this.setMessage(request, "common.haschild", null);
                break;

            case SchemeFormManager.ERROR_WEB_SERVICE:
                this.setMessage(request, "common.webservice", null);
                break;

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((SchemeForm) form).setTask(((SchemeForm) form).resolvePreviousTask());
        String task = ((SchemeForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
