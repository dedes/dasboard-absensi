package treemas.application.feature.form.scheme;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;


public class SchemeFormBean implements Serializable, Auditable {


    private static final String[] AUDIT_COLUMNS = new String[]{"schemeId", "schemeDescription", "reportFile", "isPrintable", "scheme_role", "active"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{"SCHEME_ID", "SCHEME_DESCRIPTION", "REPORT_FILE", "IS_PRINTABLE", "SCHEME_ROLE", "ACTIVE"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_SCHEME;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"schemeId"};

    private String schemeId;
    private String reportFile;

    private String schemeDescription;
    private String active;
    private String activeOri;
    private String isPrintable;

    private Integer questionGroupId;
    private String questionGroupName;
    private Integer lineSeqOrder;

    private String scheme_role;

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getReportFile() {
        return reportFile;
    }

    public void setReportFile(String reportFile) {
        this.reportFile = reportFile;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getActiveOri() {
        return activeOri;
    }

    public void setActiveOri(String activeOri) {
        this.activeOri = activeOri;
    }

    public String getIsPrintable() {
        return isPrintable;
    }

    public void setIsPrintable(String isPrintable) {
        this.isPrintable = isPrintable;
    }

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public Integer getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(Integer lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

    public String getScheme_role() {
        return scheme_role;
    }

    public void setScheme_role(String scheme_role) {
        this.scheme_role = scheme_role;
    }


}
