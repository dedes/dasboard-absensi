package treemas.application.feature.form.scheme;

import treemas.base.feature.FeatureFormBase;
import treemas.util.constant.Global;

public class SchemeForm extends FeatureFormBase {
    private String schemeId;
    private String schemeDescription;
    private String reportFile = Global.DEFAULT_REPORT_FILE;
    private String schemeReportFileName;
    private String active;
    private String activeOri;
    private String isPrintable;
    private String searchType = "10";
    private String searchValue;
    private String questionGroupId;
    private String questionGroupName;
    private String lineSeqOrder;
    private String scheme_role;

    public SchemeForm() {
        active = "0";
        isPrintable = "0";
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = (schemeDescription == null) ? null : schemeDescription.trim();
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getActiveOri() {
        return activeOri;
    }

    public void setActiveOri(String activeOri) {
        this.activeOri = activeOri;
    }

    public String getIsPrintable() {
        return isPrintable;
    }

    public void setIsPrintable(String isPrintable) {
        this.isPrintable = isPrintable;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = (searchValue == null) ? null : searchValue.trim();
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(String lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

    public String getReportFile() {
        return reportFile;
    }

    public void setReportFile(String reportFile) {
        this.reportFile = reportFile;
    }

    public String getSchemeReportFileName() {
        return schemeReportFileName;
    }

    public void setSchemeReportFileName(String schemeReportFileName) {
        this.schemeReportFileName = schemeReportFileName;
    }

    public String getScheme_role() {
        return scheme_role;
    }

    public void setScheme_role(String scheme_role) {
        this.scheme_role = scheme_role;
    }

}
