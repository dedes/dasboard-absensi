package treemas.application.feature.form.printitem;

import org.apache.struts.action.ActionMessages;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class SchemePrintItemValidator extends Validator {

    public SchemePrintItemValidator(HttpServletRequest request) {
        super(request);
    }

    public SchemePrintItemValidator(Locale locale) {
        super(locale);
    }

    public boolean validatePrintItemAdd(ActionMessages messages, SchemePrintItemForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        this.comboValidator(messages, form.getPrintItemTypeId(), "1", this.getStringLength(3), "feature.scheme.item.type", "printItemTypeId", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getLineSeqOrder(), "feature.sequence.order", "lineSeqOrder", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
        return ret;
    }

    public boolean validatePrintItemEdit(ActionMessages messages, SchemePrintItemForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        this.comboValidator(messages, form.getPrintItemTypeId(), "1", this.getStringLength(3), "feature.scheme.item.type", "printItemTypeId", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getLineSeqOrder(), "feature.sequence.order", "lineSeqOrder", "1", this.getStringLength(5), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
