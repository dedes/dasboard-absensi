package treemas.application.feature.form.printitem;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.general.ComboManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.log.ILogger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class SchemePrintItemHandler extends FeatureActionBase {
    private static final String TASK_SHOW_PRINT_ITEM = "PrintItemLoad";
    private static final String TASK_INSERT_PRINT_ITEM = "PrintItemSave";
    private static final String TASK_DELETE_PRINT_ITEM = "PrintItemDelete";
    private static final String TASK_UPDATE_PRINT_ITEM = "PrintItemUpdate";
    private static final String TASK_UPDATE_SEQ_PRINT_ITEM = "PrintItemSeqUpdate";
    private static final String TASK_EDIT_PRINT_ITEM = "PrintItemEdit";
    private static final String TASK_ADD_PRINT_ITEM = "PrintItemAdd";
    private final String screenId = "";
    private SchemePrintItemManager manager = new SchemePrintItemManager();
    private SchemePrintItemValidator sValidator;

    public SchemePrintItemHandler() {
        this.pageMap.put(TASK_SHOW_PRINT_ITEM, "view");
        this.pageMap.put(TASK_INSERT_PRINT_ITEM, "view");
        this.pageMap.put(TASK_DELETE_PRINT_ITEM, "view");
        this.pageMap.put(TASK_UPDATE_PRINT_ITEM, "view");
        this.pageMap.put(TASK_UPDATE_SEQ_PRINT_ITEM, "view");
        this.pageMap.put(TASK_EDIT_PRINT_ITEM, "edit");
        this.pageMap.put(TASK_ADD_PRINT_ITEM, "edit");

        this.exceptionMap.put(TASK_SHOW_PRINT_ITEM, "view");
        this.exceptionMap.put(TASK_DELETE_PRINT_ITEM, "view");
        this.exceptionMap.put(TASK_UPDATE_SEQ_PRINT_ITEM, "view");
        this.exceptionMap.put(TASK_INSERT_PRINT_ITEM, "edit");
        this.exceptionMap.put(TASK_UPDATE_PRINT_ITEM, "edit");
        this.exceptionMap.put(TASK_EDIT_PRINT_ITEM, "edit");
        this.exceptionMap.put(TASK_ADD_PRINT_ITEM, "edit");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.sValidator = new SchemePrintItemValidator(request);
        SchemePrintItemForm schemePrintItemForm = (SchemePrintItemForm) form;
        String action = schemePrintItemForm.getTask();

        if (TASK_SHOW_PRINT_ITEM.equals(action)) {
            this.getSchemeHeader(schemePrintItemForm);
            this.loadPrintItem(request, schemePrintItemForm);
        } else if (TASK_ADD_PRINT_ITEM.equals(action)) {
            this.getSchemeHeader(schemePrintItemForm);
            this.loadPrintItemType(request);
        } else if (TASK_INSERT_PRINT_ITEM.equals(action)) {
            if (this.sValidator.validatePrintItemAdd(new ActionMessages(), schemePrintItemForm)) {
                SchemePrintItemBean schemePrintItemBean = new SchemePrintItemBean();
                schemePrintItemForm.toBean(schemePrintItemBean);

                try {
                    this.manager.insertPrintItem(schemePrintItemBean, this.getLoginId(request),
                            this.screenId);
                    this.loadPrintItem(request, schemePrintItemForm);
                    this.getSchemeHeader(schemePrintItemForm);
                } catch (CoreException me) {
                    this.loadPrintItemType(request);
                    throw me;
                }
            }
        } else if (TASK_EDIT_PRINT_ITEM.equals(action)) {
            this.getSchemeHeader(schemePrintItemForm);
            this.loadPrintItemType(request);

            String schemeId = schemePrintItemForm.getSchemeId();
            String schemeDescription = schemePrintItemForm.getSchemeDescription();
            Integer printItemSeqno = new Integer(schemePrintItemForm.getPrintItemSeqno());
            SchemePrintItemBean bean = this.manager.getSinglePrintItem(
                    schemeId, printItemSeqno);

            schemePrintItemForm.fromBean(bean);
            schemePrintItemForm.getPaging().setDispatch(null);
            schemePrintItemForm.setSchemeDescription(schemeDescription);
        } else if (TASK_UPDATE_PRINT_ITEM.equals(action)) {
            SchemePrintItemBean schemePrintItemBean = new SchemePrintItemBean();
            schemePrintItemForm.toBean(schemePrintItemBean);

            try {
                if (this.sValidator.validatePrintItemEdit(new ActionMessages(), schemePrintItemForm)) {
                    this.manager.updatePrintItem(schemePrintItemBean, this.getLoginId(request),
                            this.screenId);
                }
                this.loadPrintItem(request, schemePrintItemForm);
                this.getSchemeHeader(schemePrintItemForm);
            } catch (CoreException me) {
                this.loadPrintItemType(request);
                throw me;
            }
        } else if (TASK_DELETE_PRINT_ITEM.equals(action)) {
            try {
                this.manager.deletePrintItem(
                        schemePrintItemForm.getSchemeId(),
                        new Integer(schemePrintItemForm.getPrintItemSeqno()),
                        this.getLoginId(request), this.screenId);
            } finally {
                this.getSchemeHeader(schemePrintItemForm);
                this.loadPrintItem(request, schemePrintItemForm);
            }
        } else if (TASK_UPDATE_SEQ_PRINT_ITEM.equals(action)) {
            try {
                List list = schemePrintItemForm.getListPrintItem();
                list = BeanConverter.convertBeansFromString(list, SchemePrintItemBean.class);
                this.manager.updateSequencePrintItem(list,
                        schemePrintItemForm.getSchemeId(), this.getLoginId(request));
            } finally {
                this.getSchemeHeader(schemePrintItemForm);
                this.loadPrintItem(request, schemePrintItemForm);
            }
        } else if (Global.WEB_TASK_BACK.equals(action)) {
            String prevUri = schemePrintItemForm.getPreviousUri();
            if (prevUri != null && !prevUri.equals("")) {
                String ctxPath = request.getContextPath();
                if (prevUri.startsWith(ctxPath)) {
                    prevUri = prevUri.substring(ctxPath.length());
                }
                return new ActionForward(prevUri, true);
            } else {
                this.logger.log(ILogger.LEVEL_ERROR,
                        "Cannot find (or empty) 'previousUri' in form");
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadPrintItem(HttpServletRequest request, SchemePrintItemForm schemePrintItemForm)
            throws CoreException {
        List list = this.manager.getPrintItem(schemePrintItemForm.getSchemeId());
        list = BeanConverter.convertBeansToString(list, SchemePrintItemForm.class);
        schemePrintItemForm.setListPrintItem(list);
    }

    private void loadPrintItemType(HttpServletRequest request) throws CoreException {
        ComboManager comboManager = new ComboManager();
        List list = comboManager.getComboPrintItemType();
        request.setAttribute("listPrintItemType", list);
    }

    private void getSchemeHeader(SchemePrintItemForm schemePrintItemForm) throws CoreException {
        String schemeDescription = this.manager.getSchemeDescription(
                schemePrintItemForm.getSchemeId());
        schemePrintItemForm.setSchemeDescription(schemeDescription);
    }

    public String resolvePreviousTask(ActionForm form) {
        SchemePrintItemForm schemePrintItemForm = (SchemePrintItemForm) form;
        String task = schemePrintItemForm.getTask();
        if (TASK_INSERT_PRINT_ITEM.equals(task))
            return TASK_ADD_PRINT_ITEM;
        else if (TASK_UPDATE_PRINT_ITEM.equals(task))
            return TASK_EDIT_PRINT_ITEM;
        else
            return schemePrintItemForm.resolvePreviousTask();
    }


    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        SchemePrintItemForm schemePrintItemForm = (SchemePrintItemForm) form;
        switch (code) {
            case SchemePrintItemManager.ERROR_DATA_EXISTS:
                this.setMessage(request, "setting.schemegroup.dataexists",
                        new Object[]{ex.getUserObject()});
                break;

            case SchemePrintItemManager.ERROR_HAS_CHILD:
                this.setMessage(request, "common.haschild", null);
                break;

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                try {
                    this.getSchemeHeader(schemePrintItemForm);
                    this.loadPrintItemType(request);
                } catch (CoreException e) {
                    e.printStackTrace();
                    this.logger.printStackTrace(e);
                }
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((SchemePrintItemForm) form).setTask(this.resolvePreviousTask(schemePrintItemForm));
        String task = ((SchemePrintItemForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
