package treemas.application.feature.form.printitem;

import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.CoreException;
import treemas.util.constant.PrintItemType;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.log.ILogger;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SchemePrintItemManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_PRINT_ITEM;

    public List getPrintItem(String schemeId) throws CoreException {
        List list = null;
        try {
            list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getPrintItem", schemeId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return list;
    }

    public SchemePrintItemBean getSinglePrintItem(String schemeId, Integer printItemSeqno) throws CoreException {
        SchemePrintItemBean result = null;
        Map paramMap = new HashMap();
        paramMap.put("schemeId", schemeId);
        paramMap.put("printItemSeqno", printItemSeqno);
        try {
            result = (SchemePrintItemBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getOnePrintItem", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public String getSchemeDescription(String schemeId) throws CoreException {
        String schemeDescription = null;
        try {
            schemeDescription = (String) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getSchemeDescription", schemeId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return schemeDescription;
    }

    public List getAvailableQuestion(String schemeId) throws CoreException {
        List list = null;
        try {
            list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAvailableQuestion", schemeId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return list;
    }

    public void insertPrintItem(SchemePrintItemBean schemeGroupBean,
                                String loginId, String screenId) throws CoreException {
        String schemeId = schemeGroupBean.getSchemeId();
        Integer printItemSeqno = null;
        try {
            try {
                this.ibatisSqlMap.startTransaction();

                boolean noSequence = "0".equals(ConfigurationProperties.getInstance().
                        get(PropertiesKey.DB_SEQUENCE));
                if (!noSequence) {
                    SequenceManager sequenceManager = new SequenceManager();
                    printItemSeqno = new Integer(sequenceManager.getSequencePrintItem());
                    schemeGroupBean.setPrintItemSeqno(printItemSeqno);
                }


                if (printItemSeqno != null) {
                    this.logger.log(ILogger.LEVEL_INFO, "Inserting Print Item Scheme "
                            + schemeId + " / " + printItemSeqno.toString());
                } else {
                    this.logger.log(ILogger.LEVEL_INFO, "Inserting Print Item Scheme "
                            + schemeId);
                }

                this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), schemeGroupBean, loginId, screenId);
                this.ibatisSqlMap.insert(this.STRING_SQL + "insertPrintItem", schemeGroupBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", schemeId);

                if (printItemSeqno != null) {
                    this.logger.log(ILogger.LEVEL_INFO, "Inserted Print Item Scheme "
                            + schemeId + " / " + printItemSeqno.toString());
                } else {
                    this.logger.log(ILogger.LEVEL_INFO, "Inserted Print Item Scheme "
                            + schemeId);
                }

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new CoreException("", sqle, ERROR_DATA_EXISTS,
                        printItemSeqno.toString());
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void updatePrintItem(SchemePrintItemBean schemeGroupBean,
                                String loginId, String screenId) throws CoreException {
        String schemeId = schemeGroupBean.getSchemeId();
        String printItemSeqno = schemeGroupBean.getPrintItemSeqno().toString();
        String type = schemeGroupBean.getPrintItemTypeId();
        try {
            try {
                this.ibatisSqlMap.startTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Updating Print Item Scheme "
                        + schemeId + " / " + printItemSeqno);

                this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), schemeGroupBean, loginId, screenId);

                if (PrintItemType.TYPE_QUESTION.equals(type)) {
                    this.ibatisSqlMap.update(this.STRING_SQL + "updatePrintItemQuestion", schemeGroupBean);
                } else {
                    this.ibatisSqlMap.update(this.STRING_SQL + "updatePrintItemNonQuestion", schemeGroupBean);
                }

                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", schemeId);

                this.logger.log(ILogger.LEVEL_INFO, "Updated Print Item Scheme "
                        + schemeId + " / " + printItemSeqno);

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void deletePrintItem(String schemeId, Integer printItemSeqno,
                                String loginId, String screenId) throws CoreException {
        SchemePrintItemBean schemeGroupBean = new SchemePrintItemBean();
        schemeGroupBean.setSchemeId(schemeId);
        schemeGroupBean.setPrintItemSeqno(printItemSeqno);
        try {
            try {
                this.ibatisSqlMap.startTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Deleting Print Item Scheme "
                        + schemeId + " / " + printItemSeqno);

                this.auditManager.auditDelete(this.ibatisSqlMap.getCurrentConnection(), schemeGroupBean, loginId, "");
                this.ibatisSqlMap.delete(this.STRING_SQL + "deletePrintItem", schemeGroupBean);
                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", schemeId);
                this.logger.log(ILogger.LEVEL_INFO, "Deleted Print Item Scheme "
                        + schemeId + " / " + printItemSeqno);

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND) {
                throw new CoreException("", sqle, ERROR_HAS_CHILD, printItemSeqno);
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }

    public void updateSequencePrintItem(List listPrintItem, String schemeId, String loginId)
            throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();

                this.logger.log(ILogger.LEVEL_INFO, "Updating print item sequence, scheme "
                        + schemeId);
                for (int i = 0; i < listPrintItem.size(); i++) {
                    SchemePrintItemBean paramBean = (SchemePrintItemBean) listPrintItem.get(i);
                    this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), paramBean, loginId, "");
                    this.ibatisSqlMap.update(this.STRING_SQL + "updateSequence", paramBean);
                }

                this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", schemeId);

                this.logger.log(ILogger.LEVEL_INFO, "Updated print item sequence, scheme "
                        + schemeId);

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
    }
}
