package treemas.application.feature.form.printitem;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;

public class SchemePrintItemBean implements Serializable, Auditable {

    private static final String[] AUDIT_COLUMNS = new String[]{
            "schemeId", "printItemSeqno", "printItemTypeId",
            "printItemLabel", "lineSeqOrder", "questionGroupId", "questionId"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{
            "SCHEME_ID", "PRINT_ITEM_SEQNO", "PRINT_ITEM_TYPE_ID",
            "PRINT_ITEM_LABEL", "LINE_SEQ_ORDER", "QUESTION_GROUP_ID", "QUESTION_ID"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_FEATURE_PRINT_ITEM;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"schemeId", "printItemSeqno"};

    private String schemeId;
    private String schemeDescription;
    private Integer printItemSeqno;
    private String printItemTypeId;
    private String printItemTypeName;
    private String printItemLabel;
    private Integer lineSeqOrder;
    private String questionGroupId;
    private String questionGroupName;
    private String questionId;


    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public Integer getPrintItemSeqno() {
        return printItemSeqno;
    }

    public void setPrintItemSeqno(Integer printItemSeqno) {
        this.printItemSeqno = printItemSeqno;
    }

    public String getPrintItemTypeId() {
        return printItemTypeId;
    }

    public void setPrintItemTypeId(String printItemTypeId) {
        this.printItemTypeId = printItemTypeId;
    }

    public String getPrintItemLabel() {
        return printItemLabel;
    }

    public void setPrintItemLabel(String printItemLabel) {
        this.printItemLabel = printItemLabel;
    }

    public Integer getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(Integer lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getPrintItemTypeName() {
        return printItemTypeName;
    }

    public void setPrintItemTypeName(String printItemTypeName) {
        this.printItemTypeName = printItemTypeName;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

}
