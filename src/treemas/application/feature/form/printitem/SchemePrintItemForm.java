package treemas.application.feature.form.printitem;

import treemas.base.feature.FeatureFormBase;

import java.util.ArrayList;
import java.util.List;


public class SchemePrintItemForm extends FeatureFormBase {
    private String schemeId;
    private String schemeDescription;
    private String printItemSeqno;
    private String printItemTypeId;
    private String printItemTypeName;
    private String printItemLabel;
    private String lineSeqOrder;
    private String questionGroupId;
    private String questionGroupName;
    private String questionId;

    private List listPrintItem;

    public List getListPrintItem() {
        return listPrintItem;
    }

    public void setListPrintItem(List listPrintItem) {
        this.listPrintItem = listPrintItem;
    }

    public SchemePrintItemForm getData(int idx) {
        if (this.listPrintItem == null) {
            this.listPrintItem = new ArrayList(idx + 1);
        }
        while (this.listPrintItem.size() < (idx + 1)) {
            this.listPrintItem.add(new SchemePrintItemForm());
        }
        return (SchemePrintItemForm) this.listPrintItem.get(idx);
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getPrintItemSeqno() {
        return printItemSeqno;
    }

    public void setPrintItemSeqno(String printItemSeqno) {
        this.printItemSeqno = printItemSeqno;
    }

    public String getPrintItemTypeId() {
        return printItemTypeId;
    }

    public void setPrintItemTypeId(String printItemTypeId) {
        this.printItemTypeId = printItemTypeId;
    }

    public String getPrintItemLabel() {
        return printItemLabel;
    }

    public void setPrintItemLabel(String printItemLabel) {
        this.printItemLabel = printItemLabel;
    }

    public String getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(String lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getPrintItemTypeName() {
        return printItemTypeName;
    }

    public void setPrintItemTypeName(String printItemTypeName) {
        this.printItemTypeName = printItemTypeName;
    }
}