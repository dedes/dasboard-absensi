package treemas.application.feature.ajax.inquiry.heatmap;

import org.apache.commons.beanutils.BeanUtils;
import treemas.application.feature.inquiry.heatmap.SurveyHeatMapSearch;
import treemas.base.inquiry.InquiryManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;

import java.sql.SQLException;
import java.util.List;


public class SurveyHeatMapManager extends InquiryManagerBase {
    public static final int ERROR_NO_LOCATION_FOUND = 100;
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_INQ_HEAT_MAP;

    public List reloadDashboard(String branch, String scheme, String tglStart, String tglEnd, String radius) throws CoreException {
        SurveyHeatMapSearch searchParam = new SurveyHeatMapSearch();
        searchParam.setBranch(branch);
        searchParam.setScheme(scheme);
        searchParam.setTglStart(tglStart);
        searchParam.setTglEnd(tglEnd);
        searchParam.setRadius(radius);
        List result = null;

        try {
            result = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAll", searchParam);

            if (null == result || result.size() == 0) {
                this.logger.log(
                        ILogger.LEVEL_INFO, "No survey location found for parameter: "
                                + BeanUtils.describe(searchParam));
                throw new CoreException(ERROR_NO_LOCATION_FOUND);
            }
        } catch (CoreException me) {
            throw me;
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return result;
    }
}
