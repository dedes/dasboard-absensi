package treemas.application.feature.ajax.inquiry.personlocation;

import org.apache.commons.beanutils.BeanUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import treemas.application.feature.inquiry.personlocation.AllSurveyorLocationBean;
import treemas.base.inquiry.InquiryManagerBase;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.format.TreemasDateFormatter;
import treemas.util.format.TreemasDecimalFormatter;
import treemas.util.geofence.CellIdLookupManager;
import treemas.util.geofence.GeofenceBean;
import treemas.util.geofence.ReverseGeocode;
import treemas.util.http.HTTPResponseReader;
import treemas.util.log.ILogger;
import treemas.util.tool.ConfigurationProperties;
import treemas.util.tool.Tools;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class AllSurveyorLocationManager extends InquiryManagerBase {
    static final int ERROR_NO_SURVEYOR_FOUND = 100;
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_INQ_PERSON_LOCATION;
    private static final String FALSE = "0";

    private Map excConverterMap = new HashMap(3, 0.75f);

    public AllSurveyorLocationManager() {
        TreemasDateFormatter convertDMYTime = TreemasDateFormatter.getInstance();
        TreemasDecimalFormatter convertDecimal = TreemasDecimalFormatter.getInstance();

        this.excConverterMap.put("timestamp", convertDMYTime);
        this.excConverterMap.put("latitude", convertDecimal);
        this.excConverterMap.put("longitude", convertDecimal);
        this.excConverterMap.put("southBound", convertDecimal);
        this.excConverterMap.put("westBound", convertDecimal);
        this.excConverterMap.put("northBound", convertDecimal);
        this.excConverterMap.put("eastBound", convertDecimal);
    }

    public List reloadDashboard(String branchId, String job) {
        if (null == branchId) {
            this.logger.log(ILogger.LEVEL_DEBUG_LOW, "branchId is null...cannot reload dashboard data");
            return null;
        }
        List result = null;

        Map paramMap = new HashMap();
        paramMap.put("branchId", branchId);
        paramMap.put("job", job);

        try {
            List temp = this.ibatisSqlMap.queryForList(this.STRING_SQL
                    + "getAll", paramMap);

            if (temp == null || temp.size() == 0) {
                this.logger.log(ILogger.LEVEL_INFO,
                        "No surveyor location found for branchId: "
                                + BeanUtils.describe(branchId).toString());
                throw new CoreException(ERROR_NO_SURVEYOR_FOUND);
            }

            for (Iterator iterator = temp.iterator(); iterator.hasNext(); ) {
                AllSurveyorLocationBean bean = (AllSurveyorLocationBean) iterator.next();
                if (FALSE.equals(bean.getIsGps())
                        && FALSE.equals(bean.getFlag())) {
                    boolean missingCoordinate = (bean.getLatitude() == null && bean.getLongitude() == null) ? true : false;

                    if (missingCoordinate) {
                        this.getMissingCoordinate(bean);
                    }
                }
            }

            result = this.refineAllLocation(temp, branchId);

            if (temp == null || temp.size() == 0) {
                this.logger.log(ILogger.LEVEL_INFO,
                        "No surveyor location found for branchId: "
                                + BeanUtils.describe(branchId).toString());
                throw new CoreException(ERROR_NO_SURVEYOR_FOUND);
            }

            boolean needLookup = "1".equals(ConfigurationProperties.getInstance()
                    .get(PropertiesKey.MONITORING_LOOKUP_ADRRESS));

            WebContext wctx = WebContextFactory.get();
            HttpServletRequest request = (HttpServletRequest) wctx.getHttpServletRequest();
            String contextPath = request.getContextPath();

            if (needLookup)
                this.getAddressOfList(temp, contextPath);

            result = BeanConverter.convertBeansToStringMap(temp, this.excConverterMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
        }

        return result;
    }

    private void getAddressOfList(List list, String contextPath) {
        ReverseGeocode geocode = new ReverseGeocode(new HTTPResponseReader());
        String currentDate = this.getCurrentDate();

        for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
            AllSurveyorLocationBean bean = (AllSurveyorLocationBean) iterator.next();
            bean.setCtxPath(contextPath);
            bean.setSysdate(currentDate);

            if (bean.getLatitude() != null || bean.getLongitude() != null) {
                try {
                    String address = geocode.reverseGeocoding(
                            bean.getLatitude().doubleValue(),
                            bean.getLongitude().doubleValue());
                    bean.setAddress(address);
                } catch (CoreException e) {
                    this.logger.log(ILogger.LEVEL_WARNING, "Cannot get address (reverse geocode) for coordinate: "
                            + bean.getLatitude().doubleValue() + ","
                            + bean.getLongitude().doubleValue());
                }
            }
        }
    }

    private String getCurrentDate() {
        String currentDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat(treemas.util.constant.Global.FROM_STRING_DATE_FORMAT);
        currentDate = sdf.format(new java.util.Date());
        return currentDate;
    }

    private void getMissingCoordinate(AllSurveyorLocationBean bean)
            throws SQLException {
        CellIdLookupManager cidManager = new CellIdLookupManager();
        GeofenceBean coordinateBean = cidManager.geocodeByCellId(bean
                .getMCC().intValue(), bean.getMNC().intValue(), bean.getLAC()
                .intValue(), bean.getCellId().intValue());

        if (coordinateBean != null) {
            bean.setLatitude(new Double(coordinateBean.getLatitude()));
            bean.setLongitude(new Double(coordinateBean.getLongitude()));
            bean.setAccuracy(new Integer(coordinateBean.getAccuracy()));
            bean.setProvider(coordinateBean.getProvider());
            this.ibatisSqlMap
                    .update(this.STRING_SQL + "updateCoordinate", bean);
        } else {
            this.ibatisSqlMap.update(this.STRING_SQL + "updateFlag", bean);
        }
    }

    private List refineAllLocation(List allSurveyorLocation)
            throws SQLException {
        List listNoCoordinates = new ArrayList();
        List listWithCoordinates = new ArrayList();
        for (Iterator iterator = allSurveyorLocation.iterator(); iterator
                .hasNext(); ) {
            AllSurveyorLocationBean bean = (AllSurveyorLocationBean) iterator
                    .next();
            if (bean.getLatitude() == null || bean.getLongitude() == null) {
                listNoCoordinates.add(bean);
            } else {
                listWithCoordinates.add(bean);
            }
        }

        List listLastGpsLocation = this.getLastGpsLocation(listNoCoordinates);
        listNoCoordinates = null;

        listWithCoordinates.addAll(listLastGpsLocation);
        return listWithCoordinates;
    }

    private List refineAllLocation(List allSurveyorLocation, String branchId)
            throws SQLException {
        List listNoCoordinates = new ArrayList();
        List listWithCoordinates = new ArrayList();
        for (Iterator iterator = allSurveyorLocation.iterator(); iterator
                .hasNext(); ) {
            AllSurveyorLocationBean bean = (AllSurveyorLocationBean) iterator
                    .next();
            if (bean.getLatitude() == null || bean.getLongitude() == null) {
                listNoCoordinates.add(bean);
            } else {
                listWithCoordinates.add(bean);
            }
        }

        List listLastGpsLocation = this.getLastGpsLocation(listNoCoordinates,
                branchId);
        listNoCoordinates = null;

        listWithCoordinates.addAll(listLastGpsLocation);
        return listWithCoordinates;
    }

    private List getLastGpsLocation(List listNoCoordinates) throws SQLException {
        List result = null;

        String[] surveyorsId = new String[listNoCoordinates.size()];
        for (int i = 0; i < listNoCoordinates.size(); i++) {
            AllSurveyorLocationBean bean = (AllSurveyorLocationBean) listNoCoordinates.get(i);
            surveyorsId[i] = bean.getSurveyorId();
        }

        String allsurveyor = "('" + Tools.implode(surveyorsId, "','") + "')";
        result = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getLastGpsLocation", allsurveyor);

        return result;
    }

    private List getLastGpsLocation(List listNoCoordinates, String branchId)
            throws SQLException {
        List result = null;

        String[] surveyorsId = new String[listNoCoordinates.size()];
        for (int i = 0; i < listNoCoordinates.size(); i++) {
            AllSurveyorLocationBean bean = (AllSurveyorLocationBean) listNoCoordinates
                    .get(i);
            surveyorsId[i] = bean.getSurveyorId();
        }

        String allsurveyor = "('" + Tools.implode(surveyorsId, "','") + "')";

        Map paramMap = new HashMap();
        paramMap.put("allsurveyor", allsurveyor);
        paramMap.put("branchId", branchId);

        result = this.ibatisSqlMap.queryForList(this.STRING_SQL
                + "getLastGpsLocation", paramMap);

        return result;
    }

    private void getAddressOfList(List list) {
        ReverseGeocode geocode = new ReverseGeocode(new HTTPResponseReader());

        for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
            AllSurveyorLocationBean bean = (AllSurveyorLocationBean) iterator
                    .next();
            if (bean.getLatitude() != null || bean.getLongitude() != null) {
                try {
                    String address = geocode.reverseGeocoding(bean
                            .getLatitude().doubleValue(), bean.getLongitude()
                            .doubleValue());
                    bean.setAddress(address);
                } catch (CoreException me) {
                    this.logger.log(ILogger.LEVEL_WARNING,
                            "Cannot get address (reverse geocode) for coordinate: "
                                    + bean.getLatitude().doubleValue() + ","
                                    + bean.getLongitude().doubleValue());
                }
            }
        }
    }

    public String getFirstMultiBranchByUserId(String userId) {
        String branchId = null;
        try {
            branchId = (String) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getFirstMultiBranchByUserId", userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return branchId;
    }
}
