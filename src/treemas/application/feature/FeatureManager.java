package treemas.application.feature;

import treemas.base.feature.FeatureManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FeatureManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE;

    public String getGeneralSettingImagePath() throws SQLException {
        return this.getGeneralSettingValue("IMAGE_PATH");
    }

    public String getGeneralSettingDateFormat() throws SQLException {
        return this.getGeneralSettingValue("DATE_FMT");
    }

    public String getGeneralSettingTimeFormat() throws SQLException {
        return this.getGeneralSettingValue("TIME_FMT");
    }

    public String getGeneralSettingDateTimeFormat() throws SQLException {
        return this.getGeneralSettingValue("DATE_TIME");
    }

    private String getGeneralSettingValue(String id) throws SQLException {
        String tmp = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getGeneralSettingValue", id);
        return tmp;
    }

    public String getMonthName(String monthId) throws SQLException {
        String tmp = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getMonthName", monthId);
        return tmp;
    }

    public List getBranchLocation(String branchIdExc) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getBranchLocation", branchIdExc);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public String getBranchName(String branchId) throws CoreException {
        String branchName = null;
        try {
            branchName = (String) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getBranchName", branchId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

        return branchName;
    }

    public boolean isInChildLoginBranch(String loginBranch, Integer mobileAssignmentId) throws CoreException {
        boolean isInChild = true;

        Map paramMap = new HashMap();
        paramMap.put("branchId", loginBranch);
        paramMap.put("mobileAssignmentId", mobileAssignmentId);

        try {
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "isInChildLoginBranch", paramMap);

            if (count.intValue() == 0)
                isInChild = false;
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

        return isInChild;
    }

    public boolean isInDistributedTemplate(String userId, Integer mobileAssignmentId) throws CoreException {
        boolean isInDistributedTemplate = true;

        Map paramMap = new HashMap();
        paramMap.put("userId", userId);
        paramMap.put("mobileAssignmentId", mobileAssignmentId);

        try {
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "isInDistributedTemplate", paramMap);

            if (count.intValue() == 0)
                isInDistributedTemplate = false;
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

        return isInDistributedTemplate;
    }

    public String checkAssignmentNC(String mobileAssignmentId) throws CoreException {
        String result = "0";
        try {
            result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "checkAssignmentNC", mobileAssignmentId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }
}
