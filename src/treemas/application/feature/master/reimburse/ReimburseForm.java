package treemas.application.feature.master.reimburse;



import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class ReimburseForm extends FeatureFormBase {

	private String nik;
    private String nama;
    private String hari;
    private String tgl;
    private String lokasi;
    private String idProject;
    private String jamMasuk;
    private String jamKeluar;
    private String transport;
    private String uangMakan;
    private String keterangan;
    private String usrUpd;
    private String dtmUpd;
    
    private String isApproved;
    private String noteapp;
    private String usrCrt;
    private String dtmCrt;
    private String usrApp;
    private String dtmApp;
    private String uangData;
    private String image;
    private String totalJamKerja;
    
    private String tglAfter;
   

    private ReimburseSearch search = new ReimburseSearch();
    
    public ReimburseForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public ReimburseSearch getSearch() {
		return search;
	}

	public void setSearch(ReimburseSearch search) {
		this.search = search;
	}


	public ReimburseBean toBean() {
		ReimburseBean bean = new ReimburseBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getHari() {
		return hari;
	}

	public void setHari(String hari) {
		this.hari = hari;
	}

	public String getTgl() {
		return tgl;
	}

	public void setTgl(String tgl) {
		this.tgl = tgl;
	}

	public String getLokasi() {
		return lokasi;
	}

	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}


	public String getJamMasuk() {
		return jamMasuk;
	}

	public void setJamMasuk(String jamMasuk) {
		this.jamMasuk = jamMasuk;
	}

	public String getJamKeluar() {
		return jamKeluar;
	}

	public void setJamKeluar(String jamKeluar) {
		this.jamKeluar = jamKeluar;
	}

	public String getTransport() {
		return transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public String getUangMakan() {
		return uangMakan;
	}

	public void setUangMakan(String uangMakan) {
		this.uangMakan = uangMakan;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getUsrUpd() {
		return usrUpd;
	}

	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}

	public String getDtmUpd() {
		return dtmUpd;
	}

	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}

	
	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	public String getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(String isApproved) {
		this.isApproved = isApproved;
	}

	public String getNoteapp() {
		return noteapp;
	}

	public void setNoteapp(String noteapp) {
		this.noteapp = noteapp;
	}

	public String getUsrCrt() {
		return usrCrt;
	}

	public void setUsrCrt(String usrCrt) {
		this.usrCrt = usrCrt;
	}

	public String getDtmCrt() {
		return dtmCrt;
	}

	public void setDtmCrt(String dtmCrt) {
		this.dtmCrt = dtmCrt;
	}

	public String getUsrApp() {
		return usrApp;
	}

	public void setUsrApp(String usrApp) {
		this.usrApp = usrApp;
	}

	public String getDtmApp() {
		return dtmApp;
	}

	public void setDtmApp(String dtmApp) {
		this.dtmApp = dtmApp;
	}

	public String getUangData() {
		return uangData;
	}

	public void setUangData(String uangData) {
		this.uangData = uangData;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTotalJamKerja() {
		return totalJamKerja;
	}

	public void setTotalJamKerja(String totalJamKerja) {
		this.totalJamKerja = totalJamKerja;
	}

	public String getTglAfter() {
		return tglAfter;
	}

	public void setTglAfter(String tglAfter) {
		this.tglAfter = tglAfter;
	}

	
}
