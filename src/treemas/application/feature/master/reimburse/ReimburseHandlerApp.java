package treemas.application.feature.master.reimburse;

import com.google.common.base.Strings;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


import treemas.application.feature.form.scheme.SchemeFormBean;
import treemas.application.feature.master.absen.AbsenBean;
import treemas.application.feature.master.absen.AbsenForm;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.format.TreemasFormatter;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class ReimburseHandlerApp extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private ReimburseManagerApp manager = new ReimburseManagerApp();
    private ReimburseValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	ReimburseForm reimburseForm = (ReimburseForm) form;
    	reimburseForm.getPaging().calculationPage();
        String action = reimburseForm.getTask();
        String nik = reimburseForm.getNik();
        this.validator = new ReimburseValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListreimburse(request, reimburseForm);
       
        } 
        else if (Global.WEB_TASK_UPDATE.equals(action)) {
        	PagingInfo page = reimburseForm.getPaging();
            try {
            	this.approveReimburse(request, reimburseForm);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
            	request.setAttribute("message", resources.getMessage("common.confirmapprove"));
            	this.loadListreimburse(request, reimburseForm);
                page.setDispatch(null);
            }
        	
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = reimburseForm.getPaging();
            try {
            	this.rejectReimburse(request, reimburseForm);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
            	request.setAttribute("message", resources.getMessage("common.confirmreject"));
            	this.loadListreimburse(request, reimburseForm);
                page.setDispatch(null);
            }
        }
        return this.getNextPage(action, mapping);
    }

    private void loadListreimburse(HttpServletRequest request, ReimburseForm form)
    throws CoreException {
    	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        
        String before = form.getSearch().getTgl();
		String after = form.getSearch().getTglAfter();
		String message = "Date Before or After is Null or Empty";
		if (!(Strings.isNullOrEmpty(before)) && Strings.isNullOrEmpty(after)) {
			request.setAttribute("message", message);
		} else if ((Strings.isNullOrEmpty(before))
				&& (!Strings.isNullOrEmpty(after))) {
			request.setAttribute("message", message);
		}
		
        PageListWrapper result = this.manager.getAllReimburse(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listReimburse", list);
     
    }
    
//    private void loadHeaderDropDown(HttpServletRequest request) throws CoreException {
//        MultipleParameterBean bean = new MultipleParameterBean();
//        List list = this.multipleManager.getListHeader(bean);
//        request.setAttribute("listHeader", list);
//    }
    
    private void approveReimburse(HttpServletRequest request, ReimburseForm form) throws AppException {
    	ReimburseBean bean = this.convertToBean(form, this.getLoginId(request), 0);
        this.manager.approveReimburse(bean);
    }
    
    private void rejectReimburse(HttpServletRequest request, ReimburseForm form) throws AppException {
    	ReimburseBean bean = this.convertToBean(form, this.getLoginId(request), -1);
        this.manager.rejectReimburse(bean);
        
    }
   
    
    private ReimburseBean convertToBean(ReimburseForm form, String updateUser, int flag) throws AppException {
    	ReimburseBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			ReimburseForm reimburseForm = (ReimburseForm) form;
		try {
			this.loadListreimburse(request, reimburseForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((ReimburseForm) form).setTask(((ReimburseForm) form).resolvePreviousTask());
		
		String task = ((ReimburseForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
