package treemas.application.feature.master.reimburse;



import treemas.application.feature.form.scheme.SchemeFormBean;
import treemas.application.feature.master.karyawan.KaryawanBean;
import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.auditrail.AuditTrailException;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.databases.SQLErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.birt.report.model.api.core.DisposeEvent;


public class ReimburseManager extends FeatureManagerBase {
    static final int SEARCH_BY_QUESTION_OR_ANSWER_LABEL = 10;
    /*TODO
     * COMMIT THIS IBATIS*/
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_REIMBURSE;

    public PageListWrapper getAllReimburse(int pageNumber, ReimburseSearch search) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAll", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "cntAll", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }
    
 public List<ReimburseBean> getAllAuditReimburse(String nik, String nama, String dateBefore, String dateAfter) throws AppException {
	 ReimburseSearch search = new ReimburseSearch();
	 	search.setNik(nik);
		search.setNama(nama);
		search.setTgl(dateBefore);
		search.setTglAfter(dateAfter);
		
		List result = null;
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute query Get Download Reimburse");
			result = this.ibatisSqlMap.queryForList(STRING_SQL + "getDownloadReimburse", search);
	         this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "Execute query Get Download Reimburse Report : "+search);
			 this.logger.log(ILogger.LEVEL_INFO, "Execute query Get Download Reimburse Sukses");
		} catch (SQLException sqle) {
			this.logger.printStackTrace(sqle);
			throw new AppException(sqle, ERROR_DB);
		}
		return result;
	}
 
 public ReimburseBean getSingleReimburse(String nik) throws CoreException {
	 ReimburseBean result = null;
	 	 try {
	 		 result = (ReimburseBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "get", nik);
	      } catch (SQLException e) {
	          this.logger.printStackTrace(e);
	          throw new CoreException(ERROR_DB);
	      }

	      return result;
	  }
 	
}
