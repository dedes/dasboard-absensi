package treemas.application.feature.master.reimburse;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.struts.action.ActionMessages;

import com.google.common.base.Strings;


import treemas.application.general.multiple.MultipleManager;
import treemas.base.servlet.ServletBase;
import treemas.globalbean.LoginBean;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.log.ILogger;


public class ReportServletReimburse extends ServletBase {
private ReimburseManagerKaryawan manager = new ReimburseManagerKaryawan();

	
@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	doPost(req, resp);
}

@Override
protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	ReimburseForm frm = new ReimburseForm();
	String nik = this.getLoginId(req);
	String dateAfter = req.getParameter("dateAfter");
	String dateBefore = req.getParameter("dateBefore");
	
	HashMap hm = new HashMap();
        // Create an Exporter
	try {
		String filePath = getServletContext().getRealPath("/report/excel/TEMPLATE - Form Reimbursement.xls");
		FileInputStream fileIn = new FileInputStream(filePath);
		POIFSFileSystem myFileSystem = new POIFSFileSystem(fileIn);
		HSSFWorkbook workbook = new HSSFWorkbook(myFileSystem);
		
		HSSFSheet sheet = workbook.getSheetAt(0);	
		for (int j = 7; j <= 38; j++) {
        	HSSFRow rowsj = sheet.getRow(j);
        	rowsj.getCell(0).setCellValue("");
        	rowsj.getCell(1).setCellValue("");
        	rowsj.getCell(3).setCellValue("");
        	rowsj.getCell(4).setCellValue("");
        	rowsj.getCell(5).setCellValue("");
        	rowsj.getCell(6).setCellValue("");
        	rowsj.getCell(7).setCellValue("");
        	rowsj.getCell(8).setCellValue("");
        	rowsj.getCell(9).setCellValue("");
		}
		
		Date date = new Date();
    	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy") ;
    	String timeStamp = dateFormat.format(new Date());
    	HSSFRow rows2 = sheet.getRow(2);
    	
    	String nama = this.manager.getNama(nik);
    	String a = null;
		Cell ts = rows2.getCell(8);
    	if (timeStamp == null) {
    		ts.setCellValue(a);
    	}else {
    		ts.setCellValue(timeStamp);
    	}
    	
    	HSSFRow rows3 = sheet.getRow(3);
    	rows3.getCell(8).setCellValue(nama);
    		
    	HSSFRow rows4 = sheet.getRow(4);
    	rows4.getCell(8).setCellValue(nik);
		List<ReimburseBean> list = this.manager.getAllAuditReimburse(nik, dateBefore, dateAfter);
		if (list.size() != 0) {
			int i = 7;
	        for (ReimburseBean bean: list) {
	 			HSSFRow rows = sheet.getRow(i);
	        	rows.getCell(0).setCellValue(bean.getHari());
	        	String tg = bean.getTgl();
	        	if (tg == null) {
	        		rows.getCell(1).setCellValue(a);
	        	}else {
	        		rows.getCell(1).setCellValue(bean.getTgl());
	        	}
	        	
	        	String lk = bean.getLokasi();
	        	if (lk == null) {
	        		rows.getCell(3).setCellValue(a);
	        	}else {
	        		rows.getCell(3).setCellValue(bean.getLokasi());
	        	}   
	        	
	        	String idp = bean.getIdProject();
	        	if (idp == null){
	        		rows.getCell(4).setCellValue(a);
	        	}else if (idp.equals("BTPN") || idp.equals("UOB") || idp.equals("BOT") || idp.equals("BNI") || 
		        	idp.equals("DKI") || idp.equals("BTV") || idp.equals("AAF") || idp.equals("SHN") || 
		        	idp.equals("CTBC") || idp.equals("BTN") || idp.equals("JKT")){
		        	rows.getCell(4).setCellValue("Jakarta");
	        	}else if (idp.equals("PMT") || idp.equals("SIGMA") || idp.equals("PMA") || 
	        			idp.equals("RKM") || idp.equals("CIMB") || idp.equals("SIGMABCA") || idp.equals("TNG")){
	        		rows.getCell(4).setCellValue("Tangerang");
	        	} else {
	        		rows.getCell(4).setCellValue("Treemas");
	        	}
	        	
	        	String jm = bean.getJamMasuk();
	        	if (jm == null) {
	        		rows.getCell(5).setCellValue(a);
	        	}else {
	        		rows.getCell(5).setCellValue(bean.getJamMasuk());
	        	}   
	        	
	        	String jmk = bean.getJamKeluar();
	        	if (jmk == null) {
	        		rows.getCell(6).setCellValue(a);
	        	}else {
	        		rows.getCell(6).setCellValue(bean.getJamKeluar());
	        	}
	        	
	        	String transport = bean.getTransport();
	        	if (transport == null){
	        		rows.getCell(7).setCellValue(a);
	        	}else{
	        		rows.getCell(7).setCellValue(new Double(bean.getTransport()));
	        	}
	        	
	        	String uangmakan = bean.getUangMakan();
	        	if (uangmakan == null) {
	        		rows.getCell(8).setCellValue(a);
	        	}else{
	        		rows.getCell(8).setCellValue(new Double(bean.getUangMakan()));
	        	}
	        	
	        	if (idp == null){
	        		rows.getCell(9).setCellValue(a);
	        	}else{
	        		rows.getCell(9).setCellValue(bean.getIdProject());
	        	}
	        	
	        	i++;
			}
	        
	        HSSFRow rowSum = sheet.getRow(39);
	        ReimburseBean sumTransport  = this.manager.getSumTransport(nik, dateBefore, dateAfter);
	        rowSum.getCell(7).setCellValue(new Double(sumTransport.getSumTransport()));
	        rowSum.getCell(8).setCellValue(new Double(sumTransport.getSumUangMakan()));
	        String isLeader = this.manager.getLeader(nik);
	        if (isLeader.equals("1")){
	        	Integer voice  = this.manager.getDataLeade(nik);
	        	HSSFRow rowVoice = sheet.getRow(43);
		        rowVoice.getCell(9).setCellValue(new Double(voice));
		        HSSFRow rowst = sheet.getRow(50);
		        rowst.getCell(9).setCellValue(new Double(voice));
		        HSSFRow rowSub = sheet.getRow(52);
		       // Integer voice = 100000;
		        Integer total = sumTransport.getSumTransport() + sumTransport.getSumUangMakan() + voice;
		        rowSub.getCell(9).setCellValue(new Double(total));
	        }else{
	        	Integer voice  = this.manager.getData();
	        	HSSFRow rowVoice = sheet.getRow(43);
		        rowVoice.getCell(9).setCellValue(new Double(voice));
		        HSSFRow rowst = sheet.getRow(50);
		        rowst.getCell(9).setCellValue(new Double(voice));
		        HSSFRow rowSub = sheet.getRow(52);
		       // Integer voice = 100000;
		        Integer total = sumTransport.getSumTransport() + sumTransport.getSumUangMakan() + voice;
		        rowSub.getCell(9).setCellValue(new Double(total));
	        }
	    }	
		FileOutputStream fileOut = new FileOutputStream(filePath);
		workbook.write(fileOut);
		fileIn.close();	 
		fileOut.close();
		String name =nama.replace(' ','_');
	
		resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		resp.setHeader("Content-Disposition", "attachment; filename=FORM_REIMBURSE_"+name+"_"+nik+".xls");
		resp.setHeader("Cache-Control", "public");
		resp.setHeader("Pragma", "public");
		resp.setHeader("CookiesConfigureNoCache", "false");	
	
		OutputStream outputStream = resp.getOutputStream();
		workbook.write(outputStream);
		
		outputStream.flush();
		outputStream.close();
		this.logger.log(ILogger.LEVEL_INFO, "Your excel file has been generated!");
	
	} catch ( Exception ex ) {
		System.out.println(ex);
	}
  }
}     
