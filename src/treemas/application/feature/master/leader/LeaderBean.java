package treemas.application.feature.master.leader;

import java.io.Serializable;
import java.util.Date;

import treemas.base.ApplicationBean;
import treemas.util.auditrail.Auditable;

public class LeaderBean implements Serializable  {
	private String nik;
    private String idProject;
    private String usrUpd;
    private Date dtmUpd;
   
  
	
	
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getIdProject() {
		return idProject;
	}
	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}
	public String getUsrUpd() {
		return usrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}
	public Date getDtmUpd() {
		return dtmUpd;
	}
	public void setDtmUpd(Date dtmUpd) {
		this.dtmUpd = dtmUpd;
	}
	
}
