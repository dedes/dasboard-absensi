package treemas.application.feature.master.leader;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;


import treemas.application.feature.master.leader.LeaderBean;
import treemas.application.feature.master.leader.LeaderForm;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class LeaderHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private LeaderManager manager = new LeaderManager();
    LeaderValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	LeaderForm leaderForm = (LeaderForm) form;
    	leaderForm.getPaging().calculationPage();
        String action = leaderForm.getTask();
        this.validator = new LeaderValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListLeader(request, leaderForm);
           // industriForm.fillCurrentUri(request, SAVE_PARAMETERS);
            
        } else if (Global.WEB_TASK_ADD.equals(action)) {
        	leaderForm.setNik("");
        	leaderForm.setIdProject("");
//        	this.loadHeaderDropDown(request);
        	
        	
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.validator.validateLeaderInsert(new ActionMessages(), leaderForm, this.manager)) {
					this.insertLeader(request, leaderForm);
					this.loadListLeader(request, leaderForm);
            }	
            
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
        	LeaderBean bean = this.manager.getSingleLeader(leaderForm.getNik());
        	leaderForm.fromBean(bean);
//        	this.loadHeaderDropDown(request);
        	leaderForm.getPaging().setDispatch(null);

        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
            if (this.validator.validateLeaderEdit(new ActionMessages(), leaderForm)) {
            	this.updateLeader(request, leaderForm);
                this.loadListLeader(request, leaderForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
            }
        	
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = leaderForm.getPaging();
            try {
            	this.deleteLeader(request, leaderForm);
//                this.manager.deleteIndustri(industriForm.getKodeIndustri(),
//                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.loadListLeader(request, leaderForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListLeader(HttpServletRequest request, LeaderForm form)
    throws CoreException {
    	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAllLead(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listLeader", list);
     
    }
    
//    private void loadHeaderDropDown(HttpServletRequest request) throws CoreException {
//        MultipleParameterBean bean = new MultipleParameterBean();
//        List list = this.multipleManager.getListHeader(bean);
//        request.setAttribute("listHeader", list);
//    }
    
    private void insertLeader(HttpServletRequest request, LeaderForm form) throws AppException {
    	LeaderBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	
    	this.manager.insertLeader(bean);

    }
    
    private void updateLeader(HttpServletRequest request, LeaderForm form) throws AppException {
    	LeaderBean bean = this.convertToBean(form, this.getLoginId(request), 0);
        this.manager.updateLeader(bean);
    }
    
    private void deleteLeader(HttpServletRequest request, LeaderForm form) throws AppException {
    	LeaderBean bean = this.convertToBean(form, this.getLoginId(request), -1);
        this.manager.deleteLeader(bean);
        
    }
    
    private LeaderBean convertToBean(LeaderForm form, String updateUser, int flag) throws AppException {
    	LeaderBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			LeaderForm leaderForm = (LeaderForm) form;
		try {
			this.loadListLeader(request, leaderForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((LeaderForm) form).setTask(((LeaderForm) form).resolvePreviousTask());
		
		String task = ((LeaderForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
