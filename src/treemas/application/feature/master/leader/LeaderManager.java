package treemas.application.feature.master.leader;



import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.CoreException;
import treemas.util.auditrail.AuditTrailException;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.databases.SQLErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LeaderManager extends FeatureManagerBase {
    static final int SEARCH_BY_QUESTION_OR_ANSWER_LABEL = 10;
    /*TODO
     * COMMIT THIS IBATIS*/
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_LEADER;

    public PageListWrapper getAllLead(int pageNumber, LeaderSearch search) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAll", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "cntAll", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    
    public void insertLeader(LeaderBean leaderBean) throws CoreException {
	try	 {
		try {
			this.ibatisSqlMap.startTransaction();
			this.logger.log(ILogger.LEVEL_INFO, "Inserting Leader"
			       + leaderBean.getNik());
			
			this.ibatisSqlMap.insert(this.STRING_SQL + "insert",
					leaderBean);
			
			this.ibatisSqlMap.commitTransaction();
			
			this.logger.log(ILogger.LEVEL_INFO, "Inserted Leader"
			       + leaderBean.getNik());
		} finally {
			this.ibatisSqlMap.endTransaction();
		}
		} catch (SQLException sqle) {
			this.logger.printStackTrace(sqle);
			if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY || sqle.getErrorCode() == SQLErrorCode.ERROR_DUPLICATE_KEY) {
			throw new CoreException("", sqle, ERROR_DATA_EXISTS, leaderBean
			       .getNik());
		} else {
			throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
		}
		} catch (Exception e) {
			this.logger.printStackTrace(e);
			throw new CoreException(e, ERROR_UNKNOWN);
		}
	}
    
    
    
    public LeaderBean getSingleLeader(String nik) throws CoreException {
    	LeaderBean result = null;
    	System.out.println(nik);
    	 try {
             result = (LeaderBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "get", nik);
         } catch (SQLException e) {
             this.logger.printStackTrace(e);
             throw new CoreException(ERROR_DB);
         }

         return result;
     }
   
    public void updateLeader(LeaderBean bean) throws CoreException {
		try {
		try {
			this.ibatisSqlMap.startTransaction();
			this.logger.log(ILogger.LEVEL_INFO, "Updating Leader"
			       + bean.getNik());
			this.ibatisSqlMap.update(this.STRING_SQL + "update",
					bean);
			this.ibatisSqlMap.commitTransaction();
			
			this.logger.log(ILogger.LEVEL_INFO, "Updated Leader"
			       + bean.getNik());
		} finally {
			this.ibatisSqlMap.endTransaction();
		}
		} catch (SQLException se) {
			this.logger.printStackTrace(se);
			throw new CoreException(se, ERROR_DB);
		} catch (Exception e) {
			this.logger.printStackTrace(e);
			throw new CoreException(e, ERROR_UNKNOWN);
		}
    }
    
    public void deleteLeader(LeaderBean bean)
	    throws CoreException {
    	String nik = bean.getNik();
	
		try {
		    try {
		        this.ibatisSqlMap.startTransaction();
		        this.logger.log(ILogger.LEVEL_INFO, "Deleting Leader" + nik);
		        this.ibatisSqlMap.delete(this.STRING_SQL + "delete", bean);
		        this.ibatisSqlMap.commitTransaction();
		
		        this.logger.log(ILogger.LEVEL_INFO, "Deleted Leader" + nik);
		    } finally {
		        this.ibatisSqlMap.endTransaction();
		    }
		} catch (SQLException sqle) {
		    this.logger.printStackTrace(sqle);
		    if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND || sqle.getErrorCode() == SQLErrorCode.ERROR_CHILD_FOUND) {
		        throw new CoreException("", sqle, ERROR_HAS_CHILD, nik);
		
		    } else {
		        throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
		    }
		} catch (Exception e) {
		    this.logger.printStackTrace(e);
		    throw new CoreException(e, ERROR_UNKNOWN);
		}
	}
}
