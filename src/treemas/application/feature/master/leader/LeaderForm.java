package treemas.application.feature.master.leader;



import java.security.NoSuchAlgorithmException;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class LeaderForm extends FeatureFormBase {

	
	private String nik;
    private String idProject;
    private String usrUpd;
    private String dtmUpd;
 
   
    private LeaderSearch search = new LeaderSearch();
    
    public LeaderForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public LeaderSearch getSearch() {
		return search;
	}

	public void setSearch(LeaderSearch search) {
		this.search = search;
	}


	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	public String getUsrUpd() {
		return usrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}
	public String getDtmUpd() {
		return dtmUpd;
	}
	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}
	
	 public LeaderBean toBean() {
		 LeaderBean bean = new LeaderBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

	
}
