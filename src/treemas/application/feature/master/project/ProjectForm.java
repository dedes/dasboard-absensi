package treemas.application.feature.master.project;


import treemas.application.feature.master.leader.LeaderBean;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;

public class ProjectForm extends FeatureFormBase {

	private static final long serialVersionUID = 1L;
 	private String idProject;
    private String namaProject;
    private String lokasi;
    private String noTlpn;
    private String biayaReimburse;
    private String gpsLatitude;
    private String gpsLongitude;
    private String usrupd;
    private String dtmupd;
    private String jarak;
    
    private ProjectSearch search = new ProjectSearch();
    
    public ProjectForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public ProjectSearch getSearch() {
		return search;
	}

	public void setSearch(ProjectSearch search) {
		this.search = search;
	}

	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	public String getNamaProject() {
		return namaProject;
	}

	public void setNamaProject(String namaProject) {
		this.namaProject = namaProject;
	}

	public String getLokasi() {
		return lokasi;
	}

	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}

	public String getNoTlpn() {
		return noTlpn;
	}

	public void setNoTlpn(String noTlpn) {
		this.noTlpn = noTlpn;
	}

	public String getBiayaReimburse() {
		return biayaReimburse;
	}

	public void setBiayaReimburse(String biayaReimburse) {
		this.biayaReimburse = biayaReimburse;
	}

	public String getGpsLatitude() {
		return gpsLatitude;
	}

	public void setGpsLatitude(String gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}

	public String getGpsLongitude() {
		return gpsLongitude;
	}

	public void setGpsLongitude(String gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}

	public String getUsrupd() {
		return usrupd;
	}

	public void setUsrupd(String usrupd) {
		this.usrupd = usrupd;
	}

	public String getDtmupd() {
		return dtmupd;
	}

	public void setDtmupd(String dtmupd) {
		this.dtmupd = dtmupd;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getJarak() {
		return jarak;
	}

	public void setJarak(String jarak) {
		this.jarak = jarak;
	}

	public ProjectBean toBean() {
		ProjectBean bean = new ProjectBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

  
	
}
