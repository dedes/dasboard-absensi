package treemas.application.feature.master.project;
import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;


import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/*TODO
 * COMMIT THIS*/
public class ProjectValidator extends Validator {
    public ProjectValidator(HttpServletRequest request) {
        super(request);
    }

    public ProjectValidator(Locale locale) {
        super(locale);
    }
 
    public boolean validateProjectInsert(ActionMessages messages, ProjectForm form, ProjectManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        
        try {
        	ProjectBean bean = manager.getSingleProject(form.getIdProject());
            if (null != bean) {
            	messages.add("idProject", new ActionMessage("errors.duplicate", "idProject" + form.getIdProject()));
            }
            
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
        
        this.textValidator(messages, form.getIdProject(), "common.id.project", "idProject", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
    	this.textValidator(messages, form.getNamaProject(), "common.nama", "nama", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getBiayaReimburse(), "common.biaya", "biayaReimburse", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getGpsLatitude(), "common.gpslatitude", "gpsLatitude", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_LATITUDE);
        this.textValidator(messages, form.getGpsLongitude(), "common.gpslongitude", "gpsLongitude", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_LONGITUDE);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
   
    public boolean validateProjectEdit(ActionMessages messages, ProjectForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages(); 
        
        this.textValidator(messages, form.getNamaProject(), "common.nama", "nama", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getBiayaReimburse(), "common.biaya", "biayaReimburse", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getGpsLatitude(), "common.gpslatitude", "gpsLatitude", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_LATITUDE);
        this.textValidator(messages, form.getGpsLongitude(), "common.gpslongitude", "gpsLongitude", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_LONGITUDE);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }      
    }


