package treemas.application.feature.master.project;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;


import treemas.application.feature.master.leader.LeaderBean;
import treemas.application.feature.master.leader.LeaderForm;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class ProjectHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private ProjectManager manager = new ProjectManager();
    ProjectValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	ProjectForm projectForm = (ProjectForm) form;
    	projectForm.getPaging().calculationPage();
        String action = projectForm.getTask();
        this.validator = new ProjectValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListProject(request, projectForm);
           // industriForm.fillCurrentUri(request, SAVE_PARAMETERS);
            
        } else if (Global.WEB_TASK_ADD.equals(action)) {
        	projectForm.setIdProject("");
        	projectForm.setNamaProject("");
//        	this.loadHeaderDropDown(request);
        	
        	
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.validator.validateProjectInsert(new ActionMessages(), projectForm, this.manager)) {
					this.insertProject(request, projectForm);
					this.loadListProject(request, projectForm);
            }	
            
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
        	ProjectBean bean = this.manager.getSingleProject(projectForm.getIdProject());
        	projectForm.fromBean(bean);
//        	this.loadHeaderDropDown(request);
        	projectForm.getPaging().setDispatch(null);

        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
            if (this.validator.validateProjectEdit(new ActionMessages(), projectForm)) {
            	this.updateProject(request, projectForm);
                this.loadListProject(request, projectForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
            }
        	
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = projectForm.getPaging();
            try {
            	this.deleteProject(request, projectForm);
//                this.manager.deleteIndustri(industriForm.getKodeIndustri(),
//                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.loadListProject(request, projectForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListProject(HttpServletRequest request, ProjectForm form)
    throws CoreException {
    	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAllProject(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listProject", list);
     
    }
    
//    private void loadHeaderDropDown(HttpServletRequest request) throws CoreException {
//        MultipleParameterBean bean = new MultipleParameterBean();
//        List list = this.multipleManager.getListHeader(bean);
//        request.setAttribute("listHeader", list);
//    }
    
    private void insertProject(HttpServletRequest request, ProjectForm form) throws AppException {
    	ProjectBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	this.manager.insertProject(bean);

    }
    
    private void updateProject(HttpServletRequest request, ProjectForm form) throws AppException {
    	ProjectBean bean = this.convertToBean(form, this.getLoginId(request), 0);
        this.manager.updateProject(bean);
    }
    
    private void deleteProject(HttpServletRequest request, ProjectForm form) throws AppException {
    	ProjectBean bean = this.convertToBean(form, this.getLoginId(request), -1);
        this.manager.deleteProject(bean);
        
    }
    
    private ProjectBean convertToBean(ProjectForm form, String updateUser, int flag) throws AppException {
    	ProjectBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			ProjectForm projectForm = (ProjectForm) form;
		try {
			this.loadListProject(request, projectForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((ProjectForm) form).setTask(((ProjectForm) form).resolvePreviousTask());
		
		String task = ((ProjectForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
