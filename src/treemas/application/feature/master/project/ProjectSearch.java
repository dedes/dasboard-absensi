package treemas.application.feature.master.project;

import java.util.Date;

import treemas.base.feature.FeatureFormBase;
import treemas.base.search.SearchForm;

public class ProjectSearch extends SearchForm {

	private static final long serialVersionUID = 1L;
 	private String idProject;
    private String namaProject;
    private String lokasi;
    private String noTlpn;
    private Integer biayaReimburse;
    private String gpsLatitude;
    private String gpsLongitude;
    private String usrupd;
    private Date dtmupd;
	public String getIdProject() {
		return idProject;
	}
	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}
	public String getNamaProject() {
		return namaProject;
	}
	public void setNamaProject(String namaProject) {
		this.namaProject = namaProject;
	}
	public String getLokasi() {
		return lokasi;
	}
	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}
	public String getNoTlpn() {
		return noTlpn;
	}
	public void setNoTlpn(String noTlpn) {
		this.noTlpn = noTlpn;
	}
	public Integer getBiayaReimburse() {
		return biayaReimburse;
	}
	public void setBiayaReimburse(Integer biayaReimburse) {
		this.biayaReimburse = biayaReimburse;
	}
	public String getGpsLatitude() {
		return gpsLatitude;
	}
	public void setGpsLatitude(String gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}
	public String getGpsLongitude() {
		return gpsLongitude;
	}
	public void setGpsLongitude(String gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}
	public String getUsrupd() {
		return usrupd;
	}
	public void setUsrupd(String usrupd) {
		this.usrupd = usrupd;
	}
	public Date getDtmupd() {
		return dtmupd;
	}
	public void setDtmupd(Date dtmupd) {
		this.dtmupd = dtmupd;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    
   

}
