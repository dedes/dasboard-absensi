package treemas.application.feature.master.project;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import treemas.application.feature.form.scheme.SchemeFormBean;
import treemas.application.feature.master.leader.LeaderBean;
import treemas.application.feature.question.question.QuestionBean;
import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.handset.login.LoginBean;
import treemas.handset.submitCuti.SubmitCuti;
import treemas.util.CoreException;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.databases.SQLErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.ConfigurationProperties;

public class ProjectManager extends FeatureManagerBase {

	private static final String STRING_SQL = SQLConstant.SQL_FEATURE_PROJECT;
	
	public PageListWrapper getAllProject(int pageNumber, ProjectSearch projectSearch) throws CoreException {
		/*PageListWrapper result = new PageListWrapper();
		SearchFormParameter param = new SearchFormParameter();
		param.setPage(numberPage);*/
		
		PageListWrapper result = new PageListWrapper();
		projectSearch.setPage(pageNumber);

		try {
			 List list = this.ibatisSqlMap.queryForList(
			 this.STRING_SQL + "getAll", projectSearch);
			 result.setResultList(list);

			 Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
					 this.STRING_SQL + "cntAll", projectSearch);
			 result.setTotalRecord(tmp.intValue());
		} catch (SQLException sqle) {
			this.logger.printStackTrace(sqle);
			throw new CoreException(sqle, ERROR_DB);
		}
		return result;
	}
	
	
	 public void deleteProject(ProjectBean bean)
	    throws CoreException {
 	String idProject = bean.getIdProject();
	
		try {
		    try {
		        this.ibatisSqlMap.startTransaction();
		        this.logger.log(ILogger.LEVEL_INFO, "Deleting Project" + idProject);
		        this.ibatisSqlMap.delete(this.STRING_SQL + "delete", bean);
		        this.ibatisSqlMap.commitTransaction();
		
		        this.logger.log(ILogger.LEVEL_INFO, "Deleted Project" + idProject);
		    } finally {
		        this.ibatisSqlMap.endTransaction();
		    }
		} catch (SQLException sqle) {
		    this.logger.printStackTrace(sqle);
		    if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND || sqle.getErrorCode() == SQLErrorCode.ERROR_CHILD_FOUND) {
		        throw new CoreException("", sqle, ERROR_HAS_CHILD, idProject);
		
		    } else {
		        throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
		    }
		} catch (Exception e) {
		    this.logger.printStackTrace(e);
		    throw new CoreException(e, ERROR_UNKNOWN);
		}
	}
	
    public void insertProject(ProjectBean projectBean) throws CoreException {
    	try	 {
    		try {
    			this.ibatisSqlMap.startTransaction();
    			this.logger.log(ILogger.LEVEL_INFO, "Inserting Leader" + projectBean.getIdProject());
    			this.ibatisSqlMap.insert(this.STRING_SQL + "insert", projectBean);
    			
    			List<ProjectBean> list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getListNik");
    			for (ProjectBean sab : list) {
    				Map paramMap = new HashMap();
    				paramMap.put("nik", sab.getNik());
    				paramMap.put("idProject", projectBean.getIdProject());
    				paramMap.put("isActive", "0");;
    				 this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "INSERT DATA: " + sab.getNik());
    				 this.ibatisSqlMap.insert( this.STRING_SQL + "insertPenempatan", paramMap);
    			}
    			this.ibatisSqlMap.commitTransaction();
    			this.logger.log(ILogger.LEVEL_INFO, "Inserted Leader" + projectBean.getIdProject());
    		} finally {
    			this.ibatisSqlMap.endTransaction();
    		}
    		} catch (SQLException sqle) {
    			this.logger.printStackTrace(sqle);
    			if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY || sqle.getErrorCode() == SQLErrorCode.ERROR_DUPLICATE_KEY) {
    			throw new CoreException("", sqle, ERROR_DATA_EXISTS, projectBean
    			       .getIdProject());
    		} else {
    			throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
    		}
    		} catch (Exception e) {
    			this.logger.printStackTrace(e);
    			throw new CoreException(e, ERROR_UNKNOWN);
    		}
    }

    public ProjectBean getSingleProject(String idProject) throws CoreException {
        ProjectBean result = null;
        try {
         ProjectBean param = new ProjectBean();
            param.setIdProject(idProject);
            result = (ProjectBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "get", param);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }
    
   
    public void updateProject(ProjectBean projectBean) throws CoreException {
			try {
				try {
				this.ibatisSqlMap.startTransaction();
				this.logger.log(ILogger.LEVEL_INFO, "Updating Project "
				     + projectBean.getIdProject() + " / Project Id ");
				/*this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), projectBean, loginId, screenId);*/
				this.ibatisSqlMap.update(this.STRING_SQL + "update", projectBean);
				/*this.ibatisSqlMap.update(this.STRING_SQL + "updateSchemeLastUpdate", questionBean.getQuestionGroupId());*/
				this.ibatisSqlMap.commitTransaction();

				this.logger.log(ILogger.LEVEL_INFO, "Updated Project "
						+ projectBean.getIdProject() + " / Project Id ");
				this.logger.log(ILogger.LEVEL_INFO, "Updated Scheme Last Update for Project Id: " +
						projectBean.getIdProject() + "/ to: " + new java.util.Date().toString());
			} finally {
				this.ibatisSqlMap.endTransaction();
			}
		} catch (SQLException se) {
			this.logger.printStackTrace(se);
			if (se.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
			/*throw new CoreException("", se, ERROR_DATA_EXISTS, projectBean.getParamName());*/
		} else {
			throw new CoreException(se.toString(), se, ERROR_DB, null);
		}
	} catch (Exception e) {
		this.logger.printStackTrace(e);
		throw new CoreException(e, ERROR_UNKNOWN);
	}
}
    
}
    
    
	

