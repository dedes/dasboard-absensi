package treemas.application.feature.master.timesheet;

import com.google.common.base.Strings;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


import treemas.application.feature.form.scheme.SchemeFormBean;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.format.TreemasFormatter;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class TimesheetHandlerKaryawan extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private TimesheetManagerKaryawan manager = new TimesheetManagerKaryawan();
    private TimesheetValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	TimesheetForm timesheetForm = (TimesheetForm) form;
    	timesheetForm.getPaging().calculationPage();
        String action = timesheetForm.getTask();
        this.validator = new TimesheetValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
        		this.loadListTimesheet(request, timesheetForm);
        	
        } 
        return this.getNextPage(action, mapping);
    }

    private void loadListTimesheet(HttpServletRequest request, TimesheetForm form)
    throws CoreException {
    	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        
        PageListWrapper result = this.manager.getAllTimesheet(pageNumber, form.getSearch(),this.getLoginId(request));
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listTimesheet", list);
     
    }
    
//    private void loadHeaderDropDown(HttpServletRequest request) throws CoreException {
//        MultipleParameterBean bean = new MultipleParameterBean();
//        List list = this.multipleManager.getListHeader(bean);
//        request.setAttribute("listHeader", list);
//    }
    
 
   
    
    private TimesheetBean convertToBean(TimesheetForm form, String updateUser, int flag) throws AppException {
    	TimesheetBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			TimesheetForm timesheetForm = (TimesheetForm) form;
		try {
			this.loadListTimesheet(request, timesheetForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((TimesheetForm) form).setTask(((TimesheetForm) form).resolvePreviousTask());
		
		String task = ((TimesheetForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
