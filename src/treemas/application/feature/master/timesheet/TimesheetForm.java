package treemas.application.feature.master.timesheet;



import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class TimesheetForm extends FeatureFormBase {

	private String nik;
    private String nama;
    private String leader;
    private String hari;
    private String tgl;
    private String idProject;
    private String note;
    private String jamMasuk;
    private String jamKeluar;
    private String overtime;
    private String totalJamKerja;
    
    private String tglAfter;
   

    private TimesheetSearch search = new TimesheetSearch();
    
    public TimesheetForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public TimesheetSearch getSearch() {
		return search;
	}

	public void setSearch(TimesheetSearch search) {
		this.search = search;
	}


	public TimesheetBean toBean() {
		TimesheetBean bean = new TimesheetBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getLeader() {
		return leader;
	}

	public void setLeader(String leader) {
		this.leader = leader;
	}

	public String getHari() {
		return hari;
	}

	public void setHari(String hari) {
		this.hari = hari;
	}

	public String getTgl() {
		return tgl;
	}

	public void setTgl(String tgl) {
		this.tgl = tgl;
	}

	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	

	public String getJamMasuk() {
		return jamMasuk;
	}

	public void setJamMasuk(String jamMasuk) {
		this.jamMasuk = jamMasuk;
	}

	public String getJamKeluar() {
		return jamKeluar;
	}

	public void setJamKeluar(String jamKeluar) {
		this.jamKeluar = jamKeluar;
	}

	public String getOvertime() {
		return overtime;
	}

	public void setOvertime(String overtime) {
		this.overtime = overtime;
	}

	

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getTotalJamKerja() {
		return totalJamKerja;
	}

	public void setTotalJamKerja(String totalJamKerja) {
		this.totalJamKerja = totalJamKerja;
	}

	public String getTglAfter() {
		return tglAfter;
	}

	public void setTglAfter(String tglAfter) {
		this.tglAfter = tglAfter;
	}

	
}
