package treemas.application.feature.master.timesheet;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.struts.action.ActionMessages;

import com.google.common.base.Strings;

import treemas.application.feature.master.karyawan.KaryawanForm;
import treemas.application.feature.master.karyawan.KaryawanValidator;
import treemas.application.general.multiple.MultipleManager;
import treemas.base.servlet.ServletBase;
import treemas.globalbean.LoginBean;
import treemas.util.CoreException;
import treemas.util.log.ILogger;


public class ReportServletTimesheet extends ServletBase {
private TimesheetManagerKaryawan manager = new TimesheetManagerKaryawan();

	
@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	doPost(req, resp);
}

@Override
protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	TimesheetForm frm = new TimesheetForm();
	String nik = this.getLoginId(req);
	String dateAfter = req.getParameter("dateAfter");
	String dateBefore = req.getParameter("dateBefore");
	Date date = new Date();
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy") ;
	String timeStamp = dateFormat.format(new Date());
	
	HashMap hm = new HashMap();
        // Create an Exporter
	try {
		String filePath = getServletContext().getRealPath("/report/excel/TEMPLATE - Time Sheet.xls");
		FileInputStream fileIn = new FileInputStream(filePath);
		POIFSFileSystem myFileSystem = new POIFSFileSystem(fileIn);
		HSSFWorkbook workbook = new HSSFWorkbook(myFileSystem);
		HSSFSheet sheet = workbook.getSheetAt(0);	
		for (int j = 12; j <= 51; j++) {
	    	HSSFRow rowsj = sheet.getRow(j);
	    	rowsj.getCell(1).setCellValue("");
	    	rowsj.getCell(2).setCellValue("");
	    	rowsj.getCell(3).setCellValue("");
	    	rowsj.getCell(4).setCellValue("");
	    	rowsj.getCell(5).setCellValue("");
	    	rowsj.getCell(6).setCellValue("");
	    	rowsj.getCell(7).setCellValue("");
	    	rowsj.getCell(8).setCellValue("");
	    	}
		TimesheetBean bn = this.manager.getNama(nik);
    	String a = null;
    	String nama = bn.getNama();
    	HSSFRow rows4 = sheet.getRow(4);
    	rows4.getCell(5).setCellValue(nama);
    	
    	HSSFRow rows5 = sheet.getRow(5);
    	rows5.getCell(5).setCellValue(nik);
    	
//			        	HSSFRow rows6 = sheet.getRow(6);
//			        	rows6.getCell(5).setCellValue(bean.getLeader());
    	
    	HSSFRow rows7 = sheet.getRow(7);
    	rows7.getCell(5).setCellValue(bn.getNoHP());
    	
    	HSSFRow rows8 = sheet.getRow(8);
    	rows8.getCell(5).setCellValue(bn.getEmail());
		
		List<TimesheetBean> list = this.manager.getAllAuditTimesheet(nik, dateBefore, dateAfter);
		if (list.size() != 0) {
			int i = 12;
	        for (TimesheetBean bean: list) {
	        	HSSFRow rows = sheet.getRow(i);
	        	String hr = bean.getHari();
	        	if (hr == null) {
	        		rows.getCell(1).setCellValue(a);
	        	}else {
	        		rows.getCell(1).setCellValue(bean.getHari());
	        	}
	        	
	        	String tg = bean.getTgl();
	        	if (tg == null) {
	        		rows.getCell(2).setCellValue(a);
	        	}else {
	        		rows.getCell(2).setCellValue(bean.getTgl());
	        	}
	        	
	        	String idp = bean.getIdProject();
	        	if (idp == null) {
	        		rows.getCell(3).setCellValue(a);
	        	}else {
	        		rows.getCell(3).setCellValue(bean.getIdProject());
	        	}
	        	
	        	String nt = bean.getNote();
	        	if (nt == null) {
	        		rows.getCell(4).setCellValue(a);
	        	}else {
	        		rows.getCell(4).setCellValue(bean.getNote());
	        	}
	        	
	        	String jm = bean.getJamMasuk();
	        	if (jm == null) {
	        		rows.getCell(5).setCellValue(a);
	        	}else {
	        		rows.getCell(5).setCellValue(bean.getJamMasuk());
	        	}
	        	
	        	String jmk = bean.getJamKeluar();
	        	if (jmk == null) {
	        		rows.getCell(6).setCellValue(a);
	        	}else {
	        		rows.getCell(6).setCellValue(bean.getJamKeluar());
	        	}
	        	
	        	String tjk = bean.getTotalJamKerja();
	        	if (tjk == null) {
	        		rows.getCell(7).setCellValue(a);
	        	}else {
	        		rows.getCell(7).setCellValue(bean.getTotalJamKerja());
	        	}
	        	
	        	String ot = bean.getOvertime();
	        	if (ot == null) {
	        		rows.getCell(8).setCellValue(a);
	        	}else {
	        		rows.getCell(8).setCellValue(bean.getOvertime());
	        	}
	        	
	        	i++;
	        	
	        }
		}
		fileIn.close();	 
		
		FileOutputStream fileOut = new FileOutputStream(filePath);
		workbook.write(fileOut);
		fileOut.close();
		
		resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		resp.setHeader("Content-Disposition", "attachment; filename=FORM_TIMESHEET_"+nama+".xls");
		resp.setHeader("Cache-Control", "public");
		resp.setHeader("Pragma", "public");
		resp.setHeader("CookiesConfigureNoCache", "true");	
	
		OutputStream outputStream = resp.getOutputStream();
		workbook.write(outputStream);
		
		outputStream.flush();
		outputStream.close();
		this.logger.log(ILogger.LEVEL_INFO, "Your excel file has been generated!");

	} catch ( Exception ex ) {
		System.out.println(ex);
	}
  }
}
	


        	
        
        

