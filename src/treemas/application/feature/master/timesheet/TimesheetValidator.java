package treemas.application.feature.master.timesheet;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import treemas.application.feature.master.inputabsen.InputAbsenForm;
import treemas.application.feature.master.karyawan.KaryawanBean;
import treemas.application.feature.master.karyawan.KaryawanForm;
import treemas.application.feature.master.karyawan.KaryawanManager;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;


import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/*TODO
 * COMMIT THIS*/
public class TimesheetValidator extends Validator {
    public TimesheetValidator(HttpServletRequest request) {
        super(request);
    }

    public TimesheetValidator(Locale locale) {
        super(locale);
    }
    
    public boolean validateTglTimesheet(ActionMessages messages, TimesheetForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages(); 
        
        this.textValidator(messages, form.getSearch().getTgl(), "common.tanggal", "search.tgl", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getSearch().getTglAfter(), "common.tanggal", "search.tglAfter", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }  
    
   }


