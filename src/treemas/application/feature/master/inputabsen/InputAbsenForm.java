package treemas.application.feature.master.inputabsen;



import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class InputAbsenForm extends FeatureFormBase {
	  private String nik;
	  private String tglAbsen;
	  private String jamMasuk;
	  private String jamPulang;
	  private String gpsLatitudeMsk;
	  private String gpsLongitudeMsk;
	  private String gpsLatitudePlg;
	  private String gpsLongitudePlg;
	  private String lokasiMsk;
	  private String lokasiPlg;
	  private String isLembur;	
	  private String note;
	  private String jarakMsk;
	  private String jarakPlg;
	  private String totalJamKerja;
	  private String flgabs;
	  private String noteapp;
	  private String isAbsen;
	  private String hariAbsen;
	  private String tglmasuk;
	  private String idProject;
	  private String alasan;
	  private String isLibur;
	  private String nikLeader;
	  private String timesheet;
	
   
    private InputAbsenSearch search = new InputAbsenSearch();
    
    public InputAbsenForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public InputAbsenSearch getSearch() {
		return search;
	}

	public void setSearch(InputAbsenSearch search) {
		this.search = search;
	}

	public InputAbsenBean toBean() {
		InputAbsenBean bean = new InputAbsenBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getTglAbsen() {
		return tglAbsen;
	}

	public void setTglAbsen(String tglAbsen) {
		this.tglAbsen = tglAbsen;
	}

	public String getJamMasuk() {
		return jamMasuk;
	}

	public void setJamMasuk(String jamMasuk) {
		this.jamMasuk = jamMasuk;
	}

	public String getJamPulang() {
		return jamPulang;
	}

	public void setJamPulang(String jamPulang) {
		this.jamPulang = jamPulang;
	}

	
	public String getIsLembur() {
		return isLembur;
	}

	public void setIsLembur(String isLembur) {
		this.isLembur = isLembur;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getJarakMsk() {
		return jarakMsk;
	}

	public void setJarakMsk(String jarakMsk) {
		this.jarakMsk = jarakMsk;
	}

	public String getJarakPlg() {
		return jarakPlg;
	}

	public void setJarakPlg(String jarakPlg) {
		this.jarakPlg = jarakPlg;
	}

	

	public String getGpsLatitudeMsk() {
		return gpsLatitudeMsk;
	}

	public void setGpsLatitudeMsk(String gpsLatitudeMsk) {
		this.gpsLatitudeMsk = gpsLatitudeMsk;
	}

	public String getGpsLongitudeMsk() {
		return gpsLongitudeMsk;
	}

	public void setGpsLongitudeMsk(String gpsLongitudeMsk) {
		this.gpsLongitudeMsk = gpsLongitudeMsk;
	}

	public String getGpsLatitudePlg() {
		return gpsLatitudePlg;
	}

	public void setGpsLatitudePlg(String gpsLatitudePlg) {
		this.gpsLatitudePlg = gpsLatitudePlg;
	}

	public String getGpsLongitudePlg() {
		return gpsLongitudePlg;
	}

	public void setGpsLongitudePlg(String gpsLongitudePlg) {
		this.gpsLongitudePlg = gpsLongitudePlg;
	}

	public String getTotalJamKerja() {
		return totalJamKerja;
	}

	public void setTotalJamKerja(String totalJamKerja) {
		this.totalJamKerja = totalJamKerja;
	}

	public String getFlgabs() {
		return flgabs;
	}

	public void setFlgabs(String flgabs) {
		this.flgabs = flgabs;
	}

	public String getIsAbsen() {
		return isAbsen;
	}

	public void setIsAbsen(String isAbsen) {
		this.isAbsen = isAbsen;
	}

	

	public String getHariAbsen() {
		return hariAbsen;
	}

	public void setHariAbsen(String hariAbsen) {
		this.hariAbsen = hariAbsen;
	}

	

	public String getIsLibur() {
		return isLibur;
	}

	public void setIsLibur(String isLibur) {
		this.isLibur = isLibur;
	}

	public String getNikLeader() {
		return nikLeader;
	}

	public void setNikLeader(String nikLeader) {
		this.nikLeader = nikLeader;
	}

	

	public String getLokasiMsk() {
		return lokasiMsk;
	}

	public void setLokasiMsk(String lokasiMsk) {
		this.lokasiMsk = lokasiMsk;
	}

	public String getLokasiPlg() {
		return lokasiPlg;
	}

	public void setLokasiPlg(String lokasiPlg) {
		this.lokasiPlg = lokasiPlg;
	}

	public String getTglmasuk() {
		return tglmasuk;
	}

	public void setTglmasuk(String tglmasuk) {
		this.tglmasuk = tglmasuk;
	}

	public String getAlasan() {
		return alasan;
	}

	public void setAlasan(String alasan) {
		this.alasan = alasan;
	}

	public String getIdProject() {
		return idProject;
	}
	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	public String getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(String timesheet) {
		this.timesheet = timesheet;
	}

	public String getNoteapp() {
		return noteapp;
	}

	public void setNoteapp(String noteapp) {
		this.noteapp = noteapp;
	}
	
	
}
