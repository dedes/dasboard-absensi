package treemas.application.feature.master.inputabsen;

import treemas.base.search.SearchForm;

public class InputAbsenSearch extends SearchForm {
	  private String nik;
	  private String tglAbsen;
	  private String jamMasuk;
	  private String jamPulang;
	  private String gpsLatitudeMsk;
	  private String gpsLongitudeMsk;
	  private String gpsLatitudePlg;
	  private String gpsLongitudePlg;
	  private String lokasiMsk;
	  private String lokasiPlg;
	  private String isLembur;	
	  private String note;
	  private String jarakMsk;
	  private String jarakPlg;
	  private String totalJamKerja;
	  private String flgabs;
	  private String noteapp;
	  private String isAbsen;
	  private String hariAbsen;
	  private String tglmasuk;
	  private String idProject;
	  private String alasan;
	  private String isLibur;
	  private String nikLeader;
	  private String timesheet;
	 
	 
	
    public InputAbsenSearch() {
    }

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getTglAbsen() {
		return tglAbsen;
	}

	public void setTglAbsen(String tglAbsen) {
		this.tglAbsen = tglAbsen;
	}

	public String getJamMasuk() {
		return jamMasuk;
	}

	public void setJamMasuk(String jamMasuk) {
		this.jamMasuk = jamMasuk;
	}

	public String getJamPulang() {
		return jamPulang;
	}

	public void setJamPulang(String jamPulang) {
		this.jamPulang = jamPulang;
	}

	
	public String getIsLembur() {
		return isLembur;
	}

	public void setIsLembur(String isLembur) {
		this.isLembur = isLembur;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getJarakMsk() {
		return jarakMsk;
	}

	public void setJarakMsk(String jarakMsk) {
		this.jarakMsk = jarakMsk;
	}

	public String getJarakPlg() {
		return jarakPlg;
	}

	public void setJarakPlg(String jarakPlg) {
		this.jarakPlg = jarakPlg;
	}

	

	public String getFlgabs() {
		return flgabs;
	}

	public void setFlgabs(String flgabs) {
		this.flgabs = flgabs;
	}

	public String getIsAbsen() {
		return isAbsen;
	}

	public void setIsAbsen(String isAbsen) {
		this.isAbsen = isAbsen;
	}

	
	public String getHariAbsen() {
		return hariAbsen;
	}

	public void setHariAbsen(String hariAbsen) {
		this.hariAbsen = hariAbsen;
	}

	
	public String getIsLibur() {
		return isLibur;
	}

	public void setIsLibur(String isLibur) {
		this.isLibur = isLibur;
	}

	public String getNikLeader() {
		return nikLeader;
	}

	public void setNikLeader(String nikLeader) {
		this.nikLeader = nikLeader;
	}
	
	

	public String getGpsLatitudeMsk() {
		return gpsLatitudeMsk;
	}

	public void setGpsLatitudeMsk(String gpsLatitudeMsk) {
		this.gpsLatitudeMsk = gpsLatitudeMsk;
	}

	public String getGpsLongitudeMsk() {
		return gpsLongitudeMsk;
	}

	public void setGpsLongitudeMsk(String gpsLongitudeMsk) {
		this.gpsLongitudeMsk = gpsLongitudeMsk;
	}

	public String getGpsLatitudePlg() {
		return gpsLatitudePlg;
	}

	public void setGpsLatitudePlg(String gpsLatitudePlg) {
		this.gpsLatitudePlg = gpsLatitudePlg;
	}

	public String getGpsLongitudePlg() {
		return gpsLongitudePlg;
	}

	public void setGpsLongitudePlg(String gpsLongitudePlg) {
		this.gpsLongitudePlg = gpsLongitudePlg;
	}

	public String getLokasiMsk() {
		return lokasiMsk;
	}

	public void setLokasiMsk(String lokasiMsk) {
		this.lokasiMsk = lokasiMsk;
	}

	public String getLokasiPlg() {
		return lokasiPlg;
	}

	public void setLokasiPlg(String lokasiPlg) {
		this.lokasiPlg = lokasiPlg;
	}

	public String getTotalJamKerja() {
		return totalJamKerja;
	}

	public void setTotalJamKerja(String totalJamKerja) {
		this.totalJamKerja = totalJamKerja;
	}

	public String getTglmasuk() {
		return tglmasuk;
	}

	public void setTglmasuk(String tglmasuk) {
		this.tglmasuk = tglmasuk;
	}

	public String getAlasan() {
		return alasan;
	}

	public void setAlasan(String alasan) {
		this.alasan = alasan;
	}

	public String getIdProject() {
		return idProject;
	}
	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	public String getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(String timesheet) {
		this.timesheet = timesheet;
	}

	public String getNoteapp() {
		return noteapp;
	}

	public void setNoteapp(String noteapp) {
		this.noteapp = noteapp;
	}
	
	
}
