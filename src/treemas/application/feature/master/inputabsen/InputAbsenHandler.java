package treemas.application.feature.master.inputabsen;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;


import treemas.application.feature.master.leader.LeaderBean;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class InputAbsenHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private InputAbsenManager manager = new InputAbsenManager();
    InputAbsenValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	InputAbsenForm intabsenForm = (InputAbsenForm) form;
    	intabsenForm.getPaging().calculationPage();
        String action = intabsenForm.getTask();
        this.validator = new InputAbsenValidator(request);
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListInpAbsen(request, intabsenForm);
        } else if (Global.WEB_TASK_ADD.equals(action)) { //Saat klik tombol masuk
	        	try {
		    		String nik = this.getLoginId(request);
		        	Integer upabsen = manager.getSingleAbsen(nik);
		            if (null != upabsen) { //Jika sudah absen, proses masuk menjadi update lokasi
		        		this.updinsertAbsen(request, intabsenForm);
		        		request.setAttribute("alertMsg", resources.getMessage("common.deskripsi.update"));
						this.loadListInpAbsen(request, intabsenForm);
		            } else {
			        		Calendar cal = Calendar. getInstance();
				        	Date date=cal.getTime();
				        	SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
				        	String time=parser.format(date);
			        		Date ten = null;
//			        		Date eighteen = null;
							try {
								ten = parser.parse("09:00");
//								eighteen = parser.parse("18:00");
							} catch (ParseException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
			        		
			        		try {
			        		    Date userDate = parser.parse(time);
			        		    if (userDate.after(ten)) { //Jika lewat jam 9 masuk ke form input alasan telat
			        		    	return mapping.findForward("note");
			        		    }else {
			        		        this.insertAbsen(request, intabsenForm);
			    			        request.setAttribute("alertMsg", resources.getMessage("common.success.absen"));
			    					this.loadListInpAbsen(request, intabsenForm);
			        		          
								}
			        		} catch (ParseException e) {
			        		    // Invalid date was entered
			        		}
		            }
	        	} catch (AppException e) {
	                this.logger.printStackTrace(e);
	            }
		         
        } else if (Global.WEB_TASK_EDIT.equals(action)) { //Saat klik tombol keluar
        	try {
	    		String nik = this.getLoginId(request);
	        	String cek = manager.getCekAbsen(nik);
	            if (null != cek) { //Saat klik tombol keluar, jika sudah absen masuk
	            	Calendar cal = Calendar. getInstance();
		        	Date date=cal.getTime();
		        	SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
		        	String time=parser.format(date);
		//    		Date ten = null;
		    		Date eighteen = null;
					try {
		//				ten = parser.parse("09:00");
						eighteen = parser.parse("17:00");
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		    		try {
		    		    Date userDate = parser.parse(time);
		    		    if (userDate.before(eighteen)) { //jika belum jam 6, maka masuk ke form input alasan pulang cepat
		    		    	return mapping.findForward("note");
		    		    }else if (userDate.after(eighteen)) {
		    		    	return mapping.findForward("tmsheet");
							}
		    		} catch (ParseException e) {
		    		    // Invalid date was entered
		    		}
	            } else { //Saat klik tombol keluar, jika belum absen masuk
	            	request.setAttribute("alertMsg", resources.getMessage("common.absenk.pending"));
	            	this.loadListInpAbsen(request, intabsenForm);
	            }
        	} catch (AppException e) {
                this.logger.printStackTrace(e);
            }
    		
        }  else if (Global.WEB_TASK_INSERT.equals(action)) { //Saat save dari form note masuk datang telat
        	if (this.validator.validateAbsentelat(new ActionMessages(), intabsenForm)) {
		        this.insertNoteAbsen(request, intabsenForm);
		        request.setAttribute("alertMsg", resources.getMessage("common.success.absen"));
				this.loadListInpAbsen(request, intabsenForm);
        	}
        } else if (Global.WEB_TASK_UPDATE.equals(action)) { //Saat save dari form note pulang cepat
        	if (this.validator.validateAbsencepat(new ActionMessages(), intabsenForm)) {	
        		this.updateNoteAbsen(request, intabsenForm);
            	return mapping.findForward("tmsheet");
        	}
        } else if (Global.WEB_TASK_NEXT_EDIT.equals(action)) { //Saat save dari form input timesheet
        	if (this.validator.validateTimesheetAbsen(new ActionMessages(), intabsenForm)) {	
        		this.updateAbsen(request, intabsenForm);
        		request.setAttribute("alertMsg", resources.getMessage("common.success.absenk"));
                this.loadListInpAbsen(request, intabsenForm);
        	}
    	
        } else if (Global.WEB_TASK_BACK_EDIT.equals(action)) { //Saat save dari note to form input timesheet
        	if (this.validator.validateTimesheetAbsen(new ActionMessages(), intabsenForm)) {	
        		this.updatetmsAbsen(request, intabsenForm);
        		request.setAttribute("alertMsg", resources.getMessage("common.success.absenk"));
                this.loadListInpAbsen(request, intabsenForm);
        	}
    	
        }
        return this.getNextPage(action, mapping);
    }

    private void loadListInpAbsen(HttpServletRequest request, InputAbsenForm form)
    throws CoreException {
     	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        
        PageListWrapper result = this.manager.getAllAbs(pageNumber, form.getSearch(),this.getLoginId(request));
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listInpAbsen", list);
     
    }
    
    private void insertAbsen(HttpServletRequest request, InputAbsenForm form) throws AppException {
    	InputAbsenBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	String abs = form.getIdProject();
		bean.setIdProject(abs);
    	this.manager.insertAbsen(bean, this.getLoginId(request));
    }
    
    private void updinsertAbsen(HttpServletRequest request, InputAbsenForm form) throws AppException {
    	InputAbsenBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	String abs = form.getIdProject();
		bean.setIdProject(abs);
    	this.manager.updInsertAbsen(bean, this.getLoginId(request));
    }
    
    private void insertNoteAbsen(HttpServletRequest request, InputAbsenForm form) throws AppException {
    	InputAbsenBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	String idp = form.getIdProject();
		bean.setIdProject(idp);
		String notet = form.getNote();
		bean.setNote(notet);
    	this.manager.insertNoteAbsen(bean, this.getLoginId(request));
    }
    
    private void updateNoteAbsen(HttpServletRequest request, InputAbsenForm form) throws AppException {
    	InputAbsenBean bean = this.convertToBean(form, this.getLoginId(request), 0);
    	String abs = form.getIdProject();
		bean.setIdProject(abs);
    	String alas = form.getAlasan();
		bean.setNote(alas);
        this.manager.updateNoteAbsen(bean, this.getLoginId(request));
    }
    
    private void updateAbsen(HttpServletRequest request, InputAbsenForm form) throws AppException {
    	InputAbsenBean bean = this.convertToBean(form, this.getLoginId(request), 0);
    	String abs = form.getIdProject();
		bean.setIdProject(abs);
		String tms = form.getTimesheet();
		bean.setTimesheet(tms);
        this.manager.updateAbsen(bean, this.getLoginId(request));
    }
    
    private void updatetmsAbsen(HttpServletRequest request, InputAbsenForm form) throws AppException {
    	InputAbsenBean bean = this.convertToBean(form, this.getLoginId(request), 0);
		String tms = form.getTimesheet();
		bean.setTimesheet(tms);
        this.manager.updatetmsAbsen(bean, this.getLoginId(request));
    }
    
    private InputAbsenBean convertToBean(InputAbsenForm form, String updateUser, int flag) throws AppException {
    	InputAbsenBean bean = form.toBean();
        return bean;
    }
    
    
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			InputAbsenForm intabsenForm = (InputAbsenForm) form;
		try {
			this.loadListInpAbsen(request, intabsenForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((InputAbsenForm) form).setTask(((InputAbsenForm) form).resolvePreviousTask());
		
		String task = ((InputAbsenForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
