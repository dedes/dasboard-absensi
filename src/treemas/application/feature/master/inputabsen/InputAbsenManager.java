package treemas.application.feature.master.inputabsen;


import treemas.application.feature.master.karyawan.KaryawanBean;
import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.CoreException;
import treemas.util.auditrail.AuditTrailException;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.databases.SQLErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class InputAbsenManager extends FeatureManagerBase {
    static final int SEARCH_BY_QUESTION_OR_ANSWER_LABEL = 10;
    /*TODO
     * COMMIT THIS IBATIS*/
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_INPABSEN;

    public PageListWrapper getAllAbs(int pageNumber, InputAbsenSearch search, String loginId) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);
        String nik = loginId;
        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAll", nik);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "cntAll", nik);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }
    
    public Integer getSingleAbsen(String nik) throws CoreException {
    	Integer result = null;
    	 try {
    		 result = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "get", nik);
         } catch (SQLException e) {
             this.logger.printStackTrace(e);
             throw new CoreException(ERROR_DB);
         }

         return result;
     }
    
    public InputAbsenBean getidProject(String idProject) throws CoreException {
    	InputAbsenBean result = null;
     	 try {
     		 result = (InputAbsenBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getAlamat", idProject);
          } catch (SQLException e) {
              this.logger.printStackTrace(e);
              throw new CoreException(ERROR_DB);
          }

          return result;
      }
    
    public String getCekAbsen(String nik) throws CoreException {
    	String result = null;
    	 try {
    		 result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "CekAbsen", nik);
         } catch (SQLException e) {
             this.logger.printStackTrace(e);
             throw new CoreException(ERROR_DB);
         }

         return result;
     }
    
    public void insertAbsen(InputAbsenBean bean, String loginId) throws CoreException {
    	 String nik = loginId;
    	 String idProject = bean.getIdProject();
    	 try	 {
    		try {
//    			String nikleader = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getKaryawan", nik);
    			InputAbsenBean bean3 = (InputAbsenBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getKaryawan", nik);
    			InputAbsenBean bean2 = (InputAbsenBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getalamatprojek", idProject);
    			this.ibatisSqlMap.startTransaction();
    			this.logger.log(ILogger.LEVEL_INFO, "Insert Absen" + nik);
    			Map Param = new HashMap();
    			Param.put("nik", nik);
    			Param.put("gpsLatitudeMsk",bean2.getGpsLatitudeMsk());
    			Param.put("gpsLongitudeMsk",bean2.getGpsLongitudeMsk());
    			Param.put("lokasiMsk", bean2.getLokasiMsk());
    			Param.put("jrkMsk","10");
    			Param.put("isApproved","0");
    			Param.put("isAbsen","1");
    			Param.put("idProject",bean2.getIdProject());
    			Param.put("nikLeader",bean3.getNikLeader());
    			
    			this.ibatisSqlMap.insert(this.STRING_SQL + "insert", Param);	
    			this.ibatisSqlMap.insert(this.STRING_SQL + "insertapp", Param);	
    			this.ibatisSqlMap.commitTransaction();			
    			this.logger.log(ILogger.LEVEL_INFO, "Inserted Absen"
    			       + bean.getNik());
    		} finally {
    			this.ibatisSqlMap.endTransaction();
    		}
    		} catch (SQLException sqle) {
    			this.logger.printStackTrace(sqle);
    			if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY || sqle.getErrorCode() == SQLErrorCode.ERROR_DUPLICATE_KEY) {
    			throw new CoreException("", sqle, ERROR_DATA_EXISTS, bean
    			       .getNik());
    		} else {
    			throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
    		}
    		} catch (Exception e) {
    			this.logger.printStackTrace(e);
    			throw new CoreException(e, ERROR_UNKNOWN);
    		}
    	}
    
    public void updInsertAbsen(InputAbsenBean bean, String loginId) throws CoreException {
    	String nik = loginId;
    	String idProject = bean.getIdProject();
    	try {
		try {
//			String nikleader = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getKaryawan", nik);
			InputAbsenBean bean3 = (InputAbsenBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getKaryawan", nik);
			InputAbsenBean bean2 = (InputAbsenBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getalamatprojek", idProject);
			this.ibatisSqlMap.startTransaction();
			this.logger.log(ILogger.LEVEL_INFO, "Updating Absen" + nik);
			Map Param = new HashMap();
			Param.put("nik", nik);
			Param.put("gpsLatitudeMsk",bean2.getGpsLatitudeMsk());
			Param.put("gpsLongitudeMsk",bean2.getGpsLongitudeMsk());
			Param.put("lokasiMsk", bean2.getLokasiMsk());
			Param.put("jrkMsk","10");
			Param.put("isApproved","0");
			Param.put("isAbsen","1");
			Param.put("idProject",bean2.getIdProject());
			Param.put("nikLeader",bean3.getNikLeader());
			
			this.ibatisSqlMap.update(this.STRING_SQL + "updateabsen",
					Param);
			this.ibatisSqlMap.update(this.STRING_SQL + "updateabsenapp",
					Param);
			this.ibatisSqlMap.update(this.STRING_SQL + "insertabsenhist",
					Param);
			
			this.ibatisSqlMap.commitTransaction();
			
			this.logger.log(ILogger.LEVEL_INFO, "Updated Absen"
			       + nik);
		} finally {
			this.ibatisSqlMap.endTransaction();
		}
		} catch (SQLException se) {
			this.logger.printStackTrace(se);
			throw new CoreException(se, ERROR_DB);
		} catch (Exception e) {
			this.logger.printStackTrace(e);
			throw new CoreException(e, ERROR_UNKNOWN);
		}
    }
   
    public void insertNoteAbsen(InputAbsenBean bean, String loginId) throws CoreException {
    	String nik = loginId;
    	String idProject = bean.getIdProject();
    	try {
		try {
//			String nikleader = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getKaryawan", nik);
			InputAbsenBean bean3 = (InputAbsenBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getKaryawan", nik);
			InputAbsenBean bean2 = (InputAbsenBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getalamatprojek", idProject);
			this.ibatisSqlMap.startTransaction();
			this.logger.log(ILogger.LEVEL_INFO, "Updating Absen" + nik);
			Map Param = new HashMap();
			Param.put("nik", nik);
			Param.put("gpsLatitudeMsk",bean2.getGpsLatitudeMsk());
			Param.put("gpsLongitudeMsk",bean2.getGpsLongitudeMsk());
			Param.put("lokasiMsk", bean2.getLokasiMsk());
			Param.put("jrkMsk","10");
			Param.put("isApproved","0");
			Param.put("isAbsen","1");
			Param.put("idProject",bean2.getIdProject());
			Param.put("nikLeader",bean3.getNikLeader());
   			Param.put("note",bean.getNote());
   			
   			this.ibatisSqlMap.insert(this.STRING_SQL + "inserttelat", Param);
   			this.ibatisSqlMap.insert(this.STRING_SQL + "inserttelatapp", Param);
   			
   			
   			this.ibatisSqlMap.commitTransaction();			
   			this.logger.log(ILogger.LEVEL_INFO, "Inserted Absen"
   			       + bean.getNik());
   		} finally {
   			this.ibatisSqlMap.endTransaction();
   		}
   		} catch (SQLException sqle) {
   			this.logger.printStackTrace(sqle);
   			if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY || sqle.getErrorCode() == SQLErrorCode.ERROR_DUPLICATE_KEY) {
   			throw new CoreException("", sqle, ERROR_DATA_EXISTS, bean
   			       .getNik());
   		} else {
   			throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
   		}
   		} catch (Exception e) {
   			this.logger.printStackTrace(e);
   			throw new CoreException(e, ERROR_UNKNOWN);
   		}
   	}
    
    protected static String selisihDateTime(Date waktuSatu, Date waktuDua) {
		long selisihMS = Math.abs(waktuSatu.getTime() - waktuDua.getTime());
		long selisihDetik = selisihMS / 1000 % 60;
		long selisihMenit = selisihMS / (60 * 1000) % 60;
		long selisihJam = selisihMS / (60 * 60 * 1000) % 24;
		long selisihHari = selisihMS / (24 * 60 * 60 * 1000);
	String selisih = selisihJam + "";     
	return selisih;
	
	}
    
	protected static Date konversiStringkeDate(String tanggalDanWaktuStr,
	String pola, Locale lokal) {
	Date tanggalDate = null;
	String date;
	SimpleDateFormat formatter;
	if (lokal == null) {
	    formatter = new SimpleDateFormat(pola);
	} else {
	    formatter = new SimpleDateFormat(pola, lokal);
	}
	try {
	    tanggalDate = formatter.parse(tanggalDanWaktuStr);
	} catch (ParseException ex) {
	    ex.printStackTrace();
	}
	return tanggalDate;
	}
    
    public void updateNoteAbsen(InputAbsenBean bean, String loginId) throws CoreException {
    	String nik = loginId;
    	String idProject = bean.getIdProject();
			try {
				 try {
					InputAbsenBean bean2 = (InputAbsenBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getalamatprojek", idProject);
					this.ibatisSqlMap.startTransaction();
					this.logger.log(ILogger.LEVEL_INFO, "Updating Absen"
					       + nik);
					Map Param = new HashMap();
					Param.put("nik", nik);
					Param.put("gpsLatitudePlg",bean2.getGpsLatitudePlg());
					Param.put("gpsLongitudePlg",bean2.getGpsLongitudePlg());
					Param.put("lokasiPlg", bean2.getLokasiPlg());
					Param.put("jrkPlg","10");
					Param.put("alasan",bean.getAlasan());
					
					this.ibatisSqlMap.update(this.STRING_SQL + "updatecepat",
							Param);
					this.ibatisSqlMap.update(this.STRING_SQL + "updatecepatapp",
							Param);
					
					Locale lokal = null;
					  String pola = "HH:mm:ss";
					  String waktuMasuk = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleWaktuMasuk", Param);
					  String waktuKeluar  = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleWaktuKeluar", Param);				  
				
					  
					  Date waktuM = InputAbsenManager.konversiStringkeDate(waktuMasuk,
					                pola, lokal);
					  Date Waktuk = InputAbsenManager.konversiStringkeDate(waktuKeluar, pola,
					                lokal);
					 String hasilSelisih = InputAbsenManager.selisihDateTime(waktuM,
							 Waktuk);  
					 Param.put("totalJamKerja", hasilSelisih);
						 this.ibatisSqlMap.update(this.STRING_SQL + "updateJamKerja",Param);	
						 this.ibatisSqlMap.update(this.STRING_SQL + "updateJamKerjaApp",Param);	
						 this.ibatisSqlMap.commitTransaction();
				} finally {
					this.ibatisSqlMap.endTransaction();
				}
			} catch (SQLException se) {
				this.logger.printStackTrace(se);
    			throw new CoreException(se, ERROR_DB);
			} catch (Exception e) {
    			this.logger.printStackTrace(e);
    			throw new CoreException(e, ERROR_UNKNOWN);
    		}
    }
        
        public void updateAbsen(InputAbsenBean bean, String loginId) throws CoreException {
        	String nik = loginId;
        	String idProject = bean.getIdProject();
    		try {
				try {
	    			InputAbsenBean bean2 = (InputAbsenBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getalamatprojek", idProject);
	    			this.ibatisSqlMap.startTransaction();
	    			this.logger.log(ILogger.LEVEL_INFO, "Updating Absen"
	    			       + nik);
	    			Map Param = new HashMap();
	    			Param.put("nik", nik);
	    			Param.put("gpsLatitudePlg",bean2.getGpsLatitudePlg());
	    			Param.put("gpsLongitudePlg",bean2.getGpsLongitudePlg());
	    			Param.put("lokasiPlg", bean2.getLokasiPlg());
	    			Param.put("jrkPlg","10");
	    			Param.put("timesheet",bean.getTimesheet());
	    			
	    			this.ibatisSqlMap.update(this.STRING_SQL + "update",
	    					Param);
	    			this.ibatisSqlMap.update(this.STRING_SQL + "updateapp",
	    					Param);
	    			Locale lokal = null;
					  String pola = "HH:mm:ss";
					  String waktuMasuk = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleWaktuMasuk", Param);
					  String waktuKeluar  = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleWaktuKeluar", Param);				  
				
					  
					  Date waktuM = InputAbsenManager.konversiStringkeDate(waktuMasuk,
					                pola, lokal);
					  Date Waktuk = InputAbsenManager.konversiStringkeDate(waktuKeluar, pola,
					                lokal);
					 String hasilSelisih = InputAbsenManager.selisihDateTime(waktuM,
							 Waktuk);  
					 Param.put("totalJamKerja", hasilSelisih);
						 this.ibatisSqlMap.update(this.STRING_SQL + "updateJamKerja",Param);	
						 this.ibatisSqlMap.update(this.STRING_SQL + "updateJamKerjaApp",Param);	
						 this.ibatisSqlMap.commitTransaction();
	    		} finally {
	    			this.ibatisSqlMap.endTransaction();
	    		}
    		} catch (SQLException se) {
    			this.logger.printStackTrace(se);
    			throw new CoreException(se, ERROR_DB);
    		} catch (Exception e) {
    			this.logger.printStackTrace(e);
    			throw new CoreException(e, ERROR_UNKNOWN);
    		}
        }

        public void updatetmsAbsen(InputAbsenBean bean, String loginId) throws CoreException {
        	String nik = loginId;
//        	String idProject = bean.getIdProject();
        	try {
    		try {
//    			InputAbsenBean bean2 = (InputAbsenBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getalamatprojek", idProject);
    			this.ibatisSqlMap.startTransaction();
    			this.logger.log(ILogger.LEVEL_INFO, "Updating Absen"
    			       + nik);
    			Map Param = new HashMap();
    			Param.put("nik", nik);
//    			Param.put("gpsLatitudePlg",bean2.getGpsLatitudePlg());
//    			Param.put("gpsLongitudePlg",bean2.getGpsLongitudePlg());
//    			Param.put("lokasiPlg", bean2.getLokasiPlg());
//    			Param.put("jrkPlg","10");
    			Param.put("timesheet",bean.getTimesheet());
    			
    			this.ibatisSqlMap.update(this.STRING_SQL + "updatetms",
    					Param);
    			this.ibatisSqlMap.update(this.STRING_SQL + "updatetmsapp",
    					Param);
    			this.ibatisSqlMap.commitTransaction();
    			
    			this.logger.log(ILogger.LEVEL_INFO, "Updated Absen"
    			       + nik);
    		} finally {
    			this.ibatisSqlMap.endTransaction();
    		}
    		} catch (SQLException se) {
    			this.logger.printStackTrace(se);
    			throw new CoreException(se, ERROR_DB);
    		} catch (Exception e) {
    			this.logger.printStackTrace(e);
    			throw new CoreException(e, ERROR_UNKNOWN);
    		}
        }
}
