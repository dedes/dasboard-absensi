package treemas.application.feature.master.inputabsen;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import treemas.application.feature.master.leader.LeaderBean;
import treemas.application.feature.master.leader.LeaderForm;
import treemas.application.feature.master.leader.LeaderManager;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;


import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class InputAbsenValidator extends Validator {
	public InputAbsenValidator(HttpServletRequest request) {
        super(request);
    }

    public InputAbsenValidator(Locale locale) {
        super(locale);
    }
    
    public boolean validateInput(ActionMessages messages, InputAbsenForm form, InputAbsenManager manager) throws CoreException {
        boolean ret = true;
        String nik = form.getNik();
        if (messages == null)
            messages = new ActionMessages();
        
        try {
        	Integer bean = manager.getSingleAbsen(nik);
            if (null != bean) {
            	messages.add("nik", new ActionMessage("errors.absen", "nik " + nik));
            }
            
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
     
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
    
    public boolean validateAbsentelat(ActionMessages messages, InputAbsenForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages(); 
        
        this.textValidator(messages, form.getNote(), "common.note.telat", "note", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }   
    
    public boolean validateAbsencepat(ActionMessages messages, InputAbsenForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages(); 
        
        this.textValidator(messages, form.getAlasan(), "common.note.cepat", "alasan", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }   
    
    public boolean validateTimesheetAbsen(ActionMessages messages, InputAbsenForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages(); 
        
        this.textValidator(messages, form.getTimesheet(), "common.note.timesheet", "timesheet", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }   
 
}
