package treemas.application.feature.master.userapp;

import java.io.Serializable;
import java.util.Date;
import treemas.base.ApplicationBean;
import treemas.util.auditrail.Auditable;

public class UserAppBean implements Serializable  {
	private String nik;
	private String nikMember;
	private String nama;
	private String userUpd;
	private String dtmUpd;
	
	
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getNikMember() {
		return nikMember;
	}
	public void setNikMember(String nikMember) {
		this.nikMember = nikMember;
	}
	public String getUserUpd() {
		return userUpd;
	}
	public void setUserUpd(String userUpd) {
		this.userUpd = userUpd;
	}
	public String getDtmUpd() {
		return dtmUpd;
	}
	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	

}
