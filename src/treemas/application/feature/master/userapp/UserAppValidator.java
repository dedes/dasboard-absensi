package treemas.application.feature.master.userapp;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;


import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/*TODO
 * COMMIT THIS*/
public class UserAppValidator extends Validator {
    public UserAppValidator(HttpServletRequest request) {
        super(request);
    }

    public UserAppValidator(Locale locale) {
        super(locale);
    }
 
    public boolean validateSpoofingInsert(ActionMessages messages, UserAppForm form, UserAppManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        
        try {
        	UserAppBean bean = manager.getSingleSpoofing(form.getNamaSpoofing());
            if (null != bean) {
            	messages.add("namaSpoofing", new ActionMessage("errors.duplicate", "namaSpoofing" + form.getNamaSpoofing()));
            }
            
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
        
        this.textValidator(messages, form.getNamaSpoofing(), "common.nama.spoofing", "namaSpoofing", "1", this.getStringLength(6), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
   
    public boolean validateSpoofingEdit(ActionMessages messages, UserAppForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages(); 
        
        this.textValidator(messages, form.getNamaSpoofing(), "common.nama.spoofing", "namaSpoofing", "1", this.getStringLength(6), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }      
    }


