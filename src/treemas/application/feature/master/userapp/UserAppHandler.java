package treemas.application.feature.master.userapp;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import treemas.application.feature.master.spoofing.SpoofingBean;
import treemas.application.feature.master.spoofing.SpoofingForm;
//import treemas.application.feature.master.userapp.UserAppValidator;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.globalbean.LoginBean;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class UserAppHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private UserAppManager manager = new UserAppManager();
 //   UserAppValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	UserAppForm userAppForm = (UserAppForm) form;
    	userAppForm.getPaging().calculationPage();
        String action = userAppForm.getTask();
 //       this.validator = new UserAppValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListAll(request, userAppForm);
            this.loadDropDown(request);
        	this.loadList(request, userAppForm);
        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
        	this.loadListAll(request, userAppForm);
            this.loadDropDown(request);
        	this.update(request, userAppForm);
        	this.loadList(request, userAppForm);
        } /*else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.validator.validateSpoofingInsert(new ActionMessages(), spoofingForm, this.manager)) {
					this.insertSpoofing(request, userMemberForm);
					this.loadListSpoofing(request, userMemberForm);
            }	
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
        	SpoofingBean bean = this.manager.getSingleSpoofing(userMemberForm.getNamaSpoofing());
        	spoofingForm.fromBean(bean);
        	spoofingForm.getPaging().setDispatch(null);

        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
            if (this.validator.validateSpoofingEdit(new ActionMessages(), userMemberForm)) {
            	this.updateSpoofing(request, userMemberForm);
                this.loadListSpoofing(request, userMemberForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
            }
        	
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = userMemberForm.getPaging();
            try {
            	this.deleteSpoofing(request, userMemberForm);
//                this.manager.deleteIndustri(industriForm.getKodeIndustri(),
//                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.loadList(request, userMemberForm);
                page.setDispatch(null);
            }
        }*/

        return this.getNextPage(action, mapping);
    }

    private void loadListAll(HttpServletRequest request, UserAppForm form)
    throws CoreException {
    	
       List list = this.manager.getAll(form.getSearch());
     //  list = BeanConverter.convertBeansToString(list, form.getClass());
       request.setAttribute("listAll", list);
     
    }
    
    private void loadList(HttpServletRequest request, UserAppForm form)
    	    throws CoreException {
    	    	
       List list = this.manager.getByNik(form.getNik());
   //    list = BeanConverter.convertBeansToString(list, form.getClass());
       request.setAttribute("listByNik", list);
    	     
    }
    
    private void loadDropDown(HttpServletRequest request) throws CoreException {
        MultipleParameterBean bean = new MultipleParameterBean();
        bean.setActive(Global.STRING_TRUE);
        bean.setValueUseKey(Global.STRING_FALSE);
        bean.setUser(this.getLoginId(request));
        List list = this.multipleManager.getListLeader(bean);
        request.setAttribute("listLeader", list);
    }
    
    private void update (HttpServletRequest request, UserAppForm form) throws AppException {
    	UserAppBean bean = this.convertToBean(form, this.getLoginId(request), 0);
      	this.manager.update(bean, form.getNikList());
		
    }
    private UserAppBean convertToBean(UserAppForm form, String updateUser, int flag) throws AppException {
    	UserAppBean bean = form.toBean();
        return bean;
    }


/*  private void insertSpoofing(HttpServletRequest request, SpoofingForm form) throws AppException {
    	SpoofingBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	this.manager.insertSpoofing(bean);
    }
    
    
    private void deleteSpoofing(HttpServletRequest request, SpoofingForm form) throws AppException {
    	SpoofingBean bean = this.convertToBean(form, this.getLoginId(request), -1);
        this.manager.deleteSpoofing(bean);
        
    }
    
    private SpoofingBean convertToBean(SpoofingForm form, String updateUser, int flag) throws AppException {
    	SpoofingBean bean = form.toBean();
        return bean;
    }
    */
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			UserAppForm sForm = (UserAppForm) form;
		try {
			this.loadListAll(request, sForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((UserAppForm) form).setTask(((UserAppForm) form).resolvePreviousTask());
		
		String task = ((UserAppForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
