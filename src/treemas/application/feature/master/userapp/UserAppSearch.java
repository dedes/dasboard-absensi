package treemas.application.feature.master.userapp;

import treemas.base.search.SearchForm;

public class UserAppSearch extends SearchForm {

	private String nik;
	private String nikMember;
	private String userUpd;
	private String dtmUpd;
    
    public UserAppSearch() {
    }

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNikMember() {
		return nikMember;
	}

	public void setNikMember(String nikMember) {
		this.nikMember = nikMember;
	}

	public String getUserUpd() {
		return userUpd;
	}

	public void setUserUpd(String userUpd) {
		this.userUpd = userUpd;
	}

	public String getDtmUpd() {
		return dtmUpd;
	}

	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}

}
