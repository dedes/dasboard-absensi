package treemas.application.feature.master.userapp;



import treemas.application.feature.master.project.ProjectBean;
import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.globalbean.LoginBean;
import treemas.util.CoreException;
import treemas.util.auditrail.AuditTrailException;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.databases.SQLErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UserAppManager extends FeatureManagerBase {
    static final int SEARCH_BY_QUESTION_OR_ANSWER_LABEL = 10;
    /*TODO
     * COMMIT THIS IBATIS*/
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_USER_APP;

    public List getAll(UserAppSearch search) throws CoreException {
        List result = null;
        
        try {
            result = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAll", search);
         } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }
    
    public List getByNik(String nik) throws CoreException {
        List result = null;
        
        try {
            result = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getByNik", nik);
         } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }
    
    public void update(UserAppBean bean, String[] nikList) throws CoreException{
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
		    Date date = new Date();   
			String[] arr = nikList[0].split(",");
			Map paramMap = new HashMap();
			paramMap.put("nik", bean.getNik());
			paramMap.put("usrUpd", "");
			paramMap.put("dtmUpd", date);
		if (arr.length > 0){
			this.ibatisSqlMap.delete( this.STRING_SQL + "delete", paramMap);
			for (int i = 0; i<arr.length; i++){
				
				paramMap.put("nikMember", arr[i]);
				this.ibatisSqlMap.insert( this.STRING_SQL + "insertMember", paramMap);
				this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "INSERT DATA (Nik Member): " + arr[i]);
			//	this.ibatisSqlMap.commitTransaction();
			//	this.ibatisSqlMap.endTransaction();
			}
		
		}
	     } catch (SQLException se) {
					this.logger.printStackTrace(se);
					throw new CoreException(se, ERROR_DB);
		 }
    }

    
   /* public void insertSpoofing(SpoofingBean spoofingBean) throws CoreException {
	try	 {
		try {
			this.ibatisSqlMap.startTransaction();
			this.logger.log(ILogger.LEVEL_INFO, "Inserting Spoofing"
			       + spoofingBean.getNamaSpoofing());
			
			this.ibatisSqlMap.insert(this.STRING_SQL + "insert",
					spoofingBean);
			
			this.ibatisSqlMap.commitTransaction();
			
			this.logger.log(ILogger.LEVEL_INFO, "Inserted Spoofing"
			       + spoofingBean.getNamaSpoofing());
		} finally {
			this.ibatisSqlMap.endTransaction();
		}
		} catch (SQLException sqle) {
			this.logger.printStackTrace(sqle);
			if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY || sqle.getErrorCode() == SQLErrorCode.ERROR_DUPLICATE_KEY) {
			throw new CoreException("", sqle, ERROR_DATA_EXISTS, spoofingBean
			       .getNamaSpoofing());
		} else {
			throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
		}
		} catch (Exception e) {
			this.logger.printStackTrace(e);
			throw new CoreException(e, ERROR_UNKNOWN);
		}
	}
    
    
    
    public SpoofingBean getSingleSpoofing(String namaSpoofing) throws CoreException {
    	SpoofingBean result = null;
    	 try {
             result = (SpoofingBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "get", namaSpoofing);
         } catch (SQLException e) {
             this.logger.printStackTrace(e);
             throw new CoreException(ERROR_DB);
         }

         return result;
     }
   
    
    
    public void deleteSpoofing(SpoofingBean bean)
	    throws CoreException {
    	String namaSpoofing = bean.getNamaSpoofing();
	
		try {
		    try {
		        this.ibatisSqlMap.startTransaction();
		        this.logger.log(ILogger.LEVEL_INFO, "Deleting Spoofing" + namaSpoofing);
		        this.ibatisSqlMap.delete(this.STRING_SQL + "delete", bean);
		        this.ibatisSqlMap.commitTransaction();
		
		        this.logger.log(ILogger.LEVEL_INFO, "Deleted Spoofing" + namaSpoofing);
		    } finally {
		        this.ibatisSqlMap.endTransaction();
		    }
		} catch (SQLException sqle) {
		    this.logger.printStackTrace(sqle);
		    if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND || sqle.getErrorCode() == SQLErrorCode.ERROR_CHILD_FOUND) {
		        throw new CoreException("", sqle, ERROR_HAS_CHILD, namaSpoofing);
		
		    } else {
		        throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
		    }
		} catch (Exception e) {
		    this.logger.printStackTrace(e);
		    throw new CoreException(e, ERROR_UNKNOWN);
		}
	}*/
}
