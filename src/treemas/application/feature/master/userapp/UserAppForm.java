package treemas.application.feature.master.userapp;



import java.security.NoSuchAlgorithmException;
import com.google.common.base.Strings;
import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class UserAppForm extends FeatureFormBase {

	private String nik;
	private String nikMember;
	private String nama;
	private String[] nikList = {};
	private String userUpd;
	private String dtmUpd;
   
    private UserAppSearch search = new UserAppSearch();
    
    public UserAppForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }
    
    public UserAppSearch getSearch() {
		return search;
	}

	public void setSearch(UserAppSearch search) {
		this.search = search;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNikMember() {
		return nikMember;
	}

	public void setNikMember(String nikMember) {
		this.nikMember = nikMember;
	}

	public String getUserUpd() {
		return userUpd;
	}

	public void setUserUpd(String userUpd) {
		this.userUpd = userUpd;
	}

	public String getDtmUpd() {
		return dtmUpd;
	}

	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public String[] getNikList() {
        return nikList;
    }

    public void setNikList(String[] nikList) {
        this.nikList = nikList;
    }

	public UserAppBean toBean() {
		UserAppBean bean = new UserAppBean();
        BeanConverter.convertBeanFromString(this, bean);
        return bean;
    }

	
}
