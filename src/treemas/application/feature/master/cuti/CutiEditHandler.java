package treemas.application.feature.master.cuti;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import treemas.application.feature.master.leader.LeaderBean;
import treemas.application.general.multiple.MultipleManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.PagingInfo;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class CutiEditHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private CutiManagerKaryawan manager = new CutiManagerKaryawan();
    CutiValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
    	CutiForm cutiForm = (CutiForm) form;
    	cutiForm.getPaging().calculationPage();
        String action = cutiForm.getTask();
        this.validator = new CutiValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || Global.WEB_TASK_SEARCH.equals(action)) {
        	CutiBean bean = this.manager.getSingleCuti(this.getLoginId(request));
        	cutiForm.fromBean(bean);
        	cutiForm.getPaging().setDispatch(null);
//        	this.loadListCuti(request, cutiForm);
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.validator.validateCutiInsert(new ActionMessages(), cutiForm, this.manager)) {
				this.insertCuti(request, cutiForm);
				request.setAttribute("message", resources.getMessage("common.pending.approve"));
				this.clearText(cutiForm);
			}	
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListCuti(HttpServletRequest request, CutiForm form)
    throws CoreException {
     	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
       
        PageListWrapper result = this.manager.getAllCuti(pageNumber, form.getSearch(), this.getLoginId(request));
        @SuppressWarnings("rawtypes")
		List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listCuti", list);
     
    }
    
    private void clearText(CutiForm form) {
    	form.setLeader("");
    	form.setTglMulai("");
    	form.setTglSelesai("");
    	form.setTglKerja("");
    	form.setJmlCuti("");
    	form.setAlmtCuti("");
    	form.setKeperluanCuti("");
    }
    
    private void insertCuti(HttpServletRequest request, CutiForm form) throws AppException {
    	CutiBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	
    	String dates = form.getTglMulai();
    	String dates2 = form.getTglSelesai();
    	String dates3 = form.getTglKerja();
		bean.setTglMulai(dates);
		bean.setTglSelesai(dates2);
		bean.setTglKerja(dates3);
		try {
			this.manager.insertCuti(bean);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    
    private CutiBean convertToBean(CutiForm form, String updateUser, int flag) throws AppException {
    	CutiBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			CutiForm cutiForm = (CutiForm) form;
		try {
			this.loadListCuti(request, cutiForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((CutiForm) form).setTask(((CutiForm) form).resolvePreviousTask());
		
		String task = ((CutiForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
