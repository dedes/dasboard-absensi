package treemas.application.feature.master.cuti;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import treemas.base.ApplicationBean;
import treemas.util.auditrail.Auditable;

public class CutiBean implements Serializable  {
	private String nik;
	private String nama;
	private String leader;
	private String tglMulai;
    private String tglSelesai;
    private String tglKerja;
    private Integer jmlCuti;
    private String almtCuti;
    private String keperluanCuti;
    private String usrUpd;
    private Date dtmUpd;
    private String isApproved;
    private String noteApp;
    private String status;
   
	
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getLeader() {
		return leader;
	}
	public void setLeader(String leader) {
		this.leader = leader;
	}
	
	
	public String getTglMulai() {
		return tglMulai;
	}
	public void setTglMulai(String tglMulai) {
		this.tglMulai = tglMulai;
	}
	public String getTglSelesai() {
		return tglSelesai;
	}
	public void setTglSelesai(String tglSelesai) {
		this.tglSelesai = tglSelesai;
	}
	public String getTglKerja() {
		return tglKerja;
	}
	public void setTglKerja(String tglKerja) {
		this.tglKerja = tglKerja;
	}
	public Integer getJmlCuti() {
		return jmlCuti;
	}
	public void setJmlCuti(Integer jmlCuti) {
		this.jmlCuti = jmlCuti;
	}
	public String getAlmtCuti() {
		return almtCuti;
	}
	public void setAlmtCuti(String almtCuti) {
		this.almtCuti = almtCuti;
	}
	public String getKeperluanCuti() {
		return keperluanCuti;
	}
	public void setKeperluanCuti(String keperluanCuti) {
		this.keperluanCuti = keperluanCuti;
	}
	public String getUsrUpd() {
		return usrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}
	public Date getDtmUpd() {
		return dtmUpd;
	}
	public void setDtmUpd(Date dtmUpd) {
		this.dtmUpd = dtmUpd;
	}
	public String getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(String isApproved) {
		this.isApproved = isApproved;
	}
	public String getNoteApp() {
		return noteApp;
	}
	public void setNoteApp(String noteApp) {
		this.noteApp = noteApp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
