package treemas.application.feature.master.cuti;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.ietf.jgss.MessageProp;

import com.google.common.base.Strings;

import treemas.application.general.multiple.MultipleManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class CutiHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private CutiManager manager = new CutiManager();
    CutiValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	CutiForm cutiForm = (CutiForm) form;
    	cutiForm.getPaging().calculationPage();
        String action = cutiForm.getTask();
        this.validator = new CutiValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListCuti(request, cutiForm);
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListCuti(HttpServletRequest request, CutiForm form)
    throws CoreException {
     	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
		
        PageListWrapper result = this.manager.getAllCuti(pageNumber, form.getSearch());
        @SuppressWarnings("rawtypes")
		List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listCuti", list);
     
    }
    
   
    private CutiBean convertToBean(CutiForm form, String updateUser, int flag) throws AppException {
    	CutiBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			CutiForm cutiForm = (CutiForm) form;
		try {
			this.loadListCuti(request, cutiForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((CutiForm) form).setTask(((CutiForm) form).resolvePreviousTask());
		
		String task = ((CutiForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
