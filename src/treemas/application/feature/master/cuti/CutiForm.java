package treemas.application.feature.master.cuti;



import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class CutiForm extends FeatureFormBase {
	private String nik;
	private String nama;
	private String leader;
    private String tglMulai;
    private String tglSelesai;
    private String tglKerja;
    private String jmlCuti;
    private String almtCuti;
    private String keperluanCuti;
    private String usrUpd;
    private Date dtmUpd;
    private String isApproved;
    private String noteApp;
    private String status;
   
   
    private CutiSearch search = new CutiSearch();
    
    public CutiForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public CutiSearch getSearch() {
		return search;
	}

	public void setSearch(CutiSearch search) {
		this.search = search;
	}


	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getLeader() {
		return leader;
	}

	public void setLeader(String leader) {
		this.leader = leader;
	}

	public String getTglMulai() {
		return tglMulai;
	}

	public void setTglMulai(String tglMulai) {
		this.tglMulai = tglMulai;
	}

	public String getTglSelesai() {
		return tglSelesai;
	}

	public void setTglSelesai(String tglSelesai) {
		this.tglSelesai = tglSelesai;
	}

	public String getTglKerja() {
		return tglKerja;
	}

	public void setTglKerja(String tglKerja) {
		this.tglKerja = tglKerja;
	}

	public String getJmlCuti() {
		return jmlCuti;
	}

	public void setJmlCuti(String jmlCuti) {
		this.jmlCuti = jmlCuti;
	}

	public String getAlmtCuti() {
		return almtCuti;
	}

	public void setAlmtCuti(String almtCuti) {
		this.almtCuti = almtCuti;
	}

	public String getKeperluanCuti() {
		return keperluanCuti;
	}

	public void setKeperluanCuti(String keperluanCuti) {
		this.keperluanCuti = keperluanCuti;
	}

	public String getUsrUpd() {
		return usrUpd;
	}

	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}

	public Date getDtmUpd() {
		return dtmUpd;
	}

	public void setDtmUpd(Date dtmUpd) {
		this.dtmUpd = dtmUpd;
	}

	
	public String getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(String isApproved) {
		this.isApproved = isApproved;
	}

	public String getNoteApp() {
		return noteApp;
	}

	public void setNoteApp(String noteApp) {
		this.noteApp = noteApp;
	}
	
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CutiBean toBean() {
		CutiBean bean = new CutiBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

}
