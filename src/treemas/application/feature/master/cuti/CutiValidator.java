package treemas.application.feature.master.cuti;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import treemas.application.feature.master.leader.LeaderBean;
import treemas.application.feature.master.leader.LeaderForm;
import treemas.application.feature.master.leader.LeaderManager;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;


import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class CutiValidator extends Validator {
	public CutiValidator(HttpServletRequest request) {
        super(request);
    }

    public CutiValidator(Locale locale) {
        super(locale);
    }
    public boolean validateCutiInsert(ActionMessages messages, CutiForm form, CutiManagerKaryawan manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        try {
        	CutiBean bean = manager.getSingleCutiVal(form.getNik());
            if (null != bean) {
            	messages.add("nik", new ActionMessage("errors.duplicate.cuti", "nik" + form.getNik()));
            }
            
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
        
        this.textValidator(messages, form.getNik(), "common.id.nik", "nik", "1", this.getStringLength(6), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getNama(), "common.namakary", "nama", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getLeader(), "common.leader", "leader", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getTglMulai(), "common.tglmulai", "tglMulai", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getTglSelesai(), "common.tglselesai", "tglSelesai", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getTglKerja(), "common.tglkerja", "tglKerja", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }  
}
