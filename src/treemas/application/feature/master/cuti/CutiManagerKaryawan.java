package treemas.application.feature.master.cuti;

import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.CoreException;
import treemas.util.auditrail.AuditTrailException;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.databases.SQLErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CutiManagerKaryawan extends FeatureManagerBase {
    static final int SEARCH_BY_QUESTION_OR_ANSWER_LABEL = 10;
    /*TODO
     * COMMIT THIS IBATIS*/
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_CUTIKR;

    public PageListWrapper getAllCuti(int pageNumber, CutiSearch search, String loginId) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);
        String nik = loginId;
        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAll", nik);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "cntAll", nik);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }
    
    public CutiBean getSingleCuti(String loginId) throws CoreException {
    	CutiBean result = null;
    	String nik = loginId;
    	 try {
             result = (CutiBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "get", nik);
         } catch (SQLException e) {
             this.logger.printStackTrace(e);
             throw new CoreException(ERROR_DB);
         }

         return result;
     }
    
    public CutiBean getSingleCutiVal(String nik) throws CoreException {
    	CutiBean result = null;
    	 try {
             result = (CutiBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getval", nik);
         } catch (SQLException e) {
             this.logger.printStackTrace(e);
             throw new CoreException(ERROR_DB);
         }

         return result;
     }
    
    public void insertCuti(CutiBean cutiBean) throws CoreException {
    	try	 {
    		try {
    			this.ibatisSqlMap.startTransaction();
    			this.logger.log(ILogger.LEVEL_INFO, "Inserting Cuti"
    			       + cutiBean.getNik());
    			
    			this.ibatisSqlMap.insert(this.STRING_SQL + "insert",
    					cutiBean);
    			
    			this.ibatisSqlMap.commitTransaction();
    			
    			this.logger.log(ILogger.LEVEL_INFO, "Inserted Cuti"
    			       + cutiBean.getNik());
    		} finally {
    			this.ibatisSqlMap.endTransaction();
    		}
    		} catch (SQLException sqle) {
    			this.logger.printStackTrace(sqle);
    			if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY || sqle.getErrorCode() == SQLErrorCode.ERROR_DUPLICATE_KEY) {
    			throw new CoreException("", sqle, ERROR_DATA_EXISTS, cutiBean
    			       .getNik());
    		} else {
    			throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
    		}
    		} catch (Exception e) {
    			this.logger.printStackTrace(e);
    			throw new CoreException(e, ERROR_UNKNOWN);
    		}
    	}
    
    
}
