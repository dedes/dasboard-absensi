package treemas.application.feature.master.absen;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;


import treemas.application.feature.master.cuti.CutiBean;
import treemas.application.feature.master.cuti.CutiForm;
import treemas.application.feature.master.leader.LeaderBean;
import treemas.application.feature.master.leader.LeaderForm;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class AbsenHandlerApp extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private AbsenManagerApp manager = new AbsenManagerApp();
    AbsenValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	AbsenForm absenForm = (AbsenForm) form;
    	absenForm.getPaging().calculationPage();
        String action = absenForm.getTask();
        this.validator = new AbsenValidator(request);
        
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListAbsen(request, absenForm);
           // industriForm.fillCurrentUri(request, SAVE_PARAMETERS);
            
        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
        	PagingInfo page = absenForm.getPaging();
            try {
            	this.approveAbsen(request, absenForm);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
            	request.setAttribute("message", resources.getMessage("common.confirmapprove"));
            	this.loadListAbsen(request, absenForm);
                page.setDispatch(null);
            }
        	
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = absenForm.getPaging();
            try {
            	this.rejectAbsen(request, absenForm);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
            	request.setAttribute("message", resources.getMessage("common.confirmreject"));
            	this.loadListAbsen(request, absenForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListAbsen(HttpServletRequest request, AbsenForm form)
    throws CoreException {
     	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        
        PageListWrapper result = this.manager.getAllAbs(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listAbsen", list);
     
    }
    
    private void approveAbsen(HttpServletRequest request, AbsenForm form) throws AppException {
    	AbsenBean bean = this.convertToBean(form, this.getLoginId(request), 0);
        this.manager.approveAbsen(bean);
    }
    
    private void rejectAbsen(HttpServletRequest request, AbsenForm form) throws AppException {
    	AbsenBean bean = this.convertToBean(form, this.getLoginId(request), -1);
        this.manager.rejectAbsen(bean);
        
    }
    
    private AbsenBean convertToBean(AbsenForm form, String updateUser, int flag) throws AppException {
    	AbsenBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			AbsenForm absenForm = (AbsenForm) form;
		try {
			this.loadListAbsen(request, absenForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((AbsenForm) form).setTask(((AbsenForm) form).resolvePreviousTask());
		
		String task = ((AbsenForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
