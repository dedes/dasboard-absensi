package treemas.application.feature.master.absen;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;


import treemas.application.feature.master.leader.LeaderBean;
import treemas.application.feature.master.leader.LeaderForm;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class AbsenHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private AbsenManager manager = new AbsenManager();
    AbsenValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	AbsenForm absenForm = (AbsenForm) form;
    	absenForm.getPaging().calculationPage();
        String action = absenForm.getTask();
        this.validator = new AbsenValidator(request);
        
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListAbsen(request, absenForm);
           // industriForm.fillCurrentUri(request, SAVE_PARAMETERS);
            
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListAbsen(HttpServletRequest request, AbsenForm form)
    throws CoreException {
     	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        
        String before = form.getSearch().getTgl();
		String after = form.getSearch().getTglAfter();
		String message = "Date Before or After is Null or Empty";
		if (!(Strings.isNullOrEmpty(before)) && Strings.isNullOrEmpty(after)) {
			request.setAttribute("message", message);
		} else if ((Strings.isNullOrEmpty(before))
				&& (!Strings.isNullOrEmpty(after))) {
			request.setAttribute("message", message);
		}
		
        PageListWrapper result = this.manager.getAllAbs(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listAbsen", list);
     
    }
    
    private AbsenBean convertToBean(AbsenForm form, String updateUser, int flag) throws AppException {
    	AbsenBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			AbsenForm absenForm = (AbsenForm) form;
		try {
			this.loadListAbsen(request, absenForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((AbsenForm) form).setTask(((AbsenForm) form).resolvePreviousTask());
		
		String task = ((AbsenForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
