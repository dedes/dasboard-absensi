package treemas.application.feature.master.absen;



import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class AbsenForm extends FeatureFormBase {
	private String nik;
	private String nama;
    private String tgl;
    private String jamMasuk;
    private String jamKeluar;
    private String gpsLatitudeMsk;
    private String gpsLongitudeMsk;
    private String lokasiMsk;
    private String lembur;
    private String note;
    private String gpsLatitudePlg;
    private String gpsLongitudePlg;
    private String lokasiPlg;
    private String jrkMsk;
    private String jrkPlg;
    private String totalJamKerja;
    private String hari;
    private String statusTelat;
    private String statusCepat;
    private String idProject;
    
	private String tglAfter;
	
   
    private AbsenSearch search = new AbsenSearch();
    
    public AbsenForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public AbsenSearch getSearch() {
		return search;
	}

	public void setSearch(AbsenSearch search) {
		this.search = search;
	}


	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	 public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTgl() {
		return tgl;
	}

	public void setTgl(String tgl) {
		this.tgl = tgl;
	}

	

	public String getJamMasuk() {
		return jamMasuk;
	}

	public void setJamMasuk(String jamMasuk) {
		this.jamMasuk = jamMasuk;
	}

	public String getJamKeluar() {
		return jamKeluar;
	}

	public void setJamKeluar(String jamKeluar) {
		this.jamKeluar = jamKeluar;
	}

	public AbsenBean toBean() {
		 AbsenBean bean = new AbsenBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

	public String getTglAfter() {
		return tglAfter;
	}

	public void setTglAfter(String tglAfter) {
		this.tglAfter = tglAfter;
	}

	public String getLembur() {
		return lembur;
	}

	public void setLembur(String lembur) {
		this.lembur = lembur;
	}

	public String getGpsLatitudeMsk() {
		return gpsLatitudeMsk;
	}

	public void setGpsLatitudeMsk(String gpsLatitudeMsk) {
		this.gpsLatitudeMsk = gpsLatitudeMsk;
	}

	public String getGpsLongitudeMsk() {
		return gpsLongitudeMsk;
	}

	public void setGpsLongitudeMsk(String gpsLongitudeMsk) {
		this.gpsLongitudeMsk = gpsLongitudeMsk;
	}

	public String getLokasiMsk() {
		return lokasiMsk;
	}

	public void setLokasiMsk(String lokasiMsk) {
		this.lokasiMsk = lokasiMsk;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getGpsLatitudePlg() {
		return gpsLatitudePlg;
	}

	public void setGpsLatitudePlg(String gpsLatitudePlg) {
		this.gpsLatitudePlg = gpsLatitudePlg;
	}

	public String getGpsLongitudePlg() {
		return gpsLongitudePlg;
	}

	public void setGpsLongitudePlg(String gpsLongitudePlg) {
		this.gpsLongitudePlg = gpsLongitudePlg;
	}

	public String getLokasiPlg() {
		return lokasiPlg;
	}

	public void setLokasiPlg(String lokasiPlg) {
		this.lokasiPlg = lokasiPlg;
	}

	public String getJrkMsk() {
		return jrkMsk;
	}

	public void setJrkMsk(String jrkMsk) {
		this.jrkMsk = jrkMsk;
	}

	public String getJrkPlg() {
		return jrkPlg;
	}

	public void setJrkPlg(String jrkPlg) {
		this.jrkPlg = jrkPlg;
	}

	public String getTotalJamKerja() {
		return totalJamKerja;
	}

	public void setTotalJamKerja(String totalJamKerja) {
		this.totalJamKerja = totalJamKerja;
	}

	public String getHari() {
		return hari;
	}

	public void setHari(String hari) {
		this.hari = hari;
	}

	
	public String getStatusTelat() {
		return statusTelat;
	}

	public void setStatusTelat(String statusTelat) {
		this.statusTelat = statusTelat;
	}

	public String getStatusCepat() {
		return statusCepat;
	}

	public void setStatusCepat(String statusCepat) {
		this.statusCepat = statusCepat;
	}

	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	
}
