package treemas.application.feature.master.jabatan;

import treemas.base.search.SearchForm;

public class JabatanSearch extends SearchForm {

	private String id;
    private String namaJabatan;
    private String usrUpd;
    private String dtmUpd;
    
    public JabatanSearch() {
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}

	public String getUsrUpd() {
		return usrUpd;
	}

	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}

	public String getDtmUpd() {
		return dtmUpd;
	}

	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}

}
