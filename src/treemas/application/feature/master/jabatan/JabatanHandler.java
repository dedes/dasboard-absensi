package treemas.application.feature.master.jabatan;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;


import treemas.application.feature.master.jabatan.JabatanBean;
import treemas.application.feature.master.jabatan.JabatanForm;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class JabatanHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private JabatanManager manager = new JabatanManager();
    JabatanValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	JabatanForm jabatanForm = (JabatanForm) form;
    	jabatanForm.getPaging().calculationPage();
        String action = jabatanForm.getTask();
        this.validator = new JabatanValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListjabatan(request, jabatanForm);
           // industriForm.fillCurrentUri(request, SAVE_PARAMETERS);
            
        } else if (Global.WEB_TASK_ADD.equals(action)) {
        	jabatanForm.setId("");
        	jabatanForm.setNamaJabatan("");
//        	this.loadHeaderDropDown(request);
        	
        	
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.validator.validateJabatanInsert(new ActionMessages(), jabatanForm, this.manager)) {
					this.insertJabatan(request, jabatanForm);
					this.loadListjabatan(request, jabatanForm);
            }	
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
        	JabatanBean bean = this.manager.getSingleJabatan(jabatanForm.getId());
        	jabatanForm.fromBean(bean);
//        	this.loadHeaderDropDown(request);
        	jabatanForm.getPaging().setDispatch(null);

        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
            if (this.validator.validateJabatanEdit(new ActionMessages(), jabatanForm)) {
            	this.updateJabatan(request, jabatanForm);
                this.loadListjabatan(request, jabatanForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
            }
        	
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = jabatanForm.getPaging();
            try {
            	this.deleteJabatan(request, jabatanForm);
//                this.manager.deleteIndustri(industriForm.getKodeIndustri(),
//                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.loadListjabatan(request, jabatanForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListjabatan(HttpServletRequest request, JabatanForm form)
    throws CoreException {
    	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAllJab(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listJabatan", list);
     
    }
    
//    private void loadHeaderDropDown(HttpServletRequest request) throws CoreException {
//        MultipleParameterBean bean = new MultipleParameterBean();
//        List list = this.multipleManager.getListHeader(bean);
//        request.setAttribute("listHeader", list);
//    }
    
    private void insertJabatan(HttpServletRequest request, JabatanForm form) throws AppException {
    	JabatanBean bean = this.convertToBean(form, this.getLoginId(request), 1);
        this.manager.insertJabatan(bean);

    }
    
    private void updateJabatan(HttpServletRequest request, JabatanForm form) throws AppException {
    	JabatanBean bean = this.convertToBean(form, this.getLoginId(request), 0);
        this.manager.updateJabatan(bean);
    }
    
    private void deleteJabatan(HttpServletRequest request, JabatanForm form) throws AppException {
    	JabatanBean bean = this.convertToBean(form, this.getLoginId(request), -1);
        this.manager.deleteJabatan(bean);
        
    }
    
    private JabatanBean convertToBean(JabatanForm form, String updateUser, int flag) throws AppException {
    	JabatanBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			JabatanForm jabatanForm = (JabatanForm) form;
		try {
			this.loadListjabatan(request, jabatanForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((JabatanForm) form).setTask(((JabatanForm) form).resolvePreviousTask());
		
		String task = ((JabatanForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
