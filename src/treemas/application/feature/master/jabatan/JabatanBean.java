package treemas.application.feature.master.jabatan;

import java.io.Serializable;
import java.util.Date;

import treemas.base.ApplicationBean;
import treemas.util.auditrail.Auditable;

public class JabatanBean implements Serializable  {
	private String id;
    private String namaJabatan;
    private String usrUpd;
    private Date dtmUpd;
   
  
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNamaJabatan() {
		return namaJabatan;
	}
	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}
	public String getUsrUpd() {
		return usrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}
	public Date getDtmUpd() {
		return dtmUpd;
	}
	public void setDtmUpd(Date dtmUpd) {
		this.dtmUpd = dtmUpd;
	}
	
}
