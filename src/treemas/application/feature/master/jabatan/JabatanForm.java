package treemas.application.feature.master.jabatan;



import java.security.NoSuchAlgorithmException;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class JabatanForm extends FeatureFormBase {

	
	private String id;
    private String namaJabatan;
    private String usrUpd;
    private String dtmUpd;
 
   
    private JabatanSearch search = new JabatanSearch();
    
    public JabatanForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public JabatanSearch getSearch() {
		return search;
	}

	public void setSearch(JabatanSearch search) {
		this.search = search;
	}

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}

	public String getUsrUpd() {
		return usrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}
	public String getDtmUpd() {
		return dtmUpd;
	}
	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}
	
	 public JabatanBean toBean() {
		 JabatanBean bean = new JabatanBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

	
}
