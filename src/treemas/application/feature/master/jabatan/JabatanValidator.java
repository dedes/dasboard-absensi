package treemas.application.feature.master.jabatan;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;


import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/*TODO
 * COMMIT THIS*/
public class JabatanValidator extends Validator {
    public JabatanValidator(HttpServletRequest request) {
        super(request);
    }

    public JabatanValidator(Locale locale) {
        super(locale);
    }
 
    public boolean validateJabatanInsert(ActionMessages messages, JabatanForm form, JabatanManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        
        try {
        	JabatanBean bean = manager.getSingleJabatan(form.getId());
            if (null != bean) {
            	messages.add("id", new ActionMessage("errors.duplicate", "id " + form.getId()));
            }
            
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
        
        this.textValidator(messages, form.getId(), "common.formjab", "id", "1", this.getStringLength(6), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getNamaJabatan(), "feature.jabatan.description", "namaJabatan", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
   
    public boolean validateJabatanEdit(ActionMessages messages, JabatanForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages(); 
        
        this.textValidator(messages, form.getNamaJabatan(), "feature.jabatan.description", "namaJabatan", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }      
    }


