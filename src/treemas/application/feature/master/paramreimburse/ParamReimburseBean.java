package treemas.application.feature.master.paramreimburse;

import java.io.Serializable;
import java.util.Date;

import treemas.base.ApplicationBean;
import treemas.util.auditrail.Auditable;

public class ParamReimburseBean implements Serializable  {
	private String idReimburse;
    private String nama;
    private String nominal;
    private String note;
    
    
	
	public String getIdReimburse() {
		return idReimburse;
	}
	public void setIdReimburse(String idReimburse) {
		this.idReimburse = idReimburse;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNominal() {
		return nominal;
	}
	public void setNominal(String nominal) {
		this.nominal = nominal;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
   
  
	
}
