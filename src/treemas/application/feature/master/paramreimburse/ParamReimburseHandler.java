package treemas.application.feature.master.paramreimburse;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;


import treemas.application.feature.master.jabatan.JabatanBean;
import treemas.application.feature.master.jabatan.JabatanForm;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class ParamReimburseHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private ParamReimburseManager manager = new ParamReimburseManager();
    ParamReimburseValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	ParamReimburseForm paramreimburseForm = (ParamReimburseForm) form;
    	paramreimburseForm.getPaging().calculationPage();
        String action = paramreimburseForm.getTask();
        this.validator = new ParamReimburseValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadList(request, paramreimburseForm);
           // industriForm.fillCurrentUri(request, SAVE_PARAMETERS);
            
        } else if (Global.WEB_TASK_ADD.equals(action)) {
        	paramreimburseForm.setIdReimburse("");
        	paramreimburseForm.setNama("");
//        	this.loadHeaderDropDown(request);
        	
        	
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.validator.validateInsert(new ActionMessages(), paramreimburseForm, this.manager)) {
					this.insert(request, paramreimburseForm);
					this.loadList(request, paramreimburseForm);
            }	
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
        	ParamReimburseBean bean = this.manager.getSingle(paramreimburseForm.getIdReimburse());
        	paramreimburseForm.fromBean(bean);
//        	this.loadHeaderDropDown(request);
        	paramreimburseForm.getPaging().setDispatch(null);

        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
            if (this.validator.validateEdit(new ActionMessages(), paramreimburseForm)) {
            	this.update(request, paramreimburseForm);
                this.loadList(request, paramreimburseForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
            }
        	
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = paramreimburseForm.getPaging();
            try {
            	this.delete(request, paramreimburseForm);
//                this.manager.deleteIndustri(industriForm.getKodeIndustri(),
//                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.loadList(request, paramreimburseForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadList(HttpServletRequest request, ParamReimburseForm form)
    throws CoreException {
    	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAll(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listParamReimburse", list);
     
    }
    
//    private void loadHeaderDropDown(HttpServletRequest request) throws CoreException {
//        MultipleParameterBean bean = new MultipleParameterBean();
//        List list = this.multipleManager.getListHeader(bean);
//        request.setAttribute("listHeader", list);
//    }
    
    private void insert(HttpServletRequest request, ParamReimburseForm form) throws AppException {
    	ParamReimburseBean bean = this.convertToBean(form, this.getLoginId(request), 1);
        this.manager.insert(bean);

    }
    
    private void update(HttpServletRequest request, ParamReimburseForm form) throws AppException {
    	ParamReimburseBean bean = this.convertToBean(form, this.getLoginId(request), 0);
        this.manager.update(bean);
    }
    
    private void delete(HttpServletRequest request, ParamReimburseForm form) throws AppException {
    	ParamReimburseBean bean = this.convertToBean(form, this.getLoginId(request), -1);
        this.manager.delete(bean);
        
    }
    
    private ParamReimburseBean convertToBean(ParamReimburseForm form, String updateUser, int flag) throws AppException {
    	ParamReimburseBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			ParamReimburseForm paramreimburseForm = (ParamReimburseForm) form;
		try {
			this.loadList(request, paramreimburseForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((ParamReimburseForm) form).setTask(((ParamReimburseForm) form).resolvePreviousTask());
		
		String task = ((ParamReimburseForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
