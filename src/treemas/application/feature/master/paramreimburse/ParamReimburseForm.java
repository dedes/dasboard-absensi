package treemas.application.feature.master.paramreimburse;

import java.security.NoSuchAlgorithmException;
import com.google.common.base.Strings;
import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class ParamReimburseForm extends FeatureFormBase {

	
	private String idReimburse;
    private String nama;
    private String nominal;
    private String note;
 
   
    private ParamReimburseSearch search = new ParamReimburseSearch();
    
    public ParamReimburseForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public ParamReimburseSearch getSearch() {
		return search;
	}

	public void setSearch(ParamReimburseSearch search) {
		this.search = search;
	}


	public String getIdReimburse() {
		return idReimburse;
	}

	public void setIdReimburse(String idReimburse) {
		this.idReimburse = idReimburse;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNominal() {
		return nominal;
	}

	public void setNominal(String nominal) {
		this.nominal = nominal;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public ParamReimburseBean toBean() {
		 ParamReimburseBean bean = new ParamReimburseBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

	
}
