package treemas.application.feature.master.paramreimburse;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;


import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/*TODO
 * COMMIT THIS*/
public class ParamReimburseValidator extends Validator {
    public ParamReimburseValidator(HttpServletRequest request) {
        super(request);
    }

    public ParamReimburseValidator(Locale locale) {
        super(locale);
    }
 
    public boolean validateInsert(ActionMessages messages, ParamReimburseForm form, ParamReimburseManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        
        try {
        	ParamReimburseBean bean = manager.getSingle(form.getIdReimburse());
            if (null != bean) {
            	messages.add("idReimburse", new ActionMessage("errors.duplicate", "idReimburse " + form.getIdReimburse()));
            }
            
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
        this.textValidator(messages, form.getNama(), "common.nm.paramreimburse", "nama", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getIdReimburse(), "common.id.paramreimburse", "idReimburse", "1", this.getStringLength(10), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getNominal(), "common.nom.paramreimburse", "nominal", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);  
         if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
   
    public boolean validateEdit(ActionMessages messages, ParamReimburseForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages(); 
        
        this.textValidator(messages, form.getNama(), "common.nm.paramreimburse", "nama", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getIdReimburse(), "common.id.paramreimburse", "idReimburse", "1", this.getStringLength(10), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getNominal(), "common.nom.paramreimburse", "nominal", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);  
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }      
    }


