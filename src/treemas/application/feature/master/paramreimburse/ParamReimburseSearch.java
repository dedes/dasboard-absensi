package treemas.application.feature.master.paramreimburse;

import treemas.base.search.SearchForm;

public class ParamReimburseSearch extends SearchForm {

	private String idReimburse;
    private String nama;
    private String nominal;
    private String note;
    
    public ParamReimburseSearch() {
    }

	public String getIdReimburse() {
		return idReimburse;
	}

	public void setIdReimburse(String idReimburse) {
		this.idReimburse = idReimburse;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNominal() {
		return nominal;
	}

	public void setNominal(String nominal) {
		this.nominal = nominal;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	

}
