package treemas.application.feature.master.auditrailTask;

import java.util.Date;

import treemas.base.ApplicationBean;

public class AuditrailTaskBean extends ApplicationBean {
	
	private String AUDITRAILID;
    private String ApprovalNO;
    private String ApprovalID;
    private String ApprovalSchemeID;
    private String UserRequest;
    private String AprovalAnswer;
    private String ApprovalNoteAnswer;
    private String UserApprovalNext;
    private Date transactionDate;
    private String dateKey;
    
	public String getAUDITRAILID() {
		return AUDITRAILID;
	}
	public void setAUDITRAILID(String aUDITRAILID) {
		AUDITRAILID = aUDITRAILID;
	}
	public String getApprovalNO() {
		return ApprovalNO;
	}
	public void setApprovalNO(String approvalNO) {
		ApprovalNO = approvalNO;
	}
	public String getApprovalID() {
		return ApprovalID;
	}
	public void setApprovalID(String approvalID) {
		ApprovalID = approvalID;
	}
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public String getUserRequest() {
		return UserRequest;
	}
	public void setUserRequest(String userRequest) {
		UserRequest = userRequest;
	}
	public String getAprovalAnswer() {
		return AprovalAnswer;
	}
	public void setAprovalAnswer(String aprovalAnswer) {
		AprovalAnswer = aprovalAnswer;
	}
	public String getApprovalNoteAnswer() {
		return ApprovalNoteAnswer;
	}
	public void setApprovalNoteAnswer(String approvalNoteAnswer) {
		ApprovalNoteAnswer = approvalNoteAnswer;
	}
	public String getUserApprovalNext() {
		return UserApprovalNext;
	}
	public void setUserApprovalNext(String userApprovalNext) {
		UserApprovalNext = userApprovalNext;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getDateKey() {
		return dateKey;
	}
	public void setDateKey(String dateKey) {
		this.dateKey = dateKey;
	}
    
    

}
