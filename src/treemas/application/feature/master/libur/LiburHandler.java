package treemas.application.feature.master.libur;

import com.google.common.base.Strings;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;


import treemas.application.feature.master.libur.LiburBean;
import treemas.application.feature.master.libur.LiburForm;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class LiburHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private LiburManager manager = new LiburManager();
    LiburValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	LiburForm liburForm = (LiburForm) form;
    	liburForm.getPaging().calculationPage();
        String action = liburForm.getTask();
        this.validator = new LiburValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListLibur(request, liburForm);
           // industriForm.fillCurrentUri(request, SAVE_PARAMETERS);
            
        } else if (Global.WEB_TASK_ADD.equals(action)) {
        	
        	liburForm.setTglLibur("");
        	liburForm.setKeterangan("");
//        	this.loadHiburDropDown(request);
        	
        	
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.validator.validateLiburInsert(new ActionMessages(), liburForm, this.manager)) {
					this.insertLibur(request, liburForm);
					this.loadListLibur(request, liburForm);
            }	
            
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
        	LiburBean bean = this.manager.getSingleLibur(liburForm.getTglLibur());
        	liburForm.fromBean(bean);
//        	this.loadHiburDropDown(request);
        	liburForm.getPaging().setDispatch(null);

        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
            if (this.validator.validateLiburEdit(new ActionMessages(), liburForm)) {
            	this.updateLibur(request, liburForm);
                this.loadListLibur(request, liburForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
            }
        	
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = liburForm.getPaging();
            try {
            	this.deleteLibur(request, liburForm);
//                this.manager.deleteIndustri(industriForm.getKodeIndustri(),
//                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.loadListLibur(request, liburForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListLibur(HttpServletRequest request, LiburForm form)
    throws CoreException {
    	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAllLibur(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listLibur", list);
     
    }
    
//    private void loadHiburDropDown(HttpServletRequest request) throws CoreException {
//        MultipleParameterBean bean = new MultipleParameterBean();
//        List list = this.multipleManager.getListHibur(bean);
//        request.setAttribute("listHibur", list);
//    }
    
    private void insertLibur(HttpServletRequest request, LiburForm form) throws AppException {
    	LiburBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	String dates = form.getTglLibur();
		bean.setTglLibur(dates);
		
    	this.manager.insertLibur(bean);

    }
    
    private void updateLibur(HttpServletRequest request, LiburForm form) throws AppException {
    	LiburBean bean = this.convertToBean(form, this.getLoginId(request), 0);
        this.manager.updateLibur(bean);
    }
    
    private void deleteLibur(HttpServletRequest request, LiburForm form) throws AppException {
    	LiburBean bean = this.convertToBean(form, this.getLoginId(request), -1);
        this.manager.deleteLibur(bean);
        
    }
    
    private LiburBean convertToBean(LiburForm form, String updateUser, int flag) throws AppException {
    	LiburBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			LiburForm liburForm = (LiburForm) form;
		try {
			this.loadListLibur(request, liburForm);
//			this.loadHiburDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((LiburForm) form).setTask(((LiburForm) form).resolvePreviousTask());
		
		String task = ((LiburForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
