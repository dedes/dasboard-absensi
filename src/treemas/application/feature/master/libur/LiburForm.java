package treemas.application.feature.master.libur;



import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class LiburForm extends FeatureFormBase {

	
//	private String idLibur;
    private String tglLibur;
    private String keterangan;
 
   
    private LiburSearch search = new LiburSearch();
    
    public LiburForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public LiburSearch getSearch() {
		return search;
	}

	public void setSearch(LiburSearch search) {
		this.search = search;
	}

	
//	 public String getIdLibur() {
//		return idLibur;
//	}
//
//	public void setIdLibur(String idLibur) {
//		this.idLibur = idLibur;
//	}

	public String getTglLibur() {
		return tglLibur;
	}

	public void setTglLibur(String tglLibur) {
		this.tglLibur = tglLibur;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public LiburBean toBean() {
		 LiburBean bean = new LiburBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

	
}
