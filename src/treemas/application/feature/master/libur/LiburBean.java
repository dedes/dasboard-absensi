package treemas.application.feature.master.libur;

import java.io.Serializable;
import java.util.Date;

import treemas.base.ApplicationBean;
import treemas.util.auditrail.Auditable;

public class LiburBean implements Serializable  {
//	private Integer idLibur;
    private String tglLibur;
    private String keterangan;
    
    
	/*public Integer getIdLibur() {
		return idLibur;
	}
	public void setIdLibur(Integer idLibur) {
		this.idLibur = idLibur;
	}*/
	public String getTglLibur() {
		return tglLibur;
	}
	public void setTglLibur(String tglLibur) {
		this.tglLibur = tglLibur;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
   
  
	
}
