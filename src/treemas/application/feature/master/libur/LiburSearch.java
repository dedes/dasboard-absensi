package treemas.application.feature.master.libur;

import treemas.base.search.SearchForm;

public class LiburSearch extends SearchForm {

//	private String idLibur;
    private String tglLibur;
    private String keterangan;
    
    public LiburSearch() {
    }

//	public String getIdLibur() {
//		return idLibur;
//	}
//
//	public void setIdLibur(String idLibur) {
//		this.idLibur = idLibur;
//	}

	public String getTglLibur() {
		return tglLibur;
	}

	public void setTglLibur(String tglLibur) {
		this.tglLibur = tglLibur;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

}
