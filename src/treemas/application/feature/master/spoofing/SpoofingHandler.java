package treemas.application.feature.master.spoofing;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;


import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class SpoofingHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private SpoofingManager manager = new SpoofingManager();
    SpoofingValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	SpoofingForm spoofingForm = (SpoofingForm) form;
    	spoofingForm.getPaging().calculationPage();
        String action = spoofingForm.getTask();
        this.validator = new SpoofingValidator(request);
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListSpoofing(request, spoofingForm);
        } else if (Global.WEB_TASK_ADD.equals(action)) {
        	spoofingForm.setNamaSpoofing("");
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
         //   if (this.validator.validateSpoofingInsert(new ActionMessages(), spoofingForm, this.manager)) {
					this.insertSpoofing(request, spoofingForm);
					this.loadListSpoofing(request, spoofingForm);
         //   }	
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
        	SpoofingBean bean = this.manager.getSingleSpoofing(spoofingForm.getNamaSpoofing());
        	spoofingForm.fromBean(bean);
        	spoofingForm.getPaging().setDispatch(null);

        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
         //   if (this.validator.validateSpoofingEdit(new ActionMessages(), spoofingForm)) {
            	this.updateSpoofing(request, spoofingForm);
                this.loadListSpoofing(request, spoofingForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
         //   }
        	
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = spoofingForm.getPaging();
            try {
            	this.deleteSpoofing(request, spoofingForm);
//                this.manager.deleteIndustri(industriForm.getKodeIndustri(),
//                        this.getLoginId(request), this.screenId);
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
                this.loadListSpoofing(request, spoofingForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListSpoofing(HttpServletRequest request, SpoofingForm form)
    throws CoreException {
    	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAllSPF(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listSpoofing", list);
     
    }
    
    private void insertSpoofing(HttpServletRequest request, SpoofingForm form) throws AppException {
    	SpoofingBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	this.manager.insertSpoofing(bean);
    }
    
    private void updateSpoofing(HttpServletRequest request, SpoofingForm form) throws AppException {
    	SpoofingBean bean = this.convertToBean(form, this.getLoginId(request), 0);
        this.manager.updateSpoofing(bean);
    }
    
    private void deleteSpoofing(HttpServletRequest request, SpoofingForm form) throws AppException {
    	SpoofingBean bean = this.convertToBean(form, this.getLoginId(request), -1);
        this.manager.deleteSpoofing(bean);
        
    }
    
    private SpoofingBean convertToBean(SpoofingForm form, String updateUser, int flag) throws AppException {
    	SpoofingBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			SpoofingForm sForm = (SpoofingForm) form;
		try {
			this.loadListSpoofing(request, sForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((SpoofingForm) form).setTask(((SpoofingForm) form).resolvePreviousTask());
		
		String task = ((SpoofingForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
