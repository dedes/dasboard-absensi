package treemas.application.feature.master.spoofing;

import java.io.Serializable;
import java.util.Date;

import treemas.base.ApplicationBean;
import treemas.util.auditrail.Auditable;

public class SpoofingBean implements Serializable  {
	private String namaSpoofing;

	public String getNamaSpoofing() {
		return namaSpoofing;
	}

	public void setNamaSpoofing(String namaSpoofing) {
		this.namaSpoofing = namaSpoofing;
	}
    
	
	
}
