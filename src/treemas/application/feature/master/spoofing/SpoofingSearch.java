package treemas.application.feature.master.spoofing;

import treemas.base.search.SearchForm;

public class SpoofingSearch extends SearchForm {

	private String namaSpoofing;
    
    public SpoofingSearch() {
    }

	public String getNamaSpoofing() {
		return namaSpoofing;
	}

	public void setNamaSpoofing(String namaSpoofing) {
		this.namaSpoofing = namaSpoofing;
	}

	
}
