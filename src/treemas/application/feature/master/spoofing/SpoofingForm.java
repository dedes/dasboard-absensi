package treemas.application.feature.master.spoofing;



import java.security.NoSuchAlgorithmException;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class SpoofingForm extends FeatureFormBase {

	
	private String namaSpoofing;
   
   
    private SpoofingSearch search = new SpoofingSearch();
    
    public SpoofingForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public SpoofingSearch getSearch() {
		return search;
	}

	public void setSearch(SpoofingSearch search) {
		this.search = search;
	}

	

	 public String getNamaSpoofing() {
		return namaSpoofing;
	}

	public void setNamaSpoofing(String namaSpoofing) {
		this.namaSpoofing = namaSpoofing;
	}

	public SpoofingBean toBean() {
		 SpoofingBean bean = new SpoofingBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

	
}
