package treemas.application.feature.master.karyawan;



import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.google.common.base.Strings;

import treemas.application.general.lookup.privilege.PrivilegeForm;
import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class KaryawanForm extends FeatureFormBase {

	private String nik;
    private String nama;
    private String tempatLahir;
    private String tanggalLahir;
    private String jenisKelamin;
    private String agama;
    private String kewarganegaraan;
    private String kodePos;
    private String alamatKtp;
    private String noHP;
    private String imei;
    private String email;
    private String noRek;
    private String npwp;
    private String jenjangPendidikan;
    private String dtmUpd;
    
    private String tanggalBergabung;
    private String alamatSekarang;
    private String statusPerkawinan;
    private String golonganDarah;
    private String nomorKtp;
    private String emergencyContact;
    private String statusEmergency;
    private String alamatEmergency;
    private String telpEmergency;
    
    private String idProject;
    private String divisi;
    private String image;
    private String fotoKTP;
    private String fotoNPWP;
    private String fotoKK;
    private String fotoMI;
    private FormFile upload;
    private FormFile uploadKTP;
    private FormFile uploadNPWP;
    private FormFile uploadKK;
    private FormFile uploadMI;
    private String isLeader;

    private List<KaryawanForm> listKaryawan;
    private KaryawanSearch search = new KaryawanSearch();
    
    public KaryawanForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public KaryawanSearch getSearch() {
		return search;
	}

	public void setSearch(KaryawanSearch search) {
		this.search = search;
	}


	public KaryawanBean toBean() {
		KaryawanBean bean = new KaryawanBean();
	        BeanConverter.convertBeanFromString(this, bean);
	        return bean;
	    }

	

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTempatLahir() {
		return tempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}

	public String getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getAgama() {
		return agama;
	}

	public void setAgama(String agama) {
		this.agama = agama;
	}

	public String getKewarganegaraan() {
		return kewarganegaraan;
	}

	public void setKewarganegaraan(String kewarganegaraan) {
		this.kewarganegaraan = kewarganegaraan;
	}

	public String getKodePos() {
		return kodePos;
	}

	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}

	public String getNoHP() {
		return noHP;
	}

	public void setNoHP(String noHP) {
		this.noHP = noHP;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNoRek() {
		return noRek;
	}

	public void setNoRek(String noRek) {
		this.noRek = noRek;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getJenjangPendidikan() {
		return jenjangPendidikan;
	}

	public void setJenjangPendidikan(String jenjangPendidikan) {
		this.jenjangPendidikan = jenjangPendidikan;
	}

	public String getAlamatKtp() {
		return alamatKtp;
	}

	public void setAlamatKtp(String alamatKtp) {
		this.alamatKtp = alamatKtp;
	}

	public String getDtmUpd() {
		return dtmUpd;
	}

	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}

	public String getTanggalBergabung() {
		return tanggalBergabung;
	}

	public void setTanggalBergabung(String tanggalBergabung) {
		this.tanggalBergabung = tanggalBergabung;
	}

	public String getAlamatSekarang() {
		return alamatSekarang;
	}

	public void setAlamatSekarang(String alamatSekarang) {
		this.alamatSekarang = alamatSekarang;
	}

	public String getStatusPerkawinan() {
		return statusPerkawinan;
	}

	public void setStatusPerkawinan(String statusPerkawinan) {
		this.statusPerkawinan = statusPerkawinan;
	}

	public String getGolonganDarah() {
		return golonganDarah;
	}

	public void setGolonganDarah(String golonganDarah) {
		this.golonganDarah = golonganDarah;
	}

	public String getNomorKtp() {
		return nomorKtp;
	}

	public void setNomorKtp(String nomorKtp) {
		this.nomorKtp = nomorKtp;
	}

	public String getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(String emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public String getStatusEmergency() {
		return statusEmergency;
	}

	public void setStatusEmergency(String statusEmergency) {
		this.statusEmergency = statusEmergency;
	}

	public String getAlamatEmergency() {
		return alamatEmergency;
	}

	public void setAlamatEmergency(String alamatEmergency) {
		this.alamatEmergency = alamatEmergency;
	}

	public String getTelpEmergency() {
		return telpEmergency;
	}

	public void setTelpEmergency(String telpEmergency) {
		this.telpEmergency = telpEmergency;
	}

	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
 
	public FormFile getUpload() {
		return upload;
	}

	public void setUpload(FormFile upload) {
		this.upload = upload;
	}

	public String getDivisi() {
		return divisi;
	}

	public void setDivisi(String divisi) {
		this.divisi = divisi;
	}
	
	public List<KaryawanForm> getListKaryawan() {
        return listKaryawan;
    }

    public void setListKaryawan(List<KaryawanForm> listKaryawan) {
        this.listKaryawan = listKaryawan;
    }

	public String getFotoKTP() {
		return fotoKTP;
	}

	public void setFotoKTP(String fotoKTP) {
		this.fotoKTP = fotoKTP;
	}

	public String getFotoNPWP() {
		return fotoNPWP;
	}

	public void setFotoNPWP(String fotoNPWP) {
		this.fotoNPWP = fotoNPWP;
	}

	public String getFotoKK() {
		return fotoKK;
	}

	public void setFotoKK(String fotoKK) {
		this.fotoKK = fotoKK;
	}

	public String getFotoMI() {
		return fotoMI;
	}

	public void setFotoMI(String fotoMI) {
		this.fotoMI = fotoMI;
	}

	public FormFile getUploadKTP() {
		return uploadKTP;
	}

	public void setUploadKTP(FormFile uploadKTP) {
		this.uploadKTP = uploadKTP;
	}

	public FormFile getUploadNPWP() {
		return uploadNPWP;
	}

	public void setUploadNPWP(FormFile uploadNPWP) {
		this.uploadNPWP = uploadNPWP;
	}

	public FormFile getUploadKK() {
		return uploadKK;
	}

	public void setUploadKK(FormFile uploadKK) {
		this.uploadKK = uploadKK;
	}

	public FormFile getUploadMI() {
		return uploadMI;
	}

	public void setUploadMI(FormFile uploadMI) {
		this.uploadMI = uploadMI;
	}

	public String getIsLeader() {
		return isLeader;
	}

	public void setIsLeader(String isLeader) {
		this.isLeader = isLeader;
	}

	
	
}
