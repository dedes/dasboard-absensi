package treemas.application.feature.master.karyawan;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import treemas.base.servlet.ServletBase;
import treemas.util.log.ILogger;


public class ReportServletKaryawan extends ServletBase {
private KaryawanManager manager = new KaryawanManager();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		KaryawanForm frm = new KaryawanForm();
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy") ;
		String timeStamp = dateFormat.format(new Date());
		
		String nik = req.getParameter("nik");
		String nama = req.getParameter("nama");
		
        HashMap hm = new HashMap();
        
        
        // Create an Exporter
        	try {
				String filePath = getServletContext().getRealPath("/report/excel/template_audit.xls");
				FileInputStream fileIn = new FileInputStream(filePath);
				POIFSFileSystem myFileSystem = new POIFSFileSystem(fileIn);
				HSSFWorkbook workbook = new HSSFWorkbook(myFileSystem);
				HSSFSheet sheet = workbook.getSheetAt(0);			
				HSSFRow row = sheet.createRow((short)0);
		
				sheet.setColumnWidth(0, 5000);
		        sheet.setColumnWidth(1, 10000);
		        sheet.setColumnWidth(2, 5000);
		        sheet.setColumnWidth(3, 10000);
		        sheet.setColumnWidth(4, 10000);
		        sheet.setColumnWidth(6, 5000);
		        sheet.setColumnWidth(7, 5000);
		        sheet.setColumnWidth(8, 5000);
		        sheet.setColumnWidth(10, 5000);
		        sheet.setColumnWidth(11, 5000);
		        sheet.setColumnWidth(12, 5000);
		        sheet.setColumnWidth(13, 5000);
		        sheet.setColumnWidth(14, 5000);
		        sheet.setColumnWidth(15, 5000);
		        sheet.setColumnWidth(16, 5000);
		        sheet.setColumnWidth(17, 5000);
		        sheet.setColumnWidth(18, 5000);
		        sheet.setColumnWidth(19, 5000);
		        sheet.setColumnWidth(20, 5000);
		        sheet.setColumnWidth(21, 5000);
		        sheet.setColumnWidth(22, 5000);
		        sheet.setColumnWidth(23, 5000);
		        sheet.setColumnWidth(24, 5000);
		        
		        row.createCell(0).setCellValue("NIK");
				row.createCell(1).setCellValue("NAMA");
				row.createCell(2).setCellValue("TANGGAL BERGABUNG");
				row.createCell(3).setCellValue("ALAMAT KTP");
				row.createCell(4).setCellValue("ALAMAT SEKARANG");
				row.createCell(5).setCellValue("KODE POS");
				row.createCell(6).setCellValue("TEMPAT LAHIR");
				row.createCell(7).setCellValue("TANGGAL LAHIR");
				row.createCell(8).setCellValue("JENIS KELAMIN");
				row.createCell(9).setCellValue("GOLONGAN DARAH");
				row.createCell(10).setCellValue("STATUS PERKAWINAN");
				row.createCell(11).setCellValue("AGAMA");
				row.createCell(12).setCellValue("KEWARGANEGARAAN");
				row.createCell(13).setCellValue("JENJANG PENDIDIKAN");
				row.createCell(14).setCellValue("NO KTP");
				row.createCell(15).setCellValue("NO NPWP");
				row.createCell(16).setCellValue("NO HP");
				row.createCell(17).setCellValue("EMAIL");
				row.createCell(18).setCellValue("IMEI");
				row.createCell(19).setCellValue("EMERGENCY CONTACT");
				row.createCell(20).setCellValue("STATUS EMERGENCY");
				row.createCell(21).setCellValue("ALAMAT EMERGENCY");
				row.createCell(22).setCellValue("TELP EMERGENCY");
				row.createCell(23).setCellValue("NOREK");
				row.createCell(24).setCellValue("ID PROJECT");
				row.createCell(25).setCellValue("DIVISI");
				
				
				
					List<KaryawanBean> list = this.manager.getAllAuditKaryawan(nik, nama);
					if (list.size() != 0) {
					int i = 1;
			        for (KaryawanBean bean: list) {
			        	HSSFRow rows = sheet.createRow(i);
			        	rows.createCell(0).setCellValue(bean.getNik());
			        	rows.createCell(1).setCellValue(bean.getNama());
			        	
			        	String dtb = bean.getTanggalBergabung();
		        		rows.createCell(2).setCellValue(dtb);
		        		
		        		rows.createCell(3).setCellValue(bean.getAlamatKtp());
		        		rows.createCell(4).setCellValue(bean.getAlamatSekarang());
		        		rows.createCell(5).setCellValue(bean.getKodePos());
		        		rows.createCell(6).setCellValue(bean.getTempatLahir());
		        		
			        	String dt = bean.getTanggalLahir();
			        	rows.createCell(7).setCellValue(dt);
			        	
			        	rows.createCell(8).setCellValue(bean.getJenisKelamin());
			        	rows.createCell(9).setCellValue(bean.getGolonganDarah());
			        	rows.createCell(10).setCellValue(bean.getStatusPerkawinan());
			        	rows.createCell(11).setCellValue(bean.getAgama());
			        	rows.createCell(12).setCellValue(bean.getKewarganegaraan());
			        	rows.createCell(13).setCellValue(bean.getJenjangPendidikan());
			        	rows.createCell(14).setCellValue(bean.getNomorKtp());
			        	rows.createCell(15).setCellValue(bean.getNpwp());
			        	rows.createCell(16).setCellValue(bean.getNoHP());
			        	rows.createCell(17).setCellValue(bean.getEmail());
			        	rows.createCell(18).setCellValue(bean.getImei());
			        	rows.createCell(19).setCellValue(bean.getEmergencyContact());
			        	rows.createCell(20).setCellValue(bean.getStatusEmergency());
			        	rows.createCell(21).setCellValue(bean.getAlamatEmergency());
			        	rows.createCell(22).setCellValue(bean.getTelpEmergency());
			        	rows.createCell(23).setCellValue(bean.getNoRek());
			        	rows.createCell(24).setCellValue(bean.getIdProject());
			        	rows.createCell(25).setCellValue(bean.getDivisi());
			        	
			        	i++;
			        	
			        }
					}
					
						fileIn.close();	 
						
						FileOutputStream fileOut = new FileOutputStream(filePath);
						workbook.write(fileOut);
						fileOut.close();

						resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
						resp.setHeader("Content-Disposition", "attachment; filename=detail_Karyawan"+timeStamp+".xls");
						resp.setHeader("Cache-Control", "public");
						resp.setHeader("Pragma", "public");
						resp.setHeader("CookiesConfigureNoCache", "false");	
					
						OutputStream outputStream = resp.getOutputStream();
						workbook.write(outputStream);
						
						outputStream.flush();
						outputStream.close();
						this.logger.log(ILogger.LEVEL_INFO, "Your excel file has been generated!");

					} catch ( Exception ex ) {
						System.out.println(ex);
					}
        }
	}
	


        	
        
        

