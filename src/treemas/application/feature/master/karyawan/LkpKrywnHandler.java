package treemas.application.feature.master.karyawan;

import com.google.common.base.Strings;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;/*
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;*/
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.encoder.Base64;
import treemas.util.format.TreemasFormatter;
import treemas.util.geofence.google.Southwest;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class LkpKrywnHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private LkpKrywnManager manager = new LkpKrywnManager();
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	KaryawanForm karyawanForm = (KaryawanForm) form;
    	karyawanForm.getPaging().calculationPage();
        String action = karyawanForm.getTask();
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListkaryawan(request, karyawanForm);
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListkaryawan(HttpServletRequest request, KaryawanForm form)
    throws CoreException {
    	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAllKaryawan(pageNumber, form.getSearch(),this.getLoginId(request));
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listKaryawan", list);
     
    }
    
    private KaryawanBean convertToBean(KaryawanForm form, String updateUser, int flag) throws AppException {
    	KaryawanBean bean = form.toBean();
        return bean;
    }
    
        /*KaryawanForm frm = (KaryawanForm) form;
        String task = frm.getTask();
        int page = frm.getPaging().getCurrentPageNo();
        frm.getPaging().calculationPage();

        if (!Strings.isNullOrEmpty(frm.getNik())) {
            String[] arrayGroup = frm.getNik().split(Global.ARRAY_COMMA_DELIMITER);
            String[] newArrayGroup = new String[arrayGroup.length];
            for (int i = 0; i < arrayGroup.length; i++) {
                String string = arrayGroup[i];
                if (!Strings.isNullOrEmpty(string)) {
                    if (string.startsWith(Global.SPACE_DELIMITER)) {
                        string = string.replaceFirst(Global.SPACE_DELIMITER, "");
                    }
                }
                newArrayGroup[i] = string;
            }
            frm.getSearch().setMemberList(newArrayGroup);
        }

        if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)
                || Global.WEB_TASK_SHOW.equals(task)) {
            this.load(request, frm);
        } else if (Global.WEB_TASK_DETAIL.equals(task)) {
            this.loadDetail(request, frm);
        }

        return this.getNextPage(task, mapping);

    }

    private void loadAppAdmin(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getAppAdmin(param);
        request.setAttribute("listComboApp", list);
    }

    private void loadAppGroup(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getAppGroup(param);
        request.setAttribute("listComboGroup", list);
    }

    private void loadPaging(HttpServletRequest request, KaryawanForm form) throws AppException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;

        PageListWrapper result = this.manager.getAllLkpKrywnPaging(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listKaryawan", list);

        this.loadAppAdmin(request);
        this.loadAppGroup(request);
    }

    private void loadDetail(HttpServletRequest request, KaryawanForm form) throws AppException {
        KaryawanBean bean = this.manager.getHeaderDetailLkpKrywn(form.getNik());
        form.fromBean(bean);
        List list = this.manager.getDetailLkpKrywn(form.getNik());
        BeanConverter.convertBeanToString(list, form.getClass());
        request.setAttribute("listKaryawan", list);
    }

    private void load(HttpServletRequest request, KaryawanForm form) throws AppException {
        List list = this.manager.getAllLkpKrywn(form.getSearch());
        list = BeanConverter.convertBeansToString(list, form.getClass());
        List listReturn = new ArrayList();
        for (Object o : list) {
        	KaryawanForm singleForm = (KaryawanForm) o;
            this.combineDetail(singleForm);
            listReturn.add(singleForm);
        }
//		System.out.println(listReturn.size());
//		listReturn = BeanConverter.convertBeansToString(listReturn, form.getClass());
        request.setAttribute("listKaryawan", listReturn);
    }

    private void combineDetail(KaryawanForm form) throws AppException {
        List list = this.manager.getDetailLkpKrywn(form.getNik());
        BeanConverter.convertBeanToString(list, form.getClass());
        form.setListKaryawan(list);
    }*/

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((KaryawanForm) form).setTask(((KaryawanForm) form).resolvePreviousTask());

        String task = ((KaryawanForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
