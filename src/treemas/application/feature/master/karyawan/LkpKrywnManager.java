package treemas.application.feature.master.karyawan;

import treemas.application.feature.master.reimburse.ReimburseSearch;
import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class LkpKrywnManager extends SetUpManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_LOOKUP_KARYAWAN;


    public List getAllLkpKrywn(KaryawanSearch search) throws AppException {
        List list = null;
        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllLkpKrywnNotPaging", search);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return list;
    }

    public PageListWrapper getAllLkpKrywnPaging(int pageNumber, KaryawanSearch search) throws AppException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllLkpKrywn", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllLkpKrywn", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public List getDetailLkpKrywn(String nik) throws AppException {
        List list = null;

        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getDetailLkpKrywn", nik);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return list;
    }

    public KaryawanBean getHeaderDetailLkpKrywn(String nik) throws AppException {
        KaryawanBean result = null;

        try {
            result = (KaryawanBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getHeaderDetailLkpKrywn", nik);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }
    
    public PageListWrapper getAllKaryawan(int pageNumber, KaryawanSearch search, String loginId) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);
        String nik = loginId;
        search.setNik(nik);
        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "get", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllLkpKrywn", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

}
