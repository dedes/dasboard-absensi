package treemas.application.feature.master.karyawan;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;


import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/*TODO
 * COMMIT THIS*/
public class KaryawanValidator extends Validator {
    public KaryawanValidator(HttpServletRequest request) {
        super(request);
    }

    public KaryawanValidator(Locale locale) {
        super(locale);
    }
 
    public boolean validateKaryawanInsert(ActionMessages messages, KaryawanForm form, KaryawanManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        
        try {
        	KaryawanBean bean = manager.getSingleKaryawan(form.getNik());
            if (null != bean) {
                messages.add("nik", new ActionMessage("errors.duplicate", "nik" + form.getNik()));
            }
            
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
        
        this.textValidator(messages, form.getNik(), "common.id.nik", "nik", "1", this.getStringLength(6), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getNama(), "common.namakary", "nama", "1", this.getStringLength(500), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getNama(), "common.imei", "imei", "1", this.getStringLength(30), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
   
    public boolean validateKaryawanEdit(ActionMessages messages, KaryawanForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages(); 
        
        this.textValidator(messages, form.getNama(), "common.namakary", "nama", "1", this.getStringLength(500), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getNama(), "common.imei", "imei", "1", this.getStringLength(30), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }      
   }


