package treemas.application.feature.master.karyawan;



import treemas.application.feature.form.scheme.SchemeFormBean;
import treemas.base.feature.FeatureManagerBase;
import treemas.base.feature.SequenceManager;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.auditrail.AuditTrailException;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.databases.SQLErrorCode;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.log.ILogger;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.birt.report.model.api.core.DisposeEvent;


public class KaryawanManager extends FeatureManagerBase {
    static final int SEARCH_BY_QUESTION_OR_ANSWER_LABEL = 10;
    /*TODO
     * COMMIT THIS IBATIS*/
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_KARYAWAN;

    public PageListWrapper getAllKaryawan(int pageNumber, KaryawanSearch search) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAll", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "cntAll", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }
    
 public List<KaryawanBean> getAllAuditKaryawan(String nik, String nama) throws AppException {
	 	KaryawanSearch search = new KaryawanSearch();
		search.setNik(nik);
		search.setNama(nama);
		

		List result = null;
		try {
			this.logger.log(ILogger.LEVEL_INFO, "Execute query Get Download Karyawan");
			result = this.ibatisSqlMap.queryForList(STRING_SQL + "getDownloadKaryawan", search);
	         this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "Execute query Get Download Karyawan Report : "+search);
			 this.logger.log(ILogger.LEVEL_INFO, "Execute query Get Download Karyawan Sukses");
		} catch (SQLException sqle) {
			this.logger.printStackTrace(sqle);
			throw new AppException(sqle, ERROR_DB);
		}
		return result;
	}
 
 	public KaryawanBean getSingleKaryawan(String nik) throws CoreException {
 	KaryawanBean result = null;
 	 try {
 		 result = (KaryawanBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "get", nik);
      } catch (SQLException e) {
          this.logger.printStackTrace(e);
          throw new CoreException(ERROR_DB);
      }

      return result;
  }
 
    public void insertKaryawan(KaryawanBean Bean) throws CoreException {
    	try {
		try {
			this.ibatisSqlMap.startTransaction();
			this.logger.log(ILogger.LEVEL_INFO, "Inserting Karyawan " + Bean.getNik());
			this.ibatisSqlMap.insert(this.STRING_SQL + "insert", Bean);
			
			Map paramMap = new HashMap();
			paramMap.put("userId", Bean.getNik());
			paramMap.put("active", "1");
			paramMap.put("password", "827CCB0EEA8A706C4C34A16891F84E7B");
			paramMap.put("isLogin", "0");
			paramMap.put("imei", Bean.getImei());
			paramMap.put("passEd", "0");
			paramMap.put("isLocked", "0");
			paramMap.put("timesLocked", "0");
			paramMap.put("userName", Bean.getNama());
			paramMap.put("flagPass", "0");
			if (Bean.getIsLeader().equals("0")){
				paramMap.put("isMobile", "0");
			}else{
				paramMap.put("isMobile", "1");
			}
			paramMap.put("jobDesc", "US");
			paramMap.put("wrongPass", "0");
			paramMap.put("branchID", "000");
			this.ibatisSqlMap.insert(this.STRING_SQL + "insertUserMob", paramMap);
			this.ibatisSqlMap.insert(this.STRING_SQL + "insertUserDash", paramMap);
			this.ibatisSqlMap.commitTransaction();
			this.logger.log(ILogger.LEVEL_INFO, "Inserted Karyawan "+ Bean.getNama());
			this.logger.log(ILogger.LEVEL_INFO, "Inserted User Mob "+ Bean.getNama());
			this.logger.log(ILogger.LEVEL_INFO, "Inserted User Dashbord "+ Bean.getNama());
			
		} finally {
			this.ibatisSqlMap.endTransaction();
		}
		} catch (SQLException sqle) {
			this.logger.printStackTrace(sqle);
			if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY || sqle.getErrorCode() == SQLErrorCode.ERROR_DUPLICATE_KEY) {
			throw new CoreException("", sqle, ERROR_DATA_EXISTS, Bean
			       .getNik());
		} else {
			throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
		}
		} catch (Exception e) {
			this.logger.printStackTrace(e);
			throw new CoreException(e, ERROR_UNKNOWN);
		}
	}
    
    public void updateKaryawan(KaryawanBean bean) throws CoreException {
		try {
		try {
			this.ibatisSqlMap.startTransaction();
			this.logger.log(ILogger.LEVEL_INFO, "Updating Customer" + bean.getNik());
			this.ibatisSqlMap.update(this.STRING_SQL + "update",bean);	
			this.ibatisSqlMap.update(this.STRING_SQL + "updateMob",bean);
			this.ibatisSqlMap.commitTransaction();
			this.logger.log(ILogger.LEVEL_INFO, "Updated Customer" + bean.getNik());
		} finally {
			this.ibatisSqlMap.endTransaction();
		}
		} catch (SQLException se) {
			this.logger.printStackTrace(se);
			throw new CoreException(se, ERROR_DB);
		} catch (Exception e) {
			this.logger.printStackTrace(e);
			throw new CoreException(e, ERROR_UNKNOWN);
		}
    }
    
    public void deleteKaryawan(String nik)throws CoreException {
    	KaryawanBean bean = new KaryawanBean();
        bean.setNik(nik);
	
		try {
		    try {
		        this.ibatisSqlMap.startTransaction();
		        this.logger.log(ILogger.LEVEL_INFO, "Deleting Karyawan" + nik);
		        this.ibatisSqlMap.delete(this.STRING_SQL + "delete", bean);
		        this.ibatisSqlMap.delete(this.STRING_SQL + "deleteMob", bean);
		        this.ibatisSqlMap.commitTransaction();
		
		        this.logger.log(ILogger.LEVEL_INFO, "Deleted Karyawan" + nik);
		    } finally {
		        this.ibatisSqlMap.endTransaction();
		    }
		} catch (SQLException sqle) {
		    this.logger.printStackTrace(sqle);
		    if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND || sqle.getErrorCode() == SQLErrorCode.ERROR_CHILD_FOUND) {
		        throw new CoreException("", sqle, ERROR_HAS_CHILD, nik);
		
		    } else {
		        throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
		    }
		} catch (Exception e) {
		    this.logger.printStackTrace(e);
		    throw new CoreException(e, ERROR_UNKNOWN);
		}
	}
    
}
