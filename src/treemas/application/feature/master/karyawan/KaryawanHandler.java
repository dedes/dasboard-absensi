package treemas.application.feature.master.karyawan;

import com.google.common.base.Strings;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;/*
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;*/
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


import treemas.application.feature.form.scheme.SchemeFormBean;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.feature.FeatureActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.encoder.Base64;
import treemas.util.format.TreemasFormatter;
import treemas.util.geofence.google.Southwest;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;
import treemas.util.struts.PagingInfo;
import treemas.util.tool.ConfigurationProperties;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class KaryawanHandler extends FeatureActionBase {
	MultipleManager multipleManager = new MultipleManager();
    private KaryawanManager manager = new KaryawanManager();
    private KaryawanValidator validator;
    
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        
    	KaryawanForm karyawanForm = (KaryawanForm) form;
    	karyawanForm.getPaging().calculationPage();
        String action = karyawanForm.getTask();
        String nik = karyawanForm.getNik();
        this.validator = new KaryawanValidator(request);
        
        
       
        if (Global.WEB_TASK_LOAD.equals(action) || 
        		Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadListkaryawan(request, karyawanForm);
        } else if (Global.WEB_TASK_ADD.equals(action)) {
        	karyawanForm.setNik("");
        	karyawanForm.setNama("");
       
        } else if (Global.WEB_TASK_INSERT.equals(action)) {
            if (this.validator.validateKaryawanInsert(new ActionMessages(), karyawanForm, this.manager)) {
					this.insertKaryawan(request, karyawanForm);
					this.loadListkaryawan(request, karyawanForm);
            }	
            
        } else if (Global.WEB_TASK_EDIT.equals(action)) {
        	KaryawanBean bean = this.manager.getSingleKaryawan(nik);
        	karyawanForm.fromBean(bean);
        	
//        	this.loadHeaderDropDown(request);
        	karyawanForm.getPaging().setDispatch(null);

        } else if (Global.WEB_TASK_UPDATE.equals(action)) {
           if (this.validator.validateKaryawanEdit(new ActionMessages(), karyawanForm)) {

                KaryawanBean bean = new KaryawanBean();
                karyawanForm.toBean(bean);

                this.updateKaryawan(request, karyawanForm);
                this.loadListkaryawan(request, karyawanForm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
               
           }
        
        } else if (Global.WEB_TASK_DELETE.equals(action)) {
            PagingInfo page = karyawanForm.getPaging();
            try {
                this.manager.deleteKaryawan(karyawanForm.getNik());
                int totalAfterDelete = page.getTotalRecord() - 1;
                if (totalAfterDelete % Global.ROW_PER_PAGE == 0) {
                    if (page.getCurrentPageNo() == 0) {
                        page.setCurrentPageNo(1);
                    } else {
                        page.setCurrentPageNo(page.getCurrentPageNo() - 1);
                    }
                }
            } finally {
            	this.loadListkaryawan(request, karyawanForm);
                page.setDispatch(null);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadListkaryawan(HttpServletRequest request, KaryawanForm form)
    throws CoreException {
    	
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAllKaryawan(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listKaryawan", list);
     
    }
    
//    private void loadHeaderDropDown(HttpServletRequest request) throws CoreException {
//        MultipleParameterBean bean = new MultipleParameterBean();
//        List list = this.multipleManager.getListHeader(bean);
//        request.setAttribute("listHeader", list);
//    }
    
 
    private void insertKaryawan(HttpServletRequest request, KaryawanForm form) throws AppException {
    	KaryawanBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	/*String filePath = ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_KARYAWAN_PATH);
    	String filePath2 = ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_KTP_PATH);
    	String filePath3 = ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_NPWP_PATH);
    	String filePath4 = ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_KK_PATH);
    	String filePath5 = ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_MI_PATH);
    	String fileName = form.getUpload().getFileName();
    	File file = new File(fileName);
    	String[] exts = file.getPath().split("\\.");
    	String ext = exts[exts.length-1];
    	try {
    		BufferedImage bi = ImageIO.read(form.getUpload().getInputStream());
    		BufferedImage biktp = ImageIO.read(form.getUploadKTP().getInputStream());
    		BufferedImage binpwp = ImageIO.read(form.getUploadNPWP().getInputStream());
    		BufferedImage bikk = ImageIO.read(form.getUploadKK().getInputStream());
    		BufferedImage bimi = ImageIO.read(form.getUploadMI().getInputStream());
    	
//    		System.out.print("path: " + file.getAbsolutePath());
    		File outputfile = new File(filePath + form.getNik() + "." + ext);
    		if(ImageIO.write(bi, ext, outputfile)){
    			System.out.println("File is upload successful!");
		    }else{
	    		System.out.println("File is failed to upload!");
		    }
    		
    		File outputfile2 = new File(filePath2 + form.getUploadKTP().getFileName());
    		if(ImageIO.write(biktp, ext, outputfile2)){
    			System.out.println("File is upload successful!");
		    }else{
	    		System.out.println("File is failed to upload!");
		    }
    		
    		File outputfile3 = new File(filePath3 + form.getUploadNPWP().getFileName());
    		if(ImageIO.write(binpwp, ext, outputfile3)){
    			System.out.println("File is upload successful!");
		    }else{
	    		System.out.println("File is failed to upload!");
		    }
    		
    		File outputfile4 = new File(filePath4 + form.getUploadKK().getFileName());
    		if(ImageIO.write(bikk, ext, outputfile4)){
    			System.out.println("File is upload successful!");
		    }else{
	    		System.out.println("File is failed to upload!");
		    }
    		
    		File outputfile5 = new File(filePath5 + form.getUploadMI().getFileName());
    		if(ImageIO.write(bimi, ext, outputfile5)){
    			System.out.println("File is upload successful!");
		    }else{
	    		System.out.println("File is failed to upload!");
		    }
    		if(file.renameTo(new File("D:\\Treemas\\" + file.getName()))){
        		System.out.println("File is moved successful!");
    	    }else{
        		System.out.println("File is failed to move!");
    	    }
    	} catch (Exception e) {
    		e.printStackTrace();
    		System.out.println("Write error for " + file.getPath() + ": " + e.getMessage());
    	}*/
		String dates = form.getTanggalLahir();
		String dates2 = form.getTanggalBergabung();
		bean.setTanggalLahir(dates);
		bean.setTanggalBergabung(dates2);
		/*bean.setImage(form.getNik() + "." + ext);
		bean.setFotoKTP(form.getUploadKTP().getFileName());
		bean.setFotoNPWP(form.getUploadNPWP().getFileName());
		bean.setFotoKK(form.getUploadKK().getFileName());
		bean.setFotoMI(form.getUploadMI().getFileName());*/
		
	
		this.manager.insertKaryawan(bean);
		
    }
    
    private void updateKaryawan(HttpServletRequest request, KaryawanForm form) throws AppException {
    	KaryawanBean bean = this.convertToBean(form, this.getLoginId(request), 1);
    	/*String filePath = ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_KARYAWAN_PATH);
    	String filePath2 = ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_KTP_PATH);
    	String filePath3 = ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_NPWP_PATH);
    	String filePath4 = ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_KK_PATH);
    	String filePath5 = ConfigurationProperties.getInstance().get(PropertiesKey.IMAGE_MI_PATH);
    	String fileName = form.getUpload().getFileName();
    	File file = new File(fileName);
    	String[] exts = file.getPath().split("\\.");
    	String ext = exts[exts.length-1];
    	try {
    		BufferedImage bi = ImageIO.read(form.getUpload().getInputStream());
    		BufferedImage biktp = ImageIO.read(form.getUploadKTP().getInputStream());
    		BufferedImage binpwp = ImageIO.read(form.getUploadNPWP().getInputStream());
    		BufferedImage bikk = ImageIO.read(form.getUploadKK().getInputStream());
    		BufferedImage bimi = ImageIO.read(form.getUploadMI().getInputStream());
    	
//    		System.out.print("path: " + file.getAbsolutePath());
    		File outputfile = new File(filePath + form.getNik() + "." + ext);
    		if(ImageIO.write(bi, ext, outputfile)){
    			System.out.println("File is upload successful!");
		    }else{
	    		System.out.println("File is failed to upload!");
		    }
    		
    		File outputfile2 = new File(filePath2 + form.getUploadKTP().getFileName());
    		if(ImageIO.write(biktp, ext, outputfile2)){
    			System.out.println("File is upload successful!");
		    }else{
	    		System.out.println("File is failed to upload!");
		    }
    		
    		File outputfile3 = new File(filePath3 + form.getUploadNPWP().getFileName());
    		if(ImageIO.write(binpwp, ext, outputfile3)){
    			System.out.println("File is upload successful!");
		    }else{
	    		System.out.println("File is failed to upload!");
		    }
    		
    		File outputfile4 = new File(filePath4 + form.getUploadKK().getFileName());
    		if(ImageIO.write(bikk, ext, outputfile4)){
    			System.out.println("File is upload successful!");
		    }else{
	    		System.out.println("File is failed to upload!");
		    }
    		
    		File outputfile5 = new File(filePath5 + form.getUploadMI().getFileName());
    		if(ImageIO.write(bimi, ext, outputfile5)){
    			System.out.println("File is upload successful!");
		    }else{
	    		System.out.println("File is failed to upload!");
		    }
    		if(file.renameTo(new File("D:\\Treemas\\" + file.getName()))){
        		System.out.println("File is moved successful!");
    	    }else{
        		System.out.println("File is failed to move!");
    	    }
    	} catch (Exception e) {
    		e.printStackTrace();
    		System.out.println("Write error for " + file.getPath() + ": " + e.getMessage());
    	}*/
		String dates = form.getTanggalLahir();
		String dates2 = form.getTanggalBergabung();
		bean.setTanggalLahir(dates);
		bean.setTanggalBergabung(dates2);
		/*bean.setImage(form.getNik() + "." + ext);
		bean.setFotoKTP(form.getUploadKTP().getFileName());
		bean.setFotoNPWP(form.getUploadNPWP().getFileName());
		bean.setFotoKK(form.getUploadKK().getFileName());
		bean.setFotoMI(form.getUploadMI().getFileName());*/
		
		this.manager.updateKaryawan(bean);
		
    }
    
   
    
    private KaryawanBean convertToBean(KaryawanForm form, String updateUser, int flag) throws AppException {
    	KaryawanBean bean = form.toBean();
        return bean;
    }
    
    public ActionForward handleException(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
		msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			KaryawanForm karyawanForm = (KaryawanForm) form;
		try {
			this.loadListkaryawan(request, karyawanForm);
//			this.loadHeaderDropDown(request);
		} catch (AppException e) {
		//this.logger.printStackTrace(e);
		}
		this.saveErrors(request, msgs);
		break;
		default:
		super.handleException(mapping, form, request, response, ex);
		break;
		}
		
		((KaryawanForm) form).setTask(((KaryawanForm) form).resolvePreviousTask());
		
		String task = ((KaryawanForm) form).getTask();
		return this.getErrorPage(task, mapping);
		    }
}
