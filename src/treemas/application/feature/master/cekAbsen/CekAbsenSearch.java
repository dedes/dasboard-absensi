package treemas.application.feature.master.cekAbsen;

import treemas.base.search.SearchForm;

public class CekAbsenSearch extends SearchForm {
	private String nik;
	private String nama;
	private String idProject;
    private String tgl;
   
    public CekAbsenSearch() {
    }

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTgl() {
		return tgl;
	}

	public void setTgl(String tgl) {
		this.tgl = tgl;
	}

	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	
	
}
