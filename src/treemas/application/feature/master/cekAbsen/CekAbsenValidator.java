package treemas.application.feature.master.cekAbsen;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;


import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class CekAbsenValidator extends Validator {
	public CekAbsenValidator(HttpServletRequest request) {
        super(request);
    }

    public CekAbsenValidator(Locale locale) {
        super(locale);
    }
 
}
