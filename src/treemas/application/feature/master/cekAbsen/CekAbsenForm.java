package treemas.application.feature.master.cekAbsen;



import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.google.common.base.Strings;

import treemas.application.user.UserBean;
import treemas.application.user.UserSearch;
import treemas.base.feature.FeatureFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;

public class CekAbsenForm extends FeatureFormBase {
	private String nik;
	private String nama;
    private String idProject;
    private String tgl;
	
   
    private CekAbsenSearch search = new CekAbsenSearch();
    
    public CekAbsenForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public CekAbsenSearch getSearch() {
		return search;
	}

	public void setSearch(CekAbsenSearch search) {
		this.search = search;
	}


	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	 public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTgl() {
		return tgl;
	}

	public void setTgl(String tgl) {
		this.tgl = tgl;
	}

	
	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	
}
