package treemas.application.feature.inquiry.locationhistory;

import java.util.Date;

public class LocationHistoryBean implements java.io.Serializable {
    private Integer seqNo;
    private Double latitude;
    private Double longitude;
    private Integer MCC;
    private Integer MNC;
    private Integer LAC;
    private Integer cellId;
    private Date timestamp;
    private String isGps;
    private Integer accuracy;
    private String isTask;

    private Double southBound;
    private Double westBound;
    private Double northBound;
    private Double eastBound;
    private String provider;
    private String flag;


    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getMCC() {
        return MCC;
    }

    public void setMCC(Integer mcc) {
        MCC = mcc;
    }

    public Integer getMNC() {
        return MNC;
    }

    public void setMNC(Integer mnc) {
        MNC = mnc;
    }

    public Integer getLAC() {
        return LAC;
    }

    public void setLAC(Integer lac) {
        LAC = lac;
    }

    public Integer getCellId() {
        return cellId;
    }

    public void setCellId(Integer cellId) {
        this.cellId = cellId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getIsGps() {
        return isGps;
    }

    public void setIsGps(String isGps) {
        this.isGps = isGps;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public Double getSouthBound() {
        return southBound;
    }

    public void setSouthBound(Double southBound) {
        this.southBound = southBound;
    }

    public Double getWestBound() {
        return westBound;
    }

    public void setWestBound(Double westBound) {
        this.westBound = westBound;
    }

    public Double getNorthBound() {
        return northBound;
    }

    public void setNorthBound(Double northBound) {
        this.northBound = northBound;
    }

    public Double getEastBound() {
        return eastBound;
    }

    public void setEastBound(Double eastBound) {
        this.eastBound = eastBound;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getIsTask() {
        return isTask;
    }

    public void setIsTask(String isTask) {
        this.isTask = isTask;
    }
}
