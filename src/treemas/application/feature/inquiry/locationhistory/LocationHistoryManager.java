package treemas.application.feature.inquiry.locationhistory;

import org.apache.commons.beanutils.BeanUtils;
import treemas.base.inquiry.InquiryManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.geofence.CellIdLookupManager;
import treemas.util.geofence.GeofenceBean;
import treemas.util.log.ILogger;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


public class LocationHistoryManager extends InquiryManagerBase {
    static final int ERROR_NO_HISTORY_FOUND = 100;
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_INQ_LOCATION;
    private static final String FALSE = "0";

    public List getLocationHistory(String name, String dateFrom, String timeFrom,
                                   String dateUntil, String timeUntil, String type) throws CoreException {
        LocationHistorySearch searchParam = new LocationHistorySearch();
        searchParam.setSurveyorId(name);
        searchParam.setDateFrom(dateFrom);
        searchParam.setDateUntil(dateUntil);
        searchParam.setTimeFrom(timeFrom);
        searchParam.setTimeUntil(timeUntil);
        searchParam.setSource(type);
        return this.getLocationHistory(searchParam);
    }


    public List getLocationHistory(LocationHistorySearch searchParam)
            throws CoreException {
        List result = null;

        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", searchParam);

            if (result == null || result.size() == 0) {
                this.logger.log(ILogger.LEVEL_INFO,
                        "No location history found for search parameter: "
                                + BeanUtils.describe(searchParam).toString());
                throw new CoreException(ERROR_NO_HISTORY_FOUND);
            }

            for (Iterator iterator = result.iterator(); iterator.hasNext(); ) {
                LocationHistoryBean bean = (LocationHistoryBean) iterator
                        .next();
                if (FALSE.equals(bean.getIsGps())
                        && FALSE.equals(bean.getFlag())) {
                    boolean missingCoordinate = (bean.getLatitude() == null && bean
                            .getLongitude() == null) ? true : false;

                    if (missingCoordinate) {
                        this.getMissingCoordinate(bean);
                    }
                }
            }

            for (ListIterator iterator = result.listIterator(result.size()); iterator
                    .hasPrevious(); ) {
                LocationHistoryBean bean = (LocationHistoryBean) iterator
                        .previous();
                if (bean.getLatitude() == null && bean.getLongitude() == null)
                    iterator.remove();
            }

            if (result == null || result.size() == 0) {
                this.logger.log(ILogger.LEVEL_INFO,
                        "No location history found for search parameter: "
                                + BeanUtils.describe(searchParam).toString());
                throw new CoreException(ERROR_NO_HISTORY_FOUND);
            }
        } catch (CoreException me) {
            throw me;
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return result;
    }

    public List getTaskLocation(LocationHistorySearch searchParam)
            throws CoreException {
        List result = null;

        try {
            result = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAllSurveyLocation",
                    searchParam);

            if (result == null || result.size() == 0) {
                this.logger.log(ILogger.LEVEL_INFO,
                        "No location history found for search parameter: "
                                + BeanUtils.describe(searchParam).toString());
                throw new CoreException(ERROR_NO_HISTORY_FOUND);
            }

            for (ListIterator iterator = result.listIterator(result.size()); iterator
                    .hasPrevious(); ) {
                LocationHistoryBean bean = (LocationHistoryBean) iterator
                        .previous();
                if (bean.getLatitude() == null && bean.getLongitude() == null)
                    iterator.remove();
            }
        } catch (CoreException me) {
            throw me;
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return result;
    }

    private void getMissingCoordinate(LocationHistoryBean bean)
            throws SQLException {
        // CoordinateBean coordinateBean = CellIdLookup.getCoordinate(
        // bean.getMCC().intValue(), bean.getMNC().intValue(),
        // bean.getLAC().intValue(), bean.getCellId().intValue());

        CellIdLookupManager cidManager = new CellIdLookupManager();
        GeofenceBean coordinateBean = cidManager.geocodeByCellId(bean
                .getMCC().intValue(), bean.getMNC().intValue(), bean.getLAC()
                .intValue(), bean.getCellId().intValue());

        if (coordinateBean != null) {
            bean.setLatitude(new Double(coordinateBean.getLatitude()));
            bean.setLongitude(new Double(coordinateBean.getLongitude()));
            bean.setAccuracy(new Integer(coordinateBean.getAccuracy()));
            bean.setProvider(coordinateBean.getProvider());
            this.ibatisSqlMap.update(
                    this.STRING_SQL + "updateCoordinate", bean);
        } else {
            this.ibatisSqlMap.update(this.STRING_SQL + "updateFlag",
                    bean);
        }
    }
}
