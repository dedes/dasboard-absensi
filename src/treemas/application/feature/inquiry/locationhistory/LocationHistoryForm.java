package treemas.application.feature.inquiry.locationhistory;

import treemas.base.inquiry.InquiryFormBase;

public class LocationHistoryForm extends InquiryFormBase {
    private String seqNo;
    private String latitude;
    private String longitude;
    private Integer MCC;
    private Integer MNC;
    private Integer LAC;
    private Integer cellId;
    private String timestamp;
    private String isGps;
    private String accuracy;
    private String isTask;

    private String southBound;
    private String westBound;
    private String northBound;
    private String eastBound;

    private String listSize;
    private String job;
    private String param;

    private String height;
    private String width;
    private LocationHistorySearch search = new LocationHistorySearch();

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Integer getMCC() {
        return MCC;
    }

    public void setMCC(Integer mcc) {
        MCC = mcc;
    }

    public Integer getMNC() {
        return MNC;
    }

    public void setMNC(Integer mnc) {
        MNC = mnc;
    }

    public Integer getLAC() {
        return LAC;
    }

    public void setLAC(Integer lac) {
        LAC = lac;
    }

    public Integer getCellId() {
        return cellId;
    }

    public void setCellId(Integer cellId) {
        this.cellId = cellId;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getIsGps() {
        return isGps;
    }

    public void setIsGps(String isGps) {
        this.isGps = isGps;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getSouthBound() {
        return southBound;
    }

    public void setSouthBound(String southBound) {
        this.southBound = southBound;
    }

    public String getWestBound() {
        return westBound;
    }

    public void setWestBound(String westBound) {
        this.westBound = westBound;
    }

    public String getNorthBound() {
        return northBound;
    }

    public void setNorthBound(String northBound) {
        this.northBound = northBound;
    }

    public String getEastBound() {
        return eastBound;
    }

    public void setEastBound(String eastBound) {
        this.eastBound = eastBound;
    }

    public String getListSize() {
        return listSize;
    }

    public void setListSize(String listSize) {
        this.listSize = listSize;
    }

    public LocationHistorySearch getSearch() {
        return search;
    }

    public void setSearch(LocationHistorySearch search) {
        this.search = search;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getIsTask() {
        return isTask;
    }

    public void setIsTask(String isTask) {
        this.isTask = isTask;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
}
