package treemas.application.feature.inquiry.locationhistory;

import org.apache.struts.action.ActionMessages;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class LocationHistoryValidator extends Validator {

    public LocationHistoryValidator(HttpServletRequest request) {
        super(request);
    }

    public LocationHistoryValidator(Locale locale) {
        super(locale);
    }

    public boolean validateLocationHistorySearch(ActionMessages messages, LocationHistoryForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        String dateTime1;
        String dateTime2;
        dateTime1 = form.getSearch().getDateFrom() + form.getSearch().getTimeFrom();
        dateTime2 = form.getSearch().getDateUntil() + form.getSearch().getTimeUntil();

        this.textValidator(messages, form.getSearch().getSurveyorName(), "feature.location.history.pic", "search.surveyorName", "1", this.getStringLength(30), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);

        this.dateValidator(messages, dateTime1, "feature.location.history.start.periode", "search.timeFrom", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_DATE_DMY_TIME_HM);
        this.dateValidator(messages, dateTime2, "feature.location.history.end.periode", "search.timeUntil", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_DATE_DMY_TIME_HM);

        if (12 == dateTime1.length() && 12 == dateTime2.length()) {
            int i = 0;
            String dateLength = dateTime1 + dateTime2;
            int intDate = 0;

            while (i < dateLength.length()) {
                try {
                    intDate = Integer.parseInt(dateLength.substring(i, i + 1));
                    i++;
                } catch (Exception e) {
                    intDate = -1;
                    i = dateLength.length();
                }
            }

            if (intDate >= 0) {
                this.dateTimeRangeValidator_ddMMyyyyhhmm(messages, dateTime1, dateTime2, "feature.location.history.start.periode", "search.timeFrom", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_DATE_DMY_TIME_HM);
            }
        }

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
