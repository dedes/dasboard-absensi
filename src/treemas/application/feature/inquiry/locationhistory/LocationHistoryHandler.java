package treemas.application.feature.inquiry.locationhistory;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.FeatureManager;
import treemas.application.login.LoginManager;
import treemas.base.inquiry.InquiryActionBase;
import treemas.base.sec.term.UserImpl;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.cryptography.SAK_Formater;
import treemas.util.format.TreemasDateFormatter;
import treemas.util.format.TreemasDecimalFormatter;
import treemas.util.format.TreemasFormatter;
import treemas.util.log.ILogger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class LocationHistoryHandler extends InquiryActionBase {
    LocationHistoryManager manager = new LocationHistoryManager();
    LocationHistoryValidator lValidator;
    private Map excConverterMap = new HashMap();

    public LocationHistoryHandler() {
        TreemasDateFormatter convertDMYTime = TreemasDateFormatter.getInstance();
        TreemasDecimalFormatter convertDecimal = TreemasDecimalFormatter.getInstance();

        this.excConverterMap.put("timestamp", convertDMYTime);
        this.excConverterMap.put("latitude", convertDecimal);
        this.excConverterMap.put("longitude", convertDecimal);
        this.excConverterMap.put("southBound", convertDecimal);
        this.excConverterMap.put("westBound", convertDecimal);
        this.excConverterMap.put("northBound", convertDecimal);
        this.excConverterMap.put("eastBound", convertDecimal);

        pageMap.put("Generate", "preview");
        exceptionMap.put("Generate", "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.lValidator = new LocationHistoryValidator(request);
        LocationHistoryForm locationHistoryForm = (LocationHistoryForm) form;
        locationHistoryForm.getPaging().calculationPage();
        String action = locationHistoryForm.getTask();

        if (locationHistoryForm.getParam() != null) {
            String userId = SAK_Formater.DecipherData(new String(locationHistoryForm.getParam()));
            LoginManager managerLogin = new LoginManager();
            SessionBean loginBean = managerLogin.doLogin(userId);
            //loginBean.setPassword(loginForm.getPassword());
            //manager.insertUserLogs(loginBean.getLoginId(),"LOGIN",request.getRemoteAddr());
            request.getSession(true).setAttribute(Global.SESSION_LOGIN, loginBean);
            locationHistoryForm.setJob(loginBean.getJobDesc());
            //request.setAttribute("redir_url", "/ToDoAction.do");
            this.logger.log(ILogger.LEVEL_INFO, "Login From VIPS User " + loginBean.getLoginId() +
                    " log-in. Address=" + request.getRemoteAddr());
        }

        if (Global.WEB_TASK_SHOW.equals(action)) {
            this.setDefaultSearchForm(locationHistoryForm);
        } else if (Global.WEB_TASK_LOAD.equals(action) ||
                Global.WEB_TASK_SEARCH.equals(action)) {
            if (this.lValidator.validateLocationHistorySearch(new ActionMessages(), locationHistoryForm)) {
                this.loadBranchPOIList(request, null);
                this.loadLocationHistory(request, locationHistoryForm);
//				this.loadTaskLocation(request, locationHistoryForm);
            }
        } else if ("Generate".equals(action)) {
            String name = !Strings.isNullOrEmpty(locationHistoryForm.getSearch().getSurveyorId()) ? locationHistoryForm.getSearch().getSurveyorId() : "";
            String dateFrom = !Strings.isNullOrEmpty(locationHistoryForm.getSearch().getDateFrom()) ? locationHistoryForm.getSearch().getDateFrom() : "";
            String timeFrom = !Strings.isNullOrEmpty(locationHistoryForm.getSearch().getTimeFrom()) ? locationHistoryForm.getSearch().getTimeFrom() : "";
            String dateUntil = !Strings.isNullOrEmpty(locationHistoryForm.getSearch().getDateUntil()) ? locationHistoryForm.getSearch().getDateUntil() : "";
            String timeUntil = !Strings.isNullOrEmpty(locationHistoryForm.getSearch().getTimeUntil()) ? locationHistoryForm.getSearch().getTimeUntil() : "";
            String type = !Strings.isNullOrEmpty(locationHistoryForm.getSearch().getSource()) ? locationHistoryForm.getSearch().getSource() : "";
            String width = !Strings.isNullOrEmpty(locationHistoryForm.getWidth()) ? locationHistoryForm.getWidth() : "";
            String height = !Strings.isNullOrEmpty(locationHistoryForm.getHeight()) ? locationHistoryForm.getHeight() : "";

            request.setAttribute("name", name);
            request.setAttribute("dateFrom", dateFrom);
            request.setAttribute("timeFrom", timeFrom);
            request.setAttribute("dateUntil", dateUntil);
            request.setAttribute("timeUntil", timeUntil);
            request.setAttribute("type", type);
            request.setAttribute("width", width);
            request.setAttribute("height", height);
        }

        return this.getNextPage(action, mapping);
    }

    private void setDefaultSearchForm(LocationHistoryForm locationHistoryForm) {
        Date today = new Date();
        String todayFormatted = TreemasFormatter.formatDate(today, Global.FROM_STRING_DATE_FORMAT);
        locationHistoryForm.getSearch().setDateFrom(todayFormatted);
        locationHistoryForm.getSearch().setDateUntil(todayFormatted);
        locationHistoryForm.getSearch().setTimeFrom("0000");
        locationHistoryForm.getSearch().setTimeUntil("2359");
        locationHistoryForm.getSearch().setSource("10");
    }

    private void loadTaskLocation(HttpServletRequest request, LocationHistoryForm form)
            throws CoreException {
        List list = this.manager.getTaskLocation(form.getSearch());
        if (list != null)
            form.setListSize(String.valueOf(list.size()));

        LocationHistoryBean boundBean = this.getBound(list);
        BeanConverter.convertBeanToString(boundBean, form, excConverterMap);

        list = BeanConverter.convertBeansToString(list, LocationHistoryForm.class, excConverterMap);
        request.setAttribute("listTaskLocation", list);
    }

    private void loadLocationHistory(HttpServletRequest request, LocationHistoryForm form)
            throws CoreException {
        List list = this.manager.getLocationHistory(form.getSearch());
        if (list != null)
            form.setListSize(String.valueOf(list.size()));

        LocationHistoryBean boundBean = this.getBound(list);
        BeanConverter.convertBeanToString(boundBean, form, excConverterMap);

        list = BeanConverter.convertBeansToString(list, LocationHistoryForm.class, excConverterMap);
        request.setAttribute("listLocation", list);
    }

    private void loadBranchPOIList(HttpServletRequest request, String branchId) throws CoreException {
        FeatureManager commonManager = new FeatureManager();
        List list = commonManager.getBranchLocation(branchId);
        list = BeanConverter.convertBeansToStringMap(list, excConverterMap);
        request.setAttribute(Global.REQUEST_ATTR_BRANCH_POI, list);

        String img64 = UserImpl.ICON_IMG.toString();
        request.setAttribute(Global.REQUEST_ATTR_IMG_ICON, img64);
    }

    private LocationHistoryBean getBound(List list) {
        LocationHistoryBean result = new LocationHistoryBean();

        LocationHistoryBean bean = (LocationHistoryBean) list.get(0);
        double minLat = bean.getLatitude().doubleValue();
        double maxLat = bean.getLatitude().doubleValue();
        double minLng = bean.getLongitude().doubleValue();
        double maxLng = bean.getLongitude().doubleValue();

        for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
            LocationHistoryBean tempBean = (LocationHistoryBean) iterator.next();

            if (tempBean.getLatitude().doubleValue() < minLat) {
                minLat = tempBean.getLatitude().doubleValue();
            }

            if (tempBean.getLongitude().doubleValue() < minLng) {
                minLng = tempBean.getLongitude().doubleValue();
            }

            if (tempBean.getLatitude().doubleValue() > maxLat) {
                maxLat = tempBean.getLatitude().doubleValue();
            }

            if (tempBean.getLongitude().doubleValue() > maxLng) {
                maxLng = tempBean.getLongitude().doubleValue();
            }
        }

        result.setSouthBound(new Double(minLat));
        result.setNorthBound(new Double(maxLat));
        result.setWestBound(new Double(minLng));
        result.setEastBound(new Double(maxLng));

        return result;
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;

        switch (code) {
            case LocationHistoryManager.ERROR_NO_HISTORY_FOUND:
                this.setMessage(request, "tracking.nohistory", null);
                break;

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((LocationHistoryForm) form).setTask(((LocationHistoryForm) form).resolvePreviousTask());
        String task = ((LocationHistoryForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
