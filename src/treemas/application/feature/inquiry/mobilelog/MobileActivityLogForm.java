package treemas.application.feature.inquiry.mobilelog;

import treemas.base.inquiry.InquiryFormBase;

public class MobileActivityLogForm extends InquiryFormBase {
    private String surveyor;
    private String branchId;
    private String activity;
    private String timestamp;

    private MobileActivityLogSearch search = new MobileActivityLogSearch();

    public String getSurveyor() {
        return surveyor;
    }

    public void setSurveyor(String surveyor) {
        this.surveyor = surveyor;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public MobileActivityLogSearch getSearch() {
        return search;
    }

    public void setSearch(MobileActivityLogSearch search) {
        this.search = search;
    }
}
