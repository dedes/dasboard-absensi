package treemas.application.feature.inquiry.mobilelog;

import treemas.util.ibatis.SearchFormParameter;

public class MobileActivityLogSearch extends SearchFormParameter {
    private String periodFrom;
    private String periodUntil;
    private String userId;
    private String userName;

    public String getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(String periodFrom) {
        this.periodFrom = periodFrom;
    }

    public String getPeriodUntil() {
        return periodUntil;
    }

    public void setPeriodUntil(String periodUntil) {
        this.periodUntil = periodUntil;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
