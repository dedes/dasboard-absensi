package treemas.application.feature.inquiry.mobilelog;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.feature.general.ComboManager;
import treemas.application.feature.task.upload.ExcelUploadManager;
import treemas.base.inquiry.InquiryActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.format.TreemasDateFormatter;
import treemas.util.format.TreemasFormatter;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;


public class MobileActivityLogHandler extends InquiryActionBase {

    private Map excConverterMap = new HashMap();
    private ComboManager comboManager = new ComboManager();
    private MobileActivityLogManager manager = new MobileActivityLogManager();

    public MobileActivityLogHandler() {
        TreemasDateFormatter convertDMYTime = TreemasDateFormatter.getInstance();
        this.excConverterMap.put("timestamp", convertDMYTime);
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        MobileActivityLogForm logForm = (MobileActivityLogForm) form;
        logForm.getPaging().calculationPage();
        String action = logForm.getTask();

        if (Global.WEB_TASK_SHOW.equals(action)) {
            String loginBranch = this.getLoginBean(request).getBranchId();
            this.loadBranch(request, loginBranch);
            logForm.getSearch().setSearchValue1(loginBranch);
        } else if (Global.WEB_TASK_LOAD.equals(action) ||
                Global.WEB_TASK_SEARCH.equals(action)) {
            this.loadList(request, logForm);
            String loginBranch = this.getLoginBean(request).getBranchId();
            this.loadBranch(request, loginBranch);
        } else if ("Export".equals(action)) {
            this.doExport(request, response, logForm);
            return null;
        }

        return this.getNextPage(action, mapping);
    }

    private void doExport(HttpServletRequest request, HttpServletResponse response, MobileActivityLogForm logForm) throws CoreException {
        Date sysdate = new Date();
        StringBuffer filename = new StringBuffer()
                .append(TreemasFormatter.formatDate(sysdate, "yyyyMMdd_HH-mm-ss"))
                .append("_mobile_activity_log.csv");

        response.setContentType("text/csv");
        response.setHeader("content-disposition", "attachment; filename=" + filename.toString());
        response.setHeader("cache-control", "max-age=1800");

        OutputStream out = null;
        PrintWriter pw = null;
        try {
            List surveyList = this.manager.getExportList(logForm.getSearch());
            try {
                out = response.getOutputStream();
                pw = new PrintWriter(out);

                if (null != surveyList) {
                    pw.println(this.manager.getCSVHeader());
                    for (Iterator iterator = surveyList.iterator(); iterator.hasNext(); ) {
                        String str = (String) iterator.next();
                        pw.println(str);
                    }
                }
            } finally {
                if (pw != null) {
                    pw.flush();
                    pw.close();
                }
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }
        } catch (IOException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ExcelUploadManager.ERROR_UNKNOWN);
        } catch (CoreException me) {
            String loginBranch = this.getLoginBean(request).getBranchId();
            this.loadBranch(request, loginBranch);

            throw me;
        }
    }

    private void loadBranch(HttpServletRequest request, String branchId) throws CoreException {
        List list = comboManager.getComboCabangByLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadList(HttpServletRequest request, MobileActivityLogForm logForm)
            throws CoreException {
        int targetPageNo = logForm.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(logForm.getTask())) {
            targetPageNo = 1;
        }
        PageListWrapper result = this.manager.getAll(targetPageNo, logForm.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, MobileActivityLogForm.class, excConverterMap);
        logForm.getPaging().setCurrentPageNo(targetPageNo);
        logForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listActivity", list);
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();

        switch (code) {
            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((MobileActivityLogForm) form).setTask(((MobileActivityLogForm) form).resolvePreviousTask());
        String task = ((MobileActivityLogForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
