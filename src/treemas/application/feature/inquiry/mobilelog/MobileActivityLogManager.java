package treemas.application.feature.inquiry.mobilelog;

import treemas.base.inquiry.InquiryManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class MobileActivityLogManager extends InquiryManagerBase {

    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_INQ_MOBILE_LOG;

    public PageListWrapper getAll(int numberPage, MobileActivityLogSearch searchParam) throws CoreException {
        PageListWrapper result = new PageListWrapper();

        searchParam.setPage(numberPage);
        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAll", searchParam);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "cntAll", searchParam);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public String getCSVHeader() throws CoreException {
        String result = null;

        try {
            result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getCSVHeader", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

        return result;
    }

    public List getExportList(MobileActivityLogSearch searchParam) throws CoreException {
        List result = null;

        try {
            result = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getExportList", searchParam);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

        return result;
    }
}
