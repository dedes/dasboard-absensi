package treemas.application.feature.inquiry.mobilelog;

import java.io.Serializable;
import java.util.Date;

public class MobileActivityLogBean implements Serializable {
    private String surveyor;
    private String branchId;
    private String activity;
    private Date timestamp;

    public String getSurveyor() {
        return surveyor;
    }

    public void setSurveyor(String surveyor) {
        this.surveyor = surveyor;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
