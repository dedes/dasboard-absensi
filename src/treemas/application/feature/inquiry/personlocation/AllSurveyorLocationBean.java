package treemas.application.feature.inquiry.personlocation;

import java.io.Serializable;
import java.util.Date;

public class AllSurveyorLocationBean implements Serializable {
    private Integer seqNo;
    private String surveyorId;
    private String surveyorName;
    private Double latitude;
    private Double longitude;
    private Date timestamp;
    private String isGps;
    private Integer accuracy;

    private Integer MCC;
    private Integer MNC;
    private Integer LAC;
    private Integer cellId;

    private String address;

    private Integer cntOsTask;
    private Integer cntSubmittedTask;
    private String provider;
    private String flag;

    private String ctxPath;
    private String sysdate;

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public String getSurveyorId() {
        return surveyorId;
    }

    public void setSurveyorId(String surveyorId) {
        this.surveyorId = surveyorId;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getIsGps() {
        return isGps;
    }

    public void setIsGps(String isGps) {
        this.isGps = isGps;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public Integer getMCC() {
        return MCC;
    }

    public void setMCC(Integer mcc) {
        MCC = mcc;
    }

    public Integer getMNC() {
        return MNC;
    }

    public void setMNC(Integer mnc) {
        MNC = mnc;
    }

    public Integer getLAC() {
        return LAC;
    }

    public void setLAC(Integer lac) {
        LAC = lac;
    }

    public Integer getCellId() {
        return cellId;
    }

    public void setCellId(Integer cellId) {
        this.cellId = cellId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getCntOsTask() {
        return cntOsTask;
    }

    public void setCntOsTask(Integer cntOsTask) {
        this.cntOsTask = cntOsTask;
    }

    public Integer getCntSubmittedTask() {
        return cntSubmittedTask;
    }

    public void setCntSubmittedTask(Integer cntSubmittedTask) {
        this.cntSubmittedTask = cntSubmittedTask;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCtxPath() {
        return ctxPath;
    }

    public void setCtxPath(String ctxPath) {
        this.ctxPath = ctxPath;
    }

    public String getSysdate() {
        return sysdate;
    }

    public void setSysdate(String sysdate) {
        this.sysdate = sysdate;
    }
}
