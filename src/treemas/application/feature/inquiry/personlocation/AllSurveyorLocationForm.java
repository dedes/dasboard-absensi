package treemas.application.feature.inquiry.personlocation;

import treemas.base.inquiry.InquiryFormBase;

public class AllSurveyorLocationForm extends InquiryFormBase {
    private String surveyorId;
    private String surveyorName;
    private String latitude;
    private String longitude;
    private String timestamp;
    private String isGps;
    private String accuracy;

    private String listSize;

    private String southBound;
    private String westBound;
    private String northBound;
    private String eastBound;

    private String address;

    private String cntOsTask;
    private String cntSubmittedTask;

    private String searchBranch;
    private String searchJob;
    private String param;

    private String sysdate;

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getSurveyorId() {
        return surveyorId;
    }

    public void setSurveyorId(String surveyorId) {
        this.surveyorId = surveyorId;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getIsGps() {
        return isGps;
    }

    public void setIsGps(String isGps) {
        this.isGps = isGps;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getListSize() {
        return listSize;
    }

    public void setListSize(String listSize) {
        this.listSize = listSize;
    }

    public String getSouthBound() {
        return southBound;
    }

    public void setSouthBound(String southBound) {
        this.southBound = southBound;
    }

    public String getWestBound() {
        return westBound;
    }

    public void setWestBound(String westBound) {
        this.westBound = westBound;
    }

    public String getNorthBound() {
        return northBound;
    }

    public void setNorthBound(String northBound) {
        this.northBound = northBound;
    }

    public String getEastBound() {
        return eastBound;
    }

    public void setEastBound(String eastBound) {
        this.eastBound = eastBound;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCntOsTask() {
        return cntOsTask;
    }

    public void setCntOsTask(String cntOsTask) {
        this.cntOsTask = cntOsTask;
    }

    public String getCntSubmittedTask() {
        return cntSubmittedTask;
    }

    public void setCntSubmittedTask(String cntSubmittedTask) {
        this.cntSubmittedTask = cntSubmittedTask;
    }

    public String getSearchBranch() {
        return searchBranch;
    }

    public void setSearchBranch(String searchBranch) {
        this.searchBranch = searchBranch;
    }

    public String getSearchJob() {
        return searchJob;
    }

    public void setSearchJob(String searchJob) {
        this.searchJob = searchJob;
    }

    public String getSysdate() {
        return sysdate;
    }

    public void setSysdate(String sysdate) {
        this.sysdate = sysdate;
    }
}
