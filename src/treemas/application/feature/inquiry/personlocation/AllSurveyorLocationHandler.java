package treemas.application.feature.inquiry.personlocation;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.feature.FeatureManager;
import treemas.application.feature.general.ComboManager;
import treemas.application.login.LoginManager;
import treemas.base.inquiry.InquiryActionBase;
import treemas.base.sec.term.UserImpl;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.SAK_Formater;
import treemas.util.format.TreemasDateFormatter;
import treemas.util.format.TreemasDecimalFormatter;
import treemas.util.geofence.BoundBean;
import treemas.util.log.ILogger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class AllSurveyorLocationHandler extends InquiryActionBase {
    private AllSurveyorLocationManager manager = new AllSurveyorLocationManager();
    private Map excConverterMap = new HashMap();

    public AllSurveyorLocationHandler() {
        TreemasDateFormatter convertDMYTime = TreemasDateFormatter.getInstance();
        TreemasDecimalFormatter convertDecimal = TreemasDecimalFormatter.getInstance();

        this.excConverterMap.put("timestamp", convertDMYTime);
        this.excConverterMap.put("latitude", convertDecimal);
        this.excConverterMap.put("longitude", convertDecimal);
        this.excConverterMap.put("southBound", convertDecimal);
        this.excConverterMap.put("westBound", convertDecimal);
        this.excConverterMap.put("northBound", convertDecimal);
        this.excConverterMap.put("eastBound", convertDecimal);
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        AllSurveyorLocationForm allSurveyorLocationForm = (AllSurveyorLocationForm) form;
        String action = allSurveyorLocationForm.getTask();

        if (allSurveyorLocationForm.getParam() != null) {
            String userId = SAK_Formater.DecipherData(new String(allSurveyorLocationForm.getParam()));
            LoginManager managerLogin = new LoginManager();
            SessionBean loginBean = managerLogin.doLogin(userId);
            //loginBean.setPassword(loginForm.getPassword());
            //manager.insertUserLogs(loginBean.getLoginId(),"LOGIN",request.getRemoteAddr());
            request.getSession(true).setAttribute(Global.SESSION_LOGIN, loginBean);
            allSurveyorLocationForm.setSearchJob(loginBean.getJobDesc());
            //request.setAttribute("redir_url", "/ToDoAction.do");
            this.logger.log(ILogger.LEVEL_INFO, "Login From VIPS User " + loginBean.getLoginId() +
                    " log-in. Address=" + request.getRemoteAddr());
        }

        if (Global.WEB_TASK_SHOW.equals(action)) {
//			String loginBranch = this.getLoginBean(request).getBranchId();
//			this.loadBranch(request, loginBranch);
            String loginUserId = this.getLoginBean(request).getLoginId();
            this.loadMultiBranch(request, loginUserId);
            String branchId = this.getFirstMultiBranchByLogin(loginUserId);

            allSurveyorLocationForm.setSearchBranch(branchId);
            this.loadAllSurveyor(request, branchId, allSurveyorLocationForm);
            this.loadBranchPOIList(request, null);

            request.setAttribute("sysdate", this.getCurrentDate());
            request.setAttribute("branch", branchId);
        } else if (Global.WEB_TASK_LOAD.equals(action)) {
//			String loginBranch = this.getLoginBean(request).getBranchId();
//			this.loadBranch(request, loginBranch);
            String loginUserId = this.getLoginBean(request).getLoginId();
            this.loadMultiBranch(request, loginUserId);
            this.loadAllSurveyor(request, allSurveyorLocationForm.getSearchBranch(), allSurveyorLocationForm);
            this.loadBranchPOIList(request, null);

            request.setAttribute("sysdate", this.getCurrentDate());
            request.setAttribute("branch", allSurveyorLocationForm.getSearchBranch());
        }

        return this.getNextPage(action, mapping);
    }

    private void loadBranch(HttpServletRequest request, String branchId)
            throws CoreException {
        ComboManager comboManager = new ComboManager();
        List list = comboManager.getComboCabangByLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadMultiBranch(HttpServletRequest request, String userId)
            throws CoreException {
        ComboManager comboManager = new ComboManager();
        List list = comboManager.getComboMultiCabangByLogin(userId);
        request.setAttribute("comboBranch", list);
    }

    private String getFirstMultiBranchByLogin(String userId)
            throws CoreException {
        String branchId = null;
        branchId = this.manager.getFirstMultiBranchByUserId(userId);
        return branchId;
    }

    private String getCurrentDate() {
        String currentDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat(Global.FROM_STRING_DATE_FORMAT);
        currentDate = sdf.format(new java.util.Date());
        return currentDate;
    }

    private void loadAllSurveyor(HttpServletRequest request, String branchId, AllSurveyorLocationForm form) throws CoreException {
        String job = form.getSearchJob();
        List list = this.manager.getAllSurveyorLocation(branchId, job);
        if (list != null)
            form.setListSize(String.valueOf(list.size()));

        BoundBean boundBean = this.getBound(list);
        BeanConverter.convertBeanToString(boundBean, form, excConverterMap);

        list = BeanConverter.convertBeansToString(list, AllSurveyorLocationForm.class, excConverterMap);
        request.setAttribute("listAllSurveyorLocation", list);
    }

    private void loadBranchPOIList(HttpServletRequest request, String branchId) throws CoreException {
        FeatureManager commonManager = new FeatureManager();
        List list = commonManager.getBranchLocation(branchId);
        list = BeanConverter.convertBeansToStringMap(list, excConverterMap);
        request.setAttribute(Global.REQUEST_ATTR_BRANCH_POI, list);

        String img64 = UserImpl.ICON_IMG.toString();
        request.setAttribute(Global.REQUEST_ATTR_IMG_ICON, img64);
    }

    private BoundBean getBound(List list) throws CoreException {
        BoundBean result = new BoundBean();

        if (list == null || list.size() == 0)
            throw new CoreException(AllSurveyorLocationManager.ERROR_NO_SURVEYOR_FOUND);

        AllSurveyorLocationBean bean = (AllSurveyorLocationBean) list.get(0);
        double minLat = bean.getLatitude().doubleValue();
        double maxLat = bean.getLatitude().doubleValue();
        double minLng = bean.getLongitude().doubleValue();
        double maxLng = bean.getLongitude().doubleValue();

        for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
            AllSurveyorLocationBean tempBean = (AllSurveyorLocationBean) iterator.next();

            if (tempBean.getLatitude().doubleValue() < minLat) {
                minLat = tempBean.getLatitude().doubleValue();
            }

            if (tempBean.getLongitude().doubleValue() < minLng) {
                minLng = tempBean.getLongitude().doubleValue();
            }

            if (tempBean.getLatitude().doubleValue() > maxLat) {
                maxLat = tempBean.getLatitude().doubleValue();
            }

            if (tempBean.getLongitude().doubleValue() > maxLng) {
                maxLng = tempBean.getLongitude().doubleValue();
            }
        }

        result.setSouthBound(new Double(minLat));
        result.setNorthBound(new Double(maxLat));
        result.setWestBound(new Double(minLng));
        result.setEastBound(new Double(maxLng));

        return result;
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();

        switch (code) {
            case AllSurveyorLocationManager.ERROR_NO_SURVEYOR_FOUND:
                this.setMessage(request, "allsurveyor.notfound", null);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((AllSurveyorLocationForm) form).setTask(((AllSurveyorLocationForm) form).resolvePreviousTask());
        String task = ((AllSurveyorLocationForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
