package treemas.application.feature.inquiry.assignment;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessages;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class AssignmentValidator extends Validator {

    public AssignmentValidator(HttpServletRequest request) {
        super(request);
    }

    public AssignmentValidator(Locale locale) {
        super(locale);
    }

    public boolean validateAssignmentSearch(ActionMessages messages, AssignmentForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        if (!Strings.isNullOrEmpty(form.getSearch().getTglStart())) {
            this.dateValidator(messages, form.getSearch().getTglStart(), "feature.location.history.start.periode", "search.tglStart", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_DATE_DMY);
        }

        if (!Strings.isNullOrEmpty(form.getSearch().getTglEnd())) {
            this.dateValidator(messages, form.getSearch().getTglEnd(), "feature.location.history.end.periode ", "search.tglEnd", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_DATE_DMY);
        }
        if (!Strings.isNullOrEmpty(form.getSearch().getTglStart()) && !Strings.isNullOrEmpty(form.getSearch().getTglEnd())) {
            if (8 == form.getSearch().getTglStart().length() && 8 == form.getSearch().getTglEnd().length()) {
                int i = 0;
                String dateLength = form.getSearch().getTglStart() + form.getSearch().getTglEnd();
                int intDate = 0;

                while (i < dateLength.length()) {
                    try {
                        intDate = Integer.parseInt(dateLength.substring(i, i + 1));
                        i++;
                    } catch (Exception e) {
                        intDate = -1;
                        i = dateLength.length();
                    }
                }

                if (intDate >= 0) {
                    this.dateRangeValidatorEqual(messages, form.getSearch().getTglStart(), form.getSearch().getTglEnd(), "feature.location.history.start.periode", "search.tglStart", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_DATE_DMY);
                }
            }
        }
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
