package treemas.application.feature.inquiry.assignment;

import treemas.util.ibatis.SearchFormParameter;

public class AssignmentSearch extends SearchFormParameter {
    private String tglStart;
    private String tglEnd;
    private String status;
    private String status2hidden;
    private String scheme;
    private String priority;
    private String orderField;
    private String branch;
    private String userId;
    private String jobDesc;

    public String getTglStart() {
        return tglStart;
    }

    public void setTglStart(String tglStart) {
        this.tglStart = tglStart;
    }

    public String getTglEnd() {
        return tglEnd;
    }

    public void setTglEnd(String tglEnd) {
        this.tglEnd = tglEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus2hidden() {
        return status2hidden;
    }

    public void setStatus2hidden(String status2hidden) {
        this.status2hidden = status2hidden;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getOrderField() {
        return orderField;
    }

    public void setOrderField(String orderField) {
        this.orderField = orderField;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getJobDesc() {
        return jobDesc;
    }

    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc;
    }
}
