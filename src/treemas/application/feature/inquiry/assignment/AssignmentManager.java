package treemas.application.feature.inquiry.assignment;

import com.google.common.base.Strings;
import treemas.base.inquiry.InquiryManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class AssignmentManager extends InquiryManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_INQ_ASSIGNMENT;

    public PageListWrapper getAllAssignment(int numberPage,
                                            AssignmentSearch searchParam) throws CoreException {
        PageListWrapper result = new PageListWrapper();

        String searchValue = searchParam.getSearchValue1();

        int sortdesc = 0;
        if (!Strings.isNullOrEmpty(searchParam.getSortDesc())
                && "ON".equals(searchParam.getSortDesc().toUpperCase())) {
            sortdesc = 1;
        }

        searchValue = searchValue != null ? searchValue.trim().toUpperCase() : searchValue;
        searchParam.setPage(numberPage);
        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", searchParam);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", searchParam);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }
}
