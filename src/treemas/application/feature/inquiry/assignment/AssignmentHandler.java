package treemas.application.feature.inquiry.assignment;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.general.ComboManager;
import treemas.base.inquiry.InquiryActionBase;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.format.TreemasDateFormatter;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AssignmentHandler extends InquiryActionBase {
    private Map excConverterMap = new HashMap();
    private AssignmentManager manager = new AssignmentManager();
    private ComboManager comboManager = new ComboManager();
    private AssignmentValidator aValidator;

    public AssignmentHandler() {
        TreemasDateFormatter convertDMYTime = TreemasDateFormatter.getInstance();
//	    this.excConverterMap.put("rfaDate", convertDMYTime);
        this.excConverterMap.put("assignmentDate", convertDMYTime);
        this.excConverterMap.put("retrieveDate", convertDMYTime);
        this.excConverterMap.put("submitDate", convertDMYTime);
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.aValidator = new AssignmentValidator(request);
        AssignmentForm assignmentForm = (AssignmentForm) form;
        assignmentForm.getPaging().calculationPage();
        String action = assignmentForm.getTask();

        if (Global.WEB_TASK_SHOW.equals(action)) {
//			String loginBranch = this.getLoginBean(request).getBranchId();
//			this.loadBranch(request, loginBranch);
//			assignmentForm.getSearch().setBranch(loginBranch);
            String loginUserId = this.getLoginBean(request).getLoginId();
            this.loadMultiBranch(request, loginUserId);

            this.loadComboStatusSurvey(request);
            this.loadComboScheme(request);
            this.loadPriority(request);

        } else if (Global.WEB_TASK_LOAD.equals(action) ||
                Global.WEB_TASK_SEARCH.equals(action)) {
            if (this.aValidator.validateAssignmentSearch(new ActionMessages(), assignmentForm)) {
                this.loadList(request, assignmentForm);
            }
        } else if (Global.WEB_TASK_GENERATE.equals(action)) {
            if (this.aValidator.validateAssignmentSearch(new ActionMessages(), assignmentForm)) {

                String branch = !Strings.isNullOrEmpty(assignmentForm.getSearch().getBranch()) ? assignmentForm.getSearch().getBranch() : "";
                String searchValue1 = !Strings.isNullOrEmpty(assignmentForm.getSearch().getSearchValue1()) ? assignmentForm.getSearch().getSearchValue1() : "";
                String tglStart = !Strings.isNullOrEmpty(assignmentForm.getSearch().getTglStart()) ? assignmentForm.getSearch().getTglStart() : "";
                String tglEnd = !Strings.isNullOrEmpty(assignmentForm.getSearch().getTglEnd()) ? assignmentForm.getSearch().getTglEnd() : "";
                String status = !Strings.isNullOrEmpty(assignmentForm.getSearch().getStatus()) ? assignmentForm.getSearch().getStatus() : "";
                String status2hidden = !Strings.isNullOrEmpty(assignmentForm.getSearch().getStatus2hidden()) ? assignmentForm.getSearch().getStatus2hidden() : "";
                String scheme = !Strings.isNullOrEmpty(assignmentForm.getSearch().getScheme()) ? assignmentForm.getSearch().getScheme() : "";
                String width = !Strings.isNullOrEmpty(assignmentForm.getWidth()) ? assignmentForm.getWidth() : "";
                String height = !Strings.isNullOrEmpty(assignmentForm.getHeight()) ? assignmentForm.getHeight() : "";
                String orderField = !Strings.isNullOrEmpty(assignmentForm.getSearch().getOrderField()) ? assignmentForm.getSearch().getOrderField() : "";
                String sortDesc = !Strings.isNullOrEmpty(assignmentForm.getSearch().getSortDesc()) ? assignmentForm.getSearch().getSortDesc() : "";
                String searchType1 = !Strings.isNullOrEmpty(Integer.toString(assignmentForm.getSearch().getSearchType1())) ? Integer.toString(assignmentForm.getSearch().getSearchType1()) : "";
                String searchType2 = !Strings.isNullOrEmpty(Integer.toString(assignmentForm.getSearch().getSearchType2())) ? Integer.toString(assignmentForm.getSearch().getSearchType2()) : "";
                String userId = !Strings.isNullOrEmpty(this.getLoginBean(request).getLoginId()) ? this.getLoginBean(request).getLoginId() : "";

                request.setAttribute("branch", branch);
                request.setAttribute("searchValue1", searchValue1);
                request.setAttribute("tglStart", tglStart);
                request.setAttribute("tglEnd", tglEnd);
                request.setAttribute("status", status);
                request.setAttribute("status2hidden", status2hidden);
                request.setAttribute("scheme", scheme);
                request.setAttribute("userId", userId);
                request.setAttribute("width", width);
                request.setAttribute("height", height);
                request.setAttribute("orderField", orderField);
                request.setAttribute("sortDesc", sortDesc);
                request.setAttribute("searchType1", searchType1);
                request.setAttribute("searchType2", searchType2);
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadBranch(HttpServletRequest request, String branchId)
            throws CoreException {
        List list = comboManager.getComboCabangByLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadMultiBranch(HttpServletRequest request, String userId)
            throws CoreException {
        List list = comboManager.getComboMultiCabangByLogin(userId);
        request.setAttribute("comboBranch", list);
    }

    private void loadComboStatusSurvey(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboStatusSurvey();
        request.setAttribute("comboStatus", list);
    }

    private void loadComboScheme(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboScheme(this.getLoginId(request));
        request.setAttribute("comboScheme", list);
    }

    private void loadPriority(HttpServletRequest request)
            throws CoreException {
        List list = comboManager.getComboPriority();
        request.setAttribute("comboPriority", list);
    }

    private void loadList(HttpServletRequest request, AssignmentForm assignmentForm)
            throws CoreException {
//		System.out.println("IS ASCENDING? "+assignmentForm.getSearch().getSortDesc());
//		int sortdesc = 0;
//		if(!Strings.isNullOrEmpty(assignmentForm.getSearch().getSortDesc()) && "ON".equals(assignmentForm.getSearch().getSortDesc().toUpperCase())){
//			sortdesc = 1;
//		}
        int targetPageNo = assignmentForm.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(assignmentForm.getTask())) {
            targetPageNo = 1;
        }

        SessionBean sBean = this.getLoginBean(request);
        String loginUsedId = sBean.getLoginId();
        String jobDesc = sBean.getJobDesc();
        assignmentForm.getSearch().setJobDesc(jobDesc);
        assignmentForm.getSearch().setUserId(loginUsedId);
        PageListWrapper result = this.manager.getAllAssignment(targetPageNo,
                assignmentForm.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, AssignmentForm.class, excConverterMap);
        assignmentForm.getPaging().setCurrentPageNo(targetPageNo);
        assignmentForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listAssignment", list);

//		String loginBranch = this.getLoginBean(request).getBranchId();
//		this.loadBranch(request, loginBranch);
        this.loadMultiBranch(request, loginUsedId);

        this.loadComboStatusSurvey(request);
        this.loadComboScheme(request);
        this.loadPriority(request);
    }


    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;

        switch (code) {

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((AssignmentForm) form).setTask(((AssignmentForm) form).resolvePreviousTask());
        String task = ((AssignmentForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
