package treemas.application.feature.inquiry.taskmonitoring;

import treemas.base.inquiry.InquiryFormBase;

import java.util.List;


public class ReportMonitorForm extends InquiryFormBase {
    private String userId;
    private String fullName;
    private String schemeId;
    private String mobileAssignmentId;
    private String assignmentDate = "";
    private String retrieveDate = "";
    private String submitDate = "";
    private String assignmentStatus;

    private String searchBranch;

    private List listMonitor;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(String mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public String getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(String assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public String getRetrieveDate() {
        return retrieveDate;
    }

    public void setRetrieveDate(String retrieveDate) {
        this.retrieveDate = retrieveDate;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getAssignmentStatus() {
        return assignmentStatus;
    }

    public void setAssignmentStatus(String assignmentStatus) {
        this.assignmentStatus = assignmentStatus;
    }

    public String getSearchBranch() {
        return searchBranch;
    }

    public void setSearchBranch(String searchBranch) {
        this.searchBranch = searchBranch;
    }

    public List getListMonitor() {
        return listMonitor;
    }

    public void setListMonitor(List listMonitor) {
        this.listMonitor = listMonitor;
    }

}
