package treemas.application.feature.inquiry.taskmonitoring;

import com.google.common.base.Strings;
import treemas.base.inquiry.InquiryManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReportMonitorManager extends InquiryManagerBase {

    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_INQ_TASK_MONITORING;

    public List getReportSurveyorList(String branch, String userLogin) throws CoreException {
        List listReturn;
        List listSurveyor;
        try {

            String mapJob = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getLoginJob", userLogin);
            boolean tr = true;

            if (!Strings.isNullOrEmpty(mapJob) && mapJob.equalsIgnoreCase("1"))
                tr = false;

            if (!tr) {

                Map param = new HashMap();

                param.put("branchCode", branch);
                param.put("userId", userLogin);

                listSurveyor = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getSurveyorByScheme", param);
            } else {
                listSurveyor = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getSurveyor", branch);
            }
            listReturn = this.getReportAssignmentList(listSurveyor);

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return listReturn;
    }

    private List getReportAssignmentList(List listSurveyor) throws SQLException {
        List list = new ArrayList();
        for (int i = 0; i < listSurveyor.size(); i++) {
            ReportMonitorBean rmb = (ReportMonitorBean) listSurveyor.get(i);
            List tempList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAssignment", rmb.getUserId());
            Map mapOfIdAndList = new HashMap();
            mapOfIdAndList.put("userId", rmb.getUserId());
            mapOfIdAndList.put("fullName", rmb.getFullName());
            mapOfIdAndList.put("listOfAssignment", tempList);
            list.add(mapOfIdAndList);
        }

        return list;
    }

    public String getFirstMultiBranchByUserId(String userId) {
        String branchId = null;
        try {
            branchId = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getFirstMultiBranchByUserId", userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return branchId;
    }
}
