package treemas.application.feature.inquiry.taskmonitoring;

import java.io.Serializable;

public class ReportMonitorBean implements Serializable {
    private String userId;
    private String fullName;
    private String schemeId;
    private String mobileAssignmentId;
    private String assignmentDate = "";
    private String retrieveDate = "";
    private String submitDate = "";
    private String assignmentStatus;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(String mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public String getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(String assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public String getRetrieveDate() {
        return retrieveDate;
    }

    public void setRetrieveDate(String retrieveDate) {
        this.retrieveDate = retrieveDate;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getAssignmentStatus() {
        return assignmentStatus;
    }

    public void setAssignmentStatus(String assignmentStatus) {
        this.assignmentStatus = assignmentStatus;
    }
}
