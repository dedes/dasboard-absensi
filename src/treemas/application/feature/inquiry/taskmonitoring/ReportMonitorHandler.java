package treemas.application.feature.inquiry.taskmonitoring;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.feature.general.ComboManager;
import treemas.base.inquiry.InquiryActionBase;
import treemas.util.CoreException;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class ReportMonitorHandler extends InquiryActionBase {
    private ReportMonitorManager manager = new ReportMonitorManager();
    private ComboManager comboManager = new ComboManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        ReportMonitorForm frm = (ReportMonitorForm) form;
        String task = frm.getTask();
        if (Global.WEB_TASK_SHOW.equals(task)) {
            String loginUserId = this.getLoginBean(request).getLoginId();
            this.loadMultiBranch(request, loginUserId);
            String branchId = this.getFirstMultiBranchByLogin(loginUserId);
            frm.setSearchBranch(branchId);
        } else if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)) {
            String loginUserId = this.getLoginBean(request).getLoginId();
            this.loadList(request, frm, loginUserId);
        }
        return this.getNextPage(task, mapping);
    }

    private void loadList(HttpServletRequest request, ReportMonitorForm reportForm, String loginUser) throws CoreException {
        List list = this.manager.getReportSurveyorList(reportForm.getSearchBranch(), loginUser);
        request.setAttribute("listAssignmentSet", list);
        String loginUserId = this.getLoginBean(request).getLoginId();
        this.loadMultiBranch(request, loginUserId);
    }

    private void loadBranch(HttpServletRequest request, String branchId)
            throws CoreException {
        List list = comboManager.getComboCabangByLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadMultiBranch(HttpServletRequest request, String userId)
            throws CoreException {
        List list = comboManager.getComboMultiCabangByLogin(userId);
        request.setAttribute("comboBranch", list);
    }

    private String getFirstMultiBranchByLogin(String userId)
            throws CoreException {
        String branchId = null;
        branchId = this.manager.getFirstMultiBranchByUserId(userId);
        return branchId;
    }

}
