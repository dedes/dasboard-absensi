package treemas.application.feature.inquiry.heatmap;

import org.apache.struts.action.ActionMessages;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class SurveyHeatMapValidator extends Validator {

    public SurveyHeatMapValidator(HttpServletRequest request) {
        super(request);
    }

    public SurveyHeatMapValidator(Locale locale) {
        super(locale);
    }

    public boolean validateSurveyHeatMapSearch(ActionMessages messages, SurveyHeatMapForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        boolean tglStart = true;
        boolean tglEnd = true;

        try {
            new SimpleDateFormat(Global.FROM_STRING_DATE_FORMAT).parse(form.getSearch().getTglStart());
            new SimpleDateFormat(Global.FROM_STRING_DATE_FORMAT).parse(form.getSearch().getTglStart());
        } catch (ParseException e) {
            e.printStackTrace();
            tglStart = false;
            tglEnd = false;
        }

        if ((form.getSearch().getTglStart().equals(form.getSearch().getTglEnd()) && form.getSearch().getTglStart().length() == 0)
                || !form.getSearch().getTglStart().equals(form.getSearch().getTglEnd())
                || (!tglStart && !tglEnd)) {
            this.dateRangeValidator(messages, form.getSearch().getTglStart(), form.getSearch().getTglEnd(), "feature.heatmap.period", "datePeriod", ValidatorGlobal.TM_CHECKED_LENGTH_FIX, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_DATE_DMY);
        }

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
        return ret;

    }
}
