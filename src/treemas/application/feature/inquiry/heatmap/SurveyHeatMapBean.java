package treemas.application.feature.inquiry.heatmap;

import java.io.Serializable;

public class SurveyHeatMapBean implements Serializable {
    private Double gpsLatitude;
    private Double gpsLongitude;

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }
}