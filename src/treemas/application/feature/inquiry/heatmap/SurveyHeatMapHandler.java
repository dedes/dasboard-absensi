package treemas.application.feature.inquiry.heatmap;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.feature.FeatureManager;
import treemas.application.feature.general.ComboManager;
import treemas.base.inquiry.InquiryActionBase;
import treemas.base.sec.term.UserImpl;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.format.TreemasDecimalFormatter;
import treemas.util.format.TreemasFormatter;
import treemas.util.geofence.BoundBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


public class SurveyHeatMapHandler extends InquiryActionBase {
    private Map excConverterMap = new HashMap();
    private SurveyHeatMapManager manager = new SurveyHeatMapManager();
    private SurveyHeatMapValidator validator;

    public SurveyHeatMapHandler() {
        TreemasDecimalFormatter convertDecimal = TreemasDecimalFormatter.getInstance();
        this.excConverterMap.put("latitude", convertDecimal);
        this.excConverterMap.put("longitude", convertDecimal);
        this.excConverterMap.put("gpsLatitude", convertDecimal);
        this.excConverterMap.put("gpsLongitude", convertDecimal);
        this.excConverterMap.put("southBound", convertDecimal);
        this.excConverterMap.put("westBound", convertDecimal);
        this.excConverterMap.put("northBound", convertDecimal);
        this.excConverterMap.put("eastBound", convertDecimal);
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.validator = new SurveyHeatMapValidator(request);
        SurveyHeatMapForm surveyHeatMapForm = (SurveyHeatMapForm) form;
        String action = surveyHeatMapForm.getTask();

        if (Global.WEB_TASK_SHOW.equals(action)) {
            String loginBranch = this.getLoginBean(request).getBranchId();
            this.loadBranch(request, loginBranch);
            this.loadComboScheme(request);
            surveyHeatMapForm.getSearch().setBranch(loginBranch);
            surveyHeatMapForm.getSearch().setTglStart(TreemasFormatter.formatDate(new Date(), Global.FROM_STRING_DATE_FORMAT));
            surveyHeatMapForm.getSearch().setTglEnd(TreemasFormatter.formatDate(new Date(), Global.FROM_STRING_DATE_FORMAT));

//			this.load(request, surveyHeatMapForm);
        } else if (Global.WEB_TASK_LOAD.equals(action)) {
            if (this.validator.validateSurveyHeatMapSearch(new ActionMessages(), surveyHeatMapForm)) {
                String loginBranch = this.getLoginBean(request).getBranchId();
                this.loadBranch(request, loginBranch);
                this.loadComboScheme(request);
            }
            this.load(request, surveyHeatMapForm);
            this.loadBranchPOIList(request, null);
        }

        return this.getNextPage(action, mapping);
    }

    private void loadBranch(HttpServletRequest request, String branchId) throws CoreException {
        ComboManager comboManager = new ComboManager();
        List list = comboManager.getComboCabangByLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadComboScheme(HttpServletRequest request) throws CoreException {
        ComboManager comboManager = new ComboManager();
        List list = comboManager.getComboScheme();
        request.setAttribute("comboScheme", list);
    }

    private void load(HttpServletRequest request, SurveyHeatMapForm form) throws CoreException {
        List list = this.manager.getAllSurvey(form.getSearch());

        BoundBean boundBean = this.getBound(list);
        BeanConverter.convertBeanToString(boundBean, form, excConverterMap);

        list = BeanConverter.convertBeansToString(list, SurveyHeatMapForm.class, excConverterMap);
        request.setAttribute("listLocation", list);
    }

    private void loadBranchPOIList(HttpServletRequest request, String branchId) throws CoreException {
        FeatureManager commonManager = new FeatureManager();
        List list = commonManager.getBranchLocation(branchId);
        list = BeanConverter.convertBeansToStringMap(list, excConverterMap);
        request.setAttribute(Global.REQUEST_ATTR_BRANCH_POI, list);

        String img64 = UserImpl.ICON_IMG.toString();
        request.setAttribute(Global.REQUEST_ATTR_IMG_ICON, img64);
    }

    private BoundBean getBound(List list) throws CoreException {
        BoundBean result = new BoundBean();

        if (list == null || list.size() == 0)
            throw new CoreException(SurveyHeatMapManager.ERROR_NO_LOCATION_FOUND);

        SurveyHeatMapBean bean = (SurveyHeatMapBean) list.get(0);
        double minLat = bean.getGpsLatitude().doubleValue();
        double maxLat = bean.getGpsLatitude().doubleValue();
        double minLng = bean.getGpsLongitude().doubleValue();
        double maxLng = bean.getGpsLongitude().doubleValue();

        for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
            SurveyHeatMapBean tempBean = (SurveyHeatMapBean) iterator.next();

            if (tempBean.getGpsLatitude().doubleValue() < minLat) {
                minLat = tempBean.getGpsLatitude().doubleValue();
            }

            if (tempBean.getGpsLongitude().doubleValue() < minLng) {
                minLng = tempBean.getGpsLongitude().doubleValue();
            }

            if (tempBean.getGpsLatitude().doubleValue() > maxLat) {
                maxLat = tempBean.getGpsLatitude().doubleValue();
            }

            if (tempBean.getGpsLongitude().doubleValue() > maxLng) {
                maxLng = tempBean.getGpsLongitude().doubleValue();
            }
        }

//		minLat = null==minLat?0:minLat;
//		maxLat = null==maxLat?0:maxLat;
//		minLng = null==minLng?0:minLng;
//		maxLng = null==maxLng?0:maxLng;

//		System.out.println(minLat);

        result.setSouthBound(minLat);
        result.setNorthBound(maxLat);
        result.setWestBound(minLng);
        result.setEastBound(maxLng);

        return result;
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();

        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;

        switch (code) {
            case SurveyHeatMapManager.ERROR_NO_LOCATION_FOUND:
                this.setMessage(request, "tracking.nohistory", null);
                break;

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                try {
                    String loginBranch = this.getLoginBean(request).getBranchId();
                    this.loadBranch(request, loginBranch);
                    this.loadComboScheme(request);
                } catch (CoreException e) {
                    e.printStackTrace();
                    this.logger.printStackTrace(e);
                }
                break;

            default:
                this.logger.printStackTrace(ex);
                super.handleException(mapping, form, request, response, ex);
                break;
        }

        ((SurveyHeatMapForm) form).setTask(((SurveyHeatMapForm) form).resolvePreviousTask());
        String task = ((SurveyHeatMapForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }


}
