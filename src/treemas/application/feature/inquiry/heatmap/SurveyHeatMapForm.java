package treemas.application.feature.inquiry.heatmap;

import treemas.base.inquiry.InquiryFormBase;

public class SurveyHeatMapForm extends InquiryFormBase {
    private String gpsLatitude;
    private String gpsLongitude;

    private String southBound = "0";
    private String westBound = "0";
    private String northBound = "0";
    private String eastBound = "0";

    private SurveyHeatMapSearch search = new SurveyHeatMapSearch();

    public String getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(String gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public String getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(String gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public String getSouthBound() {
        return southBound;
    }

    public void setSouthBound(String southBound) {
        this.southBound = southBound;
    }

    public String getWestBound() {
        return westBound;
    }

    public void setWestBound(String westBound) {
        this.westBound = westBound;
    }

    public String getNorthBound() {
        return northBound;
    }

    public void setNorthBound(String northBound) {
        this.northBound = northBound;
    }

    public String getEastBound() {
        return eastBound;
    }

    public void setEastBound(String eastBound) {
        this.eastBound = eastBound;
    }

    public SurveyHeatMapSearch getSearch() {
        return search;
    }

    public void setSearch(SurveyHeatMapSearch search) {
        this.search = search;
    }
}
