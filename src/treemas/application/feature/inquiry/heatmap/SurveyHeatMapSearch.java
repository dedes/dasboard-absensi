package treemas.application.feature.inquiry.heatmap;

import treemas.util.ibatis.SearchFormParameter;

public class SurveyHeatMapSearch extends SearchFormParameter {
    private String branch;
    private String scheme;
    private String tglStart;
    private String tglEnd;
    private String radius = "10";

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getTglStart() {
        return tglStart;
    }

    public void setTglStart(String tglStart) {
        this.tglStart = tglStart;
    }

    public String getTglEnd() {
        return tglEnd;
    }

    public void setTglEnd(String tglEnd) {
        this.tglEnd = tglEnd;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }
}
