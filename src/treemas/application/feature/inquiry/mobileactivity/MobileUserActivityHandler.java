package treemas.application.feature.inquiry.mobileactivity;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.feature.general.ComboManager;
import treemas.base.inquiry.InquiryActionBase;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class MobileUserActivityHandler extends InquiryActionBase {
    private MobileUserActivityManager manager = new MobileUserActivityManager();
    private ComboManager comboManager = new ComboManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        MobileUserActivityForm frm = (MobileUserActivityForm) form;
        String task = frm.getTask();
        frm.getPaging().calculationPage();
        String job = frm.getJob();
        if (Global.WEB_TASK_SHOW.equals(task)) {
            String loginUserId = this.getLoginBean(request).getLoginId();
            this.loadMultiBranch(request, loginUserId);
            String branchId = this.getFirstMultiBranchByLogin(loginUserId);
            frm.setSearchBranch(branchId);
            this.loadList(request, frm, job);
        } else if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)) {
            this.loadList(request, frm, job);
        } else if (Global.WEB_TASK_PREVIEW.equals(task)) {
            this.loadDetailList(request, frm, job);
        }

        return this.getNextPage(task, mapping);
    }


    private void loadList(HttpServletRequest request, MobileUserActivityForm reportForm, String job) throws CoreException {
        List list = this.manager.getReportBranchList(reportForm.getSearchBranch(), job);
        list = BeanConverter.convertBeansToString(list, MobileUserActivityForm.class);

        request.setAttribute("listReportBranch", list);

        String loginUserId = this.getLoginBean(request).getLoginId();
        this.loadMultiBranch(request, loginUserId);
    }

    private void loadDetailList(HttpServletRequest request, MobileUserActivityForm reportForm, String job) throws CoreException {
        int pageNumber = reportForm.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(reportForm.getTask()) || Global.WEB_TASK_SHOW.equals(reportForm.getTask()))
            pageNumber = 1;

        reportForm.getSearch().setPage(pageNumber);
        PageListWrapper result = this.manager.getDetailReportBranchList(
                reportForm.getSearchBranch(),
                reportForm.getSurveyor(),
                reportForm.getFilter(),
                reportForm.getSearchDate(),
                reportForm.getSearch().getStart(),
                reportForm.getSearch().getEnd(),
                job);
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, reportForm.getClass());
        reportForm.getPaging().setCurrentPageNo(pageNumber);
        reportForm.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listDetailReportBranch", list);

    }

    private void loadBranch(HttpServletRequest request, String branchId)
            throws CoreException {
        List list = comboManager.getComboCabangByLogin(branchId);
        request.setAttribute("comboBranch", list);
    }

    private void loadMultiBranch(HttpServletRequest request, String userId)
            throws CoreException {
        List list = comboManager.getComboMultiCabangByLogin(userId);
        request.setAttribute("comboBranch", list);
    }

    private String getFirstMultiBranchByLogin(String userId) throws CoreException {
        String branchId = null;
        branchId = this.manager.getFirstBranch(userId);
        return branchId;
    }
}
