package treemas.application.feature.inquiry.mobileactivity;

import treemas.base.inquiry.InquiryManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MobileUserActivityManager extends InquiryManagerBase {

    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_INQ_USER_LOG;

    public List getReportBranchList(String branch, String job) throws CoreException {
        List reportList = null;
        try {
            Map param = new HashMap();
            param.put("branch", branch);
            param.put("job", job);
            reportList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getReportUser", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_DB);
        }

        return reportList;
    }

    public PageListWrapper getDetailReportBranchList(String branch, String user,
                                                     String filter, String date, int start, int end, String job) throws CoreException {
        PageListWrapper result = new PageListWrapper();

        Map param = new HashMap();
        param.put("branch", branch);
        param.put("user", user);
        param.put("start", start);
        param.put("end", end);
        param.put("filter", filter);
        param.put("date", date);
        param.put("job", job);

        try {
            List reportList = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getDetailActivity", param);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countDetailActivity", param);
            result.setResultList(reportList);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public String getFirstBranch(String userId) {
        String branchId = null;
        try {
            branchId = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getFirstBranch", userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return branchId;
    }
}
