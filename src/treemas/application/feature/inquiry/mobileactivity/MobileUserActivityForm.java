package treemas.application.feature.inquiry.mobileactivity;

import treemas.base.inquiry.InquiryFormBase;

public class MobileUserActivityForm extends InquiryFormBase {
    private String surveyor;
    private String nama;
    private String lastLog;
    private String lastActivityLog;
    private String totalLog;
    private String searchBranch;
    private String searchBranchName;
    private String searchDate;
    private String filter;
    private String job;

    private MobileUserActivitySearch search = new MobileUserActivitySearch();

    public String getSearchBranch() {
        return searchBranch;
    }

    public void setSearchBranch(String searchBranch) {
        this.searchBranch = searchBranch;
    }

    public String getSurveyor() {
        return surveyor;
    }

    public void setSurveyor(String surveyor) {
        this.surveyor = surveyor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getLastLog() {
        return lastLog;
    }

    public void setLastLog(String lastLog) {
        this.lastLog = lastLog;
    }

    public String getLastActivityLog() {
        return lastActivityLog;
    }

    public void setLastActivityLog(String lastActivityLog) {
        this.lastActivityLog = lastActivityLog;
    }

    public String getTotalLog() {
        return totalLog;
    }

    public void setTotalLog(String totalLog) {
        this.totalLog = totalLog;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public MobileUserActivitySearch getSearch() {
        return search;
    }

    public void setSearch(MobileUserActivitySearch search) {
        this.search = search;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }

    public String getSearchBranchName() {
        return searchBranchName;
    }

    public void setSearchBranchName(String searchBranchName) {
        this.searchBranchName = searchBranchName;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

}
