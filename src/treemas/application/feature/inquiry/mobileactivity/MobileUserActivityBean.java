package treemas.application.feature.inquiry.mobileactivity;

import java.io.Serializable;

public class MobileUserActivityBean implements Serializable {
    private String surveyor;
    private String nama;
    private String lastLog;
    private String lastActivityLog;
    private Integer totalLog;


    public String getSurveyor() {
        return surveyor;
    }

    public void setSurveyor(String surveyor) {
        this.surveyor = surveyor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getLastLog() {
        return lastLog;
    }

    public void setLastLog(String lastLog) {
        this.lastLog = lastLog;
    }

    public String getLastActivityLog() {
        return lastActivityLog;
    }

    public void setLastActivityLog(String lastActivityLog) {
        this.lastActivityLog = lastActivityLog;
    }

    public Integer getTotalLog() {
        return totalLog;
    }

    public void setTotalLog(Integer totalLog) {
        this.totalLog = totalLog;
    }


}
