package treemas.application.feature.lookup.dynamic;

import treemas.base.lookup.LookupManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;


public class LuDinamicManager extends LookupManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_LU_DYNAMIC;
    private static final String FALSE = "0";

    public LuDinamicBean getHeader(Integer mobileAssignmentId) throws CoreException {

        LuDinamicBean bean = null;

        try {
            bean = (LuDinamicBean) this.ibatisSqlMap.queryForObject(SQLConstant.SQL_FEATURE_VIEW_ASSIGNMENT_SURVEYOR + "getHeader", mobileAssignmentId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return bean;
    }

    public List getCriteria(String luCode) throws CoreException {
        List list = null;

        try {
            list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getCriteria", luCode);
        } catch (SQLException sqle) {
            System.out.println("error getCriteria");
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return list;
    }

    public PageListWrapper getLookupResult(int numberPage,
                                           LuDinamicSearch searchParam, String luCode) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        String searchValue = searchParam.getSearchValue1();


        searchValue = searchValue != null ? searchValue.trim().toUpperCase() : searchValue;
        searchParam.setPage(numberPage);
        List list = null;

        try {
            if (luCode.equals("001")) {
                list = this.ibatisSqlMap.queryForList(
                        this.STRING_SQL + "getZipCode", searchParam);
                result.setResultList(list);
                System.out.println("size: " + list.size());
            } else if (luCode.equals("002")) {

            } else if (luCode.equals("003")) {

            } else if (luCode.equals("004")) {

            }
            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", searchParam);
            result.setTotalRecord(tmp.intValue());

        } catch (SQLException sqle) {
            System.out.println("error getLookup Result");
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }
        return result;
    }

}
