package treemas.application.feature.lookup.dynamic;

import treemas.util.ibatis.SearchParameter;

public class LuDinamicSearch extends SearchParameter {
    private String lookupCode;
    private String lookupField;
    private String lookupDisplayName;
    private String nameLookup;
    private String branchId;

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getLookupCode() {
        return lookupCode;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }

    public String getLookupField() {
        return lookupField;
    }

    public void setLookupField(String lookupField) {
        this.lookupField = lookupField;
    }

    public String getLookupDisplayName() {
        return lookupDisplayName;
    }

    public void setLookupDisplayName(String lookupDisplayName) {
        this.lookupDisplayName = lookupDisplayName;
    }

    public String getNameLookup() {
        return nameLookup;
    }

    public void setNameLookup(String nameLookup) {
        this.nameLookup = nameLookup;
    }

}
