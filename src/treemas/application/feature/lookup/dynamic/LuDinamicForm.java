package treemas.application.feature.lookup.dynamic;

import treemas.base.lookup.LookupFormBase;

public class LuDinamicForm extends LookupFormBase {

    private String lookupCode;
    private String lookupField;
    private String lookupDisplayName;
    private String kodeDisplayLookup;
    private String descriptionLookup;
    private String nameLookup;
    private String textFieldLookup;
    private LuDinamicSearch search = new LuDinamicSearch();

    public String getTextFieldLookup() {
        return textFieldLookup;
    }

    public void setTextFieldLookup(String textFieldLookup) {
        this.textFieldLookup = textFieldLookup;
    }

    public String getNameLookup() {
        return nameLookup;
    }

    public void setNameLookup(String nameLookup) {
        this.nameLookup = nameLookup;
    }

    public String getKodeDisplayLookup() {
        return kodeDisplayLookup;
    }

    public void setKodeDisplayLookup(String kodeDisplayLookup) {
        this.kodeDisplayLookup = kodeDisplayLookup;
    }

    public String getDescriptionLookup() {
        return descriptionLookup;
    }

    public void setDescriptionLookup(String descriptionLookup) {
        this.descriptionLookup = descriptionLookup;
    }

    public LuDinamicSearch getSearch() {
        return search;
    }

    public void setSearch(LuDinamicSearch search) {
        this.search = search;
    }

    public String getLookupCode() {
        return lookupCode;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }

    public String getLookupField() {
        return lookupField;
    }

    public void setLookupField(String lookupField) {
        this.lookupField = lookupField;
    }

    public String getLookupDisplayName() {
        return lookupDisplayName;
    }

    public void setLookupDisplayName(String lookupDisplayName) {
        this.lookupDisplayName = lookupDisplayName;
    }
}
