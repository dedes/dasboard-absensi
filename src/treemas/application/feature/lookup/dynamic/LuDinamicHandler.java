package treemas.application.feature.lookup.dynamic;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.lookup.LookupActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LuDinamicHandler extends LookupActionBase {
    private LuDinamicManager manager = new LuDinamicManager();
    private Map cordConverterMap = new HashMap();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {

        LuDinamicForm luDinamicForm = (LuDinamicForm) form;
        String action = luDinamicForm.getTask();
        luDinamicForm.getPaging().calculationPage();
        String luCode = request.getParameter("lookupCode");
        String nameLookup = request.getParameter("nameLookup");
        if (Global.WEB_TASK_SHOW.equals(action)) {
            loadCriteria(request, luCode);
        } else if (Global.WEB_TASK_LOAD.equals(action)
                || (Global.WEB_TASK_SEARCH.equals(action))) {
            load(request, luDinamicForm, luCode);
        }

        return mapping.findForward("view");
    }

    private void load(HttpServletRequest request, LuDinamicForm form,
                      String kodeLookup) throws CoreException {
        int targetPageNo = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask())) {
            targetPageNo = 1;
        }
        PageListWrapper result = null;
        result = this.manager.getLookupResult(targetPageNo, form.getSearch(),
                kodeLookup);
        List list = result.getResultList();

        list = BeanConverter.convertBeansToString(list, LuDinamicForm.class);
        form.getPaging().setCurrentPageNo(targetPageNo);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("listLookupResult", list);
        loadCriteria(request, kodeLookup);
    }

    private void loadCriteria(HttpServletRequest request, String luCod)
            throws CoreException {
        List listCriteria = this.manager.getCriteria(luCod);
        if (listCriteria != null) {
            listCriteria = BeanConverter.convertBeansToString(listCriteria,
                    LuDinamicForm.class, cordConverterMap);
        }
        request.setAttribute("listCriteria", listCriteria);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();
        switch (code) {
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        return mapping.findForward("view");
    }
}
