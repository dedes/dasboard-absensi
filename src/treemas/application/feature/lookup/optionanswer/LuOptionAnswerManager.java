package treemas.application.feature.lookup.optionanswer;

import treemas.base.lookup.LookupManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LuOptionAnswerManager extends LookupManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_LU_OPTION_ANSWER;

    public PageListWrapper getAll(Integer questionGroupId, Integer questionId)
            throws CoreException {
        PageListWrapper result = new PageListWrapper();

        Map paramMap = new HashMap();
        paramMap.put("questionGroupId", questionGroupId);
        paramMap.put("questionId", questionId);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllActive", paramMap);
            result.setResultList(list);
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
        return result;
    }

    public LuOptionAnswerBean getHeader(Integer questionGroupId, Integer questionId)
            throws CoreException {
        LuOptionAnswerBean result = null;

        Map paramMap = new HashMap();
        paramMap.put("questionGroupId", questionGroupId);
        paramMap.put("questionId", questionId);

        try {
            result = (LuOptionAnswerBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getHeader", paramMap);
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }

        return result;
    }
}
