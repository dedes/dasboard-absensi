package treemas.application.feature.lookup.optionanswer;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.lookup.LookupActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class LuOptionAnswerHandler extends LookupActionBase {
    private LuOptionAnswerManager manager = new LuOptionAnswerManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {

        LuOptionAnswerForm luOptionAnswerForm = (LuOptionAnswerForm) form;
        String action = luOptionAnswerForm.getTask();
        luOptionAnswerForm.getPaging().calculationPage();

        if (Global.WEB_TASK_SHOW.equals(action)
                || Global.WEB_TASK_LOAD.equals(action)
                || Global.WEB_TASK_SEARCH.equals(action)) {
            this.getHeader(luOptionAnswerForm);
            this.load(request, luOptionAnswerForm);
        }

        return mapping.findForward("view");
    }

    private void load(HttpServletRequest request, LuOptionAnswerForm form)
            throws CoreException {
        PageListWrapper result = this.manager.getAll(
                new Integer(form.getQuestionGroupId()),
                new Integer(form.getQuestionId()));
        List list = result.getResultList();
        if (list != null) {
            BeanConverter.convertBeansToString(list, LuOptionAnswerForm.class);
        }
        request.setAttribute("listOptionAnswer", list);
    }

    private void getHeader(LuOptionAnswerForm form) throws CoreException {
        LuOptionAnswerBean bean = null;
        bean = this.manager.getHeader(new Integer(form.getQuestionGroupId()),
                new Integer(form.getQuestionId()));
        if (bean != null) {
            BeanConverter.convertBeanToString(bean, form);
        }
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();
        switch (code) {
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        return mapping.findForward("view");
    }
}
