package treemas.application.feature.lookup.optionanswer;

import treemas.base.lookup.LookupFormBase;

public class LuOptionAnswerForm extends LookupFormBase {
    private String questionGroupId;
    private String questionGroupName;
    private String questionId;
    private String questionLabel;
    private String optionAnswerId;
    private String optionAnswerLabel;

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public String getOptionAnswerId() {
        return optionAnswerId;
    }

    public void setOptionAnswerId(String optionAnswerId) {
        this.optionAnswerId = optionAnswerId;
    }

    public String getOptionAnswerLabel() {
        return optionAnswerLabel;
    }

    public void setOptionAnswerLabel(String optionAnswerLabel) {
        this.optionAnswerLabel = optionAnswerLabel;
    }
}
