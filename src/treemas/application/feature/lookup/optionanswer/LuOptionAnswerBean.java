package treemas.application.feature.lookup.optionanswer;

import java.io.Serializable;

public class LuOptionAnswerBean implements Serializable {
    private Integer questionGroupId;
    private String questionGroupName;
    private Integer questionId;
    private String questionLabel;
    private Integer optionAnswerId;
    private String optionAnswerLabel;

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public Integer getOptionAnswerId() {
        return optionAnswerId;
    }

    public void setOptionAnswerId(Integer optionAnswerId) {
        this.optionAnswerId = optionAnswerId;
    }

    public String getOptionAnswerLabel() {
        return optionAnswerLabel;
    }

    public void setOptionAnswerLabel(String optionAnswerLabel) {
        this.optionAnswerLabel = optionAnswerLabel;
    }
}
