package treemas.application.feature.lookup.questions;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.feature.view.question.ViewQuestionSetForm;
import treemas.base.lookup.LookupActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.List;


public class LuQuestionSetHandler extends LookupActionBase {
    private LuQuestionSetManager manager = new LuQuestionSetManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {

        LuQuestionSetForm luQuestionSetForm = (LuQuestionSetForm) form;
        String action = luQuestionSetForm.getTask();
        luQuestionSetForm.getPaging().calculationPage();

        if (Global.WEB_TASK_SHOW.equals(action)
                || Global.WEB_TASK_LOAD.equals(action)
                || Global.WEB_TASK_SEARCH.equals(action)) {
            this.getHeader(luQuestionSetForm);
            this.load(request, luQuestionSetForm);
        }

        return mapping.findForward("view");
    }

    private void load(HttpServletRequest request, LuQuestionSetForm form)
            throws CoreException {
        PageListWrapper result = this.manager.getAll(form.getSchemeId());
        List list = result.getResultList();
        if (list != null) {
            for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
                List tempList = (List) iterator.next();
                tempList = BeanConverter.convertBeansToString(tempList, ViewQuestionSetForm.class);
            }
        }
        request.setAttribute("listQuestionSet", list);
    }

    private void getHeader(LuQuestionSetForm luQuestionSetForm)
            throws CoreException {
        LuQuestionSetBean bean = null;
        bean = this.manager.getHeader(luQuestionSetForm.getSchemeId());
        if (bean != null) {
            BeanConverter.convertBeanToString(bean, luQuestionSetForm);
        }
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();
        switch (code) {
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        return mapping.findForward("view");
    }
}
