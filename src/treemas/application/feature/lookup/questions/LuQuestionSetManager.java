package treemas.application.feature.lookup.questions;

import treemas.base.lookup.LookupManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LuQuestionSetManager extends LookupManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_LU_QUESTIONS;

    public LuQuestionSetBean getHeader(String schemeId) throws CoreException {
        LuQuestionSetBean bean = null;

        try {
            bean = (LuQuestionSetBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getHeader", schemeId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return bean;
    }

    public PageListWrapper getAll(String schemeId)
            throws CoreException {
        PageListWrapper result = new PageListWrapper();

        try {
            List listQuestionGroup = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getQuestionGroup", schemeId);

            List list = this.getQuestionSet(listQuestionGroup);
            result.setResultList(list);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (Exception ex) {
            this.logger.printStackTrace(ex);
            throw new CoreException(ex, ERROR_UNKNOWN);
        }

        return result;
    }

    private List getQuestionSet(List listQuestionGroup) throws SQLException {
        List list = new ArrayList();

        for (Iterator iterator = listQuestionGroup.iterator(); iterator.hasNext(); ) {
            Integer questionGroupId = (Integer) iterator.next();
            List tempList = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getQuestionSet", questionGroupId);
            list.add(tempList);
        }

        return list;
    }
}
