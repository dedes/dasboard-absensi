package treemas.application.feature.lookup.branch;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.lookup.LookupActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class LuBranchHandler extends LookupActionBase {
    private LuBranchManager manager = new LuBranchManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {

        LuBranchForm luBranchForm = (LuBranchForm) form;
        String action = luBranchForm.getTask();
        luBranchForm.getPaging().calculationPage();

        if (Global.WEB_TASK_SHOW.equals(action)) {
        } else if (Global.WEB_TASK_LOAD.equals(action)
                || (Global.WEB_TASK_SEARCH.equals(action))) {
            load(request, luBranchForm);
        }

        return mapping.findForward("view");
    }

    private void load(HttpServletRequest request, LuBranchForm form)
            throws CoreException {
        int targetPageNo = form.getPaging().getTargetPageNo();
        int searchType1 = Integer.parseInt(form.getSearchType1());

        if (Global.WEB_TASK_SEARCH.equals(form.getTask())) {
            targetPageNo = 1;
        }
        PageListWrapper result = null;

        if ("LG".equals(form.getTipe())) {
            result = this.manager.getAll(targetPageNo, searchType1,
                    form.getSearchValue1(), this.getLoginBean(request));
        } else if ("ALL".equals(form.getTipe())) {
            result = this.manager.getAll(targetPageNo, searchType1, form.getSearchValue1());
        } else if ("EXC".equals(form.getTipe())) {
            result = this.manager.getAll(targetPageNo, searchType1, form.getSearchValue1(), form.getBranchId());
        }

        List list = result.getResultList();

        list = BeanConverter.convertBeansToString(list, LuBranchForm.class);
        form.getPaging().setCurrentPageNo(targetPageNo);
        form.getPaging().setTotalRecord(result.getTotalRecord());

        request.setAttribute("list", list);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();
        switch (code) {
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        return mapping.findForward("view");
    }
}
