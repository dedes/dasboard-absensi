package treemas.application.feature.lookup.branch;

import treemas.base.lookup.LookupManagerBase;
import treemas.globalbean.SessionBean;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public class LuBranchManager extends LookupManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_LU_BRANCH;

    private static final int BRANCH_ID = 10;
    private static final int BRANCH_NAME = 20;

    public PageListWrapper getAll(int numberPage, int searchType1,
                                  String searchValue1, SessionBean loginBean)
            throws CoreException {
        PageListWrapper result = new PageListWrapper();
        SearchFormParameter param = new SearchFormParameter();

        if (searchType1 != BRANCH_ID && searchType1 != BRANCH_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: "
                    + searchType1);

        searchValue1 = searchValue1 != null ? searchValue1.trim().toUpperCase() : searchValue1;

        param.setPage(numberPage);
        param.setSearchValue1(searchValue1);
        param.setSearchType1(searchType1);
        Map paramMap = param.toMap();
        paramMap.put("branchLevel", loginBean.getBranchLevel());

        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAllByLogin", paramMap);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAllByLogin", paramMap);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
        return result;
    }

    public PageListWrapper getAll(int numberPage, int searchType1,
                                  String searchValue1)
            throws CoreException {
        PageListWrapper result = new PageListWrapper();
        SearchFormParameter param = new SearchFormParameter();

        if (searchType1 != BRANCH_ID && searchType1 != BRANCH_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: "
                    + searchType1);

        searchValue1 = searchValue1 != null ? searchValue1.trim().toUpperCase()
                : searchValue1;

        param.setPage(numberPage);
        param.setSearchValue1(searchValue1);
        param.setSearchType1(searchType1);

        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", param);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", param);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
        return result;
    }

    public PageListWrapper getAll(int numberPage, int searchType1,
                                  String searchValue1,
                                  String kodeCabangExclude)
            throws CoreException {
        PageListWrapper result = new PageListWrapper();
        SearchFormParameter param = new SearchFormParameter();

        if (searchType1 != BRANCH_ID && searchType1 != BRANCH_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: "
                    + searchType1);

        searchValue1 = searchValue1 != null ? searchValue1.trim().toUpperCase()
                : searchValue1;

        param.setPage(numberPage);
        param.setSearchValue1(searchValue1);
        param.setSearchType1(searchType1);
        Map paramMap = param.toMap();
        paramMap.put("cabangExclude", kodeCabangExclude);

        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAllExclude", paramMap);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAllExclude", paramMap);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
        return result;
    }
}
