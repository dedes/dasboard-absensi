package treemas.application.feature.lookup.branch;

import java.io.Serializable;

public class LuBranchBean implements Serializable {
    private String branchId;
    private String branchName;

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
}
