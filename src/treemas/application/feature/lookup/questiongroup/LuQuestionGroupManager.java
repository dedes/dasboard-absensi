package treemas.application.feature.lookup.questiongroup;

import treemas.base.lookup.LookupManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class LuQuestionGroupManager extends LookupManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_LU_QUESTION_GROUP;
    private static final int QUESTION_GROUP_NAME = 10;

    public PageListWrapper getAll(int numberPage, int searchType1,
                                  String searchValue1, String schemeId) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        SearchFormParameter param = new SearchFormParameter();

        if (searchType1 != QUESTION_GROUP_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: "
                    + searchType1);

        searchValue1 = searchValue1 != null ? searchValue1.trim().toUpperCase()
                : searchValue1;

        param.setPage(numberPage);
        param.setSearchValue1(searchValue1);
        param.setSearchType1(searchType1);
        Map paramMap = param.toMap();
        paramMap.put("schemeId", schemeId);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL
                    + "getAll", paramMap);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", paramMap);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
        return result;
    }
}
