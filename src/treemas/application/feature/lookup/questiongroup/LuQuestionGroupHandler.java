package treemas.application.feature.lookup.questiongroup;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.lookup.LookupActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class LuQuestionGroupHandler extends LookupActionBase {
    private LuQuestionGroupManager manager = new LuQuestionGroupManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {

        LuQuestionGroupForm luQuestionGroupForm = (LuQuestionGroupForm) form;
        String action = luQuestionGroupForm.getTask();
        luQuestionGroupForm.getPaging().calculationPage();

        if (Global.WEB_TASK_SHOW.equals(action)) {
        } else if (Global.WEB_TASK_LOAD.equals(action)
                || (Global.WEB_TASK_SEARCH.equals(action))) {
            load(request, luQuestionGroupForm);
        }

        return mapping.findForward("view");
    }

    private void load(HttpServletRequest request, LuQuestionGroupForm form)
            throws CoreException {
        int targetPageNo = form.getPaging().getTargetPageNo();
        int searchType1 = Integer.parseInt(form.getSearchType1());

        if (Global.WEB_TASK_SEARCH.equals(form.getTask())) {
            targetPageNo = 1;
        }

        PageListWrapper result = manager.getAll(targetPageNo, searchType1,
                form.getSearchValue1(),
                form.getSchemeId());
        List list = result.getResultList();

        list = BeanConverter.convertBeansToString(list, LuQuestionGroupForm.class);
        form.getPaging().setCurrentPageNo(targetPageNo);
        form.getPaging().setTotalRecord(result.getTotalRecord());

        request.setAttribute("listQuestionGroup", list);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();
        switch (code) {
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        return mapping.findForward("view");
    }
}
