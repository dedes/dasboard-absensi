package treemas.application.feature.lookup.questiongroup;

import java.io.Serializable;

public class LuQuestionGroupBean implements Serializable {
    private String questionGroupId;
    private String questionGroupName;

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }
}
