package treemas.application.feature.lookup.questiongroup;

import treemas.base.lookup.LookupFormBase;

public class LuQuestionGroupForm extends LookupFormBase {
    private String searchType1 = "10";
    private String searchValue1;
    private String questionGroupId;
    private String questionGroupName;
    private String schemeId;

    public String getSearchType1() {
        return searchType1;
    }

    public void setSearchType1(String searchType1) {
        this.searchType1 = searchType1;
    }

    public String getSearchValue1() {
        return searchValue1;
    }

    public void setSearchValue1(String searchValue1) {
        this.searchValue1 = searchValue1;
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }
}
