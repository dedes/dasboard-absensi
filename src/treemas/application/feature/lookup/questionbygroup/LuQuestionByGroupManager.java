package treemas.application.feature.lookup.questionbygroup;

import treemas.base.lookup.LookupManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LuQuestionByGroupManager extends LookupManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_LU_QUESTION_BY_GROUP;

    public PageListWrapper getAll(String questionGroupId,
                                  String excQuestionId)
            throws CoreException {
        PageListWrapper result = new PageListWrapper();

        try {
            Map paramMap = new HashMap();
            paramMap.put("questionGroupId", new Integer(questionGroupId));
            paramMap.put("excQuestionId", excQuestionId);

            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAllActive", paramMap);
            result.setResultList(list);
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
        return result;
    }
}
