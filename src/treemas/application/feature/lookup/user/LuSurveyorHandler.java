package treemas.application.feature.lookup.user;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.lookup.LookupActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class LuSurveyorHandler extends LookupActionBase {
    private LuSurveyorManager manager = new LuSurveyorManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {

        LuSurveyorForm luSurveyorForm = (LuSurveyorForm) form;
        String action = luSurveyorForm.getTask();
        luSurveyorForm.getPaging().calculationPage();

        if (Global.WEB_TASK_SHOW.equals(action)) {
        } else if (Global.WEB_TASK_LOAD.equals(action)
                || (Global.WEB_TASK_SEARCH.equals(action))) {
            load(request, luSurveyorForm);
        }

        return mapping.findForward("view");
    }

    private void load(HttpServletRequest request, LuSurveyorForm form)
            throws CoreException {
        int targetPageNo = form.getPaging().getTargetPageNo();
        int searchType1 = Integer.parseInt(form.getSearchType1());
        String jobFilter = form.getJobFilter();

        if (Global.WEB_TASK_SEARCH.equals(form.getTask())) {
            targetPageNo = 1;
        }

        PageListWrapper result = null;

        if ("ALL".equals(form.getTipe())) {
            result = manager.getAll(targetPageNo, searchType1,
                    form.getSearchValue1(), jobFilter);
        }
        // else if ("CHILD".equals(form.getTipe())) {
        // result = manager.getAllByChildBranch(targetPageNo, searchType1,
        // form.getSearchValue1(),
        // this.getLoginBean(request).getBranchId(), jobFilter);
        // }
        // else if ("ONE".equals(form.getTipe())) {
        // result = manager.getAllByBranch(targetPageNo, searchType1,
        // form.getSearchValue1(),
        // this.getLoginBean(request).getBranchId(), form.getPilih(),
        // jobFilter);
        // }
        else if ("CHILD".equals(form.getTipe())) {
            result = manager.getAllByChildBranch(targetPageNo, searchType1,
                    form.getSearchValue1(), this.getLoginBean(request)
                            .getLoginId(), jobFilter);
        } else if ("ONE".equals(form.getTipe())) {
            result = manager.getAllByBranch(targetPageNo, searchType1,
                    form.getSearchValue1(), form.getBranchId(),
                    form.getPilih(), jobFilter);
        } else if ("BRFILTER".equals(form.getTipe())) {
            result = manager.getAllByBranch(targetPageNo, searchType1,
                    form.getSearchValue1(), form.getBrfilter(),
                    form.getPilih(), jobFilter);
        }

        List list = result.getResultList();

        list = BeanConverter.convertBeansToString(list, LuSurveyorForm.class);
        form.getPaging().setCurrentPageNo(targetPageNo);
        form.getPaging().setTotalRecord(result.getTotalRecord());

        request.setAttribute("listSurveyor", list);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();
        switch (code) {
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        return mapping.findForward("view");
    }
}
