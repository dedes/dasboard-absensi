package treemas.application.feature.lookup.user;

import treemas.base.lookup.LookupFormBase;

public class LuSurveyorForm extends LookupFormBase {
    private String searchType1 = "10";
    private String searchValue1;
    private String userId;
    private String userName;
    private String branchId;
    private String branchName;
    private String tipe;
    private String pilih;
    private String brfilter;
    private String jobFilter;

    public String getSearchType1() {
        return searchType1;
    }

    public void setSearchType1(String searchType1) {
        this.searchType1 = searchType1;
    }

    public String getSearchValue1() {
        return searchValue1;
    }

    public void setSearchValue1(String searchValue1) {
        this.searchValue1 = searchValue1;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getPilih() {
        return pilih;
    }

    public void setPilih(String pilih) {
        this.pilih = pilih;
    }

    public String getBrfilter() {
        return brfilter;
    }

    public void setBrfilter(String brfilter) {
        this.brfilter = brfilter;
    }

    public String getJobFilter() {
        return jobFilter;
    }

    public void setJobFilter(String jobFilter) {
        this.jobFilter = jobFilter;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }
}
