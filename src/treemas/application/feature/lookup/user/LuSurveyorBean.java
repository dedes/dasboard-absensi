package treemas.application.feature.lookup.user;

import java.io.Serializable;

public class LuSurveyorBean implements Serializable {
    private String userId;
    private String userName;
    private String branchName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
}
