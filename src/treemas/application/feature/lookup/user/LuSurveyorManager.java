package treemas.application.feature.lookup.user;

import treemas.base.lookup.LookupManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.ibatis.SearchFormParameter;
import treemas.util.paging.PageListWrapper;
import treemas.util.tool.Tools;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class LuSurveyorManager extends LookupManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_LU_USER;
    private static final int SURVEYOR_ID = 10;
    private static final int SURVEYOR_NAME = 20;

    public PageListWrapper getAll(int numberPage, int searchType1,
                                  String searchValue1, String jobFilter) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        SearchFormParameter param = new SearchFormParameter();

        if (searchType1 != SURVEYOR_ID && searchType1 != SURVEYOR_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: "
                    + searchType1);

        searchValue1 = searchValue1 != null ? searchValue1.trim().toUpperCase()
                : searchValue1;

        param.setPage(numberPage);
        param.setSearchValue1(searchValue1);
        param.setSearchType1(searchType1);
        Map paramMap = param.toMap();
        paramMap.put("jobFilter", jobFilter);

        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAll", param);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAll", param);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
        return result;
    }

    public PageListWrapper getAllByChildBranch(int numberPage, int searchType1,
                                               String searchValue1, String userId, String jobFilter)
            throws CoreException {
        PageListWrapper result = new PageListWrapper();
        SearchFormParameter param = new SearchFormParameter();

        if (searchType1 != SURVEYOR_ID && searchType1 != SURVEYOR_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: "
                    + searchType1);

        searchValue1 = searchValue1 != null ? searchValue1.trim().toUpperCase()
                : searchValue1;

        param.setPage(numberPage);
        param.setSearchValue1(searchValue1);
        param.setSearchType1(searchType1);
        Map paramMap = param.toMap();
        paramMap.put("userId", userId);
        paramMap.put("jobFilter", jobFilter);

        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAllByChildBranch", paramMap);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAllByChildBranch", paramMap);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
        return result;
    }

    public PageListWrapper getAllByBranch(int numberPage, int searchType1,
                                          String searchValue1, String branchId, String excludedId,
                                          String jobFilter) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        SearchFormParameter param = new SearchFormParameter();

        if (searchType1 != SURVEYOR_ID && searchType1 != SURVEYOR_NAME)
            throw new IllegalArgumentException("Invalid searchType parameter: "
                    + searchType1);

        searchValue1 = searchValue1 != null ? searchValue1.trim().toUpperCase()
                : searchValue1;

        param.setPage(numberPage);
        param.setSearchValue1(searchValue1);
        param.setSearchType1(searchType1);
        Map paramMap = param.toMap();
        paramMap.put("branchId", branchId);
        if (excludedId != null && excludedId.length() > 0) {
            if (excludedId.indexOf("'") == -1) {
                String[] temp = excludedId.split(";");
                int len = temp.length;
                if (len > 1) {
                    for (int i = 0; i < len; i++) {
                        temp[i] = "'" + temp[i].trim() + "'";
                    }
                    excludedId = Tools.implode(temp, ",");
                } else {
                    excludedId = "'" + temp[0] + "'";
                }
            }
        }
        paramMap.put("pilih", excludedId);
        paramMap.put("jobFilter", jobFilter);
        try {
            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAllByBranch", paramMap);
            result.setResultList(list);

            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cntAllByBranch", paramMap);
            result.setTotalRecord(tmp.intValue());
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
        return result;
    }
}
