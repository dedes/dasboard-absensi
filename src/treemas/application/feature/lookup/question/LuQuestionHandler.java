package treemas.application.feature.lookup.question;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.lookup.LookupActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class LuQuestionHandler extends LookupActionBase {
    private LuQuestionManager manager = new LuQuestionManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {

        LuQuestionForm luQuestionForm = (LuQuestionForm) form;
        String action = luQuestionForm.getTask();
        luQuestionForm.getPaging().calculationPage();

        if (Global.WEB_TASK_SHOW.equals(action)
                || Global.WEB_TASK_LOAD.equals(action)
                || Global.WEB_TASK_SEARCH.equals(action)) {
            this.load(request, luQuestionForm);
            this.getHeader(luQuestionForm);
        }

        return mapping.findForward("view");
    }

    private void load(HttpServletRequest request, LuQuestionForm form)
            throws CoreException {
        PageListWrapper result = this.manager.getAll(form.getQuestionGroupId(),
                form.getQuestionId());
        List list = result.getResultList();
        if (list != null) {
            BeanConverter.convertBeansToString(list, LuQuestionForm.class);
        }
        request.setAttribute("listQuestion", list);
    }

    private void getHeader(LuQuestionForm form) throws CoreException {
        LuQuestionBean bean = null;
        bean = this.manager.getHeader(new Integer(form.getQuestionGroupId()));
        if (bean != null) {
            BeanConverter.convertBeanToString(bean, form);
        }
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();
        switch (code) {
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        return mapping.findForward("view");
    }
}
