package treemas.application.feature.lookup.question;

import treemas.base.lookup.LookupFormBase;

public class LuQuestionForm extends LookupFormBase {
    private String questionGroupId;
    private String questionGroupName;
    private String questionId;
    private String questionLabel;
    private String answerTypeId;
    private String answerTypeName;
    private String isMandatory;

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public String getAnswerTypeId() {
        return answerTypeId;
    }

    public void setAnswerTypeId(String answerTypeId) {
        this.answerTypeId = answerTypeId;
    }

    public String getAnswerTypeName() {
        return answerTypeName;
    }

    public void setAnswerTypeName(String answerTypeName) {
        this.answerTypeName = answerTypeName;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }
}
