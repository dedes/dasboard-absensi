package treemas.application.feature.lookup.question;

import java.io.Serializable;

public class LuQuestionBean implements Serializable {
    private Integer questionGroupId;
    private String questionGroupName;
    private Integer questionId;
    private String questionLabel;
    private String answerTypeId;
    private String answerTypeName;
    private String isMandatory;

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestionLabel() {
        return questionLabel;
    }

    public void setQuestionLabel(String questionLabel) {
        this.questionLabel = questionLabel;
    }

    public String getAnswerTypeId() {
        return answerTypeId;
    }

    public void setAnswerTypeId(String answerTypeId) {
        this.answerTypeId = answerTypeId;
    }

    public String getAnswerTypeName() {
        return answerTypeName;
    }

    public void setAnswerTypeName(String answerTypeName) {
        this.answerTypeName = answerTypeName;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }
}
