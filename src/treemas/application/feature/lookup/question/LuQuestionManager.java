package treemas.application.feature.lookup.question;

import treemas.base.lookup.LookupManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LuQuestionManager extends LookupManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_LU_QUESTION;

    public PageListWrapper getAll(String questionGroupId, String questionId)
            throws CoreException {
        PageListWrapper result = new PageListWrapper();

        try {
            Map paramMap = new HashMap();
            paramMap.put("questionGroupId", new Integer(questionGroupId));
            paramMap.put("questionId", questionId);

            List list = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getAllActive", paramMap);
            result.setResultList(list);
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }
        return result;
    }

    public LuQuestionBean getHeader(Integer questionGroupId) throws CoreException {
        LuQuestionBean result = null;

        try {
            result = (LuQuestionBean) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getHeader", questionGroupId);
        } catch (SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new CoreException(ERROR_DB);
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        }

        return result;
    }
}
