package treemas.application.menu;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import treemas.base.appadmin.AppAdminFormBase;

import javax.servlet.http.HttpServletRequest;

public class MenuForm extends AppAdminFormBase {

    private String menuId;
    private String formId;
    private String applicationId;
    private String parentId;
    private String prompt;
    private String level;
    private String result;
    private String seqOrder;
    private String active = "0";
    private String userUpd;

    private String formFileName;
    private String appName;
    private String appId;
    private String promptParent;
    private String applicationName;
    private String menuIdForm;
    private String menuLevelDetail;

    private String fieldName;
    private String searchString;
    private String fieldNameDetail;
    private String searchStringDetail;

    public MenuForm() {
        reset(null, null);
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(String seqOrder) {
        this.seqOrder = seqOrder;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getUserUpd() {
        return userUpd;
    }

    public void setUserUpd(String userUpd) {
        this.userUpd = userUpd;
    }

    public String getFormFileName() {
        return formFileName;
    }

    public void setFormFileName(String formFileName) {
        this.formFileName = formFileName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPromptParent() {
        return promptParent;
    }

    public void setPromptParent(String promptParent) {
        this.promptParent = promptParent;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getMenuIdForm() {
        return menuIdForm;
    }

    public void setMenuIdForm(String menuIdForm) {
        this.menuIdForm = menuIdForm;
    }

    public String getMenuLevelDetail() {
        return menuLevelDetail;
    }

    public void setMenuLevelDetail(String menuLevelDetail) {
        this.menuLevelDetail = menuLevelDetail;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getFieldNameDetail() {
        return fieldNameDetail;
    }

    public void setFieldNameDetail(String fieldNameDetail) {
        this.fieldNameDetail = fieldNameDetail;
    }

    public String getSearchStringDetail() {
        return searchStringDetail;
    }

    public void setSearchStringDetail(String searchStringDetail) {
        this.searchStringDetail = searchStringDetail;
    }

    public void setDetailHeaderForm(MenuBean bean) {
        this.setMenuLevelDetail(String.valueOf(bean.getMenuLevelDetail()));
        this.setPromptParent(bean.getPromptParent());
        this.setApplicationName(bean.getApplicationName());
        this.setMenuIdForm(bean.getMenuIdForm());
        this.setFormFileName(bean.getFormFileName());
        this.setApplicationId(bean.getApplicationId());
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        this.setTask(treemas.util.constant.Global.WEB_TASK_LOAD);
        fieldName = "";
        searchString = "";
        promptParent = "";
        applicationName = "";
        menuIdForm = "";
        formFileName = "";
        menuLevelDetail = "0";

        menuId = "";
        formId = "";
        applicationId = "";
        parentId = "";
        prompt = "";
        level = "0";
        result = "";
        seqOrder = "0";
        active = "0";
    }
}
