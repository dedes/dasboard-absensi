package treemas.application.menu;

import treemas.util.auditrail.Auditable;

import java.io.Serializable;
import java.util.List;

public class MenuBean implements Serializable, Auditable {
    private static final String[] AUDITCOLUMNS = new String[]{"menuId",
            "formId", "applicationId", "parentId", "prompt", "level",
            "result", "order", "active"};
    private static final String[] AUDITDBCOLUMNS = new String[]{"MENUID",
            "FORMID", "APPID", "PARENTID", "PROMPT", "LEVEL",
            "RESULT", "ORDER", "ACTIVE"};

    private static final String AUDITTABLENAME = "SYS_APPMENU";
    private static final String[] AUDITPRIMARYKEYS = new String[]{"menuId"};

    private String menuId;
    private String formId;
    private String applicationId;
    private String parentId;
    private String prompt;
    private int level;
    private String result;
    private int seqOrder;
    private String active = "0";
    private String userUpd;

    private String formFileName;
    private String appName;
    private String appId;
    private String promptParent;
    private String applicationName;
    private String menuIdForm;
    private int menuLevelDetail;

    private List<MenuChild> child;
    
    public MenuBean() {
    }

    public String[] getAuditColumns() {
        return AUDITCOLUMNS;
    }

    public String[] getAuditDBColumns() {
        return AUDITDBCOLUMNS;
    }

    public String getAuditTableName() {
        return AUDITTABLENAME;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDITPRIMARYKEYS;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUserUpd() {
        return userUpd;
    }

    public void setUserUpd(String userUpd) {
        this.userUpd = userUpd;
    }

    public String getFormFileName() {
        return formFileName;
    }

    public void setFormFileName(String formFileName) {
        this.formFileName = formFileName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPromptParent() {
        return promptParent;
    }

    public void setPromptParent(String promptParent) {
        this.promptParent = promptParent;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getMenuIdForm() {
        return menuIdForm;
    }

    public void setMenuIdForm(String menuIdForm) {
        this.menuIdForm = menuIdForm;
    }

    public int getMenuLevelDetail() {
        return menuLevelDetail;
    }

    public void setMenuLevelDetail(int menuLevelDetail) {
        this.menuLevelDetail = menuLevelDetail;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(int seqOrder) {
        this.seqOrder = seqOrder;
    }

	public List<MenuChild> getChild() {
		return child;
	}

	public void setChild(List<MenuChild> child) {
		this.child = child;
	}
    
    

}
