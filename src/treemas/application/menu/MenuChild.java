package treemas.application.menu;

import java.io.Serializable;

public class MenuChild implements Serializable {
	private String formId;
	private String parentId;
	private String prompt;
	private int level;
	private String result;
	private int seqOrder;
	private String formFileName;
	
	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}
	
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getPrompt() {
		return prompt;
	}
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public int getSeqOrder() {
		return seqOrder;
	}
	public void setSeqOrder(int seqOrder) {
		this.seqOrder = seqOrder;
	}
	public String getFormFileName() {
		return formFileName;
	}
	public void setFormFileName(String formFileName) {
		this.formFileName = formFileName;
	}
	
}
