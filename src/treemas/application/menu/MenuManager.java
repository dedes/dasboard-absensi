package treemas.application.menu;

import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.auditrail.AuditTrailManager;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.OracleErrorCode;
import treemas.util.ibatis.SearchParameter;
import treemas.util.log.ILogger;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuManager extends AppAdminManagerBase {

    public static final String STRING_SQL = SQLConstant.SQL_MENU;
    static final int ERROR_IN_USE = 20;
    static final int SEARCH_BY_PROMPT = 1;
    static final int SEARCH_BY_MENUID = 2;
    static final int HEADER_ADD_ROOT = 10;
    static final int HEADER_ADD_CHILD = 20;
    static final int ALL_DETAIL_PROMPT = 50;
    static final int ALL_DETAIL_MENUID = 60;
    static final int ALL_DETAIL_FORMFILENAME = 70;
    private AuditTrailManager auditManager = new AuditTrailManager();

    public MenuManager() {
    }

    public List getAllRecords(int searchType, String searchValue, String appId)
            throws AppException {

        List result = null;
        if (searchType != SEARCH_BY_PROMPT && searchType != SEARCH_BY_MENUID)
            throw new IllegalArgumentException("Invalid search type "
                    + searchType);

        HashMap param = new HashMap();
        param.put("searchValue", searchValue);
        param.put("searchType", new Integer(searchType));
        param.put("applicationId", appId);
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "menulist",
                    param);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new AppException(sqle, ERROR_DB);
        }
        return result;
    }

    public MenuBean getSingleRecord(String menuId) throws AppException {

        MenuBean result = null;
        try {
            result = (MenuBean) this.ibatisSqlMap.queryForObject(STRING_SQL
                    + "menuget", menuId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new AppException(sqle, ERROR_DB);
        }
        return result;
    }

    public void insertRecord(MenuBean beanMenu, String loginId, String screenId)
            throws AppException {

        try {
            beanMenu.setUserUpd(loginId);
            if (beanMenu.getParentId() == null
                    || beanMenu.getParentId().equals(""))
                beanMenu.setParentId("0");
            this.ibatisSqlMap.startTransaction();

            this.auditManager.auditAdd(
                    this.ibatisSqlMap.getCurrentConnection(), beanMenu,
                    loginId, screenId);

            this.ibatisSqlMap.insert(STRING_SQL + "menuinsert", beanMenu);
            this.ibatisSqlMap.commitTransaction();
            this.logger.log(ILogger.LEVEL_INFO,
                    "Added menu " + beanMenu.getMenuId());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new AppException("", sqle, ERROR_DATA_EXISTS,
                        beanMenu.getMenuId());
            } else {
                throw new AppException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (AppException aie) {
            throw aie;
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new AppException(e, ERROR_UNKNOWN);
        } finally {
            try {
                this.ibatisSqlMap.endTransaction();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateRecord(MenuBean beanMenu, String parentMenuId,
                             String loginId, String screenId) throws AppException {

        beanMenu.setUserUpd(loginId);
        try {
            this.ibatisSqlMap.startTransaction();

            this.auditManager.auditEdit(
                    this.ibatisSqlMap.getCurrentConnection(), beanMenu,
                    loginId, screenId);

            this.ibatisSqlMap.insert(STRING_SQL + "menuupdate", beanMenu);
            this.ibatisSqlMap.commitTransaction();
            this.logger.log(ILogger.LEVEL_INFO,
                    "Updated menu " + beanMenu.getMenuId());
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new AppException("", sqle, ERROR_DATA_EXISTS,
                        beanMenu.getMenuId());
            } else {
                throw new AppException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (AppException aie) {
            throw aie;
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new AppException(e, ERROR_UNKNOWN);
        } finally {
            try {
                this.ibatisSqlMap.endTransaction();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteMenu(String menuId, String loginId, String screenId)
            throws AppException {

        try {
            this.ibatisSqlMap.startTransaction();
            if (!this.isDeleteAble(menuId))
                throw new AppException(ERROR_HAS_CHILD);

            MenuBean bean = new MenuBean();
            bean.setMenuId(menuId);
            this.auditManager.auditDelete(
                    this.ibatisSqlMap.getCurrentConnection(), bean, loginId,
                    screenId);

            this.ibatisSqlMap.delete(STRING_SQL + "menudelete", menuId);
            this.ibatisSqlMap.commitTransaction();
            this.logger.log(ILogger.LEVEL_INFO, "Deleted menu " + menuId);
        } catch (SQLException sqle) {
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_CHILD_FOUND) {
                throw new AppException("", sqle, ERROR_HAS_CHILD, menuId);
            } else {
                throw new AppException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (AppException aie) {
            throw aie;
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new AppException(e, ERROR_UNKNOWN);
        } finally {
            try {
                this.ibatisSqlMap.endTransaction();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isDeleteAble(String menuId) throws SQLException {
        Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(STRING_SQL
                + "menugetCntChild", menuId);
        return (tmp.intValue() == 0);
    }

    public List getApplicationName() throws AppException {

        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(
                    "appmgr.application.listForCombo", null);
        } catch (java.sql.SQLException sqe) {
            this.logger.printStackTrace(sqe);
            throw new AppException(ERROR_DB);
        }
        return result;
    }

    public MenuBean getHeader(int headerSearchType, String appIdOrMenuId)
            throws AppException {

        MenuBean result = null;
        if (headerSearchType != HEADER_ADD_ROOT
                && headerSearchType != HEADER_ADD_CHILD)
            throw new IllegalArgumentException("Illegal header search type "
                    + headerSearchType);
        try {
            SearchParameter param = new SearchParameter();
            param.setSearchType1(headerSearchType);
            param.setSearchValue1(appIdOrMenuId);
            result = (MenuBean) this.ibatisSqlMap.queryForObject(STRING_SQL
                    + "menugetHeader", param);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new AppException(ERROR_DB);
        }
        return result;
    }

    public List getAllDetail(int allDetailType, String appId,
                             String parentMenuId, String searchValue) throws AppException {

        List result = null;
        try {
            SearchParameter param = new SearchParameter();
            if (allDetailType != ALL_DETAIL_PROMPT
                    && allDetailType != ALL_DETAIL_MENUID
                    && allDetailType != ALL_DETAIL_FORMFILENAME)
                throw new IllegalArgumentException(
                        "Invalid allDetailType parameter " + allDetailType);

            param.setSearchType1(allDetailType);
            param.setSearchValue1(searchValue);
            Map map = param.toMap();
            map.put("applicationId", appId);
            map.put("parentMenuId", parentMenuId);
            result = this.ibatisSqlMap.queryForList(STRING_SQL
                    + "menugetAllDetail", map);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new AppException(ERROR_DB);
        }
        return result;
    }

    public void deleteDetail(String menuId, String loginId, String screenId)
            throws AppException {

        this.deleteMenu(menuId, loginId, screenId);
    }

    public MenuBean getHeaderDetail(String menuId) throws AppException {

        MenuBean result = null;
        try {
            result = (MenuBean) this.ibatisSqlMap.queryForObject(STRING_SQL
                    + "menugetHeaderDetail", menuId);

        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new AppException(ERROR_DB);
        }
        return result;
    }

    public boolean isNonActiveAble(String menuId) throws AppException {

        boolean result = false;
        try {
            Integer tmp = (Integer) this.ibatisSqlMap.queryForObject(STRING_SQL
                    + "menugetCntActive", menuId);
            result = (tmp.intValue() == 0);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new AppException(ERROR_DB);
        }
        return result;
    }

}
