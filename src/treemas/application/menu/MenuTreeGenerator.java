package treemas.application.menu;

import treemas.base.appadmin.AppAdminManagerBase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class MenuTreeGenerator extends AppAdminManagerBase {

    public String getMenu(String userId, String contextPath) {

        int numLevel = 0;
        String sPrompt = null, sMenuId = null, sParentMenuId = null, sAppId = null;
        StringBuffer realNode = new StringBuffer();

        try {
            List list = this.ibatisSqlMap.queryForList(
                    "directory.menu.getRootMenu", null);
            List listRoot = new ArrayList(list.size());
            sParentMenuId = "";

            realNode.append("foldersTree = gFld(\" Application Directory \", null, 0);\n");

            for (int i = 0; i < list.size(); i++) {
                String[] sRoot = new String[2];
                MenuBean bean = (MenuBean) list.get(i);
                sMenuId = bean.getMenuId();
                sPrompt = bean.getPrompt();
                numLevel = bean.getLevel();
                sAppId = bean.getAppId();

                if (sPrompt == null)
                    sPrompt = "";
                if (sMenuId == null)
                    sMenuId = "";
                if (sAppId == null)
                    sAppId = "";
                sPrompt.trim();
                sMenuId.trim();
                sAppId.trim();
                if (sParentMenuId.indexOf(sMenuId) == -1) {
                    sParentMenuId = sParentMenuId + sMenuId + "?";
                }
                sRoot[0] = CekCurrent(sMenuId, sPrompt, true, "", "", "",
                        numLevel);
                sRoot[1] = sAppId;
                listRoot.add(sRoot);
            }
            realNode.append(getChild(listRoot, sParentMenuId, userId,
                    contextPath));
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
        }
        return realNode.toString();
    }

    private String getChild(List listRoot, String sParentMenuID, String userId,
                            String contextPath) {
        StringTokenizer sTempParent = new StringTokenizer(sParentMenuID, "?");
        StringBuffer sBackNode = new StringBuffer();
        int numLevel = 0, i = 0;
        String sPrompt = null, sMenuId = null, sResult = null;
        String sFormId = null, sFormFileName = null, sParentMenuId = null;

        HashMap map = new HashMap();
        map.put("userId", userId);
        try {
            while (sTempParent.hasMoreTokens()) {
                boolean valid = true;
                String dummyParent = sTempParent.nextToken();
                String appId = ((String[]) listRoot.get(i))[1];
                map.put("applicationId", appId);
                List list = this.ibatisSqlMap.queryForList(
                        "directory.menu.getDirMenu", map);
                for (int j = 0; j < list.size(); j++) {
                    MenuBean bean = (MenuBean) list.get(j);
                    sPrompt = bean.getPrompt();
                    sMenuId = bean.getMenuId();
                    sResult = bean.getResult();
                    numLevel = bean.getLevel();

                    sParentMenuId = bean.getParentId();
                    sFormId = bean.getFormId();
                    sFormFileName = contextPath + bean.getFormFileName();

                    if (sPrompt == null)
                        sPrompt = "";
                    if (sFormId == null)
                        sFormId = "";
                    if (sMenuId == null)
                        sMenuId = "";
                    if (sResult == null)
                        sResult = "";
                    if (sFormFileName == null)
                        sFormFileName = "";
                    else {
                        sFormFileName = sFormFileName.trim();
                    }

                    sPrompt = sPrompt.trim();
                    sMenuId = sMenuId.trim();
                    sResult = sResult.trim();
                    sFormId = sFormId.trim();

                    if (sParentMenuId.equals(dummyParent) && valid) {
                        String aux1 = ((String[]) listRoot.get(i))[0];
                        sBackNode.append(aux1);
                        valid = false;
                    }

                    if ("RD".equals(sResult)) {
                        sBackNode.append(CekCurrent(sMenuId, sPrompt, false,
                                sResult, sFormId, sFormFileName, numLevel));
                    } else if ("FD".equals(sResult)) {
                        sBackNode.append(CekCurrent(sMenuId, sPrompt, false,
                                sResult, sFormId, sFormFileName, numLevel));
                    }

                } // end-for
                i++;
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            return null;
        }
        return (sBackNode.toString());
    }

    private String CekCurrent(String sMenuId, String sPrompt, boolean bLevel1,
                              String sResult, String sFormId, String sFormFileName, int numLevel) {
        String sAction = null;
        if (bLevel1) {
            return ("aux1 = insFld(foldersTree, gFld('" + sPrompt
                    + "', null));" + "\n");
        } else {
            if (sResult.equals("RD")) {

                return ("aux" + numLevel + " = insFld(aux" + (numLevel - 1)
                        + ", gFld(\"" + sPrompt.replace('\'', '"')
                        + "\", null));" + "\n");
            }
            if (sResult.equals("FD")) {
                sAction = sFormFileName;
                if (sAction.length() == 0) {
                    sAction = "/error/underconstruction.jsp";
                }
                return ("insDoc(aux" + (numLevel - 1) + ", gLnk(2, \""
                        + sPrompt.replace('\'', '"') + "\", \"" + sAction
                        + "\"));" + "\n");

            }
            return ("");
        }
    }
//	NEW SIDE MENU
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<MenuBean> listSideMenu(String userId) {
		List<MenuBean> parent = new ArrayList<MenuBean>();
		List<MenuChild> child = new ArrayList<MenuChild>();
		List<MenuBean> result = new ArrayList<MenuBean>();
		HashMap map = new HashMap();
		map.put("userId", userId);
		map.put("applicationId", "MOBTMS");
		try {
			parent = this.ibatisSqlMap.queryForList("directory.menu.getDirMenu", map);
			child = this.ibatisSqlMap.queryForList("directory.menu.getSideMenu", map);
			
			for (MenuBean menuBean : parent) {
				List<MenuChild> childList = new ArrayList<MenuChild>();
				MenuBean beanParent = new MenuBean();
				beanParent.setMenuId(menuBean.getMenuId());
				beanParent.setFormId(menuBean.getFormId());
				beanParent.setParentId(menuBean.getParentId());
				beanParent.setPrompt(menuBean.getPrompt());
				beanParent.setResult(menuBean.getResult());
				beanParent.setLevel(menuBean.getLevel());
				beanParent.setSeqOrder(menuBean.getSeqOrder());
				
				for (MenuChild childMenu : child) {
					if(menuBean.getMenuId().equals(childMenu.getParentId()) || menuBean.getMenuId() == childMenu.getParentId()){
						MenuChild bean = new MenuChild();
						bean.setFormId(childMenu.getFormId());
						bean.setParentId(childMenu.getParentId());
						bean.setPrompt(childMenu.getPrompt());
						bean.setResult(childMenu.getResult());
						bean.setFormFileName(childMenu.getFormFileName());
						bean.setLevel(childMenu.getLevel());
						bean.setSeqOrder(childMenu.getSeqOrder());
						childList.add(bean);
					}
				}
				beanParent.setChild(childList);
				result.add(beanParent);
			}
			
		} catch (SQLException sqle) {
			this.logger.printStackTrace(sqle);
			return null;
		}
		return result;
	}


}
