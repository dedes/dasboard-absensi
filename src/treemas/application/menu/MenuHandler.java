package treemas.application.menu;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class MenuHandler extends AppAdminActionBase {

    private String screenIdUpd = resources.getMessage("appmgr.menu.edit.screen");
    private String screenIdList = resources.getMessage("appmgr.menu.list.screen");

    private MenuManager manager = new MenuManager();

    public MenuHandler() {
        pageMap.put("Detail", "viewDetail");
        pageMap.put("LoadDetail", "viewDetail");
        pageMap.put("AddRoot", "edit");
        pageMap.put("AddChild", "edit");
        pageMap.put("AddDetail", "editDetail");
        pageMap.put("SaveRoot", "view");
        pageMap.put("SaveChild", "view");
        pageMap.put("SaveDetail", "viewDetail");
        pageMap.put("Edit", "edit");
        pageMap.put("EditDetail", "editDetail");
        pageMap.put("DeleteMenu", "view");
        pageMap.put("DeleteDetail", "viewDetail");
        pageMap.put("Update", "view");
        pageMap.put("UpdateDetail", "viewDetail");

        exceptionMap.put("SaveRoot", "edit");
        exceptionMap.put("SaveChild", "edit");
        exceptionMap.put("SaveDetail", "editDetail");
        exceptionMap.put("Detail", "viewDetail");
        exceptionMap.put("LoadDetail", "viewDetail");
        exceptionMap.put("AddRoot", "edit");
        exceptionMap.put("AddChild", "edit");
        exceptionMap.put("AddDetail", "editDetail");
        exceptionMap.put("Edit", "edit");
        exceptionMap.put("EditDetail", "editDetail");
        exceptionMap.put("DeleteMenu", "view");
        exceptionMap.put("DeleteDetail", "viewDetail");
        exceptionMap.put("Update", "view");
        exceptionMap.put("UpdateDetail", "viewDetail");
    }

    public ActionForward doAction(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws AppException {
        MenuForm menuForm = (MenuForm) form;
        String task = menuForm.getTask();
        if ("Load".equals(task)) {
            loadApplicationName(request, manager);
            if (!"".equals(menuForm.getApplicationId())) {
                load(request, menuForm, manager);
            } else {
                request.setAttribute("listMenu", new ArrayList());
            }
        } else if ("Detail".equals(task)) {
            menuForm.setDetailHeaderForm(manager.getHeaderDetail(menuForm.
                    getMenuId()));
        } else if ("LoadDetail".equals(task)) {
            menuForm.setDetailHeaderForm(manager.getHeaderDetail(menuForm.
                    getMenuIdForm()));
            loadDetail(request, menuForm, manager);
        } else if ("AddRoot".equals(task) || "AddChild".equals(task)) {
            if ("AddRoot".equals(task)) {
                loadHeader(request, menuForm, manager, "AddRoot");
            } else if ("AddChild".equals(task)) {
                loadHeader(request, menuForm, manager, "AddChild");
            }
            menuForm.setActive("1");
            menuForm.setResult("RD");
        } else if ("AddDetail".equals(task)) {
            //loadHeaderDetail(request,menuForm,manager);
            menuForm.setLevel(String.valueOf(Integer.parseInt(menuForm.getMenuLevelDetail()) + 1));
            menuForm.setActive("1");
        } else if ("SaveRoot".equals(task) || "SaveChild".equals(task)
                || "SaveDetail".equals(task)) {
            MenuBean beanMenu = new MenuBean();
            if ("SaveDetail".equals(task)) {
                menuForm.setResult("FD");
                menuForm.setSeqOrder("1");
            }
            menuForm.toBean(beanMenu);
            try {
                manager.insertRecord(beanMenu, this.getLoginId(request),
                        this.screenIdUpd);
            } catch (AppException aie) {
                if ("SaveRoot".equals(task) || "SaveChild".equals(task)) {
                    if ("SaveRoot".equals(task)) {
                        menuForm.setTask("AddRoot");
                    } else if ("SaveChild".equals(task)) {
                        menuForm.setTask("AddChild");
                    }
                } else if ("SaveDetail".equals(task)) {
                    menuForm.setTask("AddDetail");
                }
                throw aie;
            }
            if ("SaveRoot".equals(task) || "SaveChild".equals(task)) {
                loadApplicationName(request, manager);
                load(request, menuForm, manager);
            } else if ("SaveDetail".equals(task)) {
                loadDetail(request, menuForm, manager);
            }
        } else if ("Edit".equals(task) || "EditDetail".equals(task)) {
            menuForm.fromBean(manager.getSingleRecord(menuForm.getMenuId()));
        } else if ("DeleteMenu".equals(task) || "DeleteDetail".equals(task)) {

            if ("DeleteMenu".equals(task)) {
                try {
                    manager.deleteMenu(menuForm.getMenuId(), this.getLoginId(request),
                            this.screenIdList);
                } finally {
                    loadApplicationName(request, manager);
                    load(request, menuForm, manager);
                }
            } else {
                try {
                    manager.deleteDetail(menuForm.getMenuId(),
                            this.getLoginId(request),
                            this.screenIdList);
                } finally {
                    loadDetail(request, menuForm, manager);
                }
            }
        } else if ("Update".equals(task) || "UpdateDetail".equals(task)) {
            String parentMenuId = "";
            if ("Update".equals(task)) {
                String statusOri = request.getParameter("statusOri");
                String statusNew = "";
                if ("1".equals(menuForm.getActive()))
                    statusNew = "1";
                else statusNew = "0";
                if (!statusOri.equals(statusNew)) {
                    boolean isNonActiveAble = manager.isNonActiveAble(menuForm.
                            getMenuId());
                    if (!isNonActiveAble) {
                        menuForm.setActive(statusOri);
                        menuForm.setTask("Edit");
                        throw new AppException(MenuManager.ERROR_IN_USE);
                    }
                }
                parentMenuId = menuForm.getParentId();
            } else if ("UpdateDetail".equals(task)) {
                menuForm.setResult("FD");
                menuForm.setSeqOrder("1");
                parentMenuId = menuForm.getMenuIdForm();
            }
            MenuBean beanMenu = new MenuBean();
            menuForm.toBean(beanMenu);
            manager.updateRecord(beanMenu, parentMenuId,
                    this.getLoginId(request),
                    this.screenIdUpd);
            if ("Update".equals(task)) {
                loadApplicationName(request, manager);
                load(request, menuForm, manager);
            } else {
                menuForm.setDetailHeaderForm(manager.getHeaderDetail(menuForm.
                        getMenuIdForm()));
                loadDetail(request, menuForm, manager);
            }
        }

        return this.getNextPage(task, mapping);
    }

    private void load(HttpServletRequest request, MenuForm menuForm,
                      MenuManager manager) throws AppException {
        List list = null;
        int searchType;
        String frmSearchType = menuForm.getFieldName();
        String searchValue = menuForm.getSearchString();
        searchValue = searchValue == null ? searchValue : searchValue.trim();

        if ("".equals(searchValue) || "prompt".equals(frmSearchType)) {
            searchType = MenuManager.SEARCH_BY_PROMPT;
        } else if ("menuId".equals(frmSearchType)) {
            searchType = MenuManager.SEARCH_BY_MENUID;
        } else
            throw new IllegalArgumentException("Invalid search type " + frmSearchType);

        list = manager.getAllRecords(searchType, menuForm.getSearchString().trim(),
                menuForm.getApplicationId());
        list = BeanConverter.convertBeansToString(list, MenuForm.class);
        request.setAttribute("listMenu", list);
    }


    private void loadDetail(HttpServletRequest request, MenuForm menuForm,
                            MenuManager manager) throws AppException {
        List list = null;
        int searchType;
        String searchValue = menuForm.getSearchStringDetail();
        String frmSearchType = menuForm.getFieldNameDetail();
        searchValue = searchValue == null ? searchValue : searchValue.trim();
        if ("prompt".equals(frmSearchType)) {
            searchType = MenuManager.ALL_DETAIL_PROMPT;
        } else if ("menuId".equals(frmSearchType)) {
            searchType = MenuManager.ALL_DETAIL_MENUID;
        } else if ("formFileName".equals(frmSearchType)) {
            searchType = MenuManager.ALL_DETAIL_FORMFILENAME;
        } else
            throw new IllegalArgumentException("Illegal search type detail " + frmSearchType);

        list = manager.getAllDetail(searchType, menuForm.getApplicationId(),
                menuForm.getMenuIdForm(), searchValue);

        list = BeanConverter.convertBeansToString(list, MenuForm.class);
        request.setAttribute("listMenuDetail", list);
    }


    private void loadApplicationName(HttpServletRequest request, MenuManager manager) throws AppException {
        List list = manager.getApplicationName();
        request.setAttribute("listApplicationName", list);
    }

    private void loadHeader(HttpServletRequest request, MenuForm menuForm, MenuManager manager, String status) throws AppException {
        int headerType;
        String id;
        if ("AddRoot".equals(status)) {
            headerType = MenuManager.HEADER_ADD_ROOT;
            id = menuForm.getApplicationId();
        } else if ("AddChild".equals(status)) {
            headerType = MenuManager.HEADER_ADD_CHILD;
            id = menuForm.getMenuId();
        } else
            throw new IllegalArgumentException("Illegal header status " + status);
        MenuBean bean = manager.getHeader(headerType, id);
        menuForm.fromBean(bean);
    }


    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        super.handleException(mapping, form, request, response, ex);
        int code = ex.getErrorCode();
        switch (code) {
            case MenuManager.ERROR_DATA_EXISTS:
                this.setMessage(request, "common.dataexists",
                        new Object[]{
                                ex.getUserObject()});
                break;

            case MenuManager.ERROR_IN_USE:
                this.setMessage(request, "appmgr.menu.inuse", null);
                break;

            case MenuManager.ERROR_HAS_CHILD:
                this.setMessage(request, "appmgr.menu.haschild", null);
                break;

            default:
                this.setMessage(request, "common.error", null);
                this.logger.printStackTrace(ex);
                break;
        }
        String task = ((MenuForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
