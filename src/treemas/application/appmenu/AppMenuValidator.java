package treemas.application.appmenu;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class AppMenuValidator extends Validator {
    public AppMenuValidator(HttpServletRequest request) {
        super(request);
    }

    public AppMenuValidator(Locale locale) {
        super(locale);
    }

    public boolean validateAppMenuInsert(ActionMessages messages, AppMenuForm form, AppMenuManager manager) throws AppException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        try {
            AppMenuBean bean = manager.getSingleMenu(form.getMenuid());
            if (null != bean) {
                messages.add("menuid", new ActionMessage("errors.duplicate", "MENU ID"));
            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
        this.textValidator(messages, form.getMenuid(), "app.menu.menuid", "menuid", "1", "6", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getParentid(), "app.menu.parentid", "parentid", "1", "6", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getPrompt(), "app.menu.prompt", "prompt", "1", "50", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getLevel(), "app.menu.level", "level", "1", "2", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getResult(), "app.menu.result", "result", "1", "2", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_CHAR);
        this.textValidator(messages, form.getSeqorder(), "app.menu.seqorder", "seqorder", "1", "2", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);


        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }

    public boolean validateAppMenuEdit(ActionMessages messages, AppMenuForm form, AppMenuManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.textValidator(messages, form.getParentid(), "app.menu.parentid", "parentid", "1", "6", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getPrompt(), "app.menu.prompt", "prompt", "1", "50", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getLevel(), "app.menu.level", "level", "1", "2", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);
        this.textValidator(messages, form.getResult(), "app.menu.result", "result", "1", "2", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_CHAR);
        this.textValidator(messages, form.getSeqorder(), "app.menu.seqorder", "seqorder", "1", "2", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
