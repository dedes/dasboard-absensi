package treemas.application.appmenu;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.multiple.MultipleManager;
import treemas.base.core.CoreActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AppMenuHandler extends CoreActionBase {

    MultipleManager multipleManager = new MultipleManager();
    AppMenuManager manager = new AppMenuManager();
    AppMenuValidator validator;

    public AppMenuHandler() {

    }

    @Override
    public ActionForward doAction(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
            throws AppException {
        this.validator = new AppMenuValidator(request);
        AppMenuForm frm = (AppMenuForm) form;
        frm.setTitle(Global.TITLE_APP_MENU);
        frm.getPaging().calculationPage();
        String task = frm.getTask();

        if ("".equals(frm.getFormid())) {
            frm.setFormid(null);
        }

        this.loadPrompt(request);
        this.loadAppAdmin(request);
        this.loadFormid(request);

        if (Global.WEB_TASK_SHOW.equals(task) || Global.WEB_TASK_LOAD.equals(task) || Global.WEB_TASK_SEARCH.equals(task)) {

            this.load(request, frm);
            return mapping.findForward(Global.WEB_TASK_VIEW.toLowerCase());

        } else if (Global.WEB_TASK_ADD.equals(task)) {

            frm.setTitle(Global.TITLE_APP_MENU);
            clearForm(frm);
            return mapping.findForward(Global.WEB_TASK_EDIT.toLowerCase());

        } else if (Global.WEB_TASK_INSERT.equals(task)) {

            if (this.validator.validateAppMenuInsert(new ActionMessages(), frm, this.manager)) {
                this.insertMenu(request, frm);
                request.setAttribute("message", resources.getMessage("common.process.save"));
//				this.load(request, frm);
            }
            return mapping.findForward(Global.WEB_TASK_VIEW.toLowerCase());

        } else if (Global.WEB_TASK_EDIT.equals(task)) {

            frm.setTitle(Global.TITLE_APP_MENU);
            this.loadSingleMenu(frm);
            return mapping.findForward(Global.WEB_TASK_EDIT.toLowerCase());

        } else if (Global.WEB_TASK_UPDATE.equals(task)) {

            if (this.validator.validateAppMenuEdit(new ActionMessages(), frm, this.manager)) {
                this.updateMenu(request, frm);
                request.setAttribute("message", resources.getMessage("common.process.update"));
//				this.load(request, frm);
            }
            return mapping.findForward(Global.WEB_TASK_VIEW.toLowerCase());
        }

        return null;
    }

    private void loadPrompt(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getPrompt(param);
        request.setAttribute("listPrompt", list);
    }

    private void loadAppAdmin(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getListAppId();
        request.setAttribute("listAppid", list);
    }

    private void loadFormid(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getFormid(param);
        request.setAttribute("listFormid", list);
    }

    private void load(HttpServletRequest request, AppMenuForm form) throws CoreException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAllMenu(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);

    }

    private void loadSingleMenu(AppMenuForm form) throws AppException {
        AppMenuBean bean = this.manager.getSingleMenu(form.getMenuid());
        BeanConverter.convertBeanToString(bean, form);
    }

    private void insertMenu(HttpServletRequest request, AppMenuForm form) throws AppException {
        this.manager.insertMenu(form, this.getLoginId(request));
        request.setAttribute("message", resources.getMessage("common.process.save"));
        load(request, form);
    }

    private void updateMenu(HttpServletRequest request, AppMenuForm form) throws AppException {
        this.manager.updateMenu(form, this.getLoginId(request));
        request.setAttribute("message", resources.getMessage("common.process.update"));
        this.load(request, form);
    }

    private void clearForm(AppMenuForm form) throws AppException {
        form.setMenuid("");
        form.setParentid("");
        form.setPrompt("");
        form.setLevel("");
        form.setResult("");
        form.setSeqorder("");
        form.setActive((Global.STRING_TRUE));
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {

            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                this.logger.printStackTrace(ex);
                break;

            case AppMenuManager.ERROR_DB:
                this.setMessage(request, "common.dberror", null);
                this.logger.printStackTrace(ex);
                break;

            case AppMenuManager.ERROR_DATA_EXISTS:
                this.setMessage(request, "common.dberror", null);
                this.logger.printStackTrace(ex);
                break;

            case AppMenuManager.ERROR_UNKNOWN:
                this.setMessage(request, "common.dberror", null);
                this.logger.printStackTrace(ex);
                break;

            case AppMenuManager.ERROR_WEB_SERVICE:
                this.setMessage(request, "common.dberror", null);
                this.logger.printStackTrace(ex);
                break;

            default:
                super.handleException(mapping, form, request, response, ex);
                this.setMessage(request, "common.error", null);
                this.logger.printStackTrace(ex);
                break;
        }
        ((AppMenuForm) form).setTask(((AppMenuForm) form).resolvePreviousTask());
        return mapping.findForward(Global.WEB_TASK_EDIT.toLowerCase());
    }

}
