package treemas.application.appmenu;

import treemas.base.core.CoreManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.auditrail.AuditTrailException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class AppMenuManager extends CoreManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_APPMENU;

    public PageListWrapper getAllMenu(int pageNumber, AppMenuSearch search) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllMenu", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllMenu", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public AppMenuBean getSingleMenu(String menuid) throws AppException {
        AppMenuBean bean = new AppMenuBean();

        try {
            bean = (AppMenuBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleMenu", menuid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return bean;
    }

    public void insertMenu(AppMenuForm form, String usrupd) throws CoreException, AuditTrailException {
        AppMenuBean bean = new AppMenuBean();
        BeanConverter.convertBeanFromString(form, bean);
        bean.setUsrupd(usrupd);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.insert(this.STRING_SQL + "insertMenu", bean);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }
    }

    public void updateMenu(AppMenuForm form, String usrupd) throws CoreException {
        AppMenuBean bean = new AppMenuBean();
        BeanConverter.convertBeanFromString(form, bean);
        bean.setUsrupd(usrupd);
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.update(STRING_SQL + "updateMenu", bean);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }
    }

}

