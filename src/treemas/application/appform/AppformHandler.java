package treemas.application.appform;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.multiple.MultipleManager;
import treemas.base.core.CoreActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AppformHandler extends CoreActionBase {

    MultipleManager multipleManager = new MultipleManager();
    AppformManager fManager = new AppformManager();
    AppformValidator fvalidator;
    String oldIcon = "";

    public AppformHandler() {

    }

    @Override
    public ActionForward doAction(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
            throws AppException {
        this.fvalidator = new AppformValidator(request);
        AppformForm frm = (AppformForm) form;
        frm.setTitle(Global.TITLE_APP_FORM);
        frm.getPaging().calculationPage();
        String task = frm.getTask();
//		frm.getSearch().setUsermaker(this.getLoginId(request));
        if (Global.WEB_TASK_SHOW.equals(task) || Global.WEB_TASK_LOAD.equals(task) || Global.WEB_TASK_SEARCH.equals(task)) {
            this.load(request, frm);
            return mapping.findForward(Global.WEB_TASK_VIEW.toLowerCase());
        } else if (Global.WEB_TASK_ADD.equals(task)) {
            frm.setTitle(Global.TITLE_APP_FORM);
            this.clearForm(frm);
            this.loadAppDropDown(request);
            return mapping.findForward(Global.WEB_TASK_EDIT.toLowerCase());
        } else if (Global.WEB_TASK_INSERT.equals(task)) {
            if (this.fvalidator.validateInsertAppform(new ActionMessages(), frm)) {
                if (frm.getCallbymenu() == "") {
                    frm.setCallbymenu(null);
                }
                this.insertApp(request, frm);
            }
            return mapping.findForward(Global.WEB_TASK_VIEW.toLowerCase());
        } else if (Global.WEB_TASK_EDIT.equals(task)) {
            frm.setTitle(Global.TITLE_APP_FORM);
//			this.loadAppDropDown(request);
            this.loadSingle(frm);
            oldIcon = frm.getIcon();
            return mapping.findForward(Global.WEB_TASK_EDIT.toLowerCase());
        } else if (Global.WEB_TASK_UPDATE.equals(task)) {
            if (frm.getCallbymenu() == "") {
                frm.setCallbymenu(null);
            }
            if (this.fvalidator.validateUpdateAppform(new ActionMessages(), frm)) {
                this.updateApp(request, frm, oldIcon);
            }
            return mapping.findForward(Global.WEB_TASK_VIEW.toLowerCase());
        }
        return null;
    }

    private AppformBean convertToBean(AppformForm form, String updateUser) throws AppException {
        AppformBean bean = new AppformBean();
        form.toBean(bean);
        bean.setUsrupd(updateUser);
        return bean;
    }

    private void load(HttpServletRequest request, AppformForm form) throws CoreException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.fManager.getAllAppForm(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);
        this.loadAppDropDown(request);

    }

    private void loadSingle(AppformForm form) throws CoreException {
        AppformBean bean = new AppformBean();
        bean = this.fManager.getSingleForm(form.getFormid());
        BeanConverter.convertBeanToString(bean, form);

    }

    private void loadAppDropDown(HttpServletRequest request) throws CoreException {
        List list = this.multipleManager.getListAppId();
        request.setAttribute("listAppId", list);
    }

    private void insertApp(HttpServletRequest request, AppformForm form) throws AppException {
        AppformBean bean = this.convertToBean(form, this.getLoginId(request));
        try {
            this.fManager.insert(bean);
            request.setAttribute("message", resources.getMessage("common.process.save"));
            this.load(request, form);
        } catch (CoreException e) {
            throw e;
        }
    }

    private void updateApp(HttpServletRequest request, AppformForm form, String oldIcon) throws AppException {
        AppformBean bean = this.convertToBean(form, this.getLoginId(request));
        try {
            this.fManager.update(bean, oldIcon);
            request.setAttribute("message", resources.getMessage("common.process.update"));
            this.load(request, form);
        } catch (CoreException e) {
            throw e;
        }
    }

    private void clearForm(AppformForm form) throws AppException {
        form.setFormid("");
        form.setFormname("");
        form.setFormfilename("");
        form.setCallbymenu("");
        form.setActive("1");
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                AppformForm frm = (AppformForm) form;
                try {
                    this.loadAppDropDown(request);
                } catch (AppException e) {
//				this.logger.printStackTrace(e);
                }
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((AppformForm) form).setTask(((AppformForm) form).resolvePreviousTask());

        return mapping.findForward(Global.WEB_TASK_EDIT.toLowerCase());
    }

}
