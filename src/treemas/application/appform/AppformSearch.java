package treemas.application.appform;

import treemas.base.search.SearchForm;

public class AppformSearch extends SearchForm {

    private String formid;
    private String appid;
    private String formname;
    private String usrupd;

    public AppformSearch() {

    }

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getFormname() {
        return formname;
    }

    public void setFormname(String formname) {
        this.formname = formname;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }


}
