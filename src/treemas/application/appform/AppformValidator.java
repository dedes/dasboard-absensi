package treemas.application.appform;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;


public class AppformValidator extends Validator {
    private AppformManager manager = new AppformManager();

    public AppformValidator(HttpServletRequest request) {
        super(request);
    }

    public AppformValidator(Locale locale) {
        super(locale);
    }

    public boolean validateInsertAppform(ActionMessages messages, AppformForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        try {
            AppformBean bean = manager.getSingleForm(form.getFormid());
            if (null != bean) {
                if (Global.STRING_TRUE.equals(bean.getActive())) {
                    messages.add("formid", new ActionMessage("setting.surveyor.dataexists", "appform"));
                }
            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }


        this.textValidator(messages, form.getFormid(), "app.menu.formid", "formid", "6", "6", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getFormname(), "app.appform.formname", "formname", "1", "30", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getFormfilename(), "app.appform.formfilename", "formfilename", "1", "100", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
        return ret;

    }

    public boolean validateUpdateAppform(ActionMessages messages, AppformForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        this.textValidator(messages, form.getFormname(), "app.appform.formname", "formname", "1", "30", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getFormfilename(), "app.appform.formfilename", "formfilename", "1", "100", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
        return ret;

    }
}
