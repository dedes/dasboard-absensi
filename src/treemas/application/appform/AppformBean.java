package treemas.application.appform;

import java.io.Serializable;

public class AppformBean implements Serializable {

    private String formid;
    private String appid;
    private String formname;
    private String formfilename;
    private String callbymenu;
    private String usrupd;
    private String icon;
    private String active = "1";

    public AppformBean() {
    }

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getFormname() {
        return formname;
    }

    public void setFormname(String formname) {
        this.formname = formname;
    }

    public String getFormfilename() {
        return formfilename;
    }

    public void setFormfilename(String formfilename) {
        this.formfilename = formfilename;
    }

    public String getCallbymenu() {
        return callbymenu;
    }

    public void setCallbymenu(String callbymenu) {
        this.callbymenu = callbymenu;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }


}
