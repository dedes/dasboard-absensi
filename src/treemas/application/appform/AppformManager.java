package treemas.application.appform;

import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class AppformManager extends AppAdminManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_APP_FORM;

    public PageListWrapper getAllAppForm(int pageNumber, AppformSearch search) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllAppForm", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllAppForm", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public AppformBean getSingleForm(String formid) throws CoreException {
        AppformBean result = null;
        try {
            result = (AppformBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleAppform", formid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public void insert(AppformBean bean) throws CoreException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.insert(this.STRING_SQL + "insertAppForm", bean);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }
    }

    public void update(AppformBean bean, String oldIcon) throws CoreException {
        try {
            if (bean.getIcon() == "") {
                bean.setIcon(oldIcon);
            }
            this.ibatisSqlMap.update(this.STRING_SQL + "updateAppForm", bean);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }
    }
}
