package treemas.application.login;

import treemas.base.core.CoreManagerBase;
import treemas.globalbean.SessionBean;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.cryptography.MD5;
import treemas.util.format.TreemasFormatter;
import treemas.util.ldap.ActiveDirectory;
import treemas.util.log.ILogger;
import treemas.util.tool.ConfigurationProperties;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

//import treemas.dashboard.ejb.*;
//import javax.ejb.CreateException;

public class LoginManager extends CoreManagerBase {
    public static final String STRING_SQL = SQLConstant.SQL_LOGIN;
    // private String CONTEXT_FACTORY = weblogic.jndi.WLInitialContextFactory.class.getName();
    // private AuthenticateHome dashboardHome;

    static final int ERROR_USER_NOT_FOUND_DB = 1;
    static final int ERROR_AUTH_FAILED_AD = 2;
    static final int ERROR_AD = 3;
    static final int ERROR_ALREADY_LOGGED_IN = 4;
    static final int ERROR_LOCKED = 5;
    static final int ERROR_AUTH_FAILED_DASHBOARD = 6;
    static final int ERROR_AUTH_FAILED_CONNECT_DASHBOARD = 7;
    static final int ERROR_ED = 8;
    static final int ERROR_UNAPPROVE = 9;
    static final int ERROR_ADD_REJECTED = 10;
    private boolean adEnabled = true;

    public LoginManager() {
        String tmp = ConfigurationProperties.getInstance().get(PropertiesKey.LDAP_ENABLE);
        this.adEnabled = (tmp == null || "1".equals(tmp));
        this.logger.log(ILogger.LEVEL_INFO, "Active directory checking enabled=" + this.adEnabled);
    }

    public boolean checkUserLogin(String userId)
            throws CoreException {
        String result = null;
        try {
            result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getUserLogin", userId);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ErrorCode.ERR_CACHE_ERROR);
        }
        if (Global.STRING_TRUE.equals(result)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean authActiveDirectory(String userId, String password)
            throws CoreException {
        if (!this.adEnabled)
            return true;
        try {
            ActiveDirectory ad = ActiveDirectory.getInstance();
            return (ActiveDirectory.getInstance()
                    .authenticate(userId, password));
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_AD);
        }
    }

    public SessionBean login(String userId, String password, String address)
            throws CoreException {
        return this.login(userId, password, address, true);
    }

    private SessionBean login(String userId, String password, String address, boolean authAD) throws CoreException {
        userId = userId.toUpperCase();
        SessionBean result = null;
        HashMap loginMap = new HashMap();
        loginMap.put("userId", userId);
        loginMap.put("address", address);
        try {
            loginMap.put("password", MD5.getInstance().hashData(password));
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.update(STRING_SQL + "login", loginMap);
                this.ibatisSqlMap.commitTransaction();

                Integer resultCode = (Integer) loginMap.get("resultCode");
                this.checkResultCode(userId, resultCode);

                result = (SessionBean) this.ibatisSqlMap.queryForObject(STRING_SQL + "getLoginBean", loginMap);
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (NoSuchAlgorithmException e) {
            this.logger.printStackTrace(e);
        } catch (CoreException ce) {
            throw ce;
        } catch (SQLException sqe) {
            throw new CoreException(sqe, ERROR_DB);
        }
        return result;
    }

    public SessionBean doLogin(String userId) throws CoreException {
        userId = userId.toUpperCase();
        SessionBean result = null;
        HashMap loginMap = new HashMap();
        loginMap.put("userId", userId);
        try {
            loginMap.put("password", MD5.getInstance().hashData("12345"));
            try {
                this.ibatisSqlMap.startTransaction();
                result = (SessionBean) this.ibatisSqlMap.queryForObject(STRING_SQL + "getLoginBean", loginMap);
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (NoSuchAlgorithmException e) {
            this.logger.printStackTrace(e);
        } catch (SQLException sqe) {
            throw new CoreException(sqe, ERROR_DB);
        }
        return result;
    }

    public void checkResultCode(String userId, Integer resultCode) throws CoreException {
        if (resultCode == null)
            throw new CoreException(ERROR_USER_NOT_FOUND_DB, userId);

        int code = resultCode.intValue();
        if (code != 0) {
            throw new CoreException(code, userId);
        }
    }

    public void updateUserLogout(SessionBean bean) throws CoreException {
        try {
            this.ibatisSqlMap.update(STRING_SQL + "updateLogout", bean);
        } catch (SQLException sqe) {
            throw new CoreException(sqe, ERROR_DB);
        }
    }

    public void updateAllLogout() throws CoreException {
        try {
            int n = this.ibatisSqlMap.update(STRING_SQL + "updateAllLogout", null);
            this.logger.log(ILogger.LEVEL_INFO, "Releasing login status: " + n
                    + " user(s)");
        } catch (SQLException sqe) {
            throw new CoreException(sqe, ERROR_DB);
        }
    }

    public void insertUserLogs(String userId, String activity, String clientAddress) throws CoreException {
        try {
            Date appDate = new Date();
            String activityDate = TreemasFormatter.formatDate(appDate, "yyyyMMdd HH:mm:ss");
            HashMap param = new HashMap();
            param.put("userId", userId);
            param.put("activityDate", activityDate);
            param.put("activity", activity);
            param.put("clientAddress", clientAddress);
            this.ibatisSqlMap.insert(STRING_SQL + "insertUserLogs", param);
        } catch (SQLException sqe) {
            throw new CoreException(sqe, ERROR_DB);
        }
    }

}


// public MssLoginBean doLoginDashboard(String userId, String key) throws
// CoreException {
// MssLoginBean loginBean = this.doLogin(userId, "", false);
// this.authToDashboard(userId, key);
// return loginBean;
// }
//
// private boolean authToDashboard(String userId, String key) throws
// CoreException {
// if (this.dashboardHome == null) {
// this.dashboardHome = this.getDashboardHome();
// }
// boolean result = false;
// try {
// Authenticate ejbBean = this.dashboardHome.create();
// this.logger.log(ILogger.LEVEL_DEBUG_LOW,
// "Authenticate to dashboard for user " + userId +
// ";key=" + key);
// int intResult = ejbBean.authenticate(userId, key);
// result = (intResult == Authenticate.AUTHENTICATE_OK);
// this.logger.log(ILogger.LEVEL_DEBUG_HIGH, "Authenticate to dashboard=" +
// result +
// " for user " + userId);
// }
// catch (CreateException e) {
// throw new CoreException(e, ERROR_AUTH_FAILED_CONNECT_DASHBOARD);
// }
// catch (RemoteException e) {
// throw new CoreException(e, ERROR_AUTH_FAILED_CONNECT_DASHBOARD);
// }
// return result;
// }
//
// private AuthenticateHome getDashboardHome() throws CoreException {
// AuthenticateHome result = null;
// ApplicationProperties appProps = ApplicationProperties.getInstance();
// String ejbUrl = appProps.get(PropertiesKey.DASHBOARD_EJB_URL);
// String jndiName = appProps.get(PropertiesKey.DASHBOARD_EJB_JNDI);
// String user = appProps.get(PropertiesKey.DASHBOARD_EJB_USER);
// String pwd = appProps.get(PropertiesKey.DASHBOARD_EJB_PWD);
// try {
// Hashtable env = new Hashtable();
// env.put(Context.INITIAL_CONTEXT_FACTORY, CONTEXT_FACTORY);
// env.put(Context.PROVIDER_URL, ejbUrl);
// if (user != null) {
// env.put(Context.SECURITY_PRINCIPAL, user);
// env.put(Context.SECURITY_CREDENTIALS, pwd);
// }
// this.logger.log(ILogger.LEVEL_DEBUG_HIGH,
// "Connecting to dasbboard authenticate EJB " + ejbUrl + "...");
// InitialContext ctx = new InitialContext(env);
// try {
// this.logger.log(ILogger.LEVEL_DEBUG_LOW, "Lookup ejb..." + jndiName);
// Object obj = ctx.lookup(jndiName);
// result = (AuthenticateHome) PortableRemoteObject.narrow(obj,
// AuthenticateHome.class);
// this.logger.log(ILogger.LEVEL_DEBUG_HIGH, "Ejb home retrieved!");
// }
// finally {
// ctx.close();
// }
// }
// catch (NamingException ne) {
// throw new CoreException(ne, ERROR_AUTH_FAILED_CONNECT_DASHBOARD);
// }
// return result;
// }


// private MssLoginBean doLogin(String userId, String companyId, String password, boolean
// authAD) throws CoreException {
// userId = userId.toUpperCase();
// MssLoginBean result = null;
// String saltKey = "";
// HashMap loginMap= new HashMap();
// loginMap.put("userId", userId);
// loginMap.put("companyId",companyId);
// try {
// try {
// this.ibatisSqlMap.startTransaction();
// saltKey =
// (String)this.ibatisSqlMap.queryForObject(STRING_SQL+"getUserPassword",
// loginMap);
// if(saltKey==null)
// throw new CoreException(ERROR_AUTH_FAILED_AD, userId);
//
// saltKey = SHA256.hashData(password,saltKey);
// loginMap.put("password",saltKey);
//
// this.ibatisSqlMap.update(STRING_SQL+"doLogin", loginMap);
// this.ibatisSqlMap.commitTransaction();
// Integer resultCode = (Integer) loginMap.get("resultCode");
// this.checkResultCode(userId, resultCode);
// result = (MssLoginBean)
// this.ibatisSqlMap.queryForObject(STRING_SQL+"getLoginBean", loginMap);
// }
// finally{
// this.ibatisSqlMap.endTransaction();
// }
// } catch (CoreException ce) {
// throw new CoreException(ce.getErrorCode(), userId);
// //throw ce;
// } catch (SQLException sqe) {
// throw new CoreException(sqe, ERROR_DB);
// } catch (NoSuchAlgorithmException e) {
// throw new CoreException(ERROR_AUTH_FAILED_AD, userId);
// // e.printStackTrace();
// } catch (IOException e) {
// throw new CoreException(ERROR_AUTH_FAILED_AD, userId);
// // e.printStackTrace();
// }
// return result;
// }

