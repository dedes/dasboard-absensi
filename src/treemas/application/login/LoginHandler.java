package treemas.application.login;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.base.core.CoreActionBase;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.log.ILogger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginHandler extends CoreActionBase {
    //  private static final String TASK_LOGIN_AD = "LoginDashboard";
    private static final String TASK_LOGIN = "Login";
    private static final String TASK_LOGOUT = "Logout";
    private static final String PASSWORD_REDIRECT = "passredirect";
    private static final String TASK_REDIRECT = "redirect";
    private LoginManager manager = new LoginManager();

    public LoginHandler() {
    }


    public ActionForward doAction(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws CoreException {

        LoginForm loginForm = (LoginForm) form;
        String action = loginForm.getTask();
        if (Global.WEB_TASK_LOAD.equals(action)) {
            loginForm.setTask(TASK_LOGIN);
            HttpSession sess = request.getSession();
            if (sess != null)
                sess.invalidate();
            return mapping.findForward("view");
        } else if (TASK_LOGIN.equals(action)) {
            SessionBean loginBean = this.manager.login(loginForm.getUserId(), loginForm.getPassword(), request.getRemoteAddr());
            loginBean.setPassword(loginForm.getPassword());
            manager.insertUserLogs(loginBean.getLoginId(), "LOGIN", request.getRemoteAddr());
            request.getSession(true).setAttribute(Global.SESSION_LOGIN, loginBean);
            request.setAttribute("redir_url", "/ToDoAction.do");
            this.logger.log(ILogger.LEVEL_INFO, "User " + loginBean.getLoginId() + " log-in. Address=" + request.getRemoteAddr());
            return mapping.findForward(TASK_REDIRECT);
        }

//      else if (TASK_LOGIN_AD.equals(action)) {
//         HttpSession sess = request.getSession(true);
//         SessionBean loginBean = (SessionBean) sess.getAttribute(Global.SESSION_LOGIN);
//         String dashUser = (String) sess.getAttribute(Global.SESSION_FROM_DASHBOARD_USER);
//         String dashKey = (String) sess.getAttribute(Global.SESSION_FROM_DASHBOARD_KEY);
//         String uri = (String) sess.getAttribute(Global.SESSION_FROM_DASHBOARD_URI);
//
//         sess.removeAttribute(Global.SESSION_FROM_DASHBOARD_USER);
//         sess.removeAttribute(Global.SESSION_FROM_DASHBOARD_KEY);
//         sess.removeAttribute(Global.SESSION_FROM_DASHBOARD_URI);
//
//         if (loginBean != null && !loginBean.getLoginId().equals(dashUser.toUpperCase())) {
//            sess.invalidate();
//            sess = request.getSession(true);
//            loginBean = null;
//         }
//
//         if (loginBean == null) {
//            loginBean = this.manager.doLoginDashboard(dashUser, dashKey);
//            this.logger.log(ILogger.LEVEL_INFO, "User " + loginBean.getLoginId() + " log-in from dashboard");
//            loginBean.setPassword("");  // Password is not passed from dashboard!!!
//            sess.setAttribute(Global.SESSION_LOGIN, loginBean);
//         }
//         request.setAttribute("redir_url", uri);
//         this.logger.log(ILogger.LEVEL_INFO, "From dashboard; redirected to " + uri);
//         return mapping.findForward("redir");
//      }

        else if (TASK_LOGOUT.equals(action)) {
            HttpSession sess = request.getSession();
            if (sess != null) {
                SessionBean loginBean = this.getLoginBean(request);
                if (loginBean != null) {
                    manager.insertUserLogs(loginBean.getLoginId(), "LOGOUT", request.getRemoteAddr());
                }
                sess.invalidate();
            }
            return mapping.findForward("logout");
        }
        return null;
    }


    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        int code = ex.getErrorCode();
        boolean isExp = false;
        switch (code) {
            case LoginManager.ERROR_USER_NOT_FOUND_DB:
                this.logger.log(ILogger.LEVEL_DEBUG_HIGH, "User not found: " + ex.getUserObject());
                this.setMessage(request, "login.usernotfound", new Object[]{ex.getUserObject()});
                break;

            case LoginManager.ERROR_UNAPPROVE:
                this.logger.log(ILogger.LEVEL_DEBUG_HIGH, "Unapprove User: " + ex.getUserObject());
                this.setMessage(request, "login.unapprove", new Object[]{ex.getUserObject()});
                request.setAttribute("userid", ((LoginForm) form).getUserId());
                break;

            case LoginManager.ERROR_ED:
                this.logger.log(ILogger.LEVEL_DEBUG_HIGH, "Password Expired: " + ex.getUserObject());
                this.setMessage(request, "login.ed", new Object[]{ex.getUserObject()});
                request.setAttribute("userid", ((LoginForm) form).getUserId());
                isExp = true;
                break;

            case LoginManager.ERROR_AUTH_FAILED_AD:
                this.logger.log(ILogger.LEVEL_DEBUG_HIGH, "User not found or wrong password in AD: " + ex.getUserObject());
                this.setMessage(request, "login.auth_ad_failed", new Object[]{ex.getUserObject()});
                break;

            case LoginManager.ERROR_AD:
                this.logger.printStackTrace(ex);
                this.setMessage(request, "login.ad_failed", null);
                break;

            case LoginManager.ERROR_ALREADY_LOGGED_IN:
                this.logger.log(ILogger.LEVEL_DEBUG_HIGH, "User already login: " + ex.getUserObject());
                this.setMessage(request, "login.already_login", new Object[]{ex.getUserObject()});
                break;

            case LoginManager.ERROR_LOCKED:
                this.logger.log(ILogger.LEVEL_DEBUG_HIGH, "User is locked: " + ex.getUserObject());
                this.setMessage(request, "login.locked", new Object[]{ex.getUserObject()});
                break;

            case LoginManager.ERROR_ADD_REJECTED:
                this.logger.log(ILogger.LEVEL_DEBUG_HIGH, "User not found: " + ex.getUserObject());
                this.setMessage(request, "login.usernotfound", new Object[]{ex.getUserObject()});
                break;

            case LoginManager.ERROR_DB:
                this.setMessage(request, "common.dberror", null);
                this.logger.printStackTrace(ex);
                break;

            default:
                super.handleException(mapping, form, request, response, ex);
                this.setMessage(request, "common.error", null);
                this.logger.printStackTrace(ex);
                break;
        }
        if (isExp) return mapping.findForward(PASSWORD_REDIRECT);
        ((LoginForm) form).setPassword(null);
        return mapping.findForward("view");
    }

}
