package treemas.application.changepassword;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.globalbean.PasswordManagerGenerator;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.cryptography.MD5;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;
import treemas.util.validator.ValidatorProperties;

import javax.servlet.http.HttpServletRequest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Locale;


public class ChangePasswordValidator extends Validator {
    private PasswordManagerGenerator passwordManager = new PasswordManagerGenerator();
    private ChangePasswordManager manager = new ChangePasswordManager();

    public ChangePasswordValidator(HttpServletRequest request) {
        super(request);
    }

    public ChangePasswordValidator(Locale locale) {
        super(locale);
    }

    public boolean validateChangePassword(ActionMessages messages, ChangePasswordForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        String oldHashedPassword = null;
        try {
            oldHashedPassword = MD5.getInstance().hashData(form.getOldPassword());
        } catch (NoSuchAlgorithmException e) {
            this.logger.printStackTrace(e);
        }
        try {
            if (!Strings.isNullOrEmpty(form.getOldPassword())) {
                if (!form.getUserId().equalsIgnoreCase(form.getUserId()) || !this.manager.comparePassword(form.getUserId(), oldHashedPassword)) {
                    messages.add("oldPassword", new ActionMessage("appmgr.password.mismatch", "app.pass.old"));
                }
            } else {
                messages.add("oldPassword", new ActionMessage("errors.required", new Object[]{resources.getMessage("app.pass.old")}));
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
        }
        this.textValidator(messages, form.getNewPassword(), "app.pass.new", "newPassword", this.getStringLength(ValidatorProperties.MIN_PASSWORD), this.getStringLength(0), ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        this.textValidator(messages, form.getConfirmPassword(), "app.pass.new.confirm", "confirmPassword", this.getStringLength(ValidatorProperties.MIN_PASSWORD), this.getStringLength(0), ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
        if (form.getNewPassword().length() >= ValidatorProperties.MIN_PASSWORD && form.getConfirmPassword().length() >= ValidatorProperties.MIN_CONFIRM_PASSWORD) {
            if (!form.getConfirmPassword().equalsIgnoreCase(form.getNewPassword())) {
                messages.add("confirmPassword", new ActionMessage("errors.match", "Confirm New Password", "New Password"));
            }
            try {
                this.passwordManager.cekHistory(form.getUserId(), form.getNewPassword(), messages);
            } catch (AppException e) {
                this.logger.printStackTrace(e);
            }
        }


        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
        return ret;

    }
}
