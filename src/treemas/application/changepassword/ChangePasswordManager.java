package treemas.application.changepassword;

import treemas.application.audit.AuditManager;
import treemas.application.general.parameter.ParameterBean;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.setup.SetUpManagerBase;
import treemas.globalbean.PasswordManagerGenerator;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.constant.GlobalTable;
import treemas.util.constant.SQLConstant;
import treemas.util.cryptography.MD5;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ChangePasswordManager extends SetUpManagerBase {
    public static final String STRING_SQL = SQLConstant.SQL_CHANGE_PASSWORD;
    public static final int ERROR_WRONG_OLD_PASSWORD = 81100;
    private AuditManager auditManager = new AuditManager();

    public ChangePasswordBean getPasswordInfo(String userId)
            throws CoreException {
        ChangePasswordBean result = null;
        try {
            result = (ChangePasswordBean) this.ibatisSqlMap.queryForObject(STRING_SQL + "getPasswordInfo", userId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }
        return result;
    }

    public void changePassword(String userId, String oldPassword, String password) throws CoreException {
        String oldHashedPassword = null;
        String newHashedPassword = null;
        try {
            oldHashedPassword = MD5.getInstance().hashData(oldPassword);
            newHashedPassword = MD5.getInstance().hashData(password);
        } catch (NoSuchAlgorithmException ex) {
            this.logger.printStackTrace(ex);
        }

        oldHashedPassword = (oldHashedPassword == null) ? "" : oldHashedPassword.toUpperCase();

        Map paramMap = new HashMap();
        paramMap.put("userId", userId);
        paramMap.put("password", newHashedPassword);

        PasswordManagerGenerator pwdManager = new PasswordManagerGenerator();
        try {
            try {
                pwdManager.checkPasswordValidity(userId, password);

                this.ibatisSqlMap.startTransaction();

                this.compareOldPassword(userId, oldHashedPassword);

                this.ibatisSqlMap.update(STRING_SQL + "changePassword", paramMap);
                pwdManager.insertPasswordHistory(userId, newHashedPassword, PasswordManagerGenerator.PASSWORD_HISTORY_TYPE_CHANGE, userId, oldHashedPassword);
                this.auditManager.auditEdit(userId, GlobalTable.TABLE_NAME_USER, userId, Global.FIELD_PASSWORD, oldHashedPassword, newHashedPassword, "CHANGE PASSWORD");
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (CoreException msse) {
            throw msse;
        } catch (AppException aie) {
            throw new CoreException(aie.getErrorCode());
        }
    }

    public void resetPassword(String userId, String userUpdate) throws CoreException {
        ParameterManager pmg = new ParameterManager();
        PasswordManagerGenerator pwdManager = new PasswordManagerGenerator();
        String oldHashedPassword = null;
        String newHashedPassword = null;
        try {
            String oldPassword = this.getOldPassword(userId);
            ParameterBean pBean = pmg.getSingleParameter(Global.GENERAL_PARAM_PASS_DEFAULT);
            String password = pBean.getVal();

            oldHashedPassword = MD5.getInstance().hashData(oldPassword);
            newHashedPassword = MD5.getInstance().hashData(password);

            oldHashedPassword = (oldHashedPassword == null) ? "" : oldHashedPassword.toUpperCase();

            Map paramMap = new HashMap();
            paramMap.put("userId", userId);
            paramMap.put("password", newHashedPassword);
            paramMap.put("oldPassword", oldHashedPassword);
            paramMap.put("usrupd", userUpdate);

            this.ibatisSqlMap.startTransaction();
            this.ibatisSqlMap.update(STRING_SQL + "changePassword", paramMap);
            this.auditManager.auditEdit(userUpdate, GlobalTable.TABLE_NAME_USER, userId, Global.FIELD_PASSWORD, oldHashedPassword, newHashedPassword, "RESET PASSWORD");
            this.ibatisSqlMap.commitTransaction();
        } catch (NoSuchAlgorithmException ex) {
            this.logger.printStackTrace(ex);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        } catch (AppException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        } finally {
            try {
                this.ibatisSqlMap.endTransaction();
            } catch (SQLException e) {
                this.logger.printStackTrace(e);
            }
        }
    }

    public boolean comparePassword(String userId, String oldHashedPassword) throws SQLException, CoreException {
        String oldSavedPassword = (String) this.ibatisSqlMap.queryForObject(STRING_SQL + "getOldPassword", userId);
        if (oldHashedPassword.equalsIgnoreCase(oldSavedPassword)) {
            return true;
        }
        return false;
    }

    private String getOldPassword(String userId) throws SQLException, CoreException {
        String oldSavedPassword = (String) this.ibatisSqlMap.queryForObject(STRING_SQL + "getOldPassword", userId);
        return oldSavedPassword;
    }

    private void compareOldPassword(String userId, String oldHashedPassword) throws SQLException, CoreException {
        String oldSavedPassword = (String) this.ibatisSqlMap.queryForObject(STRING_SQL + "getOldPassword", userId);
        if (!oldHashedPassword.equalsIgnoreCase(oldSavedPassword)) {
            throw new CoreException(ERROR_WRONG_OLD_PASSWORD);
        }
    }

}
