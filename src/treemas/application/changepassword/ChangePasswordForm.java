package treemas.application.changepassword;

import treemas.base.core.CoreFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.beanaction.DateTimeConverter;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordForm extends CoreFormBase {
    private static Map convMap = new HashMap();
    private String userId;
    private String lastChgPassword;
    private String passwordExpires;
    private String oldDBPassword;
    private String oldPassword;
    private String newPassword;
    private String confirmPassword;
    private String flagChg;
    private String isExpired;

    public ChangePasswordForm() {
        DateTimeConverter converter = DateTimeConverter.getInstance();
        convMap.put("lastChgPassword", converter);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLastChgPassword() {
        return lastChgPassword;
    }

    public void setLastChgPassword(String lastChgPassword) {
        this.lastChgPassword = lastChgPassword;
    }

    public String getPasswordExpires() {
        return passwordExpires;
    }

    public void setPasswordExpires(String passwordExpires) {
        this.passwordExpires = passwordExpires;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFlagChg() {
        return flagChg;
    }

    public void setFlagChg(String flagChg) {
        this.flagChg = flagChg;
    }

    public void fromBean(Object bean) {
        BeanConverter.convertBeanToString(bean, this, convMap);
    }

    public String getIsExpired() {
        return isExpired;
    }

    public void setIsExpired(String isExpired) {
        this.isExpired = isExpired;
    }

    public String getOldDBPassword() {
        return oldDBPassword;
    }

    public void setOldDBPassword(String oldDBPassword) {
        this.oldDBPassword = oldDBPassword;
    }
}
