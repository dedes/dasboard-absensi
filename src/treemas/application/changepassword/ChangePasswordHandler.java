package treemas.application.changepassword;

import com.google.common.base.Strings;
import org.apache.struts.action.*;
import treemas.base.core.CoreManagerBase;
import treemas.base.setup.SetUpActionBase;
import treemas.globalbean.PasswordManagerGenerator;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.format.TreemasFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChangePasswordHandler extends SetUpActionBase {
    public static final String TASK_CHANGE_PASSWORD = "Change";
    public static final String TASK_CHANGE_EXP_PASSWORD = "ChangeExp";
    public static final String TASK_CANCEL = "Cancel";
    ChangePasswordManager manager = new ChangePasswordManager();
    ChangePasswordValidator validate;

    public ChangePasswordHandler() {
        this.pageMap.put(TASK_CHANGE_PASSWORD, "success");
        this.pageMap.put(TASK_CHANGE_EXP_PASSWORD, "successchange");

        this.exceptionMap.put(TASK_CHANGE_PASSWORD, "view");
        this.exceptionMap.put(TASK_CHANGE_EXP_PASSWORD, "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {
        this.validate = new ChangePasswordValidator(request);
        ChangePasswordForm frm = (ChangePasswordForm) form;
        SessionBean bean = this.getLoginBean(request);
        if (Strings.isNullOrEmpty(frm.getIsExpired())) {
            if (bean == null) {
                try {
                    response.sendRedirect(request.getContextPath());
                    return null;
                } catch (IOException ioe) {
                    this.logger.printStackTrace(ioe);
                    throw new CoreException(CoreManagerBase.ERROR_UNKNOWN);
                }
            }
        }

        String action = frm.getTask();

        String userId = frm.getUserId();
        if (userId == null) {
            userId = this.getLoginId(request);
            frm.setUserId(userId);
        }

        String flagChg = frm.getFlagChg();
        if (flagChg == null) {
            flagChg = bean.getChangedPassword();
            frm.setFlagChg(flagChg);
        }

        if (Global.WEB_TASK_LOAD.equals(action)) {
            this.loadPasswordInfo(frm, userId, flagChg);
        } else if (TASK_CHANGE_PASSWORD.equals(action)) {
            if (validate.validateChangePassword(new ActionMessages(), frm)) {
                try {
                    this.manager.changePassword(userId, frm.getOldPassword(), frm.getNewPassword());
                } catch (CoreException me) {
                    throw me;
                } finally {
                    this.loadPasswordInfo(frm, userId, flagChg);
                }

                bean.setChangedPassword("1");
                frm.setOldPassword("");
                frm.setNewPassword("");
                frm.setConfirmPassword("");

                if ("1".equals(flagChg)) {
                    request.setAttribute("message", resources.getMessage("appmgr.password.success"));
                    return mapping.findForward("view");
                }
            }
        } else if (TASK_CHANGE_EXP_PASSWORD.equals(action)) {
            if (validate.validateChangePassword(new ActionMessages(), frm)) {
                try {
                    this.manager.changePassword(userId, frm.getOldPassword(), frm.getNewPassword()); //SAVE
                } catch (CoreException e) {
                    throw e;
                } finally {
                    this.loadPasswordInfo(frm, userId, flagChg);
                }

                frm.setOldPassword("");
                frm.setNewPassword("");
                frm.setConfirmPassword("");

                if ("1".equals(flagChg)) {

                    this.setMessage(request, "appmgr.password.success", null);
                    return mapping.findForward("successchange");
                }
            }
        } else if (TASK_CANCEL.equals(action)) {
            if ("0".equals(flagChg)) {
                try {
                    response.sendRedirect(request.getContextPath() + "/appadmin/UserDetail.do");
                } catch (Exception ex) {
                    this.logger.printStackTrace(ex);
                    throw new CoreException(ex, CoreManagerBase.ERROR_UNKNOWN);
                }
                return null;
            } else {
                try {
                    response.sendRedirect(request.getContextPath() + "/Login.do?Task=Logout");
                } catch (Exception ex) {
                    this.logger.printStackTrace(ex);
                    throw new CoreException(ex, CoreManagerBase.ERROR_UNKNOWN);
                }
                return null;
            }
        }

        return this.getNextPage(action, mapping);
    }

    private void loadPasswordInfo(ChangePasswordForm form, String userId, String flagChg) throws CoreException {
        ChangePasswordBean cpBean = this.manager.getPasswordInfo(userId);
        form.fromBean(cpBean);
        form.setFlagChg(flagChg);
        if (cpBean.getLast_passchg() != null) {
            form.setLastChgPassword(TreemasFormatter.formatDate(cpBean.getLast_passchg(), TreemasFormatter.DFORMAT_DMY_LONG));
        }
        if (cpBean.getPasswordEd() != null) {
            form.setPasswordExpires(TreemasFormatter.formatDate(cpBean.getPasswordEd(), TreemasFormatter.DFORMAT_DMY_LONG));
        }
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        int code = ex.getErrorCode();
        Object obj = ex.getUserObject();
        ActionMessages msgs = null;
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                ChangePasswordForm frm = ((ChangePasswordForm) form);
                try {
                    this.loadPasswordInfo(frm, frm.getUserId(), frm.getFlagChg());
                } catch (CoreException e) {
                    this.logger.printStackTrace(e);
                }
                this.saveErrors(request, msgs);
                break;
            case ChangePasswordManager.ERROR_WRONG_OLD_PASSWORD:
                msgs.add("oldPassword", new ActionMessage("appmgr.password.mismatch", new Object[]{ex.getUserObject()}));
                this.saveErrors(request, msgs);
                break;
            case PasswordManagerGenerator.PASSWORD_ERROR_MIN_LENGTH:
                msgs.add("newPassword", new ActionMessage("appmgr.password.length", new Object[]{ex.getUserObject()}));
                msgs.add("confirmPassword", new ActionMessage("appmgr.password.length", new Object[]{ex.getUserObject()}));
                this.saveErrors(request, msgs);
                break;
            case PasswordManagerGenerator.PASSWORD_ERROR_IN_HISTORY:
                msgs.add("newPassword", new ActionMessage("appmgr.password.history", new Object[]{ex.getUserObject()}));
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        String task = ((ChangePasswordForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
