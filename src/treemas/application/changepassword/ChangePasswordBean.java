package treemas.application.changepassword;

import java.io.Serializable;
import java.util.Date;

public class ChangePasswordBean implements Serializable {
    private String userId;
    private String oldDBPassword;
    private Date last_passchg;
    private Date passwordEd;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getLast_passchg() {
        return last_passchg;
    }

    public void setLast_passchg(Date last_passchg) {
        this.last_passchg = last_passchg;
    }

    public Date getPasswordEd() {
        return passwordEd;
    }

    public void setPasswordEd(Date passwordEd) {
        this.passwordEd = passwordEd;
    }

    public String getOldDBPassword() {
        return oldDBPassword;
    }

    public void setOldDBPassword(String oldDBPassword) {
        this.oldDBPassword = oldDBPassword;
    }

}
