package treemas.application.userdetail;

import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.List;


public class UserDetailManager extends AppAdminManagerBase {

    private final String STRING_SQL = SQLConstant.SQL_USERDETAIL;

    public UserDetailManager() {
    }

    public List getMenuByUser(String userid) throws AppException {
        List list = null;

        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "geMenuByUser", userid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return list;
    }

}