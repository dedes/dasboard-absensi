package treemas.application.userdetail;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.general.privilege.menu.PrivilegeMenuManager;
import treemas.application.user.UserBean;
import treemas.application.user.UserManager;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.globalbean.SessionBean;
import treemas.util.AppException;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class UserDetailHandler extends AppAdminActionBase {
    private static final String[] SAVE_PARAMETERS =
            {"paging.currentPageNo", "paging.totalRecord", "paging.dispatch",
                    "searchString", "searchType", "userId"};
    PrivilegeMenuManager menumanager = new PrivilegeMenuManager();
    private UserDetailManager manager = new UserDetailManager();
    private UserManager userManager = new UserManager();

    public UserDetailHandler() {
        this.pageMap.put(Global.WEB_TASK_SYNC, "view");
        this.exceptionMap.put(Global.WEB_TASK_SYNC, "view");
    }

    public ActionForward doAction(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws AppException {

        UserDetailForm userDetailForm = (UserDetailForm) form;

        String action = userDetailForm.getTask();
        userDetailForm.getPaging().calculationPage();
        int targetPage = userDetailForm.getPaging().getTargetPageNo();
        SessionBean loginBean = this.getLoginBean(request);

        if (Global.WEB_TASK_LOAD.equals(action)) {
            userDetailForm.setTitle(Global.TITLE_USER_DETAIL);
            UserBean user = this.userManager.getSingleUser(loginBean.getUserid());
            userDetailForm.setUserid(user.getUserid().toUpperCase());
            userDetailForm.setFullname(user.getFullname().toUpperCase());
            String password = loginBean.getPassword();
            int lengthPassword = password.length();
            String hiddenPassword = "";
            for (int i = 0; i < lengthPassword; i++) {
                hiddenPassword += "*";
            }
            userDetailForm.setSqlpassword(hiddenPassword);
            userDetailForm.setJob_descName(user.getJob_descName().toUpperCase());
            userDetailForm.setBranchname(user.getBranchname().toUpperCase());
            this.loadDetail(request, userDetailForm);
        }

        return this.getNextPage(action, mapping);
    }

    private void loadDetail(HttpServletRequest request, UserDetailForm userForm) throws AppException {
        List list = this.manager.getMenuByUser(userForm.getUserid());
        request.setAttribute("list", list);
    }

    private void load(HttpServletRequest request, UserDetailForm userForm, int targetPage) throws AppException {
    }

    public ActionForward handleException(ActionMapping mapping, ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         AppException ex) {
        super.handleException(mapping, form, request, response, ex);
        int code = ex.getErrorCode();
        switch (code) {
            case UserDetailManager.ERROR_DATA_EXISTS:
                this.setMessage(request, "common.dataexists",
                        new Object[]{
                                ex.getUserObject()});
                break;

            case UserDetailManager.ERROR_HAS_CHILD:
                this.setMessage(request, "appmgr.user.haschild", null);
                break;

            case UserDetailManager.ERROR_DB:
                this.logger.printStackTrace(ex);
                this.setMessage(request, "common.dberror", null);
                break;
            default:
                this.setMessage(request, "common.error", null);
                this.logger.printStackTrace(ex);
                break;
        }
        String task = ((UserDetailForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
