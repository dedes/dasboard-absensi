package treemas.application.userdetail;

import java.io.Serializable;


public class UserDetailBean implements Serializable {
    private String userid;
    private String branchid;
    private String branchname;
    private String fullname;
    private String sqlpassword;
    private String job_desc;
    private String job_descName;

    private String appname;
    private String parentid;
    private String menuid;
    private String prompt;
    private String result;
    private String filelevel;
    private String seqorder;
    private String rootseq;
    private String formid;
    private String webspace;
    private String filename;
    private String chainrule;
    private String usract;
    private String resultroot;
    private String promptroot;

    public UserDetailBean() {
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSqlpassword() {
        return sqlpassword;
    }

    public void setSqlpassword(String sqlpassword) {
        this.sqlpassword = sqlpassword;
    }

    public String getJob_desc() {
        return job_desc;
    }

    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }

    public String getJob_descName() {
        return job_descName;
    }

    public void setJob_descName(String job_descName) {
        this.job_descName = job_descName;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFilelevel() {
        return filelevel;
    }

    public void setFilelevel(String filelevel) {
        this.filelevel = filelevel;
    }

    public String getSeqorder() {
        return seqorder;
    }

    public void setSeqorder(String seqorder) {
        this.seqorder = seqorder;
    }

    public String getRootseq() {
        return rootseq;
    }

    public void setRootseq(String rootseq) {
        this.rootseq = rootseq;
    }

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getChainrule() {
        return chainrule;
    }

    public void setChainrule(String chainrule) {
        this.chainrule = chainrule;
    }

    public String getUsract() {
        return usract;
    }

    public void setUsract(String usract) {
        this.usract = usract;
    }

    public String getResultroot() {
        return resultroot;
    }

    public void setResultroot(String resultroot) {
        this.resultroot = resultroot;
    }

    public String getPromptroot() {
        return promptroot;
    }

    public void setPromptroot(String promptroot) {
        this.promptroot = promptroot;
    }

    public String getWebspace() {
        return webspace;
    }

    public void setWebspace(String webspace) {
        this.webspace = webspace;
    }


}
