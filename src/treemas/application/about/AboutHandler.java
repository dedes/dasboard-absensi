package treemas.application.about;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import treemas.application.general.parameter.ParameterBean;
import treemas.application.general.parameter.ParameterManager;
import treemas.base.core.CoreActionBase;
import treemas.base.sec.term.DatePeriod;
import treemas.base.sec.term.HardwareMAC;
import treemas.base.sec.term.UserImpl;
import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.format.TreemasFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class AboutHandler extends CoreActionBase {
    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws CoreException {

        ParameterManager parammanager = new ParameterManager();
        UserImpl userImplManager = new UserImpl();
        AboutForm frm = (AboutForm) form;
        try {
            List listParam = parammanager.getListAbout();
            String useUser = "0";
            String usePeriod = "0";
            String useMachine = "0";
            for (int i = 0; i < listParam.size(); i++) {
                ParameterBean bean = (ParameterBean) listParam.get(i);
                if (null != bean) {
                    if (!Strings.isNullOrEmpty(bean.getId()) && Global.ABOUT_USER.equals(bean.getId())) {
                        useUser = bean.getVal();
                    } else if (!Strings.isNullOrEmpty(bean.getId()) && Global.ABOUT_PERIOD.equals(bean.getId())) {
                        usePeriod = bean.getVal();
                    } else if (!Strings.isNullOrEmpty(bean.getId()) && Global.ABOUT_MACHINE.equals(bean.getId())) {
                        useMachine = bean.getVal();
                    } else if (!Strings.isNullOrEmpty(bean.getId()) && Global.ABOUT_COMPANY_CLIENT.equals(bean.getId())) {
                        frm.setCompanyClient(bean.getVal());
                    }
                }
            }

            frm.setUseActiveUser(useUser);
            frm.setUsePeriodeDate(usePeriod);
            frm.setUseServerImplement(useMachine);

            if (Global.STRING_TRUE.equals(frm.getUseActiveUser())) {
                int userActive = userImplManager.getActiveUser();
                frm.setNumOfActiveUser(userActive + " of " + userImplManager.LIMIT);
            }

            if (Global.STRING_TRUE.equals(frm.getUsePeriodeDate())) {
                String datePeriod = DatePeriod.ED;
                String formatDate = DatePeriod.EDF;
                Date date = TreemasFormatter.parseDate(datePeriod, formatDate);
                frm.setPeriodDate(TreemasFormatter.formatDate(date, TreemasFormatter.DFORMAT_DMY_LONG));
            }

            if (Global.STRING_TRUE.equals(frm.getUseServerImplement())) {
                String macId = HardwareMAC.MAC_ID;
                frm.setServerImplement(macId);
            }

        } catch (ParseException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(SetUpManagerBase.ERROR_UNKNOWN);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(SetUpManagerBase.ERROR_DB);
        } catch (AppException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(SetUpManagerBase.ERROR_DB);
        }

        return mapping.findForward("view");
    }
}
