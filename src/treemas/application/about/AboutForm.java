package treemas.application.about;

import treemas.base.core.CoreFormBase;

public class AboutForm extends CoreFormBase {
    //	Template Application Product/Project Development Version:v0.01<br/>Build id:20151208-001-001<br/>&copy;
    private String tOS = "Copyright PT Treemas Solusi Utama, 2015, All rights reserved.";
    private String developer = "PT Treemas Solusi Utama";
    private String companyClient;
    private String periodDate;
    private String usePeriodeDate;
    private String numOfActiveUser;
    private String useActiveUser;
    private String serverImplement;
    private String useServerImplement;

    public AboutForm() {
        this.tOS = "Copyright PT Treemas Solusi Utama, 2015, All rights reserved.";
        this.developer = "PT Treemas Solusi Utama";
    }

    public String gettOS() {
        return tOS;
    }

    public void settOS(String tOS) {
        this.tOS = tOS;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getPeriodDate() {
        return periodDate;
    }

    public void setPeriodDate(String periodDate) {
        this.periodDate = periodDate;
    }

    public String getUsePeriodeDate() {
        return usePeriodeDate;
    }

    public void setUsePeriodeDate(String usePeriodeDate) {
        this.usePeriodeDate = usePeriodeDate;
    }

    public String getNumOfActiveUser() {
        return numOfActiveUser;
    }

    public void setNumOfActiveUser(String numOfActiveUser) {
        this.numOfActiveUser = numOfActiveUser;
    }

    public String getUseActiveUser() {
        return useActiveUser;
    }

    public void setUseActiveUser(String useActiveUser) {
        this.useActiveUser = useActiveUser;
    }

    public String getServerImplement() {
        return serverImplement;
    }

    public void setServerImplement(String serverImplement) {
        this.serverImplement = serverImplement;
    }

    public String getUseServerImplement() {
        return useServerImplement;
    }

    public void setUseServerImplement(String useServerImplement) {
        this.useServerImplement = useServerImplement;
    }

    public String getCompanyClient() {
        return companyClient;
    }

    public void setCompanyClient(String companyClient) {
        this.companyClient = companyClient;
    }

}
