package treemas.application.privilege;

import treemas.application.audit.AuditManager;
import treemas.application.general.lookup.approval.privilege.LookUpPrivilegeBean;
import treemas.application.general.privilege.menu.PrivilegeMenuBean;
import treemas.application.privilege.member.PrivilegeMemberBean;
import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrivilegeManager extends AppAdminManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_PRIVILEGE;
    private AuditManager audittrailManager = new AuditManager();

    public PageListWrapper getAllPrivilege(int pageNumber, PrivilegeSearch search) throws AppException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllMenu", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllMenu", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }


    public List getAllMember(PrivilegeSearch search) throws AppException {
        try {
            return this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllMember", search);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public PrivilegeBean getSinglePrivilege(String groupid) throws AppException {
        PrivilegeBean bean = new PrivilegeBean();

        try {
            bean = (PrivilegeBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSinglePrivilege", groupid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return bean;
    }

    public List getAllMenuPrivilege(String applicationId) throws AppException {
        List list = null;

        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllMenuApplication", applicationId);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return list;
    }

    public String getFolderRoot(String groupid) throws AppException {
        String result = null;
        try {
            result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getFolderRoot", groupid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
        return result;
    }

    public String getDirectoryRoot(String groupid) throws AppException {
        String result = null;
        try {
            result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getDirectoryRoot", groupid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
        return result;
    }

    public String[] getAllMenuId(String appid, String groupid) throws AppException {
        String[] result = null;
        Map param = new HashMap();
        param.put("groupid", groupid);
        param.put("appid", appid);
        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getListMenuId", param);
            result = new String[list.size()];
            list.toArray(result);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
        return result;
    }

    /*
     * INSERT DATA TO PRIVILEGEGROUP WAITING FOR APPROVAL
     * */
    public void insertNewPrivilege(PrivilegeForm form, String userUpdate) throws AppException {
        PrivilegeBean bean = new PrivilegeBean();
        BeanConverter.convertBeanFromString(form, bean);
        bean.setUsrupd(userUpdate);
        bean.setActive("1");
        bean.setIsapproved("0");

        if (bean.getGroupname() == null) {
            try {
                String groupname = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getGroupName", bean.getGroupid());
                bean.setGroupname(groupname);
            } catch (SQLException e) {
                this.logger.printStackTrace(e);
                throw new AppException(ERROR_DB);
            }
        }

        try {
            this.ibatisSqlMap.startTransaction();
            if (Global.ACTION_ADD.equals(form.getUsract())) {
                this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), bean, userUpdate, Global.ACTION_ADD_NEW_PRIVILEGE);
            } else {
                this.ibatisSqlMap.update(this.STRING_SQL + "updateFlag", form.getGroupid());
                this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), bean, userUpdate, Global.ACTION_ADD_NEW_PRIVILEGE);
            }
            this.ibatisSqlMap.insert(this.STRING_SQL + "insertPrivilegeNew", bean);
            this.ibatisSqlMap.commitTransaction();
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        } finally {
            try {
                this.ibatisSqlMap.endTransaction();
            } catch (SQLException e) {
                this.logger.printStackTrace(e);
                throw new AppException(ERROR_DB);
            }
        }

    }

    /*
     * INSERT DATA TO PRIVILEGEMENU WAITING FOR APPROVAL
     * */
    public void insertMenuPrivilege(PrivilegeForm form, String userUpdate) throws AppException {
        String groupid = form.getGroupid();
        String appid = form.getAppid();
        String[] menuId = form.getMenuidlist();
        if (menuId.length == 0) {
            try {
                List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getMenuId", groupid);
                menuId = new String[list.size()];
                list.toArray(menuId);
            } catch (SQLException e) {
                this.logger.printStackTrace(e);
                throw new AppException(ERROR_DB);
            }
        }
        String updatemenu = "1";
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.startBatch();
                for (int i = 0; i < menuId.length; i++) {
                    PrivilegeMenuBean bean = new PrivilegeMenuBean();
                    bean.setMenu(groupid, appid, userUpdate, updatemenu);
                    bean.setMenuid(menuId[i]);
                    this.ibatisSqlMap.insert(this.STRING_SQL + "insertMenuNew", bean);
                }
                this.ibatisSqlMap.executeBatch();
                this.ibatisSqlMap.commitTransaction();

            } finally {
                this.ibatisSqlMap.endTransaction();
            }
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.startBatch();

                for (int i = 0; i < menuId.length; i++) {

                    PrivilegeMenuBean bean = new PrivilegeMenuBean();
                    bean.setMenu(groupid, appid, userUpdate, updatemenu);
                    bean.setMenuid(menuId[i]);

                    int countMenuId = 0;
                    countMenuId = this.getCountMenuId(groupid, appid, menuId[i]);

                    if (Global.ACTION_ADD.equals(form.getUsract()))
                        this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), bean, userUpdate, Global.ACTION_ADD_NEW_MENU_PRIVILEGE);
                    else if (countMenuId > 0)
                        this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), bean, userUpdate, Global.ACTION_ADD_NEW_MENU_PRIVILEGE);
                    else
                        this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), bean, userUpdate, Global.ACTION_ADD_NEW_MENU_PRIVILEGE);
                }
                this.ibatisSqlMap.executeBatch();
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public void insertGroupMemberPrivilege(String groupid, String appid, String usrupd, String action) throws AppException {
        String[] userid;

        try {
            try {
                List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getUserId", groupid);
                userid = new String[list.size()];
                list.toArray(userid);
            } catch (SQLException e) {
                this.logger.printStackTrace(e);
                throw new AppException(ERROR_DB);
            }

            try {
                this.ibatisSqlMap.startTransaction();
                for (int i = 0; i < userid.length; i++) {
                    Map param = new HashMap();
                    param.put("groupid", groupid);
                    param.put("appid", appid);
                    param.put("usrupd", usrupd);
                    param.put("userid", userid[i]);

                    this.ibatisSqlMap.insert(this.STRING_SQL + "insertPrivilegeMemberNew", param);

                    PrivilegeMemberBean mbean = new PrivilegeMemberBean();
                    mbean.setGroupid(groupid);
                    mbean.setUserid(userid[i]);
                    mbean.setUsrupd(usrupd);
                    if (Global.WEB_TASK_INSERT.equals(action)) {
                        this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), mbean, mbean.getUsrupd(), "GRANT PRIVILEGE TO " + mbean.getUserid().toUpperCase());
                    } else {
                        this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), mbean, mbean.getUsrupd(), "GRANT PRIVILEGE TO " + mbean.getUserid().toUpperCase());
                    }
                    this.ibatisSqlMap.commitTransaction();
                }

            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public LookUpPrivilegeBean getDetailApproval(String groupid) throws AppException {
        LookUpPrivilegeBean result = null;

        try {
            result = (LookUpPrivilegeBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getDetailApproval", groupid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public String[] getAllUserMemberPrivilege(String userid) throws AppException {
        String[] result = new String[0];
        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllPrivilegeUserMember", userid);
            if (null != list) {
                if (list.size() > 0) {
                    result = new String[list.size()];
                    list.toArray(result);
                    return result;
                }
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
        return result;
    }

    public void deleteUserPrivilege(String userid, int flag, String usrupd) throws AppException {
        try {
            String[] privileges = this.getAllUserMemberPrivilege(userid);
            if (privileges.length > 0 && privileges != null) {
                try {
                    this.ibatisSqlMap.startTransaction();
                    this.ibatisSqlMap.delete(this.STRING_SQL + "deletePrivilegeMember", userid);
                    if (1 == flag) {
                        for (int i = 0; i < privileges.length; i++) {
                            this.audittrailManager.auditDelete(usrupd, Global.TABLE_NAME_SYS_APPGROUPMEMBER, userid, Global.TABLE_FIELD_AUDIT_DELETE_GROUP_MEMBER, privileges[i], null, Global.ACTION_DELETE);
                        }
                    }
                    this.ibatisSqlMap.commitTransaction();
                } finally {
                    this.ibatisSqlMap.endTransaction();
                }
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public void insertUserPrivilege(PrivilegeMemberBean bean, String action) throws AppException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.insert(this.STRING_SQL + "insertPrivilegeMember", bean);
                if (Global.WEB_TASK_INSERT.equals(action)) {
                    this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), bean, bean.getUsrupd(), "GRANT PRIVILEGE TO " + bean.getUserid().toUpperCase());
                } else {
                    this.auditManager.auditEdit(this.ibatisSqlMap.getCurrentConnection(), bean, bean.getUsrupd(), "GRANT PRIVILEGE TO " + bean.getUserid().toUpperCase());
                }
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public void insertUserPrivilegeMemberNew(String userid) throws AppException {
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.insert(this.STRING_SQL + "insertPrivilegeAppGroupMemberNew", userid);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }


    public void deleteGroupMemberPrivilege(PrivilegeForm form, String usrupd) throws AppException {
        try {
            PrivilegeBean bean = new PrivilegeBean();
            form.toBean(bean);

            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.delete(this.STRING_SQL + "deleteGroupMember", bean);

                this.audittrailManager.auditDelete(usrupd, Global.TABLE_NAME_SYS_APPGROUPMEMBER, bean.getGroupid() + "||" + bean.getAppid(), Global.TABLE_FIELD_AUDIT_DELETE_GROUP_MEMBER, bean.getGroupid(), null, Global.ACTION_DELETE);

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public void deleteMenuPrivilege(PrivilegeForm form, String usrupd) throws AppException {
        try {
            PrivilegeBean bean = new PrivilegeBean();
            form.toBean(bean);

            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.delete(this.STRING_SQL + "deleteMenu", bean);

                this.audittrailManager.auditDelete(usrupd, Global.TABLE_NAME_SYS_APPGROUPMENU, bean.getGroupid() + "||" + bean.getAppid(), Global.TABLE_FIELD_AUDIT_DELETE_GROUP_MEMBER, bean.getGroupid(), null, Global.ACTION_DELETE);

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public void deleteGroupPrivilege(PrivilegeForm form, String usrupd) throws AppException {
        try {
            PrivilegeBean bean = new PrivilegeBean();
            form.toBean(bean);

            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.delete(this.STRING_SQL + "deleteGroupPrivilege", bean);

                this.audittrailManager.auditDelete(usrupd, Global.TABLE_NAME_SYS_APPGROUP, bean.getGroupid(), Global.TABLE_FIELD_AUDIT_DELETE_GROUP_MEMBER, bean.getGroupid(), null, Global.ACTION_DELETE);

                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

    public Integer getCountMenuId(String groupid, String appid, String menuid) throws AppException {
        Integer result = 0;
        try {
            Map param = new HashMap();
            param.put("groupid", groupid);
            param.put("appid", appid);
            param.put("menuid", menuid);

            result = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countMenuId", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
        return result;
    }


//	
//	public void approve(PrivilegeForm form, String userUpdate)throws AppException{
//		String groupid = form.getGroupid();
//		String appid = form.getAppid();
//		String[] menuId = form.getMenuidlist();
//		String updatemenu = "1";
//		
//		Map param = new HashMap();
//		param.put("groupid", groupid);
//		param.put("appid", appid);
//		try {
//			this.ibatisSqlMap.startTransaction();
//			
//			this.ibatisSqlMap.delete(this.STRING_SQL+"deleteMenu", param);
//			
//			for (int i = 0; i < menuId.length; i++) {
//				PrivilegeMenuBean bean = new PrivilegeMenuBean();
//				bean.setMenu(groupid, appid, userUpdate, updatemenu);
//				bean.setMenuid(menuId[i]);
//				this.ibatisSqlMap.update(this.STRING_SQL+"insertMenuNew", bean);
//			}
//			
//			this.ibatisSqlMap.commitTransaction();
//		} catch (SQLException e) {
//			this.logger.printStackTrace(e);
//			throw new AppException(ERROR_DB);
//		}finally{
//			try {
//				this.ibatisSqlMap.endTransaction();
//			} catch (SQLException e) {
//				this.logger.printStackTrace(e);
//				throw new AppException(ERROR_DB);
//			}
//		}
//		
//	}

//	public PrivilegeBean getDetailPrivilege(String groupid)  throws AppException {
//		PrivilegeBean bean = new PrivilegeBean();
//		
//		try{
//			bean = (PrivilegeBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL+"getDetailPrivilege",groupid);
//		} catch (SQLException e) {
//			this.logger.printStackTrace(e);
//			throw new AppException(ERROR_DB);
//		}
//		
//		return bean;
//	}
//	
}
