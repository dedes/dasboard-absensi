package treemas.application.privilege;

import treemas.application.general.lookup.user.UserSearch;
import treemas.util.ibatis.SearchFormParameter;

public class PrivilegeSearch extends SearchFormParameter {
    private String appid;
    private String groupid;
    private String groupname;
    private String appname;
    private String parentid;
    private String menuid;
    private String prompt;
    private String result;
    private String filelevel;
    private String seqorder;
    private String rootseq;

    private UserSearch usermembersearch = new UserSearch();

    public PrivilegeSearch() {
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFilelevel() {
        return filelevel;
    }

    public void setFilelevel(String filelevel) {
        this.filelevel = filelevel;
    }

    public String getSeqorder() {
        return seqorder;
    }

    public void setSeqorder(String seqorder) {
        this.seqorder = seqorder;
    }

    public String getRootseq() {
        return rootseq;
    }

    public void setRootseq(String rootseq) {
        this.rootseq = rootseq;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public UserSearch getUsermembersearch() {
        return usermembersearch;
    }

    public void setUsermembersearch(UserSearch usermembersearch) {
        this.usermembersearch = usermembersearch;
    }


}
