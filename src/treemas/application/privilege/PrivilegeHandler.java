package treemas.application.privilege;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.privilege.menu.PrivilegeMenuManager;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.format.TreemasFormatter;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class PrivilegeHandler extends AppAdminActionBase {

    private static final String[] SAVE_PARAMETERS = {
            "paging.currentPageNo", "paging.totalRecord", "paging.dispatch",
            "search.appid", "search.groupid"};
    PrivilegeManager manager = new PrivilegeManager();
    PrivilegeMenuManager menumanager = new PrivilegeMenuManager();
    MultipleManager multipleManager = new MultipleManager();
    PrivilegeValidator validator;

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        this.validator = new PrivilegeValidator(request);
        PrivilegeForm frm = (PrivilegeForm) form;
        String task = frm.getTask();
        int page = frm.getPaging().getCurrentPageNo();
        frm.getPaging().calculationPage();
        this.loadAppAdmin(request);
        this.loadAppGroup(request);

        if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)
                || Global.WEB_TASK_SHOW.equals(task)) {

            frm.fillCurrentUri(request, SAVE_PARAMETERS);
            this.load(request, frm);
        } else if (Global.WEB_TASK_ADD.equals(task)) {
            this.clearForm(frm);
        } else if (Global.WEB_TASK_EDIT.equals(task)) {
            String groupname = frm.getGroupname();
            this.loadSinglePrivilege(frm);
            if (!Strings.isNullOrEmpty(groupname))
                frm.setGroupname(groupname);
        } else if (Global.WEB_TASK_NEXT_ADD.equals(task)) {
            if (this.validator.validatePrivilegeInsert(new ActionMessages(), frm)) {
                String applicationId = frm.getAppid();
                String applicationName = this.getDirectoryRoot(applicationId);

                if (!Strings.isNullOrEmpty(applicationName)) {
                    applicationName = TreemasFormatter.toCamelCase(applicationName);
                }
                frm.setAppname(applicationName);
                this.loadListMenu(request, frm);
            }
        } else if (Global.WEB_TASK_NEXT_EDIT.equals(task)) {
            if (this.validator.validatePrivilegeEdit(new ActionMessages(), frm)) {
                String applicationId = frm.getAppid();
                String applicationName = this.getDirectoryRoot(applicationId);

                if (!Strings.isNullOrEmpty(applicationName)) {
                    applicationName = TreemasFormatter.toCamelCase(applicationName);
                }
                frm.setAppname(applicationName);
                this.loadListMenu(request, frm);
            }
        } else if (Global.WEB_TASK_INSERT.equals(task)) {
            frm.setUsract(Global.ACTION_ADD);
            this.insertNewPrivilege(request, frm, Global.ACTION_SAVE);
            this.load(request, frm);
        } else if (Global.WEB_TASK_UPDATE.equals(task)) {
            frm.setUsract(Global.ACTION_UPDATE);
            this.insertNewPrivilege(request, frm, Global.ACTION_UPDATE);
            this.load(request, frm);
        } else if (Global.WEB_TASK_DELETE.equals(task)) {
            frm.setUsract(Global.ACTION_DELETE);
            this.deletePrivilege(request, frm);
            this.load(request, frm);
        } else if (Global.WEB_TASK_MEMBER.equals(task)) {
            this.loadMember(request, frm);
        }
        return this.getNextPage(task, mapping);
    }

    private void insertNewPrivilege(HttpServletRequest request, PrivilegeForm form, String status) throws AppException {
        this.manager.insertNewPrivilege(form, this.getLoginId(request));
        this.manager.insertMenuPrivilege(form, this.getLoginId(request));
        if (status == Global.ACTION_SAVE) {
            request.setAttribute("message", resources.getMessage("common.process.save"));
        } else {
            request.setAttribute("message", resources.getMessage("common.process.update"));
        }

    }

    private void deletePrivilege(HttpServletRequest request, PrivilegeForm form) throws AppException {
        this.manager.insertMenuPrivilege(form, this.getLoginId(request));
        this.manager.deleteMenuPrivilege(form, this.getLoginId(request));

        this.manager.insertGroupMemberPrivilege(form.getGroupid(), form.getAppid(), this.getLoginId(request), Global.ACTION_EDIT);
        this.manager.deleteGroupMemberPrivilege(form, this.getLoginId(request));

        this.manager.insertNewPrivilege(form, this.getLoginId(request));
        this.manager.deleteGroupPrivilege(form, this.getLoginId(request));

    }

    private void loadAppAdmin(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getAppAdmin(param);
        request.setAttribute("listAppAdmin", list);
    }

    private void loadAppGroup(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getAppGroup(param);
        request.setAttribute("listAppGroup", list);
    }

    private void loadMember(HttpServletRequest request, PrivilegeForm form) throws AppException {
        this.setListToRequest("listMember", form.getClass(), request, this.manager.getAllMember(form.getSearch()));
    }

    private void load(HttpServletRequest request, PrivilegeForm form) throws AppException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;

        PageListWrapper result = this.manager.getAllPrivilege(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);

        this.loadAppAdmin(request);
        this.loadAppGroup(request);
    }

    private void loadSinglePrivilege(PrivilegeForm form) throws AppException {
        PrivilegeBean bean = this.manager.getSinglePrivilege(form.getGroupid());
        BeanConverter.convertBeanToString(bean, form);
    }

    private String getDirectoryRoot(String applicationId) throws AppException {
        return this.manager.getDirectoryRoot(applicationId);
    }

    private void loadListMenu(HttpServletRequest request, PrivilegeForm form) throws AppException {
        String[] menuIdList = manager.getAllMenuId(form.getAppid(), form.getGroupid());
        form.setMenuidlist(menuIdList);
        List list = this.menumanager.menuTracert(form.getAppid());
        request.setAttribute("listMenuPrivilege", list);
    }

    private void clearForm(PrivilegeForm form) throws AppException {
        form.setGroupid("");
        form.setGroupname("");
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((PrivilegeForm) form).setTask(((PrivilegeForm) form).resolvePreviousTask());

        String task = ((PrivilegeForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
