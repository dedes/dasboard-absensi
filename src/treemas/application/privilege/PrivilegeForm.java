package treemas.application.privilege;

import treemas.base.appadmin.AppAdminFormBase;
import treemas.util.constant.Global;

public class PrivilegeForm extends AppAdminFormBase {
    private String appRoot = Global.APPLICATION_ROOT_DIRECTORY;

    private String appid;
    private String groupid;
    private String groupname;
    private String active;
    private String template;
    private String isapproved;
    private String appname;
    private String parentid;
    private String menuid;
    private String prompt;
    private String result;
    private String formid;
    private String formfilename;
    private String usrupd;
    private String dtmupd;
    private String updatemenu;
    private String filelevel;
    private String chain;
    private String usract;
    private String[] menuidlist = {};
    private String groupmember;

    private PrivilegeSearch search = new PrivilegeSearch();

    public PrivilegeForm() {
        this.template = "0";
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppRoot() {
        return appRoot;
    }

    public void setAppRoot(String appRoot) {
        this.appRoot = appRoot;
    }

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid;
    }

    public PrivilegeSearch getSearch() {
        return search;
    }

    public void setSearch(PrivilegeSearch search) {
        this.search = search;
    }


    public String getFormfilename() {
        return formfilename;
    }

    public void setFormfilename(String formfilename) {
        this.formfilename = formfilename;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getUpdatemenu() {
        return updatemenu;
    }

    public void setUpdatemenu(String updatemenu) {
        this.updatemenu = updatemenu;
    }

    public String getFilelevel() {
        return filelevel;
    }

    public void setFilelevel(String filelevel) {
        this.filelevel = filelevel;
    }

    public String getChain() {
        return chain;
    }

    public void setChain(String chain) {
        this.chain = chain;
    }

    public String[] getMenuidlist() {
        return menuidlist;
    }

    public void setMenuidlist(String[] menuidlist) {
        this.menuidlist = menuidlist;
    }

    public String getUsract() {
        return usract;
    }

    public void setUsract(String usract) {
        this.usract = usract;
    }

    public String getGroupmember() {
        return groupmember;
    }

    public void setGroupmember(String groupmember) {
        this.groupmember = groupmember;
    }

}
