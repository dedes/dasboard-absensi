package treemas.application.privilege.member;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;

public class PrivilegeMemberBean implements Serializable, Auditable {
    private static final String[] AUDIT_COLUMNS = new String[]{"groupid", "userid", "usrupd"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{"GROUPID", "USERID", "USRUPD"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_SYS_APPGROUPMEMBER;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"groupid", "userid"};

    private String groupid;
    private String userid;
    private String usrupd;

    public PrivilegeMemberBean() {
    }

    public PrivilegeMemberBean(String groupid, String userid, String usrupd) {
        this.groupid = groupid;
        this.userid = userid;
        this.usrupd = usrupd;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }


}
