package treemas.application.privilege;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;
import java.util.Date;

public class PrivilegeBean implements Serializable, Auditable {

    private static final String[] AUDIT_COLUMNS = new String[]{"groupid", "appid", "groupname", "active", "usrupd", "template", "isapproved"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{"GROUPID", "APPID", "GROUPNAME", "ACTIVE", "USRUPD", "TEMPLATE", "ISAPPROVED"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_SYS_APPGROUP;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"groupid", "appid"};

    private String groupid;
    private String appid;
    private String groupname;
    private String active;
    private String usrupd;
    private Date dtmupd;
    private String template;
    private String isapproved;

    private String appname;
    private String parentid;
    private String menuid;
    private String prompt;
    private String result;
    private String filelevel;
    private String seqorder;
    private String rootseq;
    private String formid;
    private String filename;
    private String chainrule;
    private String usract;

    public PrivilegeBean() {
    }


    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }


    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFilelevel() {
        return filelevel;
    }

    public void setFilelevel(String filelevel) {
        this.filelevel = filelevel;
    }

    public String getSeqorder() {
        return seqorder;
    }

    public void setSeqorder(String seqorder) {
        this.seqorder = seqorder;
    }

    public String getRootseq() {
        return rootseq;
    }

    public void setRootseq(String rootseq) {
        this.rootseq = rootseq;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getChainrule() {
        return chainrule;
    }

    public void setChainrule(String chainrule) {
        this.chainrule = chainrule;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public Date getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(Date dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public String getUsract() {
        return usract;
    }

    public void setUsract(String usract) {
        this.usract = usract;
    }

}
