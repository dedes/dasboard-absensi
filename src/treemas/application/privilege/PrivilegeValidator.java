package treemas.application.privilege;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;


public class PrivilegeValidator extends Validator {
    private PrivilegeManager manager = new PrivilegeManager();

    public PrivilegeValidator(HttpServletRequest request) {
        super(request);
    }

    public PrivilegeValidator(Locale locale) {
        super(locale);
    }

    public boolean validatePrivilegeInsert(ActionMessages messages, PrivilegeForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        try {
            PrivilegeBean bean = manager.getSinglePrivilege(form.getGroupid());
            if (null != bean) {
                messages.add("groupid", new ActionMessage("errors.duplicate", "groupid"));
                throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }


        this.textValidator(messages, form.getGroupid(), "app.privilege.groupid", "groupid", "1", "10", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getAppid(), "app.privilege.appid", "appid", "1", "10", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getGroupname(), "app.privilege.groupname", "groupname", "1", "50", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
        return ret;

    }

    public boolean validatePrivilegeEdit(ActionMessages messages, PrivilegeForm form) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        try {
            PrivilegeBean bean = manager.getSinglePrivilege(form.getGroupid());
            if (null != bean) {
                if (Global.STRING_FALSE.equals(bean.getIsapproved())) {
                    messages.add("id", new ActionMessage("errors.waiting.approval", "General Parameter"));
                    throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
                }
            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }


        this.textValidator(messages, form.getGroupid(), "app.privilege.groupid", "groupid", "1", "10", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getAppid(), "app.privilege.appid", "appid", "1", "10", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getGroupname(), "app.privilege.groupname", "groupname", "1", "50", ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
        return ret;

    }
}
