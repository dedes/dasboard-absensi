package treemas.application.audit;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.multiple.MultipleManager;
import treemas.base.setup.SetUpActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AuditHandler extends SetUpActionBase {

    MultipleManager multipleManager = new MultipleManager();
    AuditManager manager = new AuditManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        AuditForm frm = (AuditForm) form;
        frm.getPaging().calculationPage();
        String task = frm.getTask();
        String call = frm.getParam();

        if (!Strings.isNullOrEmpty(call)) {
            String[] datas = this.getTableDetail(call);
            if (null != datas) {
                frm.getSearch().setTableId(datas[0]);
                frm.getSearch().setTableName(datas[1]);
                frm.setTableId(datas[0]);
                frm.setTableName(datas[1]);
                frm.setTitle(datas[2]);
                frm.setParam(call);
            }
        }

        if (Global.WEB_TASK_SHOW.equals(task) || Global.WEB_TASK_LOAD.equals(task) || Global.WEB_TASK_SEARCH.equals(task)) {
            this.loadDropDown(request);
            this.load(request, frm);
        } else if (Global.WEB_TASK_GENERATE.equals(task)) {
            String tableId = !Strings.isNullOrEmpty(frm.getSearch().getTableId()) ? frm.getSearch().getTableId() : "";
            String tableName = !Strings.isNullOrEmpty(frm.getSearch().getTableName()) ? frm.getSearch().getTableName() : "";
            String action = !Strings.isNullOrEmpty(frm.getSearch().getAction()) ? frm.getSearch().getAction() : "";
            String trxDate = !Strings.isNullOrEmpty(frm.getSearch().getTransactionDate()) ? frm.getSearch().getTransactionDate() : "";
            String trxDateAfter = !Strings.isNullOrEmpty(frm.getSearch().getTransactionDateAfter()) ? frm.getSearch().getTransactionDateAfter() : "";
            String active = !Strings.isNullOrEmpty(frm.getSearch().getActive()) ? frm.getSearch().getActive() : "";
            String dateKey = !Strings.isNullOrEmpty(frm.getSearch().getDateKey()) ? frm.getSearch().getDateKey() : "";

            frm.setTableId(tableId);
            frm.setTableName(tableName);
            frm.setAction(action);
            frm.setTransactionDate(trxDate);
            frm.setTransactionDateAfter(trxDateAfter);
            frm.setActive(active);
            frm.setDateKey(dateKey);
        }

        return this.getNextPage(task, mapping);

    }

    private String getTableId(String param) throws AppException {
        String result = this.manager.getTableId(param);
        return result;
    }

    private String[] getTableDetail(String param) throws AppException {
        String[] results = this.manager.getTableDetail(param);
        return results;
    }

    private void loadDropDown(HttpServletRequest request)
            throws AppException {
        MultipleParameterBean bean = new MultipleParameterBean();
        bean.setActive(Global.STRING_TRUE);
        bean.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getAuditCombo(bean);
        request.setAttribute("listTable", list);
    }

    private void load(HttpServletRequest request, AuditForm form) throws AppException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;

        if ("40".equals(form.getSearch().getDateKey())) {
            String before = form.getSearch().getTransactionDate();
            String after = form.getSearch().getTransactionDateAfter();
            if (Strings.isNullOrEmpty(before) || Strings.isNullOrEmpty(after)) {
                String lang = this.getLang(request);
                String message = "Tanggal Sebelum atau Sesudah belum diisi";
                if (Global.LANG_EN.equals(lang)) {
                    message = "Date Before or After is Null or Empty";
                }
                request.setAttribute("message", message);
                throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA);
            }
        }

        PageListWrapper result = this.manager.getAllAudit(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                AuditForm frm = (AuditForm) form;
                frm.getSearch().setDateKey("");
                try {
                    this.loadDropDown(request);
                    this.load(request, frm);
                } catch (AppException e) {
//				this.logger.printStackTrace(e);
                }
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((AuditForm) form).setTask(((AuditForm) form).resolvePreviousTask());

        String task = ((AuditForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
