package treemas.application.audit;

import treemas.base.setup.SetUpFormBase;

public class AuditForm extends SetUpFormBase {

    private String param;
    private String transactionDate;
    private String transactionDateAfter;
    private String transactionTime;
    private String action;
    private String userId;
    private String tableName;
    private String tableId;
    private String keyValue;
    private String fieldName;
    private String oldValue;
    private String newValue;
    private String auditDesc;
    private String dateKey;
    private AuditSearch search = new AuditSearch();

    public AuditForm() {
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public void setAuditdesc(String auditDesc) {
        this.auditDesc = auditDesc;
    }

    public String getAuditDesc() {
        return auditDesc;
    }

    public AuditSearch getSearch() {
        return search;
    }

    public void setSearch(AuditSearch search) {
        this.search = search;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getTransactionDateAfter() {
        return transactionDateAfter;
    }

    public void setTransactionDateAfter(String transactionDateAfter) {
        this.transactionDateAfter = transactionDateAfter;
    }

    public String getDateKey() {
        return dateKey;
    }

    public void setDateKey(String dateKey) {
        this.dateKey = dateKey;
    }


}
