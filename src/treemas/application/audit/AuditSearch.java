package treemas.application.audit;

import treemas.util.ibatis.SearchFormParameter;

public class AuditSearch extends SearchFormParameter {
    private String call;
    private String dateKey = "";
    private String transactionDate;
    private String transactionDateAfter;
    private String userId;
    private String tableName;
    private String tableId;
    private String keyValue;
    private String auditDesc;
    private String action;

    public AuditSearch() {
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public String getAuditDesc() {
        return auditDesc;
    }

    public void setAuditDesc(String auditDesc) {
        this.auditDesc = auditDesc;
    }

    public String getTransactionDateAfter() {
        return transactionDateAfter;
    }

    public void setTransactionDateAfter(String transactionDateAfter) {
        this.transactionDateAfter = transactionDateAfter;
    }

    public String getDateKey() {
        return dateKey;
    }

    public void setDateKey(String dateKey) {
        this.dateKey = dateKey;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }


}
