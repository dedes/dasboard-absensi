package treemas.application.audit;

import com.google.common.base.Strings;
import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.constant.Global;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuditManager extends SetUpManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_AUDIT;

    public PageListWrapper getAllAudit(int pageNumber, AuditSearch search) throws AppException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAuditAll", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAuditAll", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public String getTableId(String callParam) throws AppException {
        String result = null;
        try {
            result = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getTableId", callParam);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }


    public String[] getTableDetail(String callParam) throws AppException {
        String[] result = null;
        try {
            String res = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getTableIdNameTitle", callParam);
            if (!Strings.isNullOrEmpty(res)) {
                result = res.split(Global.ARRAY_DELIMITER);
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public void auditInsert(String usrupd, String tablename, String keyvalue,
                            String fieldname, String oldval, String newval, String desc) throws AppException, SQLException {
        Map param = new HashMap();
        param.put("usrupd", usrupd);
        param.put("tablename", tablename);
        param.put("keyvalue", keyvalue);
        param.put("fieldname", fieldname);
        param.put("oldval", oldval);
        param.put("newval", newval);
        param.put("desc", desc);
        this.ibatisSqlMap.insert(this.STRING_SQL + "auditINSERT", param);
    }

    public void auditEdit(String usrupd, String tablename, String keyvalue,
                          String fieldname, String oldval, String newval, String desc) throws AppException, SQLException {
        Map param = new HashMap();
        param.put("usrupd", usrupd);
        param.put("tablename", tablename);
        param.put("keyvalue", keyvalue);
        param.put("fieldname", fieldname);
        param.put("oldval", oldval);
        param.put("newval", newval);
        param.put("desc", desc);
        this.ibatisSqlMap.insert(this.STRING_SQL + "auditEDIT", param);
    }

    public void auditDelete(String usrupd, String tablename, String keyvalue,
                            String fieldname, String oldval, String newval, String desc) throws AppException, SQLException {
        Map param = new HashMap();
        param.put("usrupd", usrupd);
        param.put("tablename", tablename);
        param.put("keyvalue", keyvalue);
        param.put("fieldname", fieldname);
        param.put("oldval", oldval);
        param.put("newval", newval);
        param.put("desc", desc);
        this.ibatisSqlMap.insert(this.STRING_SQL + "auditDELETE", param);
    }

    public void auditApprove(String usrupd, String tablename, String keyvalue,
                             String fieldname, String oldval, String newval, String desc) throws AppException, SQLException {
        Map param = new HashMap();
        param.put("usrupd", usrupd);
        param.put("tablename", tablename);
        param.put("keyvalue", keyvalue);
        param.put("fieldname", fieldname);
        param.put("oldval", oldval);
        param.put("newval", newval);
        param.put("desc", desc);
        this.ibatisSqlMap.insert(this.STRING_SQL + "auditAPPROVE", param);
    }

    public void auditReject(String usrupd, String tablename, String keyvalue,
                            String fieldname, String oldval, String newval, String desc) throws AppException, SQLException {
        Map param = new HashMap();
        param.put("usrupd", usrupd);
        param.put("tablename", tablename);
        param.put("keyvalue", keyvalue);
        param.put("fieldname", fieldname);
        param.put("oldval", oldval);
        param.put("newval", newval);
        param.put("desc", desc);
        this.ibatisSqlMap.insert(this.STRING_SQL + "auditREJECT", param);
    }

    public void auditCustom(String action, String usrupd, String tablename, String keyvalue,
                            String fieldname, String oldval, String newval, String desc) throws AppException, SQLException {
        Map param = new HashMap();
        param.put("action", action);
        param.put("usrupd", usrupd);
        param.put("tablename", tablename);
        param.put("keyvalue", keyvalue);
        param.put("fieldname", fieldname);
        param.put("oldval", oldval);
        param.put("newval", newval);
        param.put("desc", desc);
        this.ibatisSqlMap.insert(this.STRING_SQL + "auditCUSTOM", param);
    }
}
