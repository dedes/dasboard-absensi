package treemas.application.general.lookup.karyawan;

import treemas.application.general.lookup.approval.ApprovalLookUpForm;

public class LuKaryawanForm extends ApprovalLookUpForm {

	private String nik;
    private String nama;
    private String tempatLahir;
    private String tanggalLahir;
    private String jenisKelamin;
    private String agama;
    private String kewarganegaraan;
    private String kodePos;
    private String alamatKtp;
    private String noHP;
    private String imei;
    private String email;
    private String noRek;
    private String npwp;
    private String jenjangPendidikan;
    private String dtmUpd;
    
    private String tanggalBergabung;
    private String alamatSekarang;
    private String statusPerkawinan;
    private String golonganDarah;
    private String nomorKtp;
    private String emergencyContact;
    private String statusEmergency;
    private String alamatEmergency;
    private String telpEmergency;
    
    private String idProject;
    private String divisi;
    private String image;
   
	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTempatLahir() {
		return tempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}

	public String getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getAgama() {
		return agama;
	}

	public void setAgama(String agama) {
		this.agama = agama;
	}

	public String getKewarganegaraan() {
		return kewarganegaraan;
	}

	public void setKewarganegaraan(String kewarganegaraan) {
		this.kewarganegaraan = kewarganegaraan;
	}

	public String getKodePos() {
		return kodePos;
	}

	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}

	public String getNoHP() {
		return noHP;
	}

	public void setNoHP(String noHP) {
		this.noHP = noHP;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNoRek() {
		return noRek;
	}

	public void setNoRek(String noRek) {
		this.noRek = noRek;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getJenjangPendidikan() {
		return jenjangPendidikan;
	}

	public void setJenjangPendidikan(String jenjangPendidikan) {
		this.jenjangPendidikan = jenjangPendidikan;
	}

	public String getAlamatKtp() {
		return alamatKtp;
	}

	public void setAlamatKtp(String alamatKtp) {
		this.alamatKtp = alamatKtp;
	}

	public String getDtmUpd() {
		return dtmUpd;
	}

	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}

	public String getTanggalBergabung() {
		return tanggalBergabung;
	}

	public void setTanggalBergabung(String tanggalBergabung) {
		this.tanggalBergabung = tanggalBergabung;
	}

	public String getAlamatSekarang() {
		return alamatSekarang;
	}

	public void setAlamatSekarang(String alamatSekarang) {
		this.alamatSekarang = alamatSekarang;
	}

	public String getStatusPerkawinan() {
		return statusPerkawinan;
	}

	public void setStatusPerkawinan(String statusPerkawinan) {
		this.statusPerkawinan = statusPerkawinan;
	}

	public String getGolonganDarah() {
		return golonganDarah;
	}

	public void setGolonganDarah(String golonganDarah) {
		this.golonganDarah = golonganDarah;
	}

	public String getNomorKtp() {
		return nomorKtp;
	}

	public void setNomorKtp(String nomorKtp) {
		this.nomorKtp = nomorKtp;
	}

	public String getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(String emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public String getStatusEmergency() {
		return statusEmergency;
	}

	public void setStatusEmergency(String statusEmergency) {
		this.statusEmergency = statusEmergency;
	}

	public String getAlamatEmergency() {
		return alamatEmergency;
	}

	public void setAlamatEmergency(String alamatEmergency) {
		this.alamatEmergency = alamatEmergency;
	}

	public String getTelpEmergency() {
		return telpEmergency;
	}

	public void setTelpEmergency(String telpEmergency) {
		this.telpEmergency = telpEmergency;
	}

	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
 
	public String getDivisi() {
		return divisi;
	}

	public void setDivisi(String divisi) {
		this.divisi = divisi;
	}
	
}
