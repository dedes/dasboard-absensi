package treemas.application.general.lookup.karyawan;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.lookup.karyawan.LuKaryawanManager;
import treemas.base.setup.SetUpActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;

public class LuKaryawanHandler extends SetUpActionBase {

	LuKaryawanManager manager = new LuKaryawanManager();

	public ActionForward doAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws AppException {
		LuKaryawanForm frm = (LuKaryawanForm) form;
		String task = frm.getTask();

		if (Global.WEB_TASK_LOAD.equals(task)) {
			this.getDetailKaryawan(frm);
		}

		return this.getNextPage(task, mapping);

	}

	private void getDetailKaryawan(LuKaryawanForm form)
			throws AppException {
		LuKaryawanBean bean = this.manager.getDetailKaryawan(form.getNik());
		BeanConverter.convertBeanToString(bean, form);
	}

	public ActionForward handleException(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response, AppException ex) {
		Integer code = ex.getErrorCode();
		ActionMessages msgs = null;
		Object obj = ex.getUserObject();
		if (obj instanceof ActionMessages)
			msgs = (ActionMessages) obj;
		switch (code) {
		case ErrorCode.ERR_VALIDATION_PAGE_DATA:
			this.saveErrors(request, msgs);
			break;
		default:
			super.handleException(mapping, form, request, response, ex);
			break;
		}
		((LuKaryawanForm) form).setTask(((LuKaryawanForm) form).resolvePreviousTask());

		String task = ((LuKaryawanForm) form).getTask();
		return this.getErrorPage(task, mapping);
	}

}
