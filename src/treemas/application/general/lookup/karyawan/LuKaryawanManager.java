package treemas.application.general.lookup.karyawan;

import treemas.application.feature.master.reimburse.ReimburseSearch;
import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class LuKaryawanManager extends SetUpManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_LOOKUP_KARYAWAN;
    
    public LuKaryawanBean getDetailKaryawan(String nik) throws AppException {
    	LuKaryawanBean result = null;
		
		try {
			result = (LuKaryawanBean) this.ibatisSqlMap.queryForObject(STRING_SQL+"getDetailKaryawan", nik);
		} catch (SQLException e) {
			this.logger.printStackTrace(e);
			throw new AppException(ERROR_DB);
		}
		return result;
	}
}
