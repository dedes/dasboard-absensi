package treemas.application.general.lookup.user;

import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class UserManager extends SetUpManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_LOOKUP_USER;


    public PageListWrapper getAllPaging(int pageNumber, UserSearch search) throws AppException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllPaging", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "cntAllPaging", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public List getAll(int pageNumber, UserSearch search) throws AppException {
        try {
            return this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAll", search);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

}
