package treemas.application.general.lookup.user;

import java.io.Serializable;

public class UserBean implements Serializable {
    private String userid;
    private String fullname;
    private String branchid;
    private String branchname;
    private String job_desc;
    private String job_descName;
    private String fieldperson;


    public UserBean() {
    }


    public String getUserid() {
        return userid;
    }


    public void setUserid(String userid) {
        this.userid = userid;
    }


    public String getFullname() {
        return fullname;
    }


    public void setFullname(String fullname) {
        this.fullname = fullname;
    }


    public String getBranchid() {
        return branchid;
    }


    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }


    public String getBranchname() {
        return branchname;
    }


    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }


    public String getJob_desc() {
        return job_desc;
    }


    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }


    public String getJob_descName() {
        return job_descName;
    }


    public void setJob_descName(String job_descName) {
        this.job_descName = job_descName;
    }


    public String getFieldperson() {
        return fieldperson;
    }


    public void setFieldperson(String fieldperson) {
        this.fieldperson = fieldperson;
    }


}
