package treemas.application.general.lookup.user;

import treemas.base.search.SearchForm;

public class UserSearch extends SearchForm {

    private String userid;
    private String fullname;
    private String job_desc;
    private String fieldperson;
    private String groupid;
    private String appid;
    private String appname;

    public UserSearch() {
    }


    public String getUserid() {
        return userid;
    }


    public void setUserid(String userid) {
        this.userid = userid;
    }


    public String getFullname() {
        return fullname;
    }


    public void setFullname(String fullname) {
        this.fullname = fullname;
    }


    public String getJob_desc() {
        return job_desc;
    }


    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }

    public String getFieldperson() {
        return fieldperson;
    }

    public void setFieldperson(String fieldperson) {
        this.fieldperson = fieldperson;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

}
