package treemas.application.general.lookup.user;

import treemas.application.general.lookup.LookUpForm;
import treemas.util.format.TreemasFormatter;

public class UserForm extends LookUpForm {

    private String userid;
    private String fullname;
    private String branchid;
    private String branchname;
    private String job_desc;
    private String job_descName;
    private String fieldperson;
    private String groupid;
    private String appid;
    private String appname;

    private UserSearch search = new UserSearch();


    public UserForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public String getUserid() {
        return userid;
    }


    public void setUserid(String userid) {
        this.userid = TreemasFormatter.toCamelCase(userid);
    }


    public String getBranchid() {
        return branchid;
    }


    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }


    public String getFullname() {
        return fullname;
    }


    public void setFullname(String fullname) {
        this.fullname = TreemasFormatter.toCamelCase(fullname);
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getJob_desc() {
        return job_desc;
    }

    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }

    public String getJob_descName() {
        return job_descName;
    }

    public void setJob_descName(String job_descName) {
        this.job_descName = job_descName;
    }

    public String getFieldperson() {
        return fieldperson;
    }

    public void setFieldperson(String fieldperson) {
        this.fieldperson = fieldperson;
    }

    public UserSearch getSearch() {
        return search;
    }

    public void setSearch(UserSearch search) {
        this.search = search;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }


}
