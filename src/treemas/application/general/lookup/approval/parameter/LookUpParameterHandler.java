package treemas.application.general.lookup.approval.parameter;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.approval.parameter.ApprovalParameterBean;
import treemas.application.approval.parameter.ApprovalParameterForm;
import treemas.application.approval.parameter.ApprovalParameterManager;
import treemas.base.setup.SetUpActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LookUpParameterHandler extends SetUpActionBase {

    ApprovalParameterManager manager = new ApprovalParameterManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        LookUpParameterForm frm = (LookUpParameterForm) form;
        String task = frm.getTask();

        if (Global.WEB_TASK_LOAD.equals(task)) {
            this.getDetailApproval(frm);
        }

        return this.getNextPage(task, mapping);

    }

    private void getDetailApproval(LookUpParameterForm form)
            throws AppException {
        ApprovalParameterBean bean = this.manager.getDetailApproval(form.getId());
        BeanConverter.convertBeanToString(bean, form);
        form.setUserid(form.getUsrupd());
        form.setChangedate(form.getDtmupd());
        form.setActChange("EDIT");
        form.setTitle("DETAIL CHANGE GENERAL PARAMETER (" + form.getId() + ")");
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((ApprovalParameterForm) form).setTask(((ApprovalParameterForm) form).resolvePreviousTask());

        String task = ((ApprovalParameterForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
