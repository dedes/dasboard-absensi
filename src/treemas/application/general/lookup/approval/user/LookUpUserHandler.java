package treemas.application.general.lookup.approval.user;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.approval.user.ApprovalUserManager;
import treemas.base.setup.SetUpActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LookUpUserHandler extends SetUpActionBase {

    ApprovalUserManager manager = new ApprovalUserManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        LookUpUserForm frm = (LookUpUserForm) form;
        String task = frm.getTask();

        if (Global.WEB_TASK_LOAD.equals(task)) {
            this.getDetailApproval(frm);
        }

        return this.getNextPage(task, mapping);

    }

    private void getDetailApproval(LookUpUserForm form)
            throws AppException {
        LookUpUserBean bean = this.manager.getDetailApproval(form.getUserid());
        if (bean.getNew_sqlpassword() != null) {
            bean.setNew_sqlpassword("********");
        }
        if (bean.getOld_sqlpassword() != null) {
            bean.setOld_sqlpassword("********");
        }

        BeanConverter.convertBeanToString(bean, form);
        form.setChangedate(form.getDtmupd());
        form.setTitle("DETAIL CHANGE USER (" + form.getUserid() + ")");
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((LookUpUserForm) form).setTask(((LookUpUserForm) form).resolvePreviousTask());

        String task = ((LookUpUserForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
