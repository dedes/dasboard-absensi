package treemas.application.general.lookup.approval.user;

import treemas.application.general.lookup.approval.ApprovalLookUpForm;

public class LookUpUserForm extends ApprovalLookUpForm {

    private String userid;
    private String new_branchid;
    private String new_fullname;
    private String new_active;
    private String new_sqlpassword;
    private String new_email;
    private String new_bbpin;
    private String new_phone_no;
    private String new_handphone_no;
    private String new_job_desc;
    private String old_branchid;
    private String old_fullname;
    private String old_active;
    private String old_sqlpassword;
    private String old_email;
    private String old_bbpin;
    private String old_phone_no;
    private String old_handphone_no;
    private String old_job_desc;
    private String usrupd;
    private String dtmupd;
    private String usract;
    private String old_priviledge;
    private String new_priviledge;

    public String getOld_priviledge() {
        return old_priviledge;
    }

    public void setOld_priviledge(String old_priviledge) {
        this.old_priviledge = old_priviledge;
    }

    public String getNew_priviledge() {
        return new_priviledge;
    }

    public void setNew_priviledge(String new_priviledge) {
        this.new_priviledge = new_priviledge;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getNew_branchid() {
        return new_branchid;
    }

    public void setNew_branchid(String new_branchid) {
        this.new_branchid = new_branchid;
    }

    public String getNew_fullname() {
        return new_fullname;
    }

    public void setNew_fullname(String new_fullname) {
        this.new_fullname = new_fullname;
    }

    public String getNew_active() {
        return new_active;
    }

    public void setNew_active(String new_active) {
        this.new_active = new_active;
    }

    public String getNew_sqlpassword() {
        return new_sqlpassword;
    }

    public void setNew_sqlpassword(String new_sqlpassword) {
        this.new_sqlpassword = new_sqlpassword;
    }

    public String getNew_email() {
        return new_email;
    }

    public void setNew_email(String new_email) {
        this.new_email = new_email;
    }

    public String getNew_bbpin() {
        return new_bbpin;
    }

    public void setNew_bbpin(String new_bbpin) {
        this.new_bbpin = new_bbpin;
    }

    public String getNew_phone_no() {
        return new_phone_no;
    }

    public void setNew_phone_no(String new_phone_no) {
        this.new_phone_no = new_phone_no;
    }

    public String getNew_handphone_no() {
        return new_handphone_no;
    }

    public void setNew_handphone_no(String new_handphone_no) {
        this.new_handphone_no = new_handphone_no;
    }

    public String getNew_job_desc() {
        return new_job_desc;
    }

    public void setNew_job_desc(String new_job_desc) {
        this.new_job_desc = new_job_desc;
    }

    public String getOld_branchid() {
        return old_branchid;
    }

    public void setOld_branchid(String old_branchid) {
        this.old_branchid = old_branchid;
    }

    public String getOld_fullname() {
        return old_fullname;
    }

    public void setOld_fullname(String old_fullname) {
        this.old_fullname = old_fullname;
    }

    public String getOld_active() {
        return old_active;
    }

    public void setOld_active(String old_active) {
        this.old_active = old_active;
    }

    public String getOld_sqlpassword() {
        return old_sqlpassword;
    }

    public void setOld_sqlpassword(String old_sqlpassword) {
        this.old_sqlpassword = old_sqlpassword;
    }

    public String getOld_email() {
        return old_email;
    }

    public void setOld_email(String old_email) {
        this.old_email = old_email;
    }

    public String getOld_bbpin() {
        return old_bbpin;
    }

    public void setOld_bbpin(String old_bbpin) {
        this.old_bbpin = old_bbpin;
    }

    public String getOld_phone_no() {
        return old_phone_no;
    }

    public void setOld_phone_no(String old_phone_no) {
        this.old_phone_no = old_phone_no;
    }

    public String getOld_handphone_no() {
        return old_handphone_no;
    }

    public void setOld_handphone_no(String old_handphone_no) {
        this.old_handphone_no = old_handphone_no;
    }

    public String getOld_job_desc() {
        return old_job_desc;
    }

    public void setOld_job_desc(String old_job_desc) {
        this.old_job_desc = old_job_desc;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getUsract() {
        return usract;
    }

    public void setUsract(String usract) {
        this.usract = usract;
    }


}
