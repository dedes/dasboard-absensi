package treemas.application.general.lookup.approval;

import treemas.application.general.lookup.LookUpForm;

public class ApprovalLookUpForm extends LookUpForm {
    private String changedate;
    private String dataFrom;
    private String idChange;
    private String actChange;

    public String getChangedate() {
        return changedate;
    }

    public void setChangedate(String changedate) {
        this.changedate = changedate;
    }

    public String getDataFrom() {
        return dataFrom;
    }

    public void setDataFrom(String dataFrom) {
        this.dataFrom = dataFrom;
    }

    public String getIdChange() {
        return idChange;
    }

    public void setIdChange(String idChange) {
        this.idChange = idChange;
    }

    public String getActChange() {
        return actChange;
    }

    public void setActChange(String actChange) {
        this.actChange = actChange;
    }


}
