package treemas.application.general.lookup.approval.privilege;

import java.io.Serializable;
import java.util.Date;

public class LookUpPrivilegeBean implements Serializable {

    private String appid;
    private String appname;
    private String usrupd;
    private Date dtmupd;
    private String groupid;
    private String groupname;
    private String groupidold;
    private String groupnameold;
    private String menuold;
    private String menu;
    private String actChange;

    public LookUpPrivilegeBean() {
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public Date getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(Date dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getGroupidold() {
        return groupidold;
    }

    public void setGroupidold(String groupidold) {
        this.groupidold = groupidold;
    }

    public String getGroupnameold() {
        return groupnameold;
    }

    public void setGroupnameold(String groupnameold) {
        this.groupnameold = groupnameold;
    }

    public String getMenuold() {
        return menuold;
    }

    public void setMenuold(String menuold) {
        this.menuold = menuold;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getActChange() {
        return actChange;
    }

    public void setActChange(String actChange) {
        this.actChange = actChange;
    }

}
