package treemas.application.general.lookup.approval.privilege;

import treemas.application.general.lookup.approval.ApprovalLookUpForm;

public class LookUpPrivilegeForm extends ApprovalLookUpForm {

    private String appid;
    private String appname;
    private String usrupd;
    private String dtmupd;
    private String groupid;
    private String groupname;
    private String groupidold;
    private String groupnameold;
    private String menuold;
    private String menu;

    public LookUpPrivilegeForm() {
        this.setTitle("DETAIL APPROVAL PRIVILEGE");
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public String getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getGroupidold() {
        return groupidold;
    }

    public void setGroupidold(String groupidold) {
        this.groupidold = groupidold;
    }

    public String getGroupnameold() {
        return groupnameold;
    }

    public void setGroupnameold(String groupnameold) {
        this.groupnameold = groupnameold;
    }

    public String getMenuold() {
        return menuold;
    }

    public void setMenuold(String menuold) {
        this.menuold = menuold;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

}
