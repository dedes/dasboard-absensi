package treemas.application.general.lookup.approval.privilege;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.privilege.PrivilegeManager;
import treemas.base.setup.SetUpActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LookUpPrivilegeHandler extends SetUpActionBase {

    PrivilegeManager manager = new PrivilegeManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        LookUpPrivilegeForm frm = (LookUpPrivilegeForm) form;
        String task = frm.getTask();

        if (Global.WEB_TASK_LOAD.equals(task)) {
            this.getDetailApproval(frm);
        }

        return this.getNextPage(task, mapping);

    }

    private void getDetailApproval(LookUpPrivilegeForm form) throws AppException {
        LookUpPrivilegeBean bean = this.manager.getDetailApproval(form.getGroupid());
        BeanConverter.convertBeanToString(bean, form);
        form.setUserid(form.getUsrupd());
        form.setChangedate(form.getDtmupd());
        form.setTitle("DETAIL CHANGE PRIVILEGE (" + form.getGroupid() + ")");
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_UNKNOWN:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((LookUpPrivilegeForm) form).setTask(((LookUpPrivilegeForm) form).resolvePreviousTask());

        String task = ((LookUpPrivilegeForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
