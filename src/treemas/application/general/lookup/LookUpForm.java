package treemas.application.general.lookup;

import treemas.base.setup.SetUpFormBase;

public class LookUpForm extends SetUpFormBase {
    private String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


}
