package treemas.application.general.lookup.privilege;

import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class PrivilegeManager extends SetUpManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_LOOKUP_PRIVILEGE;


    public List getAllPrivilege(PrivilegeSearch search) throws AppException {
        List list = null;
        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllPrivilegeNotPaging", search);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return list;
    }

    public PageListWrapper getAllPrivilegePaging(int pageNumber, PrivilegeSearch search) throws AppException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllPrivilege", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllPrivilege", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

    public List getDetailPrivilege(String groupid) throws AppException {
        List list = null;

        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getDetailPrivilege", groupid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return list;
    }

    public PrivilegeBean getHeaderDetailPrivilege(String groupid) throws AppException {
        PrivilegeBean result = null;

        try {
            result = (PrivilegeBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getHeaderDetailPrivilege", groupid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

}
