package treemas.application.general.lookup.privilege;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.multiple.MultipleManager;
import treemas.base.setup.SetUpActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class PrivilegeHandler extends SetUpActionBase {

    PrivilegeManager manager = new PrivilegeManager();
    MultipleManager multipleManager = new MultipleManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        PrivilegeForm frm = (PrivilegeForm) form;
        String task = frm.getTask();
        int page = frm.getPaging().getCurrentPageNo();
        frm.getPaging().calculationPage();

        if (!Strings.isNullOrEmpty(frm.getGroupid())) {
            String[] arrayGroup = frm.getGroupid().split(Global.ARRAY_COMMA_DELIMITER);
            String[] newArrayGroup = new String[arrayGroup.length];
            for (int i = 0; i < arrayGroup.length; i++) {
                String string = arrayGroup[i];
                if (!Strings.isNullOrEmpty(string)) {
                    if (string.startsWith(Global.SPACE_DELIMITER)) {
                        string = string.replaceFirst(Global.SPACE_DELIMITER, "");
                    }
                }
                newArrayGroup[i] = string;
            }
            frm.getSearch().setMemberList(newArrayGroup);
        }

        if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)
                || Global.WEB_TASK_SHOW.equals(task)) {
            this.load(request, frm);
        } else if (Global.WEB_TASK_DETAIL.equals(task)) {
            this.loadDetail(request, frm);
        }

        return this.getNextPage(task, mapping);

    }

    private void loadAppAdmin(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getAppAdmin(param);
        request.setAttribute("listComboApp", list);
    }

    private void loadAppGroup(HttpServletRequest request) throws AppException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(Global.STRING_TRUE);
        param.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getAppGroup(param);
        request.setAttribute("listComboGroup", list);
    }

    private void loadPaging(HttpServletRequest request, PrivilegeForm form) throws AppException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;

        PageListWrapper result = this.manager.getAllPrivilegePaging(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);

        this.loadAppAdmin(request);
        this.loadAppGroup(request);
    }

    private void loadDetail(HttpServletRequest request, PrivilegeForm form) throws AppException {
        PrivilegeBean bean = this.manager.getHeaderDetailPrivilege(form.getGroupid());
        form.fromBean(bean);
        List list = this.manager.getDetailPrivilege(form.getGroupid());
        BeanConverter.convertBeanToString(list, form.getClass());
        request.setAttribute("list", list);
    }

    private void load(HttpServletRequest request, PrivilegeForm form) throws AppException {
        List list = this.manager.getAllPrivilege(form.getSearch());
        list = BeanConverter.convertBeansToString(list, form.getClass());
        List listReturn = new ArrayList();
        for (Object o : list) {
            PrivilegeForm singleForm = (PrivilegeForm) o;
            this.combineDetail(singleForm);
            listReturn.add(singleForm);
        }
//		System.out.println(listReturn.size());
//		listReturn = BeanConverter.convertBeansToString(listReturn, form.getClass());
        request.setAttribute("list", listReturn);
    }

    private void combineDetail(PrivilegeForm form) throws AppException {
        List list = this.manager.getDetailPrivilege(form.getGroupid());
        BeanConverter.convertBeanToString(list, form.getClass());
        form.setListPrivilege(list);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((PrivilegeForm) form).setTask(((PrivilegeForm) form).resolvePreviousTask());

        String task = ((PrivilegeForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
