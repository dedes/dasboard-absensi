package treemas.application.general.lookup.privilege;

import treemas.application.general.lookup.LookUpForm;
import treemas.util.format.TreemasFormatter;

import java.util.List;

public class PrivilegeForm extends LookUpForm {
    private String appid;
    private String appname;
    private String groupid;
    private String groupname;
    
    private String parentid;
    private String menuid;
    private String prompt;
    private String result;
    private String filelevel;
    private String seqorder;
    private String rootseq;
    private List<PrivilegeForm> listPrivilege;
    private PrivilegeSearch search = new PrivilegeSearch();

    public PrivilegeForm() {
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = TreemasFormatter.toCamelCase(groupname);
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = TreemasFormatter.toCamelCase(prompt);
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFilelevel() {
        return filelevel;
    }

    public void setFilelevel(String filelevel) {
        this.filelevel = filelevel;
    }

    public String getSeqorder() {
        return seqorder;
    }

    public void setSeqorder(String seqorder) {
        this.seqorder = seqorder;
    }

    public String getRootseq() {
        return rootseq;
    }

    public void setRootseq(String rootseq) {
        this.rootseq = rootseq;
    }

    public PrivilegeSearch getSearch() {
        return search;
    }

    public void setSearch(PrivilegeSearch search) {
        this.search = search;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = TreemasFormatter.toCamelCase(appname);
    }

    public List<PrivilegeForm> getListPrivilege() {
        return listPrivilege;
    }

    public void setListPrivilege(List<PrivilegeForm> listPrivilege) {
        this.listPrivilege = listPrivilege;
    }


}
