package treemas.application.general.lookup.region;

import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class RegionManager extends SetUpManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_LOOKUP_REGION;


    public PageListWrapper getAllRegion(int pageNumber, RegionSearch search) throws AppException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllRegion", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllRegion", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

}
