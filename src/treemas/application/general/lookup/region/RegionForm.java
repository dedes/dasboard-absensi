package treemas.application.general.lookup.region;

import treemas.application.general.lookup.LookUpForm;
import treemas.util.format.TreemasFormatter;

public class RegionForm extends LookUpForm {
    private String id;
    private String kelurahan;
    private String kecamatan;
    private String city;
    private String zipcode;
    private RegionSearch search = new RegionSearch();

    public RegionForm() {
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = TreemasFormatter.toCamelCase(kelurahan);
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = TreemasFormatter.toCamelCase(kecamatan);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = TreemasFormatter.toCamelCase(city);
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public RegionSearch getSearch() {
        return search;
    }

    public void setSearch(RegionSearch search) {
        this.search = search;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
