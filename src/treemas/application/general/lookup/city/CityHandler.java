package treemas.application.general.lookup.city;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.base.setup.SetUpActionBase;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class CityHandler extends SetUpActionBase {

    CityManager manager = new CityManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        CityForm frm = (CityForm) form;
        String task = frm.getTask();
        int page = frm.getPaging().getCurrentPageNo();
        frm.getPaging().calculationPage();

        if (Global.WEB_TASK_LOAD.equals(task)
                || Global.WEB_TASK_SEARCH.equals(task)
                || Global.WEB_TASK_SHOW.equals(task)) {
            this.load(request, frm);
        }

        return this.getNextPage(task, mapping);

    }

    private void load(HttpServletRequest request, CityForm form) throws AppException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;

        PageListWrapper result = this.manager.getAllCity(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((CityForm) form).setTask(((CityForm) form).resolvePreviousTask());

        String task = ((CityForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
