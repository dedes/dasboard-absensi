package treemas.application.general.lookup.city;

import treemas.util.ibatis.SearchFormParameter;

public class CitySearch extends SearchFormParameter {
    private String cityid;
    private String city;

    public CitySearch() {
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
