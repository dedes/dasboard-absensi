package treemas.application.general.lookup.city;

import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.constant.SQLConstant;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public class CityManager extends SetUpManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_LOOKUP_CITY;


    public PageListWrapper getAllCity(int pageNumber, CitySearch search) throws AppException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllCity", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllCity", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return result;
    }

}
