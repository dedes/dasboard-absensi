package treemas.application.general.lookup.city;

import treemas.application.general.lookup.LookUpForm;

public class CityForm extends LookUpForm {
    private String cityid;
    private String city;
    private CitySearch search = new CitySearch();

    public CityForm() {
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public CitySearch getSearch() {
        return search;
    }

    public void setSearch(CitySearch search) {
        this.search = search;
    }

}
