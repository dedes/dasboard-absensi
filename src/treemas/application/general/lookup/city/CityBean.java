package treemas.application.general.lookup.city;

import java.io.Serializable;

public class CityBean implements Serializable {
    private String cityid;
    private String city;

    public CityBean() {
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
