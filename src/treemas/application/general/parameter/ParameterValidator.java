package treemas.application.general.parameter;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class ParameterValidator extends Validator {

    public ParameterValidator(HttpServletRequest request) {
        super(request);
    }

    public ParameterValidator(Locale locale) {
        super(locale);
    }

    public boolean validateParameterInsert(ActionMessages messages, ParameterForm form, ParameterManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        try {
            ParameterBean bean = manager.getSingleParameter(form.getId());
            if (null != bean) {
                messages.add("id", new ActionMessage("errors.duplicate", "id"));
                throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }

        this.textValidator(messages, form.getId(), "general.id", "id", "1", this.getStringLength(10), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);

        this.textValidator(messages, form.getParamdesc(), "general.desc", "paramdesc", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);

        if (form.getData_type().equalsIgnoreCase("0")) {
            messages.add("data_type", new ActionMessage("common.harusPilih", "Data Type"));
        } else if (form.getData_type().equalsIgnoreCase("N")) {
            if (!form.getVal().matches("^[0-9]*$")) {
                messages.add("val", new ActionMessage("errors.integer2.bulat", "Value"));
            }
            if (form.getVal().isEmpty()) {
                messages.add("val", new ActionMessage("errors.required", "Value"));
            }
        } else if (form.getData_type().equalsIgnoreCase("C")) {
            this.textValidator(messages, form.getVal(), "general.nilai", "val", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);

        } else if (form.getData_type().equalsIgnoreCase("D")) {
            dateValidator(messages, form.getVal(), "general.nilai", "val", ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_DATE_DMY);
        } else if (form.getData_type().equalsIgnoreCase("T1")) {
            timeValidator(messages, form.getVal(), "general.nilai", "val", ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_TIME_HM);
        } else if (form.getData_type().equalsIgnoreCase("T2")) {
            timeValidator(messages, form.getVal(), "general.nilai", "val", ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_TIME_HMS);
        }
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }

    public boolean validateParameterEdit(ActionMessages messages, ParameterForm form, ParameterManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();
        try {
            ParameterBean bean = manager.getSingleParameter(form.getId());
            if (null != bean) {
                if (Global.STRING_FALSE.equals(bean.getIsApprove())) {
                    messages.add("id", new ActionMessage("errors.waiting.approval", "General Parameter"));
                    throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);
                }
            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }

        this.textValidator(messages, form.getId(), "general.id", "id", "1", this.getStringLength(10), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);

        this.textValidator(messages, form.getParamdesc(), "general.desc", "paramdesc", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);

        if (form.getData_type().equalsIgnoreCase("0")) {
            messages.add("data_type", new ActionMessage("common.harusPilih", "Data Type"));
        } else if (form.getData_type().equalsIgnoreCase("N")) {
            if (!form.getVal().matches("^[0-9]*$")) {
                messages.add("val", new ActionMessage("errors.integer2.bulat", "Value"));
            }
            if (form.getVal().isEmpty()) {
                messages.add("val", new ActionMessage("errors.required", "Value"));
            }
        } else if (form.getData_type().equalsIgnoreCase("C")) {
            this.textValidator(messages, form.getVal(), "general.nilai", "val", "1", this.getStringLength(50), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);

        } else if (form.getData_type().equalsIgnoreCase("D")) {
            dateValidator(messages, form.getVal(), "general.nilai", "val", ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_DATE_DMY);
        } else if (form.getData_type().equalsIgnoreCase("T1")) {
            timeValidator(messages, form.getVal(), "general.nilai", "val", ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_TIME_HM);
        } else if (form.getData_type().equalsIgnoreCase("T2")) {
            timeValidator(messages, form.getVal(), "general.nilai", "val", ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_TIME_HMS);
        }
        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
