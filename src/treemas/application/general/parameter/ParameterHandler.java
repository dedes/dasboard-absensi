package treemas.application.general.parameter;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.base.setup.SetUpActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ParameterHandler extends SetUpActionBase {

    ParameterManager manager = new ParameterManager();
    ParameterValidator validator;

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        this.validator = new ParameterValidator(request);
        ParameterForm frm = (ParameterForm) form;
        String task = frm.getTask();

        if (Global.WEB_TASK_LOAD.equals(task)) {
            this.load(request, frm);
        } else if (Global.WEB_TASK_EDIT.equals(task)) {
            ParameterBean bean = this.getSingleParameter(frm);
            frm.fromBean(bean);
            frm.getPaging().setDispatch(null);
        } else if (Global.WEB_TASK_UPDATE.equals(task)) {
            if (this.validator.validateParameterEdit(new ActionMessages(), frm, this.manager)) {
                this.updateParameter(request, frm);
            }
        } else if (Global.WEB_TASK_ADD.equals(task)) {
            this.clearForm(frm);
            frm.getPaging().setDispatch(null);
        } else if (Global.WEB_TASK_INSERT.equals(task)) {
            if (this.validator.validateParameterInsert(new ActionMessages(), frm, this.manager)) {
                this.insertParameter(request, frm);
//				this.load(request, frm);
            }
        }

        return this.getNextPage(task, mapping);

    }

    private void load(HttpServletRequest request, ParameterForm form)
            throws AppException {
        List list = this.manager.getAllVisibleParameter();
        request.setAttribute("list", BeanConverter.convertBeansFromString(list, form.getClass()));
    }

    private ParameterBean getSingleParameter(ParameterForm form)
            throws AppException {
        ParameterBean bean = this.manager.getSingleParameter(form.getId());
        return bean;
    }

    private void insertParameter(HttpServletRequest request, ParameterForm form)
            throws AppException {
        ParameterBean bean = new ParameterBean();
        form.toBean(bean);
        try {
            this.manager.insertParameter(this.getLoginId(request), bean);
            request.setAttribute("message", resources.getMessage("common.process.save"));
            load(request, form);
        } catch (CoreException e) {
            throw e;
        }
    }

    private void updateParameter(HttpServletRequest request, ParameterForm form)
            throws AppException {
        ParameterBean bean = new ParameterBean();
        form.toBean(bean);
        try {
            this.manager.updateParameter(this.getLoginId(request), bean);
            request.setAttribute("message", resources.getMessage("common.process.update"));
            load(request, form);
        } catch (CoreException e) {
            throw e;
        }
    }

    private void clearForm(ParameterForm form) throws AppException {
        form.setId("");
        form.setParamdesc("");
        form.setVal("");
        form.setIsvisible("1");
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((ParameterForm) form).setTask(((ParameterForm) form).resolvePreviousTask());

        String task = ((ParameterForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
