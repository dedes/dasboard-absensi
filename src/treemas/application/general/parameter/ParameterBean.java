package treemas.application.general.parameter;

import java.io.Serializable;
import java.util.Date;

public class ParameterBean implements Serializable {

    private String id;
    private String paramdesc;
    private String data_type;
    private String val;
    private String isvisible = "1";
    private String isApprove;
    private String usrupd;
    private Date dtmupd;
    private String usrapp;
    private Date dtmapp;

    public ParameterBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParamdesc() {
        return paramdesc;
    }

    public void setParamdesc(String paramdesc) {
        this.paramdesc = paramdesc;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getIsvisible() {
        return isvisible;
    }

    public void setIsvisible(String isvisible) {
        this.isvisible = isvisible;
    }

    public String getIsApprove() {
        return isApprove;
    }

    public void setIsApprove(String isApprove) {
        this.isApprove = isApprove;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public Date getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(Date dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getUsrapp() {
        return usrapp;
    }

    public void setUsrapp(String usrapp) {
        this.usrapp = usrapp;
    }

    public Date getDtmapp() {
        return dtmapp;
    }

    public void setDtmapp(Date dtmapp) {
        this.dtmapp = dtmapp;
    }

}
