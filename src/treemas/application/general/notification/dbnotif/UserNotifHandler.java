package treemas.application.general.notification.dbnotif;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.multiple.MultipleManager;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserNotifHandler extends AppAdminActionBase {

    UserNotifManager manager = new UserNotifManager();
    MultipleManager multipleManager = new MultipleManager();

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        UserNotifForm frm = (UserNotifForm) form;
        String task = frm.getTask();

        if (Global.WEB_TASK_LOAD.equals(task)) {
            this.loadUser(request);
        } else if (Global.WEB_TASK_INSERT.equals(task)) {
            //untuk add notif
            //MENUID, TABLENAME diisi otomatis jika approval
            //kalau bukan approval (add dari form) tidak perlu mengisi menuid, tablename
            //user id null jika message untuk semua user,
            //diisi 1 atau lebih message untuk 1 atau lebih user yang dituju
            //userapproval --> message creator
            //userid --> yang dituju
        }

        return this.getNextPage(task, mapping);

    }

    private void loadUser(HttpServletRequest request) throws CoreException {
        request.setAttribute("list", this.multipleManager.getAllActiveUser());
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_UNKNOWN:
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        ((UserNotifForm) form).setTask(((UserNotifForm) form).resolvePreviousTask());

        String task = ((UserNotifForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }

}
