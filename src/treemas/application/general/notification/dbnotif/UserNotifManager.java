package treemas.application.general.notification.dbnotif;

import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.List;

public class UserNotifManager extends AppAdminManagerBase {
    public static final String STRING_SQL = SQLConstant.SQL_NOTIFICATION;

    public List getUserNotifByUserId(String userid) throws CoreException {
        List list = null;
        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getNotifByUser", userid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return list;
    }

    public List getUserNotifByUserApprove(String userid) throws AppException {
        List list = null;
        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getNotifByApprover", userid);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return list;
    }

    public void saveNotif(UserNotifForm form) throws AppException {
        UserNotifBean bean = new UserNotifBean();
        form.toBean(bean);
        try {
            this.ibatisSqlMap.startTransaction();
            this.ibatisSqlMap.insert(this.STRING_SQL + "insert", bean);
            this.ibatisSqlMap.commitTransaction();
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }
    }

}
