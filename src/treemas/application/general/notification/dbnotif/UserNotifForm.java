package treemas.application.general.notification.dbnotif;

import treemas.base.appadmin.AppAdminFormBase;

public class UserNotifForm extends AppAdminFormBase {

    private String id;
    private String transactiondate;
    private String userid;
    private String userapprove;
    private String approvername;
    private String tablename;
    private String messageinfo;
    private String menuid;
    private String menuname;
    private String groupPrivilege;
    private String[] userlist = {};

    public UserNotifForm() {
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getTransactiondate() {
        return transactiondate;
    }


    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }


    public String getUserid() {
        return userid;
    }


    public void setUserid(String userid) {
        this.userid = userid;
    }


    public String getUserapprove() {
        return userapprove;
    }


    public void setUserapprove(String userapprove) {
        this.userapprove = userapprove;
    }


    public String getTablename() {
        return tablename;
    }


    public void setTablename(String tablename) {
        this.tablename = tablename;
    }


    public String getMessageinfo() {
        return messageinfo;
    }


    public void setMessageinfo(String messageinfo) {
        this.messageinfo = messageinfo;
    }


    public String getMenuid() {
        return menuid;
    }


    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getApprovername() {
        return approvername;
    }

    public void setApprovername(String approvername) {
        this.approvername = approvername;
    }

    public String[] getUserlist() {
        return userlist;
    }

    public void setUserlist(String[] userlist) {
        this.userlist = userlist;
    }

    public String getGroupPrivilege() {
        return groupPrivilege;
    }

    public void setGroupPrivilege(String groupPrivilege) {
        this.groupPrivilege = groupPrivilege;
    }

}
