package treemas.application.general.notification.dbnotif;

import treemas.base.ApplicationBean;

import java.io.Serializable;
import java.util.Date;

public class UserNotifBean extends ApplicationBean implements Serializable {
    private int id;
    private Date transactiondate;
    private String userid;
    private String userapprove;
    private String approvername;
    private String tablename;
    private String messageinfo;
    private String menuid;
    private String menuname;

    public UserNotifBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserapprove() {
        return userapprove;
    }

    public void setUserapprove(String userapprove) {
        this.userapprove = userapprove;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getMessageinfo() {
        return messageinfo;
    }

    public void setMessageinfo(String messageinfo) {
        this.messageinfo = messageinfo;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getApprovername() {
        return approvername;
    }

    public void setApprovername(String approvername) {
        this.approvername = approvername;
    }


}
