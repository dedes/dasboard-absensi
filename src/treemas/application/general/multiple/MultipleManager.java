package treemas.application.general.multiple;

import treemas.base.setup.SetUpManagerBase;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.struts.MultipleParameterBean;

import java.sql.SQLException;
import java.util.List;

public class MultipleManager extends SetUpManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_MULTIPLE;

    public List getAuditComboReport(String active) throws CoreException {
        MultipleParameterBean param = new MultipleParameterBean();
        param.setActive(active);
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAuditCombo", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public List getAuditCombo(MultipleParameterBean param) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAuditCombo", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public List getListJobDesc(MultipleParameterBean param) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getJobDescCombo", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public List getListBranch(MultipleParameterBean param) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getBranchCombo", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public List getAppGroup(MultipleParameterBean param) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAppGroup", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public List getAppAdmin(MultipleParameterBean param) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAppAdmin", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public List getAllActiveUser() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAllActiveUser");
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public List getListAppId() throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getAppIdCombo");
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public List getPrompt(MultipleParameterBean param) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getPrompt", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public List getFormid(MultipleParameterBean param) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getFormid", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }
    
    
    public List getListLeader(MultipleParameterBean param) throws CoreException {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(STRING_SQL + "getLeader", param);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }
}
