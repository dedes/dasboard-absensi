package treemas.application.general.privilege.feature;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;
import java.util.Date;

public class FeatureMenuBean implements Serializable, Auditable {

    private static final String[] AUDIT_COLUMNS = new String[]{
            "groupid", "featureid", "formid", "appid", "usrupd", "dtmupd", "active"
    };
    private static final String[] AUDIT_DB_COLUMNS = new String[]{
            "GROUPID", "FEATUREID", "FORMID", "APPID", "USRUPD", "DTMUPD", "ACTIVE"
    };
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_SYS_APPGROUPFEATURE;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{
            "GROUPID", "FEATUREID", "FORMID", "APPID"
    };
    private String groupid;
    private String featureid;
    private String formid;
    private String appid;
    private String usrupd;
    private Date dtmupd;
    private String active;

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getFeatureid() {
        return featureid;
    }

    public void setFeatureid(String featureid) {
        this.featureid = featureid;
    }

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public Date getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(Date dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

}
