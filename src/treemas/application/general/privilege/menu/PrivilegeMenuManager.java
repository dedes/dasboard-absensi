package treemas.application.general.privilege.menu;

import treemas.base.setup.SetUpManagerBase;
import treemas.util.AppException;
import treemas.util.constant.SQLConstant;

import java.sql.SQLException;
import java.util.List;

public class PrivilegeMenuManager extends SetUpManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_PRIVILEGE;

    public List menuTracert(String applicationId) throws AppException {
        List list = null;

        try {
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "menutracert", applicationId);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new AppException(ERROR_DB);
        }

        return list;
    }


}
