package treemas.application.general.privilege.menu;

import treemas.util.auditrail.Auditable;
import treemas.util.constant.Global;

import java.io.Serializable;
import java.util.Date;

public class PrivilegeMenuBean implements Auditable, Serializable {

    private static final String[] AUDIT_COLUMNS = new String[]{"groupid", "appid", "menuid"};
    private static final String[] AUDIT_DB_COLUMNS = new String[]{"GROUPID", "APPID", "MENUID"};
    private static final String AUDIT_TABLE_NAME = Global.TABLE_NAME_SYS_APPGROUPMENU;
    private static final String[] AUDIT_PRIMARY_KEYS = new String[]{"groupid", "appid", "menuid"};

    private String groupid;
    private String groupname;
    private String appid;
    private String appname;
    private String menuid;
    private String usrupd;
    private Date dtmupd;
    private String updatemenu;

    private String prompt;
    private String formid;
    private String formfilename;
    private String result;
    private Integer filelevel;
    private String parentid;
    private String chain;

    public PrivilegeMenuBean() {
        prompt = "";
        menuid = "";
        formid = "";
        result = "";
        filelevel = 0;
        parentid = "";
        appid = "";
        appname = "";
        groupid = "";
        updatemenu = "";
        usrupd = "";
    }

    public void setMenu(String groupid, String appid, String usrupd) {
        this.groupid = groupid;
        this.appid = appid;
        this.usrupd = usrupd;
    }

    public void setMenu(String groupid, String appid, String usrupd, String updatemenu) {
        this.groupid = groupid;
        this.appid = appid;
        this.usrupd = usrupd;
        this.updatemenu = updatemenu;
    }

    public String getAuditTableName() {
        return AUDIT_TABLE_NAME;
    }

    public String[] getAuditColumns() {
        return AUDIT_COLUMNS;
    }

    public String[] getAuditPrimaryKeys() {
        return AUDIT_PRIMARY_KEYS;
    }

    public String[] getAuditDBColumns() {
        return AUDIT_DB_COLUMNS;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getUsrupd() {
        return usrupd;
    }

    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }

    public Date getDtmupd() {
        return dtmupd;
    }

    public void setDtmupd(Date dtmupd) {
        this.dtmupd = dtmupd;
    }

    public String getUpdatemenu() {
        return updatemenu;
    }

    public void setUpdatemenu(String updatemenu) {
        this.updatemenu = updatemenu;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid;
    }

    public String getFormfilename() {
        return formfilename;
    }

    public void setFormfilename(String formfilename) {
        this.formfilename = formfilename;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getFilelevel() {
        return filelevel;
    }

    public void setFilelevel(Integer filelevel) {
        this.filelevel = filelevel;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getChain() {
        return chain;
    }

    public void setChain(String chain) {
        this.chain = chain;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(this.getClass().getName() + "[");
        sb.append("prompt=" + this.prompt + ";");
        sb.append("menudId=" + this.menuid + ";");
        sb.append("formId=" + this.formid + ";");
        sb.append("result=" + this.result + ";");
        sb.append("filelevel=" + this.filelevel + ";");
        sb.append("parentid=" + this.parentid + ";");
        sb.append("appid=" + this.appid + ";");
        sb.append("appname=" + this.appname + ";");
        sb.append("groupid=" + this.groupid + ";");
        sb.append("updatemenu=" + this.updatemenu + ";");
        sb.append("userupd=" + this.usrupd + ";");
        sb.append("hashCode=" + System.identityHashCode(this) + "]");
        return sb.toString();
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

}
