package treemas.application.general.privilege;

public class MenuIDBean {
    private String menuid;
    private String formid;

    public MenuIDBean() {
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid;
    }


}
