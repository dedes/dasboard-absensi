package treemas.application.general.sync;

import com.ibatis.sqlmap.client.SqlMapClient;
import treemas.util.EODException;
import treemas.util.ibatis.EODIbatisHelper;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;
import treemas.util.thread.Animator;

import java.sql.SQLException;

public class SyncDataExecutor {
    private static final String STRING_SQL = "";
    protected SqlMapClient ibatisSqlMap = EODIbatisHelper.getSqlMap();
    private ILogger logger = LogManager.getDefaultLogger();

    public void updOptionAnswers() throws EODException {
        Animator thr = new Animator();
        try {
            try {
                this.logger.log(ILogger.LEVEL_INFO, "Updating options answer...");
                ibatisSqlMap.startTransaction();
                thr.start();

                this.ibatisSqlMap.update(this.STRING_SQL + "callSP_syncOptions", null);

                ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Updating options answer...finished!");
            } finally {
                this.ibatisSqlMap.endTransaction();
                thr.stopAnimate();
            }
        } catch (SQLException sqle) {
            throw new EODException(sqle, EODException.DB_ERROR);
        } catch (Exception e) {
            throw new EODException(e, EODException.UNKNOWN);
        }
    }

    public void updBranches() throws EODException {
        Animator thr = new Animator();
        try {
            try {
                this.logger.log(ILogger.LEVEL_INFO, "Synchronizing branch master...");
                ibatisSqlMap.startTransaction();
                thr.start();

                this.ibatisSqlMap.update(this.STRING_SQL + "callSP_syncBranch", null);

                ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Synchronizing branch master...finished!");
            } finally {
                this.ibatisSqlMap.endTransaction();
                thr.stopAnimate();
            }
        } catch (SQLException sqle) {
            throw new EODException(sqle, EODException.DB_ERROR);
        } catch (Exception e) {
            throw new EODException(e, EODException.UNKNOWN);
        }
    }

    public void updUser() throws EODException {
        Animator thr = new Animator();
        try {
            try {
                this.logger.log(ILogger.LEVEL_INFO, "Synchronizing user master...");
                ibatisSqlMap.startTransaction();
                thr.start();

                this.ibatisSqlMap.update(this.STRING_SQL + "callSP_syncUser", null);

                ibatisSqlMap.commitTransaction();
                this.logger.log(ILogger.LEVEL_INFO, "Synchronizing user master...finished!");
            } finally {
                this.ibatisSqlMap.endTransaction();
                thr.stopAnimate();
            }
        } catch (SQLException sqle) {
            throw new EODException(sqle, EODException.DB_ERROR);
        } catch (Exception e) {
            throw new EODException(e, EODException.UNKNOWN);
        }
    }
}
