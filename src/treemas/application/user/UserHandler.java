package treemas.application.user;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;
import treemas.application.general.multiple.MultipleManager;
import treemas.application.general.parameter.ParameterManager;
import treemas.application.privilege.PrivilegeManager;
import treemas.application.privilege.member.PrivilegeMemberBean;
import treemas.base.appadmin.AppAdminActionBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.paging.PageListWrapper;
import treemas.util.struts.MultipleParameterBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class UserHandler extends AppAdminActionBase {

    MultipleManager multipleManager = new MultipleManager();
    ParameterManager paramManager = new ParameterManager();
    UserManager manager = new UserManager();
    PrivilegeManager privilegeManager = new PrivilegeManager();
    UserValidator validator;

    public UserHandler() {
        pageMap.put(Global.WEB_TASK_RELEASE, "view");
        pageMap.put(Global.WEB_TASK_RELEASE_LOCK, "view");
        pageMap.put(Global.WEB_TASK_ENABLE, "view");
        pageMap.put(Global.WEB_TASK_DISABLE, "view");
        pageMap.put(Global.WEB_TASK_RESET, "view");

        exceptionMap.put(Global.WEB_TASK_RELEASE, "view");
        exceptionMap.put(Global.WEB_TASK_RELEASE_LOCK, "view");
        exceptionMap.put(Global.WEB_TASK_ENABLE, "view");
        exceptionMap.put(Global.WEB_TASK_DISABLE, "view");
        exceptionMap.put(Global.WEB_TASK_RESET, "view");
    }

    public ActionForward doAction(ActionMapping mapping, ActionForm form,
                                  HttpServletRequest request, HttpServletResponse response)
            throws AppException {
        this.validator = new UserValidator(request);
        UserForm frm = (UserForm) form;
        frm.setTitle(Global.TITLE_APP_USER);
        frm.getPaging().calculationPage();
        String task = frm.getTask();
        frm.getSearch().setUserMaker(this.getLoginId(request));
        if (Global.WEB_TASK_SHOW.equals(task) || Global.WEB_TASK_LOAD.equals(task) || Global.WEB_TASK_SEARCH.equals(task)) {
            this.load(request, frm);
        } else if (Global.WEB_TASK_ADD.equals(task)) {
            String isUseBB = this.paramManager.getValueParameter(Global.GENERAL_PARAM_USE_BB);
            String isUseMobile = this.paramManager.getValueParameter(Global.GENERAL_PARAM_USE_MOBILE);
            frm.setTitle(Global.TITLE_APP_USER);
            this.loadBranchDropDown(request);
            this.loadJobDescDropDown(request);
            this.clearField(frm);
            frm.setIsUseBB(isUseBB);
            frm.setIsUseMobile(isUseMobile);
        } else if (Global.WEB_TASK_EDIT.equals(task)) {
            String isUseBB = this.paramManager.getValueParameter(Global.GENERAL_PARAM_USE_BB);
            String isUseMobile = this.paramManager.getValueParameter(Global.GENERAL_PARAM_USE_MOBILE);
            frm.setTitle(Global.TITLE_APP_USER);
            this.loadBranchDropDown(request);
            this.loadJobDescDropDown(request);
            this.loadSingle(frm);
            frm.setIsUseBB(isUseBB);
            frm.setIsUseMobile(isUseMobile);
        } else if (Global.WEB_TASK_INSERT.equals(task)) {
            if (this.validator.validateUserAdd(new ActionMessages(), frm, this.manager)) {
                this.insertUser(request, frm);
            }
            this.load(request, frm);
        } else if (Global.WEB_TASK_UPDATE.equals(task)) {
            if (this.validator.validateUserEdit(new ActionMessages(), frm, this.manager)) {
                this.updateUser(request, frm);
            }
            this.load(request, frm);
        } else if (Global.WEB_TASK_RESET.equals(task)) {
            this.resetPassword(request, frm);
            this.load(request, frm);
        } else if (Global.WEB_TASK_RELEASE.equals(task)) {
            this.releaseLogin(request, frm);
            this.load(request, frm);
        } else if (Global.WEB_TASK_RELEASE_LOCK.equals(task)) {
            this.releaseLock(request, frm);
            this.load(request, frm);
        } else if (Global.WEB_TASK_ENABLE.equals(task)) {
            this.enable(request, frm);
            this.load(request, frm);
        } else if (Global.WEB_TASK_DISABLE.equals(task)) {
            this.disable(request, frm);
            this.load(request, frm);
        } else if (Global.WEB_TASK_DELETE.equals(task)) {
            this.delete(request, frm);
            this.load(request, frm);
        } else if (Global.WEB_TASK_SYNC.equals(task)) {
            this.syncUser();
            this.load(request, frm);
        }

        return this.getNextPage(task, mapping);

    }

    private void clearField(UserForm frm) {
        frm.setUserid("");
        frm.setFullname("");
        frm.setSqlpassword("");
        frm.setEmail("");
        frm.setPhone_no("");
        frm.setHandphone_no("");
        frm.setActive("1");
    }

    private String[] addToArray(String privileges) {
        String[] tempmemberlist = privileges.split(Global.ARRAY_COMMA_DELIMITER);
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < tempmemberlist.length; i++) {
            String string = tempmemberlist[i];
            if (!Strings.isNullOrEmpty(string)) {
                list.add(string);
            }
        }

        if (0 >= list.size()) {
            return null;
        }

        String[] result = new String[list.size()];
        list.toArray(result);
        return result;
    }

    private UserBean convertToBean(UserForm form, String updateUser, int flag) throws AppException {
        String isUseBB = this.paramManager.getValueParameter(Global.GENERAL_PARAM_USE_BB);
        String isUseMobile = this.paramManager.getValueParameter(Global.GENERAL_PARAM_USE_MOBILE);
        form.setIsUseBB(isUseBB);
        form.setIsUseMobile(isUseMobile);
        UserBean bean = form.toBean();
        if (0 == flag)
            bean.setUsrupd(updateUser);
        else if (1 == flag)
            bean.setUsercrt(updateUser);
        else
            bean.setUsrupd(updateUser);
        return bean;
    }

    private void deleteUserPrivilege(HttpServletRequest request, UserForm form, int flag) throws AppException {
        this.privilegeManager.deleteUserPrivilege(form.getUserid(), flag, this.getLoginId(request));
    }

    private void saveUserPrivilege(HttpServletRequest request, UserForm form) throws AppException {
        String privileges = form.getGroupPrivilege();
        if (!Strings.isNullOrEmpty(privileges)) {
            String[] tempmemberlist = this.addToArray(privileges);
            if (tempmemberlist.length > 0) {
                String usrupd = this.getLoginId(request);
                String userid = form.getUserid();

                for (int i = 0; i < tempmemberlist.length; i++) {
                    PrivilegeMemberBean memberBean = new PrivilegeMemberBean(tempmemberlist[i].trim(), userid, usrupd);
                    this.privilegeManager.insertUserPrivilege(memberBean, form.getTask());
                }
            }
        }

    }

    private void saveUserPrivilegeMemberNew(String userid) throws AppException {
        this.privilegeManager.insertUserPrivilegeMemberNew(userid);
    }


    private void insertUser(HttpServletRequest request, UserForm form) throws AppException {
        UserBean bean = this.convertToBean(form, this.getLoginId(request), 1);
        this.manager.insert(bean);
        this.deleteUserPrivilege(request, form, 0);
        this.saveUserPrivilege(request, form);
    }


    private void updateUser(HttpServletRequest request, UserForm form) throws AppException {
        UserBean bean = this.convertToBean(form, this.getLoginId(request), 0);
        bean.setUsrupd(this.getLoginId(request));
        this.manager.update(bean);
        this.saveUserPrivilegeMemberNew(bean.getUserid());
        this.deleteUserPrivilege(request, form, 0);
        this.saveUserPrivilege(request, form);

    }


    private void delete(HttpServletRequest request, UserForm form) throws AppException {
        UserBean bean = this.convertToBean(form, this.getLoginId(request), -1);
//        this.manager.deleteApproval(bean);
        this.saveUserPrivilegeMemberNew(bean.getUserid());
        this.deleteUserPrivilege(request, form, 1);
        this.manager.delete(bean);
    }


    private void loadJobDescDropDown(HttpServletRequest request)
            throws CoreException {
        MultipleParameterBean bean = new MultipleParameterBean();
        bean.setActive(Global.STRING_TRUE);
        bean.setValueUseKey(Global.STRING_FALSE);
        List list = this.multipleManager.getListJobDesc(bean);
        request.setAttribute("listJobDesc", list);
    }

    private void loadBranchDropDown(HttpServletRequest request) throws CoreException {
        MultipleParameterBean bean = new MultipleParameterBean();
        bean.setActive(Global.STRING_TRUE);
        bean.setValueUseKey(Global.STRING_FALSE);
        bean.setUser(this.getLoginId(request));
        List list = this.multipleManager.getListBranch(bean);
        request.setAttribute("listBranch", list);
    }

    private void load(HttpServletRequest request, UserForm form) throws CoreException {
        int pageNumber = form.getPaging().getTargetPageNo();
        if (Global.WEB_TASK_SEARCH.equals(form.getTask()) || Global.WEB_TASK_SHOW.equals(form.getTask()))
            pageNumber = 1;
        PageListWrapper result = this.manager.getAllUser(pageNumber, form.getSearch());
        List list = result.getResultList();
        list = BeanConverter.convertBeansToString(list, form.getClass());
        form.getPaging().setCurrentPageNo(pageNumber);
        form.getPaging().setTotalRecord(result.getTotalRecord());
        request.setAttribute("list", list);
        request.setAttribute("loginid", this.getLoginId(request));
        request.setAttribute("jobDesc", this.getLoginBean(request).getJobDesc());

        this.loadJobDescDropDown(request);
        this.loadBranchDropDown(request);

    }

    private void syncUser() throws CoreException {
        this.manager.synchronizeUser();
    }

    private void loadSingle(UserForm form) throws CoreException {
        UserBean bean = new UserBean();
        bean = this.manager.getSingleUser(form.getUserid());
        BeanConverter.convertBeanToString(bean, form);

    }

    private void resetPassword(HttpServletRequest request, UserForm form) throws CoreException {
        this.manager.resetUserPassword(form.getUserid(), this.getLoginId(request));
    }

    private void releaseLock(HttpServletRequest request, UserForm form) throws CoreException {
        this.manager.releaseLock(form.getUserid(), this.getLoginId(request));
    }

    private void releaseLogin(HttpServletRequest request, UserForm form) throws CoreException {
        this.manager.releaseLogin(form.getUserid(), this.getLoginId(request));
    }

    private void enable(HttpServletRequest request, UserForm form) throws CoreException {
        try {
            this.manager.activeDeactive(form.getUserid(), this.getLoginId(request), "1");
            this.saveUserPrivilegeMemberNew(form.getUserid());
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    private void disable(HttpServletRequest request, UserForm form) throws CoreException {
        try {
            this.manager.activeDeactive(form.getUserid(), this.getLoginId(request), "0");
            this.saveUserPrivilegeMemberNew(form.getUserid());
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public ActionForward handleException(ActionMapping mapping,
                                         ActionForm form, HttpServletRequest request,
                                         HttpServletResponse response, AppException ex) {
        Integer code = ex.getErrorCode();
        ActionMessages msgs = null;
        Object obj = ex.getUserObject();
        if (obj instanceof ActionMessages)
            msgs = (ActionMessages) obj;
        switch (code) {
            case ErrorCode.ERR_VALIDATION_PAGE_DATA:
                UserForm frm = (UserForm) form;
                try {
                    this.loadJobDescDropDown(request);
                    this.load(request, frm);
                } catch (AppException e) {
//				this.logger.printStackTrace(e);
                }
                this.saveErrors(request, msgs);
                break;
            default:
                super.handleException(mapping, form, request, response, ex);
                break;
        }
        String isUseBB = "0";
        String isUseMobile = "0";
        try {
            isUseBB = this.paramManager.getValueParameter(Global.GENERAL_PARAM_USE_BB);
            isUseMobile = this.paramManager.getValueParameter(Global.GENERAL_PARAM_USE_MOBILE);
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
        ((UserForm) form).setTask(((UserForm) form).resolvePreviousTask());
        ((UserForm) form).setIsUseBB(isUseBB);
        ((UserForm) form).setIsUseMobile(isUseMobile);
        String task = ((UserForm) form).getTask();
        return this.getErrorPage(task, mapping);
    }
}
