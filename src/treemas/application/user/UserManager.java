package treemas.application.user;

import com.google.common.base.Strings;
import treemas.application.audit.AuditManager;
import treemas.application.changepassword.ChangePasswordManager;
import treemas.base.appadmin.AppAdminManagerBase;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.auditrail.AuditTrailException;
import treemas.util.constant.Global;
import treemas.util.constant.GlobalTable;
import treemas.util.constant.SQLConstant;
import treemas.util.databases.SQLErrorCode;
import treemas.util.log.ILogger;
import treemas.util.openfire.user.OpenfireUserGroupManager;
import treemas.util.paging.PageListWrapper;
import treemas.util.properties.PropertiesKey;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserManager extends AppAdminManagerBase {
    private final String STRING_SQL = SQLConstant.SQL_USER;
    private ChangePasswordManager cpManager = new ChangePasswordManager();
    private AuditManager customAuditManager = new AuditManager();

    public PageListWrapper getAllUser(int pageNumber, UserSearch search) throws CoreException {
        PageListWrapper result = new PageListWrapper();
        search.setPage(pageNumber);

        try {
            List list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAllUser", search);
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "countAllUser", search);
            result.setResultList(list);
            result.setTotalRecord(count.intValue());
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public UserBean getSingleUser(String userId) throws CoreException {
        UserBean result = null;
        try {
            result = (UserBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleUser", userId);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }

        return result;
    }

    public void insert(UserBean bean) throws CoreException {
        Map paramMap = new HashMap();
        paramMap.put("userId", bean.getUserid());
        paramMap.put("userUpdate", bean.getUsercrt());
        paramMap.put("action", "ADD");
        bean.setBranchid("000");
        bean.setActive("1");
        bean.setJob_desc("US");
        bean.setIsapproved("1");
        try {
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.insert(this.STRING_SQL + "insert", bean);
                /*this.ibatisSqlMap.insert(this.STRING_SQL + "insertUserApproval", paramMap);*/
                this.auditManager.auditAdd(this.ibatisSqlMap.getCurrentConnection(), bean, bean.getUsercrt(), Global.ACTION_ADD_NEW_USER);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        } catch (AuditTrailException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(this.auditManager.ERROR_DB);
        }
    }

    public void update(UserBean bean) throws CoreException {
        Map paramMap = new HashMap();
        paramMap.put("userId", bean.getUserid());
        paramMap.put("userUpdate", bean.getUsrupd());
        paramMap.put("action", "EDIT");
        try {
           /* this.ibatisSqlMap.insert(this.STRING_SQL + "insertUserApproval", paramMap);*/
            this.ibatisSqlMap.update(this.STRING_SQL + "update", bean);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }
    }

    public void updatePriviledgeApproval(String userId) throws CoreException {
        try {
            this.ibatisSqlMap.update(this.STRING_SQL + "updateUserPriviledge", userId);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }
    }

    public void synchronizeUser() throws CoreException {
        try {
            this.ibatisSqlMap.insert(this.STRING_SQL + "synchronize");
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_WEB_SERVICE);
        }
    }

    public void resetUserPassword(String userId, String userUpdate) throws CoreException {
        this.cpManager.resetPassword(userId, userUpdate);
    }

    public void releaseLogin(String userId, String userUpdate) throws CoreException {
        try {
            this.release(userId, userUpdate, Global.RELEASE_LOGIN);
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
    }

    public void releaseLock(String userId, String userUpdate) throws CoreException {
        try {
            this.release(userId, userUpdate, Global.RELEASE_LOCKED);
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
    }

    private void release(String userId, String userUpdate, String type) throws CoreException {
        try {
            Map paramMap = new HashMap();
            paramMap.put("userId", userId);
            paramMap.put("userUpdate", userUpdate);
            paramMap.put("type", type);
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.update(this.STRING_SQL + "release", paramMap);
                if (Global.ACTION_UNLOCK.equals(type)) {
                    this.customAuditManager.auditCustom(Global.ACTION_UNLOCK, userUpdate, GlobalTable.TABLE_NAME_USER, userId, Global.FIELD_ACTIVE, Global.STRING_TRUE, Global.STRING_FALSE, "UNLOCK");
                } else {
                    this.customAuditManager.auditCustom(Global.ACTION_RELEASE, userUpdate, GlobalTable.TABLE_NAME_USER, userId, Global.FIELD_ACTIVE, Global.STRING_TRUE, Global.STRING_FALSE, "RELEASE LOGIN");
                }
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
    }

    public void activeDeactive(String userId, String userUpdate, String type) throws CoreException {
        try {
            Map paramMap = new HashMap();
            paramMap.put("userId", userId);
            paramMap.put("userUpdate", userUpdate);
            paramMap.put("type", type);
            String oldVal = Global.STRING_FALSE;
            String message = "USER";
            String action = null;
            if (Strings.isNullOrEmpty(type)) {
                type = Global.STRING_TRUE;
                oldVal = Global.STRING_FALSE;
                action = Global.ACTION_ACTIVATE;
            } else {
                if (Global.STRING_TRUE.equals(type)) {
                    oldVal = Global.STRING_FALSE;
                    action = Global.ACTION_ACTIVATE;
                } else {
                    oldVal = Global.STRING_TRUE;
                    action = Global.ACTION_DEACTIVATE;
                }
            }
            paramMap.put("action", action);
            try {
                this.ibatisSqlMap.startTransaction();
                this.ibatisSqlMap.insert(this.STRING_SQL + "insertUserApproval", paramMap);
                this.ibatisSqlMap.update(this.STRING_SQL + "activedeactive", paramMap);
                this.customAuditManager.auditCustom(action, userUpdate, GlobalTable.TABLE_NAME_USER, userId, Global.FIELD_ACTIVE, oldVal, type, action + " " + message);
                this.ibatisSqlMap.commitTransaction();
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }
    }

    public void delete(UserBean beanUser) throws CoreException {
        String userId = beanUser.getUserid();
        String isUseBB = beanUser.getIsUseBB();
        String isUseMobile = beanUser.getIsUseMobile();
        try {
            this.ibatisSqlMap.startTransaction();
            UserBean deletedBean = this.getSingleUser(userId);
            deletedBean.replicationSingleParam(deletedBean, beanUser);
            deletedBean.setIsUseBB(isUseBB);
            deletedBean.setIsUseMobile(isUseMobile);

            this.auditManager.auditDelete(this.ibatisSqlMap.getCurrentConnection(), deletedBean, beanUser.getUsrupd(), Global.ACTION_DELETE);

            this.ibatisSqlMap.delete(this.STRING_SQL + "deletePasswordHistory", userId);
            this.ibatisSqlMap.delete(this.STRING_SQL + "deleteSysUserBranch", userId);
            this.ibatisSqlMap.delete(this.STRING_SQL + "delete", userId);

            if (isUsingOpenfire()) {
                OpenfireUserGroupManager of = new OpenfireUserGroupManager();
                String ofUsername = deletedBean.getUserid();
            }

            this.ibatisSqlMap.commitTransaction();
            this.logger.log(ILogger.LEVEL_INFO, "Deleted user " + userId);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == SQLErrorCode.ERROR_CHILD_FOUND) {
                throw new CoreException("", sqle, ERROR_HAS_CHILD, userId);
            } else {
                throw new CoreException(sqle.toString(), sqle, ERROR_DB, null);
            }
        } catch (CoreException aie) {
            throw aie;
        } catch (Exception e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_UNKNOWN);
        } finally {
            try {
                this.ibatisSqlMap.endTransaction();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteApproval(UserBean bean) throws CoreException {
        Map paramMap = new HashMap();
        paramMap.put("userId", bean.getUserid());
        paramMap.put("userUpdate", bean.getUsrupd());
        paramMap.put("action", "DELETE");
        try {
            this.ibatisSqlMap.insert(this.STRING_SQL + "insertUserApproval", paramMap);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(ERROR_DB);
        }
    }


    private boolean isUsingOpenfire() {
        return "1".equals(ConfigurationProperties.getInstance().get(PropertiesKey.OPENFIRE_ENABLED));
    }
}
