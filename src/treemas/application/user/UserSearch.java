package treemas.application.user;

import treemas.base.search.SearchForm;

public class UserSearch extends SearchForm {

    private String userid;
    private String fullname;
    private String job_desc;
    private String job_area;
    private String islogin;
    private String islocked;
    private String branchId;
    private String userMaker;
    private String isapproved = "10";

    public UserSearch() {
    }


    public String getUserid() {
        return userid;
    }


    public void setUserid(String userid) {
        this.userid = userid;
    }


    public String getFullname() {
        return fullname;
    }


    public void setFullname(String fullname) {
        this.fullname = fullname;
    }


    public String getJob_desc() {
        return job_desc;
    }


    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }


    public String getJob_area() {
        return job_area;
    }


    public void setJob_area(String job_area) {
        this.job_area = job_area;
    }


    public String getIslogin() {
        return islogin;
    }


    public void setIslogin(String islogin) {
        this.islogin = islogin;
    }


    public String getIslocked() {
        return islocked;
    }


    public void setIslocked(String islocked) {
        this.islocked = islocked;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getUserMaker() {
        return userMaker;
    }

    public void setUserMaker(String userMaker) {
        this.userMaker = userMaker;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

}
