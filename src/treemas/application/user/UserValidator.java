package treemas.application.user;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.AppException;
import treemas.util.CoreException;
import treemas.util.constant.ErrorCode;
import treemas.util.constant.Global;
import treemas.util.validator.Validator;
import treemas.util.validator.ValidatorGlobal;
import treemas.util.validator.ValidatorProperties;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class UserValidator extends Validator {
    public UserValidator(HttpServletRequest request) {
        super(request);
    }

    public UserValidator(Locale locale) {
        super(locale);
    }

    public boolean validateUserAdd(ActionMessages messages, UserForm form, UserManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();

        try {
            UserBean bean = manager.getSingleUser(form.getUserid());
            if (null != bean) {
                messages.add("userid", new ActionMessage("errors.duplicate", "userid " + form.getUserid()));
            }
        } catch (AppException e) {
            this.logger.printStackTrace(e);
        }

        this.textValidator(messages, form.getUserid(), "app.user.id", "userid", "1", this.getStringLength(15), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK);
        this.textValidator(messages, form.getFullname(), "app.user.name", "fullname", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.textValidator(messages, form.getSqlpassword(), "app.user.password", "sqlpassword", this.getStringLength(ValidatorProperties.MIN_PASSWORD), this.getStringLength(0), ValidatorGlobal.TM_NOT_CHECKED_LENGTH, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);

        if (!form.getSqlpasswordconfirm().equals(form.getSqlpassword())) {
            messages.add("sqlpasswordconfirm", new ActionMessage("errors.notmatch", "password"));
        }

        this.phoneValidator(messages, form.getPhone_no(), "app.user.phone", "phone_no", ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT, ValidatorGlobal.TM_TYPE_PHONE);
        this.phoneValidator(messages, form.getHandphone_no(), "app.user.handphone", "handphone_no", ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT, ValidatorGlobal.TM_TYPE_PONSEL);

        String job_desc = form.getJob_desc();
        if (Global.JOB_SUPERVISOR_SURV.equalsIgnoreCase(job_desc)
                || Global.JOB_SUPERVISOR_COLL.equalsIgnoreCase(job_desc)
                || Global.JOB_SURV.equalsIgnoreCase(job_desc)
                || Global.JOB_COLL.equalsIgnoreCase(job_desc)) {

            this.textValidator(messages, form.getEmail(), "app.user.email", "email", "1", this.getStringLength(150), ValidatorGlobal.TM_CUSTOM_EMAIL_FLAG, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
            this.textValidator(messages, form.getHandset_imei(), "app.user.handset.imei", "handset_imei", "15", this.getStringLength(ValidatorProperties.MAX_HANDSET_IMEI), ValidatorGlobal.TM_CHECKED_LENGTH_FIX, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_ALLOW_ZERO);

        } else {
            this.textValidator(messages, form.getEmail(), "app.user.email", "email", "1", this.getStringLength(150), ValidatorGlobal.TM_CUSTOM_EMAIL_FLAG, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE);
            this.textValidator(messages, form.getHandset_imei(), "app.user.handset.imei", "handset_imei", "15", this.getStringLength(ValidatorProperties.MAX_HANDSET_IMEI), ValidatorGlobal.TM_CHECKED_LENGTH_FIX, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_ALLOW_ZERO);
        }

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }

    public boolean validateUserEdit(ActionMessages messages, UserForm form, UserManager manager) throws CoreException {
        boolean ret = true;
        if (messages == null)
            messages = new ActionMessages();


        this.textValidator(messages, form.getFullname(), "app.user.name", "fullname", "1", this.getStringLength(150), ValidatorGlobal.TM_CHECKED_LENGTH_RANGE, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE);
        this.phoneValidator(messages, form.getPhone_no(), "app.user.phone", "phone_no", ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT, ValidatorGlobal.TM_TYPE_PHONE);
        this.phoneValidator(messages, form.getHandphone_no(), "app.user.handphone", "handphone_no", ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT, ValidatorGlobal.TM_TYPE_PONSEL);
        this.textValidator(messages, form.getHandset_code(), "app.user.handset.code", "handset_code", "1", this.getStringLength(ValidatorProperties.MAX_HANDSET_CODE), ValidatorGlobal.TM_CHECKED_LENGTH_MAX, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT_DASH_UNDERSCORE);

        String job_desc = form.getJob_desc();
        if (Global.JOB_SUPERVISOR_SURV.equalsIgnoreCase(job_desc)
                || Global.JOB_SUPERVISOR_COLL.equalsIgnoreCase(job_desc)
                || Global.JOB_SURV.equalsIgnoreCase(job_desc)
                || Global.JOB_COLL.equalsIgnoreCase(job_desc)) {

            this.textValidator(messages, form.getEmail(), "app.user.email", "email", "1", this.getStringLength(150), ValidatorGlobal.TM_CUSTOM_EMAIL_FLAG, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
            this.textValidator(messages, form.getHandset_imei(), "app.user.handset.imei", "handset_imei", "15", this.getStringLength(ValidatorProperties.MAX_HANDSET_IMEI), ValidatorGlobal.TM_CHECKED_LENGTH_FIX, ValidatorGlobal.TM_FLAG_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_ALLOW_ZERO);

        } else {
            this.textValidator(messages, form.getEmail(), "app.user.email", "email", "1", this.getStringLength(150), ValidatorGlobal.TM_CUSTOM_EMAIL_FLAG, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED);
            this.textValidator(messages, form.getHandset_imei(), "app.user.handset.imei", "handset_imei", "15", this.getStringLength(ValidatorProperties.MAX_HANDSET_IMEI), ValidatorGlobal.TM_CHECKED_LENGTH_FIX, ValidatorGlobal.TM_FLAG_NON_MANDATORY, ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_ALLOW_ZERO);

        }

        if (messages != null && !messages.isEmpty())
            throw new CoreException(ErrorCode.ERR_VALIDATION_PAGE_DATA, messages);

        return ret;
    }
}
