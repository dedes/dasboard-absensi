package treemas.application.user;

import com.google.common.base.Strings;
import treemas.base.appadmin.AppAdminFormBase;
import treemas.util.beanaction.BeanConverter;
import treemas.util.constant.Global;
import treemas.util.cryptography.MD5;
import treemas.util.format.TreemasFormatter;

import java.security.NoSuchAlgorithmException;

public class UserForm extends AppAdminFormBase {

    private String userid;
    private String branchid;
    private String branchname;
    private String fullname;
    private String active = "0";
    private String sqlpassword;
    private String sqlpasswordconfirm;
    private String secquestion;
    private String secanswer;
    private String usercrt;
    private String dtmcrt;
    private String usrupd;
    private String dtmupd;
    private String islogin;
    private String lastlogin;
    private String ispasschg;
    private String user_ed;
    private String password_ed;
    private String wrongpass_count;
    private String wrongpass_time;
    private String islocked;
    private String timeslocked;
    private String locked_time;
    private String last_passchg;
    private String job_desc;
    private String job_descName;
    private String job_area;
    private String email;
    private String phone_no;
    private String handphone_no;
    private String isapproved;

    private String bbpin;
    private String handset_code;
    private String handset_imei;
    private String handset_version;
    private String gcmno;

    private String isUseMobile;
    private String isUseBB;

    private String kecamatan;
    private String kelurahan;
    private String city;
    private String zipcode;


    private String groupPrivilege;

    private UserSearch search = new UserSearch();


    public UserForm() {
        this.search.setSortBy("30");
        this.search.setSortType("20");
    }

    public String getUserid() {
        return userid;
    }


    public void setUserid(String userid) {
        this.userid = TreemasFormatter.toCamelCase(userid);
    }


    public String getBranchid() {
        return branchid;
    }


    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }


    public String getFullname() {
        return fullname;
    }


    public void setFullname(String fullname) {
        this.fullname = TreemasFormatter.toCamelCase(fullname);
    }


    public String getActive() {
        return active;
    }


    public void setActive(String active) {
        this.active = active;
    }


    public String getSqlpassword() {
        return sqlpassword;
    }


    public void setSqlpassword(String sqlpassword) {
        this.sqlpassword = sqlpassword;
    }


    public String getSecquestion() {
        return secquestion;
    }


    public void setSecquestion(String secquestion) {
        this.secquestion = secquestion;
    }


    public String getSecanswer() {
        return secanswer;
    }


    public void setSecanswer(String secanswer) {
        this.secanswer = secanswer;
    }


    public String getUsercrt() {
        return usercrt;
    }


    public void setUsercrt(String usercrt) {
        this.usercrt = usercrt;
    }


    public String getDtmcrt() {
        return dtmcrt;
    }


    public void setDtmcrt(String dtmcrt) {
        this.dtmcrt = dtmcrt;
    }


    public String getUsrupd() {
        return usrupd;
    }


    public void setUsrupd(String usrupd) {
        this.usrupd = usrupd;
    }


    public String getDtmupd() {
        return dtmupd;
    }


    public void setDtmupd(String dtmupd) {
        this.dtmupd = dtmupd;
    }


    public String getIslogin() {
        return islogin;
    }


    public void setIslogin(String islogin) {
        this.islogin = islogin;
    }


    public String getLastlogin() {
        return lastlogin;
    }


    public void setLastlogin(String lastlogin) {
        this.lastlogin = lastlogin;
    }


    public String getIspasschg() {
        return ispasschg;
    }


    public void setIspasschg(String ispasschg) {
        this.ispasschg = ispasschg;
    }


    public String getUser_ed() {
        return user_ed;
    }


    public void setUser_ed(String user_ed) {
        this.user_ed = user_ed;
    }


    public String getPassword_ed() {
        return password_ed;
    }


    public void setPassword_ed(String password_ed) {
        this.password_ed = password_ed;
    }


    public String getWrongpass_count() {
        return wrongpass_count;
    }


    public void setWrongpass_count(String wrongpass_count) {
        this.wrongpass_count = wrongpass_count;
    }


    public String getWrongpass_time() {
        return wrongpass_time;
    }


    public void setWrongpass_time(String wrongpass_time) {
        this.wrongpass_time = wrongpass_time;
    }


    public String getIslocked() {
        return islocked;
    }


    public void setIslocked(String islocked) {
        this.islocked = islocked;
    }


    public String getTimeslocked() {
        return timeslocked;
    }


    public void setTimeslocked(String timeslocked) {
        this.timeslocked = timeslocked;
    }


    public String getLocked_time() {
        return locked_time;
    }


    public void setLocked_time(String locked_time) {
        this.locked_time = locked_time;
    }


    public String getLast_passchg() {
        return last_passchg;
    }


    public void setLast_passchg(String last_passchg) {
        this.last_passchg = last_passchg;
    }


    public String getJob_desc() {
        return job_desc;
    }


    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }


    public String getJob_area() {
        return job_area;
    }


    public void setJob_area(String job_area) {
        this.job_area = job_area;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getPhone_no() {
        return phone_no;
    }


    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }


    public String getHandphone_no() {
        return handphone_no;
    }


    public void setHandphone_no(String handphone_no) {
        this.handphone_no = handphone_no;
    }


    public String getBbpin() {
        return bbpin;
    }


    public void setBbpin(String bbpin) {
        this.bbpin = bbpin;
    }


    public String getHandset_code() {
        return handset_code;
    }


    public void setHandset_code(String handset_code) {
        this.handset_code = handset_code;
    }


    public String getHandset_imei() {
        return handset_imei;
    }


    public void setHandset_imei(String handset_imei) {
        this.handset_imei = handset_imei;
    }


    public String getHandset_version() {
        return handset_version;
    }


    public void setHandset_version(String handset_version) {
        this.handset_version = handset_version;
    }


    public String getGcmno() {
        return gcmno;
    }


    public void setGcmno(String gcmno) {
        this.gcmno = gcmno;
    }


    public String getIsUseMobile() {
        return isUseMobile;
    }


    public void setIsUseMobile(String isUseMobile) {
        this.isUseMobile = isUseMobile;
    }


    public String getIsUseBB() {
        return isUseBB;
    }


    public void setIsUseBB(String isUseBB) {
        this.isUseBB = isUseBB;
    }


    public UserSearch getSearch() {
        return search;
    }


    public void setSearch(UserSearch search) {
        this.search = search;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = TreemasFormatter.toCamelCase(kecamatan);
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = TreemasFormatter.toCamelCase(kelurahan);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = TreemasFormatter.toCamelCase(city);
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getJob_descName() {
        return job_descName;
    }

    public void setJob_descName(String job_descName) {
        this.job_descName = TreemasFormatter.toCamelCase(job_descName);
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = TreemasFormatter.toCamelCase(branchname);
    }

    public String getGroupPrivilege() {
        return groupPrivilege;
    }

    public void setGroupPrivilege(String groupPrivilege) {
        this.groupPrivilege = groupPrivilege;
    }

    public String getSqlpasswordconfirm() {
        return sqlpasswordconfirm;
    }

    public void setSqlpasswordconfirm(String sqlpasswordconfirm) {
        this.sqlpasswordconfirm = sqlpasswordconfirm;
    }

    public String getIsapproved() {
        return isapproved;
    }

    public void setIsapproved(String isapproved) {
        this.isapproved = isapproved;
    }

    public UserBean toBean() {
        UserBean bean = new UserBean();
        BeanConverter.convertBeanFromString(this, bean);
        bean.setIsUseBB(this.isUseBB);
        bean.setIsUseMobile(this.isUseMobile);
        if (!Strings.isNullOrEmpty(bean.getGroupPrivilege())) {
            String privileges = bean.getGroupPrivilege();
            if (privileges.startsWith(Global.ARRAY_COMMA_DELIMITER)) {
                privileges.substring(1);
            }
            bean.setGroupPrivilege(privileges);
        }

        try {
            if (!Strings.isNullOrEmpty(this.getSqlpassword())) {
                String password = this.getSqlpassword();
                if (!Strings.isNullOrEmpty(password)) {
                    String hashPassword = MD5.getInstance().hashData(password.getBytes());
                    bean.setSqlpassword(hashPassword);
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return bean;
    }

}
