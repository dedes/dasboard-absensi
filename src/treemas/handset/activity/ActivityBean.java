package treemas.handset.activity;

public class ActivityBean {
    private String user;
    private String branch;
    private String activity;

    public ActivityBean(String user, String branch, String activity) {
        super();
        this.user = user;
        this.branch = branch;
        this.activity = activity;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

}
