package treemas.handset.detaillist;

public class ProjectBean {

	private String idProject;
	private String namaProject;
	private String lokasi;
	private String noTlp;
	private String biayaReimburse;
	 private String gpsLatitude;
	  private String gpsLongitude;
	  
	public String getIdProject() {
		return idProject;
	}
	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}
	public String getNamaProject() {
		return namaProject;
	}
	public void setNamaProject(String namaProject) {
		this.namaProject = namaProject;
	}
	public String getLokasi() {
		return lokasi;
	}
	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}
	public String getNoTlp() {
		return noTlp;
	}
	public void setNoTlp(String noTlp) {
		this.noTlp = noTlp;
	}
	public String getBiayaReimburse() {
		return biayaReimburse;
	}
	public void setBiayaReimburse(String biayaReimburse) {
		this.biayaReimburse = biayaReimburse;
	}
	public String getGpsLatitude() {
		return gpsLatitude;
	}
	public void setGpsLatitude(String gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}
	public String getGpsLongitude() {
		return gpsLongitude;
	}
	public void setGpsLongitude(String gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}

	  
	  
	  
}
