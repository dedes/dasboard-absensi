package treemas.handset.detaillist;

import java.util.Date;

public class ListAbsen {
	
	 private String nik;
	  private String tglAbsen;
	  private String jamMasuk;
	  private String jamPulang;
	  private String gpsLatitudeMsk;
	  private String gpsLongitudeMsk;
	  private String gpsLatitudePlg;
	  private String gpsLongitudePlg;
	  private String LokasiMsk;
	  private String LokasiPlg;
	  private String isLembur;	
	  private String note;
	  private String jarakMsk;
	  private String jarakPlg;
	  private String totalJamKerja;
	  private String flgabs;
	  private String isAbsen;
	  private String Hari;
	  private String Tglmasuk;
	  private String Idproject;
	  
	  
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getTglAbsen() {
		return tglAbsen;
	}
	public void setTglAbsen(String tglAbsen) {
		this.tglAbsen = tglAbsen;
	}
	public String getJamMasuk() {
		return jamMasuk;
	}
	public void setJamMasuk(String jamMasuk) {
		this.jamMasuk = jamMasuk;
	}
	public String getJamPulang() {
		return jamPulang;
	}
	public void setJamPulang(String jamPulang) {
		this.jamPulang = jamPulang;
	}
	public String getGpsLatitudeMsk() {
		return gpsLatitudeMsk;
	}
	public void setGpsLatitudeMsk(String gpsLatitudeMsk) {
		this.gpsLatitudeMsk = gpsLatitudeMsk;
	}
	public String getGpsLongitudeMsk() {
		return gpsLongitudeMsk;
	}
	public void setGpsLongitudeMsk(String gpsLongitudeMsk) {
		this.gpsLongitudeMsk = gpsLongitudeMsk;
	}
	public String getGpsLatitudePlg() {
		return gpsLatitudePlg;
	}
	public void setGpsLatitudePlg(String gpsLatitudePlg) {
		this.gpsLatitudePlg = gpsLatitudePlg;
	}
	public String getGpsLongitudePlg() {
		return gpsLongitudePlg;
	}
	public void setGpsLongitudePlg(String gpsLongitudePlg) {
		this.gpsLongitudePlg = gpsLongitudePlg;
	}
	public String getLokasiMsk() {
		return LokasiMsk;
	}
	public void setLokasiMsk(String lokasiMsk) {
		LokasiMsk = lokasiMsk;
	}
	public String getLokasiPlg() {
		return LokasiPlg;
	}
	public void setLokasiPlg(String lokasiPlg) {
		LokasiPlg = lokasiPlg;
	}
	public String getIsLembur() {
		return isLembur;
	}
	public void setIsLembur(String isLembur) {
		this.isLembur = isLembur;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getJarakMsk() {
		return jarakMsk;
	}
	public void setJarakMsk(String jarakMsk) {
		this.jarakMsk = jarakMsk;
	}
	public String getJarakPlg() {
		return jarakPlg;
	}
	public void setJarakPlg(String jarakPlg) {
		this.jarakPlg = jarakPlg;
	}
	public String getTotalJamKerja() {
		return totalJamKerja;
	}
	public void setTotalJamKerja(String totalJamKerja) {
		this.totalJamKerja = totalJamKerja;
	}
	public String getFlgabs() {
		return flgabs;
	}
	public void setFlgabs(String flgabs) {
		this.flgabs = flgabs;
	}
	public String getIsAbsen() {
		return isAbsen;
	}
	public void setIsAbsen(String isAbsen) {
		this.isAbsen = isAbsen;
	}
	public String getHari() {
		return Hari;
	}
	public void setHari(String hari) {
		Hari = hari;
	}
	public String getTglmasuk() {
		return Tglmasuk;
	}
	public void setTglmasuk(String tglmasuk) {
		Tglmasuk = tglmasuk;
	}
	public String getIdproject() {
		return Idproject;
	}
	public void setIdproject(String idproject) {
		Idproject = idproject;
	}
	  
	  

}
