package treemas.handset.detaillist;

import java.util.Date;

public class ListReimburse {
	
	private String nik;
    private String nama;
    private String hari;
    private String tgl;
    private String lokasi;
    private String kota;
    private String idProject;
    private String jamMasuk;
    private String jamKeluar;
    private String transport;
    private String uangMakan;
    private String keterangan;
    private String isApproved;
    private String noteApp;
    private String uangData;
    private String image;
    private String totalJamKerja;
    private String usrUpd;
    private String dtmUpd;
    private String usrCrt;
    private String dtmCrt;
    private String usrApp;
    private String dtmApp;
    
    
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getHari() {
		return hari;
	}
	public void setHari(String hari) {
		this.hari = hari;
	}
	public String getTgl() {
		return tgl;
	}
	public void setTgl(String tgl) {
		this.tgl = tgl;
	}
	public String getLokasi() {
		return lokasi;
	}
	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}
	public String getKota() {
		return kota;
	}
	public void setKota(String kota) {
		this.kota = kota;
	}
	public String getIdProject() {
		return idProject;
	}
	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}
	public String getJamMasuk() {
		return jamMasuk;
	}
	public void setJamMasuk(String jamMasuk) {
		this.jamMasuk = jamMasuk;
	}
	public String getJamKeluar() {
		return jamKeluar;
	}
	public void setJamKeluar(String jamKeluar) {
		this.jamKeluar = jamKeluar;
	}
	public String getTransport() {
		return transport;
	}
	public void setTransport(String transport) {
		this.transport = transport;
	}
	public String getUangMakan() {
		return uangMakan;
	}
	public void setUangMakan(String uangMakan) {
		this.uangMakan = uangMakan;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(String isApproved) {
		this.isApproved = isApproved;
	}
	public String getNoteApp() {
		return noteApp;
	}
	public void setNoteApp(String noteApp) {
		this.noteApp = noteApp;
	}
	public String getUangData() {
		return uangData;
	}
	public void setUangData(String uangData) {
		this.uangData = uangData;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getTotalJamKerja() {
		return totalJamKerja;
	}
	public void setTotalJamKerja(String totalJamKerja) {
		this.totalJamKerja = totalJamKerja;
	}
	public String getUsrUpd() {
		return usrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		this.usrUpd = usrUpd;
	}
	public String getDtmUpd() {
		return dtmUpd;
	}
	public void setDtmUpd(String dtmUpd) {
		this.dtmUpd = dtmUpd;
	}
	public String getUsrCrt() {
		return usrCrt;
	}
	public void setUsrCrt(String usrCrt) {
		this.usrCrt = usrCrt;
	}
	public String getDtmCrt() {
		return dtmCrt;
	}
	public void setDtmCrt(String dtmCrt) {
		this.dtmCrt = dtmCrt;
	}
	public String getUsrApp() {
		return usrApp;
	}
	public void setUsrApp(String usrApp) {
		this.usrApp = usrApp;
	}
	public String getDtmApp() {
		return dtmApp;
	}
	public void setDtmApp(String dtmApp) {
		this.dtmApp = dtmApp;
	}

    
    
    
}
