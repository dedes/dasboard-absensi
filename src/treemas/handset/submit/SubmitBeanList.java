package treemas.handset.submit;

import treemas.handset.tracking.TrackingBean;

import java.util.List;

public class SubmitBeanList {
    private Integer mobile_assignment_id;
    private boolean isBekas;
    private List<SubmitAnswerBean> answerList;
    private TrackingBean trackingBean;

    public SubmitBeanList() {
    }

    public Integer getMobile_assignment_id() {
        return mobile_assignment_id;
    }

    public void setMobile_assignment_id(Integer mobile_assignment_id) {
        this.mobile_assignment_id = mobile_assignment_id;
    }

    public boolean isBekas() {
        return isBekas;
    }

    public void setBekas(boolean isBekas) {
        this.isBekas = isBekas;
    }

    public List<SubmitAnswerBean> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<SubmitAnswerBean> answerList) {
        this.answerList = answerList;
    }

    public TrackingBean getTrackingBean() {
        return trackingBean;
    }

    public void setTrackingBean(TrackingBean trackingBean) {
        this.trackingBean = trackingBean;
    }


}
