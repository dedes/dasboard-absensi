package treemas.handset.newValey;

import treemas.handset.list.HeaderBean;

public class SubmitHeaderBean {

    private HeaderBean headerBean;

    public SubmitHeaderBean() {
    }

    public HeaderBean getHeaderBean() {
        return headerBean;
    }

    public void setHeaderBean(HeaderBean headerBean) {
        this.headerBean = headerBean;
    }

}
