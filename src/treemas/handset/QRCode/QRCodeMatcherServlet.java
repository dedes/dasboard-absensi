package treemas.handset.QRCode;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class QRCodeServlet
 */
public class QRCodeMatcherServlet extends HttpServlet {

	protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		String name = request.getHeader("name");
		String id = request.getHeader("id");
		System.out.println(id + name);
		System.out.println("servlet call put");
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("servlet call get");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("servlet call post");
	}

}
