package treemas.handset.timeSync;

import com.google.common.base.Strings;
import treemas.base.handset.ServletBase;
import treemas.util.encoder.Base64;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@SuppressWarnings("serial")
public class TimeSync extends ServletBase {
    private String delimiter = ":";

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String message = "User not authorized";
        PrintWriter out = resp.getWriter();
        System.out.println("Android calling /m/schemeservlet/.... method get");

        boolean isAuthorized = false;
        String token = req.getHeader("Authorization");
        if (!Strings.isNullOrEmpty(token)) {

            if (token.startsWith("Basic ")) {
                token = token.replace("Basic ", "");
                String decodeToken = new String(Base64.decode(token));
                String[] dataToken = decodeToken.split(this.delimiter);
                if (dataToken.length == 4) {
                    isAuthorized = true;
                }
            } else {
                isAuthorized = false;
            }
        } else {
            isAuthorized = false;
        }

//		boolean isTrue = false;
//		
//		InputStream in = req.getInputStream();
//		BufferedReader br = new BufferedReader(new InputStreamReader(in));
//		StringBuffer sb = new StringBuffer();
//		String line;
//		while ((line = br.readLine()) != null) {
//			sb.append(line);
//		}
//
//		String data = sb.toString();
//		if (!Strings.isNullOrEmpty(data) && isAuthorized) {
//			isTrue = true;
//		} else {
//			isTrue = false;
//		}

        if (isAuthorized) {
            message = String.valueOf(new Date().getTime());
        }

        if (isAuthorized) {
            out.write(message);
        } else {
            String ret =
                    "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">"
                            + message + "</string>";
            System.out.println(ret);
            out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            out.append(ret);
        }

    }
}
