package treemas.handset.loadlist;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import treemas.base.handset.ServletBase;
import treemas.handset.list.ParameterBean;
import treemas.handset.login.LoginBean;
import treemas.util.CoreException;
import treemas.util.cryptography.MD5;
import treemas.util.encoder.Base64;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public class LoadServlet extends ServletBase {
	
	  private boolean enEnabled = true;
	    private boolean deEnabled = true;

	    private String url = "/m/loadservlet/";
	    private String delimiter = ":";
	    private String rkm = "RKM Android User";

	    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	            throws ServletException, IOException {
	        doPost(req, resp);
	    }

	    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	            throws ServletException, IOException {
	        String message = "User not authorized";
	        PrintWriter out = resp.getWriter();
	        System.out.println("Android calling /m/loadservlet/.... method get");

	        String IMEI = null;
	        String userName = null;
	        String password = null;

	        boolean isAuthorized = false;
	        String token = req.getHeader("Authorization");
	        if (!Strings.isNullOrEmpty(token)) {
	        	

	            if (token.startsWith("Basic ")) {
	                token = token.replace("Basic ", "");
	                String decodeToken = new String(Base64.decode(token));
	                String[] dataToken = decodeToken.split(this.delimiter);
	                if (dataToken.length == 4) {

	                    IMEI = dataToken[0];
	                    password = dataToken[1];
	                    userName = dataToken[2];
	                    rkm = dataToken[3];
	                    isAuthorized = true;
	                }
	            } else {
	                isAuthorized = false;
	            }
	        } else {
	            isAuthorized = false;
	        }

	        boolean isTrue = false;

	        InputStream in = req.getInputStream();
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        StringBuffer sb = new StringBuffer();
	        String line;
	        while ((line = br.readLine()) != null) {
	            sb.append(line);
	        }

	        String data = sb.toString();

	        if (!Strings.isNullOrEmpty(data) && isAuthorized) {
	            isTrue = true;
	        } else {
	            isTrue = false;
	        }

	        if (isTrue) {
	            try {
	                LoginBean lb = new LoginBean();
	                lb.setIMEI(IMEI);
	                lb.setUserName(userName);
	                lb.setUserPassword(MD5.getInstance().hashData(password));
	                message = this.demoManager.getListData(lb);
	            } catch (SQLException e) {
	                e.printStackTrace();
	                this.logger.printStackTrace(e);
	            } catch (NoSuchAlgorithmException e) {
	                e.printStackTrace();
	                this.logger.printStackTrace(e);
	            } catch (CoreException e) {
	                e.printStackTrace();
	                this.logger.printStackTrace(e);
	            }
}
	        if (isTrue) {
	            out.write(message);
	        } else {
	            String ret =
	                    "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">"
	                            + message + "</string>";
	            System.out.println(ret);
	            out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	            out.append(ret);
	        }

	    }

}
