package treemas.handset.loadlist;

public class ListAnnouncement {

	
	private Integer id;
    private String header;
    private String notes;
    private String image_base64;
    
    
    
	public String getImage_base64() {
		return image_base64;
	}
	public void setImage_base64(String image_base64) {
		this.image_base64 = image_base64;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
    
    
 
}
