package treemas.handset.submitAbsen;

import java.util.List;

import treemas.handset.approval.SubmitApprovalBean;
import treemas.handset.tracking.TrackingBean;

public class SubmitAbsenBeanList {
	  private String mobile_assignment_id;
	  private boolean isBekas;
	    private List<SubmitAbsen> answerList;
	    private TrackingBean trackingBean;
	    
	    public SubmitAbsenBeanList() {
	    }
		public boolean isBekas() {
			return isBekas;
		}
		public void setBekas(boolean isBekas) {
			this.isBekas = isBekas;
		}
		public List<SubmitAbsen> getAnswerList() {
			return answerList;
		}
		public void setAnswerList(List<SubmitAbsen> answerList) {
			this.answerList = answerList;
		}
		public TrackingBean getTrackingBean() {
			return trackingBean;
		}
		public void setTrackingBean(TrackingBean trackingBean) {
			this.trackingBean = trackingBean;
		}
		public String getMobile_assignment_id() {
			return mobile_assignment_id;
		}
		public void setMobile_assignment_id(String mobile_assignment_id) {
			this.mobile_assignment_id = mobile_assignment_id;
		}
	    
	    
	
}
