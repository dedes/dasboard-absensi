package treemas.handset.submitAbsen;

import java.sql.Timestamp;

public class TimesheetBean {
	private String nik;
	private String nikLeader;
	private String hari;
	private String tglMsk;
	private String idProject;
	private String note;
	private String jamMasuk;
	private String jamPulang; 
	private String overtime;
	private Integer totalJamKerja;
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getNikLeader() {
		return nikLeader;
	}
	public void setNikLeader(String nikLeader) {
		this.nikLeader = nikLeader;
	}
	public String getHari() {
		return hari;
	}
	public void setHari(String hari) {
		this.hari = hari;
	}
	public String getTglMsk() {
		return tglMsk;
	}
	public void setTglMsk(String tglMsk) {
		this.tglMsk = tglMsk;
	}
	public String getIdProject() {
		return idProject;
	}
	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getJamMasuk() {
		return jamMasuk;
	}
	public void setJamMasuk(String jamMasuk) {
		this.jamMasuk = jamMasuk;
	}
	public String getJamPulang() {
		return jamPulang;
	}
	public void setJamPulang(String jamPulang) {
		this.jamPulang = jamPulang;
	}
	public String getOvertime() {
		return overtime;
	}
	public void setOvertime(String overtime) {
		this.overtime = overtime;
	}
	public Integer getTotalJamKerja() {
		return totalJamKerja;
	}
	public void setTotalJamKerja(Integer totalJamKerja) {
		this.totalJamKerja = totalJamKerja;
	}
	
	
	
}
