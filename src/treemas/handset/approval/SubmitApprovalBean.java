package treemas.handset.approval;

public class SubmitApprovalBean {
	
	private String ApprovalSchemeID;
    private String ApprovalNo;
    private String ApprovalID;
    private String UserRequest;
    private String ApprovalValue;
    private String ApprovalResult;
    private String ApprovalNote;
    private String ApprovalDate;
    private String UserRequestNext;
    private String isnext;
    private String LoginId;
    
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public String getApprovalNo() {
		return ApprovalNo;
	}
	public void setApprovalNo(String approvalNo) {
		ApprovalNo = approvalNo;
	}
	public String getApprovalID() {
		return ApprovalID;
	}
	public void setApprovalID(String approvalID) {
		ApprovalID = approvalID;
	}
	public String getUserRequest() {
		return UserRequest;
	}
	public void setUserRequest(String userRequest) {
		UserRequest = userRequest;
	}
	public String getApprovalValue() {
		return ApprovalValue;
	}
	public void setApprovalValue(String approvalValue) {
		ApprovalValue = approvalValue;
	}
	public String getApprovalResult() {
		return ApprovalResult;
	}
	public void setApprovalResult(String approvalResult) {
		ApprovalResult = approvalResult;
	}

	public String getApprovalNote() {
		return ApprovalNote;
	}
	public void setApprovalNote(String approvalNote) {
		ApprovalNote = approvalNote;
	}
	public String getApprovalDate() {
		return ApprovalDate;
	}
	public void setApprovalDate(String approvalDate) {
		ApprovalDate = approvalDate;
	}
	public String getUserRequestNext() {
		return UserRequestNext;
	}
	public void setUserRequestNext(String userRequestNext) {
		UserRequestNext = userRequestNext;
	}
	public String getIsnext() {
		return isnext;
	}
	public void setIsnext(String isnext) {
		this.isnext = isnext;
	}
	public String getLoginId() {
		return LoginId;
	}
	public void setLoginId(String loginId) {
		LoginId = loginId;
	}
    
	


}
