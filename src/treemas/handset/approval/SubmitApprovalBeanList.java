package treemas.handset.approval;

import java.util.List;

import treemas.handset.tracking.TrackingBean;

public class SubmitApprovalBeanList {
	
	  private String mobile_assignment_id;
	    private boolean isBekas;
	    private List<SubmitApprovalBean> answerList;
	    private TrackingBean trackingBean;
	    
	    public SubmitApprovalBeanList() {
	    }
	    
	
		public String getMobile_assignment_id() {
			return mobile_assignment_id;
		}


		public void setMobile_assignment_id(String mobile_assignment_id) {
			this.mobile_assignment_id = mobile_assignment_id;
		}


		public boolean isBekas() {
			return isBekas;
		}
		public void setBekas(boolean isBekas) {
			this.isBekas = isBekas;
		}
		public List<SubmitApprovalBean> getAnswerList() {
			return answerList;
		}
		public void setAnswerList(List<SubmitApprovalBean> answerList) {
			this.answerList = answerList;
		}
		public TrackingBean getTrackingBean() {
			return trackingBean;
		}
		public void setTrackingBean(TrackingBean trackingBean) {
			this.trackingBean = trackingBean;
		}
	    
	    

}
