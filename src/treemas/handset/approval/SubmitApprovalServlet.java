package treemas.handset.approval;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import treemas.base.handset.ServletBase;
import treemas.handset.login.LoginBean;
import treemas.util.CoreException;
import treemas.util.cryptography.MD5;
import treemas.util.encoder.Base64;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Date;

/**
 * Servlet implementation class SubmitCollectionServlet
 */
public class SubmitApprovalServlet extends ServletBase {
    private boolean enEnabled = true;
    private boolean deEnabled = true;

    private String url = "/m/servletsubmitcollection/";
    private String delimiter = ":";
    private String rkm = "RKM Android User";

    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String message = "User not authorized";
        PrintWriter out = resp.getWriter();
        System.out.println("Android calling /m/servletsubmitapproval/.... method PUT");

        String IMEI = null;
        String userName = null;
        String password = null;

        boolean isAuthorized = false;
        boolean isMethod = false;
        String method = req.getMethod();
        if (method.equalsIgnoreCase("PUT")) {
            isMethod = true;
        } else {
            resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            message = "Method not allowed";
        }

        String token = req.getHeader("Authorization");
        if (!Strings.isNullOrEmpty(token) && isMethod) {

            if (token.startsWith("Basic ")) {
                token = token.replace("Basic ", "");
                String decodeToken = new String(Base64.decode(token));
                String[] dataToken = decodeToken.split(this.delimiter);
                if (dataToken.length == 4) {

                    IMEI = dataToken[0];
                    password = dataToken[1];
                    userName = dataToken[2];
                    rkm = dataToken[3];
                    isAuthorized = true;
                }
            } else {
                resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                isAuthorized = false;
            }
        } else {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            isAuthorized = false;
        }

        boolean isTrue = false;
        InputStream in = req.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        StringBuffer sb = new StringBuffer();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        String data = sb.toString();

        if (!Strings.isNullOrEmpty(data) && isAuthorized) {
            isTrue = true;
        } else {
            isTrue = false;
        }
        boolean isDone = false;
        if (isTrue) {
            try {
                LoginBean loginBean = new LoginBean();
                loginBean.setIMEI(IMEI);
                loginBean.setUserName(userName);
                loginBean.setUserPassword(MD5.getInstance().hashData(password));
                Date d = new Date();
                String dJ = new Gson().toJson(d, Date.class);
                System.out.println(dJ);

                SubmitApprovalBeanList list = new Gson().fromJson(data,
                		SubmitApprovalBeanList.class);
                Integer id = this.demoManager.submitResultApproval(loginBean, list);
                isDone = true;
                resp.setStatus(HttpServletResponse.SC_OK);
                // message = this.demoManager.getSurveyList(lb, pBean);
            } catch (CoreException ex) {
                ex.printStackTrace();
                resp.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
            } catch (SQLException e) {
                e.printStackTrace();
                resp.setStatus(HttpServletResponse.SC_CONFLICT);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        }

        if (!isTrue) {
            String ret = "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">"
                    + message + "</string>";
            System.out.println(ret);
            out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            out.append(ret);
        } else if (!isDone) {
            message = "Error Saving Data";
            String ret = "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">"
                    + message + "</string>";
            System.out.println(ret);
            out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            out.append(ret);
        } else {
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

    }


}
