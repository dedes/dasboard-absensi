/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package treemas.handset.combo;

import java.util.List;

/**
 * @author user
 */
public class ListAllCombo {
    private List<TugasBean> listTugas;
    private List<PhoneBean> listPhone;

    public ListAllCombo() {
    }

    public ListAllCombo(List<TugasBean> listTugas, List<PhoneBean> listPhone) {
        this.listTugas = listTugas;
        this.listPhone = listPhone;
    }

    public List<TugasBean> getListTugas() {
        return listTugas;
    }

    public void setListTugas(List<TugasBean> listTugas) {
        this.listTugas = listTugas;
    }

    public List<PhoneBean> getListPhone() {
        return listPhone;
    }

    public void setListPhone(List<PhoneBean> listPhone) {
        this.listPhone = listPhone;
    }


}
