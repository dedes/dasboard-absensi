/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package treemas.handset.combo;

import java.util.List;

/**
 * @author user
 */
public class TugasBean {
    private Integer question_group_id;
    private Integer question_id;
    private String question_label;
    private List<HasilBean> list;

    public TugasBean() {
    }

    public TugasBean(Integer question_group_id, Integer question_id, String question_label, List<HasilBean> list) {
        this.question_group_id = question_group_id;
        this.question_id = question_id;
        this.question_label = question_label;
        this.list = list;
    }

    public Integer getQuestion_group_id() {
        return question_group_id;
    }

    public void setQuestion_group_id(Integer question_group_id) {
        this.question_group_id = question_group_id;
    }

    public Integer getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(Integer question_id) {
        this.question_id = question_id;
    }

    public String getQuestion_label() {
        return question_label;
    }

    public void setQuestion_label(String question_label) {
        this.question_label = question_label;
    }

    public List<HasilBean> getList() {
        return list;
    }

    public void setList(List<HasilBean> list) {
        this.list = list;
    }

}
