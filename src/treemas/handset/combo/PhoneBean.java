/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package treemas.handset.combo;

/**
 * @author user
 */
public class PhoneBean {

    private Integer question_group_id;
    private Integer question_id;
    private String question_label;

    public PhoneBean() {
    }

    public PhoneBean(Integer question_group_id, Integer question_id, String question_label) {
        this.question_group_id = question_group_id;
        this.question_id = question_id;
        this.question_label = question_label;
    }

    public Integer getQuestion_group_id() {
        return question_group_id;
    }

    public void setQuestion_group_id(Integer question_group_id) {
        this.question_group_id = question_group_id;
    }

    public Integer getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(Integer question_id) {
        this.question_id = question_id;
    }

    public String getQuestion_label() {
        return question_label;
    }

    public void setQuestion_label(String question_label) {
        this.question_label = question_label;
    }
}
