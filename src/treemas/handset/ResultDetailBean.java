package treemas.handset;

import treemas.util.encoder.Base64;

import java.util.Date;

public class ResultDetailBean {
    private Integer mobileAssignmentId;
    private Integer questionGroupId;
    private Integer questionId;
    private Integer optionAnswerId;
    private String textAnswer;
    private String imagePath;
    private String questionText;
    private String optionAnswerText;
    private Double gpsLatitude;
    private Double gpsLongitude;
    private Integer MCC;
    private Integer MNC;
    private Integer LAC;
    private Integer cellId;
    private String isGps;
    private Integer accuracy;
    private String provider;
    private byte[] image;
    private String lov;
    private boolean isFinal = true;
    private Date timestamp;

    public ResultDetailBean() {

    }

    public ResultDetailBean(String[] arrayData) {
        try {
            this.mobileAssignmentId = new Integer(arrayData[0]);
            this.questionGroupId = new Integer(arrayData[1]);
            this.questionId = new Integer(arrayData[2]);
            if (!"".equals(arrayData[3]))
                this.optionAnswerId = new Integer(arrayData[3]);
            this.textAnswer = arrayData[4];
            this.image = Base64.decode(arrayData[5]);
            this.lov = arrayData[6];
            if (arrayData.length > 7) {
                this.isFinal = "1".equals(arrayData[7]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Integer getMobileAssignmentId() {
        return mobileAssignmentId;
    }

    public void setMobileAssignmentId(Integer mobileAssignmentId) {
        this.mobileAssignmentId = mobileAssignmentId;
    }

    public Integer getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(Integer questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public Integer getOptionAnswerId() {
        return optionAnswerId;
    }

    public void setOptionAnswerId(Integer optionAnswerId) {
        this.optionAnswerId = optionAnswerId;
    }

    public String getTextAnswer() {
        return textAnswer;
    }

    public void setTextAnswer(String textAnswer) {
        this.textAnswer = textAnswer;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getOptionAnswerText() {
        return optionAnswerText;
    }

    public void setOptionAnswerText(String optionAnswerText) {
        this.optionAnswerText = optionAnswerText;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public Integer getMCC() {
        return MCC;
    }

    public void setMCC(Integer mcc) {
        MCC = mcc;
    }

    public Integer getMNC() {
        return MNC;
    }

    public void setMNC(Integer mnc) {
        MNC = mnc;
    }

    public Integer getLAC() {
        return LAC;
    }

    public void setLAC(Integer lac) {
        LAC = lac;
    }

    public Integer getCellId() {
        return cellId;
    }

    public void setCellId(Integer cellId) {
        this.cellId = cellId;
    }

    public String getIsGps() {
        return isGps;
    }

    public void setIsGps(String isGps) {
        this.isGps = isGps;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getLov() {
        return lov;
    }

    public void setLov(String lov) {
        this.lov = lov;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

}