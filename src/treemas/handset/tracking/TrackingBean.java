package treemas.handset.tracking;

public class TrackingBean {
    private Integer seqNo;
    private String surveyor;
    private Double latitude;
    private Double longitude;
    private Integer MCC;
    private Integer MNC;
    private Integer LAC;
    private Integer cellId;
    private String timestamp;
    private String isGps;
    private Integer accuracy;
    private String provider;
    private String isTask;

    public TrackingBean() {
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public String getSurveyor() {
        return surveyor;
    }

    public void setSurveyor(String surveyor) {
        this.surveyor = surveyor;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getMCC() {
        return MCC;
    }

    public void setMCC(Integer mCC) {
        MCC = mCC;
    }

    public Integer getMNC() {
        return MNC;
    }

    public void setMNC(Integer mNC) {
        MNC = mNC;
    }

    public Integer getLAC() {
        return LAC;
    }

    public void setLAC(Integer lAC) {
        LAC = lAC;
    }

    public Integer getCellId() {
        return cellId;
    }

    public void setCellId(Integer cellId) {
        this.cellId = cellId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getIsGps() {
        return isGps;
    }

    public void setIsGps(String isGps) {
        this.isGps = isGps;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getIsTask() {
        return isTask;
    }

    public void setIsTask(String isTask) {
        this.isTask = isTask;
    }


}
