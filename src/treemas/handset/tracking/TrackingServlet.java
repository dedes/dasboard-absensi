package treemas.handset.tracking;

import com.google.common.base.Strings;
import treemas.base.handset.ServletBase;
import treemas.handset.login.LoginBean;
import treemas.util.cryptography.MD5;
import treemas.util.encoder.Base64;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.NoSuchAlgorithmException;

public class TrackingServlet extends ServletBase {
    private boolean enEnabled = true;
    private boolean deEnabled = true;

    private String url = "/m/trackingdemo/";
    private String delimiter = ":";
    private String rkm = "RKM Android User";

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
    }

    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String message = "User not authorized";
        PrintWriter out = resp.getWriter();
        System.out.println("Android calling /m/trackingdemo/.... method get");

        String IMEI = null;
        String userName = null;
        String password = null;

        boolean isAuthorized = false;
        String token = req.getHeader("Authorization");
        if (!Strings.isNullOrEmpty(token)) {

            if (token.startsWith("Basic ")) {
                token = token.replace("Basic ", "");
                String decodeToken = new String(Base64.decode(token));
                String[] dataToken = decodeToken.split(this.delimiter);
                if (dataToken.length == 4) {

                    IMEI = dataToken[0];
                    password = dataToken[1];
                    userName = dataToken[2];
                    rkm = dataToken[3];
                    isAuthorized = true;
                }
            } else {
                isAuthorized = false;
            }
        } else {
            isAuthorized = false;
        }

        boolean isTrue = false;
//		IMEI = "352879066025843";
//		userName="SURV001";
//		password="12345678";
        //   MobileCollections/m/servletdemo/?json={"PageNumber":1, "ItemsPerPage":10,"PlanDate":"2014-11-10"}
        InputStream in = req.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        StringBuffer sb = new StringBuffer();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        String data = sb.toString();
        if (!Strings.isNullOrEmpty(data) && isAuthorized) {
            isTrue = true;
        } else {
            isTrue = false;
        }

//		isTrue = true;
        if (isTrue) {
            try {
                LoginBean lb = new LoginBean();
                lb.setIMEI(IMEI);
                lb.setUserName(userName);
                lb.setUserPassword(MD5.getInstance().hashData(password));
                message = this.demoManager.saveLocationTracking(lb, data, false);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        out.write(message);

    }
}
