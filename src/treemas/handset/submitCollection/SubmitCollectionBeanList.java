package treemas.handset.submitCollection;

import treemas.handset.tracking.TrackingBean;

import java.util.List;

public class SubmitCollectionBeanList {
    private Integer mobile_assignment_id;
    private boolean isBekas;
    private List<SubmitCollectionBean> answerList;
    private TrackingBean trackingBean;

    public SubmitCollectionBeanList() {
    }

    public Integer getMobile_assignment_id() {
        return mobile_assignment_id;
    }

    public void setMobile_assignment_id(Integer mobile_assignment_id) {
        this.mobile_assignment_id = mobile_assignment_id;
    }

    public boolean isBekas() {
        return isBekas;
    }

    public void setBekas(boolean isBekas) {
        this.isBekas = isBekas;
    }

    public List<SubmitCollectionBean> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<SubmitCollectionBean> answerList) {
        this.answerList = answerList;
    }

    public TrackingBean getTrackingBean() {
        return trackingBean;
    }

    public void setTrackingBean(TrackingBean trackingBean) {
        this.trackingBean = trackingBean;
    }

}
