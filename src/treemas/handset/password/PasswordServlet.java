package treemas.handset.password;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Strings;

import treemas.base.handset.ServletBase;
import treemas.handset.login.LoginBean;
import treemas.util.constant.PropertiesKey;
import treemas.util.cryptography.MD5;
import treemas.util.cryptography.RC4;
import treemas.util.encoder.Base64;
import treemas.util.tool.ConfigurationProperties;

public class PasswordServlet extends ServletBase  {
    private boolean enEnabled = true;
    private boolean deEnabled = true;

    private String url = "/m/password/";
    private String delimiter = ":";
    private String rkm = "RKM Android User";

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String enkrip = ConfigurationProperties.getInstance().get(PropertiesKey.ENCRYPT_ENABLE);
        String dekrip = ConfigurationProperties.getInstance().get(PropertiesKey.DECRYPT_ENBALE);

        this.enEnabled = ("1".equals(enkrip));
        this.deEnabled = ("1".equals(dekrip));

        String message = "Invalid user";
        PrintWriter out = resp.getWriter();
        System.out.println("Android calling /m/password/.... method get");
        String data = req.getRequestURI().replace(req.getContextPath(), "").replace(this.url, "");

        String decode = null;
        if (enEnabled) {
            try {
                decode = RC4.decrypt(Base64.decode(data));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            decode = new String(Base64.decode(data));
        }

        boolean loginSuccess = false;
        boolean isLogedIn = false;
        String Type = null;
        String IMEI = null;
        String userName = null;
        String password = null;
        String regId = null;
        String[] dataAndroid = new String[5];

        LoginBean lb = null;
        if (Strings.isNullOrEmpty(decode)) {
            loginSuccess = false;
        } else {
            loginSuccess = true;
            dataAndroid = decode.split(this.delimiter);
            if (Strings.isNullOrEmpty(dataAndroid[0])) {
                loginSuccess = false;
            } else {
                if (dataAndroid.length == 5) {
                    Type = dataAndroid[0];
                    userName = dataAndroid[1];
                    password = dataAndroid[2];
                    IMEI = dataAndroid[3];
                    regId = dataAndroid[4];
                    lb = new LoginBean();
                    lb.setUserName(userName);
                    lb.setUserPassword(password);
                    lb.setIMEI(IMEI);
                    lb.setRegId(regId);
                    lb.setAppType(Type);
                } else {
                    loginSuccess = false;
                }
            }
        }

        if (loginSuccess) {
            try {
            	 
            	
            	if (Type.equals("UPD")){;
            		String pass = MD5.getInstance().hashData(password);
            		this.demoManager.updatepass(userName,pass); 
            		resp.setStatus(HttpServletResponse.SC_OK);
            		message = "sukses";
            	}else if (Type.equals("RST")){
            		 String result1 = this.demoManager.cekUserName(userName);
                     if (Strings.isNullOrEmpty(result1)){
                     	message = "Invalid User";
                     } else {
            		String pwd = "12345";
            		String pass = MD5.getInstance().hashData(pwd);
            		this.demoManager.updatepass(userName,pass); 
            		resp.setStatus(HttpServletResponse.SC_OK);
            		message = "sukses";
                     }
            	}else {
            		message = "Invalid Type Forward";
            	}

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                this.logger.printStackTrace(e);
            } catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        String ret = "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">"
                + message + "</string>";
        System.out.println(ret);
        out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        out.append(ret);
    }

}
