package treemas.handset.push;

import com.google.common.base.Strings;
import treemas.base.handset.ServletBase;
import treemas.globalbean.SessionBean;
import treemas.util.constant.Global;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;


public class PushNotification extends ServletBase {
    private boolean enEnabled = true;
    private boolean deEnabled = true;

    private String url = "/m/push/notification";
    private String delimiter = ":";
    private String rkm = "RKM Android User";

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/event-stream");
//		resp.setCharacterEncoding("UTF-8");
        resp.setHeader("Access-Control-Allow-Origin", "*");
//		resp.setContentType("text/event-stream, charset=utf-8");
        while (true) {
            SessionBean bean = this.getLoginBean(req);
            if (null == bean) {
                Thread t = Thread.currentThread();
                if (null == t) {
                    t.stop();
                    t.destroy();
                }
                break;
            }
            PrintWriter out = resp.getWriter();
            Date date = new Date();
            if (null != bean) {
                if (Strings.isNullOrEmpty(bean.getUserid())) {
                    String data = bean.getLoginId();
                    System.out.println("user: " + data + "," + date.toString() + " is Loggedin: " + bean.getIsLogin());
                    out.print("event: server-time\n");
//					out.print("event: message\n");
//					out.print("data: "+ date.toString() + "\n\n");
                    out.print("data: " + data + "," + date.toString() + " is Loggedin " + bean.getIsLogin() + "\n\n");
                    out.flush();

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    Thread t = Thread.currentThread();
                    if (null == t) {
                        t.stop();
                        t.destroy();
                    }
                    break;
                }
            } else {
                Thread t = Thread.currentThread();
                if (null == t) {
                    t.stop();
                    t.destroy();
                }
                break;
            }
        }
    }

    protected String getLoginId(HttpServletRequest request) {
        String result = null;
        SessionBean bean = this.getLoginBean(request);
        if (bean != null) {
            result = bean.getLoginId();
        }
        result = (result == null) ? "NOTLOGIN" : result;
        return result;
    }

    protected SessionBean getLoginBean(HttpServletRequest request) {
        SessionBean bean = null;

        if (null != request.getSession(false)) {
            bean = (SessionBean) request.getSession(false).getAttribute(Global.SESSION_LOGIN);
        }

        return bean;
    }
}
