package treemas.handset.submitCuti;

import java.util.Date;

public class SubmitCuti {
	
	  private String nik;
	  private String nikLeader;
	  private String tglMulai;
	  private String tglSelesai;
	  private String tglKembaliKerja;
	  private String alamatCuti;
	  private String KeperluanCuti;
	  private String usrupd;	
	  private Date dtmupd;
	  private String isapproved;
	  private String noteapp;
	  private String flgKet;
	  private String usrcrt;	
	  private Date dtmcrt;
	  
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getNikLeader() {
		return nikLeader;
	}
	public void setNikLeader(String nikLeader) {
		this.nikLeader = nikLeader;
	}
	public String getTglMulai() {
		return tglMulai;
	}
	public void setTglMulai(String tglMulai) {
		this.tglMulai = tglMulai;
	}
	public String getTglSelesai() {
		return tglSelesai;
	}
	public void setTglSelesai(String tglSelesai) {
		this.tglSelesai = tglSelesai;
	}
	public String getTglKembaliKerja() {
		return tglKembaliKerja;
	}
	public void setTglKembaliKerja(String tglKembaliKerja) {
		this.tglKembaliKerja = tglKembaliKerja;
	}
	public String getAlamatCuti() {
		return alamatCuti;
	}
	public void setAlamatCuti(String alamatCuti) {
		this.alamatCuti = alamatCuti;
	}
	public String getKeperluanCuti() {
		return KeperluanCuti;
	}
	public void setKeperluanCuti(String keperluanCuti) {
		KeperluanCuti = keperluanCuti;
	}
	public String getUsrupd() {
		return usrupd;
	}
	public void setUsrupd(String usrupd) {
		this.usrupd = usrupd;
	}
	public Date getDtmupd() {
		return dtmupd;
	}
	public void setDtmupd(Date dtmupd) {
		this.dtmupd = dtmupd;
	}
	public String getIsapproved() {
		return isapproved;
	}
	public void setIsapproved(String isapproved) {
		this.isapproved = isapproved;
	}
	public String getNoteapp() {
		return noteapp;
	}
	public void setNoteapp(String noteapp) {
		this.noteapp = noteapp;
	}
	public String getFlgKet() {
		return flgKet;
	}
	public void setFlgKet(String flgKet) {
		this.flgKet = flgKet;
	}
	public String getUsrcrt() {
		return usrcrt;
	}
	public void setUsrcrt(String usrcrt) {
		this.usrcrt = usrcrt;
	}
	public Date getDtmcrt() {
		return dtmcrt;
	}
	public void setDtmcrt(Date dtmcrt) {
		this.dtmcrt = dtmcrt;
	}
	  
	  

}
