package treemas.handset.lookup;

import com.google.common.base.Strings;
import treemas.base.handset.ServletBase;
import treemas.handset.login.LoginBean;
import treemas.util.constant.PropertiesKey;
import treemas.util.cryptography.MD5;
import treemas.util.encoder.Base64;
import treemas.util.tool.ConfigurationProperties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.NoSuchAlgorithmException;

public class LookUpServlet extends ServletBase {
    private boolean enEnabled = true;
    private boolean deEnabled = true;

    private String url = "/m/lookup/";
    private String delimiter = ":";
    private String url_delimiter = "/";
    private String rkm = "RKM Android User";

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doPost(req, resp);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String enkrip = ConfigurationProperties.getInstance().get(PropertiesKey.ENCRYPT_ENABLE);
        String dekrip = ConfigurationProperties.getInstance().get(PropertiesKey.DECRYPT_ENBALE);

        this.enEnabled = ("1".equals(enkrip));
        this.deEnabled = ("1".equals(dekrip));

        String message = "User not authorized";
        PrintWriter out = resp.getWriter();
        System.out.println("Android calling " + url + ".... method get");

        String IMEI = null;
        String userName = null;
        String password = null;

        boolean isAuthorized = false;
        String token = req.getHeader("Authorization");
        if (!Strings.isNullOrEmpty(token)) {

            if (token.startsWith("Basic ")) {
                token = token.replace("Basic ", "");
                String decodeToken = new String(Base64.decode(token));
                String[] dataToken = decodeToken.split(this.delimiter);
                if (dataToken.length == 4) {

                    IMEI = dataToken[0];
                    password = dataToken[1];
                    userName = dataToken[2];
                    rkm = dataToken[3];
                    isAuthorized = true;
                }
            } else {
                isAuthorized = false;
            }
        } else {
            isAuthorized = false;
        }

        boolean isTrue = false;

        InputStream in = req.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        StringBuffer sb = new StringBuffer();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        String data = req.getRequestURI().replace(req.getContextPath(), "").replace(this.url, "").replaceAll(this.url_delimiter, "");
        String luCode = Strings.isNullOrEmpty(data) ? null : data;


//		IMEI = "352879066025843";
//		userName="COLL001";
//		password="12345678";
//		isAuthorized = true;
        if (!Strings.isNullOrEmpty(luCode) && isAuthorized) {
            isTrue = true;
        } else {
            isTrue = false;
        }

        if (isTrue) {
            try {

                LoginBean lb = new LoginBean();
                lb.setIMEI(IMEI);
                lb.setUserName(userName);
                lb.setUserPassword(MD5.getInstance().hashData(password));


//			} catch (SQLException e) {
//				e.printStackTrace();
//				this.logger.printStackTrace(e);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                this.logger.printStackTrace(e);
//			} catch (CoreException e) {
//				e.printStackTrace();
//				this.logger.printStackTrace(e);
            }

        }

        if (isTrue) {
            out.write(message);
        } else {
            String ret =
                    "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">"
                            + message + "</string>";
            System.out.println(ret);
            out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            out.append(ret);
        }

    }
}
