package treemas.handset;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import treemas.application.feature.master.approval.ApprovalBean;
import treemas.application.feature.master.auditrailTask.AuditrailTaskBean;
import treemas.base.feature.SequenceManager;
import treemas.base.handset.HandsetManagerBase;
import treemas.handset.approval.SubmitApprovalBean;
import treemas.handset.approval.SubmitApprovalBeanList;
import treemas.handset.combo.HasilBean;
import treemas.handset.combo.ListAllCombo;
import treemas.handset.combo.PhoneBean;
import treemas.handset.combo.TugasBean;
import treemas.handset.detaillist.LeaderBean;
import treemas.handset.detaillist.ListAbsen;
import treemas.handset.detaillist.ListReimburse;
import treemas.handset.detaillist.ProjectBean;
import treemas.handset.detaillist.listTimesheetBean;
import treemas.handset.list.HeaderBean;
import treemas.handset.list.ParameterBean;
import treemas.handset.list.collection.bean.CollectionBean;
import treemas.handset.list.collection.bean.CollectionData;
import treemas.handset.list.collection.bean.SingleCollection;
import treemas.handset.list.survey.SurveyBeanList;
import treemas.handset.loadlist.ListAnnouncement;
import treemas.handset.loadlist.ListKaryawan;
import treemas.handset.login.LoginBean;
import treemas.handset.newTask.SubmitNewBean;
import treemas.handset.newValey.SubmitHeaderBean;
import treemas.handset.scheme.*;
import treemas.handset.submit.SubmitAnswerBean;
import treemas.handset.submit.SubmitBeanList;
import treemas.handset.submitAbsen.ReimburseBean;
import treemas.handset.submitAbsen.SubmitAbsen;
import treemas.handset.submitAbsen.SubmitAbsenBeanList;
import treemas.handset.submitAbsen.TimesheetBean;
import treemas.handset.submitCollection.SubmitCollectionBean;
import treemas.handset.submitCollection.SubmitCollectionBeanList;
import treemas.handset.submitCuti.SubmitCuti;
import treemas.handset.submitCuti.SubmitCutiBeanList;
import treemas.handset.tracking.TrackingBean;
import treemas.util.CoreException;
import treemas.util.constant.AnswerType;
import treemas.util.constant.Global;
import treemas.util.constant.PropertiesKey;
import treemas.util.constant.SQLConstant;
import treemas.util.cryptography.MD5;
import treemas.util.encoder.Base64;
import treemas.util.format.TreemasFormatter;
import treemas.util.geofence.GeofenceBean;
import treemas.util.log.ILogger;
import treemas.util.tool.ConfigurationProperties;

import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.tomcat.util.net.URL;
import org.eclipse.birt.report.model.api.util.URIUtil;

public class HandsetManager extends HandsetManagerBase {
    public static final String STRING_SQL = SQLConstant.SQL_HANDSET_MANAGER;
    public static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    public static final String ACTIVITY_LOGIN = "LOGIN #user#:#IMEI#";
    public static final String ACTIVITY_INVALID_LOGIN = "INVALID_LOGIN #user#:#IMEI#";
    public static final String ACTIVITY_GET_LIST = "GET_TASK_LIST";
    public static final String ACTIVITY_SUBMIT_SURVEY_NEW = "SUBMIT_NEW_SURVEY #user#:#TASKID#";
    public static final String ACTIVITY_SUBMIT_SURVEY = "SUBMIT_SURVEY #user#:#TASKID#";
    public static final String ACTIVITY_SUBMIT_COLLECTION = "SUBMIT_COLLECTION #user#:#TASKID#";

    public void setActivity(String user, String activity) throws CoreException {
        String branchid = null;
        try {
            branchid = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getUserBranch", user);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
            throw new CoreException(e, ERROR_DB);
        }
        Map paramMap = new HashMap();
        paramMap.put("userId", user);
        paramMap.put("branchId", branchid);
        paramMap.put("activity", activity);

        try {
            this.ibatisSqlMap.insert(
                    this.STRING_SQL + "logMobileActivity", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

    }


    public void setActivityAndroid(String user, String param) throws CoreException {
        String branchid = null;
        this.logger.log(ILogger.LEVEL_DEBUG_LOW, "SENDING Android Activity user:" + user.toUpperCase() + " param: " + param);
        try {
            branchid = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getUserBranch", user);
        } catch (SQLException e) {
            this.logger.printStackTrace(e);
        }
        if (!Strings.isNullOrEmpty(param)) {
            String[] params = param.split("#");
            for (int i = 0; i < params.length; i++) {
                if (!Strings.isNullOrEmpty(params[i])) {
                    String[] paramBean = params[i].split("\\|");
                    if (paramBean.length == 2) {
                        String tgl = paramBean[0];
                        Date tglSql = new Date();
                        try {
                            tglSql = TreemasFormatter.parseDate(tgl, TreemasFormatter.DFORMAT_YMDHHMMSS);
                        } catch (ParseException e) {
                            tglSql = new Date();
                        }
                        String activity = paramBean[1];

                        Map paramMap = new HashMap();
                        paramMap.put("userId", user);
                        paramMap.put("branchId", branchid);
                        paramMap.put("activity", activity);
                        paramMap.put("tgl", tglSql);

                        try {
                            this.logger.log(ILogger.LEVEL_DEBUG_LOW, "INSERT Android Activity user:" + user.toUpperCase() + " param: " + paramMap);
                            this.ibatisSqlMap.insert(
                                    this.STRING_SQL + "logMobileActivityAndroid", paramMap);
                        } catch (SQLException sqle) {
                            this.logger.printStackTrace(sqle);
                        }
                    }
                }
            }
        }
    }

    public Map doLogin(LoginBean loginBean) throws CoreException,
            NoSuchAlgorithmException {
        Integer result = null;
        String resultName = null;
        Map ret = new HashMap();
        String version = "";

        Map paramMap = new HashMap();
        paramMap.put("userId", loginBean.getUserName());
        paramMap.put("password",
                MD5.getInstance().hashData(loginBean.getUserPassword()));
        paramMap.put("imei", loginBean.getIMEI());

        if (Global.LNK_SURVEY_APPS.equalsIgnoreCase(loginBean.getAppType())) {
            version = Global.VERS_SURVEY_APPS;
        } else if (Global.LNK_COLL_APPS.equalsIgnoreCase(loginBean.getAppType())) {
            version = Global.VERS_COLL_APPS;
        } else if (Global.LNK_MOBAPP_APPS.equalsIgnoreCase(loginBean.getAppType())) {
            version = Global.VERS_MOBAPP_APPS;
        } else if (Global.LNK_MSC_APPS.equalsIgnoreCase(loginBean.getAppType())) {
            version = Global.VERS_MSC_APPS;
        }

        paramMap.put("appType", loginBean.getAppType());
        paramMap.put("versApp", version);
//		paramMap.put("versSchm", "SCHM");

        try {
            result = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "login", paramMap);
            resultName = (String) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "loginName", paramMap);

            ret.put("countId", result);
            resultName = null == resultName ? null : resultName.replaceAll("~", "#");
            ret.put("param", resultName);
            if (new Integer(1).compareTo(result) == 0 || new Integer(1).compareTo(result) == 1) {
                this.setActivity(loginBean.getUserName(), ACTIVITY_LOGIN.replaceAll("#user#", loginBean.getUserName()).replaceAll("#IMEI#", loginBean.getIMEI()));
            } else {
                this.setActivity(loginBean.getUserName(), ACTIVITY_INVALID_LOGIN.replaceAll("#user#", loginBean.getUserName()).replaceAll("#IMEI#", loginBean.getIMEI()));
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

        return ret;
    }

    public String saveLocationTracking(LoginBean loginBean, String data, Boolean isTask) {
        TrackingBean bean = gson.fromJson(data, TrackingBean.class);
        if (null != bean) {
            if (Strings.isNullOrEmpty(bean.getSurveyor())) {
                bean.setSurveyor(loginBean.getUserName());
            }
        }
        try {
            SequenceManager sequence = new SequenceManager();
            boolean noSequence = Global.STRING_FALSE.equals(ConfigurationProperties.getInstance()
                    .get(PropertiesKey.DB_SEQUENCE));

            if (!isNewerThanLastLocation(bean)) {
                return data;
            }

            if (!noSequence) {
                bean.setSeqNo(new Integer(sequence.getSequenceSurveyorLocationHistory()));
            }

            if (isTask) {
                bean.setIsTask(Global.STRING_TRUE);
            } else {
                bean.setIsTask(Global.STRING_FALSE);
            }

            this.processLocation(bean);

            this.ibatisSqlMap.insert(this.STRING_SQL + "insertLocationHistory", bean);


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return gson.toJson(bean, TrackingBean.class);
    }

    private boolean isNewerThanLastLocation(TrackingBean bean)
            throws SQLException {
        boolean newer = true;
        boolean check = false;
        if (null == bean)
            check = true;
        if (!check) {
            Date lastLocationTimeStamp = (Date) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "getLastTimeStamp", bean);

            if (lastLocationTimeStamp != null) {
                Date date;
                try {
                    date = TreemasFormatter.parseDate(bean.getTimestamp(), TreemasFormatter.DFORMAT_YMDHHMMSS);
                } catch (ParseException e) {
                    date = new Date();
                }
                long trackTime = (date.getTime() / 1000) * 1000;
                long lastTrackTime = (lastLocationTimeStamp.getTime() / 1000) * 1000;

                if (lastLocationTimeStamp.equals(date)) {
                    this.logger.log(ILogger.LEVEL_DEBUG_LOW, "Last Location: " + TreemasFormatter.formatDate(lastLocationTimeStamp, TreemasFormatter.DFORMAT_YMDHHMMSS) + " EQUAL input:" + TreemasFormatter.formatDate(date, TreemasFormatter.DFORMAT_YMDHHMMSS));
                } else if (lastLocationTimeStamp.after(date)) {
                    this.logger.log(ILogger.LEVEL_DEBUG_LOW, "Last Location: " + TreemasFormatter.formatDate(lastLocationTimeStamp, TreemasFormatter.DFORMAT_YMDHHMMSS) + " AFTER input:" + TreemasFormatter.formatDate(date, TreemasFormatter.DFORMAT_YMDHHMMSS));
                } else if (lastLocationTimeStamp.before(date)) {
                    this.logger.log(ILogger.LEVEL_DEBUG_LOW, "Last Location: " + TreemasFormatter.formatDate(lastLocationTimeStamp, TreemasFormatter.DFORMAT_YMDHHMMSS) + " BEFORE input:" + TreemasFormatter.formatDate(date, TreemasFormatter.DFORMAT_YMDHHMMSS));
                }

                if (trackTime <= lastTrackTime) {
                    this.logger.log(ILogger.LEVEL_DEBUG_LOW, "Return because " + bean.getTimestamp() + " is older than " + lastLocationTimeStamp);
                    newer = false;
                }
            }
        }
        return newer;
    }

    private void processLocation(TrackingBean bean) {
        Double lat = bean.getLatitude();
        Double longi = bean.getLongitude();

        boolean isGps = false;

        if (null != lat && null != longi) {
            isGps = !(lat.doubleValue() == 0d && longi.doubleValue() == 0d) ? true : false;
        }

        if (!isGps) {
            GeofenceBean cBean = CellIdLookup.getCoordinate(bean.getMCC(), bean.getMNC(), bean.getLAC(), bean.getCellId());
            bean.setLatitude(cBean.getLatitude());
            bean.setLongitude(cBean.getLongitude());
            bean.setAccuracy(cBean.getAccuracy());
            bean.setProvider(cBean.getProvider());
            bean.setIsGps(Global.STRING_FALSE);
        } else {
            bean.setIsGps(Global.STRING_TRUE);
        }
    }


    public String getSurveyList(LoginBean loginBean, ParameterBean pBean)
            throws SQLException {
        SurveyBeanList beanList = new SurveyBeanList();
        beanList.setTotalItems(0);
        beanList.setPageOfResults(new ArrayList<HeaderBean>());

        Map paramMap = new HashMap();
        List<SchemeCheckBean> listDB = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getLastSchemeByUser", loginBean.getUserName());
        String data = "";
        int index = 0;
        for (SchemeCheckBean bean : listDB) {
            if (index == 0) {
                data = data.concat(bean.getScheme_id());
            } else {
                data = data.concat("," + bean.getScheme_id());
            }
            index++;
        }
        paramMap.put("scheme", data.split(","));
        paramMap.put("assignment_date", pBean.getPlanDate());
        paramMap.put("userId", loginBean.getUserName());

        Integer size = (Integer) this.ibatisSqlMap.queryForObject(
                this.STRING_SQL + "getSurveyListNumber", paramMap);

        if (null == size) {
            size = 0;
        }
        if (size <= 0) {
            return gson.toJson(beanList, SurveyBeanList.class);
        }
        Integer pagePerRow = pBean.getItemsPerPage();
        Integer page = pBean.getPageNumber();
        Integer totalPage = (int) Math.ceil((double) size / (double) pagePerRow);
        Integer end = pagePerRow * page;
        Integer start = end - pagePerRow;

        paramMap.put("start", start);
        paramMap.put("end", end);

        List<HeaderBean> list = this.ibatisSqlMap.queryForList(
                this.STRING_SQL + "getSurveyList", paramMap);
        if (null == list) {
            try {
                this.setActivity(loginBean.getUserName(), ACTIVITY_GET_LIST + " (List is Empty) ");
            } catch (CoreException e) {
                this.logger.printStackTrace(e);
            }
            return gson.toJson(beanList, SurveyBeanList.class);
        }

        if (list.size() > 0) {
            paramMap.put("get_date", new Date());
            paramMap.put("status", "R");

            this.ibatisSqlMap.update(this.STRING_SQL + "updateSurveyList", paramMap);
            beanList.setTotalItems(size);
            beanList.setPageOfResults(list);

        } else {
            try {
                this.setActivity(loginBean.getUserName(), ACTIVITY_GET_LIST + " (List is Empty) ");
            } catch (CoreException e) {
                this.logger.printStackTrace(e);
            }
            return gson.toJson(beanList, SurveyBeanList.class);
        }
        try {
            this.setActivity(loginBean.getUserName(), ACTIVITY_GET_LIST);
        } catch (CoreException e) {
            this.logger.printStackTrace(e);
        }
        return gson.toJson(beanList, SurveyBeanList.class);
    }

    public Integer submitHeader(LoginBean loginBean, SubmitNewBean submitNewBean)
            throws CoreException, SQLException, IOException {
        Integer result = 0;
        HeaderBean bean = submitNewBean.getHeaderBean();
        String branch = (String) this.ibatisSqlMap.queryForObject(
                this.STRING_SQL + "getUserBranch",
                loginBean.getUserName());
        Map paramMap = new HashMap();
        paramMap.put("BRANCH_ID", branch);
        paramMap.put("USER_ID", loginBean.getUserName());
        paramMap.put("CUSTOMER_NAME", bean.getNama_pemohon());
        paramMap.put("CUSTOMER_ADDRESS", bean.getAlamat_pemohon());
        paramMap.put("CUSTOMER_PHONE", bean.getTelepon());
        paramMap.put("NOTES", bean.getNotes());
        paramMap.put("CUSTOMER_HP", bean.getHP());
        paramMap.put("SCHEME_ID", bean.getScheme_id());
        paramMap.put("NIK", bean.getNik());
        paramMap.put("NOADDR", bean.getNoaddr());
        paramMap.put("ADDRESSSTREET1", bean.getAddressStreet1());
        paramMap.put("KELURAHAN", bean.getKelurahan());
        paramMap.put("KECAMATAN", bean.getKecamatan());
        paramMap.put("KABUPATEN", bean.getKabupaten());
        paramMap.put("PROPINSI", bean.getPropinsi());
        paramMap.put("CITY", bean.getCity());
        paramMap.put("NEGARA", bean.getNegara());
        paramMap.put("KODEPOS", bean.getKodePos());
        paramMap.put("GPSLONGITUDE", bean.getGpsLongitude());
        paramMap.put("GPSLATITUDE", bean.getGpsLatitude());
        
        // IRFAN 17-12-2018
        paramMap.put("RT", bean.getRt());
        paramMap.put("RW", bean.getRw());
        paramMap.put("AREAPHONE1", bean.getAreaPhone1());
        // END
        
        
        /*String address = "the white house, washington dc";
        System.out.println("addres " + address);
        Map<String, Double> coords;
        coords = OpenStreetMapUtils.getInstance().getCoordinates(address);
        System.out.println("lat " + coords.get("lat"));
        System.out.println("lon " + coords.get("lon"));
        paramMap.put("GPSLONGITUDE", coords.get("lon"));
        paramMap.put("GPSLATITUDE", coords.get("lat"));*/
        
  
        

        Integer resultSave = (Integer) this.ibatisSqlMap.insert(
                this.STRING_SQL + "insertMSASurvey", paramMap);
        Integer mobile_assignment_id = (Integer) this.ibatisSqlMap
                .queryForObject(
                        this.STRING_SQL + "selectNewMSASurvey",
                        paramMap);
        SubmitBeanList bList = new SubmitBeanList();

        bList.setMobile_assignment_id(mobile_assignment_id);
        bList.setAnswerList(submitNewBean.getAnswerList());
        bList.setTrackingBean(submitNewBean.getTrackingBean());
        this.submitResult(loginBean, bList, true);
        return result;
    }

//    public Integer submitResult(LoginBean loginBean, SubmitBeanList beanList,
//                                boolean isNew) throws CoreException, SQLException,
//            IOException {
//        Integer result = 0;
//
//        Integer mobile_assignment_id = beanList.getMobile_assignment_id();
//        String user = loginBean.getUserName();
//
//        Map param = new HashMap();
//        param.put("userId", user);
//        param.put("mai", mobile_assignment_id);
//        Integer count = (Integer) this.ibatisSqlMap.queryForObject(
//                this.STRING_SQL + "checkMSAUser", param);
//        this.logger.log(ILogger.LEVEL_DEBUG_LOW, "Submit Counting is " + count
//                + " MSA " + mobile_assignment_id + " MSA USER " + user);
//        System.out.println("ini submit result: " + count);
//        if (count > 0) {
//            try {
//                String dataTracking = gson.toJson(beanList.getTrackingBean(),
//                        TrackingBean.class);
//                this.saveLocationTracking(loginBean, dataTracking, true);
//                this.ibatisSqlMap.startTransaction();
//                boolean isBekas = beanList.isBekas();
//
//                List<SubmitAnswerBean> list = beanList.getAnswerList();
//                for (SubmitAnswerBean sab : list) {
//                    Map paramSubmit = new HashMap();
//                    Integer question_id = sab.getQuestion_id();
//                    Integer question_group_id = sab.getQuestion_group_id();
//                    String answer_type_id = sab.getAnswer_type_id();
//                    Integer option_answer_id = null;
//                    String question_label = null;
//                    String use_min = null;
//
//                    // for combo option 007,006
//                    Map paramMap = new HashMap();
//                    paramMap.put("question_group_id", question_group_id);
//                    paramMap.put("question_id", question_id);
//                    question_label = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getQuestionLabel", paramMap);
//
//                    paramSubmit.put("mobile_assignment_id", mobile_assignment_id);
//                    paramSubmit.put("question_group_id", question_group_id);
//                    paramSubmit.put("question_id", question_id);
//
//                    String answerCombo = sab.getCombo_answer();
//                    String answerText = sab.getText_answer();
//                    String answerImage = sab.getImage_base64();
//                    Double longitude = null;
//                    Double latitude = null;
//                    Boolean isGPS = null;
//                    byte[] imageBlob = null;
//                    String timestamp = null;
//                    String fileName = null;
//                    Integer mnc = null;
//                    Integer mcc = null;
//                    Integer lac = null;
//                    Integer cellid = null;
//                    Integer accuracy = null;
//                    String provider = null;
//                    // sab.getLatitude();
//                    // sab.getLongitude();
//
//                    if (answer_type_id.equalsIgnoreCase(AnswerType.RADIO)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.RADIO_WITH_DESCRIPTION)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.DROPDOWN)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.DROPDOWN_WITH_DESCRIPTION)) {
//                        paramMap.put("question_label", answerCombo);
//                        option_answer_id = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getOptionId", paramMap);
//
//                    } else if (answer_type_id.equalsIgnoreCase(AnswerType.DRAWING)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.SIGNATURE)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_TIME)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_GPS)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)) {
//                        imageBlob = Base64.decode(answerImage);
//                        String imagePath = ConfigurationProperties
//                                .getInstance().get(
//                                        PropertiesKey.ANSWER_IMAGE_PATH);
//                        if (imagePath != null) {
//                            fileName = this.writeImageToFile(imagePath,
//                                    imageBlob,
//                                    mobile_assignment_id.toString());
//                        }
//
//                        if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_TIME)
//                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)
//                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)) {
//                            timestamp = sab.getTimestamp();
//                        }
//
//                        //GEODATA
//                        if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA)
//                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)) {
//                            mcc = sab.getMcc();
//                            mnc = sab.getMnc();
//                            lac = sab.getLac();
//                            cellid = sab.getCellid();
//                            isGPS = false;
//                            GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
//                            latitude = cBean.getLatitude();
//                            longitude = cBean.getLongitude();
//                            accuracy = cBean.getAccuracy();
//                            provider = cBean.getProvider();
//                        } else if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_GPS)
//                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)) {
//                            latitude = sab.getLatitude();
//                            longitude = sab.getLongitude();
//                            accuracy = sab.getAccuracy();
//                            provider = sab.getProvider();
//                            isGPS = true;
//                        } else if (answer_type_id.equalsIgnoreCase(AnswerType.DRAWING)
//                                || answer_type_id.equalsIgnoreCase(AnswerType.SIGNATURE)) {
//                            if (null != sab.getLatitude() && null != sab.getLongitude()) {
//                                latitude = sab.getLatitude();
//                                longitude = sab.getLongitude();
//                                accuracy = sab.getAccuracy();
//                                provider = sab.getProvider();
//                                isGPS = true;
//                            } else if (null != sab.getMnc() && null != sab.getMcc() && null != sab.getLac() && null != sab.getCellid()) {
//                                mcc = sab.getMcc();
//                                mnc = sab.getMnc();
//                                lac = sab.getLac();
//                                cellid = sab.getCellid();
//                                isGPS = false;
//                                GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
//                                latitude = cBean.getLatitude();
//                                longitude = cBean.getLongitude();
//                                accuracy = cBean.getAccuracy();
//                                provider = cBean.getProvider();
//                            }
//                        }
//
//                    } else if (answer_type_id.equalsIgnoreCase(AnswerType.GPS_TIME)
//                            || answer_type_id.equalsIgnoreCase(AnswerType.GEODATA_TIME)) {
//                        timestamp = sab.getTimestamp();
//                        if (answer_type_id.equalsIgnoreCase(AnswerType.GEODATA_TIME)) {
//                            mcc = sab.getMcc();
//                            mnc = sab.getMnc();
//                            lac = sab.getLac();
//                            cellid = sab.getCellid();
//                            isGPS = false;
//                            GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
//                            latitude = cBean.getLatitude();
//                            longitude = cBean.getLongitude();
//                            accuracy = cBean.getAccuracy();
//                            provider = cBean.getProvider();
//                        } else {
//                            latitude = sab.getLatitude();
//                            longitude = sab.getLongitude();
//                            accuracy = sab.getAccuracy();
//                            provider = sab.getProvider();
//                            isGPS = true;
//                        }
//
//                    }
//
//                    paramSubmit.put("text_answer", answerText);
//                    paramSubmit.put("option_answer_id", option_answer_id);
//                    paramSubmit.put("option_answer_text", answerCombo);
//                    paramSubmit.put("gps_latitude", latitude);
//                    paramSubmit.put("gps_longitude", longitude);
//                    paramSubmit.put("is_gps", isGPS);
//                    paramSubmit.put("image_path", fileName);
//                    paramSubmit.put("image_blob", imageBlob);
//                    paramSubmit.put("timestamp", timestamp);
//                    paramSubmit.put("question_text", question_label);
//                    paramSubmit.put("mnc", mnc);
//                    paramSubmit.put("mcc", mcc);
//                    paramSubmit.put("lac", lac);
//                    paramSubmit.put("cellid", cellid);
//                    paramSubmit.put("accuracy", accuracy);
//                    paramSubmit.put("provider", provider);
//
//                    this.ibatisSqlMap.insert(
//                            this.STRING_SQL + "insertSurveyDetail",
//                            paramSubmit);
//
//                }
//                Map paramUpdate = new HashMap();
//                paramUpdate.put("endDate", TreemasFormatter.formatDate(new Date(),
//                        TreemasFormatter.DFORMAT_YMDHHMMSS));
//                paramUpdate.put("status", Global.Submitted);
//                paramUpdate.put("MSAID", mobile_assignment_id);
//                this.ibatisSqlMap.update(
//                        this.STRING_SQL + "updateMSA", paramUpdate);
//                String activity = null;
//                if (isNew)
//                    activity = ACTIVITY_SUBMIT_SURVEY_NEW.replaceAll("#user#",
//                            loginBean.getUserName()).replaceAll("#TASKID#",
//                            String.valueOf(mobile_assignment_id));
//                else
//                    activity = ACTIVITY_SUBMIT_SURVEY.replaceAll("#user#",
//                            loginBean.getUserName()).replaceAll("#TASKID#",
//                            String.valueOf(mobile_assignment_id));
//                this.setActivity(loginBean.getUserName(), activity);
//                this.ibatisSqlMap.commitTransaction();
//            } catch (SQLException sqle) {
//                this.logger.printStackTrace(sqle);
//                throw new CoreException(sqle, ERROR_DB);
//            } finally {
//                this.ibatisSqlMap.endTransaction();
//            }
//
//        } else {
//            result = -1;
//        }
//        return result;
//    }

    public Integer submitResult(LoginBean loginBean, SubmitBeanList beanList,
                                boolean isNew) throws CoreException, SQLException,
            IOException {
        Integer result = 0;

        Integer mobile_assignment_id = beanList.getMobile_assignment_id();
        String user = loginBean.getUserName();

        Map param = new HashMap();
        param.put("userId", user);
        param.put("mai", mobile_assignment_id);
        Integer count = (Integer) this.ibatisSqlMap.queryForObject(
                this.STRING_SQL + "checkMSAUser", param);
        this.logger.log(ILogger.LEVEL_DEBUG_LOW, "Submit Counting is " + count
                + " MSA " + mobile_assignment_id + " MSA USER " + user);
        System.out.println("ini submit result: " + count);
        if (count > 0) {
            try {
                String dataTracking = gson.toJson(beanList.getTrackingBean(),
                        TrackingBean.class);
                this.saveLocationTracking(loginBean, dataTracking, true);
                this.ibatisSqlMap.startTransaction();
                boolean isBekas = beanList.isBekas();

                List<SubmitAnswerBean> list = beanList.getAnswerList();
                for (SubmitAnswerBean sab : list) {
                    Map paramSubmit = new HashMap();
                    Integer question_id = sab.getQuestion_id();
                    Integer question_group_id = sab.getQuestion_group_id();
                    String answer_type_id = sab.getAnswer_type_id();
                    Integer option_answer_id = null;
                    String question_label = null;
                    String use_min = null;

                    // for combo option 007,006
                    Map paramMap = new HashMap();
                    paramMap.put("question_group_id", question_group_id);
                    paramMap.put("question_id", question_id);
                    question_label = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getQuestionLabel", paramMap);

                    paramSubmit.put("mobile_assignment_id", mobile_assignment_id);
                    paramSubmit.put("question_group_id", question_group_id);
                    paramSubmit.put("question_id", question_id);

                    String answerCombo = sab.getCombo_answer();
                    String answerText = sab.getText_answer();
                    String answerImage = sab.getImage_base64();
                    Double longitude = null;
                    Double latitude = null;
                    Boolean isGPS = null;
                    byte[] imageBlob = null;
                    String timestamp = null;
                    String fileName = null;
                    Integer mnc = null;
                    Integer mcc = null;
                    Integer lac = null;
                    Integer cellid = null;
                    Integer accuracy = null;
                    String provider = null;
                    // sab.getLatitude();
                    // sab.getLongitude();

                    if (answer_type_id.equalsIgnoreCase(AnswerType.RADIO)
                            || answer_type_id.equalsIgnoreCase(AnswerType.RADIO_WITH_DESCRIPTION)
                            || answer_type_id.equalsIgnoreCase(AnswerType.DROPDOWN)
                            || answer_type_id.equalsIgnoreCase(AnswerType.DROPDOWN_WITH_DESCRIPTION)) {
                  //START IRFAN 27-02-19  	
                        if (!question_id.equals(1736) || question_id != 1736){
                        	paramMap.put("question_label", answerCombo);
                        	option_answer_id = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getOptionId", paramMap);    	
                        }
                        else{
                        	
                        	Map paramQuestion = new HashMap();
                        	String Branch = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getBranch", user);
                        	paramQuestion.put("branch", Branch);
                        	paramQuestion.put("question_label", answerCombo);
                        	option_answer_id = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getOptionIdBranch",paramQuestion );
                        }
                  //END IRFAN 27-02-19
                    } else if (answer_type_id.equalsIgnoreCase(AnswerType.DRAWING)
                            || answer_type_id.equalsIgnoreCase(AnswerType.SIGNATURE)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_TIME)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_GPS)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)) {
                        imageBlob = Base64.decode(answerImage);
                        String imagePath = ConfigurationProperties
                                .getInstance().get(
                                        PropertiesKey.ANSWER_IMAGE_PATH);
                        if (imagePath != null) {
                            fileName = this.writeImageToFile(imagePath,
                                    imageBlob,
                                    mobile_assignment_id.toString());
                        }

                        if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_TIME)
                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)
                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)) {
                            timestamp = sab.getTimestamp();
                        }

                        //GEODATA
                        if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA)
                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)) {
                            mcc = sab.getMcc();
                            mnc = sab.getMnc();
                            lac = sab.getLac();
                            cellid = sab.getCellid();
                            isGPS = false;
                            GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
                            latitude = cBean.getLatitude();
                            longitude = cBean.getLongitude();
                            accuracy = cBean.getAccuracy();
                            provider = cBean.getProvider();
                        } else if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_GPS)
                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)) {
                            latitude = sab.getLatitude();
                            longitude = sab.getLongitude();
                            accuracy = sab.getAccuracy();
                            provider = sab.getProvider();
                            isGPS = true;
                        } else if (answer_type_id.equalsIgnoreCase(AnswerType.DRAWING)
                                || answer_type_id.equalsIgnoreCase(AnswerType.SIGNATURE)) {
                            if (null != sab.getLatitude() && null != sab.getLongitude()) {
                                latitude = sab.getLatitude();
                                longitude = sab.getLongitude();
                                accuracy = sab.getAccuracy();
                                provider = sab.getProvider();
                                isGPS = true;
                            } else if (null != sab.getMnc() && null != sab.getMcc() && null != sab.getLac() && null != sab.getCellid()) {
                                mcc = sab.getMcc();
                                mnc = sab.getMnc();
                                lac = sab.getLac();
                                cellid = sab.getCellid();
                                isGPS = false;
                                GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
                                latitude = cBean.getLatitude();
                                longitude = cBean.getLongitude();
                                accuracy = cBean.getAccuracy();
                                provider = cBean.getProvider();
                            }
                        }

                    } else if (answer_type_id.equalsIgnoreCase(AnswerType.GPS_TIME)
                            || answer_type_id.equalsIgnoreCase(AnswerType.GEODATA_TIME)) {
                        timestamp = sab.getTimestamp();
                        if (answer_type_id.equalsIgnoreCase(AnswerType.GEODATA_TIME)) {
                            mcc = sab.getMcc();
                            mnc = sab.getMnc();
                            lac = sab.getLac();
                            cellid = sab.getCellid();
                            isGPS = false;
                            GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
                            latitude = cBean.getLatitude();
                            longitude = cBean.getLongitude();
                            accuracy = cBean.getAccuracy();
                            provider = cBean.getProvider();
                        } else {
                            latitude = sab.getLatitude();
                            longitude = sab.getLongitude();
                            accuracy = sab.getAccuracy();
                            provider = sab.getProvider();
                            isGPS = true;
                        }

                    }

                    paramSubmit.put("text_answer", answerText);
                    paramSubmit.put("option_answer_id", option_answer_id);
                    paramSubmit.put("option_answer_text", answerCombo);
                    paramSubmit.put("gps_latitude", latitude);
                    paramSubmit.put("gps_longitude", longitude);
                    paramSubmit.put("is_gps", isGPS);
                    paramSubmit.put("image_path", fileName);
                    paramSubmit.put("image_blob", imageBlob);
                    paramSubmit.put("timestamp", timestamp);
                    paramSubmit.put("question_text", question_label);
                    paramSubmit.put("mnc", mnc);
                    paramSubmit.put("mcc", mcc);
                    paramSubmit.put("lac", lac);
                    paramSubmit.put("cellid", cellid);
                    paramSubmit.put("accuracy", accuracy);
                    paramSubmit.put("provider", provider);

                    this.ibatisSqlMap.insert(
                            this.STRING_SQL + "insertSurveyDetail",
                            paramSubmit);

                }
                Map paramUpdate = new HashMap();
                paramUpdate.put("endDate", TreemasFormatter.formatDate(new Date(),
                        TreemasFormatter.DFORMAT_YMDHHMMSS));
                paramUpdate.put("status", Global.Submitted);
                paramUpdate.put("MSAID", mobile_assignment_id);
                this.ibatisSqlMap.update(
                        this.STRING_SQL + "updateMSA", paramUpdate);
                String activity = null;
                if (isNew)
                    activity = ACTIVITY_SUBMIT_SURVEY_NEW.replaceAll("#user#",loginBean.getUserName()).replaceAll("#TASKID#", String.valueOf(mobile_assignment_id));
                else
                    activity = ACTIVITY_SUBMIT_SURVEY.replaceAll("#user#",
                            loginBean.getUserName()).replaceAll("#TASKID#",
                            String.valueOf(mobile_assignment_id));
                this.setActivity(loginBean.getUserName(), activity);
             //   this.setActivity(user, activity);
                this.ibatisSqlMap.commitTransaction();
            } catch (SQLException sqle) {
                this.logger.printStackTrace(sqle);
                throw new CoreException(sqle, ERROR_DB);
            } finally {
                this.ibatisSqlMap.endTransaction();
            }

        } else {
            try {
                String dataTracking = gson.toJson(beanList.getTrackingBean(),
                        TrackingBean.class);
                this.saveLocationTracking(loginBean, dataTracking, true);
                this.ibatisSqlMap.startTransaction();
                boolean isBekas = beanList.isBekas();

                List<SubmitAnswerBean> list = beanList.getAnswerList();
                for (SubmitAnswerBean sab : list) {
                    Map paramSubmit = new HashMap();
                    Integer question_id = sab.getQuestion_id();
                    Integer question_group_id = sab.getQuestion_group_id();
                    String answer_type_id = sab.getAnswer_type_id();
                    Integer option_answer_id = null;
                    String question_label = null;
                    String use_min = null;

                    // for combo option 007,006
                    Map paramMap = new HashMap();
                    paramMap.put("question_group_id", question_group_id);
                    paramMap.put("question_id", question_id);
                    question_label = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getQuestionLabel", paramMap);

                    paramSubmit.put("mobile_assignment_id", mobile_assignment_id);
                    paramSubmit.put("question_group_id", question_group_id);
                    paramSubmit.put("question_id", question_id);

                    String answerCombo = sab.getCombo_answer();
                    String answerText = sab.getText_answer();
                    String answerImage = sab.getImage_base64();
                    Double longitude = null;
                    Double latitude = null;
                    Boolean isGPS = null;
                    byte[] imageBlob = null;
                    String timestamp = null;
                    String fileName = null;
                    Integer mnc = null;
                    Integer mcc = null;
                    Integer lac = null;
                    Integer cellid = null;
                    Integer accuracy = null;
                    String provider = null;
                    // sab.getLatitude();
                    // sab.getLongitude();

                    if (answer_type_id.equalsIgnoreCase(AnswerType.RADIO)
                            || answer_type_id.equalsIgnoreCase(AnswerType.RADIO_WITH_DESCRIPTION)
                            || answer_type_id.equalsIgnoreCase(AnswerType.DROPDOWN)
                            || answer_type_id.equalsIgnoreCase(AnswerType.DROPDOWN_WITH_DESCRIPTION)) {
                       /* paramMap.put("question_label", answerCombo);
                        option_answer_id = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getOptionId", paramMap);*/
          // IRFAN 27-02-2019             
                      if (!question_id.equals(1736) || question_id != 1736){
                        	paramMap.put("question_label", answerCombo);
                        	option_answer_id = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getOptionId", paramMap);    	
                        }
                        else{
                        	//paramMap.put("user", user);
                        	Map paramQuestion = new HashMap();
                        	String Branch = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getBranch", user);
                        	paramQuestion.put("branch", Branch);
                        	paramQuestion.put("question_label", answerCombo);
                        	option_answer_id = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getOptionIdBranch", paramQuestion );
                        }
          // END IRFAN 27-02-2019                            
                    } else if (answer_type_id.equalsIgnoreCase(AnswerType.DRAWING)
                            || answer_type_id.equalsIgnoreCase(AnswerType.SIGNATURE)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_TIME)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_GPS)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)) {
                        imageBlob = Base64.decode(answerImage);
                        String imagePath = ConfigurationProperties
                                .getInstance().get(
                                        PropertiesKey.ANSWER_IMAGE_PATH);
                        if (imagePath != null) {
                            fileName = this.writeImageToFile(imagePath,
                                    imageBlob,
                                    mobile_assignment_id.toString());
                        }

                        if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_TIME)
                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)
                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)) {
                            timestamp = sab.getTimestamp();
                        }

                        //GEODATA
                        if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA)
                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)) {
                            mcc = sab.getMcc();
                            mnc = sab.getMnc();
                            lac = sab.getLac();
                            cellid = sab.getCellid();
                            isGPS = false;
                            GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
                            latitude = cBean.getLatitude();
                            longitude = cBean.getLongitude();
                            accuracy = cBean.getAccuracy();
                            provider = cBean.getProvider();
                        } else if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_GPS)
                                || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)) {
                            latitude = sab.getLatitude();
                            longitude = sab.getLongitude();
                            accuracy = sab.getAccuracy();
                            provider = sab.getProvider();
                            isGPS = true;
                        } else if (answer_type_id.equalsIgnoreCase(AnswerType.DRAWING)
                                || answer_type_id.equalsIgnoreCase(AnswerType.SIGNATURE)) {
                            if (null != sab.getLatitude() && null != sab.getLongitude()) {
                                latitude = sab.getLatitude();
                                longitude = sab.getLongitude();
                                accuracy = sab.getAccuracy();
                                provider = sab.getProvider();
                                isGPS = true;
                            } else if (null != sab.getMnc() && null != sab.getMcc() && null != sab.getLac() && null != sab.getCellid()) {
                                mcc = sab.getMcc();
                                mnc = sab.getMnc();
                                lac = sab.getLac();
                                cellid = sab.getCellid();
                                isGPS = false;
                                GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
                                latitude = cBean.getLatitude();
                                longitude = cBean.getLongitude();
                                accuracy = cBean.getAccuracy();
                                provider = cBean.getProvider();
                            }
                        }

                    } else if (answer_type_id.equalsIgnoreCase(AnswerType.GPS_TIME)
                            || answer_type_id.equalsIgnoreCase(AnswerType.GEODATA_TIME)) {
                        timestamp = sab.getTimestamp();
                        if (answer_type_id.equalsIgnoreCase(AnswerType.GEODATA_TIME)) {
                            mcc = sab.getMcc();
                            mnc = sab.getMnc();
                            lac = sab.getLac();
                            cellid = sab.getCellid();
                            isGPS = false;
                            GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
                            latitude = cBean.getLatitude();
                            longitude = cBean.getLongitude();
                            accuracy = cBean.getAccuracy();
                            provider = cBean.getProvider();
                        } else {
                            latitude = sab.getLatitude();
                            longitude = sab.getLongitude();
                            accuracy = sab.getAccuracy();
                            provider = sab.getProvider();
                            isGPS = true;
                        }

                    }

                    paramSubmit.put("text_answer", answerText);
                    paramSubmit.put("option_answer_id", option_answer_id);
                    paramSubmit.put("option_answer_text", answerCombo);
                    paramSubmit.put("gps_latitude", latitude);
                    paramSubmit.put("gps_longitude", longitude);
                    paramSubmit.put("is_gps", isGPS);
                    paramSubmit.put("image_path", fileName);
                    paramSubmit.put("image_blob", imageBlob);
                    paramSubmit.put("timestamp", timestamp);
                    paramSubmit.put("question_text", question_label);
                    paramSubmit.put("mnc", mnc);
                    paramSubmit.put("mcc", mcc);
                    paramSubmit.put("lac", lac);
                    paramSubmit.put("cellid", cellid);
                    paramSubmit.put("accuracy", accuracy);
                    paramSubmit.put("provider", provider);

                    this.ibatisSqlMap.insert(
                            this.STRING_SQL + "insertSurveyDetailTmp",
                            paramSubmit);
                }

                this.logger.log(ILogger.LEVEL_DEBUG_LOW, "=====" + mobile_assignment_id + "::INSERT INTO TEMPORARY ======");

                Map paramUpdate = new HashMap();
                paramUpdate.put("endDate", TreemasFormatter.formatDate(new Date(),
                        TreemasFormatter.DFORMAT_YMDHHMMSS));
                paramUpdate.put("status", Global.Submitted);
                paramUpdate.put("MSAID", mobile_assignment_id);
                this.ibatisSqlMap.update(
                        this.STRING_SQL + "updateMSA", paramUpdate);
                String activity = null;
                if (isNew)
                    activity = ACTIVITY_SUBMIT_SURVEY_NEW.replaceAll("#user#",
                            loginBean.getUserName()).replaceAll("#TASKID#",
                            String.valueOf(mobile_assignment_id));
                else
                    activity = ACTIVITY_SUBMIT_SURVEY.replaceAll("#user#",
                            loginBean.getUserName()).replaceAll("#TASKID#",
                            String.valueOf(mobile_assignment_id));
                this.setActivity(loginBean.getUserName(), activity);
                this.ibatisSqlMap.commitTransaction();
            } catch (SQLException sqle) {
                this.logger.printStackTrace(sqle);
                throw new CoreException(sqle, ERROR_DB);
            } finally {
                this.ibatisSqlMap.endTransaction();
            }
        }
        return result;
    }


    private String writeImageToFile(String imagePath, byte[] image,
                                    String surveyId) throws IOException {
        String dateFormat = "yyyy-MM-dd";
        String date = TreemasFormatter.formatDate(new Date(), dateFormat);

        imagePath = imagePath + date + System.getProperty("file.separator");

        FileOutputStream fos = null;
        File directory = new File(imagePath);

        if (!directory.exists())
            directory.mkdirs();

        StringBuffer imageFile = new StringBuffer().append(imagePath)
                .append(surveyId).append("_").append(new Date().getTime())
                .append(".jpg");

        this.logger.log(ILogger.LEVEL_DEBUG_LOW, "Saving image file: "
                + imageFile);
        fos = new FileOutputStream(imageFile.toString());
        fos.write(image);
        fos.close();
        this.logger.log(ILogger.LEVEL_DEBUG_LOW, "Saved image file: "
                + imageFile);

        return imageFile.toString();
    }

    public String getAllList() throws SQLException {
        String result = "";
        Map param = new HashMap();
        param.put("question_group_id", 84);
        param.put("question_id", 1681);
        List listCombo = this.ibatisSqlMap.queryForList(
                this.STRING_SQL + "getComboDetail", param);
        List<TugasBean> list = new ArrayList<TugasBean>();
        List<PhoneBean> listPhone = new ArrayList<PhoneBean>();
        for (Object bean : listCombo) {
            HasilBean object = (HasilBean) bean;
            TugasBean tbean = new TugasBean();
            tbean.setQuestion_group_id(object.getQuestion_group_id());
            tbean.setQuestion_id(object.getQuestion_id());
            tbean.setQuestion_label(object.getQuestion_label());

            Map paramInner = new HashMap();
            paramInner.put("question_group_id", 84);

            if ("TELEPON".equalsIgnoreCase(object.getQuestion_label())) {
                paramInner.put("question_id", 1680);
            } else if ("AMBIL PEMBAYARAN".equalsIgnoreCase(object
                    .getQuestion_label())) {
                paramInner.put("question_id", 1682);
            } else if ("KUNJUNGAN".equalsIgnoreCase(object.getQuestion_label())) {
                paramInner.put("question_id", 1683);
            }
            // 85 1690
            List listTugas = this.ibatisSqlMap.queryForList(
                    this.STRING_SQL + "getComboDetail", paramInner);
            tbean.setList(listTugas);
            list.add(tbean);
        }

        Map paramInner = new HashMap();
        paramInner.put("question_group_id", 85);
        paramInner.put("question_id", 1690);
        List listP = this.ibatisSqlMap.queryForList(
                this.STRING_SQL + "getComboDetail", paramInner);
        for (Object o : listP) {
            HasilBean bean = (HasilBean) o;
            PhoneBean p = new PhoneBean(bean.getQuestion_group_id(),
                    bean.getQuestion_id(), bean.getQuestion_label());
            listPhone.add(p);
        }

        ListAllCombo lac = new ListAllCombo();
        lac.setListTugas(list);
        lac.setListPhone(listPhone);

        result = gson.toJson(lac, ListAllCombo.class);
        return result;
    }

    public String getCollectionList(LoginBean loginBean,
                                    ParameterBean parameterBean) throws SQLException {
        String result = "";
        Map param = new HashMap();
        List<SchemeCheckBean> listDB = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getLastSchemeByUser", loginBean.getUserName());
        String data = "";
        int index = 0;
        for (SchemeCheckBean bean : listDB) {
            if (index == 0) {
                data = data.concat(bean.getScheme_id());
            } else {
                data = data.concat("," + bean.getScheme_id());
            }
            index++;
        }
        param.put("scheme", data.split(","));
        param.put("assignment_date", parameterBean.getPlanDate());
        param.put("userId", loginBean.getUserName());

        Integer size = (Integer) this.ibatisSqlMap.queryForObject(
                this.STRING_SQL + "getSurveyListNumber", param);

        if (null == size) {
            size = 0;
        }

        Integer pagePerRow = parameterBean.getItemsPerPage();
        Integer page = parameterBean.getPageNumber();
        Integer totalPage = (int) Math
                .ceil((double) size / (double) pagePerRow);
        Integer end = pagePerRow * page;
        Integer start = end - pagePerRow;

        param.put("start", start);
        param.put("end", end);

        List<HeaderBean> list = this.ibatisSqlMap.queryForList(
                this.STRING_SQL + "getSurveyList", param);

        CollectionBean collBean = new CollectionBean();
        if (list.size() > 0) {
            List<SingleCollection> listColl = new ArrayList<SingleCollection>();
            for (HeaderBean bean : list) {
                SingleCollection beanColl = new SingleCollection(bean);
                Map paramInner = new HashMap();
                paramInner.put("mobile_assignment_id", bean.getId());

                List<CollectionData> listData = this.ibatisSqlMap.queryForList(
                        this.STRING_SQL + "getOldDataSurvey", paramInner);
                beanColl.setListCollectionData(listData);
                listColl.add(beanColl);
            }
            param.put("get_date", new Date());
            param.put("status", "R");
            this.ibatisSqlMap.update(this.STRING_SQL + "updateSurveyList", param);
            collBean.setTotalItems(size);
            collBean.setPageOfResults(listColl);
        } else {
            try {
                this.setActivity(loginBean.getUserName(), ACTIVITY_GET_LIST + " (List is Empty) ");
            } catch (CoreException e) {
                this.logger.printStackTrace(e);
            }
        }

        result = gson.toJson(collBean, CollectionBean.class);
        return result;
    }

    public Integer submitResultCollection(LoginBean loginBean,
                                          SubmitCollectionBeanList beanList) throws CoreException,
            SQLException, IOException {
        Integer result = 0;

        Integer mobile_assignment_id = beanList.getMobile_assignment_id();
//		System.out.println("========MobileAssgId========== || "+mobile_assignment_id);

//		MODERATE DAWAM 03052017		
        try {
            String dataTracking = gson.toJson(beanList.getTrackingBean(), TrackingBean.class);
            this.saveLocationTracking(loginBean, dataTracking, true);
            this.doCallMove(mobile_assignment_id);
            this.ibatisSqlMap.startTransaction();
            boolean isBekas = beanList.isBekas();
//			MODERATE DAWAM
            Integer cekMaid = (Integer) this.ibatisSqlMap.queryForObject(
                    this.STRING_SQL + "cekMobileAssignmentId", beanList.getMobile_assignment_id());
            System.out.println("ini submit result collection: " + cekMaid);

            String insertSurveyDetail = "";
            if (cekMaid > 0) {
                insertSurveyDetail = "insertSurveyDetail";
            } else {
                insertSurveyDetail = "insertSurveyDetailTmp";
                this.logger.log(ILogger.LEVEL_DEBUG_LOW, "=====" + mobile_assignment_id + "::INSERT INTO TEMPORARY ======");

            }
//			END MODERATE DAWAM 03052017

            List<SubmitCollectionBean> list = beanList.getAnswerList();
            for (SubmitCollectionBean sab : list) {
                Map paramSubmit = new HashMap();

                Integer question_id = sab.getQuestion_id();
                Integer question_group_id = sab.getQuestion_group_id();
                String answer_type_id = sab.getAnswer_type_id();

                Integer option_answer_id = null;
                String question_label = null;

                Map paramMap = new HashMap();
                paramMap.put("question_group_id", question_group_id);
                paramMap.put("question_id", question_id);
                question_label = (String) this.ibatisSqlMap
                        .queryForObject(
                                this.STRING_SQL + "getQuestionLabel",
                                paramMap);

                paramSubmit.put("mobile_assignment_id", mobile_assignment_id);
                paramSubmit.put("question_group_id", question_group_id);
                paramSubmit.put("question_id", question_id);

                String answerCombo = sab.getCombo_answer();
                String answerText = sab.getText_answer();
                String answerImage = sab.getImage_base64();
                Double longitude = null;
                Double latitude = null;
                Boolean isGPS = null;
                byte[] imageBlob = null;
                String timestamp = null;
                String fileName = null;
                Integer mnc = null;
                Integer mcc = null;
                Integer lac = null;
                Integer cellid = null;
                Integer accuracy = null;
                String provider = null;

                if (answer_type_id.equalsIgnoreCase(AnswerType.RADIO)
                        || answer_type_id.equalsIgnoreCase(AnswerType.RADIO_WITH_DESCRIPTION)
                        || answer_type_id.equalsIgnoreCase(AnswerType.DROPDOWN)
                        || answer_type_id.equalsIgnoreCase(AnswerType.DROPDOWN_WITH_DESCRIPTION)) {
                    paramMap.put("question_label", answerCombo);
                    option_answer_id = (Integer) this.ibatisSqlMap
                            .queryForObject(
                                    this.STRING_SQL + "getOptionId",
                                    paramMap);

                } else if (answer_type_id.equalsIgnoreCase(AnswerType.DRAWING)
                        || answer_type_id.equalsIgnoreCase(AnswerType.SIGNATURE)
                        || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE)
                        || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_TIME)
                        || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_GPS)
                        || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)
                        || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA)
                        || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)) {
                    imageBlob = Base64.decode(answerImage);
                    String imagePath = ConfigurationProperties
                            .getInstance().get(
                                    PropertiesKey.ANSWER_IMAGE_PATH);
                    if (imagePath != null) {
                        fileName = this.writeImageToFile(imagePath,
                                imageBlob,
                                mobile_assignment_id.toString());
                    }

                    if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_TIME)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)) {
                        timestamp = sab.getTimestamp();
                    }

                    //GEODATA
                    if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_TIME)) {
                        mcc = sab.getMcc();
                        mnc = sab.getMnc();
                        lac = sab.getLac();
                        cellid = sab.getCellid();
                        isGPS = false;
                        GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
                        latitude = cBean.getLatitude();
                        longitude = cBean.getLongitude();
                        accuracy = cBean.getAccuracy();
                        provider = cBean.getProvider();
                    } else if (answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GEODATA_GPS)
                            || answer_type_id.equalsIgnoreCase(AnswerType.IMAGE_WITH_GPS_TIME)) {
                        latitude = sab.getLatitude();
                        longitude = sab.getLongitude();
                        accuracy = sab.getAccuracy();
                        provider = sab.getProvider();
                        isGPS = true;
                    } else if (answer_type_id.equalsIgnoreCase(AnswerType.DRAWING)
                            || answer_type_id.equalsIgnoreCase(AnswerType.SIGNATURE)) {
                        if (null != sab.getLatitude() && null != sab.getLongitude()) {
                            latitude = sab.getLatitude();
                            longitude = sab.getLongitude();
                            accuracy = sab.getAccuracy();
                            provider = sab.getProvider();
                            isGPS = true;
                        } else if (null != sab.getMnc() && null != sab.getMcc() && null != sab.getLac() && null != sab.getCellid()) {
                            mcc = sab.getMcc();
                            mnc = sab.getMnc();
                            lac = sab.getLac();
                            cellid = sab.getCellid();
                            isGPS = false;
                            GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
                            latitude = cBean.getLatitude();
                            longitude = cBean.getLongitude();
                            accuracy = cBean.getAccuracy();
                            provider = cBean.getProvider();
                        }
                    }

                } else if (answer_type_id.equalsIgnoreCase(AnswerType.GPS_TIME)
                        || answer_type_id.equalsIgnoreCase(AnswerType.GEODATA_TIME)) {
                    timestamp = sab.getTimestamp();
                    if (answer_type_id.equalsIgnoreCase(AnswerType.GEODATA_TIME)) {
                        mcc = sab.getMcc();
                        mnc = sab.getMnc();
                        lac = sab.getLac();
                        cellid = sab.getCellid();
                        isGPS = false;
                        GeofenceBean cBean = CellIdLookup.getCoordinate(mcc, mnc, lac, cellid);
                        latitude = cBean.getLatitude();
                        longitude = cBean.getLongitude();
                        accuracy = cBean.getAccuracy();
                        provider = cBean.getProvider();
                    } else {
                        latitude = sab.getLatitude();
                        longitude = sab.getLongitude();
                        accuracy = sab.getAccuracy();
                        provider = sab.getProvider();
                        isGPS = true;
                    }

                }

                paramSubmit.put("text_answer", answerText);
                paramSubmit.put("option_answer_id", option_answer_id);
                paramSubmit.put("option_answer_text", answerCombo);
                paramSubmit.put("gps_latitude", latitude);
                paramSubmit.put("gps_longitude", longitude);
                paramSubmit.put("is_gps", isGPS);
                paramSubmit.put("image_path", fileName);
                paramSubmit.put("image_blob", imageBlob);
                paramSubmit.put("timestamp", timestamp);
                paramSubmit.put("question_text", question_label);
                paramSubmit.put("mnc", mnc);
                paramSubmit.put("mcc", mcc);
                paramSubmit.put("lac", lac);
                paramSubmit.put("cellid", cellid);
                paramSubmit.put("accuracy", accuracy);
                paramSubmit.put("provider", provider);


                this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "Update Data: "
                        + answerText + ", " + option_answer_id + ", "
                        + answerCombo + ", " + latitude + ", " + longitude
                        + ", " + isGPS + ", " + fileName + ", " + imageBlob
                        + ", " + timestamp + ", " + question_label);
                this.ibatisSqlMap.update(
                        this.STRING_SQL + insertSurveyDetail,
                        paramSubmit);
            }

            result = mobile_assignment_id;
            Map paramUpdate = new HashMap();
            paramUpdate.put("endDate", TreemasFormatter.formatDate(new Date(),
                    TreemasFormatter.DFORMAT_YMDHHMMSS));
            paramUpdate.put("status", Global.Submitted);
            paramUpdate.put("MSAID", mobile_assignment_id);
            if (cekMaid > 0) {
                this.ibatisSqlMap.update(this.STRING_SQL + "updateMSA",
                        paramUpdate);
            }
            this.setActivity(loginBean.getUserName(), ACTIVITY_SUBMIT_COLLECTION
                    .replaceAll("#user#", loginBean.getUserName())
                    .replaceAll("#TASKID#", String.valueOf(mobile_assignment_id)));
            this.ibatisSqlMap.commitTransaction();
            this.ibatisSqlMap.endTransaction();
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }/*finally {
            this.ibatisSqlMap.endTransaction();
		}*/
        return result;
    }

    private void doCallMove(Integer msa_id) throws SQLException,
            CoreException {
        String result = null;
        Map map = new HashMap();
        map.put("taskId", msa_id);
        map.put("result", "-1");
        this.ibatisSqlMap.update(this.STRING_SQL + "callmove", map);
        result = (String) map.get("result");
    }

    public String getSchemeListToUpdate(LoginBean loginBean, ListSchemeCheckBean beanList) throws SQLException,
            CoreException {
        String ret = null;
//		System.out.println(loginBean.getUserName());
        List<SchemeCheckBean> listDB = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getLastSchemeByUser", loginBean.getUserName());
        Map retMap = this.checkDateOfScheme(loginBean.getUserName(), listDB, beanList.getList());
        ret = gson.toJson(retMap, Map.class);
        return ret;
    }

    private Map checkDateOfScheme(String user, List<SchemeCheckBean> listDB, List<SchemeCheckBean> listHandset) throws SQLException,
            CoreException {
        Map retMap = new HashMap();
        List<SchemeBean> ret = null;
        List<SchemeAnswerBean> retAnswer = null;
        List<SchemeCheckBean> list = null;
        List<SchemePrintItemBean> listPrintItem = null;
        String data = "";
        int index = 0;
        boolean kosong = false;

        if (null == listHandset) {
            kosong = true;
        } else if (listHandset.size() <= 0) {
            kosong = true;
        }

        for (SchemeCheckBean bean : listDB) {
            if (kosong) {
                if (index == 0) {
                    data = data.concat(bean.getScheme_id());
                } else {
                    data = data.concat("," + bean.getScheme_id());
                }
            } else {
//				if(!listHandset.contains(bean)){//harus update
                if (this.checkSchemeDate(bean, listHandset)) {
                    if (index == 0) {
                        data = data.concat(bean.getScheme_id());
                    } else {
                        data = data.concat("," + bean.getScheme_id());
                    }
                }
            }
            index++;
        }
        if (!Strings.isNullOrEmpty(data)) {
            this.setActivity(user.toUpperCase(), "UPDATE_SCHEME (" + data.replaceAll("'", "") + ")");
           
            Map param = new HashMap();
            param.put("param", data.split(","));
            param.put("user", user.toUpperCase());
            ret = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getSchemeUpdate", param);
            retAnswer = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getSchemeAnswerUpdate", param);
            list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getSchemeDate", param);
            listPrintItem = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getSchemePrintUpdate", param);
        }
        retMap.put("schemeInfo", list);
        retMap.put("schemeList", ret);
        retMap.put("answerList", retAnswer);
        retMap.put("schemePrint", listPrintItem);
        return retMap;
    }

    private boolean checkSchemeDate(SchemeCheckBean bean, List<SchemeCheckBean> listHandset) {
        boolean ret = true;
        for (SchemeCheckBean beanHandset : listHandset) {
            if (bean.getScheme_id().trim().equalsIgnoreCase(beanHandset.getScheme_id().trim())) {//jika scheme sama
                String tglHP = TreemasFormatter.formatDate(beanHandset.getScheme_last_update(), TreemasFormatter.DFORMAT_YMDHHMMSS);
                String tgl = TreemasFormatter.formatDate(bean.getScheme_last_update(), TreemasFormatter.DFORMAT_YMDHHMMSS);
                if (tglHP.equalsIgnoreCase(tgl)) {//jika tanggal sama tidak perlu update
                    ret = false;
                    break;
                } else if (beanHandset.getScheme_last_update().after(bean.getScheme_last_update())) {//jika handset sebelum tanggal DB
                    ret = false;
                    break;
                }
            }
        }
        return ret;
    }

    public void saveRegid(String user, String regid) throws CoreException {
        try {
            Map paramMap = new HashMap();
            paramMap.put("user", user);
            paramMap.put("regid", regid);
            this.ibatisSqlMap.update(this.STRING_SQL + "updateRegid", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ERROR_DB);
        }

    }

    public Integer submitHeaderValey(LoginBean loginBean, SubmitHeaderBean submitHeaderBean)
            throws CoreException, SQLException, IOException {
        HeaderBean bean = submitHeaderBean.getHeaderBean();
        String branch = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getUserBranch", loginBean.getUserName());
        Map paramMap = new HashMap();
        paramMap.put("BRANCH_ID", branch);
        paramMap.put("USER_ID", loginBean.getUserName());
        paramMap.put("CUSTOMER_NAME", bean.getNama_pemohon());
        paramMap.put("CUSTOMER_ADDRESS", bean.getAlamat_pemohon());
        paramMap.put("CUSTOMER_PHONE", bean.getTelepon());
        paramMap.put("NOTES", bean.getNotes());
        paramMap.put("CUSTOMER_HP", bean.getHP());
        paramMap.put("SCHEME_ID", bean.getScheme_id());
        paramMap.put("AREAPHONE1", bean.getAreaPhone1());
        Integer resultSave = (Integer) this.ibatisSqlMap.insert(this.STRING_SQL + "insertMSASurvey", paramMap);
        Integer mobile_assignment_id = (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "selectNewMSASurvey", paramMap);
        return mobile_assignment_id;
    }
        
    
    public String cekUserName(String username) throws SQLException {
    	String 	userName = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getUsername",username);
        return userName;
    
    }
    
 
    
   
    
   
    public String getDataList(LoginBean loginBean) throws SQLException,
    CoreException {
		String ret = null;
		//System.out.println(loginBean.getUserName());
		List<SchemeCheckBean> listDB = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getLastSchemeByUser", loginBean.getUserName());
		Map retMap = this.getDataListToUpdate(loginBean.getUserName());
		ret = gson.toJson(retMap, Map.class);
		return ret;
}
    
    private Map getDataListToUpdate(String user) throws SQLException,
    CoreException {
		Map retMap = new HashMap();
		List<ListNoteApproval> ret = null;
		List<ListUserApproval> retAnswer = null;
		List<ListTaskApproval> list = null;
		
		    ret = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getNoteApprovalUpdate", user);
		    retAnswer = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getUserApprovalUpdate", user);
		    list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getTaskApprovalUpdate", user);
			

			retMap.put("TaskList", list);
			retMap.put("UserList", retAnswer);
			retMap.put("NoteList", ret);
			return retMap;
		}
    
    public Integer submitResultApproval(LoginBean loginBean,
    		SubmitApprovalBeanList beanList) throws CoreException,
			SQLException, IOException {
			Integer result = 0;
			String mobile_assignment_id = beanList.getMobile_assignment_id();
			String Answer = "";
			
			try {
			Map paramMap = new HashMap();
			Map param = new HashMap();
			List<SubmitApprovalBean> list = beanList.getAnswerList();
			for (SubmitApprovalBean sab : list) {
			
			String next = sab.getIsnext();
			String appno = sab.getApprovalNo();
			String user	 = sab.getUserRequest();

			paramMap.put("appno", appno);
			paramMap.put("user", user);
			paramMap.put("userNext",sab.getUserRequestNext());
			paramMap.put("schemeId",sab.getApprovalSchemeID());
			ApprovalBean bean =  (ApprovalBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getBeanApproval",paramMap);
			Double limitNext = (Double) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getLimitNext",paramMap);
			Double limit = (Double) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getLimit",paramMap);
			String insertApproval = "";
			if (next.equals("1")||next == "1") {
				insertApproval = "insertApprovalNext"; 
				paramMap.put("IsFinal", 0);
				paramMap.put("ApprovalStatus", "N");
				paramMap.put("UserRequest", sab.getLoginId() );
				paramMap.put("UserApproval", sab.getUserRequestNext());
				if (sab.getApprovalResult().equals("APP")){
					paramMap.put("ApprovalResult", "A");
				}else if (sab.getApprovalResult().equals("RET")){
					paramMap.put("ApprovalResult", "R");
				}else{
					paramMap.put("ApprovalResult", "J");
				}
					paramMap.put("ApprovalNo", sab.getApprovalNo());
					paramMap.put("ApprovalID", sab.getApprovalID());
					paramMap.put("ApprovalSchemeID", sab.getApprovalSchemeID());
					paramMap.put("SecurityCode", bean.getSecurityCode());
					paramMap.put("RequestDate", bean.getRequestDate());
					paramMap.put("IsViaSMS",  bean.getIsViaSMS());
					paramMap.put("IsViaEmail", bean.getIsViaEmail());
					paramMap.put("IsViaFax", bean.getIsViaFax());
					paramMap.put("AreaFaxNo", bean.getAreaFaxNo());
					paramMap.put("FaxNo", bean.getFaxNo());
					paramMap.put("ApprovalDate", TreemasFormatter.formatDate(new Date(),
		                    TreemasFormatter.DFORMAT_YMDHHMMSS));
					paramMap.put("LimitValue", limitNext);
					paramMap.put("ApprovalNote", sab.getApprovalNote());			
					paramMap.put("ApprovalValue", bean.getApprovalValue());
					paramMap.put("IsForward", bean.getIsForward());
					paramMap.put("UserSecurityCode", bean.getUserSecurityCode());
					paramMap.put("ApprovalRoleID", bean.getApprovalRoleID());
					paramMap.put("TransactionNo", bean.getTransactionNo());
					paramMap.put("WOA", bean.getWOA());
					paramMap.put("IsEverRejected", bean.getIsEverRejected());
					paramMap.put("ClientIP", bean.getClientIP());
					paramMap.put("Argumentasi", bean.getArgumentasi());
					paramMap.put("ApprovalValuePencentage", bean.getApprovalValuePencentage());
					paramMap.put("ApprovalValuePercentage", bean.getApprovalValuePercentage());
					paramMap.put("IsApprovedByMobile", bean.getIsApprovedByMobile());
					paramMap.put("UsrUpd", sab.getLoginId());
					paramMap.put("dtmupd", TreemasFormatter.formatDate(new Date(),
		                    TreemasFormatter.DFORMAT_YMDHHMMSS));
					this.ibatisSqlMap.insert(
							this.STRING_SQL + insertApproval,
							paramMap);
					this.ibatisSqlMap.update(this.STRING_SQL + "updateTask", paramMap);	

			} else {
				insertApproval = "insertApproval";
				paramMap.put("IsFinal", 1);
				paramMap.put("ApprovalStatus","Y");
				paramMap.put("UserRequest", sab.getUserRequest());
				paramMap.put("UserApproval", bean.getUserApproval());
				if (sab.getApprovalResult().equals("APP")){
					paramMap.put("ApprovalResult", "A");
				}else if (sab.getApprovalResult().equals("RET")){
					paramMap.put("ApprovalResult", "R");
				}else{
					paramMap.put("ApprovalResult", "J");
				}
					paramMap.put("ApprovalNo", sab.getApprovalNo());
					paramMap.put("ApprovalID", sab.getApprovalID());
					paramMap.put("ApprovalSchemeID", sab.getApprovalSchemeID());
					paramMap.put("SecurityCode", bean.getSecurityCode());
					paramMap.put("RequestDate", bean.getRequestDate());
					paramMap.put("IsViaSMS",  bean.getIsViaSMS());
					paramMap.put("IsViaEmail", bean.getIsViaEmail());
					paramMap.put("IsViaFax", bean.getIsViaFax());
					paramMap.put("AreaFaxNo", bean.getAreaFaxNo());
					paramMap.put("FaxNo", bean.getFaxNo());
					paramMap.put("ApprovalDate", TreemasFormatter.formatDate(new Date(),
		                    TreemasFormatter.DFORMAT_YMDHHMMSS));
					paramMap.put("LimitValue", limit);
					paramMap.put("ApprovalNote", sab.getApprovalNote());			
					paramMap.put("ApprovalValue", bean.getApprovalValue());
					paramMap.put("IsForward", bean.getIsForward());
					paramMap.put("UserSecurityCode", bean.getUserSecurityCode());
					paramMap.put("ApprovalRoleID", bean.getApprovalRoleID());
					paramMap.put("TransactionNo", bean.getTransactionNo());
					paramMap.put("WOA", bean.getWOA());
					paramMap.put("IsEverRejected", bean.getIsEverRejected());
					paramMap.put("ClientIP", bean.getClientIP());
					paramMap.put("Argumentasi", bean.getArgumentasi());
					paramMap.put("ApprovalValuePencentage", bean.getApprovalValuePencentage());
					paramMap.put("ApprovalValuePercentage", bean.getApprovalValuePercentage());
					paramMap.put("IsApprovedByMobile", 1);
					paramMap.put("UsrUpd", sab.getLoginId());
					paramMap.put("dtmupd", TreemasFormatter.formatDate(new Date(),
		                    TreemasFormatter.DFORMAT_YMDHHMMSS));
					this.ibatisSqlMap.insert(
							this.STRING_SQL + insertApproval,
							paramMap);
					this.ibatisSqlMap.update(this.STRING_SQL + "updateTask", paramMap);
					this.ibatisSqlMap.update(this.STRING_SQL + "updateTaskApp", paramMap);
			}
			
			this.logger.log(ILogger.LEVEL_DEBUG_LOW, "=====" + sab.getApprovalNo() + "::INSERT INTO DATABSER ======");

			AuditrailTaskBean beans = new AuditrailTaskBean();
		
			if (sab.getApprovalResult().equals("APP")){
				Answer = "APPROVE";
			}else if (sab.getApprovalResult().equals("RET")){
				Answer = "RETURN";
			}else{
				Answer = "REJECT";
			}
			Map par = new HashMap();
			par.put("ApprovalNO", bean.getApprovalNo());
			par.put("ApprovalID", bean.getApprovalID());
			par.put("ApprovalNoteAnswer", bean.getApprovalNote());
			par.put("ApprovalSchemeID", bean.getApprovalSchemeID());
			par.put("AprovalAnswer", Answer);
			par.put("UserApprovalNext", sab.getUserRequestNext());
		
			this.ibatisSqlMap.insert(
					this.STRING_SQL +"insertaudittask",
					par);
			
			Map parNote = new HashMap();
			parNote.put("ApprovalNO", sab.getApprovalNo());
			parNote.put("ApprovalNote", sab.getApprovalNote());
			parNote.put("UsrUpd", sab.getLoginId());
			this.ibatisSqlMap.insert(
					this.STRING_SQL +"insertnotetask",
					parNote);
			}
			result = 1;
			
		
			
			this.ibatisSqlMap.endTransaction();
			} catch (SQLException sqle) {
			this.logger.printStackTrace(sqle);
			throw new CoreException(sqle, ERROR_DB);
			}/*finally {
			this.ibatisSqlMap.endTransaction();
			}*/
			return result;

    }
    
    public String getListData(LoginBean loginBean) throws SQLException,
    CoreException {
    	
    	Map retMap = new HashMap();
		List<ListKaryawan> rets = null;
		List<ListAnnouncement> retAnswer = null;
		List<LeaderBean> list = null;
		List<ProjectBean> project = null;
		
		    rets = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getKaryawanList", loginBean.getUserName());
		    retAnswer = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAnnouncement");
		    list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getLeader");
		    project = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getProject");

			retMap.put("karyawanBean", rets);
			retMap.put("announcementBean", retAnswer);
			retMap.put("LeaderBean", list);
			retMap.put("ProjectBean", project);
    	
		String ret = null;
		//System.out.println(loginBean.getUserName());
	
		ret = gson.toJson(retMap, Map.class);
		return ret;
}
    private Map getDataToUpdate(String user) throws SQLException,
    CoreException {
		Map retMap = new HashMap();
		List<ListTaskApproval> list = null;
		    list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getTaskApprovalUpdate", user);
			retMap.put("TaskList", list);
			return retMap;
		}
    
    public Integer updatepass(String username,String password ) throws SQLException {
    	 Integer result = 0;
    	 Map retMap = new HashMap();
    	 retMap.put("username", username);
    	 retMap.put("password", password);
    	 try {
    	 this.ibatisSqlMap.update(
                 this.STRING_SQL + "updUsrMob", retMap);
    	 
    	 } catch (SQLException sqle) {
 			this.logger.printStackTrace(sqle);
 			}
    	 return result;
    
    }
    
    public String cekImei(String userName) throws SQLException {
    	String 	imei = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getImei",userName);
        return imei;
    }
    public String cekLocked(String userName) throws SQLException {
    	String 	locked = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getLocked",userName);
        return locked;
    }
    
    public String cekUserID(String IMEI, String userName) throws SQLException {
    	Map paramMap = new HashMap();
        paramMap.put("IMEI", IMEI);
        paramMap.put("USERID", userName);
    	String 	ID = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getuserID",paramMap);
        return ID;
    
    }
    public String cekPassword(String LoginID) throws SQLException {
    	String 	pass = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getPassword",LoginID);
        return pass;
    
    }
    public String cekNames(String username) throws SQLException {
    	String 	nama = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getNama",username);
        return nama;
    }
    
    public String flag(String username) throws SQLException {
    	String 	flag = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getFlag",username);
        return flag;
    }
    
    public void updlock(String username) throws SQLException {
    	Map retMap = new HashMap();
    	Integer lock =  (Integer) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "ceklock",username);
	   	Integer lok= lock+1;
    	System.out.println(lok);
    	retMap.put("username", username);
		 retMap.put("locked", lok);
    	this.ibatisSqlMap.queryForObject(this.STRING_SQL + "updlock",retMap);
    	if (lock==3){
    		this.ibatisSqlMap.queryForObject(this.STRING_SQL + "updlocks",username);
    	}
    }
    public Integer submitAbsen(LoginBean loginBean,
    		SubmitAbsenBeanList beanList) throws CoreException,
			SQLException, IOException {
			Integer result = 0;
			//String mobile_assignment_id = beanList.getMobile_assignment_id();
			String Answer = "";
			
			try {
			Map paramMap = new HashMap();
			Map param = new HashMap();
			List<SubmitAbsen> list = beanList.getAnswerList();
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
			SimpleDateFormat formatters = new SimpleDateFormat("HH:mm:ss"); 
			for (SubmitAbsen sab : list) {
				  Double longitude = null;
	                Double latitude = null;
	                Calendar cal = Calendar.getInstance();
	                SimpleDateFormat day = new SimpleDateFormat("EEEE");
	                String days = day.format(cal.getTime());
	                param.put("nik", sab.getNik());
	                param.put("tglAbsen", formatter.format(date));
			if (sab.getFlgabs().equals("MSK")) {
				String isAbsen = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getIsabsen", param);
				if(Strings.isNullOrEmpty(isAbsen)){
					latitude = sab.getGpsLatitudeMsk();
					longitude=sab.getGpsLongitudeMsk();
					paramMap.put("nik", sab.getNik());
					paramMap.put("gpsLatitudeMsk", latitude);
					paramMap.put("gpsLongitudeMsk", longitude);
					paramMap.put("LokasiMsk", sab.getLokasiMsk());
					paramMap.put("jarakMsk", sab.getJarakMsk());
					paramMap.put("isLembur", 0);
					paramMap.put("hari", days);
					paramMap.put("tglAbsen", formatter.format(date));
					paramMap.put("jmMsk", formatters.format(date));
					paramMap.put("isAbsen", 1);
					this.logger.log(ILogger.LEVEL_DEBUG_LOW, "=====" + sab.getNik() + "::INSERT ABSEN INTO DATABASE ======");

	                this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "INSERT DATA: "
	                        + sab.getNik() + ", " + latitude + ", "
	                        + longitude + ", " + sab.getLokasiMsk() + ", " + days);
					this.ibatisSqlMap.insert(
								this.STRING_SQL + "insertAbsen",
								paramMap);
					result = 1;
					}else {
						result = 0;
					}	 

				} else if(sab.getFlgabs().equals("PLG")) {
					latitude = sab.getGpsLatitudePlg();
					longitude=sab.getGpsLongitudePlg();
					paramMap.put("nik", sab.getNik());
					paramMap.put("gpsLatitudePlg", latitude);
					paramMap.put("gpsLongitudePlg", longitude);
					paramMap.put("LokasiPlg", sab.getLokasiPlg());
					paramMap.put("jarakPlg", sab.getJarakPlg());
					paramMap.put("isLembur", sab.getIsLembur());
					paramMap.put("note", sab.getNote());
					paramMap.put("totalJamKerja", sab.getTotalJamKerja());
					paramMap.put("tglAbsen", formatter.format(date));
					paramMap.put("jmPlg", formatters.format(date));
					this.logger.log(ILogger.LEVEL_DEBUG_LOW, "=====" + sab.getNik() + "::UPDATE ABSEN INTO DATABASE ======");
				       this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "UPDATE DATA: "
		                        + sab.getNik() + ", " + latitude + ", "
		                        + longitude + ", " + sab.getLokasiPlg() + ", " +sab.getJarakPlg()+ ", " +formatter.format(date));				
					this.ibatisSqlMap.update(
								this.STRING_SQL + "updateAbsen",
								paramMap);	
					Locale lokal = null;
					  String pola = "HH:mm:ss";
					  String waktuMasuk = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleWaktuMasuk", paramMap);
					  String waktuKeluar  = (String) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getSingleWaktuKeluar", paramMap);				  
				Date waktuM = HandsetManager.konversiStringkeDate(waktuMasuk,
					                pola, lokal);
					        Date Waktuk = HandsetManager.konversiStringkeDate(waktuKeluar, pola,
					                lokal);
					 String hasilSelisih = HandsetManager.selisihDateTime(waktuM,
							 Waktuk);  
					 paramMap.put("totalJamKerja", hasilSelisih);
					 try {
						 this.ibatisSqlMap.startTransaction();
						 this.ibatisSqlMap.update(
									this.STRING_SQL + "updateJamKerja",
									paramMap);	

							//Insert Timesheet
							//this.submitTimesheet(sab.getNik());
							//Insert Reimburst
						//this.submitReimburse(sab.getNik());
						this.ibatisSqlMap.commitTransaction();
							result = 1;
					} finally {
						// TODO: handle finally clause
			            this.ibatisSqlMap.endTransaction();
					}
					
				}
			}		
				this.ibatisSqlMap.endTransaction();
				} catch (SQLException sqle) {
					this.logger.printStackTrace(sqle);
					throw new CoreException(sqle, ERROR_DB);
			}finally {
            this.ibatisSqlMap.endTransaction();
		}
		return result;

    }
    	protected static String selisihDateTime(Date waktuSatu, Date waktuDua) {
    			long selisihMS = Math.abs(waktuSatu.getTime() - waktuDua.getTime());
    			long selisihDetik = selisihMS / 1000 % 60;
    			long selisihMenit = selisihMS / (60 * 1000) % 60;
    			long selisihJam = selisihMS / (60 * 60 * 1000) % 24;
    			long selisihHari = selisihMS / (24 * 60 * 60 * 1000);
    		String selisih = selisihJam + "";     
		return selisih;

    }
    protected static Date konversiStringkeDate(String tanggalDanWaktuStr,
    String pola, Locale lokal) {
        Date tanggalDate = null;
        String date;
       SimpleDateFormat formatter;
        if (lokal == null) {
            formatter = new SimpleDateFormat(pola);
        } else {
            formatter = new SimpleDateFormat(pola, lokal);
        }
        try {
            tanggalDate = formatter.parse(tanggalDanWaktuStr);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return tanggalDate;
    }
    public Integer submitCuti(LoginBean loginBean,
    		SubmitCutiBeanList beanList) throws CoreException,
			SQLException, IOException {
			Integer result = 0;
			String Answer = "";
			
			try {
			Map paramMap = new HashMap();
			Map param = new HashMap();
			List<SubmitCuti> list = beanList.getAnswerList();
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
			SimpleDateFormat formatters = new SimpleDateFormat("HH:mm:ss"); 
			for (SubmitCuti sab : list) {
	                Calendar cal = Calendar.getInstance();
	                SimpleDateFormat day = new SimpleDateFormat("EEEE");
	                String days = day.format(cal.getTime());
			if (sab.getFlgKet().equals("CUTI")) {
				paramMap.put("nik", sab.getNik());
				paramMap.put("nikLeader", sab.getNikLeader());
				paramMap.put("tglMulai", sab.getTglMulai());
				paramMap.put("tglSelesai", sab.getTglSelesai());
				paramMap.put("tglKembaliKerja", sab.getTglKembaliKerja());
				paramMap.put("alamatCuti", sab.getAlamatCuti());
				paramMap.put("KeperluanCuti", sab.getKeperluanCuti());
				paramMap.put("usrcrt", loginBean.getUserName());
				paramMap.put("dtmcrt", formatter.format(date));
				paramMap.put("isapproved", 0);
				paramMap.put("flgKet", sab.getFlgKet());
				this.logger.log(ILogger.LEVEL_DEBUG_LOW, "=====" + sab.getNik() + "::INSERT CUTI INTO DATABASE ======");

                this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "INSERT DATA: "
                        + sab.getNik() + ", " + sab.getNikLeader() + ", "
                        + sab.getTglMulai() + ", " + sab.getTglSelesai() + ", " + sab.getTglKembaliKerja()+ ", " + sab.getFlgKet());
				this.ibatisSqlMap.insert(
							this.STRING_SQL + "insertCuti",
							paramMap);
					

			} else if(sab.getFlgKet().equals("SAKIT")) {

				paramMap.put("nik", sab.getNik());
				paramMap.put("nikLeader", sab.getNikLeader());
				paramMap.put("tglMulai", sab.getTglMulai());
				paramMap.put("tglSelesai", sab.getTglMulai());
				paramMap.put("tglKembaliKerja", sab.getTglKembaliKerja());
				paramMap.put("KeperluanCuti", sab.getKeperluanCuti());
				paramMap.put("usrcrt", loginBean.getUserName());
				paramMap.put("dtmcrt", formatter.format(date));
				paramMap.put("isapproved", 1);
				paramMap.put("flgKet", sab.getFlgKet());
				this.logger.log(ILogger.LEVEL_DEBUG_LOW, "=====" + sab.getNik() + "::INSERT SAKIT INTO DATABASE ======");
			       this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "INSERT DATA: "
	                        + sab.getNik() + ", " + sab.getTglMulai() + ", "
	                        + sab.getTglKembaliKerja() + ", " + sab.getKeperluanCuti()+ ", " + sab.getFlgKet());				
				this.ibatisSqlMap.update(
							this.STRING_SQL + "insertSakit",
							paramMap);	
			
			}
			
			}		
				this.ibatisSqlMap.endTransaction();
				} catch (SQLException sqle) {
					this.logger.printStackTrace(sqle);
					throw new CoreException(sqle, ERROR_DB);
			}finally {
            this.ibatisSqlMap.endTransaction();
		}
		return result;

    }
    
    public String submitReimburse(String nik)
    		throws SQLException, CoreException {
    	
    	String tgl = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    	
    	ReimburseBean bean = null;
    	String result1 = null;
    	
        Map param = new HashMap();
	   	param.put("nik", nik);
	    param.put("tgl", tgl);
	    
    	Double jrkMasuk = (Double) this .ibatisSqlMap.queryForObject(this.STRING_SQL + "getJrkMasuk", param);
        Double jrkKeluar = (Double) this .ibatisSqlMap.queryForObject(this.STRING_SQL + "getJrkKeluar", param);
        
        ReimburseBean result = (ReimburseBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getAbsen", param);        
        
        if(jrkMasuk >= 50 && jrkKeluar >= 50 ){
 
        	Map paramMap = new HashMap();
    		paramMap.put("nik",  result.getNik());
            paramMap.put("nama", result.getNama());
            paramMap.put("hari", result.getHari());
            paramMap.put("tgl",  result.getTgl());
            paramMap.put("lokasi", result.getLokasi());
            paramMap.put("kota", result.getKota());
            paramMap.put("idProject", result.getIdProject());
            paramMap.put("jamMasuk", result.getJamMasuk());
            paramMap.put("jamKeluar", result.getJamKeluar());
            paramMap.put("transport", result.getTransport());
            paramMap.put("totalJamKerja", result.getTotalJamKerja());
            Integer totJam = result.getTotalJamKerja();
            if (totJam <= 9){
            	paramMap.put("uangMakan",0);
            }else if (totJam >= 9 ) {
            	paramMap.put("uangMakan",20000);
            }
         //   paramMap.put("uangData", 100000);
            paramMap.put("image", result.getImage());
            paramMap.put("isApprove", 0);
            paramMap.put("noteApp", null);
        	
    		Integer saveAppReimburse = (Integer) this.ibatisSqlMap.insert(this.STRING_SQL + "insertAppReimburse", paramMap);
    	
    	}else{
    		
    		Map paramMap = new HashMap();
    		paramMap.put("nik",  result.getNik());
            paramMap.put("nama", result.getNama());
            paramMap.put("hari", result.getHari());
            paramMap.put("tgl",  result.getTgl());
            paramMap.put("lokasi", result.getLokasi());
            paramMap.put("kota", result.getKota());
            paramMap.put("idProject", result.getIdProject());
            paramMap.put("jamMasuk", result.getJamMasuk());
            paramMap.put("jamKeluar", result.getJamKeluar());
            paramMap.put("transport", result.getTransport());
            paramMap.put("totalJamKerja", result.getTotalJamKerja());
            paramMap.put("isApprove", 1);
            Integer totJam = result.getTotalJamKerja();
            if (totJam <= 9){
            	paramMap.put("uangMakan",0);
            }else if (totJam >= 9 ) {
            	paramMap.put("uangMakan",20000);
            }
        //    paramMap.put("uangData", 100000);
            paramMap.put("image", result.getImage());
            
          
    		Integer saveReimburse = (Integer) this.ibatisSqlMap.insert(this.STRING_SQL + "insertReimburse", paramMap);
    	}
    
   	return result1;
    }
    
    public String submitTimesheet(String nik)
    		throws SQLException, CoreException{
    	
    	String tgl = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    	
    	TimesheetBean bean = null;
    	String tsb = null;
    	
        Map param = new HashMap();
	   	param.put("nik", nik);
	    param.put("tgl", tgl);

	    TimesheetBean result = (TimesheetBean) this.ibatisSqlMap.queryForObject(this.STRING_SQL + "getDataTimesheet", param);        
	    Date date1,time1,time2;
		try {
			date1 = new SimpleDateFormat("yyyy-MM-dd").parse(result.getTglMsk());
			time1 = new SimpleDateFormat("HH:mm:ss").parse(result.getJamMasuk());
			time2 = new SimpleDateFormat("HH:mm:ss").parse(result.getJamPulang());
			
			Map paramMap = new HashMap();
    		paramMap.put("nik",  result.getNik());
            paramMap.put("nikLeader", result.getNikLeader());
            paramMap.put("hari", result.getHari());
            paramMap.put("tglMsk",  date1);
            paramMap.put("idProject", result.getIdProject());
            paramMap.put("jamMasuk", time1);
            paramMap.put("jamKeluar", time2);
            paramMap.put("totalJamKerja", result.getTotalJamKerja());
            paramMap.put("note", result.getNote());
            if (result.getTotalJamKerja() < 9){
            	paramMap.put("over", 0);
            }else {
            	 Integer over = result.getTotalJamKerja() - 9;
     	    	String overtime= String.valueOf(over);
     	    	paramMap.put("over", over);
            }

	    	this.logger.log(ILogger.LEVEL_DEBUG_LOW, "=====" + result.getNik() + "::INSERT TIMESHEET INTO DATABASE ======");
		       this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "INSERT DATA: "
                     + result.getNik() + ", " + result.getNikLeader() + ", "
                     + result.getHari() + ", " + result.getTglMsk()+ ", " + result.getIdProject());	
	    	this.ibatisSqlMap.insert(
					this.STRING_SQL + "insertTimesheet",
					paramMap);	
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
        	
   	return tsb;
    }
    
    public String getListDetail(LoginBean loginBean) throws SQLException,
    CoreException {
    	
    	Map retMap = new HashMap();
		List<listTimesheetBean> rets = null;
		List<ListReimburse> retAnswer = null;
		List<ListAbsen> list = null;
		String nik = loginBean.getUserName();
		
		    rets = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getTimesheet", nik);
		    retAnswer = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getReimburse", nik);
		    list = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getAbsenKaryawan", nik);

			retMap.put("TimesheetBean", rets);
			retMap.put("ReimburseBean", retAnswer);
			retMap.put("AbsenBean", list);
    	
		String ret = null;
			
		ret = gson.toJson(retMap, Map.class);
		return ret;
}
}

  

