package treemas.handset.newTask;

import treemas.handset.list.HeaderBean;
import treemas.handset.submit.SubmitAnswerBean;
import treemas.handset.tracking.TrackingBean;

import java.util.List;


public class SubmitNewBean {
    private HeaderBean headerBean;
    private List<SubmitAnswerBean> answerList;
    private TrackingBean trackingBean;

    public SubmitNewBean() {
    }

    public HeaderBean getHeaderBean() {
        return headerBean;
    }

    public void setHeaderBean(HeaderBean headerBean) {
        this.headerBean = headerBean;
    }

    public List<SubmitAnswerBean> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<SubmitAnswerBean> answerList) {
        this.answerList = answerList;
    }

    public TrackingBean getTrackingBean() {
        return trackingBean;
    }

    public void setTrackingBean(TrackingBean trackingBean) {
        this.trackingBean = trackingBean;
    }


}
