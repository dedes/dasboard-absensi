package treemas.handset.scheme;

public class SchemeAnswerBean {
    private String scheme_id;
    private Integer question_group_id;
    private Integer question_group_line_seq_order;
    private String question_group_name;
    private Integer question_id;
    private Integer option_answer_id;
    private String option_answer_label;
    private Integer option_answer_line_seq_order;
    private String branchid;
    
    
    
    public String getBranchid() {
		return branchid;
	}

	public void setBranchid(String branchid) {
		this.branchid = branchid;
	}

	public String getScheme_id() {
        return scheme_id;
    }

    public void setScheme_id(String scheme_id) {
        this.scheme_id = scheme_id;
    }

    public Integer getQuestion_group_id() {
        return question_group_id;
    }

    public void setQuestion_group_id(Integer question_group_id) {
        this.question_group_id = question_group_id;
    }

    public Integer getQuestion_group_line_seq_order() {
        return question_group_line_seq_order;
    }

    public void setQuestion_group_line_seq_order(
            Integer question_group_line_seq_order) {
        this.question_group_line_seq_order = question_group_line_seq_order;
    }

    public String getQuestion_group_name() {
        return question_group_name;
    }

    public void setQuestion_group_name(String question_group_name) {
        this.question_group_name = question_group_name;
    }

    public Integer getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(Integer question_id) {
        this.question_id = question_id;
    }

    public Integer getOption_answer_id() {
        return option_answer_id;
    }

    public void setOption_answer_id(Integer option_answer_id) {
        this.option_answer_id = option_answer_id;
    }

    public String getOption_answer_label() {
        return option_answer_label;
    }

    public void setOption_answer_label(String option_answer_label) {
        this.option_answer_label = option_answer_label;
    }

    public Integer getOption_answer_line_seq_order() {
        return option_answer_line_seq_order;
    }

    public void setOption_answer_line_seq_order(Integer option_answer_line_seq_order) {
        this.option_answer_line_seq_order = option_answer_line_seq_order;
    }

}
