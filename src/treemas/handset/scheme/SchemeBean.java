package treemas.handset.scheme;

public class SchemeBean {
    private String scheme_id;
    private String scheme_description;
    private Integer question_group_id;
    private Integer question_group_line_seq_order;
    private String question_group_name;
    private Integer question_id;
    private String question_label;
    private String answer_type;
    private String answer_type_name;
    private Integer question_master_line_seq_order;
    private Integer is_mandatory;
    private Integer max_length;
    private Integer is_visible;
    private String is_use_min;
    private Integer min_choose;
    private Double minval;
    private Double maxval;
    private String regex_pattern;
    private Integer is_readonly;
    private Integer rel_question_group_id;
    private Integer rel_question_id;
    private Integer rel_option_answer_id;
    private String rel_text_answer;
    private String FEAT_QUESTION_line_seq_order;
    private String usage_type;
    private Double totalgroup;

    public String getFEAT_QUESTION_line_seq_order() {
        return FEAT_QUESTION_line_seq_order;
    }

    public void setFEAT_QUESTION_line_seq_order(String fEAT_QUESTION_line_seq_order) {
        FEAT_QUESTION_line_seq_order = fEAT_QUESTION_line_seq_order;
    }

    public String getScheme_description() {
        return scheme_description;
    }

    public void setScheme_description(String scheme_description) {
        this.scheme_description = scheme_description;
    }

    public String getScheme_id() {
        return scheme_id;
    }

    public void setScheme_id(String scheme_id) {
        this.scheme_id = scheme_id;
    }

    public Integer getQuestion_group_id() {
        return question_group_id;
    }

    public void setQuestion_group_id(Integer question_group_id) {
        this.question_group_id = question_group_id;
    }

    public Integer getQuestion_group_line_seq_order() {
        return question_group_line_seq_order;
    }

    public void setQuestion_group_line_seq_order(
            Integer question_group_line_seq_order) {
        this.question_group_line_seq_order = question_group_line_seq_order;
    }


    public String getQuestion_group_name() {
        return question_group_name;
    }

    public void setQuestion_group_name(String question_group_name) {
        this.question_group_name = question_group_name;
    }

    public Integer getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(Integer question_id) {
        this.question_id = question_id;
    }

    public String getQuestion_label() {
        return question_label;
    }

    public void setQuestion_label(String question_label) {
        this.question_label = question_label;
    }

    public String getAnswer_type() {
        return answer_type;
    }

    public void setAnswer_type(String answer_type) {
        this.answer_type = answer_type;
    }

    public String getAnswer_type_name() {
        return answer_type_name;
    }

    public void setAnswer_type_name(String answer_type_name) {
        this.answer_type_name = answer_type_name;
    }

    public Integer getQuestion_master_line_seq_order() {
        return question_master_line_seq_order;
    }

    public void setQuestion_master_line_seq_order(
            Integer question_master_line_seq_order) {
        this.question_master_line_seq_order = question_master_line_seq_order;
    }

    public Integer getIs_mandatory() {
        return is_mandatory;
    }

    public void setIs_mandatory(Integer is_mandatory) {
        this.is_mandatory = is_mandatory;
    }

    public Integer getMax_length() {
        return max_length;
    }

    public void setMax_length(Integer max_length) {
        this.max_length = max_length;
    }

    public Integer getIs_visible() {
        return is_visible;
    }

    public void setIs_visible(Integer is_visible) {
        this.is_visible = is_visible;
    }

    public String getRegex_pattern() {
        return regex_pattern;
    }

    public void setRegex_pattern(String regex_pattern) {
        this.regex_pattern = regex_pattern;
    }

    public Integer getIs_readonly() {
        return is_readonly;
    }

    public void setIs_readonly(Integer is_readonly) {
        this.is_readonly = is_readonly;
    }

    public Integer getRel_question_group_id() {
        return rel_question_group_id;
    }

    public void setRel_question_group_id(Integer rel_question_group_id) {
        this.rel_question_group_id = rel_question_group_id;
    }

    public Integer getRel_question_id() {
        return rel_question_id;
    }

    public void setRel_question_id(Integer rel_question_id) {
        this.rel_question_id = rel_question_id;
    }

    public Integer getRel_option_answer_id() {
        return rel_option_answer_id;
    }

    public void setRel_option_answer_id(Integer rel_option_answer_id) {
        this.rel_option_answer_id = rel_option_answer_id;
    }

    public String getRel_text_answer() {
        return rel_text_answer;
    }

    public void setRel_text_answer(String rel_text_answer) {
        this.rel_text_answer = rel_text_answer;
    }

    public String getIs_use_min() {
        return is_use_min;
    }

    public void setIs_use_min(String is_use_min) {
        this.is_use_min = is_use_min;
    }

    public Integer getMin_choose() {
        return min_choose;
    }

    public void setMin_choose(Integer min_choose) {
        this.min_choose = min_choose;
    }

    public Double getMinval() {
        return minval;
    }

    public void setMinval(Double minval) {
        this.minval = minval;
    }

    public Double getMaxval() {
        return maxval;
    }

    public void setMaxval(Double maxval) {
        this.maxval = maxval;
    }

    public String getUsage_type() {
        return usage_type;
    }

    public void setUsage_type(String usage_type) {
        this.usage_type = usage_type;
    }

    public Double getTotalgroup() {
        return totalgroup;
    }

    public void setTotalgroup(Double totalgroup) {
        this.totalgroup = totalgroup;
    }

}
