package treemas.handset.scheme;

import java.util.Date;

public class ListNoteApproval {
	
	private String ApprovalNo;
	  private Date DtmUpd;
	  private String UsrUpd;
		private String ApprovalNote;
		
		public String getApprovalNo() {
			return ApprovalNo;
		}
		public void setApprovalNo(String approvalNo) {
			ApprovalNo = approvalNo;
		}
		public Date getDtmUpd() {
			return DtmUpd;
		}
		public void setDtmUpd(Date dtmUpd) {
			DtmUpd = dtmUpd;
		}
		public String getUsrUpd() {
			return UsrUpd;
		}
		public void setUsrUpd(String usrUpd) {
			UsrUpd = usrUpd;
		}
		public String getApprovalNote() {
			return ApprovalNote;
		}
		public void setApprovalNote(String approvalNote) {
			ApprovalNote = approvalNote;
		}
		
		

}
