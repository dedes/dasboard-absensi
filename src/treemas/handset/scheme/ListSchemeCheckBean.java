package treemas.handset.scheme;

import java.util.List;

public class ListSchemeCheckBean {
    private List<SchemeCheckBean> list;

    public ListSchemeCheckBean() {
    }

    public List<SchemeCheckBean> getList() {
        return list;
    }

    public void setList(List<SchemeCheckBean> list) {
        this.list = list;
    }
}
