package treemas.handset.scheme;

public class ListUserApproval {
	 private String ApprovalSchemeID;
	  private Integer ApprovalSeqNum;
	  private String LoginID;
	  private String FullName;
	  private Float LimitValue;
	  private String ProcessDuration;
	  private String AutoUser;
	  private Float LimitPercent;
	  private String IsLimitByPercent;
	  private String FLAGPASS;
	  
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public Integer getApprovalSeqNum() {
		return ApprovalSeqNum;
	}
	public void setApprovalSeqNum(Integer approvalSeqNum) {
		ApprovalSeqNum = approvalSeqNum;
	}
	public String getLoginID() {
		return LoginID;
	}
	public void setLoginID(String loginID) {
		LoginID = loginID;
	}
	public String getFullName() {
		return FullName;
	}
	public void setFullName(String fullName) {
		FullName = fullName;
	}
	public Float getLimitValue() {
		return LimitValue;
	}
	public void setLimitValue(Float limitValue) {
		LimitValue = limitValue;
	}
	public String getProcessDuration() {
		return ProcessDuration;
	}
	public void setProcessDuration(String processDuration) {
		ProcessDuration = processDuration;
	}
	public String getAutoUser() {
		return AutoUser;
	}
	public void setAutoUser(String autoUser) {
		AutoUser = autoUser;
	}
	public Float getLimitPercent() {
		return LimitPercent;
	}
	public void setLimitPercent(Float limitPercent) {
		LimitPercent = limitPercent;
	}
	public String getIsLimitByPercent() {
		return IsLimitByPercent;
	}
	public void setIsLimitByPercent(String isLimitByPercent) {
		IsLimitByPercent = isLimitByPercent;
	}
	public String getFLAGPASS() {
		return FLAGPASS;
	}
	public void setFLAGPASS(String fLAGPASS) {
		FLAGPASS = fLAGPASS;
	}
	  
	  

}
