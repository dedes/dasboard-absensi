package treemas.handset.scheme;

import java.util.Date;

public class ListSchemeApproval {
	  private String ApprovalSchemeID;
	  private Integer ApprovalSeqNum;
	  private String LoginID;
	  private String FullName;
	  private long LimitValue;
	  private String ProcessDuration;
	  private String AutoUser;
	  private long LimitPercent;
	  private String IsLimitByPercent;
	  private Date DtmUpd;
	  private String UsrUpd;
	  
	public String getApprovalSchemeID() {
		return ApprovalSchemeID;
	}
	public void setApprovalSchemeID(String approvalSchemeID) {
		ApprovalSchemeID = approvalSchemeID;
	}
	public Integer getApprovalSeqNum() {
		return ApprovalSeqNum;
	}
	public void setApprovalSeqNum(Integer approvalSeqNum) {
		ApprovalSeqNum = approvalSeqNum;
	}
	public String getLoginID() {
		return LoginID;
	}
	public void setLoginID(String loginID) {
		LoginID = loginID;
	}

	public String getProcessDuration() {
		return ProcessDuration;
	}
	public void setProcessDuration(String processDuration) {
		ProcessDuration = processDuration;
	}
	public String getAutoUser() {
		return AutoUser;
	}
	public void setAutoUser(String autoUser) {
		AutoUser = autoUser;
	}

	public String getIsLimitByPercent() {
		return IsLimitByPercent;
	}
	public void setIsLimitByPercent(String isLimitByPercent) {
		IsLimitByPercent = isLimitByPercent;
	}
	public String getFullName() {
		return FullName;
	}
	public void setFullName(String fullName) {
		FullName = fullName;
	}
	public Date getDtmUpd() {
		return DtmUpd;
	}
	public void setDtmUpd(Date dtmUpd) {
		DtmUpd = dtmUpd;
	}
	public String getUsrUpd() {
		return UsrUpd;
	}
	public void setUsrUpd(String usrUpd) {
		UsrUpd = usrUpd;
	}
	public long getLimitValue() {
		return LimitValue;
	}
	public void setLimitValue(long limitValue) {
		LimitValue = limitValue;
	}
	public long getLimitPercent() {
		return LimitPercent;
	}
	public void setLimitPercent(long limitPercent) {
		LimitPercent = limitPercent;
	}
	  
	  
	
}
