package treemas.handset.scheme;

import java.util.Date;

public class SchemeCheckBean {
    private String scheme_id;
    private Date scheme_last_update;

    public String getScheme_id() {
        return scheme_id;
    }

    public void setScheme_id(String scheme_id) {
        this.scheme_id = scheme_id;
    }

    public Date getScheme_last_update() {
        return scheme_last_update;
    }

    public void setScheme_last_update(Date scheme_last_update) {
        this.scheme_last_update = scheme_last_update;
    }


}	
