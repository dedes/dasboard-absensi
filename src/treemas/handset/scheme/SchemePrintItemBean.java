package treemas.handset.scheme;

import java.io.Serializable;

public class SchemePrintItemBean implements Serializable {

    private String schemeId;
    private String schemeDescription;
    private Integer printItemSeqno;
    private String printItemTypeId;
    private String printItemTypeName;
    private String printItemLabel;
    private Integer lineSeqOrder;
    private String questionGroupId;
    private String questionGroupName;
    private String questionId;

    public SchemePrintItemBean() {
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public Integer getPrintItemSeqno() {
        return printItemSeqno;
    }

    public void setPrintItemSeqno(Integer printItemSeqno) {
        this.printItemSeqno = printItemSeqno;
    }

    public String getPrintItemTypeId() {
        return printItemTypeId;
    }

    public void setPrintItemTypeId(String printItemTypeId) {
        this.printItemTypeId = printItemTypeId;
    }

    public String getPrintItemLabel() {
        return printItemLabel;
    }

    public void setPrintItemLabel(String printItemLabel) {
        this.printItemLabel = printItemLabel;
    }

    public Integer getLineSeqOrder() {
        return lineSeqOrder;
    }

    public void setLineSeqOrder(Integer lineSeqOrder) {
        this.lineSeqOrder = lineSeqOrder;
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getPrintItemTypeName() {
        return printItemTypeName;
    }

    public void setPrintItemTypeName(String printItemTypeName) {
        this.printItemTypeName = printItemTypeName;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

}
