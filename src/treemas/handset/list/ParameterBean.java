package treemas.handset.list;

public class ParameterBean {
    //	PageNumber":p, "ItemsPerPage":n,"PlanDate":"yyyy-mm-dd
    private Integer PageNumber;
    private Integer ItemsPerPage;
    private String PlanDate;

    public ParameterBean() {
    }

    public Integer getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        PageNumber = pageNumber;
    }

    public Integer getItemsPerPage() {
        return ItemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        ItemsPerPage = itemsPerPage;
    }

    public String getPlanDate() {
        return PlanDate;
    }

    public void setPlanDate(String planDate) {
        PlanDate = planDate;
    }


}
