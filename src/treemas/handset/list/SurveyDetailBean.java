package treemas.handset.list;

public class SurveyDetailBean {
    private String tipe_rumah;// TIPE RUMAH
    private String kondisi_lingkungan_sekitar;// KONDISI LINGKUNGAN SEKITAR
    private String permukaan_jalan_depan_rumah;// PERMUKAAN JALAN DEPAN RUMAH
    private String keadaan_jalan_menuju_lokasi;// KEADAAN JALAN MENUJU LOKASI -
    // dapat dilewati oleh
    private String garasi_carport;// GARASI/CARPORT
    private String bentu_bangunan;// BENTUK BANGUNAN
    private String kualitas_bangunan;// KUALITAS BANGUNAN
    private String perkiraan_usia_bangunan;// PERKIRAAN USIA BANGUNAN
    private String kondisi_bangunan;// KONDISI BANGUNAN
    private String perabotan_peralatan;// PERABOTAN / PERALATAN (ELEKTRONIK) DI
    // DALAM RUMAH
    private Integer jumlah_kamar;// JUMLAH KAMAR
    private String status_kepemilikan_rumah;// STATUS KEPEMILIKAN RUMAH
    private String luas_tanah_bangunan;// LUAS TANAH/BANGUNAN
    private Integer lama_tinggal_bulan;// LAMA TINGGAL BULAN
    private Integer lama_tinggal_tahun;// LAMA TINGGAL TAHUN
    private Integer jumlah_kendaraan_saat_ini;// JUMLAH KENDARAAN YANG DIMILIKI
    // SAAT INI
    private String status_kendaraan_saat_ini;// STATUS KENDARAAN YANG DIMILIKI
    // SAAT INI
    private String pembelian_kendaraan_baru_untuk;// PEMBELIAN KENDARAAN BARU
    // UNTUK
    private String asal_perolehan_pembayaran_pertama;// ASAL PEROLEHAN
    // PEMBAYARAN PERTAMA
    private String stnk_bpkb_atas_nama;// STNK / BPKB ATAS NAMA
    private String pembayaran_angsuran_yang_diinginkan;// PEMBAYARAN ANGSURAN
    // YANG DIINGINKAN
    private String nama_keluarga_dihubungi;// NAMA ANGGOTA KELUARGA TIDAK
    // SERUMAH YANG DAPAT DIHUBUNGI
    private String hubungan_keluarga_dihubungi;// HUBUNGAN ANGGOTA KELUARGA
    // TIDAK SERUMAH YANG DAPAT
    // DIHUBUNGI
    private String alamat_keluarga_dihubungi;// ALAMAT ANGGOTA KELUARGA TIDAK
    // SERUMAH YANG DAPAT DIHUBUNGI
    private String telepon_keluarga_dihubungi;// TELEPON ANGGOTA KELUARGA TIDAK
    // SERUMAH YANG DAPAT DIHUBUNGI
    private double langitude_rumah;// DENAH
    private double longitude_rumah;// DENAH
    private double accuracy;// DENAH
    private double omset_penjualan;// Omset Penjualan / Pendapatan (a)
    private double harga_pokok_penjualan;// Harga Pokok Penjualan / Biaya-biaya
    // (b)
    private double margin;// Margin
    private double pendapatan;// Pendapatan / (Biaya Lain-lain) (d)
    private double pendapatan_bersih;// Pendapatan Bersih
    private String kemauan_bayar;// Character (Kemauan Bayar)
    private String capacity;// Capacity (Kemampuan Bayar)
    private String informasi_tambahan;// Informasi Tambahan
    private String nama_cek_lingkungan;// Nama Cek Lingkungan
    private String hubungan_cek_lingkungan;// Hubungan Cek Lingkungan
    private String informasi_yang_diperoleh;// Informasi yang Diperoleh
    private String foto_kontrak_base64;// Foto Penandatanganan Kontrak
    private String foto_tempat_tinggal;// Foto Tempat Tinggal
    private String cabang;// Cabang
    private String foto_tampak_depan;// Tampak Depan
    private String foto_tampak_belakang;// Tampak Belakang
    private String foto_tampak_samping_kanan;// Tampak Samping Kanan
    private String foto_tampak_samping_kiri;// Tampak Samping Kiri


    public SurveyDetailBean() {
    }


    public String getTipe_rumah() {
        return tipe_rumah;
    }


    public void setTipe_rumah(String tipe_rumah) {
        this.tipe_rumah = tipe_rumah;
    }


    public String getKondisi_lingkungan_sekitar() {
        return kondisi_lingkungan_sekitar;
    }


    public void setKondisi_lingkungan_sekitar(String kondisi_lingkungan_sekitar) {
        this.kondisi_lingkungan_sekitar = kondisi_lingkungan_sekitar;
    }


    public String getPermukaan_jalan_depan_rumah() {
        return permukaan_jalan_depan_rumah;
    }


    public void setPermukaan_jalan_depan_rumah(String permukaan_jalan_depan_rumah) {
        this.permukaan_jalan_depan_rumah = permukaan_jalan_depan_rumah;
    }


    public String getKeadaan_jalan_menuju_lokasi() {
        return keadaan_jalan_menuju_lokasi;
    }


    public void setKeadaan_jalan_menuju_lokasi(String keadaan_jalan_menuju_lokasi) {
        this.keadaan_jalan_menuju_lokasi = keadaan_jalan_menuju_lokasi;
    }


    public String getGarasi_carport() {
        return garasi_carport;
    }


    public void setGarasi_carport(String garasi_carport) {
        this.garasi_carport = garasi_carport;
    }


    public String getBentu_bangunan() {
        return bentu_bangunan;
    }


    public void setBentu_bangunan(String bentu_bangunan) {
        this.bentu_bangunan = bentu_bangunan;
    }


    public String getKualitas_bangunan() {
        return kualitas_bangunan;
    }


    public void setKualitas_bangunan(String kualitas_bangunan) {
        this.kualitas_bangunan = kualitas_bangunan;
    }


    public String getPerkiraan_usia_bangunan() {
        return perkiraan_usia_bangunan;
    }


    public void setPerkiraan_usia_bangunan(String perkiraan_usia_bangunan) {
        this.perkiraan_usia_bangunan = perkiraan_usia_bangunan;
    }


    public String getKondisi_bangunan() {
        return kondisi_bangunan;
    }


    public void setKondisi_bangunan(String kondisi_bangunan) {
        this.kondisi_bangunan = kondisi_bangunan;
    }


    public String getPerabotan_peralatan() {
        return perabotan_peralatan;
    }


    public void setPerabotan_peralatan(String perabotan_peralatan) {
        this.perabotan_peralatan = perabotan_peralatan;
    }


    public Integer getJumlah_kamar() {
        return jumlah_kamar;
    }


    public void setJumlah_kamar(Integer jumlah_kamar) {
        this.jumlah_kamar = jumlah_kamar;
    }


    public String getStatus_kepemilikan_rumah() {
        return status_kepemilikan_rumah;
    }


    public void setStatus_kepemilikan_rumah(String status_kepemilikan_rumah) {
        this.status_kepemilikan_rumah = status_kepemilikan_rumah;
    }


    public String getLuas_tanah_bangunan() {
        return luas_tanah_bangunan;
    }


    public void setLuas_tanah_bangunan(String luas_tanah_bangunan) {
        this.luas_tanah_bangunan = luas_tanah_bangunan;
    }


    public Integer getLama_tinggal_bulan() {
        return lama_tinggal_bulan;
    }


    public void setLama_tinggal_bulan(Integer lama_tinggal_bulan) {
        this.lama_tinggal_bulan = lama_tinggal_bulan;
    }


    public Integer getLama_tinggal_tahun() {
        return lama_tinggal_tahun;
    }


    public void setLama_tinggal_tahun(Integer lama_tinggal_tahun) {
        this.lama_tinggal_tahun = lama_tinggal_tahun;
    }


    public Integer getJumlah_kendaraan_saat_ini() {
        return jumlah_kendaraan_saat_ini;
    }


    public void setJumlah_kendaraan_saat_ini(Integer jumlah_kendaraan_saat_ini) {
        this.jumlah_kendaraan_saat_ini = jumlah_kendaraan_saat_ini;
    }


    public String getStatus_kendaraan_saat_ini() {
        return status_kendaraan_saat_ini;
    }


    public void setStatus_kendaraan_saat_ini(String status_kendaraan_saat_ini) {
        this.status_kendaraan_saat_ini = status_kendaraan_saat_ini;
    }


    public String getPembelian_kendaraan_baru_untuk() {
        return pembelian_kendaraan_baru_untuk;
    }


    public void setPembelian_kendaraan_baru_untuk(
            String pembelian_kendaraan_baru_untuk) {
        this.pembelian_kendaraan_baru_untuk = pembelian_kendaraan_baru_untuk;
    }


    public String getAsal_perolehan_pembayaran_pertama() {
        return asal_perolehan_pembayaran_pertama;
    }


    public void setAsal_perolehan_pembayaran_pertama(
            String asal_perolehan_pembayaran_pertama) {
        this.asal_perolehan_pembayaran_pertama = asal_perolehan_pembayaran_pertama;
    }


    public String getStnk_bpkb_atas_nama() {
        return stnk_bpkb_atas_nama;
    }


    public void setStnk_bpkb_atas_nama(String stnk_bpkb_atas_nama) {
        this.stnk_bpkb_atas_nama = stnk_bpkb_atas_nama;
    }


    public String getPembayaran_angsuran_yang_diinginkan() {
        return pembayaran_angsuran_yang_diinginkan;
    }


    public void setPembayaran_angsuran_yang_diinginkan(
            String pembayaran_angsuran_yang_diinginkan) {
        this.pembayaran_angsuran_yang_diinginkan = pembayaran_angsuran_yang_diinginkan;
    }


    public String getNama_keluarga_dihubungi() {
        return nama_keluarga_dihubungi;
    }


    public void setNama_keluarga_dihubungi(String nama_keluarga_dihubungi) {
        this.nama_keluarga_dihubungi = nama_keluarga_dihubungi;
    }


    public String getHubungan_keluarga_dihubungi() {
        return hubungan_keluarga_dihubungi;
    }


    public void setHubungan_keluarga_dihubungi(String hubungan_keluarga_dihubungi) {
        this.hubungan_keluarga_dihubungi = hubungan_keluarga_dihubungi;
    }


    public String getAlamat_keluarga_dihubungi() {
        return alamat_keluarga_dihubungi;
    }


    public void setAlamat_keluarga_dihubungi(String alamat_keluarga_dihubungi) {
        this.alamat_keluarga_dihubungi = alamat_keluarga_dihubungi;
    }


    public String getTelepon_keluarga_dihubungi() {
        return telepon_keluarga_dihubungi;
    }


    public void setTelepon_keluarga_dihubungi(String telepon_keluarga_dihubungi) {
        this.telepon_keluarga_dihubungi = telepon_keluarga_dihubungi;
    }


    public double getLangitude_rumah() {
        return langitude_rumah;
    }


    public void setLangitude_rumah(double langitude_rumah) {
        this.langitude_rumah = langitude_rumah;
    }


    public double getLongitude_rumah() {
        return longitude_rumah;
    }


    public void setLongitude_rumah(double longitude_rumah) {
        this.longitude_rumah = longitude_rumah;
    }


    public double getAccuracy() {
        return accuracy;
    }


    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }


    public double getOmset_penjualan() {
        return omset_penjualan;
    }


    public void setOmset_penjualan(double omset_penjualan) {
        this.omset_penjualan = omset_penjualan;
    }


    public double getHarga_pokok_penjualan() {
        return harga_pokok_penjualan;
    }


    public void setHarga_pokok_penjualan(double harga_pokok_penjualan) {
        this.harga_pokok_penjualan = harga_pokok_penjualan;
    }


    public double getMargin() {
        return margin;
    }


    public void setMargin(double margin) {
        this.margin = margin;
    }


    public double getPendapatan() {
        return pendapatan;
    }


    public void setPendapatan(double pendapatan) {
        this.pendapatan = pendapatan;
    }


    public double getPendapatan_bersih() {
        return pendapatan_bersih;
    }


    public void setPendapatan_bersih(double pendapatan_bersih) {
        this.pendapatan_bersih = pendapatan_bersih;
    }


    public String getKemauan_bayar() {
        return kemauan_bayar;
    }


    public void setKemauan_bayar(String kemauan_bayar) {
        this.kemauan_bayar = kemauan_bayar;
    }


    public String getCapacity() {
        return capacity;
    }


    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }


    public String getInformasi_tambahan() {
        return informasi_tambahan;
    }


    public void setInformasi_tambahan(String informasi_tambahan) {
        this.informasi_tambahan = informasi_tambahan;
    }


    public String getNama_cek_lingkungan() {
        return nama_cek_lingkungan;
    }


    public void setNama_cek_lingkungan(String nama_cek_lingkungan) {
        this.nama_cek_lingkungan = nama_cek_lingkungan;
    }


    public String getHubungan_cek_lingkungan() {
        return hubungan_cek_lingkungan;
    }


    public void setHubungan_cek_lingkungan(String hubungan_cek_lingkungan) {
        this.hubungan_cek_lingkungan = hubungan_cek_lingkungan;
    }


    public String getInformasi_yang_diperoleh() {
        return informasi_yang_diperoleh;
    }


    public void setInformasi_yang_diperoleh(String informasi_yang_diperoleh) {
        this.informasi_yang_diperoleh = informasi_yang_diperoleh;
    }


    public String getFoto_kontrak_base64() {
        return foto_kontrak_base64;
    }


    public void setFoto_kontrak_base64(String foto_kontrak_base64) {
        this.foto_kontrak_base64 = foto_kontrak_base64;
    }


    public String getFoto_tempat_tinggal() {
        return foto_tempat_tinggal;
    }


    public void setFoto_tempat_tinggal(String foto_tempat_tinggal) {
        this.foto_tempat_tinggal = foto_tempat_tinggal;
    }


    public String getCabang() {
        return cabang;
    }


    public void setCabang(String cabang) {
        this.cabang = cabang;
    }


    public String getFoto_tampak_depan() {
        return foto_tampak_depan;
    }


    public void setFoto_tampak_depan(String foto_tampak_depan) {
        this.foto_tampak_depan = foto_tampak_depan;
    }


    public String getFoto_tampak_belakang() {
        return foto_tampak_belakang;
    }


    public void setFoto_tampak_belakang(String foto_tampak_belakang) {
        this.foto_tampak_belakang = foto_tampak_belakang;
    }


    public String getFoto_tampak_samping_kanan() {
        return foto_tampak_samping_kanan;
    }


    public void setFoto_tampak_samping_kanan(String foto_tampak_samping_kanan) {
        this.foto_tampak_samping_kanan = foto_tampak_samping_kanan;
    }


    public String getFoto_tampak_samping_kiri() {
        return foto_tampak_samping_kiri;
    }


    public void setFoto_tampak_samping_kiri(String foto_tampak_samping_kiri) {
        this.foto_tampak_samping_kiri = foto_tampak_samping_kiri;
    }


}
