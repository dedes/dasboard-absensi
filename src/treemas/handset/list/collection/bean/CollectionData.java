package treemas.handset.list.collection.bean;

public class CollectionData {
    private Integer mobile_assignment_id;
    private Integer question_group_id;
    private Integer question_id;
    private String answer_type_id;
    private Integer option_answer_id;
    private Integer lookup_id;
    private String text_answer;
    private String option_answer_text;
    private Double gps_longitude;
    private Double gps_latitude;
    private Double mcc;
    private Double mnc;
    private Double lac;
    private Double cellid;
    private Boolean is_gps;
    private Integer accuracy;

    public CollectionData() {
    }

    public Integer getMobile_assignment_id() {
        return mobile_assignment_id;
    }

    public void setMobile_assignment_id(Integer mobile_assignment_id) {
        this.mobile_assignment_id = mobile_assignment_id;
    }

    public Integer getQuestion_group_id() {
        return question_group_id;
    }

    public void setQuestion_group_id(Integer question_group_id) {
        this.question_group_id = question_group_id;
    }

    public Integer getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(Integer question_id) {
        this.question_id = question_id;
    }

    public String getAnswer_type_id() {
        return answer_type_id;
    }

    public void setAnswer_type_id(String answer_type_id) {
        this.answer_type_id = answer_type_id;
    }

    public Integer getOption_answer_id() {
        return option_answer_id;
    }

    public void setOption_answer_id(Integer option_answer_id) {
        this.option_answer_id = option_answer_id;
    }

    public Integer getLookup_id() {
        return lookup_id;
    }

    public void setLookup_id(Integer lookup_id) {
        this.lookup_id = lookup_id;
    }

    public String getText_answer() {
        return text_answer;
    }

    public void setText_answer(String text_answer) {
        this.text_answer = text_answer;
    }

    public String getOption_answer_text() {
        return option_answer_text;
    }

    public void setOption_answer_text(String option_answer_text) {
        this.option_answer_text = option_answer_text;
    }

    public Double getGps_longitude() {
        return gps_longitude;
    }

    public void setGps_longitude(Double gps_longitude) {
        this.gps_longitude = gps_longitude;
    }

    public Double getGps_latitude() {
        return gps_latitude;
    }

    public void setGps_latitude(Double gps_latitude) {
        this.gps_latitude = gps_latitude;
    }

    public Double getMcc() {
        return mcc;
    }

    public void setMcc(Double mcc) {
        this.mcc = mcc;
    }

    public Double getMnc() {
        return mnc;
    }

    public void setMnc(Double mnc) {
        this.mnc = mnc;
    }

    public Double getLac() {
        return lac;
    }

    public void setLac(Double lac) {
        this.lac = lac;
    }

    public Double getCellid() {
        return cellid;
    }

    public void setCellid(Double cellid) {
        this.cellid = cellid;
    }

    public Boolean isIs_gps() {
        return is_gps;
    }

    public void setIs_gps(Boolean is_gps) {
        this.is_gps = is_gps;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }


}
