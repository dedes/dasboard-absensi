package treemas.handset.list.collection.bean;

import treemas.handset.list.HeaderBean;

import java.util.List;

public class SingleCollection {
    private Integer mobile_assignment_id;
    private String app_id;
    private String nama_pemohon;
    private String alamat_pemohon;
    private String telepon;
    private String HP;
    private String notes;
    private String scheme_id;
    private List<CollectionData> listCollectionData;

    public SingleCollection() {
    }

    public SingleCollection(HeaderBean bean) {
        this.mobile_assignment_id = bean.getId();
        this.app_id = bean.getApp_id();
        this.nama_pemohon = bean.getNama_pemohon();
        this.alamat_pemohon = bean.getAlamat_pemohon();
        this.telepon = bean.getTelepon();
        this.HP = bean.getHP();
        this.notes = bean.getNotes();
        this.scheme_id = bean.getScheme_id();
    }

    public Integer getMobile_assignment_id() {
        return mobile_assignment_id;
    }

    public void setMobile_assignment_id(Integer mobile_assignment_id) {
        this.mobile_assignment_id = mobile_assignment_id;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getNama_pemohon() {
        return nama_pemohon;
    }

    public void setNama_pemohon(String nama_pemohon) {
        this.nama_pemohon = nama_pemohon;
    }

    public String getAlamat_pemohon() {
        return alamat_pemohon;
    }

    public void setAlamat_pemohon(String alamat_pemohon) {
        this.alamat_pemohon = alamat_pemohon;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getHP() {
        return HP;
    }

    public void setHP(String hP) {
        HP = hP;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<CollectionData> getListCollectionData() {
        return listCollectionData;
    }

    public void setListCollectionData(List<CollectionData> listCollectionData) {
        this.listCollectionData = listCollectionData;
    }

    public String getScheme_id() {
        return scheme_id;
    }

    public void setScheme_id(String scheme_id) {
        this.scheme_id = scheme_id;
    }

}
