package treemas.handset.list.collection.bean;

import java.util.List;

public class CollectionBean {
    private Integer TotalItems;
    private List<SingleCollection> PageOfResults;

    public CollectionBean() {
    }

    public Integer getTotalItems() {
        return TotalItems;
    }

    public void setTotalItems(Integer totalItems) {
        TotalItems = totalItems;
    }

    public List<SingleCollection> getPageOfResults() {
        return PageOfResults;
    }

    public void setPageOfResults(List<SingleCollection> pageOfResults) {
        PageOfResults = pageOfResults;
    }
}
