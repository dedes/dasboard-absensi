package treemas.handset.list.collection;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import treemas.base.handset.ServletBase;
import treemas.handset.list.ParameterBean;
import treemas.handset.login.LoginBean;
import treemas.util.cryptography.MD5;
import treemas.util.encoder.Base64;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public class CollectionServlet extends ServletBase {
    private boolean enEnabled = true;
    private boolean deEnabled = true;

    private String url = "/m/collectionlist/";
    private String delimiter = ":";
    private String rkm = "RKM Android User";

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String message = "User not authorized";
        PrintWriter out = resp.getWriter();
        System.out.println("Android calling /m/collectionlist/.... method get");

        String IMEI = null;
        String userName = null;
        String password = null;

        boolean isAuthorized = false;
        String token = req.getHeader("Authorization");
        if (!Strings.isNullOrEmpty(token)) {

            if (token.startsWith("Basic ")) {
                token = token.replace("Basic ", "");
                String decodeToken = new String(Base64.decode(token));
                String[] dataToken = decodeToken.split(this.delimiter);
                if (dataToken.length == 4) {

                    IMEI = dataToken[0];
                    password = dataToken[1];
                    userName = dataToken[2];
                    rkm = dataToken[3];
                    isAuthorized = true;
                }
            } else {
                isAuthorized = false;
            }
        } else {
            isAuthorized = false;
        }

        boolean isTrue = false;
//		   MobileCollections/m/servletdemo/?json={"PageNumber":1, "ItemsPerPage":10,"PlanDate":"2014-11-10"}
        String data = req.getParameter("json");
//		isAuthorized = true;
//		data = "{\"PageNumber\":1, \"ItemsPerPage\":10,\"PlanDate\":\"2014-11-10\"}";
        if (!Strings.isNullOrEmpty(data) && isAuthorized) {
            isTrue = true;
        } else {
            isTrue = false;
        }

//		IMEI = "352879066025843";
//		userName="COLL001";
//		password="12345678";
//		isTrue = true;
        if (isTrue) {
            try {
                LoginBean lb = new LoginBean();
                lb.setIMEI(IMEI);
                lb.setUserName(userName);
                lb.setUserPassword(MD5.getInstance().hashData(password));
                ParameterBean pBean = new Gson().fromJson(data, ParameterBean.class);
                message = this.demoManager.getCollectionList(lb, pBean);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

        }

        if (isTrue) {
            out.write(message);
        } else {
            String ret =
                    "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">"
                            + message + "</string>";
            System.out.println(ret);
            out.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            out.append(ret);
        }

    }
}
