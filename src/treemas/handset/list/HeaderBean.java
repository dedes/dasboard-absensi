package treemas.handset.list;

public class HeaderBean {
    private Integer id;
    private String app_id;
    private String nama_pemohon;
    private String alamat_pemohon;
    private String telepon;
    private String HP;
    private String notes;
    private String scheme_id;

    private String nik;
    private String noaddr;
    private String addressStreet1;
    private String kelurahan;
    private String kecamatan;
    private String kabupaten;
    private String propinsi;
    private String city;
    private String negara;
    private String kodePos;
    private Double gpsLongitude;
    private Double gpsLatitude;
    //IRFAN 17-12-2018
    private String rt;
    private String rw;
    private String areaPhone1;
    //IRFAN 17-12-2018

    public HeaderBean() {
    }

	public String getNama_pemohon() {
        return nama_pemohon;
    }

    public void setNama_pemohon(String nama_pemohon) {
        this.nama_pemohon = nama_pemohon;
    }

    public String getAlamat_pemohon() {
        return alamat_pemohon;
    }

    public void setAlamat_pemohon(String alamat_pemohon) {
        this.alamat_pemohon = alamat_pemohon;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getHP() {
        return HP;
    }

    public void setHP(String hP) {
        HP = hP;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getScheme_id() {
        return scheme_id;
    }

    public void setScheme_id(String scheme_id) {
        this.scheme_id = scheme_id;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNoaddr() {
        return noaddr;
    }

    public void setNoaddr(String noaddr) {
        this.noaddr = noaddr;
    }

    public String getAddressStreet1() {
        return addressStreet1;
    }

    public void setAddressStreet1(String addressStreet1) {
        this.addressStreet1 = addressStreet1;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public String getNegara() {
        return negara;
    }

    public void setNegara(String negara) {
        this.negara = negara;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    //IRFAN 17-12-2018
	public String getRt() {
		return rt;
	}

	public void setRt(String rt) {
		this.rt = rt;
	}

	public String getRw() {
		return rw;
	}

	public void setRw(String rw) {
		this.rw = rw;
	}
	
	 public String getAreaPhone1() {
			return areaPhone1;
	 }

		public void setAreaPhone1(String areaPhone1) {
		this.areaPhone1 = areaPhone1;
		}

    
 //  END
  
}
