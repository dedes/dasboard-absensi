package treemas.handset.list.survey;

import treemas.handset.list.HeaderBean;

import java.util.List;


public class SurveyBeanList {
    private Integer TotalItems;
    private List<HeaderBean> PageOfResults;

    public SurveyBeanList() {
    }

    public Integer getTotalItems() {
        return TotalItems;
    }

    public void setTotalItems(Integer totalItems) {
        TotalItems = totalItems;
    }

    public List<HeaderBean> getPageOfResults() {
        return PageOfResults;
    }

    public void setPageOfResults(List<HeaderBean> pageOfResults) {
        PageOfResults = pageOfResults;
    }


}
