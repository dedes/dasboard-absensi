package treemas.util.report.text;

import java.util.Map;

public class Calc extends ReportElement {
    public static final int TYPE_COUNT = 1;
    public static final int TYPE_SUM = 2;
    private int type;
    private Group owner;
    private String fieldName;
    private double value;

    public Calc(TextReport report, Group owner, int type, String fieldName) {
        super(report);
        if (type != TYPE_COUNT && type != TYPE_SUM)
            throw new IllegalArgumentException("Invalid FieldCalc type. Please use constant from FieldCalc class");
        this.owner = owner;
        this.fieldName = fieldName;
        this.type = type;
        this.reset();
    }

    void calculate(Map rowData) {
        Object obj = ((DataValueWrapper) rowData.get(this.fieldName)).getValue();
        if (this.type == TYPE_SUM) {
            if (obj instanceof java.lang.Integer) {
                this.value += ((Integer) obj).intValue();
            } else if (obj instanceof java.lang.Long) {
                this.value += ((Long) obj).longValue();
            } else if (obj instanceof java.lang.Double) {
                this.value += ((Double) obj).doubleValue();
            } else
                throw new IllegalArgumentException("Unsupported data class: " +
                        obj.getClass().getName() + ". Value=" + obj.toString());
        } else
            this.value++;
    }

    void reset() {
        this.value = 0;
    }

    public double getValue() {
        return this.value;
    }

    Group getOwner() {
        return this.owner;
    }
}
