package treemas.util.report.text;

import java.util.Iterator;
import java.util.Map;


public class Group extends Detail {
    private String field;
    private GroupHeader header;
    private GroupFooter footer;
    private Detail detail;

    private String lastFieldValue;
    private int lastPageCounter;
    private boolean resetRowNumberOnNewGroup = false;
    private boolean newPageOnNewGroup;
    private String newPageSign;
    private Group parentGroup;
    private boolean resetPageNumberOnNewGroup;

    public Group(TextReport report, Group parent, String field) {
        super(report);
        this.field = field;
        this.newPageSign = report.getPageBreak();
        this.parentGroup = parent;
    }

    public void setHeader(GroupHeader header) {
        this.header = header;
    }

    public void setFooter(GroupFooter footer) {
        this.footer = footer;
    }

    public String getNewPageSign() {
        return this.newPageSign;
    }

    public void setNewPageSign(String sign) {
        if (sign != null) {
            sign = sign.replaceAll("\\r", "\r");
            sign = sign.replaceAll("\\n", "\n");
        }
        this.newPageSign = sign;
    }

    public Group getParent() {
        return this.parentGroup;
    }

    public void add(Line line) {
        throw new RuntimeException("Please use detail instead of adding line directly");
    }

    public void print(StringBuffer sb, Map rowData) {
        String fieldValue = ((DataValueWrapper) rowData.get(this.field)).
                toString();

        int pageCounter = this.report.getPageCounter();


        boolean valueChange = this.isValueChange(rowData);
        boolean pageChange = pageCounter != this.lastPageCounter;

        if (valueChange) {
            this.report.getCalcManager().resetForGroup(this);

            if (this.resetRowNumberOnNewGroup) {
                this.report.resetRowNum();
            }
        }

        this.report.getCalcManager().calculateForGroup(this, rowData);

        if (valueChange || pageChange) {
            this.resetLastPrinted();
            if (this.header != null) {
                if (valueChange || (pageChange && this.header.isPrintOnNewPage()))
                    this.header.print(sb, rowData);
            }
        }

        if (this.detail != null)
            this.detail.print(sb, rowData);

        if (this.isNextValueChange(rowData)) { // last record or next record's key field is different
            if (this.footer != null)
                this.footer.print(sb, rowData);

            if (this.resetPageNumberOnNewGroup) {
                this.report.resetPageNum();
            }

            if (this.newPageOnNewGroup)
                this.report.changePage(this.newPageSign);
        }

        this.lastFieldValue = fieldValue;
        // re-read page counter in case a page break occured when printing detail
        this.lastPageCounter = this.report.getPageCounter();
    }

    public Detail getDetail() {
        return this.detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public boolean isResetRowNumberOnNewGroup() {
        return resetRowNumberOnNewGroup;
    }

    public void setResetRowNumberOnNewGroup(boolean resetRowNumberOnNewGroup) {
        this.resetRowNumberOnNewGroup = resetRowNumberOnNewGroup;
    }

    public boolean isNewPageOnNewGroup() {
        return newPageOnNewGroup;
    }

    public void setNewPageOnNewGroup(boolean newPageOnNewGroup) {
        this.newPageOnNewGroup = newPageOnNewGroup;
    }

    public boolean isResetPageNumberOnNewGroup() {
        return resetPageNumberOnNewGroup;
    }

    public void setResetPageNumberOnNewGroup(boolean resetPageNumberOnNewGroup) {
        this.resetPageNumberOnNewGroup = resetPageNumberOnNewGroup;
    }

    private void resetLastPrinted() {
        if (this.detail == null)
            return;
        Iterator iter = this.detail.lines.iterator();
        while (iter.hasNext()) {
            Line line = (Line) iter.next();
            Iterator iter2 = line.getElementsIterator();
            while (iter2.hasNext()) {
                PrintableElement element = (PrintableElement) iter2.next();
                if (element instanceof Field) {
                    ((Field) element).resetLastPrinted();
                }
            }
        }
    }

    protected boolean isValueChange(Map rowData) {
        boolean result = false;
        if (this.parentGroup != null)  // checks parent first
            result = this.parentGroup.isValueChange(rowData);
        if (!result) {
            String fieldValue = ((DataValueWrapper) rowData.get(this.field)).
                    toString();
            result = !fieldValue.equals(this.lastFieldValue);
        }
        return result;
    }

    protected boolean isNextValueChange(Map currentRowData) {
        boolean parentChange = false;
        boolean thisChange = false;
        if (this.parentGroup != null)
            parentChange = this.parentGroup.isNextValueChange(currentRowData);


        if (!parentChange) {
            Map nextRow = this.report.peekNextData();
            String fieldValue = ((DataValueWrapper) currentRowData.get(this.field)).toString();
            String nextValue = nextRow == null ? null : ((DataValueWrapper) nextRow.get(this.field)).toString();
            thisChange = !fieldValue.equals(nextValue);
        }
        return (parentChange || thisChange);
    }

}
