package treemas.util.report.text;


public abstract class ReportElement {
    protected TextReport report;

    public ReportElement(TextReport report) {
        this.report = report;
    }
}
