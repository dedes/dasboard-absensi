package treemas.util.report.text;

import java.util.Map;

public class Label extends PrintableElement {
    private String text;

    public Label(TextReport report, int start, int align, int width, String txt) {
        super(report, start, align, width);
        this.text = txt;
    }

    public void print(StringBuffer sb) {
        super.alignAndPrint(sb, this.text);
    }

    public void print(StringBuffer sb, Map rowData) {
        this.print(sb);
    }

}
