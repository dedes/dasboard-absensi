package treemas.util.report.text;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;


public class DataValueWrapper {
    private String text;
    private Object value;

    private DataValueWrapper() {
    }

    public static DataValueWrapper createWrapper(String text) {
        DataValueWrapper result = new DataValueWrapper();
        result.value = text;
        result.text = text == null ? "" : text;
        return result;
    }

    public static DataValueWrapper createWrapper(Timestamp val, String format) {
        DataValueWrapper result = new DataValueWrapper();
        result.value = val;
        if (format != null) {
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            result.text = val == null ? "" : formatter.format(val);
        } else
            result.text = val.toString();
        return result;
    }

    public static DataValueWrapper createWrapper(int val) {
        DataValueWrapper result = new DataValueWrapper();
        result.value = new Integer(val);
        result.text = String.valueOf(val);
        return result;
    }


    public static DataValueWrapper createWrapper(double val, String format, char groupSeparator, char decimalSeparator) {
        DataValueWrapper result = new DataValueWrapper();
        result.value = new Double(val);
        DecimalFormat formatter = null;
        if (format != null) {
            formatter = new DecimalFormat(format);
        } else {
            formatter = new DecimalFormat();
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(groupSeparator);
        symbols.setDecimalSeparator(decimalSeparator);
        formatter.setDecimalFormatSymbols(symbols);
        result.text = formatter.format(val);
        return result;
    }

    public String toString() {
        return this.text;
    }

    public Object getValue() {
        return this.value;
    }
}
