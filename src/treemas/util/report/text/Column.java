package treemas.util.report.text;

public class Column {
    private Class type;
    private String name;
    private String format;

    private int intLength = 0;
    private int fractionLength = 0;

    public Column(String colName, Class colType) {
        this(colName, colType, null);
    }

    public Column(String colName, Class colType, String format) {
        this.name = colName;
        this.type = colType;
        this.format = format;
    }

    public Class getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

    public String getFormat() {
        return this.format;
    }

    public int getIntLength() {
        return intLength;
    }

    public void setIntLength(int intLength) {
        this.intLength = intLength;
    }

    public int getFractionLength() {
        return fractionLength;
    }

    public void setFractionLength(int fractionLength) {
        this.fractionLength = fractionLength;
    }
}
