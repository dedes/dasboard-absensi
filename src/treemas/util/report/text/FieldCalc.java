package treemas.util.report.text;

import java.util.Map;


public class FieldCalc
        extends PrintableElement {
    private String name;
    private Group owner;


    public FieldCalc(TextReport report, String name, int start,
                     int align, int width) {
        super(report, start, align, width);
        this.owner = owner;
        this.name = name;
    }

    public void print(StringBuffer sb, Map rowData) {
        Calc calc = this.report.getCalcManager().getCalc(this.name);
        if (calc == null)
            throw new RuntimeException("Cannot find Calc " + this.name);
        super.alignAndPrint(sb,
                DataValueWrapper.createWrapper(calc.getValue(),
                        this.report.getNumberFormat(), this.report.getThousandSeparator(), this.report.getDecimalSeparator()));
    }

    public String getName() {
        return this.name;
    }

}
