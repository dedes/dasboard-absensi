package treemas.util.report.text;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

public class ReportTextDtdResolver implements EntityResolver {
    private static final String REPORT_TEXT_DTD = "treemas/report/text/treemas-rpt-text.dtd";

    ReportTextDtdResolver() {
    }

    public InputSource resolveEntity(String publicId,
                                     String systemId) throws IOException, SAXException {
        InputSource result = null;
        if (systemId.indexOf("treemas-rpt-text.dtd") != -1) {
            try {
                InputStream in = Thread.currentThread().getContextClassLoader().
                        getResourceAsStream(REPORT_TEXT_DTD);
                if (in == null) {
                    throw new SAXException("Cannot load DTD " + REPORT_TEXT_DTD);
                }

                result = new InputSource(in);
            } catch (Exception ex) {
                throw new SAXException("Failed loading DTD " + REPORT_TEXT_DTD, ex);
            }
        } else {
            throw new SAXException("Invalid SystemId!");
        }
        return result;
    }
}
