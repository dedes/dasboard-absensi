package treemas.util.report.text;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Map;

public class FieldSys extends PrintableElement {
    public static final int TYPE_SYSDATE = 1;
    public static final int TYPE_ROW_NUMBER = 2;
    public static final int TYPE_PAGE_NUMBER = 3;

    public static final String DEFAULT_DATE_FORMAT = "dd-MMM yyyy";

    private boolean printDuplicate = true;
    private String lastPrinted = null;

    private int type;
    private String format;

    private SimpleDateFormat dateFormatter;

    public FieldSys(TextReport report, int start, int align, int width, int type) {
        this(report, start, align, width, type, null, true);
    }

    public FieldSys(TextReport report, int start, int align, int width, int type, String format, boolean printDuplicate) {
        super(report, start, align, width);
        if (type != TYPE_SYSDATE && type != TYPE_ROW_NUMBER && type != TYPE_PAGE_NUMBER)
            throw new IllegalArgumentException("Invalid FieldSys type. Please use constant from FieldSys class");
        this.type = type;
        this.format = format;
        this.printDuplicate = printDuplicate;

        if (this.type == TYPE_SYSDATE) {
            if (this.format == null)
                this.format = DEFAULT_DATE_FORMAT;
            this.dateFormatter = new SimpleDateFormat(this.format);
        }

    }

    public void print(StringBuffer sb, Map rowData) {
        String text = null;
        switch (this.type) {
            case TYPE_SYSDATE:
                text = this.dateFormatter.format(new Timestamp(System.currentTimeMillis()));
                break;
            case TYPE_ROW_NUMBER:
                text = String.valueOf(this.report.getCurrentRowNum());
                break;
            case TYPE_PAGE_NUMBER:
                text = String.valueOf(this.report.getCurrentPageNumber());
                break;
        }
        if (this.printDuplicate || !text.equals(this.lastPrinted))
            super.alignAndPrint(sb, text);
        this.lastPrinted = text;
    }

}
