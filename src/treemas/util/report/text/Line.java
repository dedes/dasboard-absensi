package treemas.util.report.text;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class Line extends ReportElement {
    private LinkedList elements = new LinkedList();
    private boolean printEmpty = true;
    private int maxLength = 0;

    public Line(TextReport report) {
        this(report, true);
    }

    public Line(TextReport report, boolean printEmpty) {
        super(report);
        this.printEmpty = printEmpty;
        this.setMaxLength(report.getMaxLengthPerLine());
    }

    public int getMaxLength() {
        return this.maxLength;
    }

    public void setMaxLength(int length) {
        if (length < 0) {
            throw new IllegalArgumentException("Length shall be >= 0");
        }
        this.maxLength = length;
    }

    public void add(PrintableElement element) {
        this.elements.add(element);
    }

    public void print(StringBuffer sb, Map data) {
        if (this.report.isCurrentLineOverflow()) {
            this.report.forceChangePage(sb, data);
        }
        Iterator iter = this.elements.iterator();
        StringBuffer line = new StringBuffer();
        StringBuffer tmp = new StringBuffer();
        while (iter.hasNext()) {
            PrintableElement el = (PrintableElement) iter.next();
            el.print(tmp, data);
            ReportUtil.merge(line, tmp.toString());
            tmp.delete(0, tmp.length());
        }
        if (printEmpty || line.toString().trim().length() > 0) {
            if (this.maxLength > 0 && line.length() > this.maxLength) {
                line.delete(this.maxLength, line.length());
            }
            sb.append(line).append("\r\n");
            this.report.addLineCounter();
        }
    }

    Iterator getElementsIterator() {
        return this.elements.iterator();
    }

}
