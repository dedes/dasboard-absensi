package treemas.util.report.text;

import java.sql.Connection;
import java.sql.SQLException;

public interface IConnectionHelper {
    public Connection getConnection() throws SQLException;

    public void closeConnection(Connection conn) throws SQLException;
}

