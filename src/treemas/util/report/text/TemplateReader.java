package treemas.util.report.text;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

public class TemplateReader {
    private String fileName = null;
    private InputStream inputStream = null;
    private Document templateDocument = null;
    private Column[] columns = null;

    public TemplateReader(String fileName) {
        this.fileName = fileName;
    }

    public TemplateReader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    private void open() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        docBuilder.setEntityResolver(new ReportTextDtdResolver());
        if (this.inputStream != null) {
            this.templateDocument = docBuilder.parse(this.inputStream);
        } else {
            this.templateDocument = docBuilder.parse(new File(this.fileName));
        }
    }

    public void read(TextReport report, Column[] columns) throws InvalidTemplateException, Exception {
        if (this.templateDocument == null)
            this.open();
        this.columns = columns;
        Element root = this.templateDocument.getDocumentElement();
        if (!root.getNodeName().equals("report")) {
            throw new InvalidTemplateException("Invalid node " + root.getNodeName() + ". Expected 'report'");
        }

        NamedNodeMap reportAttr = root.getAttributes();
        String tmp = this.getTextAttribute(reportAttr, "numberoflines");
        if (tmp != null) {
            int numOfLine = 0;
            try {
                numOfLine = Integer.parseInt(tmp);
            } catch (Exception e) {
            }
            if (numOfLine <= 0) {
                throw new InvalidTemplateException("Invalid 'numberoflines' value");
            }
            report.setMaxLinePerPage(numOfLine);
        }

        int lengthPerLine = this.getIntegerAttribute(reportAttr, "maxlengthperline");
        if (lengthPerLine < 0) {
            throw new InvalidTemplateException("Invalid 'maxlengthperline' value");
        }
        report.setMaxLengthPerLine(lengthPerLine);

        String pageBreak = this.getTextAttribute(reportAttr, "pagebreak");
        if (pageBreak != null)
            report.setPageBreak(pageBreak);

        boolean suppressLinePad = this.getBooleanAttribute(reportAttr, "suppressLinesPadding", false);
        report.setSuppressLinesPadding(suppressLinePad);

        NodeList nl = root.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            Node node = nl.item(i);
            String nodeName = node.getNodeName();
            if (nodeName.startsWith("#"))
                continue;
            if (nodeName.equals("title"))
                title(report, node);
            else if (nodeName.equals("pageheader"))
                this.pageHeader(report, node);
            else if (nodeName.equals("pagefooter"))
                this.pageFooter(report, node);
            else if (nodeName.equals("columnheader"))
                this.columnHeader(report, node);
            else if (nodeName.equals("detail")) {
                Detail detail = this.createDetail(report, node);
                report.setDetail(detail);
            } else if (nodeName.equals("group")) {
                Group group = this.createGroup(report, null, node);
                report.setDetail(group);
            }
        }
    }

    private void title(TextReport report, Node titleNode) throws InvalidTemplateException {
        Title title = new Title(report);
        this.readLines(report, title, titleNode);
        report.setTitle(title);
    }

    private Detail createDetail(TextReport report, Node detailNode) throws InvalidTemplateException {
        Detail detail = new Detail(report);
        this.readLines(report, detail, detailNode);
        return detail;
    }


    private void pageHeader(TextReport report, Node pgHeaderNode) throws InvalidTemplateException {
        PageHeader pgHeader = new PageHeader(report);
        this.readLines(report, pgHeader, pgHeaderNode);
        report.setPageHeader(pgHeader);
    }

    private void pageFooter(TextReport report, Node pgFooterNode) throws InvalidTemplateException {
        PageFooter pgFooter = new PageFooter(report);
        this.readLines(report, pgFooter, pgFooterNode);
        report.setPageFooter(pgFooter);
    }

    private void columnHeader(TextReport report, Node colHeaderNode) throws InvalidTemplateException {
        ColumnHeader colHeader = new ColumnHeader(report);
        this.readLines(report, colHeader, colHeaderNode);
        report.setColumnHeader(colHeader);
    }


    private Group createGroup(TextReport report, Group parentGroup, Node groupNode) throws InvalidTemplateException {
        NamedNodeMap attrMap = groupNode.getAttributes();
        String fieldName = this.getTextAttribute(attrMap, "field");
        if (fieldName == null)
            throw new InvalidTemplateException("Attribute 'field' is required in 'group' node");

        Group group = new Group(report, parentGroup, fieldName);
        boolean resetRowNum = this.getBooleanAttribute(attrMap, "resetrownum", false);
        boolean resetPageNum = this.getBooleanAttribute(attrMap, "resetpagenum", false);
        boolean newPage = this.getBooleanAttribute(attrMap, "newpage", false);
        String newPageSign = this.getTextAttribute(attrMap, "newpagesign");
        group.setResetRowNumberOnNewGroup(resetRowNum);
        group.setResetPageNumberOnNewGroup(resetPageNum);
        group.setNewPageOnNewGroup(newPage);
        group.setNewPageSign(newPageSign);

        NodeList childs = groupNode.getChildNodes();
        for (int i = 0; i < childs.getLength(); i++) {
            Node node = childs.item(i);
            String nodeName = node.getNodeName();
            if (nodeName.startsWith("#"))
                continue;
            if (nodeName.equals("defcalc")) {
                NamedNodeMap calcAttr = node.getAttributes();
                String calcName = this.getTextAttribute(calcAttr, "name");
                if (calcName == null)
                    throw new InvalidTemplateException("Attribute 'name' is required for 'defcalc'");

                String field = this.getTextAttribute(calcAttr, "field");
                if (field == null)
                    throw new InvalidTemplateException("Attribute 'field' is required for 'defcalc'");

                String strType = this.getTextAttribute(calcAttr, "type");
                if (strType == null)
                    throw new InvalidTemplateException("Attribute 'type' is required for 'defcalc'");
                int type;
                if (strType.equals("count"))
                    type = Calc.TYPE_COUNT;
                else if (strType.equals("sum"))
                    type = Calc.TYPE_SUM;
                else
                    throw new InvalidTemplateException("Unsupported 'type' for 'defcalc': " + strType);

                Calc calc = new Calc(report, group, type, field);
                report.getCalcManager().defineCalc(calcName, calc);
            } else if (nodeName.equals("detail")) {
                Detail detail = this.createDetail(report, node);
                group.setDetail(detail);
            } else if (nodeName.equals("header")) {
                boolean printOnNewPage = this.getBooleanAttribute(node.getAttributes(), "printonnewpage", false);
                GroupHeader groupHeader = new GroupHeader(report, printOnNewPage);
                this.readLines(report, groupHeader, node);
                group.setHeader(groupHeader);
            } else if (nodeName.equals("footer")) {
                GroupFooter groupFooter = new GroupFooter(report);
                this.readLines(report, groupFooter, node);
                group.setFooter(groupFooter);
            } else if (nodeName.equals("group")) {
                Group childGroup = this.createGroup(report, group, node);
                group.setDetail(childGroup);
            }
        }
        return group;
    }


    private void readLines(TextReport report, LineContainer container, Node lines) throws InvalidTemplateException {
        NodeList child = lines.getChildNodes();
        for (int i = 0; i < child.getLength(); i++) {
            Node node = child.item(i);
            String nodeName = node.getNodeName();
            if (nodeName.startsWith("#"))
                continue;
            if (nodeName.equals("line")) {
                container.add(this.readLine(report, node));
            } else
                throw new InvalidTemplateException("Node 'line' expected, but found node " + node.getNodeName());
        }
    }

    private Line readLine(TextReport report, Node lineNode) throws InvalidTemplateException {
        boolean printEmpty = true;
        String printEmptyText = this.getTextAttribute(lineNode.getAttributes(), "printempty");
        if (printEmptyText != null) {
            printEmpty = !printEmptyText.equals("false");
        }

        int maxLength = this.getIntegerAttribute(lineNode.getAttributes(), "maxlength");
        Line line = new Line(report, printEmpty);
        if (maxLength > 0) {
            line.setMaxLength(maxLength);
        }
        NodeList fields = lineNode.getChildNodes();
        for (int i = 0; i < fields.getLength(); i++) {
            Node fieldNode = fields.item(i);
            if (fieldNode.getNodeName().startsWith("#"))
                continue;

            PrintableElement pe = null;
            int start = 0;
            int align = PrintableElement.ALIGN_LEFT;
            int width = 0;
            boolean printDuplicate = true;
            String txt = "";
            NamedNodeMap attrMap = fieldNode.getAttributes();
            start = this.getIntegerAttribute(attrMap, "start");
            width = this.getIntegerAttribute(attrMap, "width");

            String tmp = this.getTextAttribute(attrMap, "align");
            if (tmp != null) {
                if (tmp.equals("left"))
                    align = PrintableElement.ALIGN_LEFT;
                else if (tmp.equals("center"))
                    align = PrintableElement.ALIGN_CENTER;
                else if (tmp.equals("right"))
                    align = PrintableElement.ALIGN_RIGHT;
                else
                    throw new InvalidTemplateException("Invalid align value: " + tmp);
            }

            printDuplicate = this.getBooleanAttribute(attrMap, "printduplicate", printDuplicate);


            if (fieldNode.getNodeName().equals("label")) {
                txt = fieldNode.getFirstChild().getNodeValue();

                pe = new Label(report, start, align, width, txt);
            } else if (fieldNode.getNodeName().equals("field")) {
                String fieldName = this.getTextAttribute(attrMap, "name");
                if (fieldName == null)
                    throw new InvalidTemplateException("Attribute 'name' in node 'field' is required");
                if (!this.isColumnDefined(fieldName))
                    throw new InvalidTemplateException("Field " + fieldName + " is undefined");
                pe = new Field(report, start, align, width, fieldName, printDuplicate);
            } else if (fieldNode.getNodeName().equals("fieldsys")) {
                String fieldType = this.getTextAttribute(attrMap, "type");
                String format = this.getTextAttribute(attrMap, "format");
                if (fieldType == null)
                    throw new InvalidTemplateException("Attribute 'name' in node 'fieldsys' is required");
                int type;
                if (fieldType.equals("sysdate"))
                    type = FieldSys.TYPE_SYSDATE;
                else if (fieldType.equals("pagenumber"))
                    type = FieldSys.TYPE_PAGE_NUMBER;
                else if (fieldType.equals("rownumber"))
                    type = FieldSys.TYPE_ROW_NUMBER;
                else
                    throw new InvalidTemplateException("Attribute 'name' in node 'fieldsya' shall be either 'sysdate', 'pagenumber' or 'rownumber'");

                pe = new FieldSys(report, start, align, width, type, format, printDuplicate);
            } else if (fieldNode.getNodeName().equals("fieldcalc")) {
                String name = this.getTextAttribute(attrMap, "name");
                if (name == null)
                    throw new InvalidTemplateException("Attribute 'name' in node 'fieldcalc' is required");
                pe = new FieldCalc(report, name, start, align, width);
                report.getCalcManager().addFieldCalc((FieldCalc) pe);
            } else
                throw new InvalidTemplateException("Invalid node " + fieldNode.getNodeName());

            line.add(pe);
        }
        return line;
    }

    private int getIntegerAttribute(NamedNodeMap map, String key) throws InvalidTemplateException {
        int result = 0;
        Node node = map.getNamedItem(key);
        if (node != null) {
            try {
                result = Integer.parseInt(node.getNodeValue());
            } catch (Exception e) {
                throw new InvalidTemplateException("Invalid " + key + " value: " + node.getNodeName());
            }
        }
        return result;
    }

    private String getTextAttribute(NamedNodeMap map, String key) throws InvalidTemplateException {
        String result = null;
        Node node = map.getNamedItem(key);
        if (node != null) {
            result = node.getNodeValue();
        }
        return result;
    }

    private boolean getBooleanAttribute(NamedNodeMap map, String key, boolean defaultValue) throws InvalidTemplateException {
        boolean result = defaultValue;
        Node node = map.getNamedItem(key);
        if (node != null) {
            try {
                String tmp = node.getNodeValue();
                if (tmp.equals("true"))
                    result = true;
                else if (tmp.equals("false"))
                    result = false;
                else
                    throw new InvalidTemplateException("Invalid " + key + " value: " + tmp);
            } catch (Exception e) {
                throw new InvalidTemplateException("Invalid start value " + node.getNodeName());
            }
        }
        return result;
    }

    public Column[] readColumns() throws Exception {
        if (this.templateDocument == null)
            this.open();

        // find 'column' node
        Node child = this.templateDocument.getDocumentElement().getFirstChild();
        Node columnNode = null;
        while (columnNode == null && child != null) {
            if (child.getNodeName().equals("columns"))
                columnNode = child;
            child = child.getNextSibling();
        }
        if (columnNode == null)
            throw new Exception("Cannot find 'columns' node for data column definition");

        NodeList nl = columnNode.getChildNodes();
        int nColumn = nl.getLength();
        if (nColumn == 0)
            throw new InvalidTemplateException("There shall be at least one 'column' defined in 'columns' node");
        ArrayList list = new ArrayList(nColumn);
        for (int i = 0; i < nColumn; i++) {
            Node node = nl.item(i);
            String nodeName = node.getNodeName();
            if (nodeName.startsWith("#"))
                continue;
            if (!nodeName.equals("column"))
                throw new InvalidTemplateException("'columns' can only contain 'column' node");
            NamedNodeMap attrMap = node.getAttributes();
            String fieldName = this.getTextAttribute(attrMap, "name");
            if (fieldName == null)
                throw new InvalidTemplateException("Attribute 'name' is required in 'column' node");

            String typeClass = this.getTextAttribute(attrMap, "type");
            if (typeClass == null)
                throw new InvalidTemplateException("Attribute 'type' is required in 'column' node");
            Class cls = null;
            try {
                cls = Class.forName(typeClass);
            } catch (ClassNotFoundException e) {
                throw new InvalidTemplateException("Invalid / cannot find class " + typeClass);
            }

            String format = this.getTextAttribute(attrMap, "format");

            list.add(new Column(fieldName, cls, format));
        } // end-for

        Column[] columns = new Column[list.size()];
        list.toArray(columns);

        return columns;
    }

    private boolean isColumnDefined(String colName) {
        boolean found = false;
        if (this.columns != null) {
            for (int i = 0; i < this.columns.length && !found; i++) {
                found = this.columns[i].getName().equals(colName);
            }
        }
        return found;
    }


}
