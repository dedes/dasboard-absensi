package treemas.util.report.text;

public class GroupHeader extends LineContainer {
    private boolean printOnNewPage = false;

    public GroupHeader(TextReport report, boolean printOnNewPage) {
        super(report);
        this.printOnNewPage = printOnNewPage;
    }

    public GroupHeader(TextReport report) {
        this(report, false);
    }

    public boolean isPrintOnNewPage() {
        return printOnNewPage;
    }

    public void setPrintOnNewPage(boolean printOnNewPage) {
        this.printOnNewPage = printOnNewPage;
    }

}
