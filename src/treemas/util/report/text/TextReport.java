package treemas.util.report.text;

import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Map;

public class TextReport {
    private static final int CHARACTERS_PER_WRITE = 1024 * 512; // 512k buffer
    private final String newLine = System.getProperty("line.separator");
    private Title title;
    private PageHeader pageHeader;
    private PageFooter pageFooter;
    private int footerLineCount;
    private ColumnHeader columnHeader;
    private Detail detail;
    private char thousandSeparator = '.';
    private char decimalSeparator = ',';
    private String numberFormat = "###,###";
    private int currentLineNo;
    private int currentRowNum; // page number (for display purpose) - can be reset
    private int pageCounter; // real page count

    private int maxLinePerPage = 50;
    private int maxLengthPerLine = 0;

    private DBReader dbReader;

    private CalcManager calcManager = new CalcManager();

    private boolean titlePrinted;
    private boolean pageHeaderPrinted;
    private boolean columnHeaderPrinted;
    private boolean shallChangePage;
    private boolean shallResetPageNumber;
    private String pageBreak = "---page break ---";
    private String customPageBreak;
    private int currentPageNumber;
    private boolean suppressLinesPadding;

    public TextReport(DBReader dbReader) {
        this.dbReader = dbReader;
        this.dbReader.setReport(this);
    }

    public PageHeader getPageHeader() {
        return pageHeader;
    }

    public void setPageHeader(PageHeader pageHeader) {
        this.pageHeader = pageHeader;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public ColumnHeader getColumnHeader() {
        return columnHeader;
    }

    public void setColumnHeader(ColumnHeader columnHeader) {
        this.columnHeader = columnHeader;
    }

    public char getThousandSeparator() {
        return thousandSeparator;
    }

    public void setThousandSeparator(char thousandSeparator) {
        this.thousandSeparator = thousandSeparator;
    }

    public String getNumberFormat() {
        return numberFormat;
    }

    public void setNumberFormat(String numberFormat) {
        this.numberFormat = numberFormat;
    }

    public Title getTitle() {
        return this.title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public PageFooter getPageFooter() {
        return pageFooter;
    }

    public void setPageFooter(PageFooter pageFooter) {
        this.pageFooter = pageFooter;
    }

    public String getPageBreak() {
        return pageBreak;
    }

    public void setPageBreak(String pageBreak) {
        if (pageBreak != null) {
            pageBreak = pageBreak.replaceAll("\\r", "\r");
            pageBreak = pageBreak.replaceAll("\\n", "\n");
        }
        this.pageBreak = pageBreak;
    }

    public int getCurrentPageNumber() {
        return currentPageNumber;
    }

    public int getPageCounter() {
        return pageCounter;
    }

    Map peekNextData() {
        try {
            return this.dbReader.peekNextData();
        } catch (SQLException sqe) {
            throw new RuntimeException("Cannot process/retrieve data", sqe);
        }
    }

    public void print(OutputStream out) throws Exception {
        if (this.dbReader == null)
            throw new Exception("DBReader is not set!");
        StringBuffer buffer = new StringBuffer(CHARACTERS_PER_WRITE);
        StringBuffer tmpBuffer = new StringBuffer(512);
        try {
            System.out.println("Generating report...");
            Map currentRow = null;
            this.titlePrinted = false;
            this.resetRowNum();
            this.resetPageNum();
            this.pageCounter = 0;
            this.newPage();
            this.dbReader.open();

            this.footerLineCount = this.pageFooter == null ? 0 :
                    this.pageFooter.linesCount();
            int counter = 0;
            do {
                this.customPageBreak = null; // reset CustomPageBreak
                if (buffer.length() >= CHARACTERS_PER_WRITE) {
                    out.write(buffer.toString().getBytes());
                    out.flush();
                    buffer.delete(0, buffer.length());
                }

                currentRow = this.dbReader.nextData();
                if (currentRow == null)
                    continue;

                this.printTitleAndHeaders(buffer, currentRow);

                if (this.detail != null)
                    this.detail.print(buffer, currentRow);

                if (this.shallChangePage || this.isCurrentLineOverflow()) {
                    this.printFooter(buffer, currentRow);
                    this.newPage();
                }

                this.currentRowNum++;
                counter++;
            }
            while (currentRow != null);

            out.write(buffer.toString().getBytes());
            out.flush();
            System.out.println("Total data=" + counter);
        } finally {
            this.dbReader.close();
        }
    }

    void addLineCounter() {
        this.currentLineNo++;
    }

    int getCurrentRowNum() {
        return this.currentRowNum;
    }

    void resetRowNum() {
        this.currentRowNum = 1;
    }

    // reset page number
    void resetPageNum() {
        this.shallResetPageNumber = true;
    }

    public CalcManager getCalcManager() {
        return this.calcManager;
    }

    public int getMaxLinePerPage() {
        return maxLinePerPage;
    }

    public void setMaxLinePerPage(int maxLinePerPage) {
        this.maxLinePerPage = maxLinePerPage;
    }

    public int getMaxLengthPerLine() {
        return maxLengthPerLine;
    }

    public void setMaxLengthPerLine(int maxLengthPerLine) {
        this.maxLengthPerLine = maxLengthPerLine;
    }

    public char getDecimalSeparator() {
        return decimalSeparator;
    }

    public void setDecimalSeparator(char decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
    }

    public boolean isSuppressLinesPadding() {
        return suppressLinesPadding;
    }

    public void setSuppressLinesPadding(boolean suppressLinesPadding) {
        this.suppressLinesPadding = suppressLinesPadding;
    }

    private void newPage() {
        this.columnHeaderPrinted = false;
        this.pageHeaderPrinted = false;
        this.shallChangePage = false;
        this.currentLineNo = 1;
        this.pageCounter++;
        if (this.shallResetPageNumber) {
            this.currentPageNumber = 1;
            this.shallResetPageNumber = false;
        } else
            this.currentPageNumber++;
    }

    /**
     * This method is to be called from Group for executing page break on new group
     */
    void changePage(String customPageBreak) {
        this.shallChangePage = true;
        this.customPageBreak = customPageBreak;
    }

    /**
     * This method is to be called from Line for forcing page break
     *
     * @param buffer  StringBuffer
     * @param rowData Map
     */
    void forceChangePage(StringBuffer buffer, Map rowData) {
        this.printFooter(buffer, rowData);
        this.newPage();
        this.printTitleAndHeaders(buffer, rowData);
    }

    boolean isCurrentLineOverflow() {
        return (this.currentLineNo + this.footerLineCount > this.maxLinePerPage);
    }

    private void printTitleAndHeaders(StringBuffer buffer, Map rowData) {
        if (this.title != null && !this.titlePrinted) {
            this.title.print(buffer, rowData);
            this.titlePrinted = true;
        }
        if (this.pageHeader != null && !this.pageHeaderPrinted) {
            this.pageHeader.print(buffer, rowData);
            this.pageHeaderPrinted = true;
        }
        if (this.columnHeader != null && !this.columnHeaderPrinted) {
            this.columnHeader.print(buffer, rowData);
            this.columnHeaderPrinted = true;
        }
    }

    private void printFooter(StringBuffer buffer, Map rowData) {
        if (!this.suppressLinesPadding) {
            while (!this.isCurrentLineOverflow()) {
                this.currentLineNo++;
                buffer.append(this.newLine);
            }
        }

        if (this.pageFooter != null)
            this.pageFooter.print(buffer, rowData);
        if (this.customPageBreak != null)
            buffer.append(this.customPageBreak);
        else
            buffer.append(this.pageBreak);
        //buffer.append("\r\n");
    }

}
