package treemas.util.report.text;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public abstract class LineContainer extends ReportElement {
    protected LinkedList lines = new LinkedList();

    public LineContainer(TextReport report) {
        super(report);
    }

    public void print(StringBuffer sb, Map rowData) {
        Iterator iter = this.lines.iterator();
        while (iter.hasNext()) {
            Line ln = (Line) iter.next();
            ln.print(sb, rowData);
        }
    }

    public void add(Line line) {
        this.lines.add(line);
    }

    public boolean remove(Line line) {
        return this.lines.remove(line);
    }

    public int linesCount() {
        return this.lines.size();
    }


}
