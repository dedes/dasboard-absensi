package treemas.util.report.text;

import treemas.util.AppException;

public class InvalidTemplateException extends AppException {
    public InvalidTemplateException(String msg) {
        super(msg, null, 0, null);
    }

    public InvalidTemplateException(String msg, Throwable cause) {
        super(msg, cause, 0, null);
    }
}
