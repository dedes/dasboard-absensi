package treemas.util.report.text;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


public class DBReader {
    private int fetchSize = 10;
    private TextReport report;
    private LinkedList data = new LinkedList();
    private IConnectionHelper connectionHelper;
    private String appDateYMD;
    private Column[] columns;
    private String spFullName;

    private Connection connection;
    private ResultSet resultSet;
    private boolean cancelled = false;

    public DBReader(IConnectionHelper connHelper, String appDateYMD,
                    String spFullName, Column[] columns) {
        this.connectionHelper = connHelper;
        this.appDateYMD = appDateYMD;
        this.spFullName = spFullName;
        this.columns = columns;
    }

    public TextReport getReport() {
        return this.report;
    }

    public void setReport(TextReport report) {
        this.report = report;
    }

    public int getFetchSize() {
        return fetchSize;
    }

    public void setFetchSize(int fetchSize) {
        this.fetchSize = fetchSize;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public Map nextData() throws Exception {
        Map result = null;
        if (this.data.isEmpty()) {
            this.readData();
        }
        if (!this.data.isEmpty())
            result = (Map) this.data.removeFirst();
        return result;
    }

    public Map peekNextData() throws SQLException {
        Map result = null;

        if (this.data.isEmpty()) {
            this.readData();
        }
        if (!this.data.isEmpty())
            result = (Map) this.data.getFirst();
        return result;
    }

    private void readData() throws SQLException {
        if (this.resultSet == null)
            throw new SQLException("Dataset is not open!");
        int i = 0;
        while (i < this.fetchSize && this.resultSet.next()) {
            HashMap row = new HashMap();
            for (int j = 0; j < this.columns.length; j++) {
                DataValueWrapper val;
                String columnName = columns[j].getName();
                if (columns[j].getType() == java.lang.Integer.class) {
                    val = DataValueWrapper.createWrapper(this.resultSet.getInt(
                            columnName));
                } else if (columns[j].getType() == java.lang.String.class) {
                    val = DataValueWrapper.createWrapper(this.resultSet.getString(
                            columnName));
                } else if (columns[j].getType() == java.lang.Long.class) {
                    String format = columns[j].getFormat();
                    format = format == null ? this.report.getNumberFormat() : format;
                    val = DataValueWrapper.createWrapper(this.resultSet.getLong(
                            columnName), format,
                            this.report.getThousandSeparator(),
                            this.report.getDecimalSeparator());
                } else if (columns[j].getType() == java.lang.Double.class) {
                    String format = columns[j].getFormat();
                    format = format == null ? this.report.getNumberFormat() : format;
                    val = DataValueWrapper.createWrapper(this.resultSet.getDouble(
                            columnName), format,
                            this.report.getThousandSeparator(),
                            this.report.getDecimalSeparator());
                } else if (columns[j].getType() == java.sql.Timestamp.class) {
                    String format = columns[j].getFormat();
                    val = DataValueWrapper.createWrapper(this.resultSet.getTimestamp(
                            columnName), format);
                } else {
                    val = DataValueWrapper.createWrapper(this.resultSet.getObject(
                            columnName).toString());
                }
                if (this.resultSet.wasNull())
                    val = DataValueWrapper.createWrapper("");
                row.put(columnName, val);
            } // end-for
            this.data.addLast(row);
            i++;
        }
    }

    public void open() throws Exception {
        this.open(null);
    }

    public void open(String param) throws Exception {
        if (this.isOpen())
            return;
        this.connection = this.connectionHelper.getConnection();
        CallableStatement cs = this.connection.prepareCall(this.spFullName);
        int idx = 1;
        cs.setString(idx++, this.appDateYMD); // Application date (YYYYMMDD)
        if (param != null)
            cs.setString(idx++, param);
        cs.registerOutParameter(idx++, Types.INTEGER); // isOK
        cs.registerOutParameter(idx++, oracle.jdbc.OracleTypes.CURSOR);
        idx -= 2;
        cs.execute();
        this.cancelled = (cs.getInt(idx++) == 0);
        this.resultSet = (ResultSet) cs.getObject(idx++);
    }

    public boolean isOpen() {
        return this.resultSet != null && this.connection != null;
    }

    public void close() throws Exception {
        try {
            if (this.resultSet != null)
                this.resultSet.close();
            if (this.connection != null)
                this.connectionHelper.closeConnection(this.connection);
            this.resultSet = null;
            this.connection = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void finalize() throws Throwable {
        this.connectionHelper.closeConnection(this.connection);
    }

}
