package treemas.util.report.text;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class CalcManager {
    HashMap calcs = new HashMap();
    HashMap fieldCalcs = new HashMap();

    public CalcManager() {
    }

    public void defineCalc(String name, Calc calc) {
        this.calcs.put(name, calc);
        fieldCalcs.put(calc, new LinkedList());
    }

    public Calc getCalc(String name) {
        return (Calc) this.calcs.get(name);
    }

    public void addFieldCalc(FieldCalc fCalc) {
        Calc calc = (Calc) this.calcs.get(fCalc.getName());
        if (calc == null)
            throw new IllegalArgumentException("Calc " + fCalc.getName() + " has not been defined");
        LinkedList list = (LinkedList) this.fieldCalcs.get(calc);
        list.addLast(fCalc);
    }

    void resetCalcManager() {
        this.fieldCalcs.clear();
        this.calcs.clear();
    }

    void calculateForGroup(Group group, Map rowData) {
        Iterator iter = this.calcs.values().iterator();
        while (iter.hasNext()) {
            Calc calc = (Calc) iter.next();
            if (calc.getOwner() == group) {
                calc.calculate(rowData);
            }
        }
    }

    void resetForGroup(Group group) {
        Iterator iter = this.calcs.values().iterator();
        while (iter.hasNext()) {
            Calc calc = (Calc) iter.next();
            if (calc.getOwner() == group) {
                calc.reset();
            }
        }
    }


}
