package treemas.util.report.text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ReportUtil {
    private ReportUtil() {
    }

    public static String alignRight(String source, int width) {
        int padd = width - source.length();
        StringBuffer sb = new StringBuffer();
        while (padd-- > 0)
            sb.append(" ");
        sb.append(source);
        return sb.toString();
    }

    public static String alignCenter(String source, int width) {
        int padd = (width - source.length()) / 2;
        StringBuffer sb = new StringBuffer();
        while (padd-- > 0)
            sb.append(" ");
        sb.append(source);
        return sb.toString();
    }

    public static String spaces(int count) {
        if (count == 0)
            return "";
        StringBuffer sb = new StringBuffer();
        while (sb.length() < count) {
            sb.append(" ");
        }
        return sb.toString();
    }

    public static void merge(StringBuffer sb, String str) {
        int n = 0;
        for (int i = 0; i < str.length(); i++) {
            if (i < sb.length()) {
                char ch = str.charAt(i);
                if (ch != ' ')
                    sb.replace(i, i + 1, String.valueOf(str.charAt(i)));
            } else
                sb.append(str.charAt(i));
        }
    }

    public static void ensureExist(String fileName, String signature) {
        File f = new File(fileName);
        boolean exists = false;
        exists = f.isFile() && (f.length() > 0);
        if (exists)
            return;
        try {
            FileOutputStream fos = new FileOutputStream(f);
            if (signature != null)
                fos.write(signature.getBytes());
            fos.close();
            System.out.println("No-data text file created: " + fileName);
        } catch (IOException ioe) {
            System.out.println("Ensure exist failed: " + ioe.toString() +
                    "Filename=" + fileName);
        }
    }

    public static int countLines(String str) {
        int result = 0;
        int pos = 0;
        while (pos != -1) {
            pos = str.indexOf("\n", pos);
            if (pos == -1)
                continue;
            pos++;
            result++;
        }
        return result;
    }

}
