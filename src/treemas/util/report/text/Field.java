package treemas.util.report.text;

import java.util.Map;

public class Field extends PrintableElement {
    private String fieldName;
    private boolean printDuplicate = true;
    private String lastPrinted = null;

    public Field(TextReport report, int start, int align, int width, String fieldName) {
        this(report, start, align, width, fieldName, true);
    }

    public Field(TextReport report, int start, int align, int width, String fieldName, boolean printDuplicate) {
        super(report, start, align, width);
        this.fieldName = fieldName;
        this.printDuplicate = printDuplicate;
    }

    public void print(StringBuffer sb, Map rowData) {
        DataValueWrapper current = (DataValueWrapper) rowData.get(this.fieldName);
        if (printDuplicate || !current.toString().equals(lastPrinted))
            super.alignAndPrint(sb, current);
        this.lastPrinted = current.toString();
    }

    void resetLastPrinted() {
        this.lastPrinted = null;
    }
}
