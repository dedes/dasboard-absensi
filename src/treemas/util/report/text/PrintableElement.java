package treemas.util.report.text;

import java.util.Map;

public abstract class PrintableElement extends ReportElement {
    public static final int ALIGN_LEFT = 0;
    public static final int ALIGN_CENTER = 1;
    public static final int ALIGN_RIGHT = 2;

    protected int align = ALIGN_LEFT;
    protected int start = 0;
    protected int width = 0;

    public PrintableElement(TextReport report, int start, int align, int width) {
        super(report);
        this.align = align;
        this.start = start;
        this.width = width;
    }

    public abstract void print(StringBuffer sb, Map data);

    public void alignAndPrint(StringBuffer sb, DataValueWrapper data) {
        this.alignAndPrint(sb, data.toString());
    }

    public void alignAndPrint(StringBuffer sb, String text) {
        if (this.width <= 0)
            this.align = PrintableElement.ALIGN_LEFT;

        switch (this.align) {
            case PrintableElement.ALIGN_CENTER:
                text = ReportUtil.alignCenter(text, this.width);
                break;
            case PrintableElement.ALIGN_RIGHT:
                text = ReportUtil.alignRight(text, this.width);
                break;
            default:
                break;
        }
        sb.append(ReportUtil.spaces(this.start)).append(text);
    }


}
