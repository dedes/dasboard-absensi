package treemas.util.format;

import treemas.util.beanaction.BeanConverter.FieldConverter;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import java.text.ParseException;


public class TreemasDateFormatter implements FieldConverter {
    private static String VIEW_DATE_FORMAT = "dd-MMM-yyyy HH:mm:ss";
    private static ThreadLocal converters = new ThreadLocal();
    private ILogger logger = LogManager.getDefaultLogger();

    private TreemasDateFormatter() {
    }

    public static TreemasDateFormatter getInstance() {
        TreemasDateFormatter result = (TreemasDateFormatter) converters.get();
        if (result == null) {
            result = new TreemasDateFormatter();
            converters.set(result);
        }
        return result;
    }

    public String convertToString(String fieldName, Object source) {
        if (source == null)
            return null;
        String result = null;
        if (source instanceof java.util.Date) {
            result = TreemasFormatter.getDateFormat(VIEW_DATE_FORMAT).format(source);
        } else
            result = source.toString();
        return result;
    }


    public Object convertFromString(String fieldName, String source, Class targetClass) {
        if (source == null)
            return null;

        Object result = null;
        try {
            if (java.util.Date.class.equals(targetClass)) {
                result = TreemasFormatter.getDateFormat(VIEW_DATE_FORMAT).parse(source);
            } else
                throw new IllegalArgumentException("Target class " + targetClass.getName() +
                        " is not supported by TreemasDateFormatter");
        } catch (ParseException pe) {
            this.logger.log(ILogger.LEVEL_ERROR,
                    "Error parsing " + source + " to class " +
                            targetClass.getName() + ": " + pe.toString());
        }
        return result;
    }
}
