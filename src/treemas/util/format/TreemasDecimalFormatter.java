package treemas.util.format;

import treemas.util.beanaction.BeanConverter.FieldConverter;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import java.text.DecimalFormat;

public class TreemasDecimalFormatter implements FieldConverter {
    private static DecimalFormat decimalFormat = new DecimalFormat("###.######");
    private static ThreadLocal converters = new ThreadLocal();
    private ILogger logger = LogManager.getDefaultLogger();


    public static TreemasDecimalFormatter getInstance() {
        TreemasDecimalFormatter result = (TreemasDecimalFormatter) converters.get();
        if (result == null) {
            result = new TreemasDecimalFormatter();
            converters.set(result);
        }
        return result;
    }

    public String convertToString(String fieldName, Object source) {
        if (source == null)
            return null;
        String result = null;
        if (source instanceof java.lang.Double) {
            result = decimalFormat.format(source);
        } else
            result = source.toString();
        return result;
    }


    public Object convertFromString(String fieldName, String source, Class targetClass) {
        if (source == null)
            return null;

        Object result = null;
        try {
            if (java.lang.Double.class.equals(targetClass)) {
                result = Double.valueOf(source);
            } else
                throw new IllegalArgumentException("Target class " + targetClass.getName() +
                        " is not supported by DecimalConverter");
        } catch (NumberFormatException nfe) {
            this.logger.log(ILogger.LEVEL_ERROR,
                    "Error parsing " + source + " to class " +
                            targetClass.getName() + ": " + nfe.toString());
        }
        return result;
    }

}
