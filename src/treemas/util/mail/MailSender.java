package treemas.util.mail;

import treemas.util.constant.PropertiesKey;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;
import treemas.util.tool.ConfigurationProperties;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import java.util.Properties;


public class MailSender {
    private static ILogger logger = LogManager.getDefaultLogger();

    public MailSender() {
    }

    public void send(MailBean mail) throws MailException {
        Properties properties = new Properties();
        properties.setProperty(PropertiesKey.MAIL_TRANSPORT_PROTOCOL, ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_TRANSPORT_PROTOCOL));
        properties.setProperty(PropertiesKey.MAIL_HOST, ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_HOST));

        properties.put(PropertiesKey.MAIL_PORT, ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_PORT));
        properties.put(PropertiesKey.MAIL_STARTTLS_ENABLE, ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_STARTTLS_ENABLE));
        properties.put(PropertiesKey.MAIL_SMTP_AUTH, ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_SMTP_AUTH));

        Session mailSession = null;
        Transport transport = null;

        try {
            mailSession = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_SENDER_EMAIL),
                            ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_SENDER_PASSWORD));
                }
            });
            transport = mailSession.getTransport();

            MimeMessage msg = new MimeMessage(mailSession);
            msg.setFrom(mail.getFrom());
            msg.addRecipient(Message.RecipientType.TO, mail.getTo());
            msg.setSubject(mail.getSubject());
            msg.setContent(mail.getMessage(), "text/html");

            logger.log(ILogger.LEVEL_INFO, "Start send mail to: " + mail.getTo().getAddress());

            transport.connect();
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();

            logger.log(ILogger.LEVEL_INFO, "Mail to: " + mail.getTo().getAddress() + " sent.");
        } catch (NoSuchProviderException e) {
            logger.printStackTrace(e);
            throw new MailException(MailException.ERROR_MAIL_RELAY_FAILED);
        } catch (MessagingException me) {
            logger.printStackTrace(me);
            throw new MailException(MailException.ERROR_MAIL_RELAY_FAILED);
        } catch (Exception ex) {
            logger.printStackTrace(ex);
            throw new MailException(MailException.ERROR_UNKNOWN);
        }


    }
}
