package treemas.util.mail;

import treemas.util.AppException;

public class MailException extends AppException {
    public static final int ERROR_UNKNOWN = -1;
    public static final int ERROR_MAIL_TEMPLATE_NOT_FOUND = 201;
    public static final int ERROR_MAIL_TEMPLATE_IO = 202;
    public static final int ERROR_MAIL_RELAY_FAILED = 203;
    public static final int ERROR_MAIL_CREATE_ADDRESS = 204;

    public MailException(int errorCode) {
        this("", null, errorCode, null);
    }

    public MailException(int errorCode, Object userObject) {
        this("", null, errorCode, userObject);
    }

    public MailException(Throwable cause, int errorCode) {
        this("", cause, errorCode, null);
    }

    public MailException(String msg, Throwable cause, int errorCode,
                         Object userObject) {
        super(msg, cause, errorCode, userObject);
    }
}
