package treemas.util.mail;

import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import javax.servlet.ServletContext;

public class MailMessageBuilder {
    private static ILogger logger = LogManager.getDefaultLogger();

    public MailMessageBuilder() {
    }

    public String buildMessage(
            ServletContext context, String recipient,
            String surveyId, String priority, String remarks,
            String customerName, String customerPhone, String CustomerAddress,
            String officePhone)
            throws MailException {
        StringBuffer result = null;

        try {
            MailTemplateReader templateReader = new MailTemplateReader();
            result = new StringBuffer(templateReader.getTemplate(context));

            this.replace(result, Tag.TAG_RECIPIENT, recipient);
            this.replace(result, Tag.TAG_SURVEY_ID, surveyId);
            this.replace(result, Tag.TAG_PRIORITY, priority);
            this.replace(result, Tag.TAG_REMARKS, remarks);
            this.replace(result, Tag.TAG_CUSTOMER_NAME, customerName);
            this.replace(result, Tag.TAG_CUSTOMER_PHONE, customerPhone);
            this.replace(result, Tag.TAG_OFFICE_PHONE, officePhone);
            this.replace(result, Tag.TAG_CUSTOMER_ADDRESS, CustomerAddress);
        } catch (MailException me) {
            logger.printStackTrace(me);
            throw me;
        }

        return result.toString();
    }

    public String buildMessage(
            ServletContext context, TagDataBean dataBean)
            throws MailException {
        StringBuffer result = null;

        try {
            MailTemplateReader templateReader = new MailTemplateReader();
            result = new StringBuffer(templateReader.getTemplate(context));

            this.replace(result, Tag.TAG_RECIPIENT, dataBean.getRecipient());
            this.replace(result, Tag.TAG_SURVEY_ID, dataBean.getSurveyId());
            this.replace(result, Tag.TAG_PRIORITY, dataBean.getPriority());
            this.replace(result, Tag.TAG_REMARKS, dataBean.getRemarks());
            this.replace(result, Tag.TAG_CUSTOMER_NAME, dataBean.getCustomerName());
            this.replace(result, Tag.TAG_CUSTOMER_PHONE, dataBean.getCustomerPhone());
            this.replace(result, Tag.TAG_OFFICE_PHONE, dataBean.getOfficePhone());
            this.replace(result, Tag.TAG_CUSTOMER_ADDRESS, dataBean.getCustomerAddress());
        } catch (MailException me) {
            logger.printStackTrace(me);
            throw me;
        }

        return result.toString();
    }

    private void replace(StringBuffer message, String tag, String replacement) {
        int startTag = message.indexOf(tag);
        int endTag = startTag + tag.length();
        if (replacement != null && !"".equals(replacement.trim())) {
            message.replace(startTag, endTag, replacement);
        } else {
            message.replace(startTag, endTag, "(N/A)");
        }
    }
}
