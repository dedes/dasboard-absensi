package treemas.util.mail;

import treemas.util.constant.PropertiesKey;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;
import treemas.util.tool.ConfigurationProperties;

import javax.servlet.ServletContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MailTemplateReader {
    private static ILogger logger = LogManager.getDefaultLogger();

    String getTemplate(ServletContext context) throws MailException {
        StringBuffer temp = new StringBuffer();

        String templateFullPath = ConfigurationProperties.getInstance().get(
                PropertiesKey.MAIL_TEMPLATE_PATH);
        InputStream is = context.getResourceAsStream(templateFullPath);

        if (is != null) {
            InputStreamReader isr = null;
            BufferedReader reader = null;
            try {
                isr = new InputStreamReader(is);
                reader = new BufferedReader(isr);
                String lineText = null;
                while ((lineText = reader.readLine()) != null)
                    temp.append(lineText);
            } catch (IOException ioe) {
                throw new MailException(ioe, MailException.ERROR_MAIL_TEMPLATE_IO);
            } finally {
                try {
                    if (reader != null)
                        reader.close();
                    if (isr != null)
                        isr.close();
                } catch (IOException ioe) {
                    throw new MailException(ioe, MailException.ERROR_MAIL_TEMPLATE_IO);
                }
            }
        } else {
            logger.log(ILogger.LEVEL_INFO, "Cannot find resource: " + templateFullPath);
            throw new MailException(MailException.ERROR_MAIL_TEMPLATE_NOT_FOUND);
        }

        return temp.toString();
    }
}
