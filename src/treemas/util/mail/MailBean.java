package treemas.util.mail;

import treemas.util.constant.PropertiesKey;
import treemas.util.tool.ConfigurationProperties;

import javax.mail.internet.InternetAddress;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;


public class MailBean implements Serializable {
    private InternetAddress from;
    private InternetAddress to;
    private String message;
    private String subject;

    public MailBean() throws MailException {
        String emailFrom = ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_SENDER_EMAIL);
        String nameFrom = ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_SENDER_NAME);
        String subject = ConfigurationProperties.getInstance().get(PropertiesKey.MAIL_SUBJECT);
        try {
            this.from = new InternetAddress(emailFrom, nameFrom);
            this.subject = subject;
        } catch (UnsupportedEncodingException e) {
            throw new MailException(MailException.ERROR_MAIL_CREATE_ADDRESS);
        }
    }

    public MailBean(String toAddress, String toName) throws MailException {
        this();
        try {
            this.to = new InternetAddress(toAddress, toName);
        } catch (UnsupportedEncodingException e) {
            throw new MailException(MailException.ERROR_MAIL_CREATE_ADDRESS);
        }
    }

    public InternetAddress getFrom() {
        return from;
    }

    public void setFrom(InternetAddress from) {
        this.from = from;
    }

    public InternetAddress getTo() {
        return to;
    }

    public void setTo(InternetAddress to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        subject = subject;
    }

}
