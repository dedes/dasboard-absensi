package treemas.util.mail;

public abstract class Tag {
    public static final String TAG_RECIPIENT = "[recipient]";
    public static final String TAG_SURVEY_ID = "[survey_id]";
    public static final String TAG_PRIORITY = "[priority]";
    public static final String TAG_REMARKS = "[remarks]";
    public static final String TAG_CUSTOMER_NAME = "[customer_name]";
    public static final String TAG_CUSTOMER_PHONE = "[customer_phone]";
    public static final String TAG_OFFICE_PHONE = "[office_phone]";
    public static final String TAG_CUSTOMER_ADDRESS = "[customer_address]";
}
