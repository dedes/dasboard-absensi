package treemas.util.cryptography;

import treemas.util.encoder.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class RC4 {
    protected static final String key20 = "8PHy8/T19vf4+YGCg4SFhoeIiZE=";
    protected static final byte[] byteKey = Base64.decode(key20);

    private static byte[] encrypt(byte[] toEncrypt, byte[] keys) throws Exception {
        SecretKeySpec key = new SecretKeySpec(keys, "RC4");
        // create an instance of cipher
        Cipher cipher = Cipher.getInstance("RC4");
        // initialize the cipher with the key
        cipher.init(Cipher.ENCRYPT_MODE, key);
        // enctypt!
        byte[] encrypted = cipher.doFinal(toEncrypt);

        return encrypted;
    }

    private static byte[] decrypt(byte[] toDecrypt, byte[] keys) throws Exception {
        Cipher cipher = Cipher.getInstance("RC4");
        // create an instance of cipher
        SecretKeySpec key = new SecretKeySpec(keys, "RC4");
        // initialize the cipher with the key
        cipher.init(Cipher.DECRYPT_MODE, key);
        // decrypt!
        byte[] decrypted = cipher.doFinal(toDecrypt);
        return (decrypted);
    }

    public static String encrypt(byte[] toEncrypt) throws Exception {
        String result = "";
        result = new String(encrypt(toEncrypt, byteKey));
        return result;
    }

    public static String decrypt(byte[] toDecrypt) throws Exception {
        String result = "";
        result = new String(decrypt(toDecrypt, byteKey), "UTF-8");
        return result;
    }

    public static void main(String[] args) {
        String test = "Vincentius Ardha Dian Rigitama";
        try {
            String encrypt = new String(SAK_Formater.encodebase64(RC4.encrypt(test.getBytes()).getBytes()));
            String decrypt = RC4.decrypt(SAK_Formater.decodebase64(encrypt));
            System.out.println(encrypt);
            System.out.println(decrypt);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
