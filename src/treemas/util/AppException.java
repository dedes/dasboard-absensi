package treemas.util;

public class AppException extends Exception {

    protected int errorCode = 0;

    protected Object userObject = null;

    public AppException(int code) {
        this("", null, code, null);
    }

    public AppException(Throwable cause) {
        this("", cause, 0, null);
    }

    public AppException(Throwable cause, int code) {
        this("", cause, code, null);
    }

    public AppException(String msg, Throwable cause, int code, Object userObject) {
        super(msg, cause);
        this.errorCode = code;
        this.userObject = userObject;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public Object getUserObject() {
        return this.userObject;
    }

    public String toString() {
        String userObjTxt = this.userObject == null ? "" : ". User object="
                + this.userObject.toString();
        if (super.getCause() == null) {
            return super.toString() + ". Error code=" + this.errorCode
                    + userObjTxt;
        } else {
            return super.toString() + ". Error code=" + this.errorCode
                    + userObjTxt + "\n\tcause=" + super.getCause().toString();
        }
    }

}
