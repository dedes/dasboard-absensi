package treemas.util.auditrail;

public class AuditInfo implements java.io.Serializable {
    private String auditTableName;
    private String[] auditColumns;
    private String[] auditDbColumns;
    private String[] auditPrimaryKeys;


    public AuditInfo() {
    }

    public String getAuditTableName() {
        return auditTableName;
    }

    public void setAuditTableName(String auditTableName) {
        this.auditTableName = auditTableName;
    }

    public String[] getAuditColumns() {
        return auditColumns;
    }

    public void setAuditColumns(String[] auditColumns) {
        this.auditColumns = auditColumns;
    }

    public String[] getAuditPrimaryKeys() {
        return auditPrimaryKeys;
    }

    public void setAuditPrimaryKeys(String[] auditPrimaryKeys) {
        this.auditPrimaryKeys = auditPrimaryKeys;
    }

    public String[] getAuditDbColumns() {
        return auditDbColumns;
    }

    public void setAuditDbColumns(String[] auditDbColumns) {
        this.auditDbColumns = auditDbColumns;
    }

}
