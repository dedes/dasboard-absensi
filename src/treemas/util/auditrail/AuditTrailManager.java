package treemas.util.auditrail;

import com.ibatis.sqlmap.client.SqlMapClient;
import treemas.util.constant.SQLConstant;
import treemas.util.format.TreemasFormatter;
import treemas.util.format.TreemasNumberFormat;
import treemas.util.ibatis.IbatisHelper;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;
import treemas.util.struts.ManagerBase;
import treemas.util.tool.ClassMatcher;
import treemas.util.tool.Tools;

import java.sql.*;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;

public class AuditTrailManager extends ManagerBase {
    public static final String FORMAT_DATETIME = "yyyyMMdd hh:mm:ss";
    public static final TreemasNumberFormat NUMBER_FORMAT = new TreemasNumberFormat(',', '.', 4, false);

    private static final String STRING_SQL = SQLConstant.SQL_AUDIT;

    private static final String ACTION_ADD = "ADD";
    private static final String ACTION_EDIT = "EDIT";
    private static final String ACTION_DELETE = "DELETE";
    private final String SQL_AUDIT =
            new StringBuffer("")
                    .append("INSERT INTO AUDITTRAIL ( ")
                    .append(
                            "TRANSACTIONDATE, TABLENAME, FIELDNAME, ACTION, OLDVALUE, NEWVALUE, KEYVALUE, USERID, AUDITDESC ")
                    .append(") VALUES (")
                    .append("?, ?, ?, ?, ?, ?, ?, ?, ?) ")
                    .toString();
    protected ILogger logger = LogManager.getDefaultLogger();
    protected SqlMapClient ibatisSqlMap = null;

    public AuditTrailManager() {
        this.ibatisSqlMap = IbatisHelper.getSqlMap();
    }

    private AuditInfo retrieveInfo(Auditable auditable) {
        AuditInfo info = new AuditInfo();
        info.setAuditColumns(auditable.getAuditColumns());
        info.setAuditDbColumns(auditable.getAuditDBColumns());
        info.setAuditPrimaryKeys(auditable.getAuditPrimaryKeys());
        info.setAuditTableName(auditable.getAuditTableName());
        return info;
    }

    public void auditAdd(Connection conn, Auditable auditable, String userId, String message) throws AuditTrailException {
        Object audited = auditable;
        auditAdd(conn, audited, this.retrieveInfo(auditable), userId, message);
    }

    public void auditEdit(Connection conn, Auditable auditable, String userId, String auditDesc) throws AuditTrailException {
        Object audited = auditable;
        auditEdit(conn, audited, this.retrieveInfo(auditable), userId, auditDesc);
    }

    public void auditDelete(Connection conn, Auditable auditable, String userId, String message) throws AuditTrailException {
        Object audited = auditable;
        auditDelete(conn, audited, this.retrieveInfo(auditable), userId, message);
    }

    public void auditAdd(Connection conn, Object audited, AuditInfo info,
                         String userId, String auditDesc) throws AuditTrailException {
        String[] columns = info.getAuditColumns();
        String[] dbColumns = info.getAuditDbColumns();
        String[] primaryKeys = info.getAuditPrimaryKeys();
        String tableName = info.getAuditTableName();

        this.checks(columns, dbColumns, primaryKeys, tableName);

        String[] fieldValues = this.getValuesFromObject(audited, columns);

        String[] pkValues = new String[primaryKeys.length];
        for (int i = 0; i < pkValues.length; i++) {
            int idx = Tools.indexOf(columns, primaryKeys[i]);
            if (idx == -1)
                throw new AuditTrailException("auditPrimaryKeyFields has field "
                        + primaryKeys[i] +
                        " which is not in auditFields values");
            pkValues[i] = fieldValues[idx];
        }

        try {
            this.doAudit(conn, ACTION_ADD, userId, auditDesc, tableName, dbColumns, null,
                    fieldValues, pkValues);
        } catch (SQLException sqe) {
            throw new AuditTrailException("Error inserting to AuditTrail: "
                    + sqe.toString(), sqe);
        }
    }

    public void auditEdit(Connection conn, Object audited, AuditInfo info,
                          String userId, String auditDesc) throws AuditTrailException {
        String[] columns = info.getAuditColumns();
        String[] dbColumns = info.getAuditDbColumns();
        String[] primaryKeys = info.getAuditPrimaryKeys();
        String tableName = info.getAuditTableName();

        this.checks(columns, dbColumns, primaryKeys, tableName);

        String[] fieldValues = this.getValuesFromObject(audited, columns);
        String[] pkValues = this.getValuesFromObject(audited, primaryKeys);

        try {
            String[] oldValues = this.getOldValues(conn, audited, tableName,
                    columns, dbColumns, primaryKeys);
            this.doAudit(conn, ACTION_EDIT, userId, auditDesc, tableName, dbColumns,
                    oldValues,
                    fieldValues, pkValues);
        } catch (SQLException sqe) {
            throw new AuditTrailException("Error inserting to AuditTrail: "
                    + sqe.toString(), sqe);
        }
    }

    public void auditDelete(Connection conn, Object audited, AuditInfo info,
                            String userId, String auditDesc) throws AuditTrailException {
        String[] columns = info.getAuditColumns();
        String[] dbColumns = info.getAuditDbColumns();
        String[] primaryKeys = info.getAuditPrimaryKeys();
        String tableName = info.getAuditTableName();

        this.checks(columns, dbColumns, primaryKeys, tableName);

        String[] pkValues = this.getValuesFromObject(audited, primaryKeys);

        try {
            String[] oldValues = this.getOldValues(conn, audited, tableName,
                    columns, dbColumns, primaryKeys);
            this.doAudit(conn, ACTION_DELETE, userId, auditDesc, tableName,
                    dbColumns, oldValues,
                    null, pkValues);
        } catch (SQLException sqe) {
            throw new AuditTrailException("Error inserting to AuditTrail: "
                    + sqe.toString(), sqe);
        }
    }

    private String[] getPrimaryKeyColumns(String[] fields, String[] dbFields,
                                          String[] pkFields) throws AuditTrailException {
        String[] result = new String[pkFields.length];
        for (int i = 0; i < result.length; i++) {
            int idx = Tools.indexOf(fields, pkFields[i]);
            if (idx == -1)
                throw new AuditTrailException("auditPrimaryKeyFields has field "
                        + pkFields[i] +
                        " which is not in auditFields values");
            result[i] = dbFields[idx];
        }
        return result;
    }

    private String[] getOldValues(Connection conn, Object audited,
                                  String tableName, String[] columns,
                                  String[] dbColumns, String[] primaryKeys) throws
            AuditTrailException,
            SQLException {
        String[] result = new String[columns.length];
        String[] primaryKeyColumns = this.getPrimaryKeyColumns(columns, dbColumns, primaryKeys);
        Object[] primaryKeyValues = this.getObject(audited, primaryKeys);

        StringBuffer sb = new StringBuffer();

        PreparedStatement pstmt = null;
        sb.append("SELECT ").append(Tools.implode(dbColumns, ",")).append(" FROM ")
                .append(tableName).append(" with (nolock) WHERE ");

        for (int i = 0; i < primaryKeyColumns.length; i++) {
            if (i > 0) {
                sb.append(" AND ");
            }
            sb.append(primaryKeyColumns[i]).append("=").append("?");
        }

        pstmt = conn.prepareStatement(sb.toString());

        for (int i = 0; i < primaryKeyColumns.length; i++) {
            pstmt.setObject(i + 1, primaryKeyValues[i]);
        }

        if (this.logger.isLoggable(ILogger.LEVEL_DEBUG_MEDIUM)) {
            this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, "SQL=" + sb.toString());
        }

        ResultSet rs = pstmt.executeQuery();

        try {
            ResultSetMetaData meta = rs.getMetaData();
            if (rs.next()) {
                for (int i = 0; i < columns.length; i++) {
                    if (this.logger.isLoggable(ILogger.LEVEL_DEBUG_MEDIUM)) {
                        this.logger.log(ILogger.LEVEL_DEBUG_MEDIUM, meta.getColumnName(i + 1) + "=" +
                                meta.getColumnTypeName(i + 1) + "/"
                                + meta.getColumnClassName(i + 1));
                    }
                    Object obj = rs.getObject(i + 1);
                    if (!rs.wasNull()) {
                        result[i] = this.objectValueToString(obj);
                    } else
                        result[i] = null;
                }
            } else
                throw new AuditTrailException(
                        "Cannot find data when retrieving old value from table "
                                + tableName);
        } finally {
            rs.close();
            pstmt.close();
        }

        return result;
    }

    private void checks(String[] fields, String[] dbFields, String[] pkFields,
                        String tableName) throws AuditTrailException {
        if (tableName == null)
            throw new AuditTrailException("auditTableName must not be NULL");
        if (fields == null || dbFields == null ||
                fields.length == 0 || dbFields.length == 0)
            throw new AuditTrailException(
                    "auditFields and/or auditDbFields can not be NULL or zero-length array!");
        if (fields.length != dbFields.length)
            throw new AuditTrailException(
                    "getAuditFields() and getAuditDBFields() mast have same length!");
        if (pkFields == null || pkFields.length == 0)
            throw new AuditTrailException(
                    "auditPrimaryKeyFields cannot be NULL or zero-length array!");
    }

    private String[] getValuesFromObject(Object audited,
                                         String[] fields) throws AuditTrailException {
        String[] fieldValues = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            try {
                String methodName = "get"
                        + Character.toUpperCase(fields[i].charAt(0)) +
                        fields[i].substring(1);
                Object val = ClassMatcher.invokeMethod(audited, methodName, null, null);
                fieldValues[i] = null;
                if (val != null) {
                    fieldValues[i] = this.objectValueToString(val);
                }
            } catch (NoSuchMethodException noe) {
                throw new AuditTrailException(
                        "Cannot find getter method for field " +
                                fields[i], noe);
            } catch (IllegalAccessException iae) {
                throw new AuditTrailException(
                        "Illegal access invoking getter method for field " +
                                fields[i], iae);
            } catch (java.lang.reflect.InvocationTargetException ite) {
                throw new AuditTrailException(
                        "Exception occured when invoking getter method for field " +
                                fields[i], ite);
            }
        }
        return fieldValues;
    }

    private Object[] getObject(Object audited, String[] fields) throws AuditTrailException {
        Object[] fieldValues = new Object[fields.length];
        for (int i = 0; i < fields.length; i++) {
            try {
                String methodName = "get"
                        + Character.toUpperCase(fields[i].charAt(0)) +
                        fields[i].substring(1);
                Object val = ClassMatcher.invokeMethod(audited, methodName, null, null);
                fieldValues[i] = null;
                if (val != null) {
                    fieldValues[i] = val;
                    if (fieldValues[i] instanceof java.util.Date) {
                        fieldValues[i] = new Timestamp(((java.util.Date) fieldValues[i]).getTime());
                    }
                }
            } catch (NoSuchMethodException noe) {
                throw new AuditTrailException(
                        "Cannot find getter method for field " +
                                fields[i], noe);
            } catch (IllegalAccessException iae) {
                throw new AuditTrailException(
                        "Illegal access invoking getter method for field " +
                                fields[i], iae);
            } catch (java.lang.reflect.InvocationTargetException ite) {
                throw new AuditTrailException(
                        "Exception occured when invoking getter method for field " +
                                fields[i], ite);
            }
        }
        return fieldValues;
    }

    private String objectValueToString(Object obj) {
        String result = null;
        if (obj instanceof java.lang.String) {
            result = obj.toString();
        } else if (obj instanceof java.util.Date) {
            DateFormat dFormatter = TreemasFormatter.getDateFormat(FORMAT_DATETIME);
            result = dFormatter.format((java.util.Date) obj);
        } else if (obj instanceof java.lang.Double) {
            result = TreemasFormatter.getNumberFormat(NUMBER_FORMAT)
                    .format(((Double) obj).doubleValue());
        } else if (obj instanceof java.lang.Float) {
            result = TreemasFormatter.getNumberFormat(NUMBER_FORMAT)
                    .format(((Float) obj).floatValue());
        } else if (obj instanceof java.math.BigDecimal) {
            result = TreemasFormatter.getNumberFormat(NUMBER_FORMAT)
                    .format(((java.math.BigDecimal) obj).doubleValue());
        } else if (obj instanceof java.math.BigInteger) {
            result = obj.toString();
        } else if (obj instanceof java.lang.Integer) {
            int val = ((Integer) obj).intValue();
            result = String.valueOf(val);
        } else if (obj instanceof java.lang.Long) {
            long val = ((Long) obj).longValue();
            result = String.valueOf(val);
        } else {
            this.logger.log(ILogger.LEVEL_INFO, "AuditTrailManager: Unresolve class " + obj.getClass().getName());
            result = obj.toString();
        }
        return result;
    }

    private void auditXML(String action, String userId, String desc,
                          String tableName, String columnNames[], String oldValues[],
                          String newValues[], String primaryKeyValues[]) throws SQLException {
        try {
            this.ibatisSqlMap.startTransaction();
            this.ibatisSqlMap.startBatch();
            for (int i = 0; i < columnNames.length; i++) {
                String oldValue = oldValues != null ? oldValues[i] : null;
                String newValue = newValues != null ? newValues[i] : null;

                oldValue = "".equals(oldValue) ? null : oldValue;
                newValue = "".equals(newValue) ? null : newValue;

                if (!ACTION_ADD.equals(action) && oldValue == newValue) {
                    continue;
                }
                if (oldValue != null) {
                    if (oldValue.equals(newValue))
                        continue;
                }

                String keyvalue = Tools.implode(primaryKeyValues, "||");

                Map param = new HashMap();
                param.put("action", action);
                param.put("usrupd", userId);
                param.put("tablename", tableName);
                param.put("keyvalue", keyvalue);
                param.put("fieldname", columnNames[i]);
                param.put("oldval", oldValue);
                param.put("newval", newValue);
                param.put("desc", desc);
                this.ibatisSqlMap.insert(this.STRING_SQL + "doAudit", param);
            }
            this.ibatisSqlMap.executeBatch();
            this.ibatisSqlMap.commitTransaction();
        } finally {
            this.ibatisSqlMap.endTransaction();
        }
    }

    private void auditStatement(
            Connection conn, String action, String userId, String desc,
            String tableName, String columnNames[], String oldValues[],
            String newValues[], String primaryKeyValues[]) throws SQLException {
        PreparedStatement pstmt = conn.prepareStatement(SQL_AUDIT);
        String keyValue = Tools.implode(primaryKeyValues, "||");
        Timestamp now = new Timestamp(System.currentTimeMillis());
        try {
            for (int i = 0; i < columnNames.length; i++) {
                String oldValue = oldValues != null ? oldValues[i] : null;
                String newValue = newValues != null ? newValues[i] : null;

                oldValue = "".equals(oldValue) ? null : oldValue;
                newValue = "".equals(newValue) ? null : newValue;

                if (!ACTION_ADD.equals(action) && oldValue == newValue) {
                    continue;
                }
                if (oldValue != null) {
                    if (oldValue.equals(newValue))
                        continue;
                }

                pstmt.setTimestamp(1, now);
                pstmt.setString(2, tableName);
                pstmt.setString(3, columnNames[i]);
                pstmt.setString(4, action);

                if (oldValue != null)
                    pstmt.setString(5, oldValue);
                else
                    pstmt.setNull(5, Types.VARCHAR);

                if (newValue != null)
                    pstmt.setString(6, newValue);
                else
                    pstmt.setNull(6, Types.VARCHAR);

                pstmt.setString(7, keyValue);
                pstmt.setString(8, userId);
                pstmt.setString(9, desc);
                int j = pstmt.executeUpdate();
                if (j == 0)
                    this.logger.log(ILogger.LEVEL_WARNING,
                            "Execute update returns 0 for action/table " +
                                    action + "/" + tableName);
            }
        } finally {
            pstmt.close();
        }
    }

    private void doAudit(
            Connection conn, String action, String userId, String desc,
            String tableName, String columnNames[], String oldValues[],
            String newValues[], String primaryKeyValues[]) throws SQLException {

        if (null == conn) {
            this.auditXML(action, userId, desc, tableName, columnNames, oldValues, newValues, primaryKeyValues);
        } else {
            this.auditStatement(conn, action, userId, desc, tableName, columnNames, oldValues, newValues, primaryKeyValues);
        }


    }

}
