package treemas.util.auditrail;

public interface Auditable extends java.io.Serializable {

    public String getAuditTableName();

    public String[] getAuditColumns();

    public String[] getAuditPrimaryKeys();

    public String[] getAuditDBColumns();

}
