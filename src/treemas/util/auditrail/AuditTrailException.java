package treemas.util.auditrail;

import treemas.util.AppException;

public class AuditTrailException extends AppException {

    public AuditTrailException(String msg, Throwable cause) {
        super(msg, cause, 0, null);
    }

    public AuditTrailException(String msg) {
        this(msg, null);
    }
}
