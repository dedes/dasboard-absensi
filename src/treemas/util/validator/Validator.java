package treemas.util.validator;

import com.google.common.base.Strings;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.CoreException;
import treemas.util.constant.Global;
import treemas.util.format.TreemasFormatter;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;
import treemas.util.struts.ApplicationResources;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static treemas.util.validator.ValidatorProperties.*;


public class Validator {
    public static ILogger logger = LogManager.getDefaultLogger();
    //	private final String maskAlfaNumerik = "[A-Za-z0-9]*";
    public final String PATTERN_TIME_HM = "[\\d]{2}:[\\d]{2}";
    public final String PATTERN_TIME_HMS = "[\\d]{2}:[\\d]{2}:[\\d]{2}";
    public final String PATTERN_DATE_8 = "[\\d]{8}";
    public final String PATTERN_DATE_6 = "[\\d]{6}";
    public final String PATTERN_DATE_8_TIME_4 = "[\\d]{12}";
    public final String PATTERN_DATE_DMY_SHORT_SEPARATOR = "[\\d]{2}/[\\d]{2}/[\\d]{2}";
    public final String PATTERN_DATE_DMY_SHORT_SEPARATOR_OTHER = "[\\d]{2}-[\\d]{2}-[\\d]{2}";
    public final String PATTERN_DATE_DMY_SEPARATOR = "[\\d]{2}/[\\d]{2}/[\\d]{4}";
    public final String PATTERN_DATE_DMY_SEPARATOR_OTHER = "[\\d]{2}-[\\d]{2}-[\\d]{1,4}";
    public final String PATTERN_DATE_YMD_SHORT_SEPARATOR = "[\\d]{2}/[\\d]{2}/[\\d]{2}";
    public final String PATTERN_DATE_YMD_SHORT_SEPARATOR_OTHER = "[\\d]{2}-[\\d]{2}-[\\d]{2}";
    public final String PATTERN_DATE_YMD_SEPARATOR = "[\\d]{4}/[\\d]{2}/[\\d]{2}";
    public final String PATTERN_DATE_YMD_SEPARATOR_OTHER = "[\\d]{4}-[\\d]{2}-[\\d]{2}";
    public ApplicationResources resources = ApplicationResources.getInstance();
    private Locale locale;

    //
    public Validator(HttpServletRequest request) {
        String lang = (String) request.getSession().getAttribute(Global.LANG);
        if (Strings.isNullOrEmpty(lang)) {
            this.resources = ApplicationResources.getInstanceInd();
        } else {
            if (Global.LANG_EN.equalsIgnoreCase(lang)) {
                this.resources = ApplicationResources.getInstanceEng();
            } else {
                this.resources = ApplicationResources.getInstance();
            }
        }
    }

    public Validator(Locale locale) {
        this.locale = locale;
    }

    public String getStringLength(int length) {
        return String.valueOf(length);
    }

    public int getIntegerLength(String length) {
        if (Strings.isNullOrEmpty(length))
            return 0;
        else
            return Integer.parseInt(length);
    }

    public BigDecimal getBigDecimalValue(String value) {
        if (Strings.isNullOrEmpty(value))
            return BigDecimal.ZERO;
        else
            return new BigDecimal(value);
    }

    public int messageSizeCounter(ActionMessages messages, int size) {
        if (null != messages) {
            size += messages.size();
        }
        return size;
    }

    public void rangeValidatorMinMax(ActionMessages messages, String path, String property, String minField, String maxField, String operator) throws CoreException {

        //wajib value tidak kosong
        BigDecimal minLength = new BigDecimal(minField);
        BigDecimal maxLength = new BigDecimal(maxField);

        if (ValidatorGlobal.TM_OPERATOR_KURANG_DARI.equals(operator) && !ValidatorHelper.isLessThanMax(minLength, maxLength)) {
            messages.add(property, new ActionMessage("errors.aging.min", new Object[]{resources.getMessage(path), resources.getMessage(path)}));
        } else if (ValidatorGlobal.TM_OPERATOR_KURANG_DARI_SAMA_DENGAN.equals(operator) && !ValidatorHelper.isLessThanEqualMax(minLength, maxLength)) {
            messages.add(property, new ActionMessage("errors.aging", new Object[]{resources.getMessage(path), resources.getMessage(path)}));
        } else if (ValidatorGlobal.TM_OPERATOR_SAMA_DENGAN.equals(operator) && !ValidatorHelper.isEqual(minLength, maxLength)) {
            messages.add(property, new ActionMessage("errors.aging.equal", new Object[]{resources.getMessage(path), resources.getMessage(path)}));
        }
    }

    public void rangeMoneyValidatorOperatorUsingZeroChecker(ActionMessages messages, String field, String path, String property, String minField, String maxField, String operator, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && (Strings.isNullOrEmpty(minField) || Strings.isNullOrEmpty(maxField))) {
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        } else if (ValidatorHelper.isLeadingZero(minField) || ValidatorHelper.isLeadingZero(maxField)) {
            messages.add(property, new ActionMessage("errors.zero", resources.getMessage(path)));
        } else {
            this.rangeMoneyValidatorOperator(messages, field, path, property, minField, maxField, operator, flagType, flagMandatory, flagFormat);
        }
    }

    public void rangeMoneyValidatorOperator(ActionMessages messages, String field, String path, String property, String minField, String maxField, String operator, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        if (Strings.isNullOrEmpty(operator)) {
            operator = "1";//=
        }
        int checkMin = this.minValidator(messages, minField, path, property, flagType, flagMandatory, flagFormat);
        int checkMax = this.maxValidator(messages, maxField, path, property, flagType, flagMandatory, flagFormat);
        if (checkMin == 0 && checkMax == 0)
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        else if (checkMin == 0 && checkMax != 0)
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        else if (checkMin != 0 && checkMax == 0)
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        else if (checkMin == -1 || checkMax == -1)
            messages.add(property, new ActionMessage("common.decimal.maskmsg", resources.getMessage(path)));
        else if (checkMin == 1 && checkMax == 1) {
            BigDecimal[] range = new BigDecimal[2];
            BigDecimal minLength = new BigDecimal(minField);
            BigDecimal maxLength = new BigDecimal(maxField);
            range[0] = minLength;
            range[1] = maxLength;
            if (ValidatorGlobal.TM_OPERATOR_KURANG_DARI.equals(operator) && !ValidatorHelper.isLessThanMax(minLength, maxLength)) {
                messages.add(property, new ActionMessage("errors.aging.min", new Object[]{resources.getMessage(path), resources.getMessage(path)}));
            } else if (ValidatorGlobal.TM_OPERATOR_KURANG_DARI_SAMA_DENGAN.equals(operator) && !ValidatorHelper.isLessThanEqualMax(minLength, maxLength)) {
                messages.add(property, new ActionMessage("errors.aging", new Object[]{resources.getMessage(path), resources.getMessage(path)}));
            } else if (ValidatorGlobal.TM_OPERATOR_SAMA_DENGAN.equals(operator) && !ValidatorHelper.isEqual(minLength, maxLength)) {
                messages.add(property, new ActionMessage("errors.aging.equal", new Object[]{resources.getMessage(path), resources.getMessage(path)}));
            }
        }
    }

    public void rangeValidatorOperatorUsingZeroChecker(ActionMessages messages, String field, String path, String property, String minField, String maxField, String operator, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && (Strings.isNullOrEmpty(minField) || Strings.isNullOrEmpty(maxField))) {
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        } else if (ValidatorHelper.isLeadingZero(minField) || ValidatorHelper.isLeadingZero(maxField)) {
            messages.add(property, new ActionMessage("errors.zero", resources.getMessage(path)));
        } else {
            this.rangeValidatorOperator(messages, field, path, property, minField, maxField, operator, flagType, flagMandatory, flagFormat);
        }
    }

    public void rangeValidatorOperator(ActionMessages messages, String field, String path, String property, String minField, String maxField, String operator, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        if (Strings.isNullOrEmpty(operator)) {
            operator = "1";//=
        }
        int checkMin = this.minValidator(messages, minField, path, property, flagType, flagMandatory, flagFormat);
        int checkMax = this.maxValidator(messages, maxField, path, property, flagType, flagMandatory, flagFormat);
        if (checkMin == 0 && checkMax == 0)
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        else if (checkMin == 0 && checkMax != 0)
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        else if (checkMin != 0 && checkMax == 0)
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        else if (checkMin == -1 || checkMax == -1)
            messages.add(property, new ActionMessage("errors.invalid", resources.getMessage(path)));
        else if (checkMin == 1 && checkMax == 1) {
            int[] range = new int[2];
            int minLength = Integer.parseInt(minField);
            int maxLength = Integer.parseInt(maxField);
            range[0] = minLength;
            range[1] = maxLength;
            if (ValidatorGlobal.TM_OPERATOR_KURANG_DARI.equals(operator) && !ValidatorHelper.isLessThanMax(minLength, maxLength)) {
                messages.add(property, new ActionMessage("errors.aging.min", new Object[]{resources.getMessage(path), resources.getMessage(path)}));
            } else if (ValidatorGlobal.TM_OPERATOR_KURANG_DARI_SAMA_DENGAN.equals(operator) && !ValidatorHelper.isLessThanEqualMax(minLength, maxLength)) {
                messages.add(property, new ActionMessage("errors.aging", new Object[]{resources.getMessage(path), resources.getMessage(path)}));
            } else if (ValidatorGlobal.TM_OPERATOR_SAMA_DENGAN.equals(operator) && !ValidatorHelper.isEqual(minLength, maxLength)) {
                messages.add(property, new ActionMessage("errors.aging.equal", new Object[]{resources.getMessage(path), resources.getMessage(path)}));
            }
        }
    }

    private void rangeValidator(ActionMessages messages, String field, String path, String property, String minField, String maxField, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        int[] range = new int[2];
        int minLength = Integer.parseInt(minField);
        int maxLength = Integer.parseInt(maxField);
        range[0] = minLength;
        range[1] = maxLength;
        if (ValidatorGlobal.TM_CHECKED_LENGTH_FIX == flagType && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isLengthValid(field, maxLength)) {
            messages.add(property, new ActionMessage("errors.length", new Object[]{resources.getMessage(path), String.valueOf(maxLength)}));
        }
        //LENGTH>=MIN && LENGTH<=MAX
        else if (ValidatorGlobal.TM_CHECKED_LENGTH_RANGE == flagType && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isInRange(field.length(), range)) {
            messages.add(property, new ActionMessage("errors.range.equal", new Object[]{resources.getMessage(path), String.valueOf(minLength), String.valueOf(maxLength)}));
        }
        //LENGTH>MIN && LENGTH<MAX
        else if (ValidatorGlobal.TM_CHECKED_LENGTH_RANGE_NOT_EQUAL == flagType && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isInEqualRange(field.length(), range)) {
            messages.add(property, new ActionMessage("errors.range", new Object[]{resources.getMessage(path), String.valueOf(minLength), String.valueOf(maxLength)}));
        }
        //LENGTH>=MIN && LENGTH<MAX
        else if (ValidatorGlobal.TM_CHECKED_LENGTH_RANGE_FOLLOW_MIN == flagType && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isInMinEqualRange(field.length(), range)) {
            messages.add(property, new ActionMessage("errors.range.min", new Object[]{resources.getMessage(path), String.valueOf(minLength), String.valueOf(maxLength)}));
        }
        //LENGTH>MIN && LENGTH<=MAX
        else if (ValidatorGlobal.TM_CHECKED_LENGTH_RANGE_FOLLOW_MAX == flagType && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isInMaxEqualRange(field.length(), range)) {
            messages.add(property, new ActionMessage("errors.range.max", new Object[]{resources.getMessage(path), String.valueOf(minLength), String.valueOf(maxLength)}));
        }
    }

    public void minMaxValidator(ActionMessages messages, String field, String minField, String maxField, String path, String property, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        int checkMin = this.minValidator(messages, minField, path, property, flagType, flagMandatory, flagFormat);
        int checkMax = this.maxValidator(messages, maxField, path, property, flagType, flagMandatory, flagFormat);
        if (checkMin == 0 && checkMax == 0)
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        else if (checkMin == 0 && checkMax != 0)
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        else if (checkMin != 0 && checkMax == 0)
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        else if (checkMin == -1 || checkMax == -1)
            messages.add(property, new ActionMessage("errors.invalid", resources.getMessage(path)));
        else if (checkMin == 1 && checkMax == 1)
            this.rangeValidator(messages, field, path, property, minField, maxField, flagType, flagMandatory, flagFormat);
    }

    private int minValidator(ActionMessages messages, String minField, String path, String property, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && Strings.isNullOrEmpty(minField)) {
            return 0;
        }
        //DECIMAL
        else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL == flagFormat && !ValidatorHelper.isDecimalOnly(minField)) {
            return -1;
        }
        //DIGIT
        else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT == flagFormat && !ValidatorHelper.isDigitOnly(minField)) {
            return -1;

        }
        //ALFANUMERIK
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK == flagFormat && !ValidatorHelper.isAlfanumerikOnly(minField)) {
            return -1;
        }
        return 1;
    }

    private int maxValidator(ActionMessages messages, String maxField, String path, String property, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && Strings.isNullOrEmpty(maxField)) {
            return 0;
        }
        //DECIMAL
        else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL == flagFormat && !ValidatorHelper.isDecimalOnly(maxField)) {
            return -1;
        }
        //DIGIT
        else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT == flagFormat && !ValidatorHelper.isDigitOnly(maxField)) {
            return -1;
        }
        //ALFANUMERIK
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK == flagFormat && !ValidatorHelper.isAlfanumerikOnly(maxField)) {
            return -1;
        }
        return 1;
    }

    public void geoDataValidator(ActionMessages messages, String field, String path, String property, String minLength, String maxLength, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && Strings.isNullOrEmpty(field)) {
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        } else if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && ValidatorGlobal.TM_NOT_CHECKED_LENGTH == flagType && Strings.isNullOrEmpty(field)) {
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        } else if (ValidatorGlobal.TM_CHECKED_LENGTH_MAX == flagType && !ValidatorHelper.isMaxLengthValid(field, this.getIntegerLength(maxLength))) {
            messages.add(property, new ActionMessage("errors.maxlength", new Object[]{resources.getMessage(path), this.getIntegerLength(maxLength)}));
        } else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL_ALLOW_ZERO == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isDigitDotOnly(field)) {
            messages.add(property, new ActionMessage("errors.invalid.digit", resources.getMessage(path)));
        } else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL_ALLOW_ZERO == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isDecimalOnly(field)) {
            messages.add(property, new ActionMessage("errors.maxdesimal", new Object[]{resources.getMessage(path), String.valueOf(MAX_DESIMAL)}));
        }
    }

    public void textValidator(ActionMessages messages, String field, String path, String property, String minLength, String maxLength, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && Strings.isNullOrEmpty(field)) {
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        }
        //NOT CHECKING LENGTH
        else if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && ValidatorGlobal.TM_NOT_CHECKED_LENGTH == flagType && Strings.isNullOrEmpty(field)) {
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        } else if (ValidatorGlobal.TM_CHECKED_LENGTH_MAX == flagType && !ValidatorHelper.isMaxLengthValid(field, this.getIntegerLength(maxLength))) {
            messages.add(property, new ActionMessage("errors.maxlength", new Object[]{resources.getMessage(path), this.getIntegerLength(maxLength)}));
        }
        //MAX LENGTH CHECK
//		else if(!ValidatorHelper.isMaxLengthValid(field,this.getIntegerLength(maxLength))){
//			messages.add(property, new ActionMessage("errors.maxlength", new Object[]{resources.getMessage(path),this.getIntegerLength(maxLength)}));
//		} 
        //DECIMAL
        else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL_ALLOW_ZERO == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isDecimalOnly(field)) {
            messages.add(property, new ActionMessage("errors.maxdesimal", new Object[]{resources.getMessage(path), String.valueOf(MAX_DESIMAL)}));
        } else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isDecimalOnly(field)) {
            messages.add(property, new ActionMessage("errors.maxdesimal", new Object[]{resources.getMessage(path), String.valueOf(MAX_DESIMAL)}));
        }
        //DIGIT
        else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isDigitOnly(field)) {
            messages.add(property, new ActionMessage("errors.invalid.digit", resources.getMessage(path)));
        }
        //ZERO CHECK
        else if ((ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL == flagFormat || ValidatorGlobal.TM_FLAG_FORMAT_DIGIT == flagFormat) && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isEmptyNumber(field, flagFormat)) {
            messages.add(property, new ActionMessage("errors.integer", new Object[]{resources.getMessage(path), String.valueOf(MAX_DESIMAL)}));
        }
        //CHAR
        else if (ValidatorGlobal.TM_FLAG_FORMAT_CHAR == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isChar(field)) {
            messages.add(property, new ActionMessage("errors.invalid.nospace", resources.getMessage(path)));
        }
        //ALFANUMERIK
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikOnly(field)) {
            messages.add(property, new ActionMessage("errors.invalid.alfanumerik", resources.getMessage(path)));
        }
        //ALFANUMERIK SPACE
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerik(field)) {
            messages.add(property, new ActionMessage("errors.invalid.alfanumerik.space", resources.getMessage(path)));
        }
        //ALFANUMERIK KURUNG
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_KURUNG == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikKurung(field)) {
            messages.add(property, new ActionMessage("errors.invalid.alfanumerik", resources.getMessage(path)));
        }
        //CHAR SPACE AND
        else if (ValidatorGlobal.TM_FLAG_FORMAT_CHAR_SPACE_AND == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isSpecialChar(field)) {
            messages.add(property, new ActionMessage("errors.invalid.nocharcterspc2", resources.getMessage(path)));
        }
//		public static final int TM_FLAG_FORMAT_ALFANUMERIK_WITH_DASH = 5;
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DASH == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikDash(field)) {
            messages.add(property, new ActionMessage("errors.invalid.nospace.special", new Object[]{resources.getMessage(path), "-"}));
        }
//		public static final int TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT = 6;
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikDot(field)) {
            messages.add(property, new ActionMessage("errors.invalid.nospace.special", new Object[]{resources.getMessage(path), "."}));
        }
//		public static final int TM_FLAG_FORMAT_ALFANUMERIK_WITH_UNDERSCORE = 7;
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_UNDERSCORE == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikUnderscore(field)) {
            messages.add(property, new ActionMessage("errors.invalid.nospace.special", new Object[]{resources.getMessage(path), "_"}));
        }
//		public static final int TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT_DASH = 8;
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT_DASH == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikDotDash(field)) {
            messages.add(property, new ActionMessage("errors.invalid.nospace.special2", new Object[]{resources.getMessage(path), ".", "-"}));
        }
//		public static final int TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT_DASH_UNDERSCORE = 9;
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT_DASH_UNDERSCORE == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikDDU(field)) {
            messages.add(property, new ActionMessage("errors.invalid.nospace.special3", new Object[]{resources.getMessage(path), ".", "-", "_"}));
        }
        //EMAIL
        else if (ValidatorGlobal.TM_FLAG_FORMAT_NOT_CHECKED == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isValidEmail(field) && ValidatorGlobal.TM_CUSTOM_EMAIL_FLAG == flagType) {
            messages.add(property, new ActionMessage("errors.notvalid.email", resources.getMessage(path)));
        } else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_FREE == flagFormat && !Strings.isNullOrEmpty(field)) {
        }
        //CHECK RANGE
        else if (ValidatorGlobal.TM_CHECKED_LENGTH_FIX == flagType || ValidatorGlobal.TM_CHECKED_LENGTH_RANGE == flagType) {
            this.rangeValidator(messages, field, path, property, minLength, maxLength, flagType, flagMandatory, flagFormat);
        }
    }

    public void comboValidator(ActionMessages messages, String field, String minField, String maxField, String path, String property, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && Strings.isNullOrEmpty(field)) {
            messages.add(property, new ActionMessage("common.harusPilih", resources.getMessage(path)));
        }
        //TAMBAH RANGE
        else if (ValidatorGlobal.TM_TAMBAH_RANGE.equals(field)) {
            this.minMaxValidator(messages, field, minField, maxField, path, property, flagType, flagMandatory, flagFormat);
        }
    }

    //
    public String customTextValidator(String field, String path, String property, String minLength, String maxLength, int flagType, int flagMandatory, int flagFormat) throws CoreException {
        String ret = null;
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && Strings.isNullOrEmpty(field)) {
            return resources.getMessage("errors.required", path);
        }
        //NOT CHECKING LENGTH
        else if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && ValidatorGlobal.TM_NOT_CHECKED_LENGTH == flagType && Strings.isNullOrEmpty(field)) {
            return resources.getMessage("errors.required", path);
        }
        //MAX LENGTH CHECK
        else if (!ValidatorHelper.isMaxLengthValid(field, this.getIntegerLength(maxLength))) {
            return resources.getMessage("errors.maxlength", new Object[]{path, this.getIntegerLength(maxLength)});
        }
        //DECIMAL
        else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isDecimalOnly(field)) {
            return resources.getMessage("errors.maxdesimal", new Object[]{path, String.valueOf(MAX_DESIMAL)});
        }
        //DIGIT
        else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isDigitOnly(field)) {
            return resources.getMessage("errors.invalid.digit", path);
        }
        //DIGIT ALLOW ZERO
        else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_ALLOW_ZERO == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isDigitOnly(field)) {
            return resources.getMessage("errors.integer2", path);
        }
        //ZERO CHECK
        else if ((ValidatorGlobal.TM_FLAG_FORMAT_DIGIT_DECIMAL == flagFormat || ValidatorGlobal.TM_FLAG_FORMAT_DIGIT == flagFormat) && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isEmptyNumber(field, flagFormat)) {
            return resources.getMessage("errors.integer", new Object[]{path, String.valueOf(MAX_DESIMAL)});
        }
        //CHAR
        else if (ValidatorGlobal.TM_FLAG_FORMAT_CHAR == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isChar(field)) {
            return resources.getMessage("errors.invalid.char", path);
        }
        //ALFANUMERIK
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikOnly(field)) {
            return resources.getMessage("errors.invalid.alfanumerik", path);
        }
        //ALFANUMERIK NO SPACE
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_SPACE == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerik(field)) {
            return resources.getMessage("errors.invalid.alfanumerik.space", path);
        }
//		public static final int TM_FLAG_FORMAT_ALFANUMERIK_WITH_DASH = 5;
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DASH == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikDash(field)) {
            return resources.getMessage("errors.invalid.nospace.special", new Object[]{path, "-"});
        }
//		public static final int TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT = 6;
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikDot(field)) {
            return resources.getMessage("errors.invalid.nospace.special", new Object[]{path, "."});
        }
//		public static final int TM_FLAG_FORMAT_ALFANUMERIK_WITH_UNDERSCORE = 7;
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_UNDERSCORE == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikUnderscore(field)) {
            return resources.getMessage("errors.invalid.nospace.special", new Object[]{path, "_"});
        }
//		public static final int TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT_DASH = 8;
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT_DASH == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikDotDash(field)) {
            return resources.getMessage("errors.invalid.nospace.special2", new Object[]{path, ".", "-"});
        }
//		public static final int TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT_DASH_UNDERSCORE = 9;
        else if (ValidatorGlobal.TM_FLAG_FORMAT_ALFANUMERIK_WITH_DOT_DASH_UNDERSCORE == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isAlfanumerikDDU(field)) {
            return resources.getMessage("errors.invalid.nospace.special3", new Object[]{path, ".", "-", "_"});
        }
        //CHECK RANGE
        else if (ValidatorGlobal.TM_CHECKED_LENGTH_FIX == flagType || ValidatorGlobal.TM_CHECKED_LENGTH_RANGE == flagType) {
            int[] range = new int[2];
            int minField = Integer.parseInt(minLength);
            int maxField = Integer.parseInt(maxLength);
            range[0] = minField;
            range[1] = maxField;
            if (ValidatorGlobal.TM_CHECKED_LENGTH_FIX == flagType && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isLengthValid(field, maxField)) {
                return resources.getMessage("errors.length", new Object[]{path, String.valueOf(maxLength)});
            }
            //LENGTH>=MIN && LENGTH<=MAX
            else if (ValidatorGlobal.TM_CHECKED_LENGTH_RANGE == flagType && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isInRange(field.length(), range)) {
                return resources.getMessage("errors.range.equal", new Object[]{path, String.valueOf(minLength), String.valueOf(maxLength)});
            }
            //LENGTH>MIN && LENGTH<MAX
            else if (ValidatorGlobal.TM_CHECKED_LENGTH_RANGE_NOT_EQUAL == flagType && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isInEqualRange(field.length(), range)) {
                return resources.getMessage("errors.range", new Object[]{path, String.valueOf(minLength), String.valueOf(maxLength)});
            }
            //LENGTH>=MIN && LENGTH<MAX
            else if (ValidatorGlobal.TM_CHECKED_LENGTH_RANGE_FOLLOW_MIN == flagType && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isInMinEqualRange(field.length(), range)) {
                return resources.getMessage("errors.range.min", new Object[]{path, String.valueOf(minLength), String.valueOf(maxLength)});
            }
            //LENGTH>MIN && LENGTH<=MAX
            else if (ValidatorGlobal.TM_CHECKED_LENGTH_RANGE_FOLLOW_MAX == flagType && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isInMaxEqualRange(field.length(), range)) {
                return resources.getMessage("errors.range.max", new Object[]{path, String.valueOf(minLength), String.valueOf(maxLength)});
            }
        }
        return ret;
    }

    public void dateRangeValidator(ActionMessages messages, String firstField, String secondField, String path, String property, int flagType, int flagMandatory, String flagFormat) throws CoreException {
        int firstFlag = this.dateSingleValidator(messages, firstField, path, property, flagType, flagMandatory, flagFormat);
        int secondFlag = this.dateSingleValidator(messages, secondField, path, property, flagType, flagMandatory, flagFormat);
        if (firstFlag == -3 || secondFlag == -3) {
            //check range tanggal
            try {
                Date startDate = TreemasFormatter.parseDate(firstField, flagFormat);
                Date endDate = TreemasFormatter.parseDate(secondField, flagFormat);
                if (startDate.equals(endDate)) {
                    messages.add(property, new ActionMessage("errors.date.equals", resources.getMessage(path)));
                } else if (endDate.before(startDate)) {
                    messages.add(property, new ActionMessage("errors.date.before", resources.getMessage(path)));
                }
            } catch (ParseException e) {
                messages.add(property, new ActionMessage("errors.date.startend", new Object[]{resources.getMessage(path), flagFormat}));
            }
        } else if (firstFlag == -1 || secondFlag == -1) {
            //salah format
            messages.add(property, new ActionMessage("errors.date.format", new Object[]{resources.getMessage(path), flagFormat}));
        } else if (firstFlag == -2 || secondFlag == -2) {
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        }

    }

    public void dateTimeRangeValidator_ddMMyyyyhhmm(ActionMessages messages, String firstField, String secondField, String path, String property, int flagType, int flagMandatory, String flagFormat) throws CoreException {
        int firstFlag = this.dateSingleValidator(messages, firstField, path, property, flagType, flagMandatory, flagFormat);
        int secondFlag = this.dateSingleValidator(messages, secondField, path, property, flagType, flagMandatory, flagFormat);
        if (firstFlag == -3 || secondFlag == -3) {
            //check range tanggal
            try {
                String date1[] = new String[5];
                ;
                String date2[] = new String[5];
                ;

                String firstDateTime;
                String secondDateTime;

                date1[0] = firstField.substring(0, 2);
                date1[1] = firstField.substring(2, 4);
                date1[2] = firstField.substring(4, 8);
                date1[3] = firstField.substring(8, 10);
                date1[4] = firstField.substring(10, 12);

                date2[0] = secondField.substring(0, 2);
                date2[1] = secondField.substring(2, 4);
                date2[2] = secondField.substring(4, 8);
                date2[3] = secondField.substring(8, 10);
                date2[4] = secondField.substring(10, 12);

                firstDateTime = date1[0] + "-" + date1[1] + "-" + date1[2] + " " + date1[3] + ":" + date1[4] + ":00";
                secondDateTime = date2[0] + "-" + date2[1] + "-" + date2[2] + " " + date2[3] + ":" + date2[4] + ":00";

                Date startDate = new SimpleDateFormat(ValidatorGlobal.TM_DATE_DMY_TIME_HMS).parse(firstDateTime);
                Date endDate = new SimpleDateFormat(ValidatorGlobal.TM_DATE_DMY_TIME_HMS).parse(secondDateTime);

                if (startDate.equals(endDate)) {
                    messages.add(property, new ActionMessage("errors.date.equals", resources.getMessage(path)));
                } else if (endDate.before(startDate)) {
                    messages.add(property, new ActionMessage("errors.date.before", resources.getMessage(path)));
                }
            } catch (ParseException e) {
                messages.add(property, new ActionMessage("errors.date.startend", new Object[]{resources.getMessage(path), flagFormat}));
            }
        } else if (firstFlag == -1 || secondFlag == -1) {
            //salah format
            messages.add(property, new ActionMessage("errors.date.format", new Object[]{resources.getMessage(path), flagFormat}));
        } else if (firstFlag == -2 || secondFlag == -2) {
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        }

    }

    public void dateRangeValidatorEqual(ActionMessages messages, String firstField, String secondField, String path, String property, int flagType, int flagMandatory, String flagFormat) throws CoreException {
        int firstFlag = this.dateSingleValidator(messages, firstField, path, property, flagType, flagMandatory, flagFormat);
        int secondFlag = this.dateSingleValidator(messages, secondField, path, property, flagType, flagMandatory, flagFormat);
        if (firstFlag == -3 || secondFlag == -3) {
            //check range tanggal
            try {
                Date startDate = TreemasFormatter.parseDate(firstField, flagFormat);
                Date endDate = TreemasFormatter.parseDate(secondField, flagFormat);
                if (endDate.before(startDate)) {
                    messages.add(property, new ActionMessage("errors.twoDates", resources.getMessage(path)));
                }
            } catch (ParseException e) {
                messages.add(property, new ActionMessage("errors.date.startend", new Object[]{resources.getMessage(path), flagFormat}));
            }
        } else if (firstFlag == -1 || secondFlag == -1) {
            //salah format
            messages.add(property, new ActionMessage("errors.date.format", new Object[]{resources.getMessage(path), flagFormat}));
        } else if (firstFlag == -2 || secondFlag == -2) {
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        }
    }

    public int dateSingleValidator(ActionMessages messages, String field, String path, String property, int flagType, int flagMandatory, String flagFormat) throws CoreException {
        String separator = "/";
        String[] date = new String[5];
        boolean isTrue = true;
        int flag = -3;
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && Strings.isNullOrEmpty(field)) {
            //mandatory dan kosong errors.date.startend
            isTrue = false;
            flag = -2;
        } else if (!this.dateFormatChecker(field, flagFormat)) {
            //format salah
            isTrue = false;
            flag = -1;
        } else if (ValidatorGlobal.TM_DATE_DMY.equals(flagFormat)) {
            //format 8 digit
            date[0] = field.substring(0, 1);
            date[1] = field.substring(2, 3);
            date[2] = field.substring(4);
            isTrue = this.dateChecker(date);
            flag = 0;
        } else if (ValidatorGlobal.TM_DATE_YMD.equals(flagFormat)) {
            date[2] = field.substring(0, 3);
            date[1] = field.substring(4, 5);
            date[0] = field.substring(6);
            isTrue = this.dateChecker(date);
            flag = 1;
        } else if (ValidatorGlobal.TM_DATE_DMY_SHORT.equals(flagFormat)) {
            date[0] = field.substring(0, 1);
            date[1] = field.substring(2, 3);
            date[2] = field.substring(4);
            isTrue = this.dateChecker(date);
            flag = 2;
        } else if (ValidatorGlobal.TM_DATE_YMD_SHORT.equals(flagFormat)) {
            //format 6 digit
            date[0] = field.substring(0, 3);
            date[1] = field.substring(4, 5);
            date[2] = field.substring(6);
            isTrue = this.dateChecker(date);
            flag = 3;
        } else if (ValidatorGlobal.TM_DATE_DMY_SEPARATOR.equals(flagFormat) || ValidatorGlobal.TM_DATE_YMD_SEPARATOR.equals(flagFormat)) {
            //format 10 digit /
            separator = "/";
            String[] temp = field.split(separator);
            if (ValidatorGlobal.TM_DATE_DMY_SEPARATOR.equals(flagFormat)) {
                date = this.cloneDate(temp, 0);
                flag = 4;
            } else if (ValidatorGlobal.TM_DATE_YMD_SEPARATOR.equals(flagFormat)) {
                date = this.cloneDate(temp, 1);
                flag = 5;
            }
            isTrue = this.dateChecker(date);
        } else if (ValidatorGlobal.TM_DATE_DMY_SEPARATOR_OTHER.equals(flagFormat) || ValidatorGlobal.TM_DATE_YMD_SEPARATOR_OTHER.equals(flagFormat)) {
            //format 10 digit -
            separator = "-";
            String[] temp = field.split(separator);
            if (ValidatorGlobal.TM_DATE_DMY_SEPARATOR_OTHER.equals(flagFormat)) {
                date = this.cloneDate(temp, 0);
                flag = 6;
            } else if (ValidatorGlobal.TM_DATE_YMD_SEPARATOR_OTHER.equals(flagFormat)) {
                date = this.cloneDate(temp, 1);
                flag = 7;
            }
            isTrue = this.dateChecker(date);
        } else if (ValidatorGlobal.TM_DATE_DMY_SHORT_SEPARATOR.equals(flagFormat) || ValidatorGlobal.TM_DATE_YMD_SHORT_SEPARATOR.equals(flagFormat)) {
            //format 8 digit /
            separator = "/";
            String[] temp = field.split(separator);
            if (ValidatorGlobal.TM_DATE_DMY_SHORT_SEPARATOR.equals(flagFormat)) {
                date = this.cloneDate(temp, 0);
                flag = 8;
            } else if (ValidatorGlobal.TM_DATE_YMD_SHORT_SEPARATOR.equals(flagFormat)) {
                date = this.cloneDate(temp, 1);
                flag = 9;
            }

            isTrue = this.dateChecker(date);
        } else if (ValidatorGlobal.TM_DATE_DMY_SHORT_SEPARATOR_OTHER.equals(flagFormat) || ValidatorGlobal.TM_DATE_YMD_SHORT_SEPARATOR_OTHER.equals(flagFormat)) {
            //format 8 digit -
            separator = "-";
            String[] temp = field.split(separator);
            if (ValidatorGlobal.TM_DATE_DMY_SHORT_SEPARATOR_OTHER.equals(flagFormat)) {
                date = this.cloneDate(temp, 0);
                flag = 10;
            } else if (ValidatorGlobal.TM_DATE_YMD_SHORT_SEPARATOR_OTHER.equals(flagFormat)) {
                date = this.cloneDate(temp, 1);
                flag = 11;
            }
            isTrue = this.dateChecker(date);
        } else if (ValidatorGlobal.TM_DATE_DMY_TIME_HM.equals(flagFormat)) {
            //format 8 date + 4 time
            if (12 == field.length()) {
                date[0] = field.substring(0, 2);
                date[1] = field.substring(2, 4);
                date[2] = field.substring(4, 8);
                date[3] = field.substring(8, 10);
                date[4] = field.substring(10, 12);

                isTrue = this.dateTimeChecker_ddMMyyyyhhmm(date);
                flag = 12;
            }
        }
        if (!isTrue) {
            return flag;
        }
        return -3;
    }


    public void timeValidator(ActionMessages messages, String field, String path, String property, int flagType, int flagMandatory, String flagFormat) throws CoreException {
        String separator = ":";
        String[] date = new String[3];
        boolean isTrue = true;
        int flag = 0;
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && Strings.isNullOrEmpty(field)) {
            //mandatory dan kosong
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        } else if (ValidatorGlobal.TM_TIME_HM.equals(flagFormat) && (!field.matches(this.PATTERN_TIME_HM) || ValidatorGlobal.TM_TIME_HMS.equals(flagFormat)) && !field.matches(this.PATTERN_TIME_HMS)) {
            messages.add(property, new ActionMessage("errors.time.format", new Object[]{resources.getMessage(path), flagFormat}));
        } else if (ValidatorGlobal.TM_TIME_HM.equals(flagFormat) && !this.timeFormatChecker(field, flagFormat)) {
            //format salah
            messages.add(property, new ActionMessage("errors.time.formathm", new Object[]{resources.getMessage(path), flagFormat}));
        } else if (ValidatorGlobal.TM_TIME_HMS.equals(flagFormat) && !this.timeFormatChecker(field, flagFormat)) {
            //format salah
            messages.add(property, new ActionMessage("errors.time.formathms", new Object[]{resources.getMessage(path), flagFormat}));
        }
    }

    public void dateValidator(ActionMessages messages, String field, String path, String property, int flagType, int flagMandatory, String flagFormat) throws CoreException {
        String separator = "/";
        String[] date = new String[5];
        boolean isTrue = true;
        int flag = 0;
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && Strings.isNullOrEmpty(field)) {
            //mandatory dan kosong
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        } else if (!this.dateFormatChecker(field, flagFormat)) {
            //format salah
            messages.add(property, new ActionMessage("errors.date.format", new Object[]{resources.getMessage(path), flagFormat}));
        } else if (ValidatorGlobal.TM_DATE_DMY.equals(flagFormat)) {
            //format 8 digit
            date[0] = field.substring(0, 2);
            date[1] = field.substring(2, 4);
            date[2] = field.substring(4);
            isTrue = this.dateChecker(date);
            flag = 0;
        } else if (ValidatorGlobal.TM_DATE_YMD.equals(flagFormat)) {
            date[2] = field.substring(0, 4);
            date[1] = field.substring(4, 6);
            date[0] = field.substring(6);
            isTrue = this.dateChecker(date);
            flag = 1;
        } else if (ValidatorGlobal.TM_DATE_DMY_SHORT.equals(flagFormat)) {
            date[0] = field.substring(0, 2);
            date[1] = field.substring(2, 4);
            date[2] = field.substring(4);
            isTrue = this.dateChecker(date);
            flag = 2;
        } else if (ValidatorGlobal.TM_DATE_YMD_SHORT.equals(flagFormat)) {
            //format 6 digit
            date[0] = field.substring(0, 4);
            date[1] = field.substring(4, 6);
            date[2] = field.substring(6);
            isTrue = this.dateChecker(date);
            flag = 3;
        } else if (ValidatorGlobal.TM_DATE_DMY_SEPARATOR.equals(flagFormat) || ValidatorGlobal.TM_DATE_YMD_SEPARATOR.equals(flagFormat)) {
            //format 10 digit /
            separator = "/";
            String[] temp = field.split(separator);
            if (ValidatorGlobal.TM_DATE_DMY_SEPARATOR.equals(flagFormat)) {
                date = this.cloneDate(temp, 0);
                flag = 4;
            } else if (ValidatorGlobal.TM_DATE_YMD_SEPARATOR.equals(flagFormat)) {
                date = this.cloneDate(temp, 1);
                flag = 5;
            }
            isTrue = this.dateChecker(date);
        } else if (ValidatorGlobal.TM_DATE_DMY_SEPARATOR_OTHER.equals(flagFormat) || ValidatorGlobal.TM_DATE_YMD_SEPARATOR_OTHER.equals(flagFormat)) {
            //format 10 digit -
            separator = "-";
            String[] temp = field.split(separator);
            if (ValidatorGlobal.TM_DATE_DMY_SEPARATOR_OTHER.equals(flagFormat)) {
                date = this.cloneDate(temp, 0);
                flag = 6;
            } else if (ValidatorGlobal.TM_DATE_YMD_SEPARATOR_OTHER.equals(flagFormat)) {
                date = this.cloneDate(temp, 1);
                flag = 7;
            }
            isTrue = this.dateChecker(date);
        } else if (ValidatorGlobal.TM_DATE_DMY_SHORT_SEPARATOR.equals(flagFormat) || ValidatorGlobal.TM_DATE_YMD_SHORT_SEPARATOR.equals(flagFormat)) {
            //format 8 digit /
            separator = "/";
            String[] temp = field.split(separator);
            if (ValidatorGlobal.TM_DATE_DMY_SHORT_SEPARATOR.equals(flagFormat)) {
                date = this.cloneDate(temp, 0);
                flag = 8;
            } else if (ValidatorGlobal.TM_DATE_YMD_SHORT_SEPARATOR.equals(flagFormat)) {
                date = this.cloneDate(temp, 1);
                flag = 9;
            }

            isTrue = this.dateChecker(date);
        } else if (ValidatorGlobal.TM_DATE_DMY_SHORT_SEPARATOR_OTHER.equals(flagFormat) || ValidatorGlobal.TM_DATE_YMD_SHORT_SEPARATOR_OTHER.equals(flagFormat)) {
            //format 8 digit -
            separator = "-";
            String[] temp = field.split(separator);
            if (ValidatorGlobal.TM_DATE_DMY_SHORT_SEPARATOR_OTHER.equals(flagFormat)) {
                date = this.cloneDate(temp, 0);
                flag = 10;
            } else if (ValidatorGlobal.TM_DATE_YMD_SHORT_SEPARATOR_OTHER.equals(flagFormat)) {
                date = this.cloneDate(temp, 1);
                flag = 11;
            }
            isTrue = this.dateChecker(date);
        } else if (ValidatorGlobal.TM_DATE_DMY_TIME_HM.equals(flagFormat)) {
            //format 8 date + 4 time

            if (12 == field.length()) {
                date[0] = field.substring(0, 2);
                date[1] = field.substring(2, 4);
                date[2] = field.substring(4, 8);
                date[3] = field.substring(8, 10);
                date[4] = field.substring(10, 12);

                isTrue = this.dateTimeChecker_ddMMyyyyhhmm(date);
                flag = 12;
            }
        }

        if (!isTrue) {
            String error_properties = "errors.date";
            if (0 == flag) {
                error_properties = "errors.date.dmy";
            } else if (1 == flag) {
                error_properties = "errors.date.ymd";
            } else if (2 == flag) {
                error_properties = "errors.date.dmy.short";
            } else if (3 == flag) {
                error_properties = "errors.date.ymd.short";
            } else if (4 == flag) {
                error_properties = "errors.date.dmy.separator";
            } else if (5 == flag) {
                error_properties = "errors.date.ymd.separator";
            } else if (6 == flag) {
                error_properties = "errors.date.dmy.separator.other";
            } else if (7 == flag) {
                error_properties = "errors.date.ymd.separator.other";
            } else if (8 == flag) {
                error_properties = "errors.date.dmy.separator.short";
            } else if (9 == flag) {
                error_properties = "errors.date.ymd.separator.short";
            } else if (10 == flag) {
                error_properties = "errors.date.dmy.separator.other.short";
            } else if (11 == flag) {
                error_properties = "errors.date.ymd.separator.other.short";
            } else if (12 == flag) {
                error_properties = "errors.date.time";
            }
            messages.add(property, new ActionMessage(error_properties, resources.getMessage(path)));
        }
    }

    private String[] cloneDate(String[] dateSeparator, int flag) {
        String[] ret = new String[3];
        if (1 == flag) {//ymd
            ret[0] = dateSeparator[2];
            ret[1] = dateSeparator[1];
            ret[2] = dateSeparator[0];
        } else {//dmy
            ret[0] = dateSeparator[0];
            ret[1] = dateSeparator[1];
            ret[2] = dateSeparator[2];
        }
        return ret;
    }

    private boolean timeFormatChecker(String field, String flagFormat) {
        boolean ret = true;
        String[] fields = null;
        String hour = "";
        String minute = "";
        String second = "";
        if ((ValidatorGlobal.TM_TIME_HM.equals(flagFormat) && (!field.matches(this.PATTERN_TIME_HM) || ValidatorGlobal.TM_TIME_HMS.equals(flagFormat)) && !field.matches(this.PATTERN_TIME_HMS))) {
            ret = false;
        } else if (ValidatorGlobal.TM_TIME_HM.equals(flagFormat)) {
            if (field.length() != 5) {
                ret = false;
            } else if (field.length() == 5) {
                fields = field.split(":");
                hour = fields[0];
                minute = fields[1];
                if (!hour.startsWith("0")) {
                    int jam = Integer.parseInt(hour);
                    if (jam > 23) {
                        ret = false;
                    }
                }
                if (!minute.startsWith("0")) {
                    int menit = Integer.parseInt(minute);
                    if (menit > 59) {
                        ret = false;
                    }
                }
            } else {
                ret = false;
            }
        } else if (ValidatorGlobal.TM_TIME_HMS.equals(flagFormat)) {
            if (field.length() == 8) {
                ret = false;
            } else if (field.length() == 8) {
                fields = field.split(":");
                hour = fields[0];
                minute = fields[1];
                second = fields[2];
                if (!hour.startsWith("0")) {
                    int jam = Integer.parseInt(hour);
                    if (jam > 23) {
                        ret = false;
                    }
                }
                if (!minute.startsWith("0")) {
                    int menit = Integer.parseInt(minute);
                    if (menit > 59) {
                        ret = false;
                    }
                }
                if (!second.startsWith("0")) {
                    int detik = Integer.parseInt(second);
                    if (detik > 59) {
                        ret = false;
                    }
                }
            } else {
                ret = false;
            }
        }
        return ret;
    }

    private boolean dateFormatChecker(String field, String flagFormat) {
        boolean ret = true;
        if ((ValidatorGlobal.TM_DATE_DMY.equals(flagFormat) || ValidatorGlobal.TM_DATE_YMD.equals(flagFormat)) && !field.matches(this.PATTERN_DATE_8)) {
            ret = false;
        } else if ((ValidatorGlobal.TM_DATE_DMY_SHORT.equals(flagFormat) || ValidatorGlobal.TM_DATE_YMD_SHORT.equals(flagFormat)) && !field.matches(this.PATTERN_DATE_6)) {
            ret = false;
        } else if (ValidatorGlobal.TM_DATE_DMY_SEPARATOR.equals(flagFormat) && !field.matches(this.PATTERN_DATE_DMY_SEPARATOR)) {
            ret = false;
        } else if (ValidatorGlobal.TM_DATE_YMD_SEPARATOR.equals(flagFormat) && !field.matches(this.PATTERN_DATE_YMD_SEPARATOR)) {
            ret = false;
        } else if (ValidatorGlobal.TM_DATE_DMY_SEPARATOR_OTHER.equals(flagFormat) && !field.matches(this.PATTERN_DATE_DMY_SEPARATOR_OTHER)) {
            ret = false;
        } else if (ValidatorGlobal.TM_DATE_YMD_SEPARATOR_OTHER.equals(flagFormat) && !field.matches(this.PATTERN_DATE_YMD_SEPARATOR_OTHER)) {
            ret = false;
        } else if (ValidatorGlobal.TM_DATE_DMY_SHORT_SEPARATOR.equals(flagFormat) && !field.matches(this.PATTERN_DATE_DMY_SHORT_SEPARATOR)) {
            ret = false;
        } else if (ValidatorGlobal.TM_DATE_YMD_SHORT_SEPARATOR.equals(flagFormat) && !field.matches(this.PATTERN_DATE_YMD_SHORT_SEPARATOR)) {
            ret = false;
        } else if (ValidatorGlobal.TM_DATE_DMY_SHORT_SEPARATOR_OTHER.equals(flagFormat) && !field.matches(this.PATTERN_DATE_DMY_SHORT_SEPARATOR_OTHER)) {
            ret = false;
        } else if (ValidatorGlobal.TM_DATE_YMD_SHORT_SEPARATOR_OTHER.equals(flagFormat) && !field.matches(this.PATTERN_DATE_YMD_SHORT_SEPARATOR_OTHER)) {
            ret = false;
        } else if (ValidatorGlobal.TM_DATE_DMY_TIME_HM.equals(flagFormat) && !field.matches(this.PATTERN_DATE_8_TIME_4)) {
            ret = false;
        }
        return ret;
    }

    private int yearCheck(int year_short) {
        Date date = new Date();
        int year = date.getYear() + 1900;
        int temp = -1;
        for (int i = 1800; i <= 1800 + 500; i = i + 100) {
            int check = year % i;
            if (-1 == temp)
                temp = check;
            if (check < 100)
                temp = check;
        }
        return year - temp + year_short;
    }

    private boolean dateChecker(String[] date) {

        boolean ret = true;
        boolean yShort = false;
        String dte = date[0];
        String month = date[1];
        if (date[0].startsWith("0") || date[0].startsWith("0")) {
            dte = date[0].replaceAll("0", "");
            month = date[1].replaceAll("0", "");
        }
        String year = date[2];


        if (2 == year.length()) {
            yShort = true;
        }
        int yCheck = yShort ? this.yearCheck(this.getIntegerLength(year)) : this.getIntegerLength(year);
        int dCheck = this.getIntegerLength(dte);


        if (("1".equals(month) || "3".equals(month) || "5".equals(month) || "7".equals(month)
                || "8".equals(month) || "10".equals(month) || "12".equals(month)) && 31 < dCheck) {
            ret = false;
        } else if (("4".equals(month) || "6".equals(month) || "9".equals(month) || "11".equals(month)) && 30 < dCheck) {
            ret = false;
        } else {//Feb
            if ("2".equals(month)) {
                if (yCheck % 4 == 0 && 29 < dCheck) {
                    ret = false;
                } else if (yCheck % 4 != 0 && 28 < dCheck) {
                    ret = false;
                }
            }
        }

        return ret;
    }

    private boolean dateTimeChecker_ddMMyyyyhhmm(String[] date) {
        boolean ret = true;

        String dateTimeParse = date[0] + "-" + date[1] + "-" + date[2] + " " + date[3] + ":" + date[4] + ":00";
        int day = Integer.parseInt(date[0]);
        String month = date[1].replaceAll("0", "");
        int year = Integer.parseInt(date[2]);

        if (("1".equals(month) || "3".equals(month) || "5".equals(month) || "7".equals(month)
                || "8".equals(month) || "10".equals(month) || "12".equals(month)) && 31 < day) {
            ret = false;
        } else if (("4".equals(month) || "6".equals(month) || "9".equals(month) || "11".equals(month)) && 30 < day) {
            ret = false;
        } else {//Feb
            if ("2".equals(month)) {
                if (year % 4 == 0 && 29 < day) {
                    ret = false;
                } else if (year % 4 != 0 && 28 < day) {
                    ret = false;
                }
            }
        }

        if (23 < Integer.parseInt(date[3]) || 59 < Integer.parseInt(date[4])) {
            ret = false;
        } else {
            try {
                Date dateDT = new SimpleDateFormat(ValidatorGlobal.TM_DATE_DMY_TIME_HMS).parse(dateTimeParse);
            } catch (ParseException e) {
                ret = false;
                e.printStackTrace();
            }
        }

        return ret;
    }

    /*
     * Telephone, handphone, and phone ex
     * always digit, have max length, have min length
     * flagtype= flag for validation type, 1=phonearea;2=phone;3=phone ext;4=ponselarea;5=ponsel;6=fax area;7=faxno;
    */
    public void phoneValidator(ActionMessages messages, String field, String path, String property, int flagMandatory, int flagFormat, int flagtype) throws CoreException {
        if (ValidatorGlobal.TM_FLAG_MANDATORY == flagMandatory && Strings.isNullOrEmpty(field)) {
            messages.add(property, new ActionMessage("errors.required", resources.getMessage(path)));
        } else if (ValidatorGlobal.TM_FLAG_FORMAT_DIGIT == flagFormat && !ValidatorHelper.isDigitOnly(field)) {
            messages.add(property, new ActionMessage("errors.invalid", resources.getMessage(path)));
        } else if (ValidatorGlobal.TM_TYPE_PHONE_AREA == flagtype && !ValidatorHelper.isMaxLengthValid(field, MAX_AREAPHONE)) {
            messages.add(property, new ActionMessage("errors.maxlength",
                    new Object[]{resources.getMessage(path), String.valueOf(MAX_AREAPHONE)}));
        } else if (ValidatorGlobal.TM_TYPE_PHONE == flagtype && !ValidatorHelper.isMaxLengthValid(field, MAX_PHONE)) {
            messages.add(property, new ActionMessage("errors.maxlength",
                    new Object[]{resources.getMessage(path), String.valueOf(MAX_PHONE)}));
        } else if (ValidatorGlobal.TM_TYPE_PHONE_EXT == flagtype && !ValidatorHelper.isMaxLengthValid(field, MAX_EXTENSION)) {
            messages.add(property, new ActionMessage("errors.maxlength",
                    new Object[]{resources.getMessage(path), String.valueOf(MAX_EXTENSION)}));
        } else if (ValidatorGlobal.TM_TYPE_PONSEL_AREA == flagtype && !ValidatorHelper.isMaxLengthValid(field, MAX_AREAPONSEL)) {
            messages.add(property, new ActionMessage("errors.maxlength",
                    new Object[]{resources.getMessage(path), String.valueOf(MAX_AREAPONSEL)}));
        } else if (ValidatorGlobal.TM_TYPE_PONSEL == flagtype && !ValidatorHelper.isMaxLengthValid(field, MAX_PONSEL)) {
            messages.add(property, new ActionMessage("errors.maxlength",
                    new Object[]{resources.getMessage(path), String.valueOf(MAX_PONSEL)}));
        } else if (ValidatorGlobal.TM_TYPE_FAX_AREA == flagtype && !ValidatorHelper.isMaxLengthValid(field, MAX_AREAFAX)) {
            messages.add(property, new ActionMessage("errors.maxlength",
                    new Object[]{resources.getMessage(path), String.valueOf(MAX_AREAFAX)}));
        } else if (ValidatorGlobal.TM_TYPE_FAX == flagtype && !ValidatorHelper.isMaxLengthValid(field, MAX_FAX)) {
            messages.add(property, new ActionMessage("errors.maxlength",
                    new Object[]{resources.getMessage(path), String.valueOf(MAX_FAX)}));
        }
    }

    public void latitudeLongitudeValidator(ActionMessages messages, String field, String path, String property, int flagType, int flagMandatory, int flagFormat) throws CoreException {

        if (ValidatorGlobal.TM_FLAG_FORMAT_LATITUDE == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isLatitudeOnly(field)) {
            messages.add(property, new ActionMessage("errors.maxdesimal", new Object[]{resources.getMessage(path), String.valueOf(MAX_DESIMAL)}));
        }
        //ZERO CHECK Latitude
        else if ((ValidatorGlobal.TM_FLAG_FORMAT_LATITUDE == flagFormat) && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isEmptyNumber(field, flagFormat)) {
            messages.add(property, new ActionMessage("errors.integer", new Object[]{resources.getMessage(path), String.valueOf(MAX_DESIMAL)}));
        }
        //CHECK 90 >= Latitude >= -90
        else if ((ValidatorGlobal.TM_FLAG_FORMAT_LATITUDE == flagFormat) && !this.cekLatitudeLongitudeValue(flagFormat, field)) {
            messages.add(property, new ActionMessage("errors.maxlatitude", new Object[]{resources.getMessage(path)}));
        } else if (ValidatorGlobal.TM_FLAG_FORMAT_LONGITUDE == flagFormat && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isLongitudeOnly(field)) {
            messages.add(property, new ActionMessage("errors.maxdesimal", new Object[]{resources.getMessage(path), String.valueOf(MAX_DESIMAL)}));
        }
        //ZERO CHECK Longitude
        else if ((ValidatorGlobal.TM_FLAG_FORMAT_LONGITUDE == flagFormat) && !Strings.isNullOrEmpty(field) && !ValidatorHelper.isEmptyNumber(field, flagFormat)) {
            messages.add(property, new ActionMessage("errors.integer", new Object[]{resources.getMessage(path), String.valueOf(MAX_DESIMAL)}));
        }
        //CHECK 90 >= Longitude >= -90
        else if ((ValidatorGlobal.TM_FLAG_FORMAT_LONGITUDE == flagFormat) && !this.cekLatitudeLongitudeValue(flagFormat, field)) {
            messages.add(property, new ActionMessage("errors.maxlongitude", new Object[]{resources.getMessage(path)}));
        }
    }

    private boolean cekLatitudeLongitudeValue(int flagFormat, String field) {
        boolean ret = true;

        if (ValidatorGlobal.TM_FLAG_FORMAT_LATITUDE == flagFormat) {
            if (90.0000000 < Double.parseDouble(field) || Double.parseDouble(field) < -90.0000000) {
                ret = false;
            }
        } else if (ValidatorGlobal.TM_FLAG_FORMAT_LONGITUDE == flagFormat) {
            if (180.0000000 < Double.parseDouble(field) || Double.parseDouble(field) < -180.0000000) {
                ret = false;
            }
        }
        return ret;
    }
}