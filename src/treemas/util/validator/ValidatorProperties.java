package treemas.util.validator;

import treemas.util.tool.ConfigurationProperties;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;

public class ValidatorProperties {
    private static final String SYSTEM_ENVIRONMENT = "validator.properties";
    //	-----------FOR USER ----------------
    public static int MAX_USER_ID;
    public static int MAX_USER_NAME;
    public static int MIN_PASSWORD;
    public static int MIN_CONFIRM_PASSWORD;
    //	-----------FOR GENERAL -------------
    public static int MAX_DESIMAL;
    public static int MAX_AREAPHONE;
    public static int MAX_PHONE;
    public static int MAX_AREAPONSEL;
    public static int MAX_PONSEL;
    public static int MAX_AREAFAX;
    public static int MAX_FAX;
    //	-----------END OF GENERAL ----------
    public static int MAX_EXTENSION;
    //  -----------FOR HANDSET -------------
    public static int MAX_HANDSET_IMEI;
    //	-----------END OF HANDSET ----------
    public static int MAX_HANDSET_CODE;
    private static ValidatorProperties theInstance;

    static {
        initFromSystem();
    }

    private Properties properties = new Properties();

    private ValidatorProperties() {
    }

    public static synchronized void initFromFile(String propFileName)
            throws Exception {
        if (theInstance != null) {
            return;
        }

        FileInputStream fis = new FileInputStream(propFileName);
        if (fis == null) {
            throw new IllegalStateException("Cannot load " + propFileName);
        }
        try {
            theInstance = new ValidatorProperties();
            theInstance.readProperties(fis);
            System.out.println("Read validation properties from: "
                    + propFileName);
        } finally {
            try {
                fis.close();
            } catch (Exception e) {
            }
        }
    }

    private static void initFromSystem() {
        // load from system -D environment
        String propFileName = System.getProperty(SYSTEM_ENVIRONMENT);
        if (propFileName != null) {
            try {
                initFromFile(propFileName);
            } catch (Exception e) {
                System.err.println("Error loading validation properties: "
                        + e.toString());
            }
        }
    }

    public static synchronized void initFromResource(String resourceName)
            throws Exception {
        if (theInstance != null) {
            return;
        }

        theInstance = new ValidatorProperties();
        InputStream in = ConfigurationProperties.class.getClassLoader()
                .getResourceAsStream(resourceName);
        if (in == null)
            throw new IllegalStateException("Cannot find " + resourceName);
        theInstance.readProperties(in);
        System.out.println("Read validation properties from " + resourceName);
    }

    public static void loadToClassStaticProps() {

        MAX_DESIMAL = theInstance.getInt("length.max.desimal");

        MAX_AREAPHONE = theInstance.getInt("length.max.areaphone");
        MAX_PHONE = theInstance.getInt("length.max.phone");
        MAX_AREAPONSEL = theInstance.getInt("length.max.areaponsel");
        MAX_PONSEL = theInstance.getInt("length.max.ponsel");
        MAX_AREAFAX = theInstance.getInt("length.max.areafax");
        MAX_FAX = theInstance.getInt("length.max.fax");
        MAX_EXTENSION = theInstance.getInt("length.max.ext");

		/*USER*/
        MAX_USER_ID = theInstance.getInt("length.max.user.id");
        MAX_USER_NAME = theInstance.getInt("length.max.user.name");
        MIN_PASSWORD = theInstance.getInt("length.min.user.password");
        MIN_CONFIRM_PASSWORD = theInstance.getInt("length.min.user.confirmpassword");

        MAX_HANDSET_IMEI = theInstance.getInt("length.max.handset.imei");
        MAX_HANDSET_CODE = theInstance.getInt("length.max.handset.code");
    }

    public String get(String key) {
        return (String) properties.get(key);
    }

    private synchronized void readProperties(InputStream in) throws Exception {
        try {
            properties.load(in);
        } finally {
            try {
                in.close();
            } catch (Exception e) {
            }
        }
    }

    public ValidatorProperties getInstance() {
        return theInstance;
    }

    private int getInt(String key) {
        String tmp = (String) properties.get(key);
        return Integer.parseInt(tmp.trim());
    }

    private int[] getIntArr(String key) {
        int[] arr = null;

        String tmp = (String) properties.get(key);
        StringTokenizer st = new StringTokenizer(tmp, ",");
        ArrayList list = new ArrayList(2);
        while (st.hasMoreTokens()) {
            list.add(st.nextToken().trim());
        }
        if (list.size() > 0) {
            arr = new int[list.size()];
            int i = 0;
            for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
                String str = (String) iterator.next();
                arr[i] = Integer.parseInt(str);
                i++;
            }
        }

        return arr;
    }
}
