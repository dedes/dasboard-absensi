package treemas.util.validator;

import org.apache.commons.validator.Field;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.ValidatorAction;
import org.apache.commons.validator.util.ValidatorUtils;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.validator.Resources;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ValidationUtil implements Serializable {
    private static SimpleDateFormat dateFormat = new SimpleDateFormat(
            "ddMMyyyy");

    public static boolean validateTwoDates(Object bean, ValidatorAction va,
                                           Field field, ActionMessages errors, HttpServletRequest request) {

        String value = null;
        String value2 = null;
        if (field.getProperty() != null && field.getProperty().length() > 0) {
            value = ValidatorUtils.getValueAsString(bean, field.getProperty());
            String date2 = field.getVarValue("secondDate");
            value2 = ValidatorUtils.getValueAsString(bean, date2);
        }

        long start = 0;
        long end = 0;
        if (!GenericValidator.isBlankOrNull(value)) {
            try {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(dateFormat.parse(value).getTime());
                start = cal.getTimeInMillis();
                cal.setTimeInMillis(dateFormat.parse(value2).getTime());
                end = cal.getTimeInMillis();
                if (start > end) {
                    errors.add(field.getKey(),
                            Resources.getActionMessage(request, va, field));
                    return false;
                }
            } catch (ParseException pe) {
                return false;
            } catch (Exception e) {
                errors.add(field.getKey(),
                        Resources.getActionMessage(request, va, field));
                return false;
            }
        }
        return true;
    }

}