package treemas.util.beanaction;

import treemas.util.format.TreemasFormatter;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import java.text.ParseException;

public class DateTimeConverter implements BeanConverter.FieldConverter {
    private static String VIEW_DATETIME_FORMAT = "dd-MMM-yyyy HH:mm:ss";
    private static ThreadLocal converters = new ThreadLocal();
    private ILogger logger = LogManager.getDefaultLogger();

    private DateTimeConverter() {
    }

    public static DateTimeConverter getInstance() {
        DateTimeConverter result = (DateTimeConverter) converters.get();
        if (result == null) {
            result = new DateTimeConverter();
            converters.set(result);
        }
        return result;
    }

    public String convertToString(String fieldName, Object source) {
        if (source == null)
            return null;
        String result = null;
        if (source instanceof java.util.Date) {
            result = TreemasFormatter.getDateFormat(VIEW_DATETIME_FORMAT).format(source);
        } else
            result = source.toString();
        return result;
    }


    public Object convertFromString(String fieldName, String source, Class targetClass) {
        if (source == null)
            return null;

        Object result = null;
        try {
            if (java.util.Date.class.equals(targetClass)) {
                result = TreemasFormatter.getDateFormat(VIEW_DATETIME_FORMAT).parse(source);
            } else
                throw new IllegalArgumentException("Target class " + targetClass.getName() +
                        " is not supported by DMYTimeConverter");
        } catch (ParseException pe) {
            this.logger.log(ILogger.LEVEL_ERROR,
                    "Error parsing " + source + " to class " +
                            targetClass.getName() + ": " + pe.toString());
        }
        return result;
    }
}
