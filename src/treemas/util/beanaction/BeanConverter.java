package treemas.util.beanaction;

import treemas.util.format.TreemasFormatter;
import treemas.util.format.TreemasNumberFormat;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;

public class BeanConverter {

    private static final Class[] PARAM_TYPE_STRING = new Class[]{java.lang.String.class};
    private static final int MAX_CACHE_SIZE = 100;
    private static ILogger logger = LogManager.getDefaultLogger();
    private static TreemasNumberFormat default_tostring_number_format = TreemasFormatter.NFORMAT_US_2;
    private static String default_tostring_date_format = TreemasFormatter.DFORMAT_DMY;
    private static TreemasNumberFormat default_fromstring_number_format = TreemasFormatter.NFORMAT_PLAIN;
    private static String default_fromstring_date_format = TreemasFormatter.DFORMAT_DMY;
    private static Map cacheMethods = Collections
            .synchronizedMap(new LinkedHashMap() {
                protected boolean removeEldestEntry(Map.Entry eldest) {
                    return size() > MAX_CACHE_SIZE;
                }
            });

    private BeanConverter() {
    }

    public static String getDefaultToStringDateFormat() {
        return default_tostring_date_format;
    }

    public static void setDefaultToStringDateFormat(String format) {
        default_tostring_date_format = format;
    }

    public static String getDefaultFromStringDateFormat() {
        return default_fromstring_date_format;
    }

    public static void setDefaultFromStringDateFormat(String format) {
        default_fromstring_date_format = format;
    }

    public static TreemasNumberFormat getDefaultToStringNumberFormat() {
        return default_tostring_number_format;
    }

    public static void setDefaultToStringNumberFormat(TreemasNumberFormat formatType) {
        default_tostring_number_format = formatType;
    }

    public static TreemasNumberFormat getDefaultFromStringNumberFormat() {
        return default_fromstring_number_format;
    }

    public static void setDefaultFromStringNumberFormat(TreemasNumberFormat formatType) {
        default_fromstring_number_format = formatType;
    }

    public static void convertBeanToString(Object sourceBean,
                                           Object targetBean, Map fieldConverters) {

        if (sourceBean == null)
            throw new IllegalArgumentException(
                    "Cannot convert bean: source bean is null");
        if (targetBean == null)
            throw new IllegalArgumentException(
                    "Cannot convert bean: target bean is null");
        try {
            Class fromClass = sourceBean.getClass();
            Class toClass = targetBean.getClass();
            Method[] fromMethods = (Method[]) cacheMethods.get(fromClass
                    .getName());
            if (fromMethods == null) {
                fromMethods = fromClass.getMethods();
                cacheMethods.put(fromClass.getName(), fromMethods);
            }

            for (int i = 0; i < fromMethods.length; i++) {
                Method fromMethod = fromMethods[i];
                if (!fromMethod.getName().startsWith("get")
                        || fromMethod.getParameterTypes().length > 0)
                    continue;
                String tmpName = fromMethod.getName().substring(3);

                Method toMethod = null;
                try {
                    toMethod = toClass.getMethod("set" + tmpName, PARAM_TYPE_STRING);
                } catch (NoSuchMethodException e) {
                }

                if (toMethod == null) continue;

                Object source = fromMethod.invoke(sourceBean, null);
                Object result = null;

                FieldConverter converter = null;
                if (fieldConverters != null) {
                    String fieldName = Character.toLowerCase(tmpName.charAt(0))
                            + tmpName.substring(1);
                    converter = (FieldConverter) fieldConverters.get(fieldName);
                    if (converter != null) result = converter.convertToString(fieldName, source);
                    else result = convertToString(source);
                } else
                    result = convertToString(source);

                toMethod.invoke(targetBean, new Object[]{result});
            }
        } catch (Exception e) {
            logger.log(ILogger.LEVEL_ERROR, "Exception when converting bean "
                    + sourceBean.getClass().getName() + " to "
                    + targetBean.getClass().getName());
            logger.printStackTrace(e);
        }
    }

    public static void convertBeanToString(Object sourceBean, Object targetBean) {
        convertBeanToString(sourceBean, targetBean, null);
    }

    public static String convertToString(Object source) {
        if (source == null)
            return null;

        String result = null;
        if (source instanceof java.lang.Double) {
            NumberFormat formatter = TreemasFormatter.getNumberFormat(getDefaultToStringNumberFormat());
            result = formatter.format(((Double) source).doubleValue());
        } else if (source instanceof java.lang.Float) {
            NumberFormat formatter = TreemasFormatter.getNumberFormat(getDefaultToStringNumberFormat());
            result = formatter.format(((Float) source).doubleValue());
        } else if (source instanceof java.math.BigDecimal) {
            NumberFormat formatter = TreemasFormatter.getNumberFormat(getDefaultToStringNumberFormat());
            result = formatter.format(((java.math.BigDecimal) source)
                    .doubleValue());
        } else if (source instanceof java.util.Date) {
            result = TreemasFormatter.formatDate((java.util.Date) source,
                    getDefaultToStringDateFormat());
        } else
            result = source.toString();
        return result;
    }

    public static void convertBeanFromString(Object sourceBean,
                                             Object targetBean, Map fieldConverters) {
        try {
            Class fromClass = sourceBean.getClass();
            Class toClass = targetBean.getClass();
            Method[] toMethods = (Method[]) cacheMethods.get(toClass.getName());
            if (toMethods == null) {
                toMethods = toClass.getMethods();
                cacheMethods.put(toClass.getName(), toMethods);
            }

            for (int i = 0; i < toMethods.length; i++) {
                Method toMethod = toMethods[i];
                if (!toMethod.getName().startsWith("set")
                        || toMethod.getParameterTypes().length != 1
                        || !toMethod.getReturnType().equals(Void.TYPE))
                    continue;
                String tmpName = toMethod.getName().substring(3);

                Method fromMethod = null;
                try {
                    fromMethod = fromClass.getMethod("get" + tmpName, null);
                } catch (NoSuchMethodException e) {
                }

                if (fromMethod == null
                        || !fromMethod.getReturnType().equals(
                        java.lang.String.class))
                    continue;

                String source = (String) fromMethod.invoke(sourceBean, null);
                Object result = null;

                Class toMethodParamClass = toMethod.getParameterTypes()[0];

                FieldConverter converter = null;

                if (fieldConverters != null) {
                    String fieldName = Character.toLowerCase(tmpName.charAt(0))
                            + tmpName.substring(1);
                    converter = (FieldConverter) fieldConverters.get(fieldName);
                    if (converter != null)
                        result = converter.convertFromString(fieldName, source,
                                toMethodParamClass);
                    else
                        result = convertFromString(source, toMethodParamClass);
                } else
                    result = convertFromString(source, toMethodParamClass);

                if (result == null && toMethodParamClass.isPrimitive()) {
                    result = new Integer(0);
                }
                toMethod.invoke(targetBean, new Object[]{result});
            }
        } catch (Exception e) {
            logger.printStackTrace(e);
        }
    }

    public static void convertBeanFromString(Object sourceBean,
                                             Object targetBean) {
        convertBeanFromString(sourceBean, targetBean, null);
    }

    public static Object convertFromString(String source, Class targetClass) {
        if (source == null)
            return null;

        Object result = null;
        try {
            if (targetClass.equals(java.lang.String.class)) {
                result = source;
            } else if (targetClass.equals(Integer.TYPE)
                    || targetClass.equals(java.lang.Integer.class)) {
                result = Integer.valueOf(source);
            } else if (targetClass.equals(Double.TYPE)
                    || targetClass.equals(java.lang.Double.class)
                    || targetClass.equals(Float.TYPE)
                    || targetClass.equals(java.lang.Float.class)) {
                NumberFormat formatter = TreemasFormatter.getNumberFormat(getDefaultFromStringNumberFormat());
                result = formatter.parse(source);
                if (targetClass.equals(Double.TYPE) || targetClass.equals(java.lang.Double.class)) {
                    result = new Double(String.valueOf(result));
                } else if (targetClass.equals(Float.TYPE) || targetClass.equals(java.lang.Float.class)) {
                    result = new Float(String.valueOf(result));
                }
            } else if (targetClass.equals(Long.TYPE)
                    || targetClass.equals(java.lang.Long.class)) {
                result = Long.valueOf(source);
            } else if (targetClass.equals(java.util.Date.class)) {
                result = TreemasFormatter.parseDate(source,
                        getDefaultFromStringDateFormat());
            } else if (targetClass.equals(java.math.BigDecimal.class)) {
                result = new java.math.BigDecimal(source);
            } else
                logger.log(ILogger.LEVEL_WARNING, "Unsupported class: "
                        + targetClass.getName());
        } catch (ParseException pe) {
            logger.log(ILogger.LEVEL_ERROR, "Error parsing " + source + " to "
                    + targetClass.getName() + ": " + pe.toString());
        }
        return result;
    }

    public static List convertBeansToString(List list, Class targetClass,
                                            Map fieldConverters) {
        List result = new ArrayList(list.size());
        Iterator iter = list.iterator();
        try {
            while (iter.hasNext()) {
                Object target = targetClass.newInstance();
                convertBeanToString(iter.next(), target, fieldConverters);
                result.add(target);
            }
        } catch (Exception e) {
            logger.printStackTrace(e);
        }
        return result;
    }

    public static List convertBeansFromString(List list, Class targetClass) {
        return convertBeansFromString(list, targetClass, null);
    }

    public static List convertBeansFromString(List list, Class targetClass,
                                              Map fieldConverters) {
        List result = new ArrayList(list.size());
        Iterator iter = list.iterator();
        try {
            while (iter.hasNext()) {
                Object target = targetClass.newInstance();
                convertBeanFromString(iter.next(), target, fieldConverters);
                result.add(target);
            }
        } catch (Exception e) {
            logger.printStackTrace(e);
        }
        return result;
    }

    public static List convertBeansToString(List list, Class targetClass) {
        return convertBeansToString(list, targetClass, null);

    }

    public static Map convertBeanToStringMap(Object sourceBean,
                                             Map fieldConverters) {
        if (sourceBean == null)
            throw new IllegalArgumentException("Cannot convert bean: source bean is null");
        Map result = new HashMap();
        try {
            Class fromClass = sourceBean.getClass();
            Method[] fromMethods = (Method[]) cacheMethods.get(fromClass
                    .getName());
            if (fromMethods == null) {
                fromMethods = fromClass.getMethods();
                cacheMethods.put(fromClass.getName(), fromMethods);
            }
            for (int i = 0; i < fromMethods.length; i++) {
                Method fromMethod = fromMethods[i];
                if (!fromMethod.getName().startsWith("get")
                        || fromMethod.getParameterTypes().length > 0)
                    continue;
                String tmpName = fromMethod.getName().substring(3);

                Object source = fromMethod.invoke(sourceBean, null);
                String resultText = null;

                FieldConverter converter = null;
                String fieldName = Character.toLowerCase(tmpName.charAt(0))
                        + tmpName.substring(1);
                if (fieldConverters != null) {
                    converter = (FieldConverter) fieldConverters.get(fieldName);
                    if (converter != null) resultText = converter.convertToString(fieldName, source);
                    else resultText = convertToString(source);
                } else
                    resultText = convertToString(source);

                result.put(fieldName, resultText);
            }
        } catch (Exception e) {
            logger.log(ILogger.LEVEL_ERROR, "Exception when converting bean "
                    + sourceBean.getClass().getName() + " to Map");
            logger.printStackTrace(e);
        }
        return result;
    }

    public static void convertBeanFromStringMap(Map sourceMap,
                                                Object targetBean, Map fieldConverters) {
        try {
            Class toClass = targetBean.getClass();
            Method[] toMethods = (Method[]) cacheMethods.get(toClass.getName());
            if (toMethods == null) {
                toMethods = toClass.getMethods();
                cacheMethods.put(toClass.getName(), toMethods);
            }

            for (int i = 0; i < toMethods.length; i++) {
                Method toMethod = toMethods[i];
                if (!toMethod.getName().startsWith("set")
                        || toMethod.getParameterTypes().length != 1
                        || !toMethod.getReturnType().equals(Void.TYPE))
                    continue;
                String tmpName = toMethod.getName().substring(3);
                String fieldName = Character.toLowerCase(tmpName.charAt(0))
                        + tmpName.substring(1);

                Object objSource = sourceMap.get(fieldName);
                String source = objSource == null ? null : objSource.toString();
                Object result = null;

                Class toMethodParamClass = toMethod.getParameterTypes()[0];

                FieldConverter converter = null;
                if (fieldConverters != null) {
                    converter = (FieldConverter) fieldConverters.get(fieldName);
                    if (converter != null)
                        result = converter.convertFromString(fieldName, source,
                                toMethodParamClass);
                    else
                        result = convertFromString(source, toMethodParamClass);
                } else
                    result = convertFromString(source, toMethodParamClass);

                if (result == null && toMethodParamClass.isPrimitive()) {
                    result = new Integer(0);
                }
                toMethod.invoke(targetBean, new Object[]{result});
            }
        } catch (Exception e) {
            logger.printStackTrace(e);
        }
    }

    public static List convertBeansToStringMap(List list, Map fieldConverters) {
        List result = new ArrayList(list.size());
        Iterator iter = list.iterator();
        try {
            while (iter.hasNext()) {
                Map map = convertBeanToStringMap(iter.next(), fieldConverters);
                result.add(map);
            }
        } catch (Exception e) {
            logger.printStackTrace(e);
        }
        return result;
    }

    public static List convertBeansFromStringMap(List list, Class targetClass,
                                                 Map fieldConverters) {
        List result = new ArrayList(list.size());
        Iterator iter = list.iterator();
        try {
            while (iter.hasNext()) {
                Object target = targetClass.newInstance();
                convertBeanFromStringMap((Map) iter.next(), target,
                        fieldConverters);
                result.add(target);
            }
        } catch (Exception e) {
            logger.printStackTrace(e);
        }
        return result;
    }

    public static interface FieldConverter {

        public String convertToString(String fieldName, Object source);

        public Object convertFromString(String fieldName, String source,
                                        Class targetClass);
    }
}
