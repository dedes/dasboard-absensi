package treemas.util.ibatis;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

public class EODIbatisHelper {
    private static EODIbatisHelper theInstance = new EODIbatisHelper();

    private SqlMapClient sqlMap;

    private EODIbatisHelper() {
        try {
            java.io.Reader reader = Resources.getResourceAsReader("eod-ibatis-cfg.xml");

            this.sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot build iBatis SqlMap: " + e.toString(), e);
        }
    }

    public static SqlMapClient getSqlMap() {
        return theInstance.sqlMap;
    }
}
