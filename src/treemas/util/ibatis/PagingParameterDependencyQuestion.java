package treemas.util.ibatis;

import treemas.util.constant.Global;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class PagingParameterDependencyQuestion implements java.io.Serializable {
    private int pageSize;
    private int page = 1;
    private int start;
    private int end;

    /**
     * Create ParamInterval with a specified pageSize (data per page)
     *
     * @param pageSize int
     */
    public PagingParameterDependencyQuestion(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Create ParamInterval with default pageSize (data per page)
     */
    public PagingParameterDependencyQuestion() {
        this(Global.ROW_PER_PAGE_DEPENDENCY_QUESTION);
    }

    /**
     * Gets page number in this ParamInterval
     *
     * @return int
     */
    public int getPage() {
        return this.page;
    }

    /**
     * Sets a desire page number. 'start' and 'end' fields will be calculated automatically
     *
     * @param page int
     */
    public void setPage(int page) {
        this.start = (page - 1) * pageSize + 1;
        this.end = this.start + pageSize - 1;
        this.page = page;
    }

    /**
     * Gets ending row number
     *
     * @return int
     */
    public int getEnd() {
        return end;
    }

    /**
     * Gets starting row number
     *
     * @return int
     */
    public int getStart() {
        return start;
    }


    public Map toMap() {
        Map result = new HashMap();
        Method[] methods = this.getClass().getMethods();
        for (int i = 0; i < methods.length; i++) {
            String methodName = methods[i].getName();
            if (!methodName.startsWith("get"))
                continue;
            String propName = methodName.substring(3);
            propName = Character.toLowerCase(propName.charAt(0)) + propName.substring(1);
            try {
                Object obj = methods[i].invoke(this, null);

                result.put(propName, obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }


}
