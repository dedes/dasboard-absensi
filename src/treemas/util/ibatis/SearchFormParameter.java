package treemas.util.ibatis;

import com.google.common.base.Strings;

public class SearchFormParameter extends SearchParameter {
    private String sortBy;
    private String sortType;
    private String active;
    private int searchType2;
    private String searchValue2;

    public SearchFormParameter() {
    }


    public SearchFormParameter(int pageSize) {
        super(pageSize);
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        if (Strings.isNullOrEmpty(sortBy)) {
            sortBy = "10";
        }
        this.sortBy = sortBy;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        if (Strings.isNullOrEmpty(sortType)) {
            sortType = "10";
        }
        this.sortType = sortType;
    }

    public String getSearchValue2() {
        return searchValue2;
    }

    public void setSearchValue2(String searchValue2) {
        this.searchValue2 = searchValue2;
    }

    public int getSearchType2() {
        return searchType2;
    }

    public void setSearchType2(int searchType2) {
        this.searchType2 = searchType2;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

}
