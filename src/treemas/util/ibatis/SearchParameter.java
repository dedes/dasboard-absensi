package treemas.util.ibatis;

public class SearchParameter extends PagingParameter {
    private int searchType1;
    private String searchValue1;
    private String sortDesc;

    public SearchParameter() {
        super();
    }

    public SearchParameter(int pageSize) {
        super(pageSize);
    }

    public int getSearchType1() {
        return searchType1;
    }

    public void setSearchType1(int searchType1) {
        this.searchType1 = searchType1;
    }

    public String getSearchValue1() {
        return searchValue1;
    }

    public void setSearchValue1(String searchValue1) {
        this.searchValue1 = searchValue1;
    }

    public String getSortDesc() {
        return sortDesc;
    }

    public void setSortDesc(String sortDesc) {
        this.sortDesc = sortDesc;
    }

}
