package treemas.util.ibatis;


import com.ibatis.sqlmap.client.SqlMapClient;
import treemas.util.ejb.db.ICloneable;
import treemas.util.ejb.db.IDataAccess;
import treemas.util.paging.PageListWrapper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class IbatisDataAccessImpl implements IDataAccess, ICloneable {
    private SqlMapClient sqlMap = null;

    public IbatisDataAccessImpl() {
        this.sqlMap = IbatisHelper.getSqlMap();
    }

    public List queryList(String queryId, Object parameter) throws SQLException {
        return this.sqlMap.queryForList(queryId, parameter);
    }

    public PageListWrapper queryListForPage(String queryId, String queryIdForCount, Object parameter) throws
            SQLException {
        PageListWrapper result = new PageListWrapper();
        List list = IbatisHelper.getSqlMap().queryForList(queryId, parameter);
        result.setResultList(list);
        Integer tmp = (Integer) this.sqlMap.queryForObject(queryIdForCount, parameter);
        int total = 0;
        if (tmp != null) {
            total = tmp.intValue();
        }
        result.setTotalRecord(total);
        return result;
    }

    public Object querySingleObject(String queryId, Object parameter) throws SQLException {
        return this.sqlMap.queryForObject(queryId, parameter);
    }

    public int update(String queryId, Object parameter) throws SQLException {
        return this.sqlMap.update(queryId, parameter);
    }

    public int[] update(String[] queryIds, Object[] parameters) throws SQLException {
        int[] result = new int[queryIds.length];
        boolean ok = false;
        this.startTransaction();
        try {
            this.sqlMap.startBatch();
            for (int i = 0; i < result.length; i++) {
                result[i] = this.sqlMap.update(queryIds[i], parameters[i]);
            }
            this.sqlMap.executeBatch();
            ok = true;
        } finally {
            if (ok) {
                this.commitTransaction();
            } else {
                this.rollbackTransaction();
            }
        }
        return result;
    }

    public Connection getCurrentConnection() throws SQLException {
        return this.sqlMap.getCurrentConnection();
    }

    public void startTransaction() throws SQLException {
        this.sqlMap.startTransaction();
    }


    public void commitTransaction() throws SQLException {
        try {
            this.sqlMap.commitTransaction();
        } finally {
            this.sqlMap.endTransaction();
        }
    }

    public void rollbackTransaction() throws SQLException {
        this.sqlMap.endTransaction();
    }

    public Object clone() {
        Object result = null;
        try {
            result = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return result;
    }


}
