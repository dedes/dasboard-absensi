package treemas.util.ibatis;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import treemas.util.constant.PropertiesKey;
import treemas.util.tool.ConfigurationProperties;

import java.sql.SQLException;
import java.util.List;

public class IbatisHelper {

    private static IbatisHelper instance = new IbatisHelper();

    private SqlMapClient sqlMap;

    private IbatisHelper() {
        try {
            ConfigurationProperties appProps = ConfigurationProperties.getInstance();
            String configName = appProps.get(PropertiesKey.IBATIS_CONFIG_NAME);
            if (configName == null) {
                configName = "ibatis-cfg.xml";
            }
            System.out.println("Building ibatis sqlmap from " + configName);
            java.io.Reader reader = Resources.getResourceAsReader(configName);
            this.sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
            System.out.println("Building ibatis sqlmap from " + configName + ". Finished!");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot build iBatis SqlMap: " + e.toString(), e);
        }
    }

    public static SqlMapClient getSqlMap() {
        return instance.sqlMap;
    }

    public static Object getRecord(String id, Object param) throws SQLException {
        return instance.sqlMap.queryForObject(id, param);
    }

    public static List list(String id, Object param) throws SQLException {
        return instance.sqlMap.queryForList(id, param);
    }

    public static void insert(String id, Object data) throws SQLException {
        instance.sqlMap.insert(id, data);
    }

    public static void update(String id, Object data) throws SQLException {
        instance.sqlMap.update(id, data);
    }


}
