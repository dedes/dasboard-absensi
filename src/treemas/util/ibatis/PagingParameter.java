package treemas.util.ibatis;

import treemas.util.constant.Global;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class PagingParameter implements java.io.Serializable {
    private int pageSize;
    private int page = 1;
    private int start;
    private int end;

    public PagingParameter(int pageSize) {
        this.pageSize = pageSize;
    }

    public PagingParameter() {
        this(Global.ROW_PER_PAGE);
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.start = (page - 1) * pageSize + 1;
        this.end = this.start + pageSize - 1;
        this.page = page;
    }

    public int getEnd() {
        return end;
    }

    public int getStart() {
        return start;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public Map toMap() {
        Map result = new HashMap();
        Method[] methods = this.getClass().getMethods();
        for (int i = 0; i < methods.length; i++) {
            String methodName = methods[i].getName();
            if (!methodName.startsWith("get"))
                continue;
            String propName = methodName.substring(3);
            propName = Character.toLowerCase(propName.charAt(0))
                    + propName.substring(1);
            try {
                Object obj = methods[i].invoke(this, null);

                result.put(propName, obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
