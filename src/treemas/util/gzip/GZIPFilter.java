package treemas.util.gzip;

import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class GZIPFilter implements Filter {

    private final static String EXCLUDE_PATTERN = "excludePattern";
    private String[] excludePatternURL = null;
    private ILogger logger = LogManager.getDefaultLogger();
    private boolean verbose = false;

    public void init(FilterConfig filterConfig) throws ServletException {
        String temp = filterConfig.getInitParameter(EXCLUDE_PATTERN);
        if (temp != null) {
            StringTokenizer token = new StringTokenizer(temp, ",");
            List listExcludePattern = new ArrayList(5);
            while (token.hasMoreTokens()) {
                listExcludePattern.add((String) token.nextToken());
            }
            if (listExcludePattern.size() > 0) {
                this.excludePatternURL = new String[listExcludePattern.size()];
                listExcludePattern.toArray(excludePatternURL);
            }
        }
        this.verbose = (this.logger.getLogLevel() == ILogger.LEVEL_DEBUG_LOW);
        this.logger.log(ILogger.LEVEL_INFO, "GZIP filter initialized! Exclude="
                + temp + ". Verbose=" + this.verbose);
    }

    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        if (req instanceof HttpServletRequest) {
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse response = (HttpServletResponse) res;
            String ae = request.getHeader("accept-encoding");
            String reqUri = request.getRequestURI();
            if (!isExcludePattern(reqUri)) {
                if (ae != null && ae.indexOf("gzip") != -1) {
                    if (this.verbose) {
                        this.logger.log(ILogger.LEVEL_DEBUG_LOW,
                                "GZIP supported: " + reqUri);
                    }
                    GZIPResponseWrapper wrappedResponse = new GZIPResponseWrapper(
                            response);
                    chain.doFilter(req, wrappedResponse);
                    wrappedResponse.finishResponse();
                    return;
                }
            }
            chain.doFilter(req, res);
        }
    }

    public void destroy() {
    }

    private boolean isExcludePattern(String url) {
        boolean found = false;
        if (this.excludePatternURL != null) {
            for (int i = 0; i < excludePatternURL.length; i++) {
                found = (url.indexOf(this.excludePatternURL[i]) != -1);
                if (found)
                    break;
            }
        }
        return found;
    }

}
