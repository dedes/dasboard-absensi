package treemas.util.paging;

import java.util.List;

public class PageListWrapper implements java.io.Serializable {

    private int totalRecord;
    private List resultList;

    public PageListWrapper(List result, int totRecord) {
        this.totalRecord = totRecord;
        this.resultList = result;
    }

    public PageListWrapper() {
        this(null, 0);
    }

    public List getResultList() {
        return resultList;
    }

    public void setResultList(List resultList) {
        this.resultList = resultList;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

}
