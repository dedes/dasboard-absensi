package treemas.util.struts;

import org.apache.struts.util.MessageResources;

import java.util.HashMap;
import java.util.Locale;


public class ApplicationResources {
    public static final String DEFAULT_RESOURCES_KEY = "ApplicationResources_in";
    public static final String DEFAULT_RESOURCES_KEY_ENG = "ApplicationResources";

    private static HashMap instances = new HashMap();
    private MessageResources resources;

    private ApplicationResources(String resourceKey) {
        this.resources = MessageResources.getMessageResources(resourceKey);
    }

    public static synchronized ApplicationResources getInstance() {

        return getInstance(DEFAULT_RESOURCES_KEY);
    }

    public static synchronized ApplicationResources getInstanceInd() {
        return getInstance(DEFAULT_RESOURCES_KEY);
    }

    public static synchronized ApplicationResources getInstanceEng() {
        return getInstance(DEFAULT_RESOURCES_KEY_ENG);
    }

    public static synchronized ApplicationResources getInstance(String resourceKey) {
        ApplicationResources theInstance = (ApplicationResources) instances.get(resourceKey);
        if (theInstance == null) {
            theInstance = new ApplicationResources(resourceKey);
            instances.put(resourceKey, theInstance);
        }
        return theInstance;
    }

    public String getMessage(String key) {
        return this.resources.getMessage(key);
    }

    public String getMessage(String key, Object[] params) {
        return this.resources.getMessage(key, params);
    }

    public String getMessage(String key, Object param) {
        return this.resources.getMessage(key, param);
    }

    public String getMessage(Locale locale, String key) {
        return this.resources.getMessage(locale, key);
    }
}
