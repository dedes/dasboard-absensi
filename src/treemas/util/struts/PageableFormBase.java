package treemas.util.struts;

public abstract class PageableFormBase extends ValidatorFormBase {
    private PagingInfo paging;

    public PageableFormBase(int pageSize) {
        super();
        this.paging = new PagingInfo(pageSize);
    }

    public PagingInfo getPaging() {
        return paging;
    }

    public void setPaging(PagingInfo paging) {
        this.paging = paging;
    }

}
