package treemas.util.struts;

import org.apache.struts.action.*;
import treemas.util.AppException;
import treemas.util.beanaction.BeanConverter;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public abstract class ActionBase extends Action {
    protected ILogger logger = LogManager.getDefaultLogger();

    protected boolean useRedirect = true;

    public ActionBase() {
    }

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ActionForward result = null;
        result = beforeAction(mapping, form, request, response);
        if (result != null) {
            return result;
        }

        try {
            result = doAction(mapping, form, request, response);
        } catch (AppException e) {
            result = handleException(mapping, form, request, response, e);
        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public ActionForward beforeAction(ActionMapping mapping, ActionForm form,
                                      HttpServletRequest request,
                                      HttpServletResponse response) {
        return null;
    }

    public ActionForward doBack(HttpServletRequest request) {
        ActionForward result = null;
        String prevUri = request.getParameter("previousUri");
        if (prevUri != null && !prevUri.equals("")) {
            String ctxPath = request.getContextPath();
            if (prevUri.startsWith(ctxPath)) {
                prevUri = prevUri.substring(ctxPath.length());
            }
            result = new ActionForward(prevUri, useRedirect);
        } else {
            this.logger.log(ILogger.LEVEL_ERROR,
                    "Cannot find (or empty) 'previousUri' in request scope");
        }
        return result;
    }

    public abstract ActionForward handleException(ActionMapping mapping, ActionForm form,
                                                  HttpServletRequest request,
                                                  HttpServletResponse response,
                                                  AppException ex);

    public abstract ActionForward doAction(ActionMapping mapping,
                                           ActionForm form,
                                           HttpServletRequest request,
                                           HttpServletResponse response) throws
            AppException;

    protected ActionMessages setMessage(HttpServletRequest request,
                                        String msgKey, Object[] params) {
        return this.addMessage(null, request, msgKey, params);
    }

    protected ActionMessages addMessage(ActionMessages messages,
                                        HttpServletRequest request,
                                        String msgKey, Object[] params) {
        if (messages == null) {
            messages = new ActionMessages();
        }
        if (params == null) {
            messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(msgKey));
        } else {
            messages.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage(msgKey, params));
        }
        this.saveErrors(request, messages);
        return messages;
    }


    protected void setListToRequest(String attributeName, Class cls, HttpServletRequest request, List list) {
        request.removeAttribute(attributeName);
        request.setAttribute(attributeName, BeanConverter.convertBeansToString(list, cls));
    }


}
