package treemas.util.struts;

public class MultipleParameterBean implements java.io.Serializable {

    private String valueUseKey = "1";
    private String tableName;
    private String fieldKey;
    private String fieldLabel;
    private String fieldactive;
    private String conditional1;
    private String conditional2;
    private String conditional3;
    private String conditional4;
    private String conditional5;
    private String fieldConditional1;
    private String fieldConditional2;
    private String fieldConditional3;
    private String fieldConditional4;
    private String fieldConditional5;
    private String active;
    private String user;

    public MultipleParameterBean() {
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFieldKey() {
        return fieldKey;
    }

    public void setFieldKey(String fieldKey) {
        this.fieldKey = fieldKey;
    }

    public String getFieldLabel() {
        return fieldLabel;
    }

    public void setFieldLabel(String fieldLabel) {
        this.fieldLabel = fieldLabel;
    }

    public String getFieldConditional1() {
        return fieldConditional1;
    }

    public void setFieldConditional1(String fieldConditional1) {
        this.fieldConditional1 = fieldConditional1;
    }

    public String getFieldConditional2() {
        return fieldConditional2;
    }

    public void setFieldConditional2(String fieldConditional2) {
        this.fieldConditional2 = fieldConditional2;
    }

    public String getFieldConditional3() {
        return fieldConditional3;
    }

    public void setFieldConditional3(String fieldConditional3) {
        this.fieldConditional3 = fieldConditional3;
    }

    public String getFieldConditional4() {
        return fieldConditional4;
    }

    public void setFieldConditional4(String fieldConditional4) {
        this.fieldConditional4 = fieldConditional4;
    }

    public String getFieldConditional5() {
        return fieldConditional5;
    }

    public void setFieldConditional5(String fieldConditional5) {
        this.fieldConditional5 = fieldConditional5;
    }


    public String getConditional1() {
        return conditional1;
    }

    public void setConditional1(String conditional1) {
        this.conditional1 = conditional1;
    }

    public String getConditional2() {
        return conditional2;
    }

    public void setConditional2(String conditional2) {
        this.conditional2 = conditional2;
    }

    public String getConditional3() {
        return conditional3;
    }

    public void setConditional3(String conditional3) {
        this.conditional3 = conditional3;
    }

    public String getConditional4() {
        return conditional4;
    }

    public void setConditional4(String conditional4) {
        this.conditional4 = conditional4;
    }

    public String getConditional5() {
        return conditional5;
    }

    public void setConditional5(String conditional5) {
        this.conditional5 = conditional5;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getFieldactive() {
        return fieldactive;
    }

    public void setFieldactive(String fieldactive) {
        this.fieldactive = fieldactive;
    }

    public String getValueUseKey() {
        return valueUseKey;
    }

    public void setValueUseKey(String valueUseKey) {
        this.valueUseKey = valueUseKey;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
