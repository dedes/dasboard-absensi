package treemas.util.struts;

import java.io.Serializable;

public class PagingInfo implements Cloneable, Serializable {
    public static final String DISPATCH_NEXT = "Next";
    public static final String DISPATCH_PREVIOUS = "Prev";
    public static final String DISPATCH_FIRST = "First";
    public static final String DISPATCH_LAST = "Last";
    public static final String DISPATCH_GO = "Go";

    private int currentPageNo;
    private int targetPageNo;
    private int totalRecord;
    private int totalPage;
    private int firstRecord;
    private int lastRecord;
    private String dispatch;

    private int rowPerPage;

    public PagingInfo(int pageSize) {
        this.rowPerPage = pageSize;
    }

    public int getRowPerPage() {
        return rowPerPage;
    }

    public void setRowPerPage(int rowPerPage) {
        this.rowPerPage = rowPerPage;
    }

    public int getTargetPageNo() {
        return targetPageNo;
    }

    public int getCurrentPageNo() {
        return currentPageNo;
    }

    public void setCurrentPageNo(int currentPageNo) {
        this.currentPageNo = currentPageNo;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public String getDispatch() {
        return dispatch;
    }

    public void setDispatch(String dispatch) {
        this.dispatch = dispatch;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
        this.totalPage = (int) Math.ceil((double) totalRecord / this.rowPerPage);
    }

    public int getFirstRecord() {
        return firstRecord;
    }

    public void setFirstRecord(int firstRecord) {
        this.firstRecord = firstRecord;
    }

    public int getLastRecord() {
        return lastRecord;
    }

    public void setLastRecord(int lastRecord) {
        this.lastRecord = lastRecord;
    }

    public void calculationPage() {
//		this.firstRecord = (this.targetPageNo * this.rowPerPage)+1; 
        int curPage = this.currentPageNo;
        if (DISPATCH_NEXT.equals(dispatch)) {
            curPage++;
            this.targetPageNo = (curPage > this.totalPage) ? this.totalPage : curPage;
        } else if (DISPATCH_PREVIOUS.equals(dispatch)) {
            curPage--;
            this.targetPageNo = (curPage < 1) ? 1 : curPage;
        } else if (DISPATCH_FIRST.equals(dispatch)) {
            this.targetPageNo = 1;
        } else if (DISPATCH_LAST.equals(dispatch)) {
            this.targetPageNo = this.totalPage;
        } else if (DISPATCH_GO.equals(dispatch) || dispatch == null) {
            this.targetPageNo = (curPage > this.totalPage) ? this.totalPage : curPage;
            this.targetPageNo = (this.targetPageNo < 1) ? 1 : this.targetPageNo;
        } else {
            this.targetPageNo = (curPage < 1) ? 1 : curPage;
        }
        this.firstRecord = ((this.targetPageNo - 1) * this.rowPerPage) + 1;
        this.dispatch = dispatch;
    }

    public Object clone() {
        PagingInfo result = new PagingInfo(this.rowPerPage);
        result.currentPageNo = this.currentPageNo;
        result.dispatch = this.dispatch;
        result.targetPageNo = this.targetPageNo;
        result.totalPage = this.totalPage;
        result.totalRecord = this.totalRecord;
        result.firstRecord = this.firstRecord;
        result.lastRecord = this.lastRecord;
        return result;
    }

}
