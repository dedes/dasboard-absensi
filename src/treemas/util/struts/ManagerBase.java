package treemas.util.struts;

import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

public abstract class ManagerBase {
    public static final int ERROR_DATA_EXISTS = 900;
    public static final int ERROR_DB = 901;
    public static final int ERROR_UNKNOWN = 999;
    public static final int ERROR_WEB_SERVICE = 500;

    protected ILogger logger = LogManager.getDefaultLogger();

    public ManagerBase() {
    }
}
