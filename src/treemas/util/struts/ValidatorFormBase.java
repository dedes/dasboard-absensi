package treemas.util.struts;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;
import treemas.util.beanaction.BeanConverter;
import treemas.util.tool.Tools;

import javax.servlet.http.HttpServletRequest;

public abstract class ValidatorFormBase extends ValidatorForm {
    private static final String[] ENCODE_PARAMETERS = new String[]{"previousUri"};

    private String task;
    private String previousUri;
    private String currentUri;

    public ValidatorFormBase() {
    }

    public abstract String resolvePreviousTask();

    protected abstract boolean shallValidate();

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getPreviousUri() {
        return previousUri;
    }

    public void setPreviousUri(String previousUri) {
        this.previousUri = previousUri;
    }

    public String getCurrentUri() {
        return currentUri;
    }

    public void setCurrentUri(String currentUri) {
        this.currentUri = currentUri;
    }

    public ActionErrors validate(ActionMapping mapping,
                                 HttpServletRequest request) {
        if (this.shallValidate()) {
            ActionErrors result = super.validate(mapping, request);
            if (result != null && !result.isEmpty())
                this.task = resolvePreviousTask();
            return result;
        } else
            return null;
    }

    public void toBean(Object bean) {
        BeanConverter.convertBeanFromString(this, bean);
    }

    public void fromBean(Object bean) {
        BeanConverter.convertBeanToString(bean, this);
    }

    public void fillCurrentUri(HttpServletRequest request,
                               String[] includedParams) {
        this.currentUri = Tools.getCompleteUri(request, includedParams,
                ENCODE_PARAMETERS);
    }

    public void readCurrentUri(HttpServletRequest request,
                               String[] includedParams) {
        this.currentUri = Tools.getCompleteUriPlain(request, includedParams);
    }

}
