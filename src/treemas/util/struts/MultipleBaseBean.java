package treemas.util.struts;

public final class MultipleBaseBean implements java.io.Serializable {
    private String optKey;
    private String optLabel;
    private String keyValue;
    private String label;

    public MultipleBaseBean() {
        this(null, "");
    }

    public MultipleBaseBean(String optKey, String optLabel) {
        this.optKey = optKey;
        this.optLabel = optLabel;
    }

    public String getOptKey() {
        return optKey;
    }

    public void setOptKey(String optKey) {
        this.optKey = optKey;
    }

    public String getOptLabel() {
        return optLabel;
    }

    public void setOptLabel(String optLabel) {
        this.optLabel = optLabel;
    }

    public String toString() {
        return "optKey=" + this.optKey + "; optValue=" + this.optLabel;
    }

    public int hashCode() {
        if (this.optKey != null) {
            return this.optKey.hashCode();
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof MultipleBaseBean)) {
            return false;
        }
        MultipleBaseBean bean = (MultipleBaseBean) obj;
        String optValue = bean.getOptKey();
        if (optValue == this.optKey) {
            return true;
        }
        if (optValue == null || this.optKey == null) {
            return false;
        }
        return optValue.equals(this.optKey);
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
