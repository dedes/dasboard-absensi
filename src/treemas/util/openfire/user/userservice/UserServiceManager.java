package treemas.util.openfire.user.userservice;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import treemas.util.http.HTTPResponseReader;
import treemas.util.openfire.user.UserException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;


public class UserServiceManager {
    private static final String TYPE_ADD = "add";
    private static final String TYPE_UPDATE = "update";
    private static final String TYPE_DELETE = "delete";

    private static final String XML_SUCCESSFULL_ELEMENT = "result";
    private static final String SUCCESSFULL_OK = "OK";

    private static final String XML_UNSUCCESSFULL_ELEMENT = "error";
    private static final String UNSUCCESSFULL_ILLEGAL_ARG = "IllegalArgumentException";
    private static final String UNSUCCESSFULL_USER_NOT_FOUND = "UserNotFoundException";
    private static final String UNSUCCESSFULL_USER_ALREADY_EXISTS = "UserAlreadyExistsException";
    private static final String UNSUCCESSFULL_REQUEST_NOT_AUTHORISED = "RequestNotAuthorised";
    private static final String UNSUCCESSFULL_USER_SERVICE_DISABLED = "UserServiceDisabled";

    private String serviceAddress;
    private String secretKey;
    private UserServiceRequestBuilder requestBuilder;

    public UserServiceManager(String serviceAddress, String secretKey) {
        this.serviceAddress = serviceAddress;
        this.secretKey = secretKey;
        this.requestBuilder = new UserServiceRequestBuilder(serviceAddress);
    }

    public String addUser(UserServiceParameter parameter) throws UserException {
        try {
            parameter.setType(TYPE_ADD);
            parameter.setSecret(this.secretKey);
            String request = requestBuilder.buildRequest(parameter);
            HTTPResponseReader httpResponseReader = new HTTPResponseReader();

            Reader response = httpResponseReader.getResponseStream(request);
            return this.processUserServiceResponse(response);
        } catch (UnsupportedEncodingException uee) {
            throw new UserServiceException(uee, UserServiceException.UNKNOWN_ERROR);
        } catch (MalformedURLException me) {
            throw new UserServiceException(me, UserServiceException.UNKNOWN_ERROR);
        } catch (IOException ioe) {
            throw new UserServiceException(ioe, UserServiceException.UNKNOWN_ERROR);
        }
    }

    public String updateUser(UserServiceParameter parameter) throws UserException {
        try {
            parameter.setType(TYPE_UPDATE);
            parameter.setSecret(this.secretKey);
            String request = requestBuilder.buildRequest(parameter);
            HTTPResponseReader httpResponseReader = new HTTPResponseReader();

            Reader response = httpResponseReader.getResponseStream(request);
            return this.processUserServiceResponse(response);
        } catch (UnsupportedEncodingException uee) {
            throw new UserServiceException(uee, UserServiceException.UNKNOWN_ERROR);
        } catch (MalformedURLException me) {
            throw new UserServiceException(me, UserServiceException.UNKNOWN_ERROR);
        } catch (IOException ioe) {
            throw new UserServiceException(ioe, UserServiceException.UNKNOWN_ERROR);
        }
    }

    public String deleteUser(UserServiceParameter parameter) throws UserException {
        try {
            parameter.setType(TYPE_DELETE);
            parameter.setSecret(this.secretKey);
            String request = requestBuilder.buildRequest(parameter);
            HTTPResponseReader httpResponseReader = new HTTPResponseReader();

            Reader response = httpResponseReader.getResponseStream(request);
            return this.processUserServiceResponse(response);
        } catch (UnsupportedEncodingException uee) {
            throw new UserServiceException(uee, UserServiceException.UNKNOWN_ERROR);
        } catch (MalformedURLException me) {
            throw new UserServiceException(me, UserServiceException.UNKNOWN_ERROR);
        } catch (IOException ioe) {
            throw new UserServiceException(ioe, UserServiceException.UNKNOWN_ERROR);
        }
    }

    private String processUserServiceResponse(Reader response) throws UserException {
        try {
            try {
                String result = null;
                InputSource source = new InputSource(response);
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(source);

                NodeList nodeList = doc.getElementsByTagName(XML_UNSUCCESSFULL_ELEMENT);
                if (null != nodeList && nodeList.getLength() > 0) {
                    Node node = nodeList.item(0);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        NodeList errorNode = node.getChildNodes();
                        result = ((Node) errorNode.item(0)).getNodeValue();

                        if (UNSUCCESSFULL_ILLEGAL_ARG.equals(result)) {
                            throw new UserServiceException(UserServiceException.ILLEGAL_ARGUMENT, result);
                        } else if (UNSUCCESSFULL_USER_NOT_FOUND.equals(result)) {
                            throw new UserServiceException(UserServiceException.USER_NOT_FOUND, result);
                        } else if (UNSUCCESSFULL_USER_ALREADY_EXISTS.equals(result)) {
                            throw new UserServiceException(UserServiceException.USER_ALREADY_EXISTS, result);
                        } else if (UNSUCCESSFULL_REQUEST_NOT_AUTHORISED.equals(result)) {
                            throw new UserServiceException(UserServiceException.REQUEST_NOT_AUTHORISED, result);
                        } else if (UNSUCCESSFULL_USER_SERVICE_DISABLED.equals(result)) {
                            throw new UserServiceException(UserServiceException.USER_SERVICE_DISABLED, result);
                        }
                    }
                }

                NodeList ndList = doc.getElementsByTagName(XML_SUCCESSFULL_ELEMENT);
                if (null != ndList && ndList.getLength() > 0) {
                    Node node = ndList.item(0);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        NodeList errorNode = node.getChildNodes();
                        result = ((Node) errorNode.item(0)).getNodeValue();

                        if (SUCCESSFULL_OK.equals(result)) {
                            return result;
                        }
                    }
                }

                return result;
            } finally {
                response.close();
            }
        } catch (UserServiceException ue) {
            throw ue;
        } catch (Exception ex) {
            throw new UserServiceException(ex, UserException.UNKNOWN_ERROR);
        }
    }
}