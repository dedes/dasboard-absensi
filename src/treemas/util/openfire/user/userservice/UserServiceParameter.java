package treemas.util.openfire.user.userservice;

import treemas.util.openfire.user.UserParameter;

public class UserServiceParameter extends UserParameter {
    private String type;
    private String secret;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}