package treemas.util.openfire.user.userservice;

import treemas.util.openfire.user.UserException;

public class UserServiceException extends UserException {
    public static final int REQUEST_NOT_AUTHORISED = 103;
    public static final int USER_SERVICE_DISABLED = 104;

    public UserServiceException(int errorCode) {
        this("", null, errorCode, null);
    }

    public UserServiceException(int errorCode, Object userObject) {
        this("", null, errorCode, userObject);
    }

    public UserServiceException(Throwable cause, int errorCode) {
        this("", cause, errorCode, null);
    }

    public UserServiceException(String msg, Throwable cause, int errorCode,
                                Object userObject) {
        super(msg, cause, errorCode, userObject);
    }
}
