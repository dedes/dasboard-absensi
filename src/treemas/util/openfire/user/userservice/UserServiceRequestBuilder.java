package treemas.util.openfire.user.userservice;

import treemas.util.tool.Tools;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class UserServiceRequestBuilder {
    public static final String PARAM_NAME_TYPE = "type";
    public static final String PARAM_NAME_SECRET = "secret";
    public static final String PARAM_NAME_USERNAME = "username";
    public static final String PARAM_NAME_PASSWORD = "password";
    public static final String PARAM_NAME_NAME = "name";
    public static final String PARAM_NAME_EMAIL = "email";
    public static final String PARAM_NAME_GROUPS = "groups";

    private String serviceAddress;

    public UserServiceRequestBuilder(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }

    public String buildRequest(UserServiceParameter parameter) throws UnsupportedEncodingException {
        StringBuffer request = new StringBuffer(this.serviceAddress);

        List listOfFilledParameter = this.getFilledParameter(parameter);
        String[] arrQueryString = new String[listOfFilledParameter.size()];
        listOfFilledParameter.toArray(arrQueryString);
        if (null != arrQueryString && arrQueryString.length > 0) {
            request.append("?");
            String queryString = Tools.implode(arrQueryString, "&");
            request.append(queryString);
        }

        System.out.println(request.toString());
        return request.toString();
    }

    private List getFilledParameter(UserServiceParameter parameter) throws UnsupportedEncodingException {
        List queryString = new ArrayList();

        String type = parameter.getType();
        if (null != type) {
            queryString.add(new StringBuffer(PARAM_NAME_TYPE).
                    append("=").
                    append(URLEncoder.encode(type, "UTF-8")).toString());
        }

        String secret = parameter.getSecret();
        if (null != secret) {
            queryString.add(new StringBuffer(PARAM_NAME_SECRET).
                    append("=").append(URLEncoder.encode(secret, "UTF-8")).toString());
        }

        String username = parameter.getUsername();
        if (null != username) {
            queryString.add(new StringBuffer(PARAM_NAME_USERNAME).
                    append("=").append(URLEncoder.encode(username, "UTF-8")).toString());
        }

        String password = parameter.getPassword();
        if (null != parameter.getPassword()) {
            queryString.add(new StringBuffer(PARAM_NAME_PASSWORD).
                    append("=").append(URLEncoder.encode(password, "UTF-8")).toString());
        }

        String name = parameter.getName();
        if (null != name) {
            queryString.add(new StringBuffer(PARAM_NAME_NAME).
                    append("=").append(URLEncoder.encode(name, "UTF-8")).toString());
        }

        String email = parameter.getEmail();
        if (null != parameter.getEmail()) {
            queryString.add(new StringBuffer(PARAM_NAME_EMAIL).
                    append("=").append(URLEncoder.encode(email, "UTF-8")).toString());
        }

        String[] groups = parameter.getGroups();
        if (null != groups && groups.length > 0) {
            String flattedGroups = Tools.implode(groups, ",");
            queryString.add(new StringBuffer(PARAM_NAME_GROUPS).
                    append("=").append(URLEncoder.encode(flattedGroups, "UTF-8")).toString());
        }

        return queryString;
    }
}
