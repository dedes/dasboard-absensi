package treemas.util.openfire.user;

import com.ibatis.sqlmap.client.SqlMapClient;
import treemas.util.databases.OracleErrorCode;
import treemas.util.ibatis.IbatisHelper;
import treemas.util.openfire.util.Blowfish;
import treemas.util.openfire.util.StringUtils;
import treemas.util.struts.ManagerBase;
import treemas.util.tool.Tools;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OpenfireUserGroupManager extends ManagerBase {
    protected SqlMapClient ibatisSqlMap = null;

    public OpenfireUserGroupManager() {
        this.ibatisSqlMap = IbatisHelper.getSqlMap();
    }

    public void addGroup(String groupName, String description)
            throws UserException {
        if (null == groupName)
            throw new IllegalArgumentException("Groupname can not be null!!!");

        try {
            Map paramMap = new HashMap();
            paramMap.put("groupName", groupName);
            paramMap.put("description", description);
            this.ibatisSqlMap.insert("of.usergroup.insertOfGroup", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new UserException("", sqle, UserException.GROUP_ALREADY_EXISTS, groupName);
            } else {
                throw new UserException(sqle.toString(), sqle, ERROR_DB, null);
            }
        }
    }

    public void addGroupPropShowInRoster(String groupName)
            throws UserException {
        if (null == groupName)
            throw new IllegalArgumentException("Groupname can not be null!!!");

        try {
            this.ibatisSqlMap.insert("of.usergroup.insertOfGroupPropShowInRoster", groupName);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new UserException("", sqle, UserException.GROUP_ALREADY_EXISTS, groupName);
            } else {
                throw new UserException(sqle.toString(), sqle, ERROR_DB, null);
            }
        }
    }

    public void addGroupPropDisplayName(String groupName, boolean isCommandCenter)
            throws UserException {
        if (null == groupName)
            throw new IllegalArgumentException("Groupname can not be null!!!");

        try {
            Map paramMap = new HashMap();
            paramMap.put("groupName", groupName);
            if (isCommandCenter) {
                paramMap.put("propValue", "Surveyors");
            } else {
                paramMap.put("propValue", "CallCenter");
            }

            this.ibatisSqlMap.insert("of.usergroup.insertOfGroupPropDisplayName", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new UserException("", sqle, UserException.GROUP_ALREADY_EXISTS, groupName);
            } else {
                throw new UserException(sqle.toString(), sqle, ERROR_DB, null);
            }
        }
    }

    public void addUser(String username, String plainPassword,
                        String name) throws UserException {
        if (null == username)
            throw new IllegalArgumentException("Username can not be null!!!");

        try {
            String encryptedPassword = null;
            if (plainPassword == null) {
                encryptedPassword = null;
            }
            String passwordKey = (String) this.ibatisSqlMap.queryForObject(
                    "of.usergroup.getPasswordKey", null);
            Blowfish cipher = new Blowfish(passwordKey);

            if (cipher == null) {
                throw new UnsupportedOperationException();
            }
            encryptedPassword = cipher.encryptString(plainPassword);

            Date sysdate = new Date(System.currentTimeMillis());
            String creationDate = StringUtils.dateToMillis(sysdate);

            Map paramMap = new HashMap();
            paramMap.put("username", username);
            paramMap.put("encryptedPassword", encryptedPassword);
            paramMap.put("name", name);
            paramMap.put("creationDate", creationDate);
            this.ibatisSqlMap.insert("of.usergroup.insertOfUser", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new UserException("", sqle, UserException.USER_ALREADY_EXISTS, username);
            } else {
                throw new UserException(sqle.toString(), sqle, ERROR_DB, null);
            }
        }
    }

    public void addUserToGroup(String username, String groupName) throws UserException {
        if (null == username || null == groupName)
            throw new IllegalArgumentException("Username or Groupname can not be null!!!");

        try {
            Map paramMap = new HashMap();
            paramMap.put("groupName", groupName);
            paramMap.put("username", username);
            this.ibatisSqlMap.insert("of.usergroup.insertUserToGroup", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            if (sqle.getErrorCode() == OracleErrorCode.ERROR_DUPLICATE_KEY) {
                throw new UserException("", sqle, UserException.USER_GROUP_ALREADY_EXISTS, username);
            } else {
                throw new UserException(sqle.toString(), sqle, ERROR_DB, null);
            }
        }
    }

    public void updateGroupList(String groupName, String[] groupList) throws UserException {
        if (null == groupName)
            throw new IllegalArgumentException("Groupname can not be null!!!");

        try {
            if (groupList != null && groupList.length > 0) {
                String flattenGroupList = Tools.implode(groupList, ",");

                Integer count = (Integer) this.ibatisSqlMap.queryForObject(
                        "of.usergroup.countPropGroupList", groupName);

                Map paramMap = new HashMap();
                paramMap.put("groupName", groupName);
                paramMap.put("propValue", flattenGroupList);

                if (count.intValue() == 0) {
                    this.ibatisSqlMap.insert("of.usergroup.insertOfGroupPropGroupList", paramMap);
                } else {
                    this.ibatisSqlMap.update("of.usergroup.updateOfGroupPropGroupList", paramMap);
                }
            } else {
                this.ibatisSqlMap.delete("of.usergroup.deleteOfGroupPropGroupList", groupName);
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new UserException(ERROR_DB);
        }
    }

    public void removeUserFromGroup(String groupName, String username) throws UserException {
        if (null == username || null == groupName)
            throw new IllegalArgumentException("Username or Groupname can not be null!!!");

        try {
            Map paramMap = new HashMap();
            paramMap.put("groupName", groupName);
            paramMap.put("username", username);
            this.ibatisSqlMap.delete("of.usergroup.deleteUserFromGroup", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new UserException(ERROR_DB);
        }
    }

    public void deleteGroup(String groupName) throws UserException {
        if (null == groupName)
            throw new IllegalArgumentException("Groupname can not be null!!!");

        try {
            this.ibatisSqlMap.delete("of.usergroup.deleteOfGroup", groupName);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new UserException(ERROR_DB);
        }
    }

    public void deleteGroupProp(String groupName) throws UserException {
        if (null == groupName)
            throw new IllegalArgumentException("Groupname can not be null!!!");

        try {
            this.ibatisSqlMap.delete("of.usergroup.deleteOfGroupProp", groupName);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new UserException(ERROR_DB);
        }
    }

    public void enableUser(String username) throws UserException {
        if (null == username)
            throw new IllegalArgumentException("Username can not be null!!!");

        try {
            this.ibatisSqlMap.delete("of.usergroup.deleteFlagLockout", username);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new UserException(ERROR_DB);
        }
    }

    public void disableUser(String username) throws UserException {
        if (null == username)
            throw new IllegalArgumentException("Username can not be null!!!");

        try {
            Integer count = (Integer) this.ibatisSqlMap.queryForObject(
                    "of.usergroup.countLockoutUser", username);

            if (count.intValue() == 0) {
                this.ibatisSqlMap.insert("of.usergroup.insertFlagLockoutUser", username);
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new UserException(ERROR_DB);
        }
    }

    public void deleteUser(String username) throws UserException {
        if (null == username)
            throw new IllegalArgumentException("Username can not be null!!!");

        try {
            this.ibatisSqlMap.delete("of.usergroup.deleteOfUser", username);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new UserException(ERROR_DB);
        }
    }
}