package treemas.util.openfire.user;

import treemas.util.openfire.OpenfireException;


public class UserException extends OpenfireException {
    public static final int ILLEGAL_ARGUMENT = 100;
    public static final int USER_NOT_FOUND = 101;
    public static final int USER_ALREADY_EXISTS = 102;
    public static final int GROUP_NOT_FOUND = 103;
    public static final int GROUP_ALREADY_EXISTS = 104;
    public static final int USER_GROUP_NOT_FOUND = 105;
    public static final int USER_GROUP_ALREADY_EXISTS = 106;
    public static final int UNKNOWN_ERROR = 999;

    public UserException(int errorCode) {
        this("", null, errorCode, null);
    }

    public UserException(int errorCode, Object userObject) {
        this("", null, errorCode, userObject);
    }

    public UserException(Throwable cause, int errorCode) {
        this("", cause, errorCode, null);
    }

    public UserException(String msg, Throwable cause, int errorCode,
                         Object userObject) {
        super(msg, cause, errorCode, userObject);
    }
}
