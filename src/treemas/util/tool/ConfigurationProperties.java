package treemas.util.tool;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class ConfigurationProperties {
    private static final String SYSTEM_ENVIRONMENT = "configuration.properties";

    private static ConfigurationProperties theInstance;

    static {
        initFromSystem();
    }

    private Properties properties = new Properties();

    private ConfigurationProperties() {
    }

    public static synchronized void initFromFile(String propFileName)
            throws Exception {
        if (theInstance != null) {
            return;
        }

        FileInputStream fis = new FileInputStream(propFileName);
        if (fis == null)
            throw new IllegalStateException("Cannot load " + propFileName);
        try {
            theInstance = new ConfigurationProperties();
            theInstance.readProperties(fis);
        } finally {
            try {
                fis.close();
            } catch (Exception e) {
            }
        }
    }

    private static void initFromSystem() {
        String propFileName = System.getProperty(SYSTEM_ENVIRONMENT);
        if (propFileName != null) {
            try {
                initFromFile(propFileName);
            } catch (Exception e) {
                System.err.println("Error loading configuration properties: "
                        + e.toString());
            }
        }
    }

    public static synchronized void initFromResource(String resourceName)
            throws Exception {
        if (theInstance != null) {
            return;
        }

        theInstance = new ConfigurationProperties();
        InputStream in = ConfigurationProperties.class.getClassLoader()
                .getResourceAsStream(resourceName);
        if (in == null)
            throw new IllegalStateException("Cannot find " + resourceName);
        theInstance.readProperties(in);

    }

    public static ConfigurationProperties getInstance() {
        return theInstance;
    }

    public String get(String key) {
        return (String) properties.get(key);
    }

    private synchronized void readProperties(InputStream in) throws Exception {
        try {
            properties.load(in);
        } finally {
            try {
                in.close();
            } catch (Exception e) {
            }
        }
    }
}
