package treemas.util.gcm;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostMan {

    public static Model generateInput(String regId, String assginmentId, String user) {
        Model model = new Model();
        List<String> registration_ids = new ArrayList<String>();
        Map<String, String> data = new HashMap<String, String>();
        registration_ids.add(regId);
        data.put("title", "N-New Assignment " + assginmentId);
//    	data.put("message", "You got new assignment id:"+assginmentId+", by:"+user);
        data.put("message", "You got " + assginmentId + " new assignment, by:" + user);
        model.setRegistration_ids(registration_ids);
        model.setData(data);

        return model;
    }

    public static Model generateReassign(String regId, String[] assginmentId, String user) {
        Model model = new Model();
        List<String> registration_ids = new ArrayList<String>();
        Map<String, String> data = new HashMap<String, String>();
        registration_ids.add(regId);
        String assId = "";
        int count = assginmentId.length;
        for (int i = 0; i < assginmentId.length; i++) {
            if (i == assginmentId.length - 1)
                assId = assId + "," + assginmentId[i];
            else
                assId = assId + "," + assginmentId[i] + ",";
        }
        data.put("title", "R-" + count + "-New Task(s)");
        data.put("message", "You got " + count + " re-assignment task id(s):" + assId + ", by:" + user);
        model.setRegistration_ids(registration_ids);
        model.setData(data);

        return model;
    }

    public static Model generateDeleteReassignment(String regId, String[] assginmentId, String user) {
        Model model = new Model();
        List<String> registration_ids = new ArrayList<String>();
        Map<String, String> data = new HashMap<String, String>();
        registration_ids.add(regId);
        String assId = "";
        int count = assginmentId.length;
        for (int i = 0; i < assginmentId.length; i++) {
            if (i == assginmentId.length - 1)
                assId = assId + "," + assginmentId[i];
            else
                assId = assId + "," + assginmentId[i] + ",";
        }
        data.put("title", "D-" + count + " Assignment(s) Deleted");
        data.put("message", "Your assignment(s):" + assId + " has been deleted by:" + user);
        model.setRegistration_ids(registration_ids);
        model.setData(data);

        return model;
    }

    public static Model generateDelete(String regId, String assginmentId, String user) {
        Model model = new Model();
        List<String> registration_ids = new ArrayList<String>();
        Map<String, String> data = new HashMap<String, String>();
        registration_ids.add(regId);
        data.put("title", "D-Assignment " + assginmentId + " Delete");
        data.put("message", "Your assignment:" + assginmentId + " has been deleted by:" + user);
        model.setRegistration_ids(registration_ids);
        model.setData(data);

        return model;
    }

    public static void post(String apiKey, Model model) {
        try {

            URL url = new URL("https://android.googleapis.com/gcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key=" + apiKey);
            conn.setDoOutput(true);
            Gson gson = new Gson();
            String postData = gson.toJson(model);
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();
            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Model generateLockHandset(String regId, String user, String admin, String password) {
        Model model = new Model();
        List<String> registration_ids = new ArrayList<String>();
        Map<String, String> data = new HashMap<String, String>();
        registration_ids.add(regId);
        data.put("title", user + " Lock Handset");
        data.put("message", password);
        model.setRegistration_ids(registration_ids);
        model.setData(data);

        return model;
    }

    public static Model generateResetHandset(String regId, String user) {
        Model model = new Model();
        List<String> registration_ids = new ArrayList<String>();
        Map<String, String> data = new HashMap<String, String>();
        registration_ids.add(regId);
        data.put("title", user + " Reset Handset");
        model.setRegistration_ids(registration_ids);
        model.setData(data);

        return model;
    }
}
