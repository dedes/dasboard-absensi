/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package treemas.util.geofence.google;

import com.google.common.base.Strings;

/**
 * @author vince
 */
public class GoogleAddressFormater {

    private static final String separator = ",";


    public static String formatAddress(String address1, String address2,
                                       String noAddr, String rt, String rw,
                                       String kelurahan, String kecamatan,
                                       String kabupaten, String city, String propinsi,
                                       String country, String postcode) {
        String result = "";

        result = result + (Strings.isNullOrEmpty(address1) ? "" : address1 + " ");
        result = result + (Strings.isNullOrEmpty(address2) ? "" : address2 + " ");
        result = result + (Strings.isNullOrEmpty(noAddr) ? "" : noAddr + " ");
        if (!Strings.isNullOrEmpty(rt) && !Strings.isNullOrEmpty(rw)) {
            result = result + "RT/RW:" + rt + "/" + rw + ", ";
        } else if (!Strings.isNullOrEmpty(rt) && Strings.isNullOrEmpty(rw)) {
            result = result + "RT:" + rt + ", ";
        } else if (Strings.isNullOrEmpty(rt) && !Strings.isNullOrEmpty(rw)) {
            result = result + "RW:" + rw + ", ";
        }
        result = result + (Strings.isNullOrEmpty(kelurahan) ? "" : kelurahan + ", ");
        result = result + (Strings.isNullOrEmpty(kecamatan) ? "" : kecamatan + ", ");
        result = result + (Strings.isNullOrEmpty(kabupaten) ? "" : kabupaten + ", ");
        result = result + (Strings.isNullOrEmpty(city) ? "" : city + ", ");
        result = result + (Strings.isNullOrEmpty(propinsi) ? "" : propinsi + ", ");
        result = result + (Strings.isNullOrEmpty(postcode) ? "" : postcode + ", ");
        result = result + (Strings.isNullOrEmpty(country) ? "" : country);

        if (result.endsWith(" ")) {
            result = result.substring(0, result.length() - 1);
        }

        return result;
    }

    public static String formatAddress(String address) {
//        String address = "Suryomentaraman Wetan no 39,DIY,55131,Indonesia";
        String[] addresses = address.split(GoogleAddressFormater.separator);
        String result = "";
        for (int i = 0; i < addresses.length; i++) {
            String temp = addresses[i].replaceAll(" ", "+");

            temp = (temp.startsWith("+")) ? temp.substring(1) : temp;
            temp = (temp.endsWith("+")) ? temp.substring(0, temp.length() - 1) : temp;
            result = (i < addresses.length - 1) ? result + temp + ",+" : result + temp;

        }
        System.out.println(result);
        return result;
    }
}
