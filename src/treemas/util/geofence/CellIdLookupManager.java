package treemas.util.geofence;

import treemas.base.feature.FeatureManagerBase;
import treemas.util.constant.SQLConstant;
import treemas.util.log.ILogger;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

public class CellIdLookupManager extends FeatureManagerBase {
    private static final String STRING_SQL = SQLConstant.SQL_FEATURE_GEOCODE;

    public GeofenceBean geocodeByCellId(int MCC, int MNC, int LAC, int CID) {
        GeofenceBean result = null;

        List providers = this.getGeocodeServiceProvider();
        if (providers != null && providers.size() > 0) {
            for (Iterator iterator = providers.iterator(); iterator.hasNext(); ) {
                String provider = (String) iterator.next();
                AbstractCellIdLookup lookupMgr = CellIdLookupFactory.getCellIdProvider(provider);
                try {
                    result = lookupMgr.getCoordinate(MCC, MNC, LAC, CID);
                    if (result == null) {
                        continue;
                    } else {
                        break;
                    }
                } catch (Exception ex) {
                    this.logger.printStackTrace(ex);
                }
            }
        } else {
            this.logger.log(ILogger.LEVEL_INFO, "No geocode service provider is active!!!");
        }

        return result;
    }

    private List getGeocodeServiceProvider() {
        List result = null;
        try {
            result = this.ibatisSqlMap.queryForList(this.STRING_SQL + "getGeocodeProvider", null);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
        }
        return result;
    }
}
