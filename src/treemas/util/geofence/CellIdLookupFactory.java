package treemas.util.geofence;


public class CellIdLookupFactory {
    public static final String GOOGLE = "GEO_GOOGLE";
    public static final String OPENCELLID = "GEO_OPENCI";

    private CellIdLookupFactory() {
    }

    public static AbstractCellIdLookup getCellIdProvider(String provider) {
        if (GOOGLE.equals(provider)) {
            return new GoogleCellIdLookupImpl();
        } else if (OPENCELLID.equals(provider)) {
            return new OpenCellIdLookupImpl();
        } else {
            return new GoogleCellIdLookupImpl();
        }
    }
}
