package treemas.util.geofence;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import treemas.util.CoreException;
import treemas.util.http.HTTPResponseReader;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;

import java.io.IOException;
import java.net.MalformedURLException;


public class ReverseGeocode {
    private static final String GEOCODE_URL =
            "http://maps.google.com/maps/api/geocode/json?latlng=[lat],[lng]&sensor=false";
    private static final String TAG_LATITUDE = "[lat]";
    private static final String TAG_LONGITUDE = "[lng]";
    private static final int ERROR_GEOCODE_UNKNOWN = -1;
    private static final int ERROR_GEOCODE_REQUEST = 301;
    private static final int ERROR_GEOCODE_PARSE_RESPONSE = 302;
    private static final String KEY_FORMATTED_ADDRESS = "formatted_address";
    private static ILogger logger = LogManager.getDefaultLogger();
    private HTTPResponseReader httpResponseReader;

    public ReverseGeocode() {
    }

    public ReverseGeocode(HTTPResponseReader httpResponseReader) {
        this.httpResponseReader = httpResponseReader;
    }

    private static String buildReverseGeocodeRequestURL(
            double latitude, double longitude) {
        StringBuffer result = new StringBuffer(ReverseGeocode.GEOCODE_URL);
        ReverseGeocode.replace(result, TAG_LATITUDE, String.valueOf(latitude));
        ReverseGeocode.replace(result, TAG_LONGITUDE, String.valueOf(longitude));
        return result.toString();
    }

    private static void replace(StringBuffer message, String tag, String replacement) {
        int startTag = message.indexOf(tag);
        int endTag = startTag + tag.length();
        if (replacement != null && !"".equals(replacement.trim())) {
            message.replace(startTag, endTag, replacement);
        }
    }

    public String reverseGeocoding(
            double latitude, double longitude) throws CoreException {
        String addressResult = null;

        String jsonRequestUri = ReverseGeocode.buildReverseGeocodeRequestURL(latitude, longitude);
        if (logger != null)
            logger.log(ILogger.LEVEL_INFO, "Trying to reverse geocode through URI: " + jsonRequestUri);

        try {
            String jsonData = httpResponseReader.getResponseString(jsonRequestUri);
            JSONParser parser = new JSONParser();
            KeyFinder finder = new KeyFinder();
            finder.setMatchKey(KEY_FORMATTED_ADDRESS);
            while (!finder.isEnd()) {
                parser.parse(jsonData, finder, true);
                if (finder.isFound()) {
                    finder.setFound(false);
                    addressResult = (String) finder.getValue();
                    break;
                }
            }

            if (logger != null)
                logger.log(ILogger.LEVEL_INFO, "Trying to reverse geocode through URI: " + jsonRequestUri
                        + " finished... Address found: " + addressResult);
        } catch (MalformedURLException me) {
            if (logger != null)
                logger.printStackTrace(me);
            else
                me.printStackTrace();

            throw new CoreException(ERROR_GEOCODE_REQUEST);
        } catch (IOException ioe) {
            if (logger != null)
                logger.printStackTrace(ioe);
            else
                ioe.printStackTrace();

            throw new CoreException(ERROR_GEOCODE_REQUEST);
        } catch (ParseException pe) {
            if (logger != null)
                logger.printStackTrace(pe);
            else
                pe.printStackTrace();

            throw new CoreException(ERROR_GEOCODE_PARSE_RESPONSE);
        } catch (Exception ex) {
            if (logger != null)
                logger.printStackTrace(ex);
            else
                ex.printStackTrace();

            throw new CoreException(ERROR_GEOCODE_UNKNOWN);
        }

        return addressResult;
    }
}
