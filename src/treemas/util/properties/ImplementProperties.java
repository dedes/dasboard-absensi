package treemas.util.properties;

import java.io.InputStream;
import java.util.Properties;


public class ImplementProperties {
    private static ImplementProperties instance = new ImplementProperties();

    private Properties properties = new Properties();

    private ImplementProperties() {
        try {
            this.initFromResource(PropertiesKey.IMPLEMENT_PROPERTIES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ImplementProperties getInstance() {
        return instance;
    }

    private void initFromResource(String resourceName) throws Exception {
        InputStream in = ImplementProperties.class.getClassLoader().
                getResourceAsStream(resourceName);
        if (in == null)
            throw new IllegalStateException("Cannot find " + resourceName);
        this.readProperties(in);
    }

    public String get(String key) {
        return (String) properties.get(key);
    }

    private synchronized void readProperties(InputStream in) throws Exception {
        try {
            properties.load(in);
        } finally {
            try {
                in.close();
            } catch (Exception e) {
            }
        }
    }

}
