package treemas.util.properties;

public abstract class PropertiesKey {

    public static final String CONFIGURATION_PROPERTIES = "configuration.properties";
    public static final String VALIDATOR_PROPERTIES = "validator.properties";
    public static final String IMPLEMENT_PROPERTIES = "implement.properties";
    public static final String INIT_DATE_PROPERTIES = "treemas.base.sec.term.DatePeriod";
    public static final String INIT_USER_PROPERTIES = "treemas.base.sec.term.UserInterface";
    public static final String INIT_HARDWARE_PROPERTIES = "treemas.base.sec.term.HardwareMAC";
    public static final String INIT_CLASS = "initializer.class";
    public static final String ERROR_PAGE = "/error/notfound.jsp";
    public static final String DB_JNDI = "db.jndi";
    public static final String EJB_CONTEXT_URL = "ejb.context.url";
    public static final String EJB_CONTEXT_FACTORY = "ejb.context.factory";
    public static final String EJB_CONTEXT_URL_PKG = "ejb.context.url.pkg";
    public static final String EJB_CONTEXT_PRINCIPAL = "ejb.context.principal";
    public static final String EJB_CONTEXT_CREDENTIAL = "ejb.context.credential";
    public static final String CONTEXT_URL = "context.url";
    public static final String CONTEXT_FACTORY = "context.factory";
    public static final String LOG_FILE_FULLNAME = "log.file.name";
    public static final String LOG_FILE_SIZE = "log.file.size";
    public static final String LOG_FILE_COUNT = "log.file.count";
    public static final String LOG_FILE_LEVEL = "log.file.level";
    public static final String INIT_CLASS_KEY = "initializer.class";
    public static final String INIT_FILE_NAME = "initializer.config.file";
    public static final String INIT_RESOURCE_NAME = "initializer.config.resource";
    public static final String IBATIS_CONFIG_NAME = "ibatis.config";
    public static final String EXECUTOR_EJB_ENABLE = "executor.enable";
    public static final String ODS_USEEJB = "ods.useEjb";
    public static final String IMPL_DATA_ACCESS = "impl.ejb.dataaccess";
    public static final String OPENFIRE_ENABLED = "openfire.enable";
    public static final String DB_SEQUENCE = "db.sequence";
    public static final String APP_NAME = "application.name";

    protected PropertiesKey() {
        throw new IllegalStateException(
                "PropertiesKey class shall not be instantiated! Class="
                        + this.getClass().getName());
    }
}
