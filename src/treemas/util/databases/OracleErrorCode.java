package treemas.util.databases;

public class OracleErrorCode {

    public static final int ERROR_DUPLICATE_KEY = 1;
    public static final int ERROR_CANNOT_INSERT_NULL = 1400;
    public static final int ERROR_PARENT_NOT_FOUND = 2291;
    public static final int ERROR_CHILD_FOUND = 2292;

    public OracleErrorCode() {
    }
}
