package treemas.util.databases;

import treemas.util.constant.Global;
import treemas.util.properties.PropertiesKey;
import treemas.util.tool.ConfigurationProperties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Hashtable;

public class ConnectionManager {
    private static HashMap managers = new HashMap(2, 1f);
    private static String configJndi =
            ConfigurationProperties.getInstance().get(PropertiesKey.DB_JNDI);
    private static String configUrl =
            ConfigurationProperties.getInstance().get(PropertiesKey.CONTEXT_URL);

    private static String configContextFactory =
            ConfigurationProperties.getInstance().get(PropertiesKey.CONTEXT_FACTORY);

    static {
        System.out.println("Default datasource JNDI/Url=" + configJndi + "/"
                + configUrl);
    }

    private DataSource dataSource = null;
    private String jndiName;
    private Hashtable environment = new Hashtable(2, 1f);


    private ConnectionManager(String jndiName, String serverUrl) {
        this.jndiName = jndiName;
        if (serverUrl != null) {
            String factory = configContextFactory != null ? configContextFactory :
                    Global.WEBLOGIC_CONTEXT_FACTORY;
            this.environment.put(Context.INITIAL_CONTEXT_FACTORY, factory);
            this.environment.put(Context.PROVIDER_URL, serverUrl);

        }
    }

    public static ConnectionManager getInstance() {
        return getInstance(configJndi, configUrl);
    }

    public static ConnectionManager getInstance(String jndiName) {
        return getInstance(jndiName, null);
    }

    public static ConnectionManager getInstance(String jndiName,
                                                String serverUrl) {
        ConnectionManager result = null;
        String key = serverUrl == null ? jndiName : serverUrl + jndiName;
        result = (ConnectionManager) managers.get(key);
        if (result == null) {
            result = new ConnectionManager(jndiName, serverUrl);
            managers.put(key, result);
        }
        return result;
    }

    public Connection getConnection() throws SQLException {
        Connection conn = null;
        try {
            if (this.dataSource == null) {
                this.getDataSource();
            }

            conn = this.dataSource.getConnection();

            if (conn == null) {
                throw new SQLException("Connection is null!");
            }
        } catch (SQLException sqe) {
            throw sqe;
        } catch (NamingException ne) {
            throw new SQLException("Error getting datasource:" + ne.toString());
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("Exception when getting connection:" +
                    e.toString());
        }
        return conn;
    }

    public void closeConnection(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
    }

    private synchronized void getDataSource() throws NamingException {
        if (this.dataSource != null) {
            return;
        }
        InitialContext ctx = null;
        try {
            ctx = new InitialContext(this.environment);
            dataSource = (DataSource) ctx.lookup(this.jndiName);
        } finally {
            if (ctx != null) {
                ctx.close();
            }
        }
    }


}
