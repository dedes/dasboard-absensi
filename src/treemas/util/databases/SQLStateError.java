package treemas.util.databases;

import java.util.HashMap;

public class SQLStateError {

    public static final String SUCCESS = "00000";
    public static final String WARNING = "01000";
    public static final String CURSOR_CONFLICT = "01001";
    public static final String DISCONNECT_ERROR = "01002";
    public static final String NULL_VALUE_ELIMINATED = "01003";
    public static final String STRING_TRUNCATION = "01004";
    public static final String INSUFFICIENT_ITEM_DESC = "01005";
    public static final String PRIVILEGED_NOT_REVOKED = "01006";
    public static final String PRIVILEGED_NOT_GRANTED = "01007";
    public static final String IMPLICIT_ZERO_BIT_PADDING = "01008";
    public static final String SEARCH_CONDITION_TOO_LONG = "01009";
    public static final String QUERY_EXPRESSION_TOO_LONG = "0100A";
    public static final String NO_DATA = "02000";
    public static final String DYNAMIC_SQL_ERROR = "07000";
    public static final String USING_CLAUSE_INVALID_PARAM = "07001";
    public static final String USING_CLAUSE_INVALID_TARGET = "07002";
    public static final String CANNOT_EXECUTE_CURSOR_SPEC = "07003";
    public static final String USING_CLAUSE_REQUIRED_4_DYNAMIC = "07004";
    public static final String PREPARED_STATEMENT_INVALID = "07005";
    public static final String DATATYPE_VIOLATION = "07006";
    public static final String USING_CLAUSE_REQUIRED_4_RESULT = "07007";
    public static final String INVALID_DESCRIPTOR_COUNT = "07008";
    public static final String INVALID_DESCRIPTOR_INDEX = "07009";
    public static final String CONNECTION_EXCEPTION = "08000";
    public static final String SQL_CLIENT_CANNOT_CONNECT = "08001";
    public static final String CONNECTION_NAME_IN_USE = "08002";
    public static final String CONNECTION_DOES_NOT_EXIST = "08003";
    public static final String SQL_SERVER_REJECT_CONNECTION = "08004";
    public static final String CONNECTION_FAILURE = "08006";
    public static final String TRANSACTION_RESOLUTION_UNKNOWN = "08007";
    public static final String FEATURE_NOT_SUPPORTED = "0A000";
    public static final String MULTIPLE_SERVER_TRANSACTIONS = "0A001";
    public static final String CARDINALITY_VIOLATION = "21000";
    public static final String DATA_EXCEPTION = "22000";
    public static final String DATA_TRUNCATION2 = "22001";
    public static final String NULL_VALUE = "22002";
    public static final String NUMERIC_OUT_OF_RANGE = "22003";
    public static final String ERROR_IN_ASSIGNMENT = "22005";
    public static final String INVALID_DATE_TIME_FORMAT = "22007";
    public static final String DATE_TIME_FIELD_OVERFLOW = "22008";
    public static final String INVALID_TIME_ZONE_DISPLACEMENT = "22009";
    public static final String SUBSTRING_ERROR = "22011";
    public static final String DIVISION_BY_ZERO = "22012";
    public static final String INTERVAL_FIELD_OVERFLOW = "22015";
    public static final String INVALID_CHARACTER_VALUE = "22018";
    public static final String INVALID_ESCAPE_CHARACTER = "22019";
    public static final String CHARACTER_NOT_IN_REPERTOIRE = "22021";
    public static final String INDICATOR_OVERFLOW = "22022";
    public static final String INVALID_PARAMETER_VALUE = "22023";
    public static final String UNTERMINATED_C_STRING = "22024";
    public static final String INVALID_ESCAPE_SEQUENCE = "22025";
    public static final String STRING_DATA_LENGTH_MISMATCH = "22026";
    public static final String TRIM_ERROR = "22027";
    public static final String INTEGRITY_CONSTRAINT_VIOLATION = "23000";
    public static final String INVALID_CURSOR_STATE = "24000";
    public static final String INVALID_TRANSACTION_STATE = "25000";
    public static final String INVALID_SQL_STATEMENT_NAME = "26000";
    public static final String TRIGGER_DATA_VIOLATION = "27000";
    public static final String INVALID_AUTHORIZATION = "28000";
    public static final String SQL_SYNTAX_ERROR_DIRECT = "2A000";
    public static final String DEPENDENT_PRIVILEGE_EXIST = "2B000";
    public static final String INVALID_CHARACTER_SET_NAME = "2C000";
    public static final String INVALID_TRANSACTION_TERMINATION = "2D000";
    public static final String INVALID_CONNECTION_NAME = "2E000";
    public static final String INVALID_SQL_DESCRIPTOR = "33000";
    public static final String INVALID_CURSOR_NAME = "34000";
    public static final String INVALID_CONDITION_NUMBER = "35000";
    public static final String SQL_SYNTAX_ERROR_DYNAMIC = "37000";
    public static final String AMBIGUOUS_CURSOR_NAME = "3C000";
    public static final String INVALID_CATALOG_NAME = "3D000";
    public static final String INVALID_SCHEMA_NAME = "3F000";
    public static final String TRANSACTION_ROLLBACK = "40000";
    public static final String SERIALIZATION_FAILURE = "40001";
    public static final String INTEGRITY_CONSTRAINT_VIOLATION2 = "40002";
    public static final String STATEMENT_COMPLETION_UNKNOWN = "40003";
    public static final String SYNTAX_ERROR_OR_VIOLATION = "42000";
    public static final String WITH_CHECK_OPTION_VIOLATION = "44000";
    public static final String SYSTEM_ERROR = "60000";
    public static final String RESOURCE_ERROR = "61000";
    public static final String MULTI_THREAD_OR_DETACH_ERROR = "62000";
    public static final String ORACLE_XA_TWO_TASK_ERROR = "63000";
    public static final String CONTROL_DATABASE_REDO_FILE_ERROR = "64000";
    public static final String PL_SQL_ERROR = "65000";
    public static final String SQL_NET_DRIVER_ERROR = "66000";
    public static final String LICENSING_ERROR = "67000";
    public static final String SQL_CONNECT_ERROR = "69000";
    public static final String SQL_EXECUTE_PHASE_ERROR = "72000";
    public static final String OUT_OF_MEMORY = "82100";
    public static final String CURSOR_MISMATCH = "82101";
    public static final String NO_GLOBAL_CURSOR_ENTRY = "82102";
    public static final String CURSOR_CACHE_OUT_OF_RANGE = "82103";
    public static final String NO_CURSOR_CACHE_AVAILABLE = "82104";
    public static final String GLOBAL_CURSOR_NOT_FOUND = "82105";
    public static final String INVALID_ORACLE_CURSOR_NUMBER = "82106";
    public static final String PROGRAM_TOO_OLD = "82107";
    public static final String INVALID_DESCRIPTOR = "82108";
    public static final String HOST_REFERENCE_OUT_OF_RANGE = "82109";
    public static final String INVALID_HOST_CACHE_TYPE_ENTRY = "82110";
    public static final String HEAP_CONSISTENCY_ERROR = "82111";
    public static final String UNABLE_TO_OPEN_MESSAGE_FILE = "82112";
    public static final String INTERNAL_CONSISTENCY_FAILED = "82113";
    public static final String INVALID_CONTEXT_REENTRANT_CODE = "82114";
    public static final String INVALID_HSTDEF_ARGUMENT = "82115";
    public static final String SQLRCN_ARGUMENTS_NULL = "82116";
    public static final String INVALID_OPEN_OR_PREPARE = "82117";
    public static final String APPLICATION_CONTEXT_NOT_FOUND = "82118";
    public static final String CONNECT_ERROR_NO_ERROR_TEXT = "82119";
    public static final String SQLLIB_VERSION_MISMATCH = "82120";
    public static final String ODD_FETCHED_NUMBER_OF_BYTES = "82121";
    public static final String EXEC_TOOLS_NOT_AVAILABLE = "82122";
    public static final String REMOTE_DATABASE_ACCESS = "HZ000";
    public static final String DEBUG_EVENTS = "90000";
    public static final String OTHERS = "99999";

    private static HashMap messages = new HashMap(120, 1f);

    static {
        messages.put(SUCCESS, "Successful completion");
        messages.put(WARNING, "Warning");
        messages.put(CURSOR_CONFLICT, "Cursor operation conflict");
        messages.put(DISCONNECT_ERROR, "Disconnect error ");
        messages.put(NULL_VALUE_ELIMINATED, "Null value eliminated in set function");
        messages.put(STRING_TRUNCATION, "String data - right truncation");
        messages.put(INSUFFICIENT_ITEM_DESC, "Insufficient item descriptor areas");
        messages.put(PRIVILEGED_NOT_REVOKED, "Privilege not revoked ");
        messages.put(PRIVILEGED_NOT_GRANTED, "Privilege not granted");
        messages.put(IMPLICIT_ZERO_BIT_PADDING, "Implicit zero-bit padding");
        messages.put(SEARCH_CONDITION_TOO_LONG, "Search condition too long for info schema");
        messages.put(QUERY_EXPRESSION_TOO_LONG, "Query expression too long for info schema");
        messages.put(NO_DATA, "No data");
        messages.put(DYNAMIC_SQL_ERROR, "Dynamic SQL error");
        messages.put(USING_CLAUSE_INVALID_PARAM, "Using clause does not match parameter specs");
        messages.put(USING_CLAUSE_INVALID_TARGET, "Using clause does not match target specs");
        messages.put(CANNOT_EXECUTE_CURSOR_SPEC, "Cursor specification cannot be executed");
        messages.put(USING_CLAUSE_REQUIRED_4_DYNAMIC, "Using clause required for dynamic parameters");
        messages.put(PREPARED_STATEMENT_INVALID, "Prepared statement not a cursor specification");
        messages.put(DATATYPE_VIOLATION, "Restricted datatype attribute violation");
        messages.put(USING_CLAUSE_REQUIRED_4_RESULT, "Using clause required for result fields");
        messages.put(INVALID_DESCRIPTOR_COUNT, "Invalid descriptor count");
        messages.put(INVALID_DESCRIPTOR_INDEX, "Invalid descriptor index");
        messages.put(CONNECTION_EXCEPTION, "Connection exception");
        messages.put(SQL_CLIENT_CANNOT_CONNECT, "SQL client unable to establish SQL connection");
        messages.put(CONNECTION_NAME_IN_USE, "Connection name in use");
        messages.put(CONNECTION_DOES_NOT_EXIST, "Connection does not exist");
        messages.put(SQL_SERVER_REJECT_CONNECTION, "SQL server rejected SQL connection");
        messages.put(CONNECTION_FAILURE, "Connection failure");
        messages.put(TRANSACTION_RESOLUTION_UNKNOWN, "Transaction resolution unknown");
        messages.put(FEATURE_NOT_SUPPORTED, "Feature not supported");
        messages.put(MULTIPLE_SERVER_TRANSACTIONS, "Multiple server transactions");
        messages.put(CARDINALITY_VIOLATION, "Cardinality violation");
        messages.put(DATA_EXCEPTION, "Data exception ");
        messages.put(DATA_TRUNCATION2, "String data - right truncation");
        messages.put(NULL_VALUE, "Null value - no indicator parameter ");
        messages.put(NUMERIC_OUT_OF_RANGE, "Numeric value out of range");
        messages.put(ERROR_IN_ASSIGNMENT, "Error in assignment");
        messages.put(INVALID_DATE_TIME_FORMAT, "Invalid date-time format ");
        messages.put(DATE_TIME_FIELD_OVERFLOW, "Date-time field overflow");
        messages.put(INVALID_TIME_ZONE_DISPLACEMENT, "Invalid time zone displacement value");
        messages.put(SUBSTRING_ERROR, "Substring error");
        messages.put(DIVISION_BY_ZERO, "Division by zero");
        messages.put(INTERVAL_FIELD_OVERFLOW, "Interval field overflow");
        messages.put(INVALID_CHARACTER_VALUE, "Invalid character value for cast");
        messages.put(INVALID_ESCAPE_CHARACTER, "Invalid escape character");
        messages.put(CHARACTER_NOT_IN_REPERTOIRE, "Character not in repertoire");
        messages.put(INDICATOR_OVERFLOW, "Indicator overflow ");
        messages.put(INVALID_PARAMETER_VALUE, "Invalid parameter value");
        messages.put(UNTERMINATED_C_STRING, "Unterminated C string");
        messages.put(INVALID_ESCAPE_SEQUENCE, "Invalid escape sequence");
        messages.put(STRING_DATA_LENGTH_MISMATCH, "String data - length mismatch ");
        messages.put(TRIM_ERROR, "Trim error");
        messages.put(INTEGRITY_CONSTRAINT_VIOLATION, "Integrity constraint violation");
        messages.put(INVALID_CURSOR_STATE, "Invalid cursor state");
        messages.put(INVALID_TRANSACTION_STATE, "Invalid transaction state");
        messages.put(INVALID_SQL_STATEMENT_NAME, "Invalid SQL statement name");
        messages.put(TRIGGER_DATA_VIOLATION, "Triggered data change violation");
        messages.put(INVALID_AUTHORIZATION, "Invalid authorization specification");
        messages.put(SQL_SYNTAX_ERROR_DIRECT, "Direct SQL syntax error or access rule violation");
        messages.put(DEPENDENT_PRIVILEGE_EXIST, "Dependent privilege descriptors still exist");
        messages.put(INVALID_CHARACTER_SET_NAME, "Invalid character set name");
        messages.put(INVALID_TRANSACTION_TERMINATION, "Invalid transaction termination");
        messages.put(INVALID_CONNECTION_NAME, "Invalid connection name ");
        messages.put(INVALID_SQL_DESCRIPTOR, "Invalid SQL descriptor name");
        messages.put(INVALID_CURSOR_NAME, "Invalid cursor name");
        messages.put(INVALID_CONDITION_NUMBER, "Invalid condition number");
        messages.put(SQL_SYNTAX_ERROR_DYNAMIC, "Dynamic SQL syntax error or access rule violation");
        messages.put(AMBIGUOUS_CURSOR_NAME, "Ambiguous cursor name");
        messages.put(INVALID_CATALOG_NAME, "Invalid catalog name ");
        messages.put(INVALID_SCHEMA_NAME, "Invalid schema name");
        messages.put(TRANSACTION_ROLLBACK, "Transaction rollback");
        messages.put(SERIALIZATION_FAILURE, "Serialization failure");
        messages.put(INTEGRITY_CONSTRAINT_VIOLATION2, "Integrity constraint violation");
        messages.put(STATEMENT_COMPLETION_UNKNOWN, "Statement completion unknown ");
        messages.put(SYNTAX_ERROR_OR_VIOLATION, "Syntax error or access rule violation");
        messages.put(WITH_CHECK_OPTION_VIOLATION, "With check option violation");
        messages.put(SYSTEM_ERROR, "System errors");
        messages.put(RESOURCE_ERROR, "Resource error");
        messages.put(MULTI_THREAD_OR_DETACH_ERROR, "Multi-threaded server and detached process errors");
        messages.put(ORACLE_XA_TWO_TASK_ERROR, "Oracle*XA and two-task interface errors");
        messages.put(CONTROL_DATABASE_REDO_FILE_ERROR, "Control file, database file, and redo file errors archival and media recovery errors");
        messages.put(PL_SQL_ERROR, "PL/SQL errors");
        messages.put(SQL_NET_DRIVER_ERROR, "SQL*Net driver errors");
        messages.put(LICENSING_ERROR, "Licensing errors");
        messages.put(SQL_CONNECT_ERROR, "SQL*Connect errors");
        messages.put(SQL_EXECUTE_PHASE_ERROR, "SQL execute phase errors");
        messages.put(OUT_OF_MEMORY, "Out of memory (could not allocate)");
        messages.put(CURSOR_MISMATCH, "Inconsistent cursor cache: unit cursor/global cursor mismatch");
        messages.put(NO_GLOBAL_CURSOR_ENTRY, "Inconsistent cursor cache: no global cursor entry");
        messages.put(CURSOR_CACHE_OUT_OF_RANGE, "Inconsistent cursor cache: out of range cursor cache reference");
        messages.put(NO_CURSOR_CACHE_AVAILABLE, "Inconsistent host cache: no cursor cache available");
        messages.put(GLOBAL_CURSOR_NOT_FOUND, "Inconsistent cursor cache: global cursor not found");
        messages.put(INVALID_ORACLE_CURSOR_NUMBER, "Inconsistent cursor cache: invalid Oracle cursor number");
        messages.put(PROGRAM_TOO_OLD, "Program too old for runtime library");
        messages.put(INVALID_DESCRIPTOR, "Invalid descriptor passed to runtime library");
        messages.put(HOST_REFERENCE_OUT_OF_RANGE, "Inconsistent host cache: host reference is out of range");
        messages.put(INVALID_HOST_CACHE_TYPE_ENTRY, "Inconsistent host cache: invalid host cache entry type");
        messages.put(HEAP_CONSISTENCY_ERROR, "Heap consistency error");
        messages.put(UNABLE_TO_OPEN_MESSAGE_FILE, "Unable to open message file ");
        messages.put(INTERNAL_CONSISTENCY_FAILED, "Code generation internal consistency failed");
        messages.put(INVALID_CONTEXT_REENTRANT_CODE, "Reentrant code generator gave invalid context");
        messages.put(INVALID_HSTDEF_ARGUMENT, "Invalid hstdef argument");
        messages.put(SQLRCN_ARGUMENTS_NULL, "First and second arguments to sqlrcn both null");
        messages.put(INVALID_OPEN_OR_PREPARE, "Invalid OPEN or PREPARE for this connection");
        messages.put(APPLICATION_CONTEXT_NOT_FOUND, "Application context not found");
        messages.put(CONNECT_ERROR_NO_ERROR_TEXT, "Connect error; can't get error text");
        messages.put(SQLLIB_VERSION_MISMATCH, "Precompiler/SQLLIB version mismatch");
        messages.put(ODD_FETCHED_NUMBER_OF_BYTES, "FETCHed number of bytes is odd");
        messages.put(EXEC_TOOLS_NOT_AVAILABLE, "EXEC TOOLS interface is not available");
        messages.put(REMOTE_DATABASE_ACCESS, "Remote database access");
        messages.put(DEBUG_EVENTS, "Debug events");
        messages.put(OTHERS, "unknown errors");
    }

    private String sqlState;
    private String message;

    private SQLStateError() {
    }

    public static SQLStateError get(String sqlState) {
        SQLStateError result = new SQLStateError();
        String msg = (String) messages.get(sqlState);
        if (msg == null)
            msg = (String) messages.get(OTHERS);
        result.message = msg;
        result.sqlState = sqlState;
        return result;
    }

    public String getSqlState() {
        return sqlState;
    }

    public String getMessage() {
        return message;
    }

    public String toString() {
        return "SQLState=" + this.sqlState + "; Msg=" + this.message;
    }
}
