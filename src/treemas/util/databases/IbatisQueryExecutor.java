package treemas.util.databases;

import treemas.util.auditrail.AuditInfo;
import treemas.util.auditrail.AuditTrailException;
import treemas.util.auditrail.Auditable;
import treemas.util.paging.PageListWrapper;

import java.sql.SQLException;
import java.util.List;

public interface IbatisQueryExecutor {

    public static final int AUDIT_ADD = 23;
    public static final int AUDIT_EDIT = 24;
    public static final int AUDIT_DELETE = 25;

    public List queryList(String queryId, Object parameter) throws SQLException;

    public PageListWrapper queryListForPage(String queryId,
                                            String countQueryId, Object parameter) throws SQLException;

    public Object querySingleObject(String queryId, Object parameter)
            throws SQLException;

    public int update(String queryId, Object parameter) throws SQLException;

    public int[] update(String[] queryIds, Object[] parameters)
            throws SQLException;

    public int updateWithAudit(String queryId, Auditable parameter,
                               int auditType, String loginId, String screenId)
            throws SQLException, AuditTrailException;

    public int[] updateWithAudit(String[] queryIds, Auditable[] parameters,
                                 int[] auditType, String loginId, String screenId)
            throws SQLException, AuditTrailException;

    public int updateWithAudit(String queryId, Object parameter,
                               AuditInfo auditInfo, int auditType, String loginId, String screenId)
            throws SQLException, AuditTrailException;

    public int[] updateWithAudit(String[] queryIds, Object[] parameters,
                                 AuditInfo[] auditInfo, int[] auditType, String loginId,
                                 String screenId) throws SQLException, AuditTrailException;
}
