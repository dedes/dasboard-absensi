package treemas.util.databases;

public class SQLErrorCode {

    public static final int ERROR_DUPLICATE_KEY = 2627;
    public static final int ERROR_CHILD_FOUND = 547;

    private SQLErrorCode() {
    }
}
