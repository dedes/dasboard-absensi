package treemas.util.ldap;

import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;
import treemas.util.constant.PropertiesKey;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;
import treemas.util.tool.ConfigurationProperties;

public class ActiveDirectory {
    private static ActiveDirectory instance = new ActiveDirectory();
    private ILogger logger = LogManager.getDefaultLogger();
    private String ldapHost;
    private int ldapPort;
    private String domainName;

    private ActiveDirectory() {
        this.ldapHost = ConfigurationProperties.getInstance().get(
                PropertiesKey.LDAP_HOST);
        this.domainName = ConfigurationProperties.getInstance().get(
                PropertiesKey.LDAP_DOMAIN);

        String ldapPort = ConfigurationProperties.getInstance().get(
                PropertiesKey.LDAP_PORT);
        this.ldapPort = (ldapPort == null) ? LDAPConnection.DEFAULT_PORT :
                Integer.parseInt(ldapPort);
    }

    public static ActiveDirectory getInstance() {
        return instance;
    }

    public boolean authenticate(String user, String password) throws LDAPException {
        boolean isValid = false;

        if (password == null || "".equals(password))
            return false;

        LDAPConnection conn = new LDAPConnection();
        try {
            try {
                System.out.println(domainName + "\\" + user + "," + password);
                conn.connect(ldapHost, ldapPort);
                conn.authenticate(domainName + "\\" + user, password);
                isValid = true;
            } finally {
                conn.disconnect();
            }
        } catch (LDAPException ex) {
            if (ex.getLDAPResultCode() == LDAPException.INVALID_CREDENTIALS) {
                isValid = false;
            } else {
                this.logger.printStackTrace(ex);
                throw ex;
            }
        }
        return isValid;
    }
}