package treemas.util.http;

public abstract class HTTPConstant {

    public static String HTML_START = "<html>";
    public static String HTML_END = "</html>";
    public static String HEAD_START = "<head>";
    public static String HEAD_END = "</head>";
    public static String BODY_START = "<body>";
    public static String BODY_END = "</body>";

    public static String DOC_TYPE = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";

    public static String TITLE = "<title>#TITLE#</title>";
    public static String SCRIPT_START = "<script language=\"javascript\">";
    public static String SCRIPT_END = "</script>";
    public static String SCRIPT_BASIC = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">"
            + "<meta http-equiv=\"Expires\" content=\"0\">"
            + "<meta http-equiv=\"Pragma\" content=\"no-cache\">"
            + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />"
            + "<link href=\"../../style/style.css\" rel=\"stylesheet\" type=\"text/css\">"
            + "<link rel=\"stylesheet\" href=\"../../style/jquery-ui.css\" type=\"text/css\">"
            + "<script language=\"javascript\" SRC=\"../../javascript/global/jquery-1.10.2.js\"></script>"
            + "<script language=\"javascript\" SRC=\"../../javascript/global/jquery-all.js\"></script>"
            + "<script language=\"javascript\" SRC=\"../../javascript/global/global.js\"></script>"
            + "<script language=\"javascript\" SRC=\"../../javascript/global/disable.js\"></script>"
            + "<script language=\"javascript\" SRC=\"../../javascript/global/js_validator.js\"></script>";
    public static String DIV_CENTER_START = "<div align=\"center\">";
    public static String DIV_END = "</div>";
    public static String TITLE_HEAD = ""
            + " 	<table width=\"95%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">"
            + " 		<tbody><tr class=\"Top-Table-Header\">"
            + " 			<td class=\"Top-Table-Header-Left\"></td>"
            + " 			<td class=\"Top-Table-Header-Center\">"
            + "             #TITLE#"
            + " 			</td>"
            + " 			<td class=\"Top-Table-Header-Right\"></td>"
            + "		</tr></tbody>"
            + "	</table>";
}
