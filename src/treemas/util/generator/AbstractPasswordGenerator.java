package treemas.util.generator;

import treemas.util.AppException;
import treemas.util.log.ILogger;
import treemas.util.log.LogManager;
import treemas.util.validator.ValidatorProperties;

import java.sql.SQLException;

public abstract class AbstractPasswordGenerator {
    public static final int PASSWORD_ERROR_MIN_LENGTH = 80100;
    protected ILogger logger = LogManager.getDefaultLogger();
    protected boolean validateMinimumLength = true;
    protected boolean validateHistory = true;
    protected int minimumLength = ValidatorProperties.MIN_PASSWORD;
    protected int passwordHistory = ValidatorProperties.MIN_PASSWORD;

    public AbstractPasswordGenerator() {
    }

    public AbstractPasswordGenerator(int minimumLength, int passwordHistory) {
        if (minimumLength > 0) {
            this.validateMinimumLength = true;
            this.minimumLength = minimumLength;
        } else {
            this.validateMinimumLength = false;
        }

        if (passwordHistory > 0) {
            this.validateHistory = true;
            this.passwordHistory = passwordHistory;
        } else {
            this.validateHistory = false;
        }
    }

    public boolean isValidateMinimumLength() {
        return validateMinimumLength;
    }

    public void setValidateMinimumLength(boolean validateMinimumLength) {
        this.validateMinimumLength = validateMinimumLength;
    }

    public boolean isValidateHistory() {
        return validateHistory;
    }

    public void setValidateHistory(boolean validateHistory) {
        this.validateHistory = validateHistory;
    }

    public int getMinimumLength() {
        return minimumLength;
    }

    public void setMinimumLength(int minimumLength) {
        this.minimumLength = minimumLength;
    }

    public int getPasswordHistory() {
        return passwordHistory;
    }

    public void setPasswordHistory(int passwordHistory) {
        this.passwordHistory = passwordHistory;
    }

    public abstract String hashPassword(String string);

    public abstract boolean checkPasswordValidity(String userId, String string)
            throws SQLException, AppException;

    public boolean validateMinimumLength(String string) {
        if (string == null)
            return false;

        if (string.length() < this.minimumLength)
            return false;
        else
            return true;
    }

    public abstract boolean validatePasswordHistory(String userId,
                                                    String password) throws SQLException;

    public abstract void insertPasswordHistory(String userId, String password,
                                               int type, String usrUpd, String oldPass) throws SQLException;
}
