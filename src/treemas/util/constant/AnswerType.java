package treemas.util.constant;

public abstract class AnswerType {
    public static final String TEXT = "001";
    public static final String RESPONDEN = "019";

    public static final String MULTIPLE = "002";
    public static final String MULTIPLE_ONE_DESCRIPTION = "018";
    public static final String MULTIPLE_WITH_DESCRIPTION = "003";
    public static final String MULTIPLE_LOOKUP_WITH_FILTER = "026";

    public static final String LOOKUP = "015";
    public static final String LOOKUP_WITH_FILTER = "016";

    public static final String RADIO = "004";
    public static final String RADIO_WITH_DESCRIPTION = "005";
    public static final String DROPDOWN = "006";
    public static final String DROPDOWN_WITH_DESCRIPTION = "007";

    public static final String NUMERIC = "008";
    public static final String DECIMAL = "009";
    public static final String ACCOUNTING = "028";
    public static final String NUMERIC_WITH_CALCULATION = "027";

    public static final String DATE = "010";
    public static final String TIME = "011";
    public static final String DATE_TIME = "012";

    public static final String DRAWING = "017";
    public static final String SIGNATURE = "029";
    public static final String IMAGE = "013";
    public static final String IMAGE_WITH_TIME = "021";
    public static final String IMAGE_WITH_GEODATA_GPS = "020";
    public static final String IMAGE_WITH_GPS_TIME = "023";
    public static final String IMAGE_WITH_GEODATA = "014";
    public static final String IMAGE_WITH_GEODATA_TIME = "022";
    public static final String GPS_TIME = "025";
    public static final String GEODATA_TIME = "024";

    /*TODO
     *
     * COMMIT THIS
     * */
    public static final String TRESHOLD = "030";

}
