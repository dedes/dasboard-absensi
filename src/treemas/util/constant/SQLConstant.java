package treemas.util.constant;

/**
 * SQL MAPPING IBATIS FILE NAME
 */
public class SQLConstant {
    public static final String SQL_GENERAL_PARAMETER = "appadmin.generalparameter.";
    public static final String SQL_AUDIT = "security.audit.";
    public static final String SQL_CHANGE_PASSWORD = "appadmin.password.";
    public static final String SQL_MULTIPLE = "multiple.";
    public static final String SQL_LOGIN = "appadmin.login.";
    public static final String SQL_MENU = "directory.";
    public static final String SQL_REPORT = "report.";
    public static final String SQL_USER = "appadmin.user.";
    public static final String SQL_PRIVILEGE = "appadmin.privilege.";
    public static final String SQL_NOTIFICATION = "notification.";
    public static final String SQL_APP_FORM = "appadmin.appform.";
    public static final String SQL_APPADMIN = "appadmin.application.";
    public static final String SQL_APPMENU = "appadmin.appmenu.";
    public static final String SQL_USERDETAIL = "appadmin.userdetail.";
    public static final String SQL_MANAGEMENTHANDSET = "appadmin.managementhandset.";

    public static final String SQL_LOOKUP_REGION = "lookup.region.";
    public static final String SQL_LOOKUP_CITY = "lookup.city.";
    public static final String SQL_LOOKUP_PRIVILEGE = "lookup.privilege.";
    public static final String SQL_LOOKUP_KARYAWAN = "lookup.karyawan.";
    public static final String SQL_LOOKUP_USER = "lookup.user.";

    public static final String SQL_FEATURE = "feature.";// "feature.common."
    public static final String SQL_FEATURE_COMMON = "feature.common.";//combo
    public static final String SQL_FEATURE_GEOCODE = "feature.geocode.";
    public static final String SQL_FEATURE_REPORT = "feature.report.";
    public static final String SQL_FEATURE_BRANCH = "feature.branch.";
    public static final String SQL_FEATURE_SCHEME = "feature.scheme.";
    public static final String SQL_FEATURE_SCHEME_GROUP = "feature.scheme.group.";
    public static final String SQL_FEATURE_GROUP_TEMPLATE = "feature.template.group.";
    public static final String SQL_FEATURE_GROUP_TEMPLATE_UNDISTRIBUTE = "feature.group.template.undistribute.";
    public static final String SQL_FEATURE_SEQ = "feature.sequence.";
    public static final String SQL_FEATURE_PRINT_ITEM = "feature.printitem.";
    public static final String SQL_FEATURE_QUESTION = "feature.question.";
    public static final String SQL_FEATURE_QUESTION_GROUP = "feature.question.group.";
    public static final String SQL_FEATURE_QUESTION_RELEVANT = "feature.question.relevant.";
    public static final String SQL_FEATURE_ASSIGNMENT = "feature.assignment.";
    public static final String SQL_FEATURE_PRETASK = "feature.pretask.";
    public static final String SQL_FEATURE_ASSIGNMENT_UPLOAD = "feature.assignmentupload.";
    public static final String SQL_FEATURE_ANSWER_ASSIGNMENT = "feature.answer.assignment.";
    public static final String SQL_FEATURE_REASSIGNMENT = "feature.reassignment.";
    public static final String SQL_FEATURE_UNDISTRIBUTED = "feature.undistributed.";
    public static final String SQL_FEATURE_APPROVAL_ASSIGNMENT = "feature.approval.assignment.";
    public static final String SQL_FEATURE_INPUT_ANSWER_ASSIGNMENT = "feature.input.anwser.assignment.";

    public static final String SQL_FEATURE_JABATAN = "feature.jabatan.";
    public static final String SQL_FEATURE_KARYAWAN = "feature.karyawan.";
    public static final String SQL_FEATURE_REIMBURSE = "feature.reimburse.";
    public static final String SQL_FEATURE_REIMBURSEKR = "feature.reimbursekr.";
    public static final String SQL_FEATURE_REIMBURSE_APP = "feature.reimburseapp.";
    public static final String SQL_FEATURE_TIMESHEET = "feature.timesheet.";
    public static final String SQL_FEATURE_TIMESHEETKR = "feature.timesheetkr.";
    public static final String SQL_FEATURE_LEADER = "feature.leader.";
    public static final String SQL_FEATURE_SPOOFING = "feature.spoofing.";
    public static final String SQL_FEATURE_USER_MEMBER = "feature.usermember.";
    public static final String SQL_FEATURE_USER_APP = "feature.userapp.";
    public static final String SQL_FEATURE_ABSEN = "feature.absen.";
    public static final String SQL_FEATURE_INPABSEN = "feature.inpabsen.";
    public static final String SQL_FEATURE_CEKABSEN = "feature.cekabsen.";
    public static final String SQL_FEATURE_ABSEN_APP = "feature.absenapp.";
    public static final String SQL_FEATURE_CUTI = "feature.cuti.";
    public static final String SQL_FEATURE_CUTIKR = "feature.cutikr.";
    public static final String SQL_FEATURE_CUTI_APP = "feature.cutiapp.";
    public static final String SQL_FEATURE_LIBUR = "feature.libur.";
    public static final String SQL_FEATURE_PROJECT = "feature.project.";
    public static final String SQL_FEATURE_PARAM_REIMBURSE = "feature.paramReimburse.";
    
    public static final String SQL_FEATURE_INQ_ASSIGNMENT = "feature.inquiry.assignment.";// "mss.inquiry.assignment.
    public static final String SQL_FEATURE_INQ_HEAT_MAP = "feature.inquiry.heatmap.";// "mss.inquiry.surveyHeatMap.
    public static final String SQL_FEATURE_INQ_LOCATION = "feature.inquiry.locationhistory.";// "mss.inquiry.locationHistory.
    public static final String SQL_FEATURE_INQ_MOBILE_LOG = "feature.inquiry.mobilelog.";// "mss.inquiry.mobileactivitylog.
    public static final String SQL_FEATURE_INQ_USER_LOG = "feature.inquiry.userlog.";
    public static final String SQL_FEATURE_INQ_PERSON_LOCATION = "feature.inquiry.personlocation.";// "mss.inquiry.allSurveyorLocation.
    public static final String SQL_FEATURE_INQ_TASK_MONITORING = "feature.inquiry.taskmonitoring.";

    /*
     * VIEW IBATIS
     * */
    public static final String SQL_FEATURE_VIEW_ASSIGNMENT = "feature.view.assignment.";
    public static final String SQL_FEATURE_VIEW_ASSIGNMENT_SURVEYOR = "feature.view.assignment.surveyor.";// mss.view.assignmentsurveyor.
    public static final String SQL_FEATURE_VIEW_QUESTIONS = "feature.view.questions.";// mss.view.questionset.

    /*
     * LOOK UP IBATIS
     * */
    public static final String SQL_FEATURE_LU_BRANCH = "feature.lookup.branch.";// "mss.lookup.branch.
    public static final String SQL_FEATURE_LU_DYNAMIC = "feature.lookup.dynamic.";// "mss.lookup.lookupdinamic.
    public static final String SQL_FEATURE_LU_OPTION_ANSWER = "feature.lookup.optionanswer.";// "mss.lookup.optionanswer.
    public static final String SQL_FEATURE_LU_QUESTION = "feature.lookup.question.";// "mss.lookup.question.
    public static final String SQL_FEATURE_LU_QUESTION_BY_GROUP = "feature.lookup.questionbygroup.";// "mss.lookup.questionbygroup.
    public static final String SQL_FEATURE_LU_QUESTION_GROUP = "feature.lookup.questiongroup.";// "mss.lookup.questiongroup.
    public static final String SQL_FEATURE_LU_QUESTIONS = "feature.lookup.questions.";// "mss.lookup.questionset.
    public static final String SQL_FEATURE_LU_USER = "feature.lookup.user.";// "mss.lookup.surveyor.


    public static final String SQL_AJAX_USER_LOCATION = "";//mss.ajax.allSurveyorLocation
    public static final String SQL_AJAX_QUESTION = "";//mss.ajax.setting.question
    public static final String SQL_AJAX_SUBMIT_TASK = "";//mss.ajax.submittask
    public static final String SQL_AJAX_TRACKING = "";//mss.ajax.tracking

    /*
     * HANDSET IBATIS
     * */
    public static final String SQL_HANDSET_MANAGER = "handset.sql.";// handset


    //	DAWAM ENHANCE 2017
    public static final String SQL_FEATURE_LINK = "feature.link.";
    public static final String SQL_FEATURE_MOBILEREPORT = "feature.mobilereport.";
    
    /*
     * MOBILE APPROVAL
     * */
    
    public static final String SQL_MOBAPP_USER = "feature.mobileapproval.usermobile.";
    public static final String SQL_APPROVAL = "feature.mobileapproval.";
    public static final String SQL_APPROVALNOTE = "feature.mobileapprovalnote.";
    public static final String SQL_APPROVALPATH = "feature.approval.path.";
    public static final String SQL_APPROVALTYPE = "feature.approval.type.";
    public static final String SQL_AUDITRAIL = "feature.auditrailmobile.";
    public static final String SQL_AUDITRAILTASK = "feature.mobileapproval.auditrailTaskmobile.";
    public static final String SQL_DETAIL = "feature.mobileapproval.detailmobile.";
    public static final String SQL_HISTORY = "feature.historymobile.";
    public static final String SQL_LOGINN = "feature.loginn.";
    
}
