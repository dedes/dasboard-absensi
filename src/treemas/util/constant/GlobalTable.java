package treemas.util.constant;

public abstract class GlobalTable {
    public static final String TABLE_NAME_AUDITTABLE = "AUDITTABLE";
    public static final String TABLE_NAME_USER = "SYS_USER";
    public static final String TABLE_NAME_GENERALPARAM = "GENERALPARAM";
    public static final String TABLE_NAME_SYS_APPADMIN = "SYS_APPADMIN";
    public static final String TABLE_NAME_SYS_APPFEATUREFORM = "SYS_APPFEATUREFORM";
    public static final String TABLE_NAME_SYS_APPGROUPMEMBER = "SYS_APPGROUPMEMBER";
    public static final String TABLE_NAME_SYS_APPGROUP = "SYS_APPGROUP";
    public static final String TABLE_NAME_SYS_APPGROUPMENU = "SYS_APPGROUPMENU";
    public static final String TABLE_NAME_SYS_APPGROUPFEATURE = "SYS_APPGROUPFEATURE";

    public static final String TABLE_NAME_FEATURE_BRANCH = "TBL_BRANCH";
    public static final String TABLE_NAME_FEATURE_BRANCH_POLYGON = "TBL_BRANCH_POLYGON";
    public static final String TABLE_NAME_FEATURE_SCHEME = "FEAT_SCHEME";
    public static final String TABLE_NAME_FEATURE_SCHEME_GROUP = "FEAT_SCHEME_GROUP";
    public static final String TABLE_NAME_FEATURE_GROUP_TEMPLATE = "FEAT_GROUP_TEMPLATE";
    public static final String TABLE_NAME_FEATURE_GROUP_TEMPLATE_UNDISTRIBUTE = "FEAT_GROUP_TEMPLATE_UNDISTRIBUTED";
    public static final String TABLE_NAME_FEATURE_PRINT_ITEM = "FEAT_PRINT_ITEM";
    public static final String TABLE_NAME_FEATURE_QUESTION = "FEAT_QUESTION";
    public static final String TABLE_NAME_FEATURE_QUESTION_GROUP = "FEAT_QUESTION_GROUP";
    public static final String TABLE_NAME_FEATURE_QUESTION_RELEVANT = "FEAT_QUESTION_RELEVANT";
    public static final String TABLE_NAME_FEATURE_QUESTION_OPTIONS = "FEAT_QUESTION_ANSWER";

    public static final String TABLE_ID_AUDITTABLE = "TABLEID";
    public static final String TABLE_FIELD_AUDITTABLE = "TABLENAME";
    public static final String TABLE_FIELD_AUDIT_DELETE_GROUP_MEMBER = "GROUPID";


    public static final String TABLE_COMMON_ACTIVE = "ACTIVE";
}
