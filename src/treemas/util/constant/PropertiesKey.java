package treemas.util.constant;

public abstract class PropertiesKey extends
        treemas.util.properties.PropertiesKey {

    public static final String DASHBOARD_EJB_URL = "dashboard.url";
    public static final String DASHBOARD_EJB_USER = "dashboard.user";
    public static final String DASHBOARD_EJB_PWD = "dashboard.password";
    public static final String DASHBOARD_EJB_JNDI = "dashboard.jndiname";
    public static final String DATABASE_DRIVER = "db.driver";
    public static final String DATABASE_URL = "db.url";
    public static final String DATABASE_USER = "db.user";
    public static final String DATABASE_PASSWORD = "db.password";
    public static final String MAIL_TEMPLATE_PATH = "mail.template.path";
    public static final String MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";
    public static final String MAIL_HOST = "mail.host";
    public static final String MAIL_SENDER_EMAIL = "mail.sender.email";
    public static final String MAIL_SENDER_NAME = "mail.sender.name";
    public static final String MAIL_SENDER_PASSWORD = "mail.sender.password";
    public static final String MAIL_SUBJECT = "mail.subject";
    public static final String MAIL_APPROVAL_NOTIFICATION = "mail.approvalnotification";
    public static final String MAIL_PORT = "mail.smtp.port";
    public static final String MAIL_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
    public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    public static final String TRACKING_ON = "tracking.on";
    public static final String TRACKING_INTERVAL = "tracking.interval";
    public static final String ANSWER_IMAGE_PATH = "answer.imagepath";
    public static final String ANSWER_DATE_FORMAT = "answer.dateformat";
    public static final String ANSWER_TIME_FORMAT = "answer.timeformat";
    public static final String ANSWER_DATETIME_FORMAT = "answer.datetimeformat";
    public static final String MONITORING_LOOKUP_ADRRESS = "monitoring.lookupaddress";
    public static final String USER_SERVICE_ENABLE = "userservice.enable";
    public static final String USER_SERVICE_URL = "userservice.url";
    public static final String USER_SERVICE_SECRET = "userservice.secret";
    public static final String LDAP_HOST = "ldap.host";
    public static final String LDAP_PORT = "ldap.port";
    public static final String LDAP_DOMAIN = "ldap.domain";
    public static final String LDAP_ENABLE = "ldap.enable";
    public static final String ENCRYPT_ENABLE = "bounty.en.enable";
    public static final String DECRYPT_ENBALE = "bounty.de.enable";
    public static final String IMAGE_DATABASE = "image.database";
    public static final String NC_ViewAssignment_URL = "view.answer.url";
    public static final String NC_BranchID = "nc.branchid";
    public static final String NC_DefaultPassowrd = "nc.password";
    public static final String WEBSERVICE_NC_URL = "ws.nc.url";
    public static final String API_KEY = "android.apikey";
    public static final String IMAGE_KARYAWAN_PATH = "karyawan.imagepath";
    public static final String IMAGE_KTP_PATH = "ktp.imagepath";
    public static final String IMAGE_NPWP_PATH = "npwp.imagepath";
    public static final String IMAGE_KK_PATH = "kk.imagepath";
    public static final String IMAGE_MI_PATH = "mi.imagepath";
    public static final String WEB_KEY = "web.apikey";
    public static final String WEB_STATIC_KEY = "web.static.apikey";

    private PropertiesKey() {
    }
}