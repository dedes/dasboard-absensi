package treemas.util.ejb.db;

import treemas.util.paging.PageListWrapper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface IDataAccess {

    /**
     * Query for list of data
     *
     * @param queryId   String
     * @param parameter Object
     * @return List
     * @throws SQLException
     */
    public List queryList(String queryId, Object parameter) throws SQLException;

    /**
     * Query for list of data.
     *
     * @param queryId         String Id of query
     * @param queryIdForCount String Id of query that will give total records
     * @param parameter       Object
     * @return PageListWrapper
     * @throws SQLException
     */
    public PageListWrapper queryListForPage(String queryId, String queryIdForCount, Object parameter) throws
            SQLException;

    /**
     * Query for one single object
     *
     * @param queryId   String
     * @param parameter Object
     * @return Object
     * @throws SQLException
     */
    public Object querySingleObject(String queryId, Object parameter) throws SQLException;

    /**
     * Executes update/insert SQL statement
     *
     * @param queryId   String
     * @param parameter Object
     * @return int
     * @throws SQLException
     */
    public int update(String queryId, Object parameter) throws SQLException;

    /**
     * Executes multiple update/insert of SQL statements. Implementation of this interface shall
     * ensures integrity by using transaction internally
     *
     * @param queryIds   String[]
     * @param parameters Object[]
     * @return int[]
     * @throws SQLException
     */
    public int[] update(String[] queryIds, Object[] parameters) throws SQLException;

    /**
     * Starts jdbc transaction. When this method is called, implementation of this interface
     * is not guaranteed to be thread-safe
     *
     * @throws SQLException
     */
    public void startTransaction() throws SQLException;

    /**
     * Commits jdbc transaction. When this method is called, implementation of this interface
     * is not guaranteed to be thread-safe
     *
     * @throws SQLException
     */
    public void commitTransaction() throws SQLException;

    /**
     * Rollbacks jdbc transaction. When this method is called, implementation of this interface
     * is not guaranteed to be thread-safe
     *
     * @throws SQLException
     */
    public void rollbackTransaction() throws SQLException;


    /**
     * Gets current connection (if transaction is active).
     * If startTransaction has not been called, returns null
     *
     * @return Connection
     * @throws SQLException
     */
    public Connection getCurrentConnection() throws SQLException;
}
