package treemas.util.ejb.db;

public interface ICloneable extends Cloneable {
    public Object clone();
}
