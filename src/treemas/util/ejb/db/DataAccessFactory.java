package treemas.util.ejb.db;

import treemas.util.properties.ImplementProperties;
import treemas.util.properties.PropertiesKey;

public final class DataAccessFactory {
    private static IDataAccess data_access_manager = null;
    private static ICloneable cloneable = null;
    private static Class impl_class = null;

    static {
        ImplementProperties props = ImplementProperties.getInstance();
        String className = props.get(PropertiesKey.IMPL_DATA_ACCESS);
        if (className != null) {
            try {
                impl_class = Class.forName(className);
            } catch (ClassNotFoundException ex) {
                System.err.println("Cannot find class " + className);
                ex.printStackTrace();
            }

            data_access_manager = createDataAccess();

            if (data_access_manager instanceof ICloneable) {
                cloneable = (ICloneable) data_access_manager;
                System.out.println("DataAccessImpl " + className + " implements " + ICloneable.class.getName());
            }
        } else {
            System.err.println("Cannot find key " + PropertiesKey.IMPL_DATA_ACCESS +
                    " in impl properties");
        }
    }

    private DataAccessFactory() {
    }

    private static IDataAccess createDataAccess() {
        IDataAccess result = null;
        Object obj = null;
        try {
            obj = impl_class.newInstance();
        } catch (IllegalAccessException ex) {
            System.err.println("Cannot instantiate class " + impl_class.getName());
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            System.err.println("Cannot instantiate class " + impl_class.getName());
            ex.printStackTrace();
        }

        try {
            result = (IDataAccess) obj;
        } catch (ClassCastException ex) {
            System.err.println("Class " + impl_class.getName() + " does not implement " + IDataAccess.class.getName());
            ex.printStackTrace();
        }
        return result;
    }


    public static IDataAccess getSharedDataAccessManager() {
        return data_access_manager;
    }

    public static IDataAccess newDataAccessManager() {
        if (cloneable == null) {
            return createDataAccess();
        } else {
            return (IDataAccess) cloneable.clone();
        }
    }

}
