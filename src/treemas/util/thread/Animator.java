package treemas.util.thread;

public class Animator extends Thread {
    private static final int SLEEP_TIME = 750;
    private char[] sequences = new char[]{'-', '\\', '|', '/'};
    private boolean keepRunning;

    public Animator() {
    }

    public void run() {
        this.keepRunning = true;
        int idx = 0;
        int len = sequences.length;
        while (this.keepRunning) {
            System.out.print(sequences[idx++]);
            System.out.print('\b');
            idx = (idx % len);
            try {
                Thread.currentThread().sleep(SLEEP_TIME);
            } catch (InterruptedException ie) {
            }
        }
    }

    public void startAnimate() {
        this.keepRunning = false;

        this.start();

        while (!this.keepRunning) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException ie) {
            }
        }
    }

    public void stopAnimate() {
        this.keepRunning = false;
        try {
            this.join();
        } catch (InterruptedException ie) {
        }
    }
}