package treemas.globalbean;

import com.ibatis.sqlmap.client.SqlMapClient;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import treemas.util.CoreException;
import treemas.util.constant.SQLConstant;
import treemas.util.cryptography.MD5;
import treemas.util.generator.AbstractPasswordGenerator;
import treemas.util.ibatis.IbatisHelper;
import treemas.util.struts.ManagerBase;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class PasswordManagerGenerator extends AbstractPasswordGenerator {
    public static final int PASSWORD_ERROR_IN_HISTORY = 80200;
    public static final int PASSWORD_HISTORY_TYPE_NEW = 1;
    public static final int PASSWORD_HISTORY_TYPE_CHANGE = 2;
    public static final int PASSWORD_HISTORY_TYPE_RESET = 3;
    private static final String STRING_SQL = SQLConstant.SQL_CHANGE_PASSWORD;
    protected SqlMapClient ibatisSqlMap;

    public PasswordManagerGenerator() {
        super();
        this.ibatisSqlMap = IbatisHelper.getSqlMap();
    }

    public PasswordManagerGenerator(int minimumLength, int passwordHistory) {
        super(minimumLength, passwordHistory);
        this.ibatisSqlMap = IbatisHelper.getSqlMap();
    }

    public String hashPassword(String string) {
        String hash = null;
        try {
            hash = MD5.getInstance().hashData(string);
        } catch (NoSuchAlgorithmException e) {
            this.logger.printStackTrace(e);
        }
        return hash;
    }

    public boolean validatePasswordHistory(String userId, String password)
            throws SQLException {
        boolean valid = true;
        try {
            Integer passwordHistory = new Integer(this.getPasswordHistory());

            Map paramMap = new HashMap();
            paramMap.put("userId", userId);
            paramMap.put("passwordHistory", passwordHistory);
            paramMap.put("password", password);

            Integer inHistory = (Integer) this.ibatisSqlMap.queryForObject(
                    STRING_SQL + "checkHistory", paramMap);

            if (inHistory == null || inHistory.intValue() > 0) {
                return false;
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw sqle;
        }
        return valid;
    }

    public boolean checkPasswordValidity(String userId, String string)
            throws CoreException {
        boolean valid = true;

        try {
            if (validateMinimumLength) {
                if (!validateMinimumLength(string)) {
                    throw new CoreException(PASSWORD_ERROR_MIN_LENGTH);
                }
            }

            String password = this.hashPassword(string);

            if (validateHistory) {
                if (!validatePasswordHistory(userId, password)) {
                    throw new CoreException(PASSWORD_ERROR_IN_HISTORY);
                }
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ManagerBase.ERROR_DB);
        }

        return valid;
    }

    public boolean cekHistory(String userId, String string, ActionMessages messages) throws CoreException {
        boolean valid = true;
        try {

            String password = this.hashPassword(string);
            if (validateHistory) {
                if (!validatePasswordHistory(userId, password)) {
                    messages.add("newPassword", new ActionMessage("appmgr.password.history", string));
                    throw new CoreException(PASSWORD_ERROR_IN_HISTORY);
                }
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ManagerBase.ERROR_DB);
        }

        return valid;
    }

    public boolean checkHashedPasswordValidity(String userId,
                                               String hashedString) throws CoreException {
        boolean valid = true;

        try {
            if (validateHistory) {
                if (!validatePasswordHistory(userId, hashedString)) {
                    throw new CoreException(PASSWORD_ERROR_IN_HISTORY);
                }
            }
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw new CoreException(sqle, ManagerBase.ERROR_DB);
        }

        return valid;
    }

    public void insertPasswordHistory(String userId, String password, int type,
                                      String usrUpd, String oldPass) throws SQLException {
        try {
            Map paramMap = new HashMap();
            paramMap.put("userId", userId);
            paramMap.put("password", password);
            paramMap.put("type", String.valueOf(type));
            paramMap.put("usrupd", usrUpd);
            paramMap.put("oldPassword", oldPass);

            this.ibatisSqlMap.insert(STRING_SQL + "insertPasswordHistory", paramMap);
        } catch (SQLException sqle) {
            this.logger.printStackTrace(sqle);
            throw sqle;
        }
    }

}
