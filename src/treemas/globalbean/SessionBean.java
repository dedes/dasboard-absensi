package treemas.globalbean;

import java.util.List;

public class SessionBean extends LoginBean {
    private String dwilingual;
    private String userid;
    private String password;
    private String isLogin;
    private String isLocked;
    private String branchLevel;
    private String jobDesc;
    private String companyId;
    private String companyName;
    private String companyIcon;
    private String branchId;
    private String branchName;
    private String changedPassword;
    private String privilege_editor;
    private List homeinfo;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIsLogin() {
        return isLogin;
    }

    public void setIsLogin(String isLogin) {
        this.isLogin = isLogin;
    }

    public String getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(String isLocked) {
        this.isLocked = isLocked;
    }

    public String getBranchLevel() {
        return branchLevel;
    }

    public void setBranchLevel(String branchLevel) {
        this.branchLevel = branchLevel;
    }

    public String getJobDesc() {
        return jobDesc;
    }

    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyIcon() {
        return companyIcon;
    }

    public void setCompanyIcon(String companyIcon) {
        this.companyIcon = companyIcon;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getChangedPassword() {
        return changedPassword;
    }

    public void setChangedPassword(String changedPassword) {
        this.changedPassword = changedPassword;
    }

    public String getPrivilege_editor() {
        return privilege_editor;
    }

    public void setPrivilege_editor(String privilege_editor) {
        this.privilege_editor = privilege_editor;
    }

    public String getDwilingual() {
        return dwilingual;
    }

    public void setDwilingual(String dwilingual) {
        this.dwilingual = dwilingual;
    }

    public List getHomeinfo() {
        return homeinfo;
    }

    public void setHomeinfo(List homeinfo) {
        this.homeinfo = homeinfo;
    }

}
