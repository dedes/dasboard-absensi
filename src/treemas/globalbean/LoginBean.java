package treemas.globalbean;

import java.io.Serializable;

public class LoginBean implements Serializable {

    private String loginId;
    private String fullName;

    public LoginBean() {
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = (loginId.trim());
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

}
