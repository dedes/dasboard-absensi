package com.google.common.base;

public class Strings {
    public static boolean isNullOrEmpty(String key) {
        if (null == key) {
            return true;
        } else if ("".equals(key.trim())) {
            return true;
        } else if ("null".equals(key.trim()) || "NULL".equals(key.trim())) {
            return true;
        }
        return false;
    }
}
